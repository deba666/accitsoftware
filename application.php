<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<title>Application Form</title>

<!-- Bootstrap -->
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/custom.css" rel="stylesheet">
<link rel="stylesheet" href="css/build.css"/>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">


<!--Date Picker-->
<link rel="stylesheet" href="css/calender.css" type="text/css">


<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
  <script src="http://code.jquery.com/jquery-1.10.2.js"></script>
  <script>
  $(document).ready(function(){
    $('.category').on('change', function(){
      var category_list = [];
      $('#filters :input:checked').each(function(){
        var category = $(this).val();
        category_list.push(category);
      });
      if(category_list.length == 0)
        $('.resultblock').fadeIn();
      else {
        $('.resultblock').each(function(){
          var item = $(this).attr('data-tag');
          if(jQuery.inArray(item,category_list) > -1)
            $(this).fadeIn('slow');
          else
            $(this).hide();
        });
      }   
    });
  }); 
  </script>
</head>
<body>

<header>
  <div class="container">
    <div class="row">
      <div class="col-md-2 col-sm-2 col-xs-12">
        <h2 class="logo"><a href="index.html"><img src="images/logo.png" class="img-responsive"></a></h2>
      </div>
      <div class="col-md-7 col-sm-7 col-xs-12">
        <marquee><h4>Welcome To <span>ACCIT</span>Application Section</h4></marquee>
      </div>
      <div class="col-md-2 col-sm-2 col-xs-12">
        <a href="javascript:void(0);" >
          <div class="close pull-right" data-toggle="tooltip" data-placement="left" title="Back to Home">
            <img src="images/close-icon.png" class="img-responsive" >
          </div>
        </a>
      </div>
    </div>
  </div>
</header>
    


<section class="enrolment-section">
<div class="enrolment-form">
          <!-- multistep form -->
          <form id="msform" >
            <!-- fieldsets -->
            <fieldset>
              <h2 class="fs-title">step 1</h2>
              <div class="row" id="filter">
                <div class="col-md-12 col-sm-12">
                  <div class="form-group">
                    <label class="col-md-12 col-sm-12 control-label">What Course do you looking for?</label>
                    <div class="col-md-12 col-sm-12 control-label">
                      <div class="checkbox checkbox-success col-md-12 col-sm-12 col-xs-12" 
                          style="padding-left: 36px;">
                        <input id="checkboxvisa1" class="styled" value="business" type="checkbox">
                        <label for="checkboxvisa1">
                            Business
                        </label>
                      </div>
                      <div class="checkbox checkbox-success col-md-12 col-sm-12 col-xs-12" 
                          style="padding-left: 36px;">
                        <input id="checkboxvisa2" class="styled" value="it" type="checkbox">
                        <label for="checkboxvisa2">
                            IT
                        </label>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <input type="button" name="next" class="next action-button" value="Next" />             
            </fieldset>

            <!-- step2 -->
            <fieldset>
              <h2 class="fs-title">step 2</h2>
              <div class="row">
                <div class="col-md-12 col-sm-12">
                  <div class="form-group">
                    <label class="col-md-12 col-sm-12 control-label">Lorem Ipsum is simply dummy text of the printing and typesetting industry?</label>
                    <div class="col-md-12 col-sm-12 control-label">
                      <div class="radio radio-danger">
                        <input type="radio" name="fast" id="id_radio3" value="cer" checked>
                        <label for="id_radio3">
                            Certificate
                        </label>
                      </div>
                      <div class="radio radio-danger">
                        <input type="radio" name="fast" id="id_radio4" value="dip" >
                        <label for="id_radio4">
                            Diploma
                        </label>
                      </div>
                      
                      <div class="radio radio-danger">
                        <input type="radio" name="fast" id="id_radio5" value="com" >
                        <label for="id_radio5">
                            Combine Course
                        </label>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <input type="button" name="previous" class="previous action-button" value="Previous" />
              <input type="button" name="next" class="next action-button" value="Next" />       
            </fieldset>

            <!-- Step3 -->
            <fieldset class="front-address-field">
              <h2 class="fs-title">step 3</h2>
              <div class="row">
                <div class="col-md-12 col-sm-12">
                  <div class="form-group" data-tag="business">
                    <label class="col-md-12 col-sm-12 control-label">1. Lorem Ipsum is simply dummy text of the printing and typesetting industry.</label>
                    <div class="col-md-12 col-sm-12 control-label">
                      <div class="checkbox checkbox-success col-md-12 col-sm-12 col-xs-12" style="padding-left: 36px;">
                        <input id="checkboxvisa4" class="styled" type="checkbox">
                        <label for="checkboxvisa4">
                           <span class="numbering">A</span> Business
                        </label>
                      </div>
                      <div class="checkbox checkbox-success col-md-12 col-sm-12 col-xs-12" style="padding-left: 36px;">
                        <input id="checkboxvisa5" class="styled" type="checkbox">
                        <label for="checkboxvisa5">
                            <span class="numbering">B</span>IT
                        </label>
                      </div>
                      <div class="checkbox checkbox-success col-md-12 col-sm-12 col-xs-12" style="padding-left: 36px;">
                        <input id="checkboxvisa6" class="styled" type="checkbox">
                        <label for="checkboxvisa6">
                            <span class="numbering">C</span>Business
                        </label>
                      </div>
                      <div class="checkbox checkbox-success col-md-12 col-sm-12 col-xs-12" style="padding-left: 36px;">
                        <input id="checkboxvisa7" class="styled" type="checkbox">
                        <label for="checkboxvisa7">
                            <span class="numbering">D</span>IT
                        </label>
                      </div>
                    </div>
                  </div>

                  <div class="clearfix"></div>

                  <div class="form-group" data-tag="business">
                    <label class="col-md-12 col-sm-12 control-label">2. Lorem Ipsum is simply dummy text of the printing and typesetting industry.</label>
                    <div class="col-md-12 col-sm-12 control-label">
                      <div class="checkbox checkbox-success col-md-12 col-sm-12 col-xs-12" style="padding-left: 36px;">
                        <input id="checkboxvisa8" class="styled" type="checkbox">
                        <label for="checkboxvisa8">
                           <span class="numbering">A</span> work
                        </label>
                      </div>
                      <div class="checkbox checkbox-success col-md-12 col-sm-12 col-xs-12" style="padding-left: 36px;">
                        <input id="checkboxvisa9" class="styled" type="checkbox">
                        <label for="checkboxvisa9">
                            <span class="numbering">B</span>power
                        </label>
                      </div>
                      <div class="checkbox checkbox-success col-md-12 col-sm-12 col-xs-12" style="padding-left: 36px;">
                        <input id="checkboxvisa10" class="styled" type="checkbox">
                        <label for="checkboxvisa10">
                            <span class="numbering">C</span>electricity

                        </label>
                      </div>
                      <div class="checkbox checkbox-success col-md-12 col-sm-12 col-xs-12" style="padding-left: 36px;">
                        <input id="checkboxvisa11" class="styled" type="checkbox">
                        <label for="checkboxvisa11">
                            <span class="numbering">D</span>IT
                        </label>
                      </div>
                    </div>
                  </div>

                  <div class="clearfix"></div>

                  <div class="form-group" data-tag="it">
                    <label class="col-md-12 col-sm-12 control-label">3. Lorem Ipsum is simply dummy text of the printing and typesetting industry.</label>
                    <div class="col-md-12 col-sm-12 control-label">
                      <div class="checkbox checkbox-success col-md-12 col-sm-12 col-xs-12" style="padding-left: 36px;">
                        <input id="checkboxvisa12" class="styled" type="checkbox">
                        <label for="checkboxvisa12">
                           <span class="numbering">A</span> work
                        </label>
                      </div>
                      <div class="checkbox checkbox-success col-md-12 col-sm-12 col-xs-12" style="padding-left: 36px;">
                        <input id="checkboxvisa13" class="styled" type="checkbox">
                        <label for="checkboxvisa13">
                            <span class="numbering">B</span>power
                        </label>
                      </div>
                      <div class="checkbox checkbox-success col-md-12 col-sm-12 col-xs-12" style="padding-left: 36px;">
                        <input id="checkboxvisa14" class="styled" type="checkbox">
                        <label for="checkboxvisa14">
                            <span class="numbering">C</span>electricity

                        </label>
                      </div>
                      <div class="checkbox checkbox-success col-md-12 col-sm-12 col-xs-12" style="padding-left: 36px;">
                        <input id="checkboxvisa15" class="styled" type="checkbox">
                        <label for="checkboxvisa15">
                            <span class="numbering">D</span>IT
                        </label>
                      </div>
                    </div>
                  </div>

                  <div class="clearfix"></div>

                  <div class="form-group" data-tag="it">
                    <label class="col-md-12 col-sm-12 control-label">4. Lorem Ipsum is simply dummy text of the printing and typesetting industry.</label>
                    <div class="col-md-12 col-sm-12 control-label">
                      <div class="checkbox checkbox-success col-md-12 col-sm-12 col-xs-12" style="padding-left: 36px;">
                        <input id="checkboxvisa16" class="styled" type="checkbox">
                        <label for="checkboxvisa16">
                           <span class="numbering">A</span> work
                        </label>
                      </div>
                      <div class="checkbox checkbox-success col-md-12 col-sm-12 col-xs-12" style="padding-left: 36px;">
                        <input id="checkboxvisa17" class="styled" type="checkbox">
                        <label for="checkboxvisa17">
                            <span class="numbering">B</span>power
                        </label>
                      </div>
                      <div class="checkbox checkbox-success col-md-12 col-sm-12 col-xs-12" style="padding-left: 36px;">
                        <input id="checkboxvisa18" class="styled" type="checkbox">
                        <label for="checkboxvisa18">
                            <span class="numbering">C</span>electricity

                        </label>
                      </div>
                      <div class="checkbox checkbox-success col-md-12 col-sm-12 col-xs-12" style="padding-left: 36px;">
                        <input id="checkboxvisa19" class="styled" type="checkbox">
                        <label for="checkboxvisa19">
                            <span class="numbering">D</span>IT
                        </label>
                      </div>
                    </div>
                  </div>





                   
                </div>
              </div>

              <div class="clearfix"></div>
              
              <input type="button" name="previous" class="previous action-button" value="Previous" />
              <input type="submit" name="submit" class="submit action-button" value="Submit" />
              
            </fieldset>

          </form>
        </div>
        
        
  <?php /*?><div class="container">
    <div class="row">
      <div class="col-ms-12 col-sm-12 col-xs-12">
        <div id="step-enrolment">
          <h2>Student Application</h2>
          <p class="text-center">Follow these&nbsp;<b>3 steps</b>&nbsp;to start your Application at ACCIT</p>
        </div>
        <div class="container">
 	<div id="filters">
 		<div class="filterblock">
	 		  <input id="check1" type="checkbox" name="check" value="game" class="category">
    		<label for="check1">Business</label>
	 	</div>

 		<div class="filterblock">
	 		  <input id="check2" type="checkbox" name="check" value="technology" class="category">
    		<label for="check2">IT</label>
	 	</div>

	 	<div class="filterblock">
	 		  <input id="check3" type="checkbox" name="check" value="cer" class="category">
    		<label for="check3">Certificate</label>
	 	</div>
        <div class="filterblock">
	 		  <input id="check4" type="checkbox" name="check" value="diploma" class="category">
    		<label for="check4">Diploma</label>
	 	</div>
        <div class="filterblock">
	 		  <input id="check5" type="checkbox" name="check" value="com" class="category">
    		<label for="check5">Combine Course</label>
	 	</div>
 	</div>
 	<div class="searchresults">
 		
 		<div class="resultblock" data-tag="game">
 			<div class="form-group" data-tag="it">
                    <label class="col-md-12 col-sm-12 control-label">3. Lorem Ipsum is simply dummy text of the printing and typesetting industry.</label>
                    <div class="col-md-12 col-sm-12 control-label">
                      <div class="checkbox checkbox-success col-md-12 col-sm-12 col-xs-12" style="padding-left: 36px;">
                        <input id="checkboxvisa12" class="styled" type="checkbox">
                        <label for="checkboxvisa12">
                           <span class="numbering">A</span> work
                        </label>
                      </div>
                      <div class="checkbox checkbox-success col-md-12 col-sm-12 col-xs-12" style="padding-left: 36px;">
                        <input id="checkboxvisa13" class="styled" type="checkbox">
                        <label for="checkboxvisa13">
                            <span class="numbering">B</span>power
                        </label>
                      </div>
                      <div class="checkbox checkbox-success col-md-12 col-sm-12 col-xs-12" style="padding-left: 36px;">
                        <input id="checkboxvisa14" class="styled" type="checkbox">
                        <label for="checkboxvisa14">
                            <span class="numbering">C</span>electricity

                        </label>
                      </div>
                      <div class="checkbox checkbox-success col-md-12 col-sm-12 col-xs-12" style="padding-left: 36px;">
                        <input id="checkboxvisa15" class="styled" type="checkbox">
                        <label for="checkboxvisa15">
                            <span class="numbering">D</span>IT
                        </label>
                      </div>
                    </div>
                  </div>
 		</div>

 		<div class="resultblock" data-tag="cer">
 			<div class="form-group" data-tag="business">
                    <label class="col-md-12 col-sm-12 control-label">1. Lorem Ipsum is simply dummy text of the printing and typesetting industry.</label>
                    <div class="col-md-12 col-sm-12 control-label">
                      <div class="checkbox checkbox-success col-md-12 col-sm-12 col-xs-12" style="padding-left: 36px;">
                        <input id="checkboxvisa4" class="styled" type="checkbox">
                        <label for="checkboxvisa4">
                           <span class="numbering">A</span> Business
                        </label>
                      </div>
                      <div class="checkbox checkbox-success col-md-12 col-sm-12 col-xs-12" style="padding-left: 36px;">
                        <input id="checkboxvisa5" class="styled" type="checkbox">
                        <label for="checkboxvisa5">
                            <span class="numbering">B</span>IT
                        </label>
                      </div>
                      <div class="checkbox checkbox-success col-md-12 col-sm-12 col-xs-12" style="padding-left: 36px;">
                        <input id="checkboxvisa6" class="styled" type="checkbox">
                        <label for="checkboxvisa6">
                            <span class="numbering">C</span>Business
                        </label>
                      </div>
                      <div class="checkbox checkbox-success col-md-12 col-sm-12 col-xs-12" style="padding-left: 36px;">
                        <input id="checkboxvisa7" class="styled" type="checkbox">
                        <label for="checkboxvisa7">
                            <span class="numbering">D</span>IT
                        </label>
                      </div>
                    </div>
                  </div>
 		</div>

 		<div class="resultblock" data-tag="technology">
 			<div class="form-group" data-tag="business">
                    <label class="col-md-12 col-sm-12 control-label">2. Lorem Ipsum is simply dummy text of the printing and typesetting industry.</label>
                    <div class="col-md-12 col-sm-12 control-label">
                      <div class="checkbox checkbox-success col-md-12 col-sm-12 col-xs-12" style="padding-left: 36px;">
                        <input id="checkboxvisa8" class="styled" type="checkbox">
                        <label for="checkboxvisa8">
                           <span class="numbering">A</span> work
                        </label>
                      </div>
                      <div class="checkbox checkbox-success col-md-12 col-sm-12 col-xs-12" style="padding-left: 36px;">
                        <input id="checkboxvisa9" class="styled" type="checkbox">
                        <label for="checkboxvisa9">
                            <span class="numbering">B</span>power
                        </label>
                      </div>
                      <div class="checkbox checkbox-success col-md-12 col-sm-12 col-xs-12" style="padding-left: 36px;">
                        <input id="checkboxvisa10" class="styled" type="checkbox">
                        <label for="checkboxvisa10">
                            <span class="numbering">C</span>electricity

                        </label>
                      </div>
                      <div class="checkbox checkbox-success col-md-12 col-sm-12 col-xs-12" style="padding-left: 36px;">
                        <input id="checkboxvisa11" class="styled" type="checkbox">
                        <label for="checkboxvisa11">
                            <span class="numbering">D</span>IT
                        </label>
                      </div>
                    </div>
                  </div>
 		</div>

 		<div class="resultblock" data-tag="com">
 			<div class="form-group" data-tag="it">
                    <label class="col-md-12 col-sm-12 control-label">3. Lorem Ipsum is simply dummy text of the printing and typesetting industry.</label>
                    <div class="col-md-12 col-sm-12 control-label">
                      <div class="checkbox checkbox-success col-md-12 col-sm-12 col-xs-12" style="padding-left: 36px;">
                        <input id="checkboxvisa12" class="styled" type="checkbox">
                        <label for="checkboxvisa12">
                           <span class="numbering">A</span> work
                        </label>
                      </div>
                      <div class="checkbox checkbox-success col-md-12 col-sm-12 col-xs-12" style="padding-left: 36px;">
                        <input id="checkboxvisa13" class="styled" type="checkbox">
                        <label for="checkboxvisa13">
                            <span class="numbering">B</span>power
                        </label>
                      </div>
                      <div class="checkbox checkbox-success col-md-12 col-sm-12 col-xs-12" style="padding-left: 36px;">
                        <input id="checkboxvisa14" class="styled" type="checkbox">
                        <label for="checkboxvisa14">
                            <span class="numbering">C</span>electricity

                        </label>
                      </div>
                      <div class="checkbox checkbox-success col-md-12 col-sm-12 col-xs-12" style="padding-left: 36px;">
                        <input id="checkboxvisa15" class="styled" type="checkbox">
                        <label for="checkboxvisa15">
                            <span class="numbering">D</span>IT
                        </label>
                      </div>
                    </div>
                  </div>
 		</div>

 		<div class="resultblock" data-tag="diploma">
 			<div class="form-group" data-tag="it">
                    <label class="col-md-12 col-sm-12 control-label">3. Lorem Ipsum is simply dummy text of the printing and typesetting industry.</label>
                    <div class="col-md-12 col-sm-12 control-label">
                      <div class="checkbox checkbox-success col-md-12 col-sm-12 col-xs-12" style="padding-left: 36px;">
                        <input id="checkboxvisa12" class="styled" type="checkbox">
                        <label for="checkboxvisa12">
                           <span class="numbering">A</span> work
                        </label>
                      </div>
                      <div class="checkbox checkbox-success col-md-12 col-sm-12 col-xs-12" style="padding-left: 36px;">
                        <input id="checkboxvisa13" class="styled" type="checkbox">
                        <label for="checkboxvisa13">
                            <span class="numbering">B</span>power
                        </label>
                      </div>
                      <div class="checkbox checkbox-success col-md-12 col-sm-12 col-xs-12" style="padding-left: 36px;">
                        <input id="checkboxvisa14" class="styled" type="checkbox">
                        <label for="checkboxvisa14">
                            <span class="numbering">C</span>electricity

                        </label>
                      </div>
                      <div class="checkbox checkbox-success col-md-12 col-sm-12 col-xs-12" style="padding-left: 36px;">
                        <input id="checkboxvisa15" class="styled" type="checkbox">
                        <label for="checkboxvisa15">
                            <span class="numbering">D</span>IT
                        </label>
                      </div>
                    </div>
                  </div>
 		</div>

 		<div class="resultblock" data-tag="os">
 			<div class="form-group" data-tag="it">
                    <label class="col-md-12 col-sm-12 control-label">3. Lorem Ipsum is simply dummy text of the printing and typesetting industry.</label>
                    <div class="col-md-12 col-sm-12 control-label">
                      <div class="checkbox checkbox-success col-md-12 col-sm-12 col-xs-12" style="padding-left: 36px;">
                        <input id="checkboxvisa12" class="styled" type="checkbox">
                        <label for="checkboxvisa12">
                           <span class="numbering">A</span> work
                        </label>
                      </div>
                      <div class="checkbox checkbox-success col-md-12 col-sm-12 col-xs-12" style="padding-left: 36px;">
                        <input id="checkboxvisa13" class="styled" type="checkbox">
                        <label for="checkboxvisa13">
                            <span class="numbering">B</span>power
                        </label>
                      </div>
                      <div class="checkbox checkbox-success col-md-12 col-sm-12 col-xs-12" style="padding-left: 36px;">
                        <input id="checkboxvisa14" class="styled" type="checkbox">
                        <label for="checkboxvisa14">
                            <span class="numbering">C</span>electricity

                        </label>
                      </div>
                      <div class="checkbox checkbox-success col-md-12 col-sm-12 col-xs-12" style="padding-left: 36px;">
                        <input id="checkboxvisa15" class="styled" type="checkbox">
                        <label for="checkboxvisa15">
                            <span class="numbering">D</span>IT
                        </label>
                      </div>
                    </div>
                  </div>
 		</div> 

 		<div class="resultblock" data-tag="technology">
 			<div class="form-group" data-tag="it">
                    <label class="col-md-12 col-sm-12 control-label">3. Lorem Ipsum is simply dummy text of the printing and typesetting industry.</label>
                    <div class="col-md-12 col-sm-12 control-label">
                      <div class="checkbox checkbox-success col-md-12 col-sm-12 col-xs-12" style="padding-left: 36px;">
                        <input id="checkboxvisa12" class="styled" type="checkbox">
                        <label for="checkboxvisa12">
                           <span class="numbering">A</span> work
                        </label>
                      </div>
                      <div class="checkbox checkbox-success col-md-12 col-sm-12 col-xs-12" style="padding-left: 36px;">
                        <input id="checkboxvisa13" class="styled" type="checkbox">
                        <label for="checkboxvisa13">
                            <span class="numbering">B</span>power
                        </label>
                      </div>
                      <div class="checkbox checkbox-success col-md-12 col-sm-12 col-xs-12" style="padding-left: 36px;">
                        <input id="checkboxvisa14" class="styled" type="checkbox">
                        <label for="checkboxvisa14">
                            <span class="numbering">C</span>electricity

                        </label>
                      </div>
                      <div class="checkbox checkbox-success col-md-12 col-sm-12 col-xs-12" style="padding-left: 36px;">
                        <input id="checkboxvisa15" class="styled" type="checkbox">
                        <label for="checkboxvisa15">
                            <span class="numbering">D</span>IT
                        </label>
                      </div>
                    </div>
                  </div>
 		</div>

 		<div class="resultblock" data-tag="game">
 			<div class="form-group" data-tag="it">
                    <label class="col-md-12 col-sm-12 control-label">3. Lorem Ipsum is simply dummy text of the printing and typesetting industry.</label>
                    <div class="col-md-12 col-sm-12 control-label">
                      <div class="checkbox checkbox-success col-md-12 col-sm-12 col-xs-12" style="padding-left: 36px;">
                        <input id="checkboxvisa12" class="styled" type="checkbox">
                        <label for="checkboxvisa12">
                           <span class="numbering">A</span> work
                        </label>
                      </div>
                      <div class="checkbox checkbox-success col-md-12 col-sm-12 col-xs-12" style="padding-left: 36px;">
                        <input id="checkboxvisa13" class="styled" type="checkbox">
                        <label for="checkboxvisa13">
                            <span class="numbering">B</span>power
                        </label>
                      </div>
                      <div class="checkbox checkbox-success col-md-12 col-sm-12 col-xs-12" style="padding-left: 36px;">
                        <input id="checkboxvisa14" class="styled" type="checkbox">
                        <label for="checkboxvisa14">
                            <span class="numbering">C</span>electricity

                        </label>
                      </div>
                      <div class="checkbox checkbox-success col-md-12 col-sm-12 col-xs-12" style="padding-left: 36px;">
                        <input id="checkboxvisa15" class="styled" type="checkbox">
                        <label for="checkboxvisa15">
                            <span class="numbering">D</span>IT
                        </label>
                      </div>
                    </div>
                  </div>
 		</div>

 		<div class="resultblock" data-tag="os">
 			<div class="form-group" data-tag="it">
                    <label class="col-md-12 col-sm-12 control-label">3. Lorem Ipsum is simply dummy text of the printing and typesetting industry.</label>
                    <div class="col-md-12 col-sm-12 control-label">
                      <div class="checkbox checkbox-success col-md-12 col-sm-12 col-xs-12" style="padding-left: 36px;">
                        <input id="checkboxvisa12" class="styled" type="checkbox">
                        <label for="checkboxvisa12">
                           <span class="numbering">A</span> work
                        </label>
                      </div>
                      <div class="checkbox checkbox-success col-md-12 col-sm-12 col-xs-12" style="padding-left: 36px;">
                        <input id="checkboxvisa13" class="styled" type="checkbox">
                        <label for="checkboxvisa13">
                            <span class="numbering">B</span>power
                        </label>
                      </div>
                      <div class="checkbox checkbox-success col-md-12 col-sm-12 col-xs-12" style="padding-left: 36px;">
                        <input id="checkboxvisa14" class="styled" type="checkbox">
                        <label for="checkboxvisa14">
                            <span class="numbering">C</span>electricity

                        </label>
                      </div>
                      <div class="checkbox checkbox-success col-md-12 col-sm-12 col-xs-12" style="padding-left: 36px;">
                        <input id="checkboxvisa15" class="styled" type="checkbox">
                        <label for="checkboxvisa15">
                            <span class="numbering">D</span>IT
                        </label>
                      </div>
                    </div>
                  </div>
 		</div>		

 	</div>
 </div>
        
      </div>
    </div>
  </div><?php */?>
  
</section>
<!---->

<div class="clearfix"></div>


<footer>
  
</footer>




<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>

<!--Date Picker-->
<script type="text/javascript" src="js/zebra_datepicker.js"></script>
<script type="text/javascript" src="js/core.js"></script>

<!-- jQuery easing plugin -->
<script src="js/jquery.easing.min.js" type="text/javascript"></script>
<script src="js/classie.js"></script>
<script src="js/custom.js"></script>



<script>
    function init() {
        window.addEventListener('scroll', function(e){
            var distanceY = window.pageYOffset || document.documentElement.scrollTop,
                shrinkOn = 300,
                header = document.querySelector("header");
            if (distanceY > shrinkOn) {
                classie.add(header,"smaller");
            } else {
                if (classie.has(header,"smaller")) {
                    classie.remove(header,"smaller");
                }
            }
        });
    }
    window.onload = init();
</script>
<script type="text/javascript">
    function changeState(el) {
        if (el.readOnly) el.checked=el.readOnly=false;
        else if (!el.checked) el.readOnly=el.indeterminate=true;
    }
</script>
<script>
$(document).ready(function () {
    $('#id_radio1').click(function () {
       $('.slidingDiv').show('fast');
});
$('#id_radio2').click(function () {
      $('.slidingDiv').hide('fast');
 });
});
</script>
<script>
$(document).ready(function () {
    $('#id_radio6').click(function () {
       $('.slidingDiv2').show('fast');
       $('.slidingDiv7').hide('fast');
       $('.slidingDiv8').hide('fast');
});
$('#id_radio3').click(function () {
      $('.slidingDiv2').hide('fast');
      $('.slidingDiv7').hide('fast');
      $('.slidingDiv8').hide('fast');
 });
$('#id_radio4').click(function () {
      $('.slidingDiv2').hide('fast');
      $('.slidingDiv7').hide('fast');
      $('.slidingDiv8').hide('fast');
 });
$('#id_radio5').click(function () {
      $('.slidingDiv2').hide('fast');
      $('.slidingDiv7').hide('fast');
      $('.slidingDiv8').hide('fast');
 });
$('#id_radio7').click(function () {
      $('.slidingDiv2').hide('fast');
      $('.slidingDiv7').show('fast');
      $('.slidingDiv8').hide('fast');
 });
$('#id_radio8').click(function () {
      $('.slidingDiv2').hide('fast');
      $('.slidingDiv7').hide('fast');
      $('.slidingDiv8').show('fast');

 });
});
</script>
<script>
$(document).ready(function () {
    $('#id_radiovisa').click(function () {
       $('.slidingvisa').show('fast');
       $('.slidingvisa2').hide('fast');
       $('.slidingvisa3').hide('fast');
    });
    $('#id_radiovisa2').click(function () {
       $('.slidingvisa').hide('fast');
       $('.slidingvisa2').show('fast');
       $('.slidingvisa3').hide('fast');
    });
    $('#id_radiovisa3').click(function () {
       $('.slidingvisa').hide('fast');
       $('.slidingvisa2').hide('fast');
       $('.slidingvisa3').show('fast');
    });
});
</script>
<script>
$(document).ready(function () {
    $('#id_radiocourse').click(function () {
       $('.slidingcourse1').show('fast');
       $('.slidingcourse2').hide('fast');
       $('.slidingcourse3').hide('fast');
       $('.slidingcourse4').hide('fast');
       $('.slidingcourse5').hide('fast');
    });
    $('#id_radiocourse2').click(function () {
       $('.slidingcourse1').hide('fast');
       $('.slidingcourse2').show('fast');
       $('.slidingcourse3').hide('fast');
       $('.slidingcourse4').hide('fast');
       $('.slidingcourse5').hide('fast');
    });
    $('#id_radiocourse3').click(function () {
       $('.slidingcourse1').hide('fast');
       $('.slidingcourse2').hide('fast');
       $('.slidingcourse3').show('fast');
       $('.slidingcourse4').hide('fast');
       $('.slidingcourse5').hide('fast');
    });
    $('#id_radiocourse4').click(function () {
       $('.slidingcourse1').hide('fast');
       $('.slidingcourse2').hide('fast');
       $('.slidingcourse3').hide('fast');
       $('.slidingcourse4').show('fast');
       $('.slidingcourse5').hide('fast');
    });
    $('#id_radiocourse5').click(function () {
       $('.slidingcourse1').hide('fast');
       $('.slidingcourse2').hide('fast');
       $('.slidingcourse3').hide('fast');
       $('.slidingcourse4').hide('fast');
       $('.slidingcourse5').show('fast');
    });
});
</script>
<script>
$(document).ready(function () {
    $('#id_radiocourse6').click(function () {
       $('.slidingcourse6').show('fast');
});
$('#id_radiocourse7').click(function () {
      $('.slidingcourse6').hide('fast');
 });
});
</script>
<script>
$(document).ready(function () {
    $('#checkboxvisa3').click(function () {
       $('#passporthide').slideToggle('fast');
    });
});
</script>
<script type="text/javascript">
  $(function () {
    $('[data-toggle="tooltip"]').tooltip()
  })
</script>

<script type="text/javascript">
 $('#datepicker-example10').Zebra_DatePicker({
      view: 'years'
  });
</script>
<script type="text/javascript">
  $('#datepicker-example8').Zebra_DatePicker({
      format: 'M d, Y'
  });
</script>
<script type="text/javascript">
  $('#datepicker-example9').Zebra_DatePicker({
      format: 'M d, Y'
  });
</script>
<script type="text/javascript">
  $('#datepicker-exampleeducation').Zebra_DatePicker({
      format: 'M d, Y'
  });
</script>
<script type="text/javascript">
  $('#datepicker-exampleflight').Zebra_DatePicker({
      format: 'M d, Y'
  });
</script>

<!--<script type="text/javascript">
 $('#datepicker-example11').Zebra_DatePicker({
      view: 'years'
  });
</script>
<script type="text/javascript">
 $('#datepicker-example12').Zebra_DatePicker({
      view: 'years'
  });
</script>
<script type="text/javascript">
 $('#datepicker-example13').Zebra_DatePicker({
      view: 'years'
  });
</script>-->

<!--Another Course-->
<script>
$(document).ready(function () {
    $('#id_radioanothercourse').click(function () {
       $('.slidinganothercourse1').show('fast');
       $('.slidinganothercourse2').hide('fast');
       $('.slidinganothercourse3').hide('fast');
       $('.slidinganothercourse4').hide('fast');
       $('.slidinganothercourse5').hide('fast');
    });
    $('#id_radioanothercourse2').click(function () {
       $('.slidinganothercourse1').hide('fast');
       $('.slidinganothercourse2').show('fast');
       $('.slidinganothercourse3').hide('fast');
       $('.slidinganothercourse4').hide('fast');
       $('.slidinganothercourse5').hide('fast');
    });
    $('#id_radioanothercourse3').click(function () {
       $('.slidinganothercourse1').hide('fast');
       $('.slidinganothercourse2').hide('fast');
       $('.slidinganothercourse3').show('fast');
       $('.slidinganothercourse4').hide('fast');
       $('.slidinganothercourse5').hide('fast');
    });
    $('#id_radioanothercourse4').click(function () {
       $('.slidinganothercourse1').hide('fast');
       $('.slidinganothercourse2').hide('fast');
       $('.slidinganothercourse3').hide('fast');
       $('.slidinganothercourse4').show('fast');
       $('.slidinganothercourse5').hide('fast');
    });
    $('#id_radioanothercourse5').click(function () {
       $('.slidinganothercourse1').hide('fast');
       $('.slidinganothercourse2').hide('fast');
       $('.slidinganothercourse3').hide('fast');
       $('.slidinganothercourse4').hide('fast');
       $('.slidinganothercourse5').show('fast');
    });
});
</script>

<script>
$(document).ready(function () {
    $('#id_radioanothercourse6').click(function () {
       $('.slidinganothercourse6').show('fast');
});
$('#id_radioanothercourse7').click(function () {
      $('.slidinganothercourse6').hide('fast');
 });
});
</script>
<script>
$(document).ready(function () {
    $('#id_radiocourse8').click(function () {
       $('.slidingcourse8').show('fast');
});
$('#id_radiocourse9').click(function () {
      $('.slidingcourse8').hide('fast');
 });
});
</script>

<script>
$(document).ready(function () {
    $('#id_radiocourse16').click(function () {
       $('.slidingcourse16').show('fast');
});
$('#id_radiocourse17').click(function () {
      $('.slidingcourse16').hide('fast');
 });
});
</script>

<script>
$(document).ready(function () {
    $('.add-course').click(function () {
       $('.add-another-course-div').show('fast');
       $('.add-course').hide('fast');
       $('.remove-another-course').show('fast');
    });
    $('.remove-another-course').click(function () {
       $('.add-another-course-div').hide('fast');
       $('.add-course').show('fast');
       $('.remove-another-course').hide('fast');
    });
});
</script>
<script>
$(document).ready(function () {
    $('#id_radioeducation').click(function () {
    $('.slidingeducation1').show('fast');
});
$('#id_radioeducation2').click(function () {
      $('.slidingeducation1').hide('fast');
 });
$('#id_radioeducation3').click(function () {
      $('.slidingeducation1').hide('fast');
 });
});
</script>
<script>
$(document).ready(function () {
    $('#id_radiowork-program').click(function () {
    $('.slidingwork-program1').show('fast');
});
$('#id_radiowork-program2').click(function () {
      $('.slidingwork-program1').hide('fast');
 });
$('#id_radiowork-program3').click(function () {
      $('.slidingwork-program1').hide('fast');
 });
});
</script>

<script>
$(document).ready(function () {
    $('#id_radioflight').click(function () {
    $('.slidingflight1').show('fast');
});
$('#id_radioflight2').click(function () {
      $('.slidingflight1').hide('fast');
 });
});
</script>

</body>
</html>
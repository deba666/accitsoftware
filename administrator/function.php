<?php

    /*
        * @package    ACCIT
         * @author    Paperlink Softwares Team
         * @copyright    Copyright (c) 2016 , Paperlink Softwares Pvt. Ltd.  (http://www.paperlinksoftwares.com/)
         * @since    Version 1.0.0
         * @filesource
		 * Code for Database Functions
         */

	function select_record($query)
	{
		$rst=mysql_query($query);
		if($rst)
		{
			while($row=@mysql_fetch_assoc($rst))
			{
				$query_rst[]=$row;
			}
			return $query_rst;
		}
		else
		{echo  mysql_error();}
	}
	
	
	function insert_record($query)
	{
		if(mysql_query($query))	
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	function update_record($tbl_name,$set,$condition)
	{
		if($condition!="")
		{
			$whr=" WHERE ".$condition;
		}
		$update_qry="UPDATE ".$tbl_name." SET ".$set.$whr; //echo $update_qry; die;
		if(mysql_query($update_qry))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	function delete_record($query)
	{
		if(mysql_query($query))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	function check_duplicate($tbl_name,$pri_key,$condition)
	{
		if($condition!="")
		{
			$whr=" WHERE ".$condition;
		}
		//echo "SELECT ".$pri_key." FROM ".$tbl_name.$whr;
		$query=mysql_query("SELECT ".$pri_key." FROM ".$tbl_name.$whr);
		if(mysql_num_rows($query)==0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

//Function to fetch data with or without condition 
function getCount($condition='', $table_name)
{
	$sql = "SELECT * FROM `".$table_name."` ";
	if($condition!='')
	{
		$sql = $sql.$condition;
	}
	
	$res = mysql_query($sql) or die(mysql_error()." Error in getName : ".$sql);
	$count=mysql_num_rows($res);
	if($count>0)
	{
		return $count;
	}
	else
		{return $count;}
		

		
	
}


//Function to fetch data with or without condition 
function getRows($condition='', $table_name)
{
	$sql = "SELECT * FROM `".$table_name."` ";
	if($condition!='')
	{
		$sql = $sql.$condition;
	}
	//echo $sql; die;
	$res = mysql_query($sql) or die(mysql_error()." Error in getName : ".$sql);
	if(mysql_num_rows($res)>0)
		{
			while($row=@mysql_fetch_assoc($res))
			{
				$query_rst[]=$row;
			}
			return $query_rst;
		}
		else
		{echo  mysql_error();}
	
}

//Function to fetch data with or without condition 
function getRowsArray($condition='', $table_name)
{
	$sql = "SELECT`".$table_name."` ";
	if($condition!='')
	{
		$sql = $sql.$condition;
	}
	//echo $sql; die;
	$res = mysql_query($sql) or die(mysql_error()." Error in getName : ".$sql);
	if(mysql_num_rows($res)>0)
		{
			while($row=@mysql_fetch_assoc($res))
			{
				$query_rst[]=$row;
			}
			return $query_rst;
		}
		else
		{echo  mysql_error();}
	
}

///////////// Get randum Records///////////////
function getRandRows($condition='', $table_name)
{
	$sql = "SELECT * FROM `".$table_name."` ";
	if($condition!='')
	{
		$sql = $sql.$condition;
	}
	
	$sql .=" ORDER BY rand( )";		
	$res = mysql_query($sql) or die(mysql_error()." Error in getRandRows : ".$sql);
	if(mysql_num_rows($res)>0)
		{
			while($row=@mysql_fetch_assoc($res))
			{
				$query_rst[]=$row;
			}
			return $query_rst;
		}
		else
		{echo  mysql_error();}
	
}

///////////// Search Records///////////////
function searchVideo($condition='', $table_name)
{
	$sql = "SELECT * FROM `".$table_name."` ";
	if($condition!='')
	{
		$sql = $sql."where ".$condition;
	}
	
	$sql .=" ORDER BY rand( )";	//echo $sql; die;	
	$res = mysql_query($sql) or die(mysql_error()." Error in getRandRows : ".$sql);
	if(mysql_num_rows($res)>0)
		{
			while($row=@mysql_fetch_assoc($res))
			{
				$query_rst[]=$row;
			}
			return $query_rst;
		}
		else
		{echo  mysql_error();}
	
}



//Function to fetch the name of a Category
function getName($id, $table, $match_field, $fetch_field)
{
	$sql_getName = "SELECT `".$fetch_field."` FROM `".$table."` WHERE `".$match_field."` = ".$id; 
	$rs_getName = mysql_query($sql_getName) or die(mysql_error()." Error in getName : ".$sql_getName);

	$rec_getName = mysql_fetch_array($rs_getName);

	$getName = stripslashes($rec_getName[$fetch_field]);
	return $getName; 
}

// change status function
function change_status($row_id, $table_name, $field_name, $id_field)
{
	$sql = "update ".$table_name." set ".$field_name." = if(".$field_name."='0','1','0') where ".$id_field."=".$row_id;	
	if (!mysql_query($sql)) 
		echo mysql_error();
	else 
		$_SESSION['p_msg'] ="Status changed successfully";
}

//Function to fetch the multilevel Category with UL LI

function hasChild($parent_id)
  {
    $sql = "SELECT COUNT(*) as count FROM category WHERE parent_id =" . $parent_id;
    $qry = mysql_query($sql);
    $rs = mysql_fetch_array($qry);
    return $rs['count'];
  }
  
  function CategoryTree($list,$parent,$append)
  {
    $list = '<li>'.$parent['category_name'].'</li>';
    
    if (hasChild($parent['category_id'])) // check if the id has a child
    {
      $append++; // this is our basis on what level is the category e.g. (child1,child2,child3)
      $list .= "<ul class='child child".$append." '>";
      $sql = "SELECT * FROM category WHERE parent_id =" . $parent['category_id']." order by category_name";
      $qry = mysql_query($sql);
      $child = mysql_fetch_array($qry);
      do{
        $list .= CategoryTree($list,$child,$append);
      }while($child = mysql_fetch_array($qry));
      $list .= "</ul>";
    }
    return $list;
  }
  function CategoryList()
  {
    $list = "";
    
    $sql = "SELECT * FROM category WHERE (parent_id = 0 OR parent_id IS NULL) order by category_name";
    $qry = mysql_query($sql);
    $parent = mysql_fetch_array($qry);
    $mainlist = "<ul class='parent'>";
    do{
      $mainlist .= CategoryTree($list,$parent,$append = 0);
    }while($parent = mysql_fetch_array($qry));
    $mainlist .= "</ul>";
    return $mainlist;
  }



/* GET THE DROP DOWN LIST OF CATEGORIES */

function get_cat_selectlist($current_cat_id, $count) {

static $option_results;
// if there is no current category id set, start off at the top level (zero)
if (!isset($current_cat_id)) {
$current_cat_id =0;
}
// increment the counter by 1
$count = $count+1;

// query the database for the sub-categories of whatever the parent category is
$sql =  'SELECT `category_id`, `category_name` from `category` where `is_active`=1 and `parent_id` ='.$current_cat_id;
$sql .=  ' order by `category_name`';

$get_options = mysql_query($sql);
$num_options = mysql_num_rows($get_options);

// our category is apparently valid, so go ahead €¦
if ($num_options > 0) {
while (list($cat_id, $cat_name) = mysql_fetch_row($get_options)) {

// if its not a top-level category, indent it to
//show that its a child category

if ($current_cat_id!=0) {
$indent_flag =  '&nbsp;';
for ($x=2; $x <= $count; $x++) {
$indent_flag .=  '&nbsp;&nbsp;';
}
}
$cat_name = $indent_flag.$cat_name;
$option_results[$cat_id] = $cat_name;
// now call the function again, to recurse through the child categories
get_cat_selectlist($cat_id, $count );
}
}
return $option_results;
}


// Time format is UNIX timestamp or
  // PHP strtotime compatible strings
  function dateDiff($time1, $time2, $precision = 6) {
    // If not numeric then convert texts to unix timestamps
    if (!is_int($time1)) {
      $time1 = strtotime($time1);
    }
    if (!is_int($time2)) {
      $time2 = strtotime($time2);
    }
 
    // If time1 is bigger than time2
    // Then swap time1 and time2
    if ($time1 > $time2) {
      $ttime = $time1;
      $time1 = $time2;
      $time2 = $ttime;
    }
 
    // Set up intervals and diffs arrays
    $intervals = array('year','month','day','hour','minute','second');
    $diffs = array();
 
    // Loop thru all intervals
    foreach ($intervals as $interval) {
      // Create temp time from time1 and interval
      $ttime = strtotime('+1 ' . $interval, $time1);
      // Set initial values
      $add = 1;
      $looped = 0;
      // Loop until temp time is smaller than time2
      while ($time2 >= $ttime) {
        // Create new temp time from time1 and interval
        $add++;
        $ttime = strtotime("+" . $add . " " . $interval, $time1);
        $looped++;
      }
 
      $time1 = strtotime("+" . $looped . " " . $interval, $time1);
      $diffs[$interval] = $looped;
    }
 
    $count = 0;
    $times = array();
    // Loop thru all diffs
    foreach ($diffs as $interval => $value) {
      // Break if we have needed precission
      if ($count >= $precision) {
	break;
      }
      // Add value and interval 
      // if value is bigger than 0
      if ($value > 0) {
	// Add s if value is not 1
	if ($value != 1) {
	  $interval .= "s";
	}
	// Add value and interval to times array
	$times[] = $value . " " . $interval;
	 
	$count++;
      }
    }
 
    // Return string with times
    //return implode(", ", $times);
	if($times[0]=="1 day")
		return "Today";
	else
		return $times[0]." ago";
  }
  
function toCoordinates($Address)
{
    	
	$Address = urlencode($Address);
	$request_url = "http://maps.googleapis.com/maps/api/geocode/xml?address=".$Address."&sensor=true";
	//echo $request_url;
	$xml = simplexml_load_file($request_url) or die("url not loading");
	$status = $xml->status;
	if ($status=="OK") {
	  $Lat = $xml->result->geometry->location->lat;
	  $Lon = $xml->result->geometry->location->lng;
	  $LatLng = "$Lat,$Lon";
	}
	return $LatLng;
}

//Get an array with geoip-infodata
 function geoCheckIP($ip)
 
{
 
//check, if the provided ip is valid
 
if(!filter_var($ip, FILTER_VALIDATE_IP))
 
{
 
throw new InvalidArgumentException("IP is not valid");
 
}
 
//contact ip-server
 
$response=@file_get_contents('http://www.netip.de/search?query='.$ip);
 
if (empty($response))
 
{
 
throw new InvalidArgumentException("Error contacting Geo-IP-Server");
 
}
 
//Array containing all regex-patterns necessary to extract ip-geoinfo from page
 
$patterns=array();
 
$patterns["domain"] = '#Domain: (.*?)&nbsp;#i';
 
$patterns["country"] = '#Country: (.*?)&nbsp;#i';
 
$patterns["state"] = '#State/Region: (.*?)<br#i';
 
$patterns["town"] = '#City: (.*?)<br#i';
 
//Array where results will be stored
 
$ipInfo=array();

 
//check response from ipserver for above patterns
 
foreach ($patterns as $key => $pattern)
 
{
 
//store the result in array
 
$ipInfo[$key] = preg_match($pattern,$response,$value) && !empty($value[1]) ? $value[1] : 'not found';
 
}
/*I've included the substr function for Country to exclude the abbreviation (UK, US, etc..)
To use the country abbreviation, simply modify the substr statement to:
substr($ipInfo["country"], 0, 3)
*/
//$ipdata = $ipInfo["town"]. ", ".$ipInfo["state"].", ".substr($ipInfo["country"], 4);
 
return($ipInfo);
 
}

function generateRandomPassword() {
  //Initialize the random password
  $password = '';

  //Initialize a random desired length
  $desired_length = rand(8, 12);

  for($length = 0; $length < $desired_length; $length++) {
    //Append a random ASCII character (including symbols)
    $password .= chr(rand(32, 126));
  }

  return $password;
}

//Function to fetch data with or without condition 
function getOptionList($condition='', $value_field, $display_field, $selected, $table_name)
{
	$sql = "SELECT * FROM `".$table_name."` ";
	if($condition!='')
	{
		$sql = $sql.$condition;
	}
	//echo $sql; die;
	$res = mysql_query($sql) or die(mysql_error()." Error in getName : ".$sql);
	if(mysql_num_rows($res)>0)
		{
			while($row=@mysql_fetch_array($res))
			{
				if($row[$value_field]==$selected)
					$option .="<option value='$row[$value_field]' selected='selected'>$row[$display_field]</option>";
				else
					$option .="<option value='$row[$value_field]'>$row[$display_field]</option>";
			}
			return $option;
		}
		else
		{echo  mysql_error();}
	
}


function upload_file($allowedExts,$file,$path)
{

	$allowedExts = array("gif", "jpeg", "jpg", "png");
	$temp = explode(".", $_FILES["file"]["name"]);
	$extension = end($temp);
	if ((($_FILES["file"]["type"] == "image/gif")
	|| ($_FILES["file"]["type"] == "image/jpeg")
	|| ($_FILES["file"]["type"] == "image/jpg")
	|| ($_FILES["file"]["type"] == "image/pjpeg")
	|| ($_FILES["file"]["type"] == "image/x-png")
	|| ($_FILES["file"]["type"] == "image/png"))
	&& ($_FILES["file"]["size"] < 200000)
	&& in_array($extension, $allowedExts))
	  {
	  if ($_FILES["file"]["error"] > 0)
		{
		echo "Return Code: " . $_FILES["file"]["error"] . "<br>";
		}
	  else
		{
		echo "Upload: " . $_FILES["file"]["name"] . "<br>";
		echo "Type: " . $_FILES["file"]["type"] . "<br>";
		echo "Size: " . ($_FILES["file"]["size"] / 1024) . " kB<br>";
		echo "Temp file: " . $_FILES["file"]["tmp_name"] . "<br>";
	
		if (file_exists("upload/" . $_FILES["file"]["name"]))
		  {
		  echo $_FILES["file"]["name"] . " already exists. ";
		  }
		else
		  {
		  move_uploaded_file($_FILES["file"]["tmp_name"],
		  "upload/" . $_FILES["file"]["name"]);
		  echo "Stored in: " . "upload/" . $_FILES["file"]["name"];
		  }
		}
	  }
	else
	  {
	  echo "Invalid file";
	  }

}
function seoUrl($string) {
		//Lower case everything
		$string = strtolower($string);
		//Make alphanumeric (removes all other characters)
		$string = preg_replace("/[^a-z0-9_\s-]/", "", $string);
		//Clean up multiple dashes or whitespaces
		$string = preg_replace("/[\s-]+/", " ", $string);
		//Convert whitespaces and underscore to dash
		$string = preg_replace("/[\s_]/", "-", $string);
return $string;
}
//////////////////////Age Calculator//////////////////////////
function calcutateAge($dob){

        $dob = date("m/d/Y",strtotime($dob));

        $dobObject = new DateTime($dob);
        $nowObject = new DateTime();

        $diff = $dobObject->diff($nowObject);

        return $diff->y;

}
function UniqueRandomNumbersWithinRange($min, $max, $quantity) {
    $numbers = range($min, $max);
    shuffle($numbers);
    return array_slice($numbers, 0, $quantity);
}
////////////////////////////////////////////holiday////////////////////
function getWorkingDays($startDate,$endDate,$holidays) {
    // do strtotime calculations just once
    $endDate = strtotime($endDate);
    $startDate = strtotime($startDate);


    //The total number of days between the two dates. We compute the no. of seconds and divide it to 60*60*24
    //We add one to inlude both dates in the interval.
    $days = ($endDate - $startDate) / 86400 + 1;

    $no_full_weeks = floor($days / 7);
    $no_remaining_days = fmod($days, 7);

    //It will return 1 if it's Monday,.. ,7 for Sunday
    $the_first_day_of_week = date("N", $startDate);
    $the_last_day_of_week = date("N", $endDate);

    //---->The two can be equal in leap years when february has 29 days, the equal sign is added here
    //In the first case the whole interval is within a week, in the second case the interval falls in two weeks.
    if ($the_first_day_of_week <= $the_last_day_of_week) {
        if ($the_first_day_of_week <= 6 && 6 <= $the_last_day_of_week) $no_remaining_days--;
        if ($the_first_day_of_week <= 7 && 7 <= $the_last_day_of_week) $no_remaining_days--;
    }
    else {
        // (edit by Tokes to fix an edge case where the start day was a Sunday
        // and the end day was NOT a Saturday)

        // the day of the week for start is later than the day of the week for end
        if ($the_first_day_of_week == 7) {
            // if the start date is a Sunday, then we definitely subtract 1 day
            $no_remaining_days--;

            if ($the_last_day_of_week == 6) {
                // if the end date is a Saturday, then we subtract another day
                $no_remaining_days--;
            }
        }
        else {
            // the start date was a Saturday (or earlier), and the end date was (Mon..Fri)
            // so we skip an entire weekend and subtract 2 days
            $no_remaining_days -= 2;
        }
    }

    //The no. of business days is: (number of weeks between the two dates) * (5 working days) + the remainder
//---->february in none leap years gave a remainder of 0 but still calculated weekends between first and last day, this is one way to fix it
   $workingDays = $no_full_weeks * 5;
    if ($no_remaining_days > 0 )
    {
      $workingDays += $no_remaining_days;
    }

    //We subtract the holidays
    foreach($holidays as $holiday){
        $time_stamp=strtotime($holiday);
        //If the holiday doesn't fall in weekend
        if ($startDate <= $time_stamp && $time_stamp <= $endDate && date("N",$time_stamp) != 6 && date("N",$time_stamp) != 7)
            $workingDays--;
    }

    return $workingDays;
}

//Example:

 /*$holidays=array("2008-12-25","2008-12-26","2009-01-01");

echo getWorkingDays("2008-12-22","2009-01-02",$holidays)*/
// => will return 7




/* IMAGE UPLOAD FUNCTION */

function title($op)
{
	switch ($op)
	{
	   case MD5('settings'): 		      
	   			return "settings";
	   break;
	   case MD5('avetmiss_report'): 		      
	   			return "Avetmiss Report Generate";
	   break;
	   case MD5('avetmiss_esoc_report'): 		      
	   			return "Avetmiss ESOC Report Generate";
	   break;
	   case MD5('mail_send'):
				return "Send Mail";
	   break;
	   
	   case MD5('profile'): 		      
	   			return "Profile";
	   break;
	   case MD5('logout'): 		      
	   			return "Logout";
	   break;
	   case MD5('change'): 		      
	   			return "Changes";
	   break;
       case MD5('holidays'): 		      
	   			return "Holiday List";
	   break;
	   case MD5('holidays_add'): 		      
	   			return "New Holiday Add";
	   break;
	    case MD5('holidays_edit'): 		      
	   			return "Modify Holiday Data";
	   break;
	    case MD5('holi'): 		      
	   			return "Holiday";
	   break;
	    /////////////////////////Configarations/////////////////////////////
	   
	   case MD5('config'): 		      
	   			return "Configuration";
	   break;
	   case MD5('receipt_new'): 		      
	   			return "Add New Receipt";
	   break;

	   case MD5('make_payment'): 		      
	   			return "Make Payment";
	   break;
	    case MD5('pay_amount'): 		      
	   			return "Pay Fees Amount";
	   break;

/////////////////////////Extend Enrolment/////////////////////////////
	   
	   case MD5('changes_extend_enrolment'): 		      
	   			return "Extend Enrolment";
	   break;
	   case MD5('changes_shorten'): 		      
	   			return "Enrolment Shorten";
	   break;

	   case MD5('changes_start_date'): 		      
	   			return "Enrolment Start Date Change";
	   break;
	    case MD5('changes_cancel_enrolment'): 		      
	   			return "Cancel Enrolment";
	   break;
	    /////////////////////////Users/////////////////////////////
	   
	    case MD5('users'): 		      
	   			return "User List";
	   break;
	   case MD5('user_add'): 		      
	   			return "New User Add";
	   break;
	   case MD5('accordian'): 		      
	   			return "settings";
	   break;
	   	   
	   /////////////////////////student Page/////////////////////////////
	   
	    case MD5('student'):
				return "All Student";
	   break;
	   
	   case MD5('student_add'):
				return "Add New Student";
				
	   break;
	   
	    case MD5('student_details'):
				return "Student Details";
	   break;
	   
	   case MD5('student_edit'):
				return "Modify Student Data";
				
	   break;
	   case MD5('display_student_contact'):
				return "Add New Contact Person For Student";
	   break;
	   case MD5('display_student_contact_edit'):
				return "Modify Student Contact Person Data";
	   break;
	   case MD5('display_offer_agent'):
				return "Select Agent";
	   break;
	   case MD5('display_offer_course'):
				return "Select Course";
	   break;
	   case MD5('display_offer_course_edit'):
				return "Modify Offer Course";
	   break;
	   case MD5('display_new_course'):
				return "Add New Offer Course";
	   break;
	    case MD5('display_new_course_edit'):
				return "Modify Offer Course Data";
	   break;
	   case MD5('display_other_offer'):
				return "Other Offer";
	   break;
	   case MD5('display_other_offer_edit'):
				return "Modify Other Offer";
	   break;
	   case MD5('display_other_offer_after_enrolment'):
				return "Other Offer";
	   break;
	   case MD5('display_other_offer_after_enrolment_edit'):
				return "Modify Other Offer";
	   break;
	   
	     /////////////////////////Offer Page/////////////////////////////
	    case MD5('offer'):
				return "Make New Offer";
	   break;
	   case MD5('student_2'):
				return "All Student";
	   break;
	   
	   case MD5('offer_all'):
				return "All Offer List";
	    break;

	   case MD5('offer_edit'):
				return "Modify Offer";
	    break;
		case MD5('offer_add'):
				return "New Offer Add";
	    break;

	   case MD5('offer_details'):
				return "Offer Details";
	   break;
	   case MD5('split_offer'):
				return "Split Offer";
	   break;
	   case MD5('display_offer_others_items'):
				return "Other Offer Item";
	   break;
	   case MD5('display_offer_others_items_after_enrolment'):
				return "Other Offer Item";
	   break;
	   case MD5('display_offer_others_items_edit'):
				return "Modify Other Offer Item";
	   break;
	   case MD5('display_new_insurance'):
				return "Add Insurence";
	   break;
	   case MD5('display_offer_insurance'):
				return "New Insurance";
	   break;
	   /////////////////////////Enrollment/////////////////////////////
	    case MD5('enroll_language'):
				return "Language Enrolment";
	   break;
	   
	   case MD5('enroll_module'):
				return "Module Enrolment";
	   break;
	   
	   case MD5('enroll_language_details'):
				return "Language Enrolment";
	   break;
	   case MD5('enrolment_details'):
				return "Enrolment Details";
	   break;
	   case MD5('display_student_diary'):
				return "Student Diary";
	   break;
	   case MD5('display_leave'):
				return "Student Leave";
	   break;
	   case MD5('display_leave_edit'):
				return "Modify Student Leave";
	   break;
	   case MD5('display_student_absence'):
				return "Student Absent Entry";
	   break;
	   case MD5('display_student_absence_edit'):
				return "Modify Student Absent Data";
	   break;
	   case MD5('enroll_debit'):
				return "Debit Wizard";
	   break;
	    case MD5('enroll_debit_transfer'):
				return "Debit Transfer";
	   break;
	   
	    case MD5('enroll_debit_transfer_other_page'):
				return "Transfer Fees To Another Page";
	   break;
	   
	   case MD5('enroll_debit_transfer_other_enrolment'):
				return "Transfer Fees To Another Enrollment";
	   break;
	    case MD5('enroll_debit_refund_student'):
				return "Student Fees Refund";
	   break;
	    case MD5('enroll_debit_refund_agent'):
				return "Agent Fees Refund";
	   break;
	   case MD5('enroll_debit2'):
				return "Debit Wizard";
	   break;
	   case MD5('enroll_debit_operation'):
				return "Debit Operation";
	   break;
	   
	   case MD5('enroll_credit'):
				return "Credit Wizard";
	   break;
	   case MD5('enroll_credit_discount'):
				return "Credit Discount";
	   break;
	   case MD5('enroll_credit_discount_operation'):
				return "Credit Discount Operation";
				
	   case MD5('enroll_credit_writeoff'):
				return "Credit Writeoff";
	   case MD5('enroll_credit_writeoff_operation'):
				return "Credit Writeoff Operation";
	   break;
	   
	   case MD5('enroll_credit_currency_fluctuation'):
				return "Currency Fluctuation";
	   break;
	   case MD5('enroll_credit_currency_fluctuation_operation'):
				return "Credit Currency Fluctuation Operation";
	   break;
	   
	    case MD5('enroll_credit_bank_charge'):
				return "Credit Bank Charge";
	   break;
	   case MD5('enroll_credit_bank_charge_operation'):
				return "Credit Bank Change Operation";
	   break;
	   
	    case MD5('enroll_credit_agent_fee'):
				return "Credit Agent Fees";
	   break;
	   case MD5('enroll_credit_agent_fee_operation'):
				return "Agent Fees Operation";
	   break;
	   
	    case MD5('enroll_credit_cancel_outstanding_fees'):
				return "Cancel Outstanding Fees";
	   break;
	   case MD5('enroll_credit_cancel_outstanding_fees_operation'):
				return "Cancel Outstanding Fees Operation";
	   break;
	   
	   case MD5('enroll_credit_operation'):
				return "Credit Operation";
	   break;
           case MD5('enroll_debit'):
				return "Debit Wizard";
	   break;
	   case MD5('enroll_debit2'):
				return "Debit Wizard";
	   break;
	   case MD5('enroll_debit_notice'):
				return "Debit Wizard";
	   case MD5('enroll_debit_notice_1'):
				return "Debit Wizard";
	   break;
	   case MD5('enroll_debit_operation'):
				return "Debit Operation";
	   break;
	   
	   case MD5('enroll_credit'):
				return "Credit Wizard";
	   break;
	   case MD5('enroll_credit2'):
				return "Credit Wizard";
	   break;
	   case MD5('enroll_credit_operation'):
				return "Credit Operation";
	   break;
	   
	   

	   /////////////////////////Tour Groups/////////////////////////////
	   
	   case MD5('display_tour_all'): 		      
	   			return "All Student Tour List";
	   break;
	   case MD5('display_tour_all_edit'): 		      
	   			return "Modify Student Tour";
	   break;
	   
	    case MD5('display_tour_new'): 		      
	   			return "Student Tour";
	   break;
	    case MD5('display_tour_students'): 		      
	   			return "Add New Student Tour";
	   break;
	   case MD5('display_tour_students_edit'): 		      
	   			return "Modify Student Tour";
	   break;
	   break;
	    case MD5('display_tour_fees'): 		      
	   			return "Tour Fees";
	   break;
	   break;
	    case MD5('display_tour_airport'): 		      
	   			return "Add Tour Airport";
	   break;
	   case MD5('display_tour_airport_edit'): 		      
	   			return "Modify Tour Airport";
	   break;
	   break;
	    case MD5('display_tour_classes'): 		      
	   			return "Tour Classes";
	   break;
	   case MD5('display_tour_agent'): 		      
	   			return "Tour Agent";
	   break;
	   
	   /////////////////////////Diary/////////////////////////////
	    case MD5('diary_student'):
				return "Student Diary";
	   break;
	   
	   case MD5('diary_agent'):
				return "Agent Diary";
	   break;
	   
	   case MD5('diary_accomodation'):
				return "Diary Accomodation";
	   break;

	    case MD5('display_stu_diary'):
				return "Add New Student Diary";
	   break;
	   case MD5('display_student_diary_edit'):
				return "Modify Student Diary";
	   break;

	    
	   /////////////////////////classes/////////////////////////////
	   case MD5('classes'):
				return "All Classes List";
	   break;

	   case MD5('classes_add'):
				return "New Class Add";
	   break;
	   case MD5('classes_module'):
				return "Module Classes";
	   break;
	   case MD5('display_language_module'):
				return "All Language Module";
	   break;
	   case MD5('display_language_module_student_add'):
				return "Add New Module Language";
	   break;
	   case MD5('display_language_module_edit'):
				return "Modify Module Language";
	   break;

	   case MD5('classes_edit'):
				return "Modify Class Data";
	   break;

	   case MD5('classes_language'):
				return "All Language classes List";
	   break;

	    case MD5('display_language_class'):
				return "New Language classes Add";
	   break; 

	   case MD5('display_language_class_edit'):
				return "Modify Language classes";
	   break; 

	   case MD5('classes_language_enroll'):
				return "Language Enrolment";
	   break; 

	    case MD5('display_absence_class'):
				return "Student Absent";
	   break;


	   case MD5('display_absence_details'):
				return "Student Absent Details";
	   break;
	   
	   //////////////////////Language/////////////////////////////////
	   
	    case MD5('classes_language'):
				return "Language classes";
	   break;

	    case MD5('classes_module'):
				return "Module classes";
	   break; 
	    case MD5('classes_language_details'):
				return "Language classes Details";
	   break; 
	   
	   
	   ////////////////////////Room/////////////////////////////////////
	    case MD5('room'):
				return "All Room List";
	   break;
	   case MD5('room_add'):
				return "New Room Add";
	   break;   
	   
	   case MD5('room_edit'):
				return "Modify Room Data";
	   break;   
	   
	   /////////////////////////Prospectus/////////////////////////////
	    case MD5('prospectus'):
				return "settings";
	   break;
	   
	   case MD5('prospectus_edit'):
				return "settings";
	   break;
	   
	   /////////////////////////Advertise/////////////////////////////
	    case MD5('advertise'):
				return "settings";
	   break;
	   
	   case MD5('advertise_edit'):
				return "settings";
	   break;
	   
	  
	   /////////////////////////testimonial/////////////////////////////
	   
	   case MD5('testimonial'):
				return "Testimonial List";
	   break;
	   
	   case MD5('testimonial_edit'):
				return "Modify Testimonial";
	   break;
	   
	   /////////////////////////Student service/////////////////////////////
	    case MD5('services'):
				return "services";
	   break;

	    case MD5('services_edit'):
				return "settings";
	   break; 
	   
	   /////////////////////////Register applicant/////////////////////////////
	    case MD5('applicant'):
				return "applicant";
	   break;
	   
	   
	   /////////////////////////Faculties/////////////////////////////
	    case MD5('faculties'):
				return "All Faculties List";
	   break;

	    case MD5('faculties_add'):
				return "New Faculties Add";
	   break; 
	   
	   case MD5('faculties_edit'):
				return "Modify Faculties";
	   break; 
	   
	    /////////////////////////Teacher/////////////////////////////
	   case MD5('teacher'):
				return "All Teacher List";
	   break;
	    case MD5('teacher_add'):
				return "New Teacher Add";
	   break;
	    case MD5('teacher_edit'):
				return "Modify Teacher Data";
	   break;
	    case MD5('teacher_details'):
				return "Teacher Details";
	   break;
	   
	    /////////////////////////Course/////////////////////////////
	    case MD5('course'):
				return "All Course";
	   break;

	    case MD5('course_add'):
				return "New Course";
	   break; 
	   
	   case MD5('course_edit'):
				return "Modify Course";
	   break; 
	   
	   case MD5('courses_module'):
				return "Module Course";
	   break; 
	   
	   case MD5('courses_module_edit'):
				return "Module Course Modify";
	   break;	   
       
	   case MD5('courses_module_add'):
				return "Module Course Add";
	   break;

	   case MD5('display_module'):
				return "settings";
	   break;
	    case MD5('course_module_modify'):
				return "Course Module";
	   break;
	   case MD5('course_module_modify_add'):
				return "Module Course Add";
	   break;

		
	  /////////////////////////Fees/////////////////////////////
	    case MD5('fees'):
				return "Student Fees";
	   break;
	   case MD5('receipt'):
				return "New Receipt";
	   break;
	   case MD5('receipt_details'):
				return "Receipt Details";
	   break;
	   case MD5('receipt_payment'):
				return "Receipt Payment";
	   break;
	   case MD5('receipt_payment_edit'):
				return "Receipt Payment Edit";
	   break;
	   case MD5('debit'):
				return "Debit Wizard";
	   break;
	   case MD5('credit'):
				return "Credit Wizard";
	   break;
	   case MD5('new_invoice'):
				return "New Invoice";
	   break;
	    case MD5('receipt_invoice_select'):
				return "settings";
	   break;

		/////////////////////////Agents/////////////////////////////
	    case MD5('agent'):
				return "Agent List";
	   break;
	    case MD5('agent_add'):
				return "Add New Agent";
	   break;

	    case MD5('agent_edit'):
				return "Modify Agent";
	   break; 
	   case MD5('agent_details'):
				return "Agent Details";
	   break; 
	   case MD5('display_agent_employee'):
				return "Agent Employee";
	   break;
	    case MD5('display_agent_employee_edit'):
				return "Modify Agent Employee";
	   break;
	   
	   break; 
	   case MD5('display_agent_diary'):
				return "Agent Diary";
	   break;
	    case MD5('display_agent_diary_edit'):
				return "Agent Diary modify";
	   break;

	    break; 
	   case MD5('display_agent_commission'):
				return "New Agent Commission";
	   break;
	    case MD5('display_agent_commission_edit'):
				return "Agent Commission Modify";
	   break;
	   //////////////////////////workflow/////////////////////////////
	   case MD5('display_workflow_agent_detail'):
				return "Agents Workflow Details";
	   break;
           case MD5('mail_template'):
				return "Mail Template";
	   break;
           case MD5('workflow_history'):
				return "Workflow History";
	   break;
           /////////////////////////Reports////////////////////////////////

           case MD5('stud_lang_enroll_report'):
				return "Enrolment Reports";
	   break;
           
           case MD5('stud_lang_report_pop'):
				return "Language Reports";
	   break;
           
           case MD5('agent_report_pop'):
				return "Language Reports";
	   break;
           case MD5('agent_report'):
				return "Agents Reports";
	   break;

 /////////////////////////Accomodation////////////////////////////////

           case MD5('new_accom'):
				return "New Accomodation";
	   break;
	   case MD5('accom_provider'):
				return "Accomodation Provider";
	   break;
	   case MD5('accom_bed'):
				return "Student Bed Accomodation";
	   break;
	   
	    /////////////////////////Airport////////////////////////////////

           case MD5('airport_new'):
				return "New Airport Transport";
	   break;
	   case MD5('airport_new_edit'):
				return "Airport Transport Modify";
	   break;

	   /////////////////////////Visa////////////////////////////////

           case MD5('new_visa'):
				return "New Visa";
	   break;
	   case MD5('visa_edit'):
				return "Visa Modify";
	   break;
	   case MD5('ecoe'):
				return "Ecoe";
	   break;
	   case MD5('ecoe_edit'):
				return "Ecoe Modify";
	   break;
	    /////////////////////////Workflow/////////////////////////////
	    case MD5('workflow'):
				return "Workflow";
	   break;
	   
	   case MD5('workflow_edit'):
				return "Workflow Modify";
	   break;
	   case MD5('diary_workflow_details'):
				return "Diary Workflow Details";
	   break;
	   case MD5('student_1'):
				return "All Students";
	   break;
	   case MD5('agent_1'):
				return "All Agent";
	   break;
	   case MD5('all_agent_diary'):
				return "Agents Diary";
	   break;
	    case MD5('all_student_diary'):
				return "Students Diary";
	   break;
	   case MD5('student_workflow_details'):
				return "Student workflow Details";
	   break;
	   case MD5('enrolment_workflow_details'):
				return "Enrolment workflow Details";
	   break;
	   case MD5('classes_enrolment'):
				return "Started Enrolment Search";
	   break;
	   

	   
	   /////////////////////////Dashboard/////////////////////////////   
	    default :
				return "Home Page";
	   break;
	}
}


//CHECK ENROLMENT DATE IS UNDER CURRENT DATE
function check_in_range($start_date, $end_date, $date)
{
  // Convert to timestamp
  $start_ts = strtotime($start_date);
  $end_ts = strtotime($end_date);
  $user_ts = strtotime($date);

  // Check that user date is between start & end
  if(($user_ts >= $start_ts) && ($user_ts <= $end_ts))
	{
	  return "Current";
	}
	elseif(($user_ts >= $start_ts))
	{
		return "Finish";
	}

   else return "Not Started";	
}
function check_in_range_date($start_date, $end_date, $date)
{
  // Convert to timestamp
  $start_ts = strtotime($start_date);
  $end_ts = strtotime($end_date);
  $user_ts = strtotime($date);

  // Check that user date is between start & end
  if(($user_ts >= $start_ts) && ($user_ts <= $end_ts))
	{
	  return $user_ts;
	}
}

?>

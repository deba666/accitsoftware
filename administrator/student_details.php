<?php
    /*
        * @package    ACCIT
         * @author    Paperlink Softwares Team
         * @copyright    Copyright (c) 2016 , Paperlink Softwares Pvt. Ltd.  (http://www.paperlinksoftwares.com/)
         * @since    Version 1.0.0
         * @filesource
         * Code for display details of a particular student
         */
    
        if($_GET['sid'])
        {
            $cond="where `STUD_NO`=".$_GET['sid'];
            $std_row=getRows($cond,'student');
            $cond2="where `STUD_NO`=".$std_row[0]['STUD_NO'];
            $std_row2=getRows($cond2,'stu_contact'); 
            $s="select c1.COURSE_NAME,c1.USER_NO from course c1,student s1,enrol E1 where E1.STUD_NO=s1.STUD_NO and E1.COURSE_NO=C1.COURSE_NO and S1.STUD_NO=".$_GET['sid'];
            $q=mysql_query($s);
            $r=mysql_fetch_array($q);
            $s1="select d.* , s.customer_no,s.FNAME from document d,student s where d.create_user_no=s.USER_NO and s.STUD_NO=".$_GET['sid'];
            $q1=mysql_query($s1);
            $r1=mysql_fetch_array($q1);
            
        }
    
        if($_GET['del2'])
        {
            if(delete_record("DELETE FROM `enrol` WHERE `ENROL_NO` = ".$_GET['del2']))
                $_SESSION['s_msg']="Item successfully deleted";
        }
        if($_GET['del3'])
        {
            if(delete_record("DELETE FROM `diary` WHERE `DIARY_NO` = ".$_GET['del3']))
                $_SESSION['s_msg']="Item successfully deleted";
        }
        ?>
<div role="main" class="right_col" style="min-height: 782px;">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Student Details</h3>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12">
                <div class="x_panel">
                    <div class="clearfix"></div>
                    <div class="x_content">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel recent-app">
                                    <div class="x_content">
                                        <div class="student-details-page1">
                                            <div class="col-md-3 col-sm-3 col-xs-12">
                                                <div class="profile-left">
                                                    <div class="upload-pic">
                                                      
                                                            <div class="student-profile_pic">
                                                                <img  alt="" src="<?php echo $std_row[0]['PHOTO']; ?>" height="140" width="220">
                                                             
                                                            </div>
                                                       
                                                    </div>
                                                    <div class="table-responsive">
                                                        <table class="table table-striped jambo_table">
                                                            <tbody>
                                                                <tr class="even pointer">
                                                                    <td class="item">Student Id</td>
                                                                    <td class=""><?=$std_row[0]['EXT_STN']?></td>
                                                                </tr>
                                                                <tr class="even pointer">
                                                                    <td class="item">Name</td>
                                                                    <td class=""><?=$std_row[0]['FNAME']?> <?=$std_row[0]['MNAME']?> <?=$std_row[0]['LNAME']?></td>
                                                                </tr>
                                                                <tr class="even pointer">
                                                                    <td class="item">Course</td>
                                                                    <td class=""><?=$r['COURSE_NAME']?></td>
                                                                </tr>
                                                                <tr class="even pointer">
                                                                    <td class="item">Batch</td>
                                                                    <td class="">2013</td>
                                                                </tr>
                                                                <tr class="even pointer">
                                                                    <td class="item">Email Id</td>
                                                                    <td class=""><?=$std_row[0]['O_EMAIL']?></td>
                                                                </tr>
                                                                <tr class="even pointer">
                                                                    <td class="item">Mobile No</td>
                                                                    <td class=""> <?=$std_row[0]['O_PH']?></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                        <!--  <form method="post" action="mail.php">
                                                            <input type="text" name="std_no" value="<?=$std_row[0]['STUD_NO']?>" />
                                                            <input type="text" name="std_name" value="<?=$std_row[0]['FNAME']?> <?=$std_row[0]['MNAME']?> <?=$std_row[0]['LNMAE']?>" />
                                                            <input type="hidden" name="std_address" value="<?=$std_row[0]['std_address']?>" />
                                                            <input type="submit" value="Submit">
                                                            </form> -->
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                <div class="profile-right">
                                                    <div class="card">
                                                        <ul role="tablist" class="nav nav-tabs">
                                                            <li class="active" role="presentation"><a aria-expanded="true" data-toggle="tab" role="tab" aria-controls="personald" href="#personald"><i aria-hidden="true" class="fa fa-street-view"></i>Personal</a></li>
                                                            <li class="" role="presentation"><a aria-expanded="false" data-toggle="tab" role="tab" aria-controls="academicd" href="#academicd"><i aria-hidden="true" class="fa fa-graduation-cap"></i>Academic</a></li>
                                                            <li class="" role="presentation"><a aria-expanded="false" data-toggle="tab" role="tab" aria-controls="addressd" href="#addressd"><i aria-hidden="true" class="fa fa-home"></i>Address</a></li>
                                                            <li class="" role="presentation"><a aria-expanded="false" data-toggle="tab" role="tab" aria-controls="documentsd" href="#documentsd"><i aria-hidden="true" class="fa fa-file-text"></i>Documents</a></li>
                                                            <li class="" role="presentation"><a aria-expanded="false" data-toggle="tab" role="tab" aria-controls="feesd" href="#feesd"><i aria-hidden="true" class="fa fa-inr"></i>Fees</a></li>
                                                            <li class="" role="presentation"><a aria-expanded="false" data-toggle="tab" role="tab" aria-controls="diaryd" href="#diaryd"><i aria-hidden="true" class="fa fa-inr"></i>Diary</a></li>
                                                        </ul>
                                                        <div class="tab-content">
                                                            <div id="personald" class="tab-pane active" role="tabpanel">
                                                                <div class="personal-info-tab">
                                                                    <div class="edit-section">
                                                                        <h4><i aria-hidden="true" class="fa fa-info-circle"></i>Personal Details</h4>
                                                                        <span class="pull-right">
                                                                        <a href="dashboard.php?op=<?=MD5('student_edit')?>&sid=<?=$std_row[0]['STUD_NO'];?>&st_id=<?=$std_row[0]['student_id'];?>" class="color-sky stting"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>EDIT</a>
                                                                        </span>
                                                                    </div>
                                                                    <div class="clearfix"></div>
                                                                    <div class="table-responsive">
                                                                        <table class="table table-striped jambo_table">
                                                                            <tbody>
                                                                                <tr class="even pointer">
                                                                                    <td class="item">Title</td>
                                                                                    <?php if($std_row[0]['TITLE']=='1')
                                                                                        $title="Mr";
                                                                                        else
                                                                                        if($std_row[0]['TITLE']=='2')
                                                                                        $title="Dr";
                                                                                        else
                                                                                        if($std_row[0]['TITLE']=='3')
                                                                                        $title="Sir";
                                                                                        else
                                                                                        if($std_row[0]['TITLE']=='4')
                                                                                        $title="Ms";
                                                                                        
                                                                                        else
                                                                                        if($std_row[0]['TITLE']=='5')
                                                                                        $title="Mrs";
                                                                                        else
                                                                                        if($std_row[0]['TITLE']=='6')
                                                                                        $title="Miss";
                                                                                        
                                                                                        ?>
                                                                                    <td class=""><?php echo $title;?></td>
                                                                                </tr>
                                                                                <tr class="even pointer">
                                                                                    <td class="item">First Name</td>
                                                                                    <td class=""><?=$std_row[0]['FNAME']?></td>
                                                                                </tr>
                                                                                <tr class="even pointer">
                                                                                    <td class="item">Middle Name</td>
                                                                                    <td class=""> <?=$std_row[0]['MNAME']?></td>
                                                                                </tr>
                                                                                <tr class="even pointer">
                                                                                    <td class="item">Last Name</td>
                                                                                    <td class=""><?=$std_row[0]['LNAME']?></td>
                                                                                </tr>
                                                                                <tr class="even pointer">
                                                                                    <td class="item">Date of Birth</td>
                                                                                    <td class=""><?=$std_row[0]['DOB']?>  Age: <?php $dob = $std_row[0]['DOB']; echo calcutateAge($dob);?></td>
                                                                                </tr>
                                                                                <tr class="even pointer">
                                                                                    <td class="item">Birth Country Name</td>
                                                                                    <?php if($std_row[0]['BIRTH_COUNTRY_NAME'])
                                                                                        {
                                                                                        $cond3a="where `COUNTRY_NO`=".$std_row[0]['BIRTH_COUNTRY_NAME'];
                                                                                              $lana=getRows($cond3a,'country');
                                                                                        }?>
                                                                                    <td class=""><?=$lana[0]['COUNTRY_NAME']?></td>
                                                                                </tr>
                                                                                <tr class="even pointer">
                                                                                    <td class="item">Gender</td>
                                                                                    <td class=""><?=$std_row[0]['GENDER']?></td>
                                                                                </tr>
                                                                                <tr class="even pointer">
                                                                                    <td class="item">Nationality</td>
                                                                                    <td class=""><?=$std_row[0]['COUNTRY_NAME']?></td>
                                                                                </tr>
                                                                                <tr class="even pointer">
                                                                                    <td class="item">Language</td>
                                                                                    <?php if($std_row[0]['LANGUAGE_NO'])
                                                                                        {
                                                                                        $cond3="where `LANGUAGE_NO`=".$std_row[0]['LANGUAGE_NO'];
                                                                                              $lan=getRows($cond3,'language');
                                                                                        }?>
                                                                                    <td class=""><?=$lan[0]['LANGUAGE_DESC']?></td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div id="academicd" class="tab-pane" role="tabpanel">
                                                                <div class="academic-tab">
                                                                    <div class="edit-section">
                                                                        <h4><i aria-hidden="true" class="fa fa-info-circle"></i>Academic Details</h4>
                                                                    </div>
                                                                    <div class="clearfix"></div>
                                                                    <div class="table-responsive">
                                                                        <table class="table table-striped jambo_table">
                                                                            <tbody>
                                                                                <tr class="even pointer">
                                                                                    <td class="item">Examination</td>
                                                                                    <td class="">HS</td>
                                                                                </tr>
                                                                                <tr class="even pointer">
                                                                                    <td class="item">Board</td>
                                                                                    <td class="">WBBHSE</td>
                                                                                </tr>
                                                                                <tr class="even pointer">
                                                                                    <td class="item">Percentage</td>
                                                                                    <td class=""> 79%</td>
                                                                                </tr>
                                                                                <tr class="even pointer">
                                                                                    <td class="item">Year of Passing</td>
                                                                                    <td class="">2017</td>
                                                                                </tr>
                                                                                <tr class="even pointer">
                                                                                    <td class="item">Course</td>
                                                                                    <td class="">Diploma</td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div id="addressd" class="tab-pane" role="tabpanel">
                                                                <div class="academic-tab">
                                                                    <div class="edit-section">
                                                                        <h4><i aria-hidden="true" class="fa fa-info-circle"></i>Address</h4>
                                                                        <span class="pull-right">
                                                                        <a href="dashboard.php?op=<?=MD5('student_edit')?>&sid=<?=$std_row[0]['STUD_NO'];?>&st_id=<?=$std_row[0]['student_id'];?>" class="color-sky stting"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>EDIT</a>
                                                                        </span>
                                                                    </div>
                                                                    <div class="clearfix"></div>
                                                                    <div class="table-responsive">
                                                                        <table class="table table-striped jambo_table">
                                                                            <tbody>
                                                                                <tr class="even pointer">
                                                                                    <td class="item">Address1</td>
                                                                                    <td class=""><?=$std_row[0]['LADDR1']?></td>
                                                                                </tr>
                                                                                <tr class="even pointer">
                                                                                    <td class="item">Address2</td>
                                                                                    <td class=""><?=$std_row[0]['LADDR2']?></td>
                                                                                </tr>
                                                                                <tr class="even pointer">
                                                                                    <td class="item">Address3</td>
                                                                                    <td class=""><?=$std_row[0]['LADDR3']?></td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div id="documentsd" class="tab-pane" role="tabpanel">
                                                                <div class="academic-tab">
                                                                    <div class="edit-section">
                                                                        <h4><i aria-hidden="true" class="fa fa-info-circle"></i>Documents</h4>
                                                                    </div>
                                                                    <div class="clearfix"></div>
                                                                    <div class="table-responsive">
                                                                        <table class="table table-striped jambo_table">
                                                                            <tbody>
                                                                                <tr class="even pointer">
                                                                                    <td class="item">Document No</td>
                                                                                    <td class="">8002</td>
                                                                                </tr>
                                                                                <tr class="even pointer">
                                                                                    <td class="item">Name</td>
                                                                                    <td class="">Sk Rafael</td>
                                                                                </tr>
                                                                                <tr class="even pointer">
                                                                                    <td class="item">Description</td>
                                                                                    <td class=""></td>
                                                                                </tr>
                                                                                <tr class="even pointer">
                                                                                    <td class="item">Category No</td>
                                                                                    <td class="">00215</td>
                                                                                </tr>
                                                                                <tr class="even pointer">
                                                                                    <td class="item">Customer No</td>
                                                                                    <td class="">00215263</td>
                                                                                </tr>
                                                                                <tr class="even pointer">
                                                                                    <td class="item">Submission Date</td>
                                                                                    <td class="">30/02/2011</td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div id="feesd" class="tab-pane" role="tabpanel">
                                                                <div class="academic-tab">
                                                                    <div class="edit-section">
                                                                        <h4><i aria-hidden="true" class="fa fa-info-circle"></i>Fees</h4>
                                                                    </div>
                                                                    <div class="clearfix"></div>
                                                                    <div class="table-responsive">
                                                                        <table class="table table-striped jambo_table bulk_action">
                                                                            <thead>
                                                                                <tr class="headings">
                                                                                    <th class="column-title">Enrol No
                                                                                    </th>
                                                                                    <th class="column-title">Course Code 
                                                                                    </th>
                                                                                    <th class="column-title">Course Name
                                                                                    </th>
                                                                                    <th class="column-title">Start Date
                                                                                    </th>
                                                                                    <th class="column-title">End Date
                                                                                    </th>
                                                                                    <th class="column-title">Status
                                                                                    </th>
                                                                                    <th class="column-title">Outstanding Amount
                                                                                    </th>
                                                                                    <th class="column-title">Action
                                                                                    </th>
                                                                                </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                                <?php
                                                                                    //display fees details
                                                                                                                                           $cond00="where `STUD_NO`=".$std_row[0]['STUD_NO'];
                                                                                                                                           $std_row00=getRows($cond00,'enrol');
                                                                                                                                           foreach($std_row00 as $std_row001)
                                                                                                                                           {
                                                                                                                                           
                                                                                                                                            ?>
                                                                                <tr class="even pointer">
                                                                                    <td class=" "><?=$std_row001['ENROL_NO']?></td>
                                                                                    <?php
                                                                                        if($std_row001['COURSE_NO']){
                                                                                        $cond004="where `COURSE_NO`=".$std_row001['COURSE_NO'];
                                                                                                                                              $std_row004=getRows($cond004,'course'); }
                                                                                        ?>
                                                                                    <td class=" "><?=$std_row004[0]['CODE']?></td>
                                                                                    <td class=" "><?=$std_row004[0]['COURSE_NAME']?></td>
                                                                                    <td class=" "><?=$std_row001['ST_DATE']?></td>
                                                                                    <td class=" "><?=$std_row001['END_DATE']?></td>
                                                                                    <?php   $mydate = $std_row001['END_DATE'];
                                                                                        $date1 = str_replace('/', '-', $mydate);//replace date format dd/mm/yy to yy-mm-dd
                                                                                                                                        $date1a=date("Y-m-d", strtotime($date1));
                                                                                            ?>
                                                                                    <td class=" "><?=(strtotime($date1a) < strtotime(date('Y-m-d')) )?'Finished':'Not Finished'?></td>
                                                                                    <?php
                                                                                        //code for display due/outstanding amount
                                                                                                      $arr=array();
                                                                                                                                                              $due_amount="where `ENROL_NO`=".$std_row001['ENROL_NO'];
                                                                                                                                                              $due_amount1=getRows($due_amount,'due_history');
                                                                                                                                                              foreach($due_amount1 as $due_amount2)
                                                                                                                                                               {
                                                                                                                                                               if($due_amount2['DELETE_DATE']!='0')
                                                                                                                                                                {
                                                                                                                                                               $due_amount3="where `DUE_NO`=".$due_amount2['DUE_NO'];
                                                                                                                                                                  $due_amount4=getRows($due_amount3,'due');
                                                                                                                                                               foreach($due_amount4 as $due_amount5)
                                                                                                                                                                    {
                                                                                                                                                              
                                                                                                                                                               $arr[]=$due_amount5['AMOUNT'];
                                                                                                                                                                 
                                                                                                                                                                $sum=array_sum($arr);
                                                                                                                                                                     }
                                                                                                                                                                
                                                                                                                                                                }
                                                                                                                                                               }
                                                                                                                                              //$std_row003=getRows($cond003,'due');
                                                                                        ?>
                                                                                    <td class=" "><?php echo $sum;?></td>
                                                                                    <td>
                                                                                        <a href="#" onclick="javascript:void window.open('dashboard.php?op=<?=MD5('enrolment_details')?>&eid=<?=$std_row001['ENROL_NO']?>&s_no=<?=$std_row[0]['EXT_STN']?>&s_fname=<?=$std_row[0]['FNAME']?>&s_lname=<?=$std_row[0]['LNAME']?>&s_mname=<?=$std_row6[0]['MNAME']?>&ag_name=<?=$std_row[0]['AGENT_NAME']?>','1470138621367','width=1400,height=900,toolbar=0,menubar=0,location=1,status=1,scrollbars=1,resizable=1,left=0,top=0');return false;">Edit</a><!--|<a href="dashboard.php?op=<?=MD5('student_details')?>&del2=<?=$std_row001['ENROL_NO']?>&sid=<?=$std_row[0]['STUD_NO']?>" class="color-sky stting"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>Delete</a>-->
                                                                                    </td>
                                                                                </tr>
                                                                                <?php
                                                                                    }
                                                                                     ?>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div id="diaryd" class="tab-pane" role="tabpanel">
                                                                <div class="academic-tab">
                                                                    <div class="edit-section">
                                                                        <h4><i aria-hidden="true" class="fa fa-info-circle"></i>Diary</h4>
                                                                    </div>
                                                                    <div class="clearfix"></div>
                                                                    <div class="table-responsive">
                                                                        <table class="table table-striped jambo_table bulk_action">
                                                                            <thead>
                                                                                <tr class="headings">
                                                                                    <th class="column-title">Attention
                                                                                    </th>
                                                                                    <th class="column-title">Date 
                                                                                    </th>
                                                                                    <th class="column-title">Category
                                                                                    </th>
                                                                                    <th class="column-title">Subjects
                                                                                    </th>
                                                                                    <th class="column-title">User
                                                                                    </th>
                                                                                    <th class="column-title">When
                                                                                    </th>
                                                                                    <th class="column-title">Detail
                                                                                    </th>
                                                                                    <th class="column-title">Action
                                                                                    </th>
                                                                                </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                                <?php
                                                                                    //Code for display diary
                                                                                                                                           $con1="where `CUSTOMER_NO`=".$std_row[0]['CUSTOMER_NO'];
                                                                                                                                           $std_r1=getRows($con1,'diary');
                                                                                                                                           foreach($std_r1 as $std_r2)
                                                                                                                                           {
                                                                                                                                           $con2="where `LOOKUP_CODE_NO`=".$std_r2['CATEGORY_NO'];
                                                                                                                                           $std_r3=getRows($con2,'lookup_code');
                                                                                                                                           foreach($std_r3 as $std_r4)
                                                                                                                                           {
                                                                                                                                           $con3="where `USER_NO`=".$std_r2['USER_NO'];
                                                                                                                                           $std_r5=getRows($con3,'users');
                                                                                                                                           foreach($std_r5 as $std_r6)
                                                                                                                                           {
                                                                                                                                            ?>
                                                                                <tr class="even pointer">
                                                                                    <td class=" "><?=($std_r2['ATTENTION'])? '<input type="checkbox" checked disabled />':'<input disabled type="checkbox"  />' ?></td>
                                                                                    <td class=" "><?=$std_r2['DIARY_DATE']?></td>
                                                                                    <td class=" "><?=$std_r4['NAME']?></td>
                                                                                    <td class=" "><?=$std_r2['SUMMARY']?></td>
                                                                                    <td class=" "><?=$std_r6['INIT']?></td>
                                                                                    <td class=" "><?=$std_r2['WHO_DATE']?></td>
                                                                                    <td class=" "><?=$std_r2['NOTES']?></td>
                                                                                    <td><a href="#" onclick="javascript:void window.open('dashboard.php?op=<?=MD5('display_student_diary_edit')?>&d_no=<?=$std_r2['DIARY_NO']?>&s_no=<?=$std_row[0]['EXT_STN']?>&s_fname=<?=$std_row[0]['FNAME']?>&s_lname=<?=$std_row[0]['LNAME']?>&s_mname=<?=$std_row6[0]['MNAME']?>&ag_name=<?=$std_row[0]['AGENT_NAME']?>','1470138621367','width=700,height=500,toolbar=0,menubar=0,location=1,status=1,scrollbars=1,resizable=1,left=0,top=0');return false;">Edit</a>|<a href="dashboard.php?op=<?=MD5('student_details')?>&del3=<?=$std_r2['DIARY_NO']?>&sid=<?=$std_row[0]['STUD_NO']?>" class="color-sky stting"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>Delete</a></td>
                                                                                </tr>
                                                                                <?php
                                                                                    }}}
                                                                                     ?>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
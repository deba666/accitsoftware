<?php
    /*
        * @package    ACCIT
         * @author    Paperlink Softwares Team
         * @copyright    Copyright (c) 2016 , Paperlink Softwares Pvt. Ltd.  (http://www.paperlinksoftwares.com/)
         * @since    Version 1.0.0
         * @filesource
		 * Code for footer page
         */
		 ?>

<footer>
                  <div class="pull-left">
                   
                  </div>
                  <div class="pull-right">DESIGN and developed BY
                    <a href="javascript:void(0);">ACCIT</a>
                  </div>
                  <div class="clearfix"></div>
                </footer>
                <!-- /footer content -->

        
        <!-- jQuery Js Main File Do not add another -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <!-- Bootstrap -->
        <script src="./vendors/bootstrap/dist/js/bootstrap.min.js"></script>
        <!-- bootstrap-daterangepicker -->
        <script src="js/moment/moment.min.js"></script>
        <script src="js/datepicker/daterangepicker.js"></script>
        <!-- FastClick -->
        <script src="./vendors/fastclick/lib/fastclick.js"></script>
        <!-- bootstrap-progressbar -->
        <script src="./vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
        <!-- iCheck 
        <script src="./vendors/iCheck/icheck.min.js"></script>-->
        <!-- Skycons -->
        <script src="./vendors/skycons/skycons.js"></script>
        <!--NiceScroll-->
        <script src="./vendors/nicescroll/jquery.nicescroll.js"></script>
        <!-- validator -->
        <script src="./vendors/validator/validator.min.js"></script>
        <!-- Datatables -->
        <script src="js/jquery.js"></script>
        <script src="js/jquery-ui-1.8.16.custom.min.js"></script>
        <script src="js/bootstrap.js"></script>
        <script src="js/prettify.js"></script>
        <script src="js/jquery.sparkline.min.js"></script>
        <script src="js/jquery.nicescroll.min.js"></script>
        <script src="js/accordion.jquery.js"></script>
        <script src="js/smart-wizard.jquery.js"></script>
        <script src="js/vaidation.jquery.js"></script>
        <script src="js/jquery-dynamic-form.js"></script>
        <script src="js/fullcalendar.js"></script>
        <script src="js/raty.jquery.js"></script>
        <script src="js/jquery.noty.js"></script>
        <script src="js/jquery.cleditor.min.js"></script>
        <script src="js/data-table.jquery.js"></script>
        <script src="js/TableTools.min.js"></script>
        <script src="js/ColVis.min.js"></script>
        <script src="js/plupload.full.js"></script>
        <script src="js/elfinder/elfinder.min.js"></script>
        <script src="js/chosen.jquery.js"></script>
        <script src="js/uniform.jquery.js"></script>
        <script src="js/jquery.tagsinput.js"></script>
        <script src="js/jquery.colorbox-min.js"></script>
        <script src="js/check-all.jquery.js"></script>
        <script src="js/inputmask.jquery.js"></script>
        <script src="http://bp.yahooapis.com/2.4.21/browserplus-min.js"></script>
        <script src="js/plupupload/jquery.plupload.queue/jquery.plupload.queue.js"></script>
        <script src="js/excanvas.min.js"></script>
        <script src="js/jquery.jqplot.min.js"></script>
        <script src="js/chart/jqplot.highlighter.min.js"></script>
        <script src="js/chart/jqplot.cursor.min.js"></script>
        <script src="js/chart/jqplot.dateAxisRenderer.min.js"></script>
        <script src="js/custom-script.js"></script>
        <script src="css/jquery.easing.min.js" type="text/javascript"></script>
        
        <!-- html5.js for IE less than 9 -->
        <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <script src="js/respond.min.js"></script>
        <script src="js/ios-orientationchange-fix.js"></script>

        <!--========================Fixing Start=================================-->
        <!--Menu, Notification,Message Scrool-->
        <script src="js/jquery.mCustomScrollbar.concat.min.js"></script>

        <!-- end -->
        <script src="js/dataTables.colVis.js"></script>
        <script type="text/javascript" src="js/bootstrap-datetimepicker.min.js"></script>

	<!--<script type="text/javascript" src="js/jquery.validate.js"></script>-->
<!--Multiselect Dropdown-->
<script src="js/jquery.multiselect.js"></script>
      <!-- Custom Theme Scripts -->
        <script src="js/custom.js"></script>
      <!-- Custom Theme Scripts -->
      
			<!--Fees Popup-->
        <script type="text/javascript">
            jQuery().ready(function() {
            
              // validate form on keyup and submit
              var v = jQuery("#basicform").validate({
                rules: {
                  uname: {
                    required: true,
                    minlength: 2,
                    maxlength: 16
                  },
                  uemail: {
                    required: true,
                    minlength: 2,
                    email: true,
                    maxlength: 100,
                  },
                  upass1: {
                    required: true,
                    minlength: 6,
                    maxlength: 15,
                  },
                  upass2: {
                    required: true,
                    minlength: 6,
                    equalTo: "#upass1",
                  }
            
                },
                errorElement: "span",
                errorClass: "help-inline-error",
              });
            
              $(".open1").click(function() {
                if (v.form()) {
                  $(".frm").hide("fast");
                  $("#sf2").show("slow");
                }
              });
            
              $(".open2").click(function() {
                if (v.form()) {
                  $(".frm").hide("fast");
                  $("#sf3").show("slow");
                }
              });
              
              $(".open3").click(function() {
                if (v.form()) {
                  $("#loader").show();
                   setTimeout(function(){
                     $("#basicform").html('<h2>Thanks for your time.</h2>');
                   }, 1000);
                  return false;
                }
              });
              
              $(".back2").click(function() {
                $(".frm").hide("fast");
                $("#sf1").show("slow");
              });
            
              $(".back3").click(function() {
                $(".frm").hide("fast");
                $("#sf2").show("slow");
              });
            
            });
        </script>
              
        
        <!-- Skycons -->
        <script>
            $(document).ready(function() {
              var icons = new Skycons({
                  "color": "#73879C"
                }),
                list = [
                  "clear-day", "clear-night", "partly-cloudy-day",
                  "partly-cloudy-night", "cloudy", "rain", "sleet", "snow", "wind",
                  "fog"
                ],
                i;
            
              for (i = list.length; i--;)
                icons.set(list[i], list[i]);
            
              icons.play();
            });
        </script>
        <!-- /Skycons -->
        <!-- validator -->
        <script>
          // initialize the validator function
          validator.message.date = 'not a real date';

          // validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
          $('form')
            .on('blur', 'input[required], input.optional, select.required', validator.checkField)
            .on('change', 'select.required', validator.checkField)
            .on('keypress', 'input[required][pattern]', validator.keypress);

          $('.multi.required').on('keyup blur', 'input', function() {
            validator.checkField.apply($(this).siblings().last()[0]);
          });

          $('form').submit(function(e) {
            e.preventDefault();
            var submit = true;

            // evaluate the form using generic validaing
            if (!validator.checkAll($(this))) {
              submit = false;
            }

            if (submit)
              this.submit();

            return false;
          });
        </script>
        <!-- /validator -->
        <!-- bootstrap-daterangepicker
        <script>
          $(document).ready(function() {
            $('#birthday').daterangepicker({
              singleDatePicker: true,
              calender_style: "picker_4",
			   altFormat: "yyyy-mm-dd",
			   altField : "#birthday"
            }, function(start, end, label) {
              console.log(start.toISOString(), end.toISOString(), label);
            });
          });
        </script> -->
    
        <script>
          $( document ).ready(function() {
              $('#editBox').on('hidden.bs.modal', function () {
                    $(this).removeData('bs.modal');
              });
          });
        </script>

<script>
//Quantity Of Price

	$('.value-plus').on('click', function(){
      var divUpd = $(this).parent().find('.value'), newVal = parseInt(divUpd.text(), 10)+1;
      divUpd.text(newVal);
    });
    
    $('.value-minus').on('click', function(){
      var divUpd = $(this).parent().find('.value'), newVal = parseInt(divUpd.text(), 10)-1;
      if(newVal>=1) divUpd.text(newVal);
    });
</script>

<!--Multiselect Dropdown-->
		<script>
			$('.langOpt').multiselect({
    columns: 1,
    placeholder: 'NONE'
});
		</script>


<!--image upload -->
<script>
$(document).ready(function (e) {
	
	$(".frmUpload").on('submit',(function(e) {
		e.preventDefault();
		$(".upload-msg").text('Loading...');	
		$.ajax({
			url: "dashboard.php?op=<?=MD5('student_add')?>",        // Url to which the request is send
			type: "POST",             // Type of request to be send, called as method
			data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
			contentType: false,       // The content type used when sending data to the server.
			cache: false,             // To unable request pages to be cached
			processData:false,        // To send DOMDocument or non processed data file it is set to false
			success: function(data)   // A function to be called if request succeeds
			{
				$(".upload-msg").html(data);
			}
		});
	}
));

// Function to preview image after validation

$("#userImage").change(function() {
	$(".upload-msg").empty(); 
	var file = this.files[0];
	var imagefile = file.type;
	var imageTypes= ["image/jpeg","image/png","image/jpg"];
		if(imageTypes.indexOf(imagefile) == -1)
		{
			$(".upload-msg").html("<span class='msg-error'>Please Select A valid Image File</span><br /><span>Only jpeg, jpg and png Images type allowed</span>");
			return false;
		}
		else
		{
			var reader = new FileReader();
			reader.onload = function(e){
				$(".img-preview").html('<img src="' + e.target.result + '" />');				
			};
			reader.readAsDataURL(this.files[0]);
		}
	});	
});
</script>

<!-- Check Age Under 18 -->
    <script type="text/javascript">

        var minAge = 18;
        function _calcAge() {
            var date = new Date(document.getElementById("d").value);
            var today = new Date();

            var timeDiff = Math.abs(today.getTime() - date.getTime());
            var age1 = Math.ceil(timeDiff / (1000 * 3600 * 24)) / 365;
            return age1;
        }
        //Compares calculated age with minimum age and acts according to rules//
        function _setAge() {

            var age = _calcAge();
            //alert("my age is " + age);
            if (age < minAge) {
                alert("The student is below 18");
            } 
var age_round=Math.floor(age);
document.getElementById("agecalc").innerHTML = "Your Age: " + age_round;
if(document.getElementById("d").value==""){
alert("Please Enter A Valid Date");
}
        }

function checkCountry(val) {
          alert(val);
          return false;
            
        }
    </script>
<!-- ERROR MSG POPUP BEFORE CLOSE WINDOW 

<script language="JavaScript">
  var needToConfirm = "You have unsaved changes on this page!";

$(document).ready(function() {
    $('input:not(:button,:submit),textarea,select').change(function () {
        window.onbeforeunload = function () {
            if (needToConfirm != null) return needToConfirm;
        }
    });
    $('input:submit').click(function(e) {
        needToConfirm = null;
    });
});
</script>-->

<!-- DISABLED ENTER -->
<script type="text/javascript"> 

function stopRKey(evt) { 
  var evt = (evt) ? evt : ((event) ? event : null); 
  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null); 
  if ((evt.keyCode == 13) && (node.type=="text"))  {return false;} 
} 

document.onkeypress = stopRKey; 

</script>

<!--<script type="text/javascript">
            var datefield=document.createElement("input")
            datefield.setAttribute("type", "date")
            if (datefield.type!="date"){ //if browser doesn't support input type="date", load files for jQuery UI Date Picker
                document.write('<link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="stylesheet" type="text/css" />\n')
                document.write('<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"><\/script>\n')
                document.write('<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"><\/script>\n') 
            }
        </script>-->

<!-- Application Form -->
<script type="text/javascript">
            //jQuery time
            var current_fs, next_fs, previous_fs; //fieldsets
            var left, opacity, scale; //fieldset properties which we will animate
            var animating; //flag to prevent quick multi-click glitches
            
            $(".next").click(function(){
              if(animating) return false;
              animating = true;
              
              current_fs = $(this).parent();
              next_fs = $(this).parent().next();
              
              //activate next step on progressbar using the index of next_fs
              $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");
              
              //show the next fieldset
              next_fs.show(); 
              //hide the current fieldset with style
              current_fs.animate({opacity: 0}, {
                step: function(now, mx) {
                  //as the opacity of current_fs reduces to 0 - stored in "now"
                  //1. scale current_fs down to 80%
                  scale = 1 - (1 - now) * 0.2;
                  //2. bring next_fs from the right(50%)
                  left = (now * 50)+"%";
                  //3. increase opacity of next_fs to 1 as it moves in
                  opacity = 1 - now;
                  current_fs.css({
                    'transform': 'scale('+scale+')',
                    'position': 'absolute'
                  });
                  next_fs.css({'left': left, 'opacity': opacity});
                }, 
                duration: 800, 
                complete: function(){
                  current_fs.hide();
                  animating = false;
                }, 
                //this comes from the custom easing plugin
                easing: 'easeInOutBack'
              });
            });
            
            $(".previous").click(function(){
              if(animating) return false;
              animating = true;
              
              current_fs = $(this).parent();
              previous_fs = $(this).parent().prev();
              
              //de-activate current step on progressbar
              $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");
              
              //show the previous fieldset
              previous_fs.show(); 
              //hide the current fieldset with style
              current_fs.animate({opacity: 0}, {
                step: function(now, mx) {
                  //as the opacity of current_fs reduces to 0 - stored in "now"
                  //1. scale previous_fs from 80% to 100%
                  scale = 0.8 + (1 - now) * 0.2;
                  //2. take current_fs to the right(50%) - from 0%
                  left = ((1-now) * 50)+"%";
                  //3. increase opacity of previous_fs to 1 as it moves in
                  opacity = 1 - now;
                  current_fs.css({'left': left});
                  previous_fs.css({'transform': 'scale('+scale+')', 'opacity': opacity});
                }, 
                duration: 800, 
                complete: function(){
                  current_fs.hide();
                  animating = false;
                }, 
                //this comes from the custom easing plugin
                easing: 'easeInOutBack'
              });
            });
            
            $(".submit").click(function(){
              return false;
            })
            
        </script>

        <!--Header Script Transfer Here-->

<script type="text/javascript">
$(function() {

$('#chkveg').multiselect({

includeSelectAllOption: true

});

$('#btnget').click(function() {

alert($('#chkveg').val());

})

});
</script>
<script type="text/javascript">
    function addmsg(type, msg) {

        $('#notification_count').html(msg);

    }

    function waitForMsg() {

        $.ajax({
            type: "GET",
            url: "select.php",

            async: true,
            cache: false,
            timeout: 500000,

            success: function(data) {
                addmsg("new", data);
                setTimeout(
                    waitForMsg,
                    10000000000
                );
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                addmsg("error", textStatus + " (" + errorThrown + ")");
                setTimeout(
                    waitForMsg,
                    150000000);
            }
        });
    };

    $(document).ready(function() {

        waitForMsg();

    });
</script>
 


<script type="text/javascript">
    $(function getNotification() {



     $.ajax({
       type: "POST",
       url: "update.php", 
      
    });
    });
</script>
<script type="text/javascript">
    function addmsgacc(type, msg) {

        $('#notification_account').html(msg);

    }

    function waitForMsgAcc() {

        $.ajax({
            type: "GET",
            url: "select1.php",

            async: true,
            cache: false,
            timeout: 500000,

            success: function(data) {
                addmsgacc("new", data);
                setTimeout(
                    waitForMsgAcc,
                    10000000000
                );
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                addmsgacc("error", textStatus + " (" + errorThrown + ")");
                setTimeout(
                    waitForMsgAcc,
                    150000000);
            }
        });
    };

    $(document).ready(function() {

        waitForMsgAcc();

    });
</script>

<script type="text/javascript">
    $(function getNotificationAcc() {
     $.ajax({
       type: "POST",
       url: "update1.php", 
      
    });

    });
</script>
<script type="text/javascript">
    function addmsgacc(type, msg) {

        $('#notification_account').html(msg);

    }

    function waitForMsgAcc() {

        $.ajax({
            type: "GET",
            url: "select1.php",

            async: true,
            cache: false,
            timeout: 500000,

            success: function(data) {
                addmsgacc("new", data);
                setTimeout(
                    waitForMsgAcc,
                    10000000000
                );
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                addmsgacc("error", textStatus + " (" + errorThrown + ")");
                setTimeout(
                    waitForMsgAcc,
                    150000000);
            }
        });
    };

    $(document).ready(function() {

        waitForMsgAcc();

    });
</script>

<script type="text/javascript">
    $(function getNotificationAcc() {
     $.ajax({
       type: "POST",
       url: "update1.php", 
      
    });

    });
</script>


<script>
function checkAvailability() { 
  $("#loaderIcon").show();
  jQuery.ajax({
  url: "check_availability.php",
  data:'EXT_STN='+$("#ext_stn").val(),
  type: "POST",
  success:function(data){
    $("#user-availability-status").html(data);
    $("#loaderIcon").hide();
  },
  error:function (){}
  });
}
function showUser(str) {
    if (str == "") {
        document.getElementById("std_name").innerHTML = "";
        return;
    } else {
        if (window.XMLHttpRequest) {
            // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        } else {
            // code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                document.getElementById("std_name").innerHTML = xmlhttp.responseText;
            }
        };
        xmlhttp.open("GET","server.php?q="+str,true);
        xmlhttp.send();
    }
}
 
</script>
<!--<script type="text/javascript">
    $(document).ready( function () {
        $('#datatable').DataTable( {
            "dom": 'C<"clear">lfrtip'
        } );
    } );

    $(document).ready( function () {
      $('#datatable').DataTable( {
             "dom": 'C<"clear">lfrtip'
         } );
  } );
</script>-->
<script type="text/javascript">
   function add(type, msg) {

        $('#notification').html(msg);

    }

    function show() {

        $.ajax({
            type: "GET",
            url: "workflow_ajax.php",

            async: true,
            cache: false,
            timeout: 500000,

            success: function(data) {
                add("new", data);
                setTimeout(
                    show,
                    10000000000
                );
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                add("error", textStatus + " (" + errorThrown + ")");
                setTimeout(
                    show,
                    150000000);
            }
        });
    };

    $(document).ready(function() {

        show();

    });
</script>


<script>
function commissionUser(str) {
    if (str == "") {
        document.getElementById("txtHint").innerHTML = "";
        return;
    } else { 
        if (window.XMLHttpRequest) {
            // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        } else {
            // code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("txtHint").innerHTML = this.responseText;
            }
        };
        xmlhttp.open("GET","commission.php?q="+str,true);
        xmlhttp.send();
    }
}
</script>

<script>
$("#prev").on("click", function(){
    if($(".page.active").index() > 0)
        $(".page.active").removeClass("active").prev().addClass("active");
});
$("#next").on("click", function(){
    if($(".page.active").index() < $(".page").length-1)
        $(".page.active").removeClass("active").next().addClass("active");
});
</script>

<script type="text/javascript">   
function sync()
{
  var n1 = document.getElementById('n1');
  var n2 = document.getElementById('n2');
  n2.value = n1.value;
}
</script>

<script type="text/javascript">
$(document).ready(function(){
  $('a[data-toggle="tab"]').on('show.bs.tab', function(e) {
    localStorage.setItem('activeTab', $(e.target).attr('href'));
  });
  var activeTab = localStorage.getItem('activeTab');
  if(activeTab){
    $('#student-acc-tab a[href="' + activeTab + '"]').tab('show');
  }
});
</script>

<script type="text/javascript">
$(document).ready (function(){
            $(".alert").delay(2000).slideUp(200, function() {
    $(this).alert('close');
});
 });
</script>
<!-- checkbox enable -->
 <script type="text/javascript">
	$(document).ready(function(){
		$('.sccb').click(function(){
			if (this.checked) {
				$('.cns').removeAttr("disabled");
				$('.cns2').removeAttr("disabled");
				$('.cns4').removeAttr("disabled");
				$('.cns5').removeAttr("disabled");
			} else {
				$(".cns").attr("disabled", true);
				$(".cns2").attr("disabled", true);
				$(".cns4").attr("disabled", true);
				$(".cns5").attr("disabled", true);
			}
		});

		$('body').on('change', '.taxValueDrop', function(){
			var value = $(this).val();
			$('#TAX').val(value);
		});



	});
</script>
<!-- discount Script -->
 <script>
	function count() {
	  var num1 = document.getElementById("input1").value;
	  var num2 = document.getElementById("input2").value;
	   //var num3 = document.getElementById("input3").value;
	   //var add_gst=(num1*num3)/100;
	 
	  if(document.getElementById("operation").value==1)
	  {
	  var res=(num1-num2);
	  document.getElementById('output').value = eval(res);
	  }
	  else
	  if(document.getElementById("operation").value==2)
	  {
	  var res=((num1*num2)/100);
	  document.getElementById('output').value = eval(num1-res);
	  }
	}
</script>
    </body>
</html>
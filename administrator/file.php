<?php 
require_once("settings.php");
//Include database connection here
if($_GET['id'])
{
	$cond="where `STUD_NO`=".$_GET['id'];
	$std_row=getRows($cond,'student');
}
?>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <h4 class="modal-title"><center>Student Assignment</center></h4>
</div>
<div class="modal-body">
                <form class="form-horizontal form-label-left" action=""  method="post">
                 <input type="hidden" name="STUD_NO" value="<?=$std_row[0]['STUD_NO']?>" />
                  <input type="hidden" name="FNAME" value="<?=$std_row[0]['FNAME']?>" />
                  <input type="hidden" name="O_EMAIL" value="<?=$std_row[0]['O_EMAIL']?>" />
                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Select Staff</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                   
                      <select id="f_stuff" name="f_stuff" i class="custom-select form-control">
                        <option value=''>None - Please Select</option>
                        <option value='Jeeva'>Jeeva</option>
                        <option value='Jhon'>Jhon</option>
                        <option value='Sudha'>Sudha</option>
                        <option value='Arkadeep'>Arkadeep</option>
                        <option value='Soumik'>Soumik</option>
                      </select>
                    </div>
                  </div>
                  <div class="clearfix"></div>
                   <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Select Account Stuff</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                      <select id="standard" name="a_stuff" class="custom-select form-control">
                        <option value=''>None - Please Select</option>
                        <option value='Tamal'>Tamal</option>
                        <option value='Amit'>Amit</option>
                        <option value='Asim'>Asim</option>
                        <option value='Kunal'>Kunal</option>
                        <option value='Pravas'>Pravas</option>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Select Date</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                      <div class="row"> 
                        <div class='col-md-6'>
                          <div class="form-group">
                              <div class='input-group date' id='datetimepicker6'>
                                  <input type='text' name="to_date" class="form-control" />
                                  <span class="input-group-addon">
                                      <span class="glyphicon glyphicon-calendar"></span>
                                  </span>
                              </div>
                          </div>
                      </div>
                      <div class='col-md-6'>
                          <div class="form-group">
                              <div class='input-group date' id='datetimepicker7'>
                                  <input type='text' name="from_date" class="form-control" />
                                  <span class="input-group-addon">
                                      <span class="glyphicon glyphicon-calendar"></span>
                                  </span>
                              </div>
                          </div>
                      </div>
                      </div>
                    </div>
                  </div>
                  <div class="modal-footer">
                    <button type="submit" name="submit" id="submit" value="sub" class="btn btn-default">Submit</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
                </form>
              </div>

 <script type="text/javascript">
          $(function () {
              $('#datetimepicker6').datetimepicker();
              $('#datetimepicker7').datetimepicker({
                  useCurrent: false //Important! See issue #1075
              });
              $("#datetimepicker6").on("dp.change", function (e) {
                  $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
              });
              $("#datetimepicker7").on("dp.change", function (e) {
                  $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);
              });
          });
      </script>
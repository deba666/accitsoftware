<?php
/*
        * @package    ACCIT
         * @author    Paperlink Softwares Team
         * @copyright    Copyright (c) 2016 , Paperlink Softwares Pvt. Ltd.  (http://www.paperlinksoftwares.com/)
         * @since    Version 1.0.0
         * @filesource
		 * Code for project side bar
		 * all the sidebar menu  are define here
         */


//condition for sidebar

 if($_GET['op']==MD5('faculties') || $_GET['op']==MD5('faculties_add') || $_GET['op']==MD5('faculties_edit') 
                    || $_GET['op']==MD5('teacher') || $_GET['op']==MD5('teacher_add') ||  $_GET['op']==MD5('teacher_edit') || $_GET['op']==MD5('teacher_details')
         || $_GET['op']==MD5('holidays') || $_GET['op']==MD5('holidays_add') || $_GET['op']==MD5('holidays_edit') || $_GET['op']==MD5('holi')
         
        || $_GET['op']==MD5('room') || $_GET['op']==MD5('room_add') || $_GET['op']==MD5('room_edit') || $_GET['op']==MD5('fees')
         || $_GET['op']==MD5('course') || $_GET['op']==MD5('course_add') || $_GET['op']==MD5('course_edit')
        || $_GET['op']==MD5('courses_module') || $_GET['op']==MD5('courses_module_add') || $_GET['op']==MD5('courses_module_edit') || 
         $_GET['op']==MD5('user_add') || $_GET['op']==MD5('users'))
 {
     $set_slide_utilities = 1;
 }
 if($_GET['op']==MD5('faculties') || $_GET['op']==MD5('faculties_add') || $_GET['op']==MD5('faculties_edit'))
 {
     $state = 1;
 }
 else if($_GET['op']==MD5('teacher') || $_GET['op']==MD5('teacher_add') ||  $_GET['op']==MD5('teacher_edit') || $_GET['op']==MD5('teacher_details'))
 {
     $state = 2;
 }
  else if($_GET['op']==MD5('holidays') || $_GET['op']==MD5('holidays_add') || $_GET['op']==MD5('holidays_edit') || $_GET['op']==MD5('holi'))
 {
     $state = 3;
 }
  else if($_GET['op']==MD5('room') || $_GET['op']==MD5('room_add') || $_GET['op']==MD5('room_edit'))
 {
     $state = 4;
 }
 else if($_GET['op']==MD5('course') || $_GET['op']==MD5('course_add') || $_GET['op']==MD5('course_edit'))
 {
     $state = 5;
 }
 else if($_GET['op']==MD5('courses_module') || $_GET['op']==MD5('courses_module_edit') || $_GET['op']==MD5('courses_module_add'))
 {
     $state = 6;
 }
 else if($_GET['op']==MD5('users') || $_GET['op']==MD5('user_add'))
 {
     $state = 7;
 }
 else
 {
     $state = "";
 }

 
 
 
 
 
 if($_GET['op']==MD5('classes') || $_GET['op']==MD5('classes_add') 
                    || $_GET['op']==MD5('classes_edit')  
         || $_GET['op']==MD5('classes_language')
         || $_GET['op']==MD5('classes_module') || $_GET['op']==MD5('classes_language_details') || $_GET['op']==MD5('display_language_class_edit')
         || $_GET['op']==MD5('display_language_class') || $_GET['op']==MD5('display_language_module') || $_GET['op']==MD5('display_language_module_edit'))
 {
     $set_slide_class = 1;
 }
 if($_GET['op']==MD5('classes_language') || $_GET['op']==MD5('display_language_class_edit') || $_GET['op']==MD5('display_language_class'))
 {
     $state_class = 1;
 }
 else if($_GET['op']==MD5('classes_module') || $_GET['op']==MD5('display_language_module') || $_GET['op']==MD5('display_language_module_edit'))
 {
     $state_class = 2;
 }
 else
 {
     $state_class = "";
 }

 
  if($_GET['op']==MD5('invoices') || $_GET['op']==MD5('receipts') || $_GET['op']==MD5('outstanding'))
 {
     $set_slide_invoices = 1;
 }
 else
 {
     $set_slide_invoices = '';
 }
 
?>

<div id="sidebar-menu" class="main_menu_side hidden-print main_menu mCustomScrollbar">
    <div class="menu_section">
        <ul class="nav side-menu nicescroll">
            <li> <a href="dashboard.php"><i class="fa fa-tachometer" aria-hidden="true"></i>Dashboard</a></li>
            <li <?php if($_GET['op']==MD5('student') || $_GET['op']==MD5('student_add') || $_GET['op']==MD5('student_edit') 
                    || $_GET['op']==MD5('student_details')) { ?> class="active" <?php } ?>>
                <a><i class="fa fa-user"></i>Student<span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu" <?php if($_GET['op']==MD5('student') || $_GET['op']==MD5('student_add') || 
                        $_GET['op']==MD5('student_edit') 
                    || $_GET['op']==MD5('student_details')) { ?> style="display:block;" <?php } ?>>
                    <li><a href="dashboard.php?op=<?=MD5('student')?>">All Student</a></li>
                    <li><a href="dashboard.php?op=<?=MD5('student_add')?>">Add Student</a></li>
                    
                </ul>
            </li>
            <li <?php if($_GET['op']==MD5('offer_all') || $_GET['op']==MD5('student_2') || $_GET['op']==MD5('offer_edit') 
                    || $_GET['op']==MD5('offer_details')) { ?> class="active" <?php } ?>>
                <a><i class="fa fa-spinner"></i>Offer<span class="fa fa-chevron-down"></span></a>
                <ul <?php if($_GET['op']==MD5('offer_all') || $_GET['op']==MD5('student_2') || 
                        $_GET['op']==MD5('offer_edit') 
                    || $_GET['op']==MD5('offer_details')) { ?> style="display:block;" <?php } ?>
                    class="nav child_menu">
                    <li><a href="dashboard.php?op=<?=MD5('offer_all')?>">All Offer</a></li>
                    <li><a href="dashboard.php?op=<?=MD5('student_2')?>&stu_no=<?=$std_row[0]['STUD_NO']?>&sname=<?=$std_row[0]['LNAME']?>&cust_no=<?=$std_row[0]['CUSTOMER_NO']?>&cun_no=<?=$std_row[0]['COUNTRY_NO']?>">Add Offer</a></li>
                    
                </ul>
            </li>
            <li>
                <a><i class="fa fa-graduation-cap"></i>Enrollment<span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li><a href="dashboard.php?op=<?=MD5('enroll_language')?>">Language Enrollment</a></li>
                    <li><a href="dashboard.php?op=<?=MD5('enroll_module')?>">Module Enrollment</a></li>
                    
                </ul>
            </li>
            
            
            
            
            
            
            
            
            
            
            
            
             <li <?php if($set_slide_class == 1) { ?> class="active" <?php } ?>>
                <a><i class="fa fa-book"></i>Classes<span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu" <?php if($set_slide_class == 1) { ?> style="display: block;" <?php } ?>>
                    <li>
                        <a href="javascript:void(0);">Language Classes<span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu" <?php if($state_class == 1) { ?> style="display: block;" <?php } ?>>
                            <li><a href="dashboard.php?op=<?=MD5('classes_language')?>">All Classes</a></li>
                            <li><a href="dashboard.php?op=<?=MD5('display_language_class')?>">Add Class</a></li>
                            
                        </ul>
                    </li>
                      <li>
                        <a href="javascript:void(0);">Module Classes<span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu" <?php if($state_class == 2) { ?> style="display: block;" <?php } ?>>
                            <li><a href="dashboard.php?op=<?=MD5('classes_module')?>">All Classes</a></li>
                            <li><a href="dashboard.php?op=<?=MD5('display_language_module')?>">Add Class</a></li>
                        </ul>
                    </li>
                    
                   
                   
                  
                </ul>
            </li>

          <!-- <li>
                <a><i class="fa fa-book"></i>Classes<span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li><a href="dashboard.php?op=<?=MD5('classes_language')?>">Languages Classes</a></li>
                    <li><a href="dashboard.php?op=<?=MD5('classes_module')?>">Modules Classes</a></li>
                </ul>
            </li> -->
            
            <li <?php if($_GET['op']==MD5('agent') || $_GET['op']==MD5('agent_add') || $_GET['op']==MD5('agent_details') 
                    || $_GET['op']==MD5('agent_edit')) { ?> class="active" <?php } ?>>
                <a><i class="fa fa-book"></i>Agent<span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu" <?php if($_GET['op']==MD5('agent') || $_GET['op']==MD5('agent_add') || $_GET['op']==MD5('agent_details') 
                        || $_GET['op']==MD5('agent_edit')) { ?> style="display: block;" <?php } ?> >
                    <li><a href="dashboard.php?op=<?=MD5('agent')?>">All Agent</a></li>
                    <li><a href="dashboard.php?op=<?=MD5('agent_add')?>">Add Agent</a></li>
                </ul>
            </li>
            <li>
                <a><i class="fa fa-book"></i>Diary<span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li><a href="dashboard.php?op=<?=MD5('diary_student')?>">Student Diary</a></li>
                    <li><a href="dashboard.php?op=<?=MD5('diary_agent')?>">Agent Diary</a></li>
                   <!-- <li><a href="dashboard.php?op=<?=MD5('diary_accomodation')?>">Accomodation</a></li> -->
                </ul>
            </li>
            <li <?php if($set_slide_utilities == 1) { ?> class="active" <?php } ?>>
                <a><i class="fa fa-book"></i>Utility<span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu" <?php if($set_slide_utilities == 1) { ?> style="display: block;" <?php } ?>>
                    <li>
                        <a href="javascript:void(0);">Faculties<span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu" <?php if($state == 1) { ?> style="display: block;" <?php } ?>>
                            <li><a href="dashboard.php?op=<?=MD5('faculties')?>">All Faculties</a></li>
                            <li><a href="dashboard.php?op=<?=MD5('faculties_add')?>">Add Faculties</a></li>
                            <!--<li><a href="#">Student Account</a>-->
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:void(0);">Courses<span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu" <?php if($state == 5) { ?> style="display: block;" <?php } ?>>
                            <li><a href="dashboard.php?op=<?=MD5('course')?>">All Courses</a></li>
                            <li><a href="dashboard.php?op=<?=MD5('course_add')?>">Add Course</a></li>
                            <!--<li><a href="#">Student Account</a>-->
                        </ul>
                    </li>
                     <li>
                        <a href="javascript:void(0);">Modules<span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu" <?php if($state == 6) { ?> style="display: block;" <?php } ?>>
                            <li><a href="dashboard.php?op=<?=MD5('courses_module')?>">All Modules</a></li>
                            <li><a href="dashboard.php?op=<?=MD5('courses_module_add')?>">Add Module</a></li>
                            <!--<li><a href="#">Student Account</a>-->
                        </ul>
                    </li>
                      <li>
                        <a href="javascript:void(0);">Teachers<span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu" <?php if($state == 2) { ?> style="display: block;" <?php } ?>>
                            <li><a href="dashboard.php?op=<?=MD5('teacher')?>">All Teachers</a></li>
                            <li><a href="dashboard.php?op=<?=MD5('teacher_add')?>"> Add Teacher</a></li>
                        </ul>
                    </li>
                     <li>
                        <a href="javascript:void(0);">Holidays<span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu" <?php if($state == 3) { ?> style="display: block;" <?php } ?>>
                            <li><a href="dashboard.php?op=<?=MD5('holidays')?>">All Holidays</a></li>
                            <li><a href="dashboard.php?op=<?=MD5('holidays_add')?>"> Add Holiday</a></li>
                        </ul>
                    </li>
                     <li>
                        <a href="javascript:void(0);">Users<span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu" <?php if($state == 7) { ?> style="display: block;" <?php } ?>>
                            <li><a href="dashboard.php?op=<?=MD5('users')?>">All Users</a></li>
                            <li><a href="dashboard.php?op=<?=MD5('user_add')?>">Add User</a></li>
                        </ul>
                    </li>
                   <!-- <li>
                        <a href="javascript:void(0);">Courses<span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="dashboard.php?op=<?=MD5('course')?>">All Courses</a></li>
                            <li><a href="dashboard.php?op=<?=MD5('course_add')?>">Add Courses</a></li>
                            <!--<li><a href="#">Student Account</a>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:void(0);">Module Courses<span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="dashboard.php?op=<?=MD5('courses_module')?>">All Module Courses</a></li>
                            <li><a href="dashboard.php?op=<?=MD5('courses_module_add')?>"> Add Module Courses</a></li>
                        </ul>
                    </li> -->
                  
                    <li>
                        <a href="javascript:void(0);">Rooms<span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu" <?php if($state == 4) { ?> style="display: block;" <?php } ?>>
                            <li><a href="dashboard.php?op=<?=MD5('room')?>">All Rooms</a></li>
                            <li><a href="dashboard.php?op=<?=MD5('room_add')?>"> Add Room</a></li>
                        </ul>
                    </li>
                   <!-- <li><a href="dashboard.php?op=<?=MD5('users')?>">Users</a></li> -->
                    <li><a href="dashboard.php?op=<?=MD5('fees')?>">Fees</a></li>
                    <!-- <li><a target="_blank" href="dashboard.php?op=<?=MD5('config')?>">System Configaration</a></li> -->
                </ul>
            </li>

            <li>
                <a><i class="fa fa-book"></i>Report<span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li><a href="dashboard.php?op=<?=MD5('stud_lang_report_pop')?>">Student Report</a></li>
                    <li><a href="dashboard.php?op=<?=MD5('agent_report_pop')?>">Agent Report</a></li>
                    <li><a href="dashboard.php?op=<?=MD5('avetmiss_report')?>">Avetmiss</a></li>
                    <li><a href="dashboard.php?op=<?=MD5('avetmiss_esoc_report')?>">ESOS ARC Report</a></li>
</ul>
            </li>
            
            <li <?php if($set_slide_invoices==1) { ?> class="active" <?php } ?>>
                <a><i class="fa fa-book"></i>Finance<span class="fa fa-chevron-down"></span></a>
               <ul class="nav child_menu">
                            <li><a href="dashboard.php?op=<?=MD5('invoices')?>">All Invoices</a></li>
                            <li><a href="dashboard.php?op=<?=MD5('receipts')?>">All Receipts</a></li>
                            <li><a href="dashboard.php?op=<?=MD5('outstanding')?>">All Outstanding</a></li>
                            <li><a href="dashboard.php?op=<?=MD5('')?>">All Transactions</a></li>
                            
                        </ul>
            </li>
           
            <?php /*?>
            <li><a href="#"><i class="fa fa-book" aria-hidden="true"></i>Account <span class="fa fa-chevron-down"></span></a></li>
            <?php */?>
        </ul>
    </div>
</div>
<div class="right_col" role="main">
                  <div class="">
                    <div class="page-title">
                      <div class="title_left">
                        <h3>System Configarations</h3>
                      </div>
                    </div>

                    <div class="clearfix"></div>

                    <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bhoechie-tab-container">
                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 bhoechie-tab-menu">
                                      <div class="list-group">
                                        <a href="#" class="list-group-item active text-center">
                                          <h4 class="glyphicon glyphicon-plane"></h4><br/>General
                                        </a>
                                        <a href="#" class="list-group-item text-center">
                                          <h4 class="glyphicon glyphicon-road"></h4><br/>Train
                                        </a>
                                        <a href="#" class="list-group-item text-center">
                                          <h4 class="glyphicon glyphicon-home"></h4><br/>Hotel
                                        </a>
                                        <a href="#" class="list-group-item text-center">
                                          <h4 class="glyphicon glyphicon-cutlery"></h4><br/>Restaurant
                                        </a>
                                        <a href="#" class="list-group-item text-center">
                                          <h4 class="glyphicon glyphicon-credit-card"></h4><br/>Credit Card
                                        </a>
                                        <a href="#" class="list-group-item text-center">
                                          <h4 class="glyphicon glyphicon-road"></h4><br/>Train
                                        </a>
                                        <a href="#" class="list-group-item text-center">
                                          <h4 class="glyphicon glyphicon-home"></h4><br/>Hotel
                                        </a>
                                        <a href="#" class="list-group-item text-center">
                                          <h4 class="glyphicon glyphicon-cutlery"></h4><br/>Restaurant
                                        </a>
                                        <a href="#" class="list-group-item text-center">
                                          <h4 class="glyphicon glyphicon-credit-card"></h4><br/>Credit Card
                                        </a>
                                      </div>
                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 bhoechie-tab">
                                        <!-- General section -->
                                        <div class="bhoechie-tab-content active">
                                        <center><h3>General Details</h3></center>
                                           <form class="form-horizontal form-label-left" action="" method="post">
                                              <div class="item form-group">
                                                 <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">College Name:</label>
                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                  <div class="row">
                                                    <div class="col-md-7 col-sm-7 col-xs-12">
                                                       <input type="text" id="name" class="form-control col-md-12 col-xs-12" name="college_name" placeholder="Enter College Name" required="">
                                                    </div>
                                                  </div>
                                                </div>
                                              </div>
                                              <div class="item form-group">
                                              <input type="hidden" name="status" value="unread">
                                                 <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">College Code:</label>
                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                  <div class="row">
                                                    <div class="col-md-7 col-sm-7 col-xs-12">
                                                       <input type="text" id="name" class="form-control col-md-12 col-xs-12" name="room_name" placeholder="College code" required="">
                                                    </div>
                                                  </div>
                                                </div>
                                              </div>
                                              <div class="item form-group">
                                              <input type="hidden" name="status" value="unread">
                                                 <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Name:</label>
                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                  <div class="row">
                                                    <div class="col-md-7 col-sm-7 col-xs-12">
                                                       <input type="text" id="name" class="form-control col-md-12 col-xs-12" name="room_name" placeholder="Name" required="">
                                                    </div>
                                                  </div>
                                                </div>
                                              </div>
                                              <div class="item form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Country</label>
                                                <div class="col-md-7 col-sm-7 col-xs-12">
                                                  <div class="row">
                                                    <div class="col-md-6 col-sm-4 col-xs-12">
                                                      <select name="location" class="form-control">
                                                        <option value="1">AUSTRILIA</option>
                                                        <option value="2">INDIA</option>
                                                      </select>
                                                    </div>
                                                    
                                                  </div>
                                                  
                                                </div>
                                                
                                              </div>
                                              <div class="item form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Location</label>
                                                <div class="col-md-7 col-sm-7 col-xs-12">
                                                  <div class="row">
                                                    <div class="col-md-6 col-sm-4 col-xs-12">
                                                      <select name="location" class="form-control">
                                                        <option value="1">SYDNEY</option>
                                                        <option value="2">KOLKATA</option>
                                                      </select>
                                                    </div>
                                                    
                                                  </div>
                                                  
                                                </div>
                                                
                                              </div>                                              
                                              <div class="item form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name"></label>
                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                  <div class="row">
                                                    <div class="col-md-6 col-sm-4 col-xs-12">
                                                      <p class="pull-left">
                                                        <button class="btn btn-primary">Cancel</button>
                                                        <button type="submit" class="btn btn-success">Submit</button>
                                                      </p>
                                                    </div>
                                                  </div>
                                                </div>
                                              </div>
                                            </form>
                                           
                                           
                                        </div>
                                        <!-- train section -->
                                        <div class="bhoechie-tab-content">
                                            <center>
                                              <h1 class="glyphicon glyphicon-road" style="font-size:12em;color:#55518a"></h1>
                                              <h2 style="margin-top: 0;color:#55518a">Cooming Soon</h2>
                                              <h3 style="margin-top: 0;color:#55518a">Train Reservation</h3>
                                            </center>
                                        </div>
                            
                                        <!-- hotel search -->
                                        <div class="bhoechie-tab-content">
                                            <center>
                                              <h1 class="glyphicon glyphicon-home" style="font-size:12em;color:#55518a"></h1>
                                              <h2 style="margin-top: 0;color:#55518a">Cooming Soon</h2>
                                              <h3 style="margin-top: 0;color:#55518a">Hotel Directory</h3>
                                            </center>
                                        </div>
                                        <div class="bhoechie-tab-content">
                                            <center>
                                              <h1 class="glyphicon glyphicon-cutlery" style="font-size:12em;color:#55518a"></h1>
                                              <h2 style="margin-top: 0;color:#55518a">Cooming Soon</h2>
                                              <h3 style="margin-top: 0;color:#55518a">Restaurant Diirectory</h3>
                                            </center>
                                        </div>
                                        <div class="bhoechie-tab-content">
                                            <center>
                                              <h1 class="glyphicon glyphicon-credit-card" style="font-size:12em;color:#55518a"></h1>
                                              <h2 style="margin-top: 0;color:#55518a">Cooming Soon</h2>
                                              <h3 style="margin-top: 0;color:#55518a">Credit Card</h3>
                                            </center>
                                        </div>
                                         <!-- train section -->
                                        <div class="bhoechie-tab-content">
                                            <center>
                                              <h1 class="glyphicon glyphicon-road" style="font-size:12em;color:#55518a"></h1>
                                              <h2 style="margin-top: 0;color:#55518a">Cooming Soon</h2>
                                              <h3 style="margin-top: 0;color:#55518a">Train Reservation</h3>
                                            </center>
                                        </div>
                            
                                        <!-- hotel search -->
                                        <div class="bhoechie-tab-content">
                                            <center>
                                              <h1 class="glyphicon glyphicon-home" style="font-size:12em;color:#55518a"></h1>
                                              <h2 style="margin-top: 0;color:#55518a">Cooming Soon</h2>
                                              <h3 style="margin-top: 0;color:#55518a">Hotel Directory</h3>
                                            </center>
                                        </div>
                                        <div class="bhoechie-tab-content">
                                            <center>
                                              <h1 class="glyphicon glyphicon-cutlery" style="font-size:12em;color:#55518a"></h1>
                                              <h2 style="margin-top: 0;color:#55518a">Cooming Soon</h2>
                                              <h3 style="margin-top: 0;color:#55518a">Restaurant Diirectory</h3>
                                            </center>
                                        </div>
                                        <div class="bhoechie-tab-content">
                                            <center>
                                              <h1 class="glyphicon glyphicon-credit-card" style="font-size:12em;color:#55518a"></h1>
                                              <h2 style="margin-top: 0;color:#55518a">Cooming Soon</h2>
                                              <h3 style="margin-top: 0;color:#55518a">Credit Card</h3>
                                            </center>
                                        </div>
                                    </div>
                                </div>
                          </div>
                  </div>
                </div>
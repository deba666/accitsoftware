<?php
    /*
        * @package    ACCIT
         * @author    Paperlink Softwares Team
         * @copyright    Copyright (c) 2016 , Paperlink Softwares Pvt. Ltd.  (http://www.paperlinksoftwares.com/)
         * @since    Version 1.0.0
         * @filesource
		 * select Agents According Filter Option
         */
    ?>
<div role="main" class="right_col" style="min-height: 648px;">
    <div class="">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="page-title">
                <div class="title_left">
                    <h3>Agents Reports</h3>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="clearfix"></div>
                    <div class="x_content">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel recent-app">
                                    <div class="x_content">
                                        <div class="student-account-page view-faculty-page enroll-popup">
                                            <form action="dashboard.php?op=<?=MD5('agent_report')?>" method="POST" name="">
                                                <div class="row">
                                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                                        Agent Status Active: <input type="checkbox" value="act" name="active" checked/><br />
                                                        Agent All: <input type="checkbox" value="all" name="inactive"/>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-12 col-sm-12 col-xs-12" for="name">Enrollment Filter
                                                            </label>
                                                            <select name="status" class="form-control">
                                                                <?php $cty_row=getRows('ORDER BY `COUNTRY_NO` DESC','country');
                                                                    foreach($cty_row as $country)
                                                                                {
                                                                    ?>
                                                                <option value="<?=$country['COUNTRY_NO']?>"><?=$country['COUNTRY_NAME']?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <input type="submit" class="" name="sub" />
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
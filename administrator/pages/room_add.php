<?php
    /*
        * @package    ACCIT
         * @author    Paperlink Softwares Team
         * @copyright    Copyright (c) 2016 , Paperlink Softwares Pvt. Ltd.  (http://www.paperlinksoftwares.com/)
         * @since    Version 1.0.0
         * @filesource
		 * Code foradd new rooms into database
		 * Link comes from sidebar(utility->rooms->add rooms)
         */
    
    if($_POST['room_code'])
    	{
    	if(insert_record("insert into `room` set `ROOM_CODE`='".$_POST['room_code']."',`ROOM_DESC`='".$_POST['room_name']."',`ROOM_CAPACITY`='".$_POST['capacity']."',`LOCATION_NO`='".$_POST['location']."', `STATUS`='".$_POST['is_active']."'"))
    		$_SESSION['s_msg']="<strong>Fine!</strong> Room Added";
    		else{
    			$_SESSION['e_msg']="<strong>Oh snap!</strong> Room is not added";
    		}
    	}
    ?>
<div class="right_col" role="main">
    <div class="">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="page-title">
                <div class="title_left">
                    <?php 
                        if($_SESSION['s_msg'] || $_SESSION['e_msg'])
                        {
                        ?>
                    <div class="alert <?=($_SESSION['s_msg'])?'alert-success':'alert-error'?> fade in">
                        <button data-dismiss="alert" class="close" type="button">×</button>
                        <?=($_SESSION['s_msg'])?$_SESSION['s_msg']:$_SESSION['e_msg']?>
                    </div>
                    <?php
                        unset($_SESSION['s_msg']);
                        unset($_SESSION['e_msg']);
                        }
                        ?>
                    <h3>Add Room</h3>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="clearfix"></div>
                    <div class="x_content">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel recent-app">
                                    <div class="x_content">
                                        <div class="student-account-page edit-faculty-page">
                                            <!-- Nav tabs -->
                                            <div class="card">
                                                <div class="all-students-list add student">
                                                    <div class="add-student-section">
                                                        <form method="post" action="" class="form-horizontal form-label-left">
                                                            <div class="item form-group">
                                                                <label for="name" class="control-label col-md-3 col-sm-3 col-xs-12">Room Code:</label>
                                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                                    <div class="row">
                                                                        <div class="col-md-7 col-sm-7 col-xs-12">
                                                                            <input type="text" required="" placeholder="Room Code" name="room_code" class="form-control col-md-12 col-xs-12" id="name" required>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="item form-group">
                                                                <input type="hidden" value="unread" name="status">
                                                                <label for="name" class="control-label col-md-3 col-sm-3 col-xs-12">Room Name:</label>
                                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                                    <div class="row">
                                                                        <div class="col-md-7 col-sm-7 col-xs-12">
                                                                            <input type="text" required="" placeholder="Room Name" name="room_name" class="form-control col-md-12 col-xs-12" id="name" required>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="item form-group">
                                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Capacity</label>
                                                                <div class="col-md-4 col-sm-6 col-xs-6">
                                                                    <div class="row">
                                                                        <div class="col-md-4 col-sm-4 col-xs-12">
                                                                            <input type="number" step="1" min="1" max="" name="capacity" id="capacity" value="1" title="capacity" class="form-control input-text qty text" size="4">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                               
                                                            </div>
                                                            
                                                            
                                                             <div class="item form-group">
                                                                 
                                                                 
                                                                 <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Status</label>
                                                                <div class="col-md-4 col-sm-6 col-xs-6">
                                                                    <div class="row">
                                                                        <div class="col-md-4 col-sm-4 col-xs-12">
                                                                            <input type="checkbox" value="1" id="is_active" name="is_active" class="flat">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                 
                                                                 
                                                                   
                                                                </div>
                                                            
                                                            
                                                            
                                                            <div class="item form-group">
                                                                <label for="name" class="control-label col-md-3 col-sm-3 col-xs-12">Location</label>
                                                                <div class="col-md-7 col-sm-7 col-xs-12">
                                                                    <div class="row">
                                                                        <div class="col-md-6 col-sm-4 col-xs-12">
                                                                            <select class="form-control" name="location">
                                                                                <option value="1">SYDNEY</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="item form-group">
                                                                <label for="name" class="control-label col-md-3 col-sm-3 col-xs-12"></label>
                                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                                    <div class="row">
                                                                        <div class="col-md-6 col-sm-4 col-xs-12">
                                                                            <p class="pull-left"><br/>
                                                                                <button class="btn btn-primary" type="submit">Cancel</button>
                                                                                <button class="btn btn-success" type="submit">Submit</button>
                                                                            </p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
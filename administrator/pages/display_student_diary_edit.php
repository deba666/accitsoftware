<?php
    /*
        * @package    ACCIT
         * @author    Paperlink Softwares Team
         * @copyright    Copyright (c) 2016 , Paperlink Softwares Pvt. Ltd.  (http://www.paperlinksoftwares.com/)
         * @since    Version 1.0.0
         * @filesource
		 * code for edit student diary
         */
       if($_POST['DIARY_DATE'])
       {
       	if(mysql_query("update `diary` set `DIARY_DATE`='".$_POST['DIARY_DATE']."',`REF_NO`='".$_GET['s_no']."',`REF_TYPE`='3',`CONTACT_TYPE_NO`='0',`ATTENTION`='".$_POST['ATTENTION']."',`PRIORITY`='',`SUMMARY`='".$_POST['SUMMARY']."',`NOTES`='".$_POST['NOTES']."',`CATEGORY_NO`='".$_POST['CATEGORY_NO']."',`PUBLISH`='0',`DOCUMENT_NO`='',`USER_NO`='".$_POST['user']."' where `DIARY_NO`=".$_GET['d_no'])){
    
    			if(mysql_query("update `workflow` set `application_type`='".$_POST['CATEGORY_NO']."',`status`='0',`stud`='',`WHO_DATE`='".$_POST['WHO_DATE']."',`USER_NO`='".$_POST['user']."'where `diary_id`=".$_GET['d_no']))
    
       			$_SESSION['s_msg']="<strong>Fine! </strong>Student Diary Updated Successfully";
                        ?>
<script>
           
    
        window.opener.location.reload();
        self.close();
   
     </script>
<?php
    	}
       		else{
       			$_SESSION['e_msg']="<strong>Oh snap! </strong>Diary is not Update";
       }
       }
     
       if($_GET['d_no'])
    {
    	$cond="where `DIARY_NO`=".$_GET['d_no'];
    	$std_row=getRows($cond,'diary');
    }
       ?>
<script language="javascript" type="text/javascript" src="datetimepicker.js"></script>

<div class="col-md-12 col-sm-12 col-xs-12">
    <div class="page-title">
        <div class="title_left">
            <?php 
                if($_SESSION['s_msg'] || $_SESSION['e_msg'])
                {
                ?>
            <div class="alert <?=($_SESSION['s_msg'])?'alert-success':'alert-error'?> fade in">
                <button data-dismiss="alert" class="close" type="button">�</button>
                <?=($_SESSION['s_msg'])?$_SESSION['s_msg']:$_SESSION['e_msg']?>
            </div>
            <?php
                unset($_SESSION['s_msg']);
                unset($_SESSION['e_msg']);
                }
                ?>
            <h3>Edit Diary - Student</h3>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="clearfix"></div>
            <div class="x_content">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel recent-app">
                            <div class="x_content">
                                <div class="student-account-page edit-faculty-page">
                                    <!-- Nav tabs -->
                                    <div class="card">
                                        <div class="all-students-list add student">
                                            <div class="add-student-section">
                                                <form method="post" action="" class="form-horizontal form-label-left">
                                                    <div class="item form-group">
                                                        <?php date_default_timezone_set('Australia/Melbourne');
                                                            $date = date('d/m/Y h:i:s a', time());
                                                            ?>
                                                        <input id="who"  name="WHO_DATE"  type="hidden" value="<?php echo $date?>">
                                                        <input id="when"  name="user" type="hidden" value="<?php echo $_SESSION['user_no']?>">
                                                        <label for="name" class="control-label col-md-2 col-sm-2 col-xs-2">Student:</label>
                                                        <div class="col-md-8 col-sm-8 col-xs-8">
                                                            <div class="row">
                                                                <div class="col-md-7 col-sm-7 col-xs-12">
                                                                    <input required="" name="" value="<?=$_GET['s_no']?>, <?=$_GET['s_lname']?> <?=$_GET['s_mname']?> <?=$_GET['s_fname']?>" class="form-control col-md-12 col-xs-12" type="text" disabled>
                                                                    <input  value="<?=$_GET['ag_no']?>" class="form-control col-md-12 col-xs-12" id="name" type="hidden" name="AGENT_NO">
                                                                    <input  value="<?=$_GET['cus_no']?>" class="form-control col-md-12 col-xs-12" id="name" type="hidden" name="CUSTOMER_NO">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-1 col-sm-1 col-xs-12">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-12 col-sm-12 col-xs-12" for="name">Date
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 col-sm-3 col-xs-12">
                                                            <div class="form-group">
                                                                <input class="control-label col-md-12 col-sm-12 col-xs-12" name="DIARY_DATE" value="<?php echo $std_row[0]["DIARY_DATE"]?>" type="date">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-1 col-sm-1 col-xs-12">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-12 col-sm-12 col-xs-12" for="name">Time
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 col-sm-3 col-xs-12">
                                                            <div class="form-group">
                                                                <?php date_default_timezone_set('Australia/Melbourne');
                                                                    $date1 = date('h:i:s a', time());
                                                                    ?>
                                                                <input class="control-label col-md-12 col-sm-12 col-xs-12" Value="<?php echo $date1;?>" type="text" readonly>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-1 col-sm-1 col-xs-12">
                                                            <div class="form-group">
                                                                <input name="ATTENTION" type="hidden" value="0">
                                                                <input class="control-label col-md-12 col-sm-12 col-xs-12" value="1" <?php echo ($std_row[0]['ATTENTION'] ? 'checked' : '');?> name="ATTENTION" type="checkbox" style="zoom:1.5">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 col-sm-3 col-xs-12">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-12 col-sm-12 col-xs-12" for="name">Attention
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="item form-group">
                                                        <label for="name" class="control-label col-md-2 col-sm-2 col-xs-2">Category</label>
                                                        <div class="col-md-8 col-sm-8 col-xs-8">
                                                            <div class="row">
                                                                <div class="col-md-7 col-sm-7 col-xs-12">
                                                                    <select name="CATEGORY_NO" class="form-control">
                                                                    <?php
                                                                        $sql="select * from lookup_code where LOOKUP_TYPE_NO=3 ORDER BY NAME ASC";
                                                                        $dd_res=mysql_query("Select *  from lookup_code where LOOKUP_TYPE_NO=3 ORDER BY NAME ASC");
                                                                                                                   while($r=mysql_fetch_array($dd_res))
                                                                                                                      { 
                                                                                                                       if($std_row[0]['CATEGORY_NO']==$r['LOOKUP_CODE_NO'])
                                                                                                                         {
                                                                                                                           echo "<option value='$r[LOOKUP_CODE_NO]' selected> $r[NAME] </option>";
                                                                                                                           }
                                                                                                                            else
                                                                                                                            {
                                                                                                                             echo "<option value='$r[LOOKUP_CODE_NO]'> $r[NAME] </option>";
                                                                                                                              }
                                                                                                                        }
                                                                        ?>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="item form-group">
                                                        <label for="name" class="control-label col-md-2 col-sm-2 col-xs-2">Subject</label>
                                                        <div class="col-md-8 col-sm-8 col-xs-8">
                                                            <div class="row">
                                                                <div class="col-md-7 col-sm-7 col-xs-12">
                                                                    <input required="" placeholder="Subject" name="SUMMARY" value="<?=$std_row[0]['SUMMARY']?>" class="form-control col-md-12 col-xs-12" id="name" type="text">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="item form-group">
                                                        <label for="name" class="control-label col-md-2 col-sm-2 col-xs-2">Notes</label>
                                                        <div class="col-md-8 col-sm-8 col-xs-8">
                                                            <div class="row">
                                                                <div class="col-md-7 col-sm-7 col-xs-12">
                                                                    <textarea style="margin: 0px; width: 509px; height: 111px;" placeholder="Notes" name="NOTES" class="form-control col-md-12 col-xs-12" id="name"><?=$std_row[0]['NOTES']?></textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="item form-group">
                                                        <label for="name" class="control-label col-md-3 col-sm-3 col-xs-12"></label>
                                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                                            <div class="row">
                                                                <div class="col-md-6 col-sm-4 col-xs-12">
                                                                    <p class="pull-left">
                                                                        <button class="btn btn-primary" type="submit">Cancel</button>
                                                                        <button class="btn btn-success" type="submit" onclick="needToConfirm = false;">Submit</button>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
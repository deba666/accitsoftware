<?php
    /*
        * @package    ACCIT
         * @author    Paperlink Softwares Team
         * @copyright    Copyright (c) 2016 , Paperlink Softwares Pvt. Ltd.  (http://www.paperlinksoftwares.com/)
         * @since    Version 1.0.0
         * @filesource
		 * Code for add leave(holidays) for student Either Extend Or Not Extend . Links comes from enrol details(Leave Field)
         */
?>

<div class="col-md-12 col-sm-12 col-xs-12">
    <div class="page-title">
        <div class="title_left">
            <?php 
                if($_SESSION['s_msg'] || $_SESSION['e_msg'])
                {
                ?>
            <div class="alert <?=($_SESSION['s_msg'])?'alert-success':'alert-error'?> fade in">
                <button data-dismiss="alert" class="close" type="button">�</button>
                <?=($_SESSION['s_msg'])?$_SESSION['s_msg']:$_SESSION['e_msg']?>
            </div>
            <?php
                unset($_SESSION['s_msg']);
                unset($_SESSION['e_msg']);
                }
                ?>
            <h3>ESOS ARC Report</h3>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="clearfix"></div>
            <div class="x_content">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel recent-app">
                            <div class="x_content">
                                <div class="student-account-page edit-faculty-page">
                                    <!-- Nav tabs -->
                                    <div class="card">
                                        <div class="all-students-list add student">
                                            <div class="add-student-section">
                                                <form method="post" target="_blank" action="avetmiss_esoc_report_page.php" class="form-horizontal form-label-left">
                                                    <div class="item form-group">
                                                        <?php date_default_timezone_set('Australia/Melbourne');
                                                            $date = date('d/m/Y h:i:s a', time());
                                                            ?>
                                                        <input id="who"  name="WHO_DATE"  type="hidden" value="<?php echo $date?>">
                                                        <input id="when"  name="user" type="hidden" value="<?php echo $_SESSION['user_no']?>">
                                                   <div class="item form-group">
                                                        <label for="name" class="control-label col-md-2 col-sm-2 col-xs-2">Year Of</label>
                                                        <div class="col-md-8 col-sm-8 col-xs-8">
                                                            <div class="row">
                                                                <div class="col-md-7 col-sm-7 col-xs-12">
                                                                 <input type="number" name="FROM_DATE" value="<?php echo date("Y");?>" min="1970" max="<?php echo date("Y");?>" class="form-control col-md-12 col-xs-12">
                                                            </div>
                                                        </div>
													</div>
                                                   </div>
                                                      <div class="item form-group">
                                                        <label for="name" class="control-label col-md-2 col-sm-2 col-xs-2">Please select the applicable Student Visa type :</label>
                                                        <div class="col-md-8 col-sm-8 col-xs-8">
                                                            <div class="row">
                                                                <div class="col-md-7 col-sm-7 col-xs-12">
                                                                <select id="location" name="TYPE_NO" class="form-control" >
																	<?php
																		$sql2="select * from lookup_code where LOOKUP_TYPE_NO=".'30';
																		$dd_res2=mysql_query("select * from lookup_code where LOOKUP_TYPE_NO=".'30');
																		   while($r2=mysql_fetch_array($dd_res2))
																			  { 
																			   if($std_row[0]['TYPE_NO']==$r2['LOOKUP_CODE_NO'])
																				 {
																				   echo "<option value='$r2[LOOKUP_CODE_NO]' selected> $r2[NAME] </option>";
																				   }
																					else
																					{
																					 echo "<option value='$r2[LOOKUP_CODE_NO]'> $r2[NAME] </option>";
																					  }
																				}
																		?>
                                                                 </select>
                                                            </div>
                                                        </div>
													</div>
                                                   </div>
												   <div class="item form-group">
                                                        <label for="name" class="control-label col-md-2 col-sm-2 col-xs-2">Location</label>
                                                        <div class="col-md-8 col-sm-8 col-xs-8">
                                                            <div class="row">
                                                                <div class="col-md-7 col-sm-7 col-xs-12">
                                                                <select id="location" name="LOCATION_NO" class="form-control" >
																<option value='1'>Sydney</option>
																<option value='2'>All</option>
																<select>
                                                            </div>
                                                        </div>
													</div>
                                                   </div>
												   <div class="item form-group">
                                                        <label for="name" class="control-label col-md-2 col-sm-2 col-xs-2">Faculty</label>
                                                        <div class="col-md-8 col-sm-8 col-xs-8">
                                                            <div class="row">
                                                                <div class="col-md-7 col-sm-7 col-xs-12">
                                                                <select id="location" name="FACULTY_TYPE" class="form-control" >
																	<?php
																		$dd_res2=mysql_query("select * from crse_type where STATUS=0");
																		   while($r2=mysql_fetch_array($dd_res2))
																			  { 
																			  ?>
																			  <option value="<?= $r2['CRT_NO']?>"><?= $r2['CRT_NAME']?></option>
																			  <?php
																				}
																		?>
																		<option value="0">All</option>
                                                                 </select>
                                                            </div>
                                                        </div>
													</div>
                                                   </div>  
                                                    <div class="row">
                                                        <div class="item form-group">
                                                            <label for="name" class="control-label col-md-3 col-sm-3 col-xs-12"></label>
                                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                                <div class="col-md-6 col-sm-4 col-xs-12">
                                                                    <p class="pull-left">
                                                                        <button class="btn btn-primary" type="submit">Cancel</button>
                                                                        <button class="btn btn-success" type="submit">Generate Report</button>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
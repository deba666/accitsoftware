<?php
    /*
        * @package    ACCIT
         * @author    Paperlink Softwares Team
         * @copyright    Copyright (c) 2016 , Paperlink Softwares Pvt. Ltd.  (http://www.paperlinksoftwares.com/)
         * @since    Version 1.0.0
         * @filesource
    * Code for add new course in offer. After create new offer when you clicked on new course (For add new offer) this code will work.
         */
    $holiday_list = array();
    $result = mysql_query("SELECT * FROM student WHERE STUD_NO='" . $_POST["STUD_NO"] . "'"); //Take the student details from student table 
    $row = mysql_fetch_array($result);
    $name = $row['FNAME'] ." ". $row['MNAME'] ." ". $row['LNAME'];
    if($_GET['of_no'])
    {
    $s=$_GET['of_no'];
    }
    else
    {
    $s=$_SESSION['offer_no'];
    }
    
    $AGENT_NO=$_GET["AGENT_NO"];
    
    $_SESSION['AGENT_NO']=$AGENT_NO;
    //$qq=mysql_fetch_array(mysql_query("select `TAX_CODE` from `customer` where `REF_NO`=".$_SESSION['AGENT_NO']));
    if($_POST['RATE_PERCENT'])
    {
    $commission=$_POST['RATE_PERCENT'];
    }
    else if($_POST['CUSTOM_RATE'])
    {
    $commission=$_POST['CUSTOM_RATE'];
    }
    $amount=$_GET['cost'];
    $commission_percentage=($commission*$amount)/100;
    $afte_add_tax_commission=($commission_percentage*$_POST['TAX_AMOUNT'])/100;
    $final_commission= $commission_percentage+$afte_add_tax_commission;
    $q=mysql_query("select * from holiday where CRT_NO=".$_GET['faculty']);
    $i=0;
        while($row=mysql_fetch_array($q))
        {
        if(!$row['DELETE_DATE'])
            {
                $begin = new DateTime($row['FROM_DATE']);//store holiday start date in $begin
                $end = new DateTime($row['TO_DATE']);//store holiday end date in $end
                $end = $end->add(new DateInterval('P1D'));
                $diff = $end->diff($begin)->format("%a");//calculate total difference dates in holiday from date to end date
                //echo"TOTAL DIFFERENCE DATE IS=".$diff ."<br>";
                $daterange = new DatePeriod($begin, new DateInterval('P1D'), $end); 
               
            
            foreach($daterange as $date)
                {
                $date1=$date->format("Y-m-d");//change holiday date format into dd/mm/yyyy format
                //echo "Holiday Dates Are=".$date1."</br>";
                $holiday_list[]=$date1;//store all the holidays into $holiday_list variable which are fetch from database 
             
                }
                
             }  
        }
        
if(isset($_REQUEST['start_date']) && $_REQUEST['start_date']!='' && isset($_REQUEST['length']) && $_REQUEST['length']!='')
{
   
$start_date=$_REQUEST['start_date'];
$length=$_REQUEST['length'];
$start = new DateTime($start_date);
$start_date = date_create($start->format('Y-m-d'));
$start_date= date_format($start_date, 'Y-m-d'); 
$start_date=date('Y-m-d', strtotime($start_date));

if(in_array($start_date,$holiday_list)==true)//check if the user input date comes under holiday dates or not
        {
    
           $holiday='0';
           // echo " THIS DATE MATCHED SUCCESSFULLY!!!!!!MATHED DATE IS".$start_date."<br>";
            
            ?> 
<script type="text/javascript"> 
    alert("You Can not start Course at Holidays! Please select Another date."); 
    //return false;
    window.location.href='<?php echo $_SESSION['URL_REDIRECT']; ?>';
</script>
            
   <?php   exit(); }

//if 7 days a week for a course
 $length = 28;
//if 5 days a week for a course
//$length = $length*5;
$end_date1=date('Y-m-d', strtotime($start_date.' +'.$length.'days'));
$end_date=date('Y-m-d', strtotime($end_date1.'last friday'));
//$end_date=date('Y-m-d', strtotime($end_date1));
$temp=date('Y-m-d', strtotime($end_date1));
//echo "End Date=".$end_date."</br>";
$datesa=array();
$i=0;   
    
    // Call the function
    $datesa = getDatesFromRange($start_date, $end_date);
    $diff = array_intersect($holiday_list, $datesa);
    $no_of_matching_holiday_date=count($diff);
    if($no_of_matching_holiday_date>0)
    {
        do{
            $i++;
            $end_date=date('Y-m-d', strtotime($end_date.' + 1days'));
            //echo "Holiday Dates= " . $end_date . "<br>";
            }
        while(in_array($end_date,$holiday_list));
    }

    $no_of_date=$no_of_matching_holiday_date+$i;
    $add_holiday_with_end_date=date('Y-m-d', strtotime($temp.' +'.$no_of_date.'days'));
    $lastfriday=new DateTime($add_holiday_with_end_date);
    $lastfriday->modify('friday');
    $lastfriday1=$lastfriday->format('d-m-Y');//change format of last friday from dd/mm/yyyy to dd-mm-yyyy
    $lastfriday2=$lastfriday->format('Y-m-d');
    $ending_date=date('Y-m-d', strtotime($lastfriday2.'last friday'));
    //echo "No Of holidays Matching=".$no_of_matching_holiday_date."</br>";
    //echo "No Of Holiday Found=".$i."</br>";
     //echo "No Of Total Holiday=".$no_of_date."</br>";
    //echo "End Date After Holiday=".date('Y-m-d', strtotime($lastfriday2.'friday'))."</br>";
function getDatesFromRange($start, $end, $format = 'Y-m-d') {//Get all the dates from course start date to course final end date
        $array = array();
        $interval = new DateInterval('P1D');
    
        $realEnd = new DateTime($end);
        $realEnd->add($interval);
    
        $period = new DatePeriod(new DateTime($start), $interval, $realEnd);
    
        foreach($period as $date) { 
            $array[] = $date->format($format); 
        }
    
        return $array;
    }
}  
    if($_POST['DISCOUNT']==1)
    {
    $disc_amount=$_POST['DISCOUNT_RATE'];
    }
    else if($_POST['DISCOUNT']==2)
    { 
    $disc_percent=$_POST['DISCOUNT_RATE'];
    }
    else { }
    //echo "HGb".$dd."<br>";
    //echo "HOLIDAY NO=".$holiday."<br>";
    //echo "END DATE(AFTER ADD HOLIDAY -NEXT FRIDAY)" .$lastfriday->format('d-m-Y')."<br>";
   if(isset($_POST['DESCRIPTION']))
    {
              if(insert_record("insert into `offer_item` set `OFFER_NO`='$s',`CUSTOMER_NO`='".$_GET['cust_no']."',`PRODUCT_NO`='".$_GET['PRODUCT_NO']."',`PRODUCT_TYPE_NO`='".$_GET['PRODUCT_TYPE_NO']."',`PARENT_NO`='',`REF_NO`='".$_GET['REFERENCE_NO']."',`REF_TYPE_NO`='',`DESCRIPTION`='".$_POST['DESCRIPTION']."',`FROM_DATE`='$start_date',`TO_DATE`='$ending_date',`PERIOD_LENGTH`='".$_POST['length']."',`NOTES`='".$_POST["NOTE"]."',`AGENT_COMMISSION_NO`='".$_POST['AGENT_COMMISSION_NO']."',`COMMISSION_AMOUNT`='".intval($_REQUEST['COMMISSION_AMOUNT'])."',`AMOUNT`='".$_POST['AMOUNT']."',`DISC_AMOUNT`='$disc_amount',`SUB_AMOUNT`='".$_POST['SUB_AMOUNT']."',`TAX_AMOUNT`='$afte_add_tax_commission',`TOTAL_AMOUNT`='".$_POST['TOTAL_AMOUNT']."',`CUSTOM_1`='".$_POST['CUSTOM_1']."',`CUSTOM_2`='".$_POST['CUSTOM_2']."',`CUSTOM_3`='".$_POST['CUSTOM_3']."',"
                      
                      . "`CUSTOM_4`='".$_POST['CUSTOM_4']."', `CUSTOM_5`='".$_POST['CUSTOM_5']."',"
                      . "`CREATE_DATE`='".$_POST['CREATE_DATE']."',`DELETE_DATE`='',`LOCK_NUM`='".$_GET['LOCK_NUM']."',`PROVIDER_CUSTOMER_NO`='',`PRODUCT_RATE_NO`='',`PRICE_BOOK_NO`='',`CREATE_ENTRY_FLAG`='1',`OPTION_NO`='".$_POST['OPTION_NO']."',`GROUP_NO`='".$_POST['GROUP_NO']."',`UPDATE_DATE`='',`DISPLAY_ORDER`='1',`DISC_PERCENT`='$disc_percent'"))
        //echo "###End Date=".date('Y-m-d', strtotime($lastfriday2 .'last friday'));
              {  $_SESSION['s_msg']="<strong>Well done!</strong>Course Offer Added."; 
              ?>
          <script>
           window.onunload = closeMeAndRefreshOpener;
    function closeMeAndRefreshOpener() {
        window.opener.location.reload();
        self.close();
    }
     </script>
              <?php }
                
                    
               else {
                    $_SESSION['e_msg']="<strong>Oh snap!</strong>Course Offer Not Added"; 
                   
                  
    } }

    ?>
     
<script language="javascript" type="text/javascript" src="datetimepicker.js"></script>
<div role="main" class="right_col" style="min-height: 738px;">
    <div class="">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="custom-alrt">
                <?php 
                    if($_SESSION['s_msg'] || $_SESSION['e_msg'])
                    {
                    ?>
                <div class="alert <?=($_SESSION['s_msg'])?'alert-success':'alert-error'?> fade in">
                    <button data-dismiss="alert" class="close" type="button"><i class="fa fa-times" aria-hidden="true"></i></button>
                    <?=($_SESSION['s_msg'])?$_SESSION['s_msg']:$_SESSION['e_msg']?>
                </div>
                <?php
                    unset($_SESSION['s_msg']);
                    unset($_SESSION['e_msg']);
                    }
                    ?>
            </div>
              
            <div class="page-title">
                <div class="title_left">
                    <h3>Offer Item - Course</h3>
                </div>
            </div>
        </div>
       
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="clearfix"></div>
                    <div class="x_content">
                        <form  method="POST" action="" name="datepick">
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="x_panel recent-app">
                                        <div class="x_content">
                                            <div class="student-account-page">
                                                <!-- Nav tabs -->
                                                <div class="card">
                                                    <div class="all-students-list add student">
                                                        <div class="add-student-section">
                                                           
                                                            <div class="tab-content">
                                                            
                                                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                                                        <div class="row">
                                                                            <h4>
                                                                                <fieldset>Course Details</fieldset>
                                                                            </h4>
                                                                            <div class="col-md-5 col-sm-5 col-xs-12">
                                                                                <?php date_default_timezone_set('Australia/Melbourne');
                                                                                    $date = date('d/m/Y h:i:s', time());
                                                                                    
                                                                                    ?>
                                                                                <input type="hidden" class="form-control col-md-12 col-xs-12" name="CREATE_DATE" value="<?php echo $date?>">
                                                                                <div class="form-group">
                                                                                    <label for="name" class="pull-left control-label">Code
                                                                                    </label>
                                                                                    <input type="text" value="<?=$_GET['code']?>" class="form-control col-md-12 col-xs-12" placeholder="Course code" name="COURSE_CODE" readonly required>
                                                                                </div>
                                                                            </div>
                                                                            <?php
                                                                                $p=0;
                                                                                if($s)
                                                                                {
                                                                                 $grp_field="where `OFFER_NO`=".$s;
                                                                                 $grp=getRows($grp_field,'offer_item');
                                                                                 foreach($grp as $grp1)
                                                                                 {
                                                                                 if($grp1['GROUP_NO']==null)
                                                                                 {
                                                                                  $group_no=$p;
                                                                                 }
                                                                                 else
                                                                                 {
                                                                                  $group_no=$grp1['GROUP_NO'];
                                                                                 }
                                                                                }
                                                                                }
                                                                                ?>
                                                                            <input type="hidden" value="<?php echo $group_no+1;?>" class="form-control col-md-12 col-xs-12" placeholder="Group No" name="GROUP_NO">
                                                                            <div class="col-md-5 col-sm-5 col-xs-12">
                                                                                <div class="form-group">
                                                                                    <label for="name" class="pull-left control-label">Course</label>
                                                                                    <input type="text" value="<?=$_GET['name']?>" class="form-control col-md-12 col-xs-12" placeholder="Course Name" name="COURSE_NAME" readonly required>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-2 col-sm-2 col-xs-12">
                                                                                <div class="form-group">
                                                                                    <a style="margin-top: 29px;" class="btn btn-success" href="#" onclick="javascript:void window.open('dashboard_popup.php?op=<?=MD5('display_offer_course')?>&of_no=<?=$_GET['of_no1']?>&of_no1=<?=$_GET['of_no']?>&stu_no=<?=$_GET['stu_no']?>&sname=<?=$_GET['sname']?>&cust_no=<?=$_GET['cust_no']?>&AGENT_NO=<?=$_SESSION['AGENT_NO']?>&aname=<?=$_GET['aname']?>','147123445749','width=800,height=600,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=0,top=0');return false;">Select</a>
                                                                                </div>
                                                                            </div>
                                                                            <?php
                                                                                if($_GET['faculty'])
                                                                                {
                                                                                $cond="where `CRT_NO`=".$_GET['faculty'];
                                                                                $row=getRows($cond,'crse_type');
                                                                                }
                                                                                ?>
                                                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                                                <div class="form-group">
                                                                                    <label for="name" class="pull-left control-label">Faculty
                                                                                    </label>
                                                                                    <input type="text" value="<?=$row[0]['CRT_NAME']?>" placeholder="Faculty Name" class="date-picker form-control col-md-12 col-xs-12 active" name="FACULTY" readonly>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                                                <div class="form-group">
                                                                                    <label for="name" class="pull-left control-label">Location
                                                                                    </label>
                                                                                    <input type="text" value="Sydney" class="form-control col-md-12 col-xs-12" placeholder="Location" name="LOCATION" readonly>
                                                                                </div>
                                                                            </div>
                                                                            <div>&nbsp;</div>
                                                                            <div class="col-md-3 col-sm-3 col-xs-12">
                                                                                <div class="form-group">
                                                                                    <label for="name" class="pull-left control-label">Start
                                                                                    </label>
                                                                                    <?php $date2 = date( "j-M-Y") ?>
                                                                                     <input type="date" value="" name="start_date" id="start_date" class="form-control col-md-12 col-xs-12" onchange="javascript: doCalcu(this.value,document.getElementById('length').value);" required/>
                                                                                </div>
                                                                            </div>
                                                                            <?php if($_GET['week_flag']==1){ ?>
                                                                            <div class="col-md-3 col-sm-3 col-xs-12">
                                                                                <div class="form-group">
                                                                                    <label for="name" class="pull-left control-label col-md-12 col-sm-12 col-xs-12" >Length
                                                                                    </label> 
                                                                                    <input type="number" name="length" id="length" class="form-control col-md-12 col-xs-12" onchange="doCalcu2(this.value);" value="<?=$_GET['len']?>" />
                                                                            <input type="hidden" name="OPTION_NO" class="form-control col-md-12 col-xs-12" value="1"/>
                                                                           </div>
                                                                           </div> 
                                                                           <?php } else { ?>

                                                                           <div class="col-md-3 col-sm-3 col-xs-12">
                                                                                <div class="form-group">
                                                                                    <label for="name" class="pull-left control-label col-md-12 col-sm-12 col-xs-12" >Length
                                                                                    </label> 
                                                                                    <input type="number" name="length" id="length" class="form-control col-md-12 col-xs-12" onchange="doCalcu2(this.value);" value="<?=$_GET['len']?>" onchange="javascript: doCalcu(this.value);" required readonly />
                                                                           </div>
                                                                           </div> 
                                                                           <?php }?>
                                                                              <?php   if(date('Y-m-d', strtotime($lastfriday2.'last friday'))!=date('Y-m-d', strtotime(' - 3days')))
                                                                              {
                                                                                ?>
                                                                            <div class="col-md-3 col-sm-3 col-xs-12">
                                                                                <div class="form-group">
                                                                                    <label for="name" class="pull-left control-label">Finish Date
                                                                                    </label>
                                                                                    <input name="finish_date" id="finish_date" type="date" value="<?php echo date('Y-m-d', strtotime($lastfriday2.'last friday'));?>" class="form-control col-md-12 col-xs-12" readonly>
                                                                                </div>
                                                                            </div>
                                                                               <?php 
                                                                              } else { ?>
                                                                             <div class="col-md-3 col-sm-3 col-xs-12">
                                                                                <div class="form-group">
                                                                                    <label for="name" class="pull-left control-label">Finish Date
                                                                                    </label>
                                                                                    <input name="finish_date" id="finish_date" type="date" value="" class="form-control col-md-12 col-xs-12" readonly>
                                                                                </div>
                                                                            </div>
                                                                              <?php } ?>
                                                                            
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                                                        <div class="row">
                                                                            <div class="col-md-12 col-sm-12">
                                                                                <div class="row">
                                                                                    <div>&nbsp;</div>
                                                                                    <fieldset>
                                                                                        <h4>Item Description</h4>
                                                                                    </fieldset>
                                                                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                                                                        <div class="item form-group">
                                                                                            <label class="control-label col-md-12 col-sm-12 col-xs-12">
                                                                                            </label>
                                                                                            <div class="row">
                                                                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                                                                    <input type="text"value="<?=$_GET['code']?>-<?=$_GET['name']?>, (<?=$_GET['len']?> Weeks)"  class="date-picker form-control col-md-12 col-xs-12 active" name="DESCRIPTION" >
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                                                        <div class="row">
                                                                            <div class="col-md-12 col-sm-12">
                                                                                <div class="row">
                                                                                    <fieldset>
                                                                                        <h4>Custom Field</h4>
                                                                                    </fieldset>
                                                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                                                        <div class="item form-group">
                                                                                            <input type="text"  class="date-picker form-control active" name="CUSTOM_1" >
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                                                        <div class="form-group">
                                                                                            <input type="text" class="form-control"  name="CUSTOM_2" >
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                                                        <div class="form-group">
                                                                                            <input type="text" class="form-control " name="CUSTOM_3" >
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                                                        <div class="form-group">
                                                                                            <input type="text" class="form-control " name="CUSTOM_4" >
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                                                        <div class="form-group">
                                                                                            <input type="text" class="form-control " name="CUSTOM_5" >
                                                                                        </div>
                                                                                    </div>
                                                                             
                                                                                    
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                                                        <div class="row">
                                                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                                                <div class="row">
                                                                                    <div class="col-md-12 col-sm-12">
                                                                                        <div class="row">
                                                                                            <fieldset id="group1">
                                                                                                <h4>Billing Rate</h4>
                                                                                            </fieldset>
                                                                                            <div class="row">
                                                                                                <div class="col-md-4 col-sm-4 col-xs-4">
                                                                                                    <div class="item form-group">
                                                                                                        <input id="defcost" type="radio" name="group1" style="width:20px; height:20px;" checked="checked" onclick="javascript: document.getElementById('ADD_AFTER_TAX').value=''; document.getElementById('apply_agent_commission').checked = false; 
                                                                                                            document.getElementById('def_cnsrate').disabled = true; document.getElementById('custom').disabled = true; document.getElementById('CUSTOM_RATE').disabled = true; showCost('cost',<?=$_GET['cost']?>);">Default
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="col-md-4 col-sm-4 col-xs-4">
                                                                                                    <div class="item form-group">
                                                                                                        <input type="text" name="SUB_AMOUNT" id="cost" class="form-control col-md-12 col-xs-12" value="<?=$_GET['cost']?>" readonly disabled >
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="row">
                                                                                                <div class="col-md-4 col-sm-4 col-xs-4">
                                                                                                    <div class="item form-group">
                                                                                                        <input id="alternate" type="radio" name="group1" style="width:20px; height:20px;" onclick="javascript: document.getElementById('ADD_AFTER_TAX').value=''; document.getElementById('apply_agent_commission').checked = false; 
                                                                                                            document.getElementById('def_cnsrate').disabled = true; document.getElementById('custom').disabled = true; document.getElementById('CUSTOM_RATE').disabled = true; showCost('altcost',<?=$_GET['alt_cost']?>);">Alternate
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="col-md-4 col-sm-4 col-xs-4">
                                                                                                    <div class="item form-group">
                                                                                                        <input type="text" name="ALTERNATE" id="altcost" class="form-control col-md-12 col-xs-12" readonly value="" disabled>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="row">
                                                                                                <div class="col-md-4 col-sm-4 col-xs-4">
                                                                                                    <div class="item form-group">
                                                                                                        <input type="radio"  name="group1" style="width:20px; height:20px;" id="customcost" onclick="javascript: document.getElementById('ADD_AFTER_TAX').value=''; document.getElementById('apply_agent_commission').checked = false; 
                                                                                                            document.getElementById('def_cnsrate').disabled = true; document.getElementById('custom').disabled = true; document.getElementById('CUSTOM_RATE').disabled = true; showCost('customcost','');">Custom
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="col-md-4 col-sm-4 col-xs-4">
                                                                                                    <div class="item form-group">
                                                                                                        <input type="text" id="custcost" name="CUSTOM" class="form-control col-md-12 col-xs-12" onkeyup="javascript: showCost('custcost',this.value);" disabled>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                           
                                                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                                                <div class="row">
                                                                                    <div class="col-md-12 col-sm-12">
                                                                                        <div class="row">
                                                                                            <fieldset id="group2">
                                                                                                <h4>Agent Commission</h4>
                                                                                            </fieldset>
                                                                                            <div class="row">
                                                                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                                                                    <div class="item form-group">
                                                                                                        <label for="chkPassport">
     <input class="sccb" id="apply_agent_commission" type="checkbox" value="<?=$_GET['commission'] ?>" name="apply_agent_commission" style="width:20px; height:20px;" <?php if(!isset($_GET['AGENT_NO']) || $_GET['AGENT_NO']=='' || $_GET['AGENT_NO']=='0') { ?> disabled <?php } ?> onclick="javascript: document.getElementById('APPLY_BEFORE_TAX').value = '';
     document.getElementById('AGENT_COMMISSION_NO').value = '';
     document.getElementById('COMMISSION_AMOUNT').value=''; document.getElementById('CUSTOM_RATE').disabled=true; document.getElementById('COMMISSION_AMOUNT').value=''; commissionUser(document.getElementById('custom').value);">Apply Agent Commission </label>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="row">
                                                                                                <div class="col-md-4 col-sm-4 col-xs-4">
                                                                                                    <div class="item form-group">
                                                                                                        <input onclick="document.getElementById('custom').disabled = false; document.getElementById('CUSTOM_RATE').disabled = true; document.getElementById('CUSTOM_RATE').value = '';" class="cns" disabled="disabled" type="radio" name="group2" style="width:20px; height:20px;" checked id="def_cnsrate">Default Rate
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="col-md-4 col-sm-4 col-xs-4">
                                                                                                    <div class="item form-group">
                                                                                                        <?php //echo "select * from agent_commission where AGENT_NO='".$_GET['AGENT_NO']."' AND `CRT_NO` = '".$_GET['faculty']."'"; ?>
                                                                                                        <select id="custom" class="form-control col-md-12 col-xs-12 cns2" disabled="disabled" name="RATE" onchange="javascript: commissionUser(this.value);">
                                                                                                          
                                                                                                            <?php
                                                                                                                $sq="select * from agent_commission where AGENT_NO='".$_GET['AGENT_NO']."'";
                                                                                                                if($_GET['faculty']!='')
                                                                                                                {
                                                                                                                    $sq = $sq . " AND `CRT_NO` = '".$_GET['faculty']."'";
                                                                                                                }
                                                                                                               
                                                                                                                $qe=mysql_query($sq);
                                                                                                                while($ro=mysql_fetch_array($qe))
                                                                                                                {
                                                                                                                if(!$ro['DELETE_DATE']) {
                                                                                                                ?>
                                                                                                            <option value="<?=$ro["AGENT_COMMISSION_NO"]?>" <?php if($ro['LOCK_NUM']==1) { ?>  selected <?php } ?>><?=$ro['NAME']?></option>
                                                                                                            
                                                                                                            <?php } } ?>
                                                                                                        </select>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="col-md-4 col-sm-4 col-xs-4">
                                                                                                    <div class="item form-group">
                                                                                                        <div id="txtHint" style="display:none;">
                                                                                                            <input id='cns3' type='text' name='RATE_PERCENT' class='form-control col-md-12 col-xs-12' style="width:40px!important" readonly value="" />&nbsp;&nbsp;<strong>%</strong>
                                                                                                            <input type='hidden' value='' name='AGENT_COMMISSION_NO' id="AGENT_COMMISSION_NO" class='form-control col-md-12 col-xs-12' />
                                                                                                           <input type='hidden' value='' name='COMMISSION_AMOUNT' id="COMMISSION_AMOUNT" class='form-control col-md-12 col-xs-12' />
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="row">
                                                                                                <div class="col-md-4 col-sm-4 col-xs-4">
                                                                                                    <div class="item form-group">
                                                                                                        <input onclick="document.getElementById('custom').disabled = true; document.getElementById('CUSTOM_RATE').disabled = false;" type="radio" name="group2" style="width:20px; height:20px;" class="cns4" disabled onclick="javascript: updateTotalWithAgentCommCustom(document.getElementById('CUSTOM_RATE').value);">Custom Rate
                                                                                                        
                                                                                                </div></div>
                                                                                                <div class="col-md-4 col-sm-4 col-xs-4">
                                                                                                    <div class="item form-group">
                                                                                                        <input id="CUSTOM_RATE" disabled="disabled" type="text" name="CUSTOM_RATE" class="form-control col-md-12 col-xs-12 cns5" onkeyup="javascript: updateTotalWithAgentComm(this.value,'0','0');" >%
                                                                                                    </div>
                                                                                                    <input type="button" name="calculate_commission" id="calculate_commission" value="CALCULATE" onmouseover="commissionUser(document.getElementById('custom').value);" onclick="javascript: calculateCommission();"></div>
                                                                                            </div>
                                                                                            
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                                                        <div class="row">
                                                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                                                <fieldset>
                                                                                    <h4>Notes</h4>
                                                                                </fieldset>
                                                                                <div class="form-group">
                                                                                    <textarea class="form-control" name="NOTE" cols="60" rows="4"></textarea>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                                                <fieldset>
                                                                                    <h4>Billing Amount</h4>
                                                                                </fieldset>
                                                                                <div class="row">
                                                                                    <label for="name" class="pull-left control-label col-md-4 col-sm-4 col-xs-12">Amount
                                                                                    </label>
                                                                                    <div class="col-md-8 col-sm-8 col-xs-12">
                                                                                        <div class="form-group">
                                                                                            <input type="text" name="AMOUNT" id="amount" <?php if($_GET['week_flag']==1) { ?> value="<?=$_GET['cost']*$_GET['COURSE_LEN']?>" <?php } else { ?> value="<?=$_GET['cost']?>" <?php } ?> readonly />
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="row">
                                                                                    <label for="name" class="pull-left control-label col-md-4 col-sm-4 col-xs-12">Discount
                                                                                    </label>
                                                                                    <div class="col-md-8 col-sm-8 col-xs-12">
                                                                                        <div class="row">
                                                                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                                                                <div class="item form-group">
                                                                                                    <select class="form-control col-md-3 col-sm-3 col-xs-6" id="operation"  name="DISCOUNT" onchange="javascript: countTotal(this.value);">
                                                                                                        <option value="1">$</option>
                                                                                                        <option value="2">%</option>
                                                                                                    </select>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                                                                <div class="item form-group">
                                                                                                    <input id="input2" type="text" name="DISCOUNT_RATE" class="form-control" onkeyup="javascript: countTotal(this.value) , tax();">
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>

                                                                             <!--   <div class="row">
                                                                                    <label for="name" class="pull-left control-label col-md-4 col-sm-4 col-xs-12">GST %
                                                                                    </label>
                                                                                    <div class="col-md-8 col-sm-8 col-xs-12">
                                                                                        <div class="row">
                                                                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                                                                <?php 
                                                                                                   // $tax_query=mysql_query("SELECT * FROM gst_rate");
                                                                                                   // $tax_row=mysql_fetch_array($tax_query);
                                                                                                    ?>
                                                                                                <div class="item form-group">
                                                                                                    <select class="form-control col-md-3 taxValueDrop" id="operation1"  name="TaxValue" onchange="javascript: tax(this.value);">
                                                                                                        <option value="0">Exclude GST</option>
                                                                                                        <option value="<?=$tax_row['GST_RATE']?>">Include GST</option>
                                                                                                    </select>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                                                                <div class="item form-group">
                                                                                                    <input type="text" name="TAX_AMOUNT" id="TAX" class="form-control" readonly value="0"  />
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div> -->

                                                                                <div class="row">
                                                                                    <label for="name" class="pull-left control-label col-md-4 col-sm-4 col-xs-12">Total
                                                                                        &nbsp;<input type="button" name="reload" id="reload" value="RELOAD" onclick="javascript: doReload();" style="width: 63px; height: 22px;" /> </label>
                                                                                    <div class="col-md-8 col-sm-8 col-xs-12">
                                                                                        
                                                                                        <div class="item form-group">
                                                                                            <input type="text" name="TOTAL_AMOUNT" id="total_amount" value="" readonly />
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                   <!-- <div class="col-md-12 col-sm-12 col-xs-12">
                                                                        <div class="row">
                                                                            <div class="col-md-12 col-sm-12">
                                                                                <div class="row">
                                                                                    <div class="item form-group">
                                                                                        <input type="checkbox" name="RULES" style="width:20px; height:20px;">Run Fee Rules After Closing
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div> -->
                                                               
                                                                <!--Offer-->
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                             <div class="item form-group">
                                                        <label for="name" class="control-label col-md-3 col-sm-3 col-xs-12"></label>
                                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                                            <div class="row">
                                                                <div class="col-md-6 col-sm-4 col-xs-12">
                                                                    <p class="pull-left">
                                                                        <input class="btn btn-primary" type="button" onclick="javascript: window.close();" value="Cancel">
                                                                        <input type="submit" class="btn btn-success" value="Submit">
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                            <input type="hidden" name="APPLY_BEFORE_TAX" id="APPLY_BEFORE_TAX" value="" />
                            <input type="hidden" name="RATE_AMOUNT_TYPE" id="RATE_AMOUNT_TYPE" value="" />
                            <input type="hidden" name="ADD_AFTER_TAX" id="ADD_AFTER_TAX" value="" />
                            <input type="hidden" name="ADD_BEFORE_TAX" id="ADD_BEFORE_TAX" value="" />
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function calculateCommission()
    {
        
        var valcom1 = document.getElementById('ADD_AFTER_TAX').value; 
        var valcom2 = document.getElementById('ADD_BEFORE_TAX').value;  
        if(valcom1!='') 
        { alert('The commission is '+valcom1); } 
        else 
        { alert('The commission is '+valcom2);  }
                                                                                                
    }
    function doReload()
    {
        countTotal();
        tax();
        return true;
        
    }
 
    // this function calculates the start date and finish date when the course type is weekly one 
    function doCalcu(start_date,length)
    {
    <?php $actual_link = "http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; $_SESSION['URL_REDIRECT'] = $actual_link; ?>    
    
        window.location.href='<?php echo $actual_link; ?>'+'&start_date='+start_date+'&length='+length;
       var days = document.getElementById('length').value*7;
     
      var newfinishDate = new Date(new Date(val).getTime()+(days*24*60*60*1000));
      // 01, 02, 03, ... 29, 30, 31
   var dd = (newfinishDate.getDate() < 10 ? '0' : '') + newfinishDate.getDate();
   // 01, 02, 03, ... 10, 11, 12
   var MM = ((newfinishDate.getMonth() + 1) < 10 ? '0' : '') + (newfinishDate.getMonth() + 1);
   // 1970, 1971, ... 2015, 2016, ...
   var yyyy = newfinishDate.getFullYear();

   // create the format you want
 //  newfinishDateformat = (MM + "/" + dd + "/" + yyyy);
 newfinishDateformat = ( yyyy+ "-" + MM + "-" +dd );
  //alert(newfinishDateformat);
       // alert(newfinishDateformat);
      document.getElementById('finish_date').value = newfinishDateformat;

    }
     // this function calculates the start date and finish date when the course type is weekly one 
    function doCalcu2(week)
    { 
       var val = document.getElementById('start_date').value;
       if(val=='') { alert('Please select start date!'); return false; }
     var days = week*7;
      var newfinishDate = new Date(new Date(val).getTime()+(days*24*60*60*1000));
      // 01, 02, 03, ... 29, 30, 31
   var dd = (newfinishDate.getDate() < 10 ? '0' : '') + newfinishDate.getDate();
   // 01, 02, 03, ... 10, 11, 12
   var MM = ((newfinishDate.getMonth() + 1) < 10 ? '0' : '') + (newfinishDate.getMonth() + 1);
   // 1970, 1971, ... 2015, 2016, ...
   var yyyy = newfinishDate.getFullYear();

   // create the format you want
 //  newfinishDateformat = (MM + "/" + dd + "/" + yyyy);
 newfinishDateformat = ( yyyy+ "-" + MM + "-" +dd );
  //alert(newfinishDateformat);
       // alert(newfinishDateformat);
      document.getElementById('finish_date').value = newfinishDateformat;

    }
</script>

<script>
    
    
    
    function countTotal2() {
	  var num1 = document.getElementById("amount").value;
	  var num2 = document.getElementById("input2").value;
	  //var rate = parseInt(document.getElementById("operation1").value);
	   //alert(num2);
	  //return false;
	  if(document.getElementById("operation").value==1)
	  {
	  var res=(num1-num2);
	  document.getElementById('total_amount').value = eval(res);
	  }
	  else
	  if(document.getElementById("operation").value==2)
	  {
	  var res=((num1*num2)/100);
	  
	  document.getElementById('total_amount').value = eval(num1-res);
	  }
	 
	}
	
	function tax2()
    {
        countTotal2();
	  var num1 = document.getElementById("total_amount").value;
	  var rate = parseInt(document.getElementById("operation1").value); 
	  if(document.getElementById("operation1").value)
	  {
	   var add_gst=((num1*rate)/100);
	     var result = parseInt(num1) + parseInt(add_gst);
       if (!isNaN(result)) {
           document.getElementById('total_amount').value = result;
       }
	  }
    }
    
    
    
    
    
	function countTotal() {
           
          var val4 = parseInt(document.getElementById("total_amount").value);
         // alert(val4);
	  var num1 = parseInt(document.getElementById("amount").value); 
	  var num2 = parseInt(document.getElementById("input2").value);
	  //var rate = parseInt(document.getElementById("operation1").value);
	   //alert(num2);
	  //return false;
	  if(document.getElementById("operation").value==1)
	  {
	  var res=(num1-num2); 
         
             // alert(document.getElementById("cns3").value);
	  document.getElementById('total_amount').value = parseInt(res); }
	  
         
	 if(document.getElementById("operation").value==2)
	  {
	  var res=((num1*num2)/100); 
	 
	  document.getElementById('total_amount').value = eval(num1-res); }
     
	   
          if(document.getElementById("input2").value=='' && document.getElementById("operation1").value=='0')
          {
             document.getElementById('total_amount').value = parseInt(num1); 
          }
          
        }
	
	function tax()
    { 
        
        countTotal();
	  var num1 = parseInt(document.getElementById("amount").value); 
	  var rate = parseInt(document.getElementById("operation1").value);
          
          var disc = parseInt(document.getElementById("input2").value);
	  if(document.getElementById("operation1").value!=0)
	  {
              if(disc>0) {
                  if(document.getElementById("operation").value==1) 
                  {
	   var add_gst=eval(((num1*rate)/100)); 
           var add_gst_disc = eval(((disc*rate)/100)); 
	     var result = parseInt(add_gst) - parseInt(add_gst_disc); 
             var new_num = parseInt(num1) - parseInt(disc); 
      
           document.getElementById('total_amount').value = parseInt(new_num) + parseInt(result); }
   else {
        var add_gst=eval(((num1*rate)/100)); 
           var add_gst_disc = eval(((num1*disc)/100)); 
           var add_gst_disc_update = eval(((add_gst_disc*rate)/100)); 
	     var result = parseInt(add_gst) - parseInt(add_gst_disc); 
             var new_num = parseInt(num1) - parseInt(add_gst_disc_update); 
             
      
           document.getElementById('total_amount').value = parseInt(new_num) + parseInt(result);
   }
       
   } else  {  var add_gst=eval(((num1*rate)/100)); 
       document.getElementById('total_amount').value = parseInt(num1) + parseInt(add_gst); }
           //alert(document.getElementById('total_amount').value);
       
	  }
          
    }
	
	function edit_count() {
		
	  var num1 = document.getElementById("input1").value;
	  var num3 = document.getElementById("input2").value;
	  var num4 = document.getElementById("operation").value;
	  
	  //alert(num1);
	 // return false;
	 // var rate = parseInt(document.getElementById("operation1").value);
	
	  if(document.getElementById("operation").value==1)
	  {
	  var res=(num1-num3);
	  document.getElementById('operation').value = eval(res);
	  
	  }
	  else
	  if(document.getElementById("operation").value==2)
	  {
	  var res=((num1*num3)/100);
	 // alert(res);
	  // return false;
	  document.getElementById('output').value = eval(num1-res);
	  }
	 
	}
	
	function edit_tax()
    {
	  var num1 = document.getElementById("input1").value;
	  var rate = parseInt(document.getElementById("operation4").value); 
	  if(document.getElementById("operation4").value)
	  {
	   var add_gst=((num1*rate)/100);
	     var result = parseInt(num1) + parseInt(add_gst);
       if (!isNaN(result)) {
           document.getElementById('output').value = result;
       }
	  }
    }
	
	
	function showCost(id , val)
        {
            
            //document.datepick.reset();
          //  document.getElementById('apply_agent_commission').checked=false;
            if(id=="customcost")
            {
                document.getElementById('custcost').disabled=false;
                document.getElementById('custcost').value='';
            }
            
            //document.getElementById('custcost').value='';
            
            if(val!='')
            {
            document.getElementById(id).value = val;
            <?php if($_GET['week_flag']==1) { ?>
            document.getElementById('amount').value = val*<?=$_GET['COURSE_LEN']?>;
            <?php } else { ?>
                document.getElementById('amount').value = val;
            <?php } ?>
            countTotal();
            tax();
            }
        else
        {
            document.getElementById('amount').value = '';
            document.getElementById('total_amount').value = '';
        }
        
        }
        
        function updateAmount(val)
        {
           
            //document.getElementById('amount').value = val;
            document.getElementById('amount').value = parseInt(val);
            countTotal();
            tax();
            
        }
	function updateTotalWithAgentComm(val2,type,amount_type)
        {
            
            
            if(document.getElementById('defcost').checked==true)
            {
                showCost('cost',document.getElementById('cost').value);
            }
            if(document.getElementById('alternate').checked==true)
            {
                showCost('altcost',document.getElementById('altcost').value);
            }
            if(document.getElementById('customcost').checked==true)
            { 
                showCost('custcost',document.getElementById('custcost').value);
            }
            
            
            
            var val1 = parseInt(document.getElementById('amount').value);
            var val3 = parseInt(document.getElementById('total_amount').value);
           if(document.getElementById('apply_agent_commission').checked==true) {
            
            if(amount_type==1)
            {
                if(type==1)
                {
                    var newVal = eval((val2/100)*val1);
                }
                else
                {
                    var newVal = eval((val2/100)*val3);
                }
            }
            else
            {   
                var newVal = parseInt(val2);
            }
            
            if(type==1)
            {
                
                document.getElementById('ADD_AFTER_TAX').value = '';
                document.getElementById('ADD_BEFORE_TAX').value = newVal;
                
            }
            else
            {
                
                document.getElementById('ADD_AFTER_TAX').value = newVal;
                document.getElementById('ADD_BEFORE_TAX').value = '';
               
                
            }
            
        }
        else 
        {
            if(document.getElementById('defcost').checked==true)
            {
                showCost('cost',document.getElementById('cost').value);
            }
            if(document.getElementById('alternate').checked==true)
            {
                showCost('altcost',document.getElementById('altcost').value);
            }
            if(document.getElementById('customcost').checked==true)
            { 
                showCost('custcost',document.getElementById('custcost').value);
            }
            document.getElementById('ADD_AFTER_TAX').value = '';
            document.getElementById('ADD_BEFORE_TAX').value = '';
        }
            
            //countTotal();
          
            
      
        
    }
        
    
    
    function updateTotalWithAgentCommCustom(val2)
        {
            
            if(document.getElementById('def_cnsrate').checked==false) {
          val1 = document.getElementById('total_amount').value;
            
           newval = (val2/100)*val1; 
           document.getElementById('COMMISSION_AMOUNT').value=newval;
           document.getElementById("APPLY_BEFORE_TAX").value = '';
           document.getElementById("AGENT_COMMISSION_NO").value = '';
           alert('Agent Commission is '+newval+' and to be added after tax!'); }
       else {
           commissionUser(document.getElementById('custom').value);
            newval = document.getElementById('COMMISSION_AMOUNT').value;
            alert('Agent Commission is '+newval+' and to be added before tax!');
       }
      
   }
</script>

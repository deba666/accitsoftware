<?php
    /*
        * @package    ACCIT
         * @author    Paperlink Softwares Team
         * @copyright    Copyright (c) 2016 , Paperlink Softwares Pvt. Ltd.  (http://www.paperlinksoftwares.com/)
         * @since    Version 1.0.0
         * @filesource
         */
    ?>

	<div role="main" class="right_col" style="min-height: 648px;">
    <div class="">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="page-title">
                <div class="title_left">
                    <h3>Language Classes</h3>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="clearfix"></div>
                    <div class="x_content">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel recent-app">
                                    <div class="x_content">
                                        <div class="student-account-page">
                                            <!-- Nav tabs -->
                                            <div class="card">
                                                <ul role="tablist" class="nav nav-tabs">
                                                    <li class="active" role="presentation"><a data-toggle="tab" role="tab" aria-controls="all-student" href="#all-student">Language Classes</a></li>
                                                </ul>
                                                <div class="tab-content">
                                                    <div id="all-student" class="tab-pane active" role="tabpanel">
                                                        <div class="all-students-list">
                                                            <div class="table-responsive">
                                                                <div id="datatable-buttons_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                                                                    <div class="dataTables_wrapper form-inline dt-bootstrap no-footer" id="datatable-buttons_wrapper">
                                                                        <div id="datatable-buttons_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                                                                            <table class="table table-striped jambo_table bulk_action dataTable no-footer dtr-inline table data-tbl-tools" id="datatable-buttons" role="grid" aria-describedby="datatable-buttons_info" style="width: 2024px;">
                                                                                <thead>
                                                                                    <tr class="headings" role="row">
                                                                                        <th class="column-title sorting_asc" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1" style="width: 145px;" aria-label="Date
                                                                                            : activate to sort column descending" aria-sort="ascending">Class Name
                                                                                        </th>
                                                                                        <th class="column-title sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1" style="width: 145px;" aria-label="Attention
                                                                                            : activate to sort column ascending">Class Session
                                                                                        </th>
                                                                                        <th class="column-title sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1" style="width: 145px;" aria-label="Student No
                                                                                            : activate to sort column ascending">Class Room
                                                                                        </th>
                                                                                        <th class="column-title sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1" style="width: 145px;" aria-label="First Name
                                                                                            : activate to sort column ascending">Class Level
                                                                                        </th>
                                                                                        <th class="column-title sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1" style="width: 145px;" aria-label="Middle Name
                                                                                            : activate to sort column ascending">Class Type
                                                                                        </th>
                                                                                        <th class="column-title sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1" style="width: 145px;" aria-label="Last Name
                                                                                            : activate to sort column ascending">Class per Week
                                                                                        </th>
                                                                                        <th class="column-title no-link last sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1" style="width: 145px;" aria-label="Action
                                                                                            : activate to sort column ascending"><span class="nobr">Action</span>
                                                                                        </th>
                                                                                    </tr>
                                                                                </thead>
                                                                                <?php $class_row=getRows(' ORDER BY`class_id` DESC','classes'); ?>
                                                                                <tbody>
                                                                                    <?php
                                                                                        $sl=0;
                                                                                        foreach($class_row as $row)
                                                                                        {
                                                                                        ?>
                                                                                    <tr class="pointer odd" role="row">
                                                                                        <td class="sorting_1" tabindex="0"><?=$row['class_name'];?></td>
                                                                                        <td class=" "><?=$row['class_session'];?></td>
                                                                                        <td class=" "><?=$row['class_room'];?></td>
                                                                                        <td class=" "><?=$row['class_level'];?></td>
                                                                                        <td class=" "><?=$row['class_type'];?></td>
                                                                                        <td class=" "><?=$row['class_per_w'];?></td>
                                                                                        <td class="last"><a class="color-sky stting" href="#"><i aria-hidden="true" class="fa fa-cog"></i>Edit</a>| <a class="color-sky stting" href="#"><i aria-hidden="true" class="fa fa-pencil-square-o"></i>Delete</a></td>
                                                                                    </tr>
                                                                                    <?php }?>    
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
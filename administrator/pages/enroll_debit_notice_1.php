<?php
    /*
        * @package    ACCIT
         * @author    Paperlink Softwares Team
         * @copyright    Copyright (c) 2016 , Paperlink Softwares Pvt. Ltd.  (http://www.paperlinksoftwares.com/)
         * @since    Version 1.0.0
         * @filesource
		 * code for display what type of debit give to the student
		 * Comes under Enrol details->fees->debit.
         */
    ?>
<div role="main" class="right_col" style="min-height: 648px;">
    <div class="">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="page-title">
                <div class="title_left">
                    <h3>Debit Wizard</h3>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="clearfix"></div>
                    <div class="x_content">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel recent-app">
                                    <div class="x_content">
                                        <div class="student-account-page">
                                            <!-- Nav tabs -->
                                            <div class="card">
                                                <ul role="tablist" class="nav nav-tabs">
                                                    <li class="active" role="presentation"><a data-toggle="tab" role="tab" aria-controls="all-student" href="#all-student">Pleade read the following nitice, then click Next to proceed</a></li>
                                                </ul>
                                                <div class="tab-content">
                                                    <div id="all-student" class="tab-pane active" role="tabpanel">
                                                        <div class="all-students-list">
                                                            <div class="table-responsive">
                                                                <div  class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                                                                    <div class="dataTables_wrapper form-inline dt-bootstrap no-footer" id="datatable-buttons_wrapper">
                                                                        <div  class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                                                                            <div style="font-size:20px; text-align:center">The Debit Wizard Can not Refund the following Fees</div></br>
                                                                            <div style="font-size:14px; text-align:center">The Agent Commission Payments</div></br>
                                                                            <div style="font-size:14px; text-align:center">OSHC Payments to Medicare</div></br>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                              
                                            <div class="row">
                                                      <div class="col-md-2 col-sm-2 col-xs-2">
                                                            <div class="form-group">
                                                              <a class="btn btn-success" href="" onclick="javascript:void window.open('dashboard.php?op=<?=MD5('enroll_debit')?>&enrol_no=<?=$_GET['enrol_no']?>&stu_no=<?php echo $_GET['stu_no']?>&cust_no=<?php echo $_GET['cust_no']?>','144546548621367','width=800,height=550,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=0,top=0');return false;">Previous</a>
                                                            </div>
                                                        </div>
														<div class="col-md-4 col-sm-4 col-xs-4">
                                                            <div class="form-group">
                                                                <a class="btn btn-success" href="" onclick="javascript:void window.open('dashboard.php?op=<?=MD5('enroll_debit_refund_agent')?>&enrol_no=<?=$_GET['enrol_no']?>&stu_no=<?php echo $_GET['stu_no']?>&cust_no=<?php echo $_GET['cust_no']?>','1470877445645367','width=800,height=550,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=0,top=0');return false;">Next</a>
                                                            </div>
                                                        </div>     
                                                    </div>
                                        
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
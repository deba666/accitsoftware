<?php
/*
        * @package    ACCIT
         * @author    Paperlink Softwares Team
         * @copyright    Copyright (c) 2016 , Paperlink Softwares Pvt. Ltd.  (http://www.paperlinksoftwares.com/)
         * @since    Version 1.0.0
         * @filesource
		 * Code for Top navigation menu- message , notification etc
         */

?><div class="top_nav">
                    <div class="nav_menu">
                        <nav class="" role="navigation">
                            <div class="nav toggle">
                                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                            </div>
                            <ul class="nav navbar-nav navbar-right">
                                <!--Account-->
                                <li class="">
                                    <a href="javascript:void(0);" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                    <?php if($_SESSION['row']['PROFILE_IMAGE']!='') { ?>
                                                                            <img src="upload/<?=$_SESSION['row']['PROFILE_IMAGE']?>" border="0" height="100" width="100" />
                                                                            <?php } else { ?>
                                                                            <img src="images/avatar.png" border="0" height="100" width="100" />
                                                                            <?php } ?><?=$_SESSION['frnd_admin']?>
                                    <span class=" fa fa-angle-down"></span>
                                    </a>
                                    
                                    <ul class="dropdown-menu dropdown-usermenu pull-right">
                                        <!--<li><a href="javascript:void(0);"><i class="fa fa-eye" aria-hidden="true"></i>View Profile</a>-->
                                        </li>
                                        <li><a href="dashboard.php?op=<?=MD5('edit_profile')?>"><i class="fa fa-edit"></i>Edit Profile</a>
                                        </li>
                                        <!--<li>
                                            <a href="dashboard.php?op=<?=MD5('settings')?>">
                                            <span><i class="fa fa-wrench" aria-hidden="true"></i>Settings</span>
                                            </a>
                                        </li>-->
                                        <li>
                                            <a href="javascript:void(0);"><i class="fa fa-info-circle" aria-hidden="true"></i>Help</a>
                                        </li>
                                        <li>
                                          <a href="dashboard.php?op=<?=MD5('logout')?>" class="btn display-block font-normal btn-danger">
                                            <i class="fa fa-power-off"></i>
                                            Logout
                                          </a>
                                        </li>
                                    </ul>
                                </li>
                                
                                
                                <!--Email   
                                <li role="presentation" class="dropdown email">
                                  <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                                    <i class="fa fa-envelope-o"></i>
                                    <span class="badge bg-green">2</span>
                                     
                                  </a>
                                  <ul id="menu1" class="email-dropdown dropdown-menu list-unstyled msg_list" role="menu">
                                    <div class="content mCustomScrollbar">
                                    
                                      <li>
                                        <a>
                                          <span class="image"><img src="images/img.jpg" alt="Profile Image" /></span>
                                          <span>
                                            <span>hi</span>
                                            <span class="time">3 mins ago</span>
                                          </span>
                                          <span class="message">
                                            Film festivals used to be do-or-die moments for movie makers. They were where...
                                          </span>
                                        </a>
                                      </li>
                                                                            
                                      <li>
                                        <div class="text-center">
                                          <a href="javascript:void(0);" class="btn display-block font-normal btn-danger">Read All Email
                                            <i class="fa fa-angle-right"></i>
                                          </a>
                                        </div>
                                      </li>
                                    </div>
                                  </ul>
                                </li> -->
                                 

                                <!--Notifications-->
                                <li role="presentation" class="dropdown notification">
                                  <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                                    <i class="fa fa-bell-o" aria-hidden="true"></i>
                                    <span class="badge bg-green" id="notification_count"></span>
                                  </a>
                                  <ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
                                    <div class="content mCustomScrollbar">
                                      <li class="notifi-title"><h4>STUDENT Notification</h4></li>
                                      <?php 
                                          $sql=mysql_query("SELECT * FROM notification WHERE APP_NO=1 and STATUS_NO=1 ORDER BY STUD_NO DESC");
                                          while($row=mysql_fetch_array($sql))
                                            {
if($row['STUD_NO']){
                                          $sql1=mysql_query("SELECT * FROM student WHERE STUD_NO=".$row['STUD_NO']);
                                          $row1=mysql_fetch_array($sql1);
}
?>
                                   
                                      <li class="nicescroll notification-list">
                                        <a href="#" class="list-group-item">
                                            <div class="media">
                                               <div class="pull-left p-r-10">
                                                  <span class="notify-profile-image"><img src="images/img.jpg" alt="Profile Image" /></span>
                                               </div>
                                               <div class="media-body">

                                                  <h5 class="media-heading"><a href="dashboard.php?op=<?=MD5('student_edit')?>&sid=<?=$row['STUD_NO']?>&nid=<?=$row['NOTIFICATION_NO']?>"><?=$row1['LNAME']?>  <?=$row1['FNAME']?></a></h5>
                                                  <p class="m-0">
                                                      <small>New Apply</small>
                                                  </p>
                                               </div>
                                            </div>
                                         </a>
                                      </li>
                                 
 										<?php }?>
                                      <li>
                                        <div class="text-center">
                                          <a href="javascript:void(0);" class="btn display-block font-normal btn-danger">See All Notification
                                            <i class="fa fa-angle-right"></i>
                                          </a>
                                        </div>
                                      </li>
                                    </div>
                                  </ul>
                                </li>
                                
                                <li role="presentation" class="dropdown notification">
                                  <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                                    <i class="fa fa-money" aria-hidden="true"></i>
                                    <span class="badge bg-green" id="notification_account"></span>
                                  </a>
                                  <ul id="menu2" class="dropdown-menu list-unstyled msg_list" role="menu">
                                    <div class="content mCustomScrollbar">
                                      <li class="notifi-title"><h4>ENROLMENT Notification</h4></li>
                                     <?php 
                                          $sql2=mysql_query("SELECT * FROM notification WHERE APP_NO=2 and STATUS_NO=1 ORDER BY ENROL_NO DESC");
                                          while($row2=mysql_fetch_array($sql2))
                                            {
if($row2['STUD_NO']){
                                          $sql3=mysql_query("SELECT * FROM student WHERE STUD_NO =".$row2['STUD_NO']);
                                          $row3=mysql_fetch_array($sql3);
}
?>
                                     	 <li class="nicescroll notification-list">
                                        <a href="#" class="list-group-item">
                                            <div class="media">
                                               <div class="pull-left p-r-10">
                                                  <span class="notify-profile-image"><img src="images/img.jpg" alt="Profile Image" /></span>
                                               </div>
                                               <div class="media-body">
                                                  <h5 class="media-heading"><a href="dashboard.php?op=<?=MD5('enrolment_details')?>&eid=<?=$row2['ENROL_NO']?>&nid=<?=$row2['NOTIFICATION_NO']?>"><?=$row3['FNAME']?> <?=$row3['LNAME']?></a></h5>
                                                  <p class="m-0">
                                                   <small>New Enrol<small>
                                                  </p>
                                               </div>
                                            </div>
                                         </a>
                                      </li>
                                      <?php }?>
                                      <li>
                                        <div class="text-center">
                                          <a href="javascript:void(0);" class="btn display-block font-normal btn-danger">See All Notification
                                            <i class="fa fa-angle-right"></i>
                                          </a>
                                        </div>
                                      </li>
                                    </div>
                                  </ul>
                                </li>
                                
                            </ul>
                        </nav>
                    </div>
                </div>
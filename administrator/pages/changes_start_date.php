<?php
    /*
        * @package    ACCIT
         * @author    Paperlink Softwares Team
         * @copyright    Copyright (c) 2016 , Paperlink Softwares Pvt. Ltd.  (http://www.paperlinksoftwares.com/)
         * @since    Version 1.0.0
         * @filesource
		 * change start date in any date chosen from calender
         */
    
        include_once 'functions.php';
        $holiday_list = array();
        $q=mysql_query("select * from holiday where CRT_NO=".$_GET['faculty']." ORDER BY `FROM_DATE` , `TO_DATE`");
        $i=0;
        while($row=mysql_fetch_array($q))
        {
        if(!$row['DELETE_DATE'])
            {
                $array_terms[$i]['start_date'] = date("Y-m-d",strtotime($row['FROM_DATE']));
                $array_terms[$i]['end_date'] = date("Y-m-d",strtotime($row['TO_DATE']));
                $array_terms[$i]['extend'] = $row['OPTION_NO'];
                $array_terms[$i]['id'] = $row['HOLIDAY_NO'];
                
                $begin = new DateTime($row['FROM_DATE']);//store holiday start date in $begin
                $end = new DateTime($row['TO_DATE']);//store holiday end date in $end
                $end = $end->add(new DateInterval('P1D'));
                $diff = $end->diff($begin)->format("%a");//calculate total difference dates in holiday from date to end date
                //echo"TOTAL DIFFERENCE DATE IS=".$diff ."<br>";
                $daterange = new DatePeriod($begin, new DateInterval('P1D'), $end); 
               
            
            foreach($daterange as $date)
                {
                $date1=$date->format("Y-m-d");//change holiday date format into dd/mm/yyyy format
                //echo "Holiday Dates Are=".$date1."</br>";
                $holiday_list[]=$date1;//store all the holidays into $holiday_list variable which are fetch from database 
             
                }
                $i++;
             }  
        }
    	if(isset($_POST['postform']))
        {
            $start_date=$_POST['start_date'];
            $length=$_POST['length']*5;
            //$length = $length*7;
            $start = new DateTime($start_date);
            $start_date = date_create($start->format('Y-m-d'));
            $start_date= date_format($start_date, 'Y-m-d'); 
            $start_date=date('Y-m-d', strtotime($start_date)); 
            $end_date = calculateEndDateOffer($start_date,$length,$array_terms); 

            if(in_array($start_date,$holiday_list)==true)//check if the user input date comes under holiday dates or not
                    {
    
           $holiday='0';
          
            
            ?> 
<script type="text/javascript"> 
    alert("You Can not start Course at Holidays! Please select Another date."); 
    //return false;
    //window.location.href="<?php echo $_SESSION['URL_REDIRECT']; ?>";
    window.location.href="http://<?=$_SERVER['HTTP_HOST']?><?=$_SERVER['REQUEST_URI']?>";
</script>
            
   <?php   exit(); } else { 
       mysql_query("UPDATE `enrol` SET `ST_DATE` = '".$start_date."' , `END_DATE` = '".$end_date."' WHERE `ENROL_NO` ='".$_POST['ENROL_NO']."'");
       
       ?> <script type="text/javascript">     
    //window.location.href="http://<?=$_SERVER['HTTP_HOST']?><?=$_SERVER['REQUEST_URI']?>&finish_date=<?=$end_date?>&start_date=<?=$start_date?>";
</script>   <script>
           //window.onunload = closeMeAndRefreshOpener;
    //function closeMeAndRefreshOpener() {
        window.opener.location.reload();
        self.close();
    //}
     </script><?php }
        }
       ?>



<div class="col-md-12 col-sm-12 col-xs-12">
    <div class="page-title">
        <div class="title_left">
            <?php 
                if($_SESSION['s_msg'] || $_SESSION['e_msg'])
                {
                ?>
            <div class="alert <?=($_SESSION['s_msg'])?'alert-success':'alert-error'?> fade in">
                <button data-dismiss="alert" class="close" type="button">�</button>
                <?=($_SESSION['s_msg'])?$_SESSION['s_msg']:$_SESSION['e_msg']?>
            </div>
            <?php
                unset($_SESSION['s_msg']);
                unset($_SESSION['e_msg']);
                }
                ?>
            <h3>Change Start Date</h3>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="clearfix"></div>
            <div class="x_content">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel recent-app">
                            <div class="x_content">
                                <div class="student-account-page edit-faculty-page">
                                    <!-- Nav tabs -->
                                    <div class="card">
                                        <div class="all-students-list add student">
                                            <div class="add-student-section">
                                                <form method="post" action="" class="form-horizontal form-label-left" name="changedateenrol" id="changedateenrol">
                                                   
                                                    <input class="control-label col-md-12 col-sm-12 col-xs-12" name="ENROL_NO" value="<?=$_GET['ENROL_NO']?>" type="hidden">
                                                    
                                                    <div class="row">
                                                        <div class="col-md-2 col-sm-2 col-xs-12">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-12 col-sm-12 col-xs-12" for="name">Start Date
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 col-sm-3 col-xs-12">
                                                            <div class="form-group">
                                                                <?php $start=$_GET['ST_DATE']?>
                                                                <input class="control-label col-md-3 col-sm-3 col-xs-3" name="start_date" id="start_date" <?php if($_GET['start_date']!='') { ?> value="<?php echo $_GET['start_date']; ?>" <?php } else { ?> value="<?=$start?>" <?php } ?> type="date" onchange="javascript: document.getElementById('changedateenrol').submit();">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="item form-group">
                                                        <div class="form-group">
                                                        <label for="name" class="control-label col-md-2 col-sm-2 col-xs-2">Length</label>
                                                        </div>
                                                        <div class="col-md-8 col-sm-8 col-xs-8">
                                                            <div class="row">
                                                               
                                                                <input class="control-label col-md-3 col-sm-3 col-xs-3" name="length" id="length" type="number" disabled value="<?php echo $_GET['length']; ?>">&nbsp;&nbsp;Weeks
                                                              
                                                                
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-2 col-sm-2 col-xs-12">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-12 col-sm-12 col-xs-12" for="name">Finish
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 col-sm-3 col-xs-12">
                                                            <div class="form-group">
                                                                <?php $end=$_GET['END_DATE']?>
                                                                <input name="finish_date" id="finish_date" class="control-label col-md-3 col-sm-3 col-xs-3" <?php if($_GET['finish_date']!='') { ?> value="<?php echo $_GET['finish_date']; ?>" <?php } else { ?> value="<?=$end?>" <?php } ?> type="date" disabled>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="item form-group">
                                                        <label for="name" class="control-label col-md-3 col-sm-3 col-xs-12"></label>
                                                       <!-- <div class="col-md-9 col-sm-9 col-xs-12">
                                                            <div class="row">
                                                                <div class="col-md-6 col-sm-4 col-xs-12">
                                                                    <p class="pull-left">
                                                                        <button class="btn btn-primary" type="submit">Cancel</button>
                                                                        <button class="btn btn-success" type="submit">Submit</button>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div> -->
                                                    </div>
                                                    <input type="hidden" name="postform" id="postform" value="1" />
                                                    <input type="hidden" name="length" id="length" value="<?=$_GET['length']?>" />
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
    /*
        * @package    ACCIT
         * @author    Paperlink Softwares Team
         * @copyright    Copyright (c) 2016 , Paperlink Softwares Pvt. Ltd.  (http://www.paperlinksoftwares.com/)
         * @since    Version 1.0.0
         * @filesource
		 * Code for display all the language classes from databse
		 * Link add for add new language class(Edcit and delete links are also bellow in the code)
		 * Link add for  Absece students
         */
    
        if($_GET['dtl1'])
            {
              if(delete_record("DELETE FROM `lang_class` WHERE `LANG_CLASS_NO`=".$_GET['dtl1']))
            {
                $_SESSION['s_msg']="Item successfully deleted";
            }
            }
        
        ?>
<div role="main" class="right_col" style="min-height: 648px;">
    <div class="">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="page-title">
                <div class="title_left">
                    <h3>Language Classes</h3>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="clearfix"></div>
                    <div class="x_content">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel recent-app">
                                    <div class="x_content">
                                        <div class="student-account-page">
                                            <?php 
                                                if($_SESSION['s_msg'] || $_SESSION['e_msg'])
                                                {
                                                ?>
                                            <div class="alert <?=($_SESSION['s_msg'])?'alert-success':'alert-error'?> fade in">
                                                <button data-dismiss="alert" class="close" type="button">�</button>
                                                <?=($_SESSION['s_msg'])?$_SESSION['s_msg']:$_SESSION['e_msg']?>
                                            </div>
                                            <?php
                                                unset($_SESSION['s_msg']);
                                                unset($_SESSION['e_msg']);
                                                }
                                                ?>
                                            <!-- Nav tabs -->
                                            <div class="card">
                                             

                                                <div class="tab-content">
                                                    <div id="all-student" class="tab-pane active" role="tabpanel">
                                                        <div class="all-students-list">
                                                            <div class="table-responsive">
                                                                <table class="table data-tbl-tools" cellspacing="0" width="100%">
                                                                    <thead>
                                                                        <tr class="headings">
                                                                            <th class="column-title">Class</th>
                                                                            <th class="column-title">Session</th>
                                                                            <th class="column-title">Level</th>
                                                                            <th class="column-title">Room</th>
                                                                            <th class="column-title">Teacher</th>
                                                                            
                                                                            
                                                                            
                                                                            <th class="column-title no-link last">
                                                                                <span class="nobr">Action</span>
                                                                            </th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        <?php 
																		//display all the language classes
                                                                            $sql1="select * from lang_class";
                                                                            $q1=mysql_query($sql1);
                                                                            while($row1=mysql_fetch_array($q1))
                                                                            { 
                                                                            
                                                                              ?>
                                                                        <tr class="pointer odd" role="row">
                                                                            <td class=""><?=$row1['LANG_CLASS_NAME'];?></td>
                                                                            <td class=" ">Session&nbsp;&nbsp;<?=$row1['SESSION_NO'];?></td>
                                                                            <td class=" "><?=$row1['LEVEL_NO'];?></td>
                                                                            <?php $cond="where `ROOM_NO`=".$row1['ROOM_NO']; $cou=getRows($cond,'room');?>
                                                                            <td class=" "><?=$cou[0]['ROOM_DESC'];?></td>
                                                                            <?php $cond1="where `TEACHER_NO`=".$row1['TEACHER_NO']; $cou1=getRows($cond1,'teacher');?>
                                                                            <td class=" "><?=$cou1[0]['TEACHER_CODE'];?></td>
                                                                            <?php $sql="select SUM(STUD_NO) from enroll E, course C, lang_class L where L.LANG_CLAS_No=C.LANG_CLASS_NO and C.COURSE_NO=E.COURSE_NO";
                                                                                $q=mysql_query($sql);
                                                                                ?>
                                                                            <td><a href="dashboard.php?op=<?=MD5('display_language_class_edit')?>&lang_no=<?=$row1['LANG_CLASS_NO']?>">Edit</a>|<a href="dashboard.php?op=<?=MD5('classes_enrolment')?>&dtl=<?=$row1['LANG_CLASS_NO']?>" class="color-sky stting"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>Enrolment</a>|<a onclick="javascript: var p = confirm('Are you sure?'); if(p==false) { return false; }" href="dashboard.php?op=<?=MD5('classes_language')?>&dtl1=<?=$row1['LANG_CLASS_NO']?>" class="color-red stting"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>Delete</a><a href="#" onclick="javascript:void window.open('dashboard.php?op=<?=MD5('display_absence_class')?>&abs=<?=$row1['LANG_CLASS_NO']?>&class=<?=$row1['LANG_CLASS_NAME']?>&level=<?=$row1['LEVEL_NO']?>&room=<?=$cou[0]['ROOM_DESC']?>&teacher=<?=$cou1[0]['TEACHER_CODE']?>','147013862137','width=1300,height=800,toolbar=0,menubar=0,location=1,status=1,scrollbars=1,resizable=1,left=0,top=0');return false;">Absences</a></td>
                                                                            <?php }  ?>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
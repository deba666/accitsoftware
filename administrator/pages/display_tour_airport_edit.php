<?php
    /*
        * @package    ACCIT
         * @author    Paperlink Softwares Team
         * @copyright    Copyright (c) 2016 , Paperlink Softwares Pvt. Ltd.  (http://www.paperlinksoftwares.com/)
         * @since    Version 1.0.0
         * @filesource
		 * Code for Tour group airport edit
         */
    
    if($_POST['F_DATE'])
    	{
    	if(mysql_query("UPDATE `tour_airport` set `F_DATE`='".$_POST['F_DATE']."',`F_TIME`='".$_POST['F_TIME']."',`F_FROM`='".$_POST['F_FROM']."',`F_TO`='".$_POST['F_TO']."',`FLIGHT_NAME`='".$_POST['FLIGHT_NAME']."',`PICKUP`='".$_POST['PICKUP']."',`NOTES`='".$_POST['NOTES']."',`USER_NO`='".$_POST['USER_NO']."',`WHO_DATE`='".$_POST['WHO_DATE']."' where TOUR_AIRPORT_NO=".$_GET['ap_no']))
    	  
    		$_SESSION['s_msg']="<strong>Fine!</strong> New Teacher Added";
    		
    		else
    			$_SESSION['e_msg']="<strong>Oh snap!</strong> Teacher is not added";
    		
    	}
    	if($_GET['ap_no'])
    {
    	$cond="where `TOUR_AIRPORT_NO`=".$_GET['ap_no'];
    	$std_row=getRows($cond,'tour_airport');
    }
    ?>
<script language="javascript" type="text/javascript" src="datetimepicker.js"></script>
<div class="right_col" role="main">
    <div class="">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="page-title">
                <div class="title_left">
                    <?php 
                        if($_SESSION['s_msg'] || $_SESSION['e_msg'])
                        {
                        ?>
                    <div class="alert <?=($_SESSION['s_msg'])?'alert-success':'alert-error'?> fade in">
                        <button data-dismiss="alert" class="close" type="button">�</button>
                        <?=($_SESSION['s_msg'])?$_SESSION['s_msg']:$_SESSION['e_msg']?>
                    </div>
                    <?php
                        unset($_SESSION['s_msg']);
                        unset($_SESSION['e_msg']);
                        }
                        ?>
                    <h3>Tour Group Airport(Modify)</h3>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="clearfix"></div>
                    <div class="x_content">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel recent-app">
                                    <div class="x_content">
                                        <div class="student-account-page">
                                            <!-- Nav tabs -->
                                            <div class="card">
                                                <div class="all-students-list add-course">
                                                    <div class="add-student-section">
                                                        <form class="form-horizontal form-label-left" action="" method="post" novalidate>
                                                            <ul id="student-acc-tab" class="nav nav-tabs" role="tablist">
                                                                <li role="presentation"  class="active"><a href="#details" aria-controls="details" role="tab" data-toggle="tab">Details</a></li>
                                                            </ul>
                                                            <div class="tab-content">
                                                                <!--Details-->
                                                                <div role="tabpanel" class="tab-pane active" id="details">
                                                                    <h4>
                                                                        <fieldset>Basic Info</fieldset>
                                                                    </h4>
                                                                    <input id="when"  name="USER_NO" type="hidden" value="<?php echo $_SESSION['user_no']?>">
                                                                    <?php date_default_timezone_set('Australia/sydney');
                                                                        $date = date('d/m/Y h:i:s a', time());
                                                                        ?>
                                                                    <input id="who"  name="WHO_DATE"  type="hidden" value="<?php echo $date?>">
                                                                    <div class="row">
                                                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                                                <div class="form-group">
                                                                                    <label class="pull-left control-label col-md-12 col-sm-12 col-xs-12" for="name">Date</label>
                                                                                    <input value="<?=$std_row[0]['F_DATE']?>" class="form-control col-md-12 col-sm-12 col-xs-12" name="F_DATE" placeholder="MM/DD/YYYY" type="date">
                                                                                </div>
                                                                            </div>
                                                                        </div>
																	
																	
																	<div class="row">
                                                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                                                <div class="form-group">
                                                                                    <label class="pull-left control-label col-md-12 col-sm-12 col-xs-12" for="name">Time</label>
                                                                                <input  name="F_TIME" class="form-control col-md-12 col-xs-12" type="time" value="<?=$std_row[0]['F_TIME']?>">
																	              </div>
                                                                            </div>
                                                                        </div>
                                                                    <div class="row">
                                                                        <div class="col-md-2 col-sm-2 col-xs-2">
                                                                            <div class="item form-group">
                                                                                From
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-4 col-sm-4 col-xs-4">
                                                                            <div class="item form-group">
                                                                                <input type="text" value="<?=$std_row[0]['F_FROM']?>" name="F_FROM" class="form-control col-md-12 col-xs-12">
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-2 col-sm-2 col-xs-2">
                                                                            <div class="item form-group">
                                                                                To
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-4 col-sm-4 col-xs-4">
                                                                            <div class="item form-group">
                                                                                <input type="text" value="<?=$std_row[0]['F_TO']?>" name="F_TO" class="form-control col-md-12 col-xs-12">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-md-2 col-sm-2 col-xs-2">
                                                                            <div class="item form-group">
                                                                                Flight
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-4 col-sm-4 col-xs-4">
                                                                            <div class="item form-group">
                                                                                <input type="text" value="<?=$std_row[0]['FLIGHT_NAME']?>" name="FLIGHT_NAME" class="form-control col-md-12 col-xs-12">
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-2 col-sm-2 col-xs-2">
                                                                            <div class="item form-group">
                                                                                Pickup By
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-4 col-sm-4 col-xs-4">
                                                                            <div class="item form-group">
                                                                                <input type="text" value="<?=$std_row[0]['PICKUP']?>" name="PICKUP" class="form-control col-md-12 col-xs-12">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="item form-group">
                                                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Notes</label>
                                                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                                                    <textarea rows="4" cols="50" class="form-control col-md-12 col-xs-12" name="NOTES" placeholder="Notes" type="text"><?=$std_row[0]['NOTES']?></textarea>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <button type="submit" class="btn btn-success" onclick="needToConfirm = false;">Save</button>
                                                        </form>
                                                        </div>
                                                        </div>
                                                    </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
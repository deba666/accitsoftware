<?php
    /*
        * @package    ACCIT
         * @author    Paperlink Softwares Team
         * @copyright    Copyright (c) 2016 , Paperlink Softwares Pvt. Ltd.  (http://www.paperlinksoftwares.com/)
         * @since    Version 1.0.0
         * @filesource
		 * Code for display all rooms from database
		 * Link comes from sidebar(utility->rooms->all rooms)
         */
    
    
    ?>
<div class="right_col" role="main">
    <div class="">
      
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="clearfix"></div>
                    <div class="x_content">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel recent-app">
                                    <div class="x_content">
                                        <div class="student-account-page">
                                            <!-- Nav tabs -->
                                            <div class="card">
                                                <div class="tab-content">
                                                    <div role="tabpanel" class="tab-pane active" id="all-student">
                                                        <div class="all-students-list">
                                                            <div class="table-responsive">
                                                                <table  class="table data-tbl-tools">
                                                                    <thead>
                                                                        <tr class="headings">
                                                                            <th class="column-title">Invoice No</th>
                                                                            <th class="column-title">Enrol No</th>
                                                                            <th class="column-title">Student</th>
                                                                            <th class="column-title">Amount</th>
                                                                            <th class="column-title">Due date</th>
                                                                            <th class="column-title">Invoice date</th>
                                                                            <th class="column-title">User</th>
                                                                            <th class="column-title">Agent</th>
                                                                            <th class="column-title">Create date</th>                                                                          
                                                                        </tr>
                                                                    </thead>
                                                                    <?php $invoice=getRows('order by `INVOICE_NO` DESC','invoice'); ?>
                                                                    <tbody>
                                                                        <?php
                                                                           
                                                                            foreach($invoice as $row)
                                                                            {
                                                                                $stud=getRows("WHERE `STUD_NO` = '".$row['STUD_NO']."'","student");
                                                                                $user=getRows("WHERE `USER_NO` = '".$row['USER_NO']."'","users");
                                                                                $agent=getRows("WHERE `CUSTOMER_NO` = '".$row['PAYEE_CUSTOMER_NO']."'","customer");
                                                                            ?>
                                                                        <tr class="even pointer">
                                                                            <td class=" "><?=$row['INVOICE_NO'];?> </td>
                                                                            <td class=" "><?="#".$row['ENROL_NO'];?> </td>
                                                                            <td class=" "><?=$stud[0]['FNAME']."&nbsp;".$stud[0]['MNAME']."&nbsp;".$stud[0]['LNAME'];?> </td>
                                                                            <td class=" "><?=$row['INVOICE_AMOUNT'];?></td>
                                                                            <td class=" "><?=$row['DUE_DATE'];?></td>
                                                                            <td class="last"><?=$row['INVOICE_DATE'];?></td>
                                                                            <td class="last"><?=$user[0]['USER_NAME'];?></td>
                                                                            <td class="last"><?=$agent[0]['NAME'];?></td>
                                                                            <td class="last"><?=$row['CREATE_DATE'];?></td>
                                                                        </tr>
                                                                        <?php   }    ?>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
    /*
        * @package    ACCIT
         * @author    Paperlink Softwares Team
         * @copyright    Copyright (c) 2016 , Paperlink Softwares Pvt. Ltd.  (http://www.paperlinksoftwares.com/)
         * @since    Version 1.0.0
         * @filesource
		 * Code for Display all the student
         */
    ?>
<div class="right_col" role="main">
    <div class="">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="page-title">
                <div class="title_left">
                    <h3>All Students</h3>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="clearfix"></div>
                    <div class="x_content">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel recent-app">
                                    <div class="x_content">
                                        <div class="student-account-page">
                                            <!-- Nav tabs -->
                                            <div class="card">
                                                <ul class="nav nav-tabs" role="tablist">
                                                    <li role="presentation"  class="active"><a href="#all-student" aria-controls="all-student" role="tab" data-toggle="tab">All Students</a></li>
                                                    <!--<li role="presentation"><a href="#add-student" aria-controls="add-student" role="tab" data-toggle="tab">Add Students</a></li>
                                                        <li role="presentation"><a href="#student-account" aria-controls="student-account" role="tab" data-toggle="tab">Students Account</a></li>-->
                                                </ul>
                                                <!-- Tab panes -->
                                                <div class="tab-content">
                                                    <div role="tabpanel" class="tab-pane active" id="all-student">
                                                        <div class="all-students-list">
                                                            <div class="table-responsive">
                                                                <table  class="table data-tbl-tools">
                                                                    <thead>
                                                                        <tr class="headings">
                                                                            <th class="column-title">Student No.</th>
                                                                            <th class="column-title">Name </th>
                                                                            <th class="column-title">Gender</th>
                                                                            <th class="column-title">DOB</th>
                                                                            <th class="column-title">Phone</th>
                                                                            <th class="column-title">Email</th>
                                                                            <th class="column-title no-link last"><span class="nobr">Action</span> </th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        <?php $std_row="select * from student ORDER BY STUD_NO DESC";
                                                                            $q=mysql_query($std_row);
                                                                            while($row=mysql_fetch_array($q))
                                                                            {
                                                                            ?>
                                                                        <tr class="even pointer">
                                                                            <td class=" "><?=$row['EXT_STN'];?> </td>
                                                                            <td class=" "><?=$row['FNAME'];?>&nbsp;&nbsp;<?=$row['MNAME'];?>&nbsp;&nbsp;<?=$row['LNAME'];?></td>
                                                                            <td class=" "><?=$row['GENDER'];?></td>
                                                                            <?php
                                                                                $originalDate5 = $row['DOB'];
                                                                                $newDate5 = date("d/m/y", strtotime($originalDate5)); ?>	
                                                                            <td class=" "><?php echo $newDate5 ?></td>
                                                                            <td class=" "><?=$row['O_PH'];?></td>
                                                                            <td class=" "><?=$row['O_EMAIL'];?></td>
                                                                            <td class="last"><a href="#" onclick="javascript:void window.open('dashboard.php?op=<?=MD5('display_student_diary')?>&s_no=<?=$row['STUD_NO']?>&s_fname=<?=$row['FNAME']?>&s_lname=<?=$row['LNAME']?>&s_mname=<?=$row['MNAME']?>&ag_name=<?=$row['AGENT_NAME']?>&cus_no=<?=$row['CUSTOMER_NO']?>','1408709021367','width=700,height=500,toolbar=0,menubar=0,location=1,status=1,scrollbars=1,resizable=1,left=0,top=0');return false;">Add New Diary</a>|<a href="#" onclick="javascript:void window.open('dashboard.php?op=<?=MD5('all_student_diary')?>&s_no=<?=$row['STUD_NO']?>&s_fname=<?=$row['FNAME']?>&s_lname=<?=$row['LNAME']?>&s_mname=<?=$row['MNAME']?>&ag_name=<?=$row['AGENT_NAME']?>&cus_no=<?=$row['CUSTOMER_NO']?>','1406570021367','width=700,height=500,toolbar=0,menubar=0,location=1,status=1,scrollbars=1,resizable=1,left=0,top=0');return false;">All Diary</a></td>
                                                                        </tr>
                                                                        <?php   }    ?>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<?php
    /*
        * @package    ACCIT
         * @author    Paperlink Softwares Team
         * @copyright    Copyright (c) 2016 , Paperlink Softwares Pvt. Ltd.  (http://www.paperlinksoftwares.com/)
         * @since    Version 1.0.0
         * @filesource
         */
     $user=$_SESSION['frnd_admin']; 
     $user_row=getRows("where `username`='$user'",'administrator'); 
     $user=$user_row[0]['id'];
    if($_POST['email'])
    {
    		$name	    =$_POST['name'];
    		$email	=$_POST['email'];
    		$password	=MD5($_POST['password']);
    		$phone	=$_POST['phone'];
    								
    		if(update_record("`administrator`", "`name`='".$name."', `email`='".$email."', `password`='".$password."', `phone`='".$phone."'", "`id`=$user"))
    			$_SESSION['s_msg']="<strong>Well done!</strong> data successfully updated";
    		
    		else
    			$_SESSION['e_msg']="<strong>Oh snap!</strong> data is not updated";
    }
    
    ?>
<div role="main" class="right_col" style="min-height: 648px;">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>General Settings</h3>
            </div>
        </div>
        <?php 
            if($_SESSION['s_msg'] || $_SESSION['e_msg'])
            {
            ?>
        <div class="alert <?=($_SESSION['s_msg'])?'alert-success':'alert-error'?> fade in">
            <button data-dismiss="alert" class="close" type="button">×</button>
            <?=($_SESSION['s_msg'])?$_SESSION['s_msg']:$_SESSION['e_msg']?>
        </div>
        <?php
            unset($_SESSION['s_msg']);
            unset($_SESSION['e_msg']);
            }
            ?>      
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12">
                <div class="x_panel">
                    <div class="clearfix"></div>
                    <div class="x_content">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel recent-app">
                                    <div class="x_content">
                                        <div class="admin-setting text-center">
                                            <div class="add-student-section">
                                                <form novalidate="" action="" method="post" class="form-horizontal form-label-left">
                                                    <div class="item form-group">
                                                        <label for="name" class="control-label col-md-2 col-sm-2 col-xs-12">Name 
                                                        </label>
                                                        <div class="col-md-7 col-sm-7 col-xs-12">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <input type="text" value="<?=$user_row[0]['name']?>" name="name" class="form-control col-md-12 col-xs-12" id="name">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="item form-group">
                                                        <label for="email" class="control-label col-md-2 col-sm-2 col-xs-12">Email 
                                                        </label>
                                                        <div class="col-md-7 col-sm-7 col-xs-12">
                                                            <input type="email" value="<?=$user_row[0]['email']?>" class="form-control col-md-7 col-xs-12" name="email" id="email">
                                                        </div>
                                                    </div>
                                                    <div class="item form-group">
                                                        <label for="email" class="control-label col-md-2 col-sm-2 col-xs-12">Password
                                                        </label>
                                                        <div class="col-md-7 col-sm-7 col-xs-12">
                                                            <input type="password" value="<?=$user_row[0]['password']?>" class="form-control col-md-7 col-xs-12" name="password" id="password">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-2 col-sm-2 col-xs-12">Phone Number
                                                        </label>
                                                        <div class="col-md-7 col-sm-7 col-xs-12">
                                                            <input type="text" value="<?=$user_row[0]['phone']?>" class="form-control col-md-7 col-xs-12" name="phone" id="phone">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-2 col-sm-2 col-xs-12">User Roll
                                                        </label>
                                                        <?php /*?>
                                                        <div class="col-md-7 col-sm-7 col-xs-12">
                                                            <select name="role" id="role" class="form-control">
                                                                <option value="0">Select</option>
                                                                <option value="admin">Admin</option>
                                                                <option value="staff">Staff</option>
                                                            </select>
                                                        </div>
                                                        <?php */?>
                                                    </div>
                                                    <div class="col-md-7 col-sm-7 col-xs-12 col-md-offset-2">
                                                        <button class="pull-left btn btn-primary" type="submit">Cancel</button>
                                                        <button class="pull-left btn btn-success" type="submit">Submit</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
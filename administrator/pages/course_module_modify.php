<?php

/*
        * @package    ACCIT
         * @author    Paperlink Softwares Team
         * @copyright    Copyright (c) 2016 , Paperlink Softwares Pvt. Ltd.  (http://www.paperlinksoftwares.com/)
         * @since    Version 1.0.0
         * @filesource
		 * Code for add new course module/Subject. Links Feth from the left sidebar under utility
*/
    if($_POST['VET_OUTCOME_NO'])
    	{
    	 if(mysql_query("update `results` set `END_DATE`='".$_POST['END_DATE']."', `VET_OUTCOME_NO`='".$_POST['VET_OUTCOME_NO']."' WHERE `RESULT_NO`=".$_POST['RESULT_NO']))
    		$_SESSION['s_msg']="<strong>Fine!</strong> Update Successfully"; 
    	else
    		$_SESSION['e_msg']="<strong>Oh snap!</strong>Not Update";
    }
	if($_GET['RESULT_NO'])
        {
        	$cond="where `RESULT_NO`=".$_GET['RESULT_NO'];
        	$result_row=getRows($cond,'results');
		}
    ?>
<div class="right_col" role="main">
    <div class="">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="page-title">
                <div class="title_left">
                    <?php 
                        if($_SESSION['s_msg'] || $_SESSION['e_msg'])
                        {
                        ?>
                    <div class="alert <?=($_SESSION['s_msg'])?'alert-success':'alert-error'?> fade in">
                        <button data-dismiss="alert" class="close" type="button">�</button>
                        <?=($_SESSION['s_msg'])?$_SESSION['s_msg']:$_SESSION['e_msg']?>
                    </div>
                    <?php
                        unset($_SESSION['s_msg']);
                        unset($_SESSION['e_msg']);
                        }
                        ?>
                    <h3>Add Course Module</h3>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="clearfix"></div>
                    <div class="x_content">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel recent-app">
                                    <div class="x_content">
                                        <div class="student-account-page">
                                            <!-- Nav tabs -->
                                            <div class="card">
                                                <div class="all-students-list add-course">
                                                    <div class="add-student-section">
                                                        <form class="form-horizontal form-label-left" action="" method="post" enctype="multipart/form-data">
                                                            <?php date_default_timezone_set('Australia/Sydney');
                                                                $date = date('d/m/Y h:i:s a', time());
                                                                ?>
                                                            <input id="who"  name="who"  type="hidden" value="<?php echo $date?>">
                                                            <input id="when"  name="user" type="hidden" value="<?php echo $_SESSION['user_no']?>">
															<input id="when"  name="RESULT_NO" type="hidden" value="<?= $result_row[0]['RESULT_NO']?>">
                                                              <div class="item form-group">
                                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Date Finished</label>
                                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                                    <div class="row">
                                                                        <div class="col-md-7 col-sm-7 col-xs-12">
                                                                                <input type="date" class="form-control col-md-12 col-xs-12" name="END_DATE" value="<?php echo date("Y-m-d");?>">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            
                                                            <div class="item form-group">
                                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Outcome</label>
                                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                                    <div class="row">
                                                                        <div class="col-md-7 col-sm-7 col-xs-12">
                                                                           <select name="VET_OUTCOME_NO" class="col-md-12 col-xs-12 form-control">
																				<option Value="0"></option>
																				<?php
																					$sql7="select * from system_code where `SYSTEM_TYPE_NO`=".'222';
																					$q7=mysql_query($sql7);
																					while($row7=mysql_fetch_array($q7))
																					{
																					?>
																				<option Value="<?=$row7['SYSTEM_CODE_NO']?>"><?=$row7['NAME']?></option>
																				<?php } ?>
																			</select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="item form-group">
                                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name"></label>
                                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                                    <div class="row">
                                                                        <div class="col-md-6 col-sm-4 col-xs-12">
                                                                            <p class="pull-left">
                                                                                <button type="reset" value="Reset" class="btn btn-primary">Cancel</button>
                                                                                <button type="submit" class="btn btn-success">Submit</button>
                                                                            </p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
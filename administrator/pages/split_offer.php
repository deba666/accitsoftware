<?php
    /*
        * @package    ACCIT
         * @author    Paperlink Softwares Team
         * @copyright    Copyright (c) 2016 , Paperlink Softwares Pvt. Ltd.  (http://www.paperlinksoftwares.com/)
         * @since    Version 1.0.0
         * @filesource
		 * Code for split offer items
         */
    
    $cr=$_POST['offer_no'];
    //echo "cr=".$cr."</br>";
    $amount=$_POST['amount'];
    $div=$_POST['div'];
	if($div!=1)
	{
    $installment_amount=$amount/$div;
	$installment_amount1 = number_format($installment_amount, 2, '.', '');
	$period_length=0;
	}
	else 
	{
	$installment_amount=$amount/$div;
	$installment_amount1 = number_format($installment_amount, 2, '.', '');
	$period_length=1;
	}
    $installment=$amount/$installment_amount;
    //echo "Installmen Amounts are=".$installment_amount."</br>";
    //echo "Installment =".$installment."</br>";
    $commission_amount=$_GET['commission_amount'];
    $ccamm=$commission_amount/$installment;
	$ccamm1 = number_format($ccamm, 2, '.', '');
    //echo "Commission =".$commission_amount."</br>";
    $start_date =strtotime($_POST['FROM_DATE']);
    //echo $start_date."<br>";
    $end_date =strtotime($_POST['TO_DATE']);
    //echo $end_date."<br>";
    $datediff =$end_date-$start_date;
    $days=secondsToTime($datediff);
    //echo "Total Difference of Days=".$days;
    function secondsToTime($seconds) {
    	$dtF = new DateTime("@0");
    	$dtT = new DateTime("@$seconds");
    	return $dtF->diff($dtT)->format('%a');
    } 
    $installment_period=intval($days/$installment);
    //echo "</br>Installment Pay after =".$installment_period."days"."<br>";
    
    
    
    $q=mysql_query("select * from holiday");//fetch all the holidays
    while($row=mysql_fetch_array($q)){
    if(!$row['DELETE_DATE']){
    //echo "DB_FROM_DATE=".date("d/m/Y", strtotime($row['FROM_DATE']))."<br>";
    
    
    $begin = new DateTime($row['FROM_DATE']);
    $end = new DateTime($row['TO_DATE']);
    $diff = $end->diff($begin)->format("%a");
    //echo"TOTAL DIFFERENCE DATE IS=".$diff ."<br>";
    $daterange = new DatePeriod($begin, new DateInterval('P1D'), $end);
    
    //print_r($holiday_list);
    foreach($daterange as $date)
    	{
        $date1=$date->format("Y-m-d");
        $holiday_list[]=$date1;//store all the holiday dates in a array
     
    	}
    	
         }  
     }
    
    //print_r($holiday_list);
    
    $count=0;
    for($i=0;$i<$days;$i++)//loop for counting total day
    {
    
    $first1=date('Y-m-d', strtotime($_POST['FROM_DATE'].'+'.$i.'days'));//increment day by 1 day
    
      if(in_array($first1,$holiday_list))//check if date comes under holiday(Just for check)
    	{
    	//echo "Match Day=>".$first1."<br>";
    	
    	//$first1=date('Y-m-d', strtotime($first1.'+ 7days'));
    	}
    	//echo "COUNT VALUE IS=>".$count."<br>";
    	
    $mon_day = date("D",strtotime($first1));//take only day (MON) from the date 
    if($mon_day != 'Mon')//check whether the day is monday or not
    	{
    $monday=new DateTime($first1);
    $monday->modify('last monday');//if not set date to last monday and change the date format as yyyy-mm-dd
    $mod_monday=$monday->format('Y-m-d');
    
    	}
    	else
    	{
    		$monday=new DateTime($first1);
    		$mod_monday=$monday->format('Y-m-d');//else change only the date format as yyyy-mm-dd
    	}
    
    if(in_array($mod_monday,$holiday_list))//check if the date comes under holidays
    	{
    $mod_monday1=date('Y-m-d', strtotime($mod_monday.' + 7days'));//if true , increse by 7 day to take next monday
    	}
    	else
    	{
    		$mod_monday1=date('Y-m-d', strtotime($mod_monday.' + 0days'));//if false , not increase
    	}
    	//echo "Due Date=>".$mod_monday1."<br>";
    if(in_array($mod_monday1,$holiday_list))
    	{
    $mod_monday2=date('Y-m-d', strtotime($mod_monday1.' + 7days'));
    	}
    	else
    	{
    		$mod_monday2=date('Y-m-d', strtotime($mod_monday1.' + 0days'));
    	}
    	
    	//echo "Due Date=>".$mod_monday2."<br>";
    if(in_array($mod_monday2,$holiday_list))
    	{
    $mod_monday3=date('Y-m-d', strtotime($mod_monday2.' + 7days'));
    	}
    	else
    	{
    		$mod_monday3=date('Y-m-d', strtotime($mod_monday2.' + 0days'));
    	}
    	
    	//echo "Due Date=>".$mod_monday1."<br>";
    if(in_array($mod_monday3,$holiday_list))
    	{
    $mod_monday4=date('Y-m-d', strtotime($mod_monday3.' + 7days'));
    	}
    	else
    	{
    		$mod_monday4=date('Y-m-d', strtotime($mod_monday3.' + 0days'));
    	}
    	
    	//echo "Due Date=>".$mod_monday4."<br>";
    if(in_array($mod_monday4,$holiday_list))
    	{
    $mod_monday5=date('Y-m-d', strtotime($mod_monday4.' + 7days'));
    	}
    	else
    	{
    		$mod_monday5=date('Y-m-d', strtotime($mod_monday4.' + 0days'));

    	}
    	
    	//echo "Due Date Of Course=>".$mod_monday5."<br>";
    	
    insert_record("insert into `offer_instalment` set `OFFER_NO`='$cr',`DUE_DATE`='$mod_monday5',`AMOUNT`='$installment_amount1',`CREATE_DATE`='".$_GET['create_date']."',`DELETE_DATE`='".$_GET['delete']."',`LOCK_NUM`='".$_GET['lock_num']."',`OFFER_ITEM_NO`='".$_GET['offer_item_no']."',`TAX_AMOUNT`='".$_GET['tax_amount']."',`COMMISSION_AMOUNT`='$ccamm1',`DESCRIPTION`='',`FROM_DATE`='$start_date',`TO_DATE`='$end_date',`PERIOD_LENGTH`='$period_length',`FEE_NO`='',`SUBJECT_NO`='',`PAYEE_CUSTOMER_NO`='".$_GET['cust_no']."'"); 
    	//} 
    	$i+=$installment_period;
    $_SESSION['s_msg']="<strong>Fine!</strong> Split Fees Successfully";
	$sql="update `offer_item` set `FROM_DATE`='".$_POST['FROM_DATE']."',`TO_DATE`='".$_POST['TO_DATE']."' where `OFFER_ITEM_NO`=".$_GET['offer_item_no']; //update the status of the offer.
    		 mysql_query($sql);
    //}
    
    }
    
    
    
    
    ?>
<script language="javascript" type="text/javascript" src="datetimepicker.js"></script>
<div class="right_col" role="main">
    <div class="">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="custom-alrt">
                <?php 
                        if($_SESSION['s_msg'] || $_SESSION['e_msg'])
                        {
                        ?>
                    <div class="alert <?=($_SESSION['s_msg'])?'alert-success':'alert-error'?> fade in">
                        <button data-dismiss="alert" class="close" type="button"><i class="fa fa-times" aria-hidden="true"></i></button>
                        <?=($_SESSION['s_msg'])?$_SESSION['s_msg']:$_SESSION['e_msg']?>
                    </div>
                    <?php
                        unset($_SESSION['s_msg']);
                        unset($_SESSION['e_msg']);
                        }
                ?>
            </div>
            <div class="page-title">
                <div class="title_left">
                    
                    <h3>Split Fee</h3>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="clearfix"></div>
                    <div class="x_content">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel recent-app">
                                    <div class="x_content">
                                        <div class="student-account-page">
                                            <!-- Nav tabs -->
                                            <div class="card">
                                                <div class="all-students-list add-course">
                                                    <div class="add-student-section">
                                                        <form class="form-horizontal form-label-left" action="" method="post" novalidate>
                                                            <ul id="student-acc-tab" class="nav nav-tabs" role="tablist">
                                                                <center>
                                                                    <strong>
                                                                        <h4>
                                                                        <?php $r=$_GET['from_date']?>
                                                                        Due:&nbsp;&nbsp;&nbsp;<?php echo $r;?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(Multiple_Fees)&nbsp;&nbsp;&nbsp;&nbsp;$<?=$_GET['amount']?>.00
                                                                        <h4>
                                                                    </strong>
                                                                </center>
                                                                <li role="presentation">
                                                                </li>
                                                            </ul>
                                                            <input name="offer_no" type="hidden" value="<?= $_GET['offer_no']?>">
                                                            <div class="tab-content">
                                                                <!--Details-->
                                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                                    <input id="when"  name="USER_NO" type="hidden" value="<?php echo $_SESSION['user_no']?>">
                                                                    <?php date_default_timezone_set('Australia/sydney');
                                                                        $date = date('d/m/Y h:i:s a', time());
                                                                        ?>
                                                                    <input id="who"  name="WHO_DATE"  type="hidden" value="<?php echo $date?>">
                                                                </div>
                                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                                    <center>
                                                                        <h3>Distribute Installment Over Date Range</h3>
                                                                    </center>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="item form-group">
                                                                        <div class="col-md-3 col-sm-3 col-xs-3">
                                                                            <input type="date" value="<?= $_GET['from_date'];?>" name="FROM_DATE" placeholder="dd/mm/yyyy" class="form-control col-md-12 col-xs-12">
                                                                        </div>
                                                                        <div class="col-md-2 col-sm-2 col-xs-12">
                                                                            
                                                                        </div>
                                                                        <div class="col-md-2 col-sm-2 col-xs-12">
                                                                            To
                                                                        </div>
                                                                        <div class="col-md-3 col-sm-3 col-xs-3">
                                                                            <input type="date" value="<?= $_GET['to_date'];?>" name="TO_DATE" placeholder="dd/mm/yyyy" class="form-control col-md-12 col-xs-12">
                                                                        </div>
                                                                        <div class="col-md-2 col-sm-2 col-xs-12">
                                                                            
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="">
                                                                        <div class="col-md-1 col-sm-1 col-xs-6">
                                                                            <div class="item form-group">
                                                                                <input type="radio" name="group1" style="width:20px; height:20px;">
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-4 col-sm-4 col-xs-4">
                                                                            <div class="item form-group">
                                                                                <input type="hidden" value="<?=$_GET['amount']?>" name="amount">
                                                                                <input id="second" type="text" name="div" class="form-control col-md-12 col-xs-12">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-1 col-sm-1 col-xs-1">
                                                                        <div class="item form-group">
                                                                            Payments
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <button type="submit" class="btn btn-success">Save</button>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>
<?php
    /*
        * @package    ACCIT
         * @author    Paperlink Softwares Team
         * @copyright    Copyright (c) 2016 , Paperlink Softwares Pvt. Ltd.  (http://www.paperlinksoftwares.com/)
         * @since    Version 1.0.0
         * @filesource
		 * Code for all the language enrolment of the enroled student(Edit and delete links are added.)
         */
    ?>
<div role="main" class="right_col" style="min-height: 648px;">
    <div class="">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="page-title">
                <div class="title_left">
                    <h3>Language Enrollmenet</h3>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="clearfix"></div>
                    <div class="x_content">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel recent-app">
                                    <div class="x_content">
                                        <div class="student-account-page view-faculty-page">
                                            <!-- Nav tabs -->
                                            <div class="card">
                                                <div class="all-students-list add student">
                                                    <div class="add-student-section">
                                                        <div class="table-responsive">
                                                            <table id="datatable" class="display table table-striped table-bordered table data-tbl-tools" cellspacing="0" width="100%">
                                                                <thead>
                                                                    <tr class="headings">
                                                                        <th class="column-title">Student No.
                                                                        </th>
                                                                        <th class="column-title">First Name 
                                                                        </th>
                                                                        <th class="column-title">Middle Name
                                                                        </th>
                                                                        <th class="column-title">Last Name
                                                                        </th>
                                                                        <th class="column-title">Course Name 
                                                                        </th>
                                                                        <th class="column-title">Faculty
                                                                        </th>
                                                                        <th class="column-title">Start 
                                                                        </th>
                                                                        <th class="column-title">End
                                                                        </th>
                                                                        <th class="column-title">Status
                                                                        </th>
                                                                        <th class="column-title">DOB
                                                                        </th>
                                                                        <th class="column-title">Gender
                                                                        </th>
                                                                        <th class="column-title">Country
                                                                        </th>
                                                                        <th class="column-title">Agent
                                                                        </th>
                                                                        <th class="column-title">Current Attn. 
                                                                        </th>
                                                                        <th class="column-title">Overall Attn.
                                                                        </th>
                                                                        <th class="column-title">OutStanding Fees 
                                                                        </th>
                                                                        <th class="column-title">Visa Type 
                                                                        </th>
                                                                        <th class="column-title">Action
                                                                        </th>
                                                                    </tr>
                                                                </thead>
                                                                <?php $course_row=getRows('order by `ENROL_NO` DESC','enrol'); ?>
                                                                <tbody>
                                                                    <?php
                                                                        $sl=0;
                                                                        foreach($course_row as $row)
                                                                        {
                                                                        ?>
                                                                    <tr class="even pointer">
                                                                        <?php $cond="where `STUD_NO`=".$row['STUD_NO']; $std=getRows($cond,'student');?>
                                                                        <td class=" "><?=$std[0]['EXT_STN'];?></td>
                                                                        <td class=" "><?=$std[0]['FNAME'];?></td>
                                                                        <td class=" "><?=$std[0]['MNAME'];?></td>
                                                                        <td class=" "><?=$std[0]['LNAME'];?></td>
                                                                        <?php $cond="where `COURSE_NO`=".$row['COURSE_NO']; $course=getRows($cond,'course');?>
                                                                        <td class=" "><?=$course[0]['COURSE_NAME'];?></td>
                                                                        <?php $cond="where `CRT_NO`=".$row['CRT_NO']; $crt=getRows($cond,'crse_type');?>
                                                                        <td class=" "><?=$crt[0]['CRT_NAME'];?></td>
                                                                        <td class=" "><?=$row['ST_DATE'];?></td>
                                                                        <td class=" "><?=$row['END_DATE'];?></td>
                                                                        <td class=" "><?=($row['STATUS'])?'Active':'Inactive'?></td>
                                                                        <td class=" "><?=$std[0]['DOB'];?></td>
                                                                        <td class=" "><?=$std[0]['GENDER'];?></td>
                                                                        <?php $cond="where `COUNTRY_NO`=".$std[0]['COUNTRY_NO']; $cou=getRows($cond,'country');?>
                                                                        <td class=" "><?=$cou[0]['COUNTRY_NAME'];?></td>
                                                                        <?php $cond="where `AGENT_NO`=".$row['AGENT_NO']; $ag=getRows($cond,'agent');?>
                                                                        <td class=" "><?=$ag[0]['AGENT_NAME'];?></td>
                                                                        <td class=" "><?=$row['CUR_ATT'];?></td>
                                                                        <td class=" "><?=$row['OVR_ATT'];?></td>
                                                                        <td class=" "><?=$row['ENROL_NO'];?></td>
                                                                        <td class=" "><?=$row['VS_TYPE']?></td>
                                                                        <td class="last"><a href="dashboard.php?op=<?=MD5('xxx')?>&edit=<?=$row['ENROL_NO'];?>" class="color-sky stting"><i class="fa fa-cog" aria-hidden="true"></i>Edit</a>| <a href="dashboard.php?op=<?=MD5('enroll_language_details')?>&details=<?=$row['ENROL_NO'];?>&cid=<?=$row['CUSTOMER_NO'];?>&eid=<?=$row['ENROL_NO'];?>"class="color-sky stting"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>Details</a></td>
                                                                    </tr>
                                                                    <?php } ?>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
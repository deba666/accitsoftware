<?php

/*
        * @package    ACCIT
         * @author    Paperlink Softwares Team
         * @copyright    Copyright (c) 2016 , Paperlink Softwares Pvt. Ltd.  (http://www.paperlinksoftwares.com/)
         * @since    Version 1.0.0
         * @filesource
		 * Code for add new course module/Subject. Links Feth from the left sidebar under utility
*/
    if($_POST['SUBJ_CODE'])
    	{
    	if(insert_record("insert into `subject` set `SUBJ_CODE`='".$_POST['SUBJ_CODE']."',`HOURS`='".$_POST['HOURS']."',`WEEKS`='',`STATUS`='".$_POST['STATUS']."',`WHO_DATE`='".$_POST['who']."',`USER_NO`='".$_POST['user']."',`SUBJ_TYPE`='2', `SUBJ_DESC`='".$_POST['SUBJ_DESC']."',`FEE_AMOUNT`='0',`FEE_PER_MODULE`='0',`PARENT_NO`='0',`TYPE_NO`='1',`CREATE_DATE`='".$_POST['who']."',`DELETE_DATE`='',`LOCK_NUM`='1',`VET_FLAG`='".$_POST['VET_FLAG']."',`VET_CODE`='".$_POST['VET_CODE']."',`VET_NAME`='".$_POST['SUBJ_DESC']."',`VET_DELIVERY_MODE_NO`='".$_POST['VET_DELIVERY_MODE_NO']."',`VET_COMPETENCY_NO`='".$_POST['VET_COMPETENCY_NO']."',`VET_FIELD_NO`='".$_POST['VET_FIELD_NO']."',`VET_NATIONAL_CODE`='',`RESOURCE_FEE`='".$_POST['RESOURCE_FEE']."'"))
    		$_SESSION['s_msg']="<strong>Fine!</strong> module Course Added.";
    		else{
    			$_SESSION['e_msg']="<strong>Oh snap!</strong> module course not added";
    		}
    	}
    ?>
<div class="right_col" role="main">
    <div class="">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="page-title">
                <div class="title_left">
                    <?php 
                        if($_SESSION['s_msg'] || $_SESSION['e_msg'])
                        {
                        ?>
                    <div class="alert <?=($_SESSION['s_msg'])?'alert-success':'alert-error'?> fade in">
                        <button data-dismiss="alert" class="close" type="button">×</button>
                        <?=($_SESSION['s_msg'])?$_SESSION['s_msg']:$_SESSION['e_msg']?>
                    </div>
                    <?php
                        unset($_SESSION['s_msg']);
                        unset($_SESSION['e_msg']);
                        }
                        ?>
                    <h3>Add Course Module</h3>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="clearfix"></div>
                    <div class="x_content">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel recent-app">
                                    <div class="x_content">
                                        <div class="student-account-page">
                                            <!-- Nav tabs -->
                                            <div class="card">
                                                <div class="all-students-list add-course">
                                                    <div class="add-student-section">
                                                        <form class="form-horizontal form-label-left" action="" method="post" enctype="multipart/form-data">
                                                            <?php date_default_timezone_set('Australia/Sydney');
                                                                $date = date('d/m/Y h:i:s a', time());
                                                                ?>
                                                            <input id="who"  name="who"  type="hidden" value="<?php echo $date?>">
                                                            <input id="when"  name="user" type="hidden" value="<?php echo $_SESSION['user_no']?>">
                                                            <div class="item form-group">
                                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Code</label>
                                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                                    <div class="row">
                                                                        <div class="col-md-6 col-sm-4 col-xs-12">
                                                                            <input type="text" id="code" class="form-control col-md-12 col-xs-12" name="SUBJ_CODE" placeholder="Subject code" required>
                                                                        </div>
                                                                        <div class="col-md-6 col-sm-4 col-xs-12">
                                                                            <label class="pull-left">
                                                                            <input type="hidden" name="STATUS" value="0">
                                                                            <input type="checkbox" value="1" name="STATUS">
                                                                            Active
                                                                            </label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="item form-group">
                                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Name</label>
                                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                                    <div class="row">
                                                                        <div class="col-md-7 col-sm-7 col-xs-12">
                                                                            <input type="text" id="name" class="form-control col-md-12 col-xs-12" name="SUBJ_DESC" placeholder="Subject Name" required>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="item form-group">
                                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Total Hours/lebel</label>
                                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                                    <div class="row">
                                                                        <div class="col-md-4 col-sm-4 col-xs-12">
                                                                            <input type="number" step="1" min="1" max="" name="HOURS" id="quantity" value="1" title="Qty" class="form-control input-text qty text" size="4" required>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="item form-group">
                                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Subject ID</label>
                                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                                    <div class="row">
                                                                        <div class="col-md-7 col-sm-7 col-xs-12">
                                                                            <input type="text" id="VET_CODE" class="form-control col-md-12 col-xs-12" name="VET_CODE" placeholder="Subject ID" required>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="item form-group">
                                                                <label class="control-label col-md-5 col-sm-3 col-xs-12" for="name">The Intension of this Module vocational or pre-vocational:</label>
                                                                <div class="col-md-1 col-sm-9 col-xs-12">
                                                                    <div class="row">
                                                                        <div class="col-md-7 col-sm-7 col-xs-12">
                                                                            <input type="hidden" value="0" id="std_curr_attend" name="VET_FLAG" >
                                                                         <input type="checkbox" value="1" id="name" class="form-control col-md-12 col-xs-12" name="VET_FLAG">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="item form-group">
                                                                <label class="control-label col-md-5 col-sm-3 col-xs-12" for="name">This is the part of a VET school program:</label>
                                                                <div class="col-md-1 col-sm-9 col-xs-12">
                                                                    <div class="row">
                                                                        <div class="col-md-7 col-sm-7 col-xs-12">
                                                                          <input type="hidden" value="0" id="std_curr_attend" name="RESOURCE_FEE" >
                                                                         <input type="checkbox" value="1" id="name" class="form-control col-md-12 col-xs-12" name="RESOURCE_FEE">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="item form-group">
                                                                <label class="control-label col-md-5 col-sm-3 col-xs-12" for="name">This is a unit of competency in a national training package:</label>
                                                                <div class="col-md-1 col-sm-9 col-xs-12">
                                                                    <div class="row">
                                                                        <div class="col-md-7 col-sm-7 col-xs-12">
																		 <input type="hidden" value="5221" id="std_curr_attend" name="VET_COMPETENCY_NO" >
                                                                         <input type="checkbox" value="5220" id="name" class="form-control col-md-12 col-xs-12" name="VET_COMPETENCY_NO">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="item form-group">
                                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Delivery Mode:</label>
                                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                                    <div class="row">
                                                                        <div class="col-md-7 col-sm-7 col-xs-12">
                                                                           <select name="VET_DELIVERY_MODE_NO" class="col-md-12 col-xs-12 form-control" required>
																				<option Value="">Select Delivery Model</option>
																				<?php
																					$sql7="select * from system_code where `SYSTEM_TYPE_NO`=".'219 LIMIT 5';
																					$q7=mysql_query($sql7);
																					while($row7=mysql_fetch_array($q7))
																					{
																					?>
																				<option Value="<?=$row7['SYSTEM_CODE_NO']?>"><?=$row7['NAME']?></option>
																				<?php } ?>
																			</select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="item form-group">
                                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Field of Education:</label>
                                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                                    <div class="row">
                                                                        <div class="col-md-7 col-sm-7 col-xs-12">
                                                                           <select name="VET_FIELD_NO" class="col-md-12 col-xs-12 form-control" required>
																				<option Value="">Select Field of Education</option>
																				<?php
																					$sql7="select * from system_code where `SYSTEM_TYPE_NO`=".'212';
																					$q7=mysql_query($sql7);
																					while($row7=mysql_fetch_array($q7))
																					{
																					?>
																				<option Value="<?=$row7['SYSTEM_CODE_NO']?>"><?=$row7['NAME']?></option>
																				<?php } ?>
																			</select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="item form-group">
                                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name"></label>
                                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                                    <div class="row">
                                                                        <div class="col-md-6 col-sm-4 col-xs-12">
                                                                            <p class="pull-left"><br/>
                                                                                <button type="reset" value="Reset" class="btn btn-primary">Cancel</button>
                                                                                <button type="submit" class="btn btn-success">Submit</button>
                                                                            </p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
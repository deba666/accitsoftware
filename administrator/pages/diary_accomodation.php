<?php
    /*
        * @package    ACCIT
         * @author    Paperlink Softwares Team
         * @copyright    Copyright (c) 2016 , Paperlink Softwares Pvt. Ltd.  (http://www.paperlinksoftwares.com/)
         * @since    Version 1.0.0
         * @filesource
		 * Code for Display all the diary accomodation fetch from diary table. Links come from the sidebar under diary option
         */
    ?>
<div style="min-height: 648px;" class="right_col" role="main">
    <div class="">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="page-title">
                <div class="title_left">
                    <h3>Accomodation Diary</h3>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="clearfix"></div>
                    <div class="x_content">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel recent-app">
                                    <div class="x_content">
                                        <div class="student-account-page">
                                            <!-- Nav tabs -->
                                            <div class="card">
                                                <ul class="nav nav-tabs" role="tablist">
                                                    <li role="presentation" class="active"><a href="#all-student" aria-controls="all-student" role="tab" data-toggle="tab">Accomodation Diary</a></li>
                                                </ul>
                                                <div class="tab-content">
                                                    <div role="tabpanel" class="tab-pane active" id="all-student">
                                                        <div class="all-students-list">
                                                            <div class="table-responsive">
                                                                <div>
                                                                    <div>
                                                                        <table class="table data-tbl-tools" cellspacing="0" width="100%">
                                                                            <thead>
                                                                                <tr class="headings">
                                                                                    <th class="column-title">Date
                                                                                    </th>
                                                                                    <th class="column-title">Attention
                                                                                    </th>
                                                                                    <th class="column-title">Student No
                                                                                    </th>
                                                                                    <th class="column-title">First Name
                                                                                    </th>
                                                                                    <th class="column-title">Middle Name
                                                                                    </th>
                                                                                    <th class="column-title">Last Name
                                                                                    </th>
                                                                                    <th class="column-title">Subject
                                                                                    </th>
                                                                                    <th class="column-title">Note
                                                                                    </th>
                                                                                    <th class="column-title">User
                                                                                    </th>
                                                                                    <th class="column-title">When
                                                                                    </th>
                                                                                </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                                <?php $diary_sql=getRows('order by `DIARY_NO` DESC','diary'); 
                                                                                    $sl=0;
                                                                                    foreach($diary_sql as $row)
                                                                                    {?>
                                                                                <tr class="even pointer">
                                                                                    <td class=" "><?=$row['DIARY_DATE']?></td>
                                                                                    <td class=" "><?=($row['ATTENTION'])?'<input type="checkbox" checked class="flat">':'<input type="checkbox" class="flat">'?></td>
                                                                                    <?php $cond="where `CUSTOMER_NO`=".$row['CUSTOMER_NO']; $std=getRows($cond,'student');?>
                                                                                    <td class=" "><?=$std[0]['STUD_NO']?></td>
                                                                                    <td class=" "><?=$std[0]['FNAME']?></td>
                                                                                    <td class=" "><?=$std[0]['MNAME']?></td>
                                                                                    <td class=" "><?=$std[0]['LNAME']?></td>
                                                                                    <td class=" "><?=$row['SUMMARY']?></td>
                                                                                    <td class=" "><?=$row['NOTES']?></td>
                                                                                    <?php $cond="where `USER_NO`=".$row['USER_NO']; $user=getRows($cond,'users');?>
                                                                                    <td class=" "><?=$user[0]['INIT']?></td>
                                                                                    <td class="last"><?=$row['WHO_DATE']?></td>
                                                                                </tr>
                                                                                <?php } ?>                                                  
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
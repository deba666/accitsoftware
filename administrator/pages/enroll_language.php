<?php
    /*
        * @package    ACCIT
         * @author    Paperlink Softwares Team
         * @copyright    Copyright (c) 2016 , Paperlink Softwares Pvt. Ltd.  (http://www.paperlinksoftwares.com/)
         * @since    Version 1.0.0
         * @filesource
		 * Code for display all the language enrolment
		 * Links comes from sidebar(Enrolment->Language Enrolment)
         */
    ?>
<div role="main" class="right_col" style="min-height: 648px;">
    <div class="">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="page-title">
                <div class="title_left">
                    <h3>Language Enrollment</h3>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="clearfix"></div>
                    <div class="x_content">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel recent-app">
                                    <div class="x_content">
                                        <div class="student-account-page view-faculty-page">
                                            <!-- Nav tabs -->
                                            <div class="card">
                                                <div class="all-students-list add student">
                                                    <div class="add-student-section">
                                                        <div class="table-responsive">
                                                            <table class="table data-tbl-tools" cellspacing="0" width="100%">
                                                                <thead>
                                                                    <tr class="headings">
                                                                        <th class="column-title">Enrol No.
                                                                        </th>
                                                                        <th class="column-title">Student No.
                                                                        </th>
                                                                        <th class="column-title">Arrived
                                                                        </th>
                                                                        <th class="column-title">First Name 
                                                                        </th>
                                                                        <th class="column-title">Middle Name
                                                                        </th>
                                                                        <th class="column-title">Last Name
                                                                        </th>
                                                                        <th class="column-title">Course Name 
                                                                        </th>
                                                                        <th class="column-title">Faculty
                                                                        </th>
                                                                        <th class="column-title">Start 
                                                                        </th>
                                                                        <th class="column-title">End
                                                                        </th>
                                                                        <th class="column-title">Status
                                                                        </th>
                                                                        
                                                                        <th class="column-title">Agent
                                                                        </th>
                                                                        
                                                                        <th class="column-title">OutStanding Fees 
                                                                        </th>
                                                                       
                                                                        <th class="column-title">Action
                                                                        </th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <?php
                                                                        $course_row=getRows('order by `ENROL_NO` DESC','enrol');
                                                                                                            foreach($course_row as $row)
                                                                                                                  {
                                                                                                                  ?>
                                                                    <tr class="even pointer" <?php if($row['CANCELLED']==1) { ?> style="background-color: red;" <?php } ?>>
                                                                        <td class=" " <?php if($row['CANCELLED']==1) { ?> style="color: white;" <?php } ?>><?=$row['ENROL_NO'];?></td>
                                                                        <?php 
                                                                            $cond="where `STUD_NO`=".$row['STUD_NO']; $std=getRows($cond,'student');?>
                                                                        <td class=" " <?php if($row['CANCELLED']==1) { ?> style="color: white;" <?php } ?>><?=$std[0]['EXT_STN'];?></td>
                                                                        <td class=" " <?php if($row['CANCELLED']==1) { ?> style="color: white;" <?php } ?>><input type="hidden" name="ARVD" id="course_publish" value="0" >
                                                                            <input type="checkbox" name="ARVD" value="1" <?php echo ($row['ARVD'] ? 'checked' : '');?> readonly disabled />
                                                                        </td>
                                                                        <td class=" " <?php if($row['CANCELLED']==1) { ?> style="color: white;" <?php } ?>><?=$std[0]['FNAME'];?></td>
                                                                        <td class=" " <?php if($row['CANCELLED']==1) { ?> style="color: white;" <?php } ?>><?=$std[0]['MNAME'];?></td>
                                                                        <td class=" " <?php if($row['CANCELLED']==1) { ?> style="color: white;" <?php } ?>><?=$std[0]['LNAME'];?></td>
                                                                        <?php $cond="where `COURSE_NO`=".$row['COURSE_NO']; $course=getRows($cond,'course');?>
                                                                        <td class=" " <?php if($row['CANCELLED']==1) { ?> style="color: white;" <?php } ?>><?=$course[0]['COURSE_NAME'];?></td>
                                                                        <?php if($course[0]['CRT_NO']) { $cond="where `CRT_NO`=".$course[0]['CRT_NO']; $crt=getRows($cond,'crse_type'); }?>
                                                                        <td class=" " <?php if($row['CANCELLED']==1) { ?> style="color: white;" <?php } ?>><?=$crt[0]['CRT_NAME'];?></td>
                                                                        <td class=" " <?php if($row['CANCELLED']==1) { ?> style="color: white;" <?php } ?>><?=$row['ST_DATE'];?></td>
                                                                        <td class=" " <?php if($row['CANCELLED']==1) { ?> style="color: white;" <?php } ?>><?=$row['END_DATE'];?></td>
                                                                        <?php $date=date('Y-m-d'); ?>
                                                                        <td class=" " <?php if($row['CANCELLED']==1) { ?> style="color: white;" <?php } ?>><?=check_in_range($row['ST_DATE'], $row['END_DATE'], $date);?></td>
                                                                        <!--<td class=" "><?=$std[0]['DOB'];?></td>
                                                                        <td class=" "><?=$std[0]['GENDER'];?></td>
                                                                        <?php 
                                                                            if($std[0]['COUNTRY_NO']){
                                                                            $cond="where `COUNTRY_NO`=".$std[0]['COUNTRY_NO']; $cou=getRows($cond,'country');}?>
                                                                        <td class=" "><?=$cou[0]['COUNTRY_NAME'];?></td> -->
                                                                        <?php $cond="where `AGENT_NO`=".$row['AGENT_NO']; $ag=getRows($cond,'agent');?>
                                                                        <td class=" " <?php if($row['CANCELLED']==1) { ?> style="color: white;" <?php } ?>><?=$ag[0]['AGENT_NAME'];?></td>
                                                                        
                                                                        <?php
                                                                            $arr=array();
                                                                            $due_amount="where `ENROL_NO`=".$row['ENROL_NO'];
                                                                            $due_amount1=getRows($due_amount,'due_history');
                                                                            foreach($due_amount1 as $due_amount2)
                                                                             {
                                                                             if($due_amount2['DELETE_DATE']!='0')
                                                                              {
                                                                             $due_amount3="where `DUE_NO`=".$due_amount2['DUE_NO'];
                                                                                $due_amount4=getRows($due_amount3,'due');
                                                                             foreach($due_amount4 as $due_amount5)
                                                                                  {
                                                                            
                                                                             $arr[]=$due_amount5['AMOUNT'];
                                                                               
                                                                              $sum=array_sum($arr);
                                                                                   }
                                                                            	
                                                                              }
                                                                             }
                                                                            
                                                                           ?>
                                                                        <td class=" " <?php if($row['CANCELLED']==1) { ?> style="color: white;" <?php } ?>><?php if($row['CANCELLED']!=1) { echo $sum; } else { echo 'Canceled'; } ?></td>
                                                                       
                                                                        <td class="last"><a <?php if($row['CANCELLED']==1) { ?> style="color: white;" <?php } ?> href="dashboard.php?op=<?=MD5('enrolment_details')?>&eid=<?=$row['ENROL_NO'];?>" class="color-sky stting"><i class="fa fa-cog" aria-hidden="true" <?php if($row['CANCELLED']==1) { ?> style="color: white;" <?php } ?>></i>Edit</a>| <a <?php if($row['CANCELLED']==1) { ?> style="color: white;" <?php } ?> href="dashboard.php?op=<?=MD5('enrolment_details')?>&cid=<?=$row['CUSTOMER_NO'];?>&eid=<?=$row['ENROL_NO'];?>"class="color-sky stting"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>Details</a></td>
                                                                    </tr>
                                                                    <?php } 
                                                                        ?>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
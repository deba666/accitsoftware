<?php
    /*
        * @package    ACCIT
         * @author    Paperlink Softwares Team
         * @copyright    Copyright (c) 2016 , Paperlink Softwares Pvt. Ltd.  (http://www.paperlinksoftwares.com/)
         * @since    Version 1.0.0
         * @filesource
		 * Code foradd new rooms into database
		 * Link comes from sidebar(utility->rooms->add rooms)
         */

    $user_profile = mysql_fetch_array(mysql_query("SELECT * FROM `users` WHERE `USER_NO` = '".$_SESSION['user_no']."'"));
    
    if(isset($_POST['UPDATE']) && $_POST['UPDATE']==1)
    	{
       
        $msgerrors = array();
        if(mysql_num_rows(mysql_query("SELECT * FROM `users` WHERE `USER_NO` != '".$_SESSION['user_no']."' AND `EMAIL` = '".$_POST['EMAIL']."'"))>0)
        {
            $msgerrors[0]="Email already exists!";
        }
        if(mysql_num_rows(mysql_query("SELECT * FROM `users` WHERE `USER_NO` != '".$_SESSION['user_no']."' AND `USER_NAME` = '".$_POST['USER_NAME']."'"))>0)
        {
            $msgerrors[1]="Username already exists!";
        }
        
        if(count($msgerrors)==0) {
        if(count($_SESSION['msgerrors'])>0)
        {
            unset($_SESSION['msgerrors']);
           
            
        }
        if($_FILES['PROFILE_IMAGE']['name']!='')
        { 
            if($_POST['PROFILE_IMAGE']!='')
            {
                @unlink('upload/'.$_POST['PROFILE_IMAGE']);
            }
            if(strtolower($_FILES['PROFILE_IMAGE']['type'])=='image/jpeg' || strtolower($_FILES['PROFILE_IMAGE']['type'])=='image/jpg' || strtolower($_FILES['PROFILE_IMAGE']['type'])=='image/png' || strtolower($_FILES['PROFILE_IMAGE']['type'])=='image/gif')
            {
                $info = new SplFileInfo($_FILES['PROFILE_IMAGE']['name']);               
                $ext = $info->getExtension(); 
                $new_file = date("Ymdhis").".".$ext;
                move_uploaded_file($_FILES['PROFILE_IMAGE']['tmp_name'],'upload/'.$new_file);
            }
        }    
        else
        {
            $new_file = $_POST['PROFILE_IMAGE'];
        }
        if(mysql_query("UPDATE `users` SET `USER_NAME` = '".$_POST['USER_NAME']."' , `PWORD` = '".$_POST['PWORD']."' , `FNAME` = '".$_POST['FNAME']."' , `LNAME` = '".$_POST['LNAME']."' , `EMAIL` = '".$_POST['EMAIL']."' , `LOCATION_NO` = '".$_POST['LOCATION_NO']."' , `PROFILE_IMAGE` = '".$new_file."' WHERE `USER_NO` = '".$_SESSION['user_no']."'"))
        { 
            
            $_SESSION['s_msg']="<strong>Fine!</strong> Profile Updated!"; 
            ?>
<script>
window.location.href="dashboard.php?op=<?=MD5('edit_profile')?>";
</script>
<?php
            
        }
        else{
                $_SESSION['s_msg']="<strong>Oh snap!</strong> Profile is not Updated";
                ?>
<script>
window.location.href="dashboard.php?op=<?=MD5('edit_profile')?>";
</script>
<?php
        }
    	}
        else
        {
            
            $_SESSION['s_msg']="";
            $_SESSION['msgerrors']=$msgerrors;
            ?>
<script>
window.location.href="dashboard.php?op=<?=MD5('edit_profile')?>";
</script>
<?php
        }
        }
        
    ?>
<div class="right_col" role="main">
    <div class="">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="page-title">
                <div class="title_left">
                    <?php 
                        if($_SESSION['s_msg']!='')
                        {
                        ?>
                    <div class="alert <?=($_SESSION['s_msg'])?'alert-success':'alert-error'?> fade in">
                        <button data-dismiss="alert" class="close" type="button">×</button>
                        <?=$_SESSION['s_msg']?>
                    </div>
                    <?php
                       // unset($_SESSION['s_msg']);
                       //  unset($_SESSION['e_msg']);
                        
                    
                        }
                        
                        ?>
                    <h3>Edit Profile</h3>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="clearfix"></div>
                    <div class="x_content">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel recent-app">
                                    <div class="x_content">
                                        <div class="student-account-page edit-faculty-page">
                                            <?php
                                            if(count($_SESSION['msgerrors'])>0)
                                            {
                                               foreach($_SESSION['msgerrors'] as $msg)
                                               {
                                                 ?>  
                                            <div style="color: red;"><strong><?=$msg?></strong></div>
                                            <br/>
                                            <?php
                                               }
                                            }
                                            ?>
                                            <!-- Nav tabs -->
                                            <form method="post" action="" name="f1" id="f1" class="form-horizontal form-label-left" enctype="multipart/form-data">
                                            <div class="card">
                                                <div class="all-students-list add student">
                                                    <div class="add-student-section">
                                                        
                                                            <div class="item form-group">
                                                                <label for="name" class="control-label col-md-3 col-sm-3 col-xs-12">First Name</label>
                                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                                    <div class="row">
                                                                        <div class="col-md-7 col-sm-7 col-xs-12">
                                                                            <input type="text" required="" placeholder="" name="FNAME" class="form-control col-md-12 col-xs-12" id="FNAME" required value="<?=$user_profile['FNAME']?>">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="item form-group">
                                                                
                                                                <label for="name" class="control-label col-md-3 col-sm-3 col-xs-12">Last Name</label>
                                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                                    <div class="row">
                                                                        <div class="col-md-7 col-sm-7 col-xs-12">
                                                                            <input type="text" required="" placeholder="" name="LNAME" class="form-control col-md-12 col-xs-12" id="LNAME" required value="<?=$user_profile['LNAME']?>">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            
                                                           
                                                             <div class="item form-group">
                                                                
                                                                <label for="name" class="control-label col-md-3 col-sm-3 col-xs-12">Email</label>
                                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                                    <div class="row">
                                                                        <div class="col-md-7 col-sm-7 col-xs-12">
                                                                            <input type="text" required="" placeholder="" name="EMAIL" class="form-control col-md-12 col-xs-12" id="EMAIL" required value="<?=$user_profile['EMAIL']?>">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="item form-group">
                                                                
                                                                <label for="name" class="control-label col-md-3 col-sm-3 col-xs-12">Username</label>
                                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                                    <div class="row">
                                                                        <div class="col-md-7 col-sm-7 col-xs-12">
                                                                            <input type="text" required="" placeholder="" name="USER_NAME" class="form-control col-md-12 col-xs-12" id="USER_NAME" required value="<?=$user_profile['USER_NAME']?>">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="item form-group">
                                                                
                                                                <label for="name" class="control-label col-md-3 col-sm-3 col-xs-12">Password</label>
                                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                                    <div class="row">
                                                                        <div class="col-md-7 col-sm-7 col-xs-12">
                                                                            <input type="password" required="" placeholder="" name="PWORD" class="form-control col-md-12 col-xs-12" id="PWORD" required value="<?=$user_profile['PWORD']?>">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="item form-group">
                                                                <label for="name" class="control-label col-md-3 col-sm-3 col-xs-12">Location</label>
                                                                <div class="col-md-7 col-sm-7 col-xs-12">
                                                                    <div class="row">
                                                                        <div class="col-md-6 col-sm-4 col-xs-12">
                                                                            <select class="form-control" name="LOCATION_NO" id="LOCATION_NO">
                                                                                <option value="1">SYDNEY</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        <div class="item form-group">
                                                                <label for="name" class="control-label col-md-3 col-sm-3 col-xs-12">Profile Photo</label>
                                                                <div class="col-md-7 col-sm-7 col-xs-12">
                                                                    <div class="row">
                                                                        <div class="col-md-6 col-sm-4 col-xs-12">
                                                                            <input type="file" name="PROFILE_IMAGE" id="PROFILE_IMAGE" value="" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                         <div class="item form-group">
                                                                <label for="name" class="control-label col-md-3 col-sm-3 col-xs-12">Profile Photo</label>
                                                                <div class="col-md-7 col-sm-7 col-xs-12">
                                                                    <div class="row">
                                                                        <div class="col-md-6 col-sm-4 col-xs-12">
                                                                            <?php if($user_profile['PROFILE_IMAGE']!='') { ?>
                                                                            <img src="upload/<?=$user_profile['PROFILE_IMAGE']?>" border="0" height="100" width="100" />
                                                                            <?php } else { ?>
                                                                            <img src="images/avatar.png" border="0" height="100" width="100" />
                                                                            <?php } ?>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="item form-group">
                                                                <label for="name" class="control-label col-md-3 col-sm-3 col-xs-12"></label>
                                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                                    <div class="row">
                                                                        
                                                                           
                                                                               <br/> 
                                                                                <button class="btn btn-success" type="submit">Submit</button>
                                                                       
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <input type="hidden" name="UPDATE" id="UPDATE" value="1" />
                                                        <input type="hidden" name="PROFILE_IMAGE" id="PROFILE_IMAGE" value="<?=$user_profile['PROFILE_IMAGE']?>" />
                                                    </div>
                                                </div>
                                            </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
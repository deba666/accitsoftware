<?php
    /*
        * @package    ACCIT
         * @author    Paperlink Softwares Team
         * @copyright    Copyright (c) 2016 , Paperlink Softwares Pvt. Ltd.  (http://www.paperlinksoftwares.com/)
         * @since    Version 1.0.0
         * @filesource
		 * code for display what type of debit give to the student
		 * Comes under Enrol details->fees->debit.
         */
    ?>
<div role="main" class="right_col" style="min-height: 648px;">
    <div class="">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="page-title">
                <div class="title_left">
                    <h3>All Debit</h3>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="clearfix"></div>
                    <div class="x_content">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel recent-app">
                                    <div class="x_content">
                                        <div class="student-account-page">
                                            <!-- Nav tabs -->
                                            <div class="card">
                                                <ul role="tablist" class="nav nav-tabs">
                                                    <li class="active" role="presentation"><a data-toggle="tab" role="tab" aria-controls="all-student" href="#all-student">Debit</a></li>
                                                </ul>
                                                <div class="tab-content">
                                                    <div id="all-student" class="tab-pane active" role="tabpanel">
                                                        <div class="all-students-list">
                                                            <div class="table-responsive">
                                                                <div  class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                                                                    <div class="dataTables_wrapper form-inline dt-bootstrap no-footer" id="datatable-buttons_wrapper">
                                                                        <div  class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                                                                            <div style="font-size:20px; text-align:center"><a class="btn btn-success" href="" onclick="javascript:void window.open('dashboard.php?op=<?=MD5('enroll_debit_transfer')?>&enrol_no=<?=$_GET['enrol_no']?>&stu_no=<?php echo $_GET['stu_no']?>&cust_no=<?php echo $_GET['cust_no']?>','14708748621367','width=800,height=550,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=0,top=0');return false;">Transfar</a></div>
                                                                            <div style="font-size:20px; text-align:center"><a class="btn btn-success" href="" onclick="javascript:void window.open('dashboard.php?op=<?=MD5('enroll_debit_notice')?>&enrol_no=<?=$_GET['enrol_no']?>&stu_no=<?php echo $_GET['stu_no']?>&cust_no=<?php echo $_GET['cust_no']?>','144546548621367','width=800,height=550,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=0,top=0');return false;">Refund To Student</a></div>
                                                                            <div style="font-size:20px; text-align:center"><a class="btn btn-success" href="" onclick="javascript:void window.open('dashboard.php?op=<?=MD5('enroll_debit_notice_1')?>&enrol_no=<?=$_GET['enrol_no']?>&stu_no=<?php echo $_GET['stu_no']?>&cust_no=<?php echo $_GET['cust_no']?>','144546548621367','width=800,height=550,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=0,top=0');return false;">Refunt To Agent</a></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
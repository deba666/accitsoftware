<?php
    /*
        * @package    ACCIT
         * @author    Paperlink Softwares Team
         * @copyright    Copyright (c) 2016 , Paperlink Softwares Pvt. Ltd.  (http://www.paperlinksoftwares.com/)
         * @since    Version 1.0.0
         * @filesource
         */
    ?>
<div role="main" class="right_col" style="min-height: 648px;">
    <div class="">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="page-title">
                <div class="title_left">
                    <h3>All Agents</h3>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="clearfix"></div>
                    <div class="x_content">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel recent-app">
                                    <div class="x_content">
                                        <div class="student-account-page">
                                            <!-- Nav tabs -->
                                            <div class="card">
                                                <ul role="tablist" class="nav nav-tabs">
                                                    <li class="active" role="presentation"><a data-toggle="tab" role="tab" aria-controls="all-student" href="#all-student">Agent</a></li>
                                                </ul>
                                                <div class="tab-content">
                                                    <div id="all-student" class="tab-pane active" role="tabpanel">
                                                        <div class="all-students-list">
                                                            <div class="table-responsive">
                                                                <div id="datatable-buttons_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                                                                    <div class="dataTables_wrapper form-inline dt-bootstrap no-footer" id="datatable-buttons_wrapper">
                                                                        <div id="datatable-buttons_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                                                                            <table class="table table-striped jambo_table bulk_action dataTable no-footer dtr-inline table data-tbl-tools" id="datatable-buttons" role="grid" aria-describedby="datatable-buttons_info">
                                                                                <thead>
                                                                                    <tr class="headings">
                                                                                        <th>Code</th>
                                                                                        <th>Name</th>
                                                                                        <th>Country</th>
                                                                                        <th>Select</th>
                                                                                    </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                    <?php
                                                                                        $sql="select * from  agent order by AGENT_NAME ASC";
                                                                                        $q=mysql_query($sql);
                                                                                        while($row=mysql_fetch_array($q))
                                                                                        {
                                                                                          $sql1="select COUNTRY_NAME from `country` where `COUNTRY_NO`='$row[COUNTRY_NO]'";
                                                                                          $res1=mysql_query($sql1);
                                                                                          $row1=mysql_fetch_array($res1);
                                                                                          
                                                                                        ?>
                                                                                    <tr class="pointer odd" role="row">
                                                                                        <td><?=$row['AGENT_CODE'];?></td>
                                                                                        <td><?=$row['AGENT_NAME'];?></td>
                                                                                        <td ><?=$row1['COUNTRY_NAME'];?></td>
                                                                                        <td class=" " style="width: 465px !important;"><a href="#" onclick="javascript:void window.open('dashboard.php?op=<?=MD5('display_new_tour')?>&TOUR_NAME=<?=$_GET['TOUR_NAME']?>&STATUS=<?=$_GET['STATUS']?>&STUDENTS=<?=$_GET['STUDENTS']?>&COUNTRY_NAME=<?=$_GET['COUNTRY_NAME']?>&START=<?=$_GET['START']?>&FINISH=<?=$_GET['FINISH']?>&LOCATION_NO=<?=$_GET['LOCATION_NO']?>&COURSE_NAME=<?=$_GET['COURSE_NAME']?>&C_START=<?=$_GET['C_START']?>&C_FINISH=<?=$_GET['C_FINISH']?>&ACCOMODATION=<?=$_GET['ACCOMODATION']?>&IN_DATE=<?=$_GET['IN_DATE']?>&OUT_DATE=<?=$_GET['OUT_DATE']?>&NOTES=<?=$_GET['NOTES']?>&ag_no=<?=$row['AGENT_NO']?>&aname=<?=$row['AGENT_NAME'];?>','147013847777','width=800,height=600,toolbar=0,menubar=0,location=1,status=1,scrollbars=1,resizable=1,left=0,top=0');self.close();return false;">Add</a></td>
                                                                                        <?php 
                                                                                            }?>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
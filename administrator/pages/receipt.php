<?php
    /*
        * @package    ACCIT
         * @author    Paperlink Softwares Team
         * @copyright    Copyright (c) 2016 , Paperlink Softwares Pvt. Ltd.  (http://www.paperlinksoftwares.com/)
         * @since    Version 1.0.0
         * @filesource
		 * Code for receipt wizard 
		 * Link Comes from enrolment details page(Fees->receipt)
		 * The data are send to next page(receipt_payment.php) using POST
         */
    ?>
<div class="col-md-12 col-sm-12 col-xs-12">
    <div class="page-title">
        <div class="title_left">
            <!--	 -->
            <h3>Receipt Wizard</h3>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="clearfix"></div>
            <div class="x_content">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel recent-app">
                            <div class="x_content">
                                <div class="student-account-page edit-faculty-page">
                                    <!-- Nav tabs -->
                                    <div class="card">
                                        <div class="all-students-list add student">
                                            <div class="add-student-section">
                                                <form method="POST" action="dashboard.php?op=<?=MD5('receipt_payment')?>" class="form-horizontal form-label-left">
                                                    <input name="ENROL_NO"  value="<?=$_SESSION['enrol_no']=$_GET['enrol_no'];?>"class="form-control col-md-12 col-xs-12" id="name" type="hidden">
                                                    <input name="OFFER_ITEM_NO"  value="<?=$_SESSION['offer_item_no']=$_GET['offer_item_no'];?>"class="form-control col-md-12 col-xs-12" id="name" type="hidden">
													
                                                    <input name="cust_no"  value="<?=$_SESSION['cust_no']=$_GET['cust_no'];?>" class="form-control col-md-12 col-xs-12" id="name" type="hidden">
                                                    <?php date_default_timezone_set('Australia/Melbourne');
                                                        $date = date('d/m/Y h:i:s a', time());
                                                        ?>
                                                    <input id="who"  name="WHO_DATE"  type="hidden" value="<?php echo $date; ?>">
                                                    <input id="when"  name="user" type="hidden" value="<?php echo $_SESSION['user_no']; ?>">
                                                    <div class="row">
                                                        <div class="col-md-2 col-sm-2 col-xs-2">
                                                            <div class="form-group">
                                                                <label class="pull-left control-label col-md-2 col-sm-2 col-xs-2" for="name">Date</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2 col-sm-2 col-xs-2">
                                                            <div class="form-group">
                                                                <?php if($_GET['ref_date']!='') { $_SESSION['ref_date']=$_GET['ref_date']; } else { $_SESSION['ref_date']=date('Y-m-d',time()); } ?>
                                                                <input value="<?=$_SESSION['ref_date']?>"  name="RECEIPT_DATE" type="date">
                                                            </div>
                                                        </div>
                                                        
                                                    </div>
                                                    <?=$_SESSION['BANK_NO']=$_GET['BANK_NO']?>
                                                    <div class="item form-group">
                                                        <label for="name" class="control-label col-md-2 col-sm-2 col-xs-2">Bank Account</label>
                                                        <div class="col-md-8 col-sm-8 col-xs-8">
                                                            <div class="row">
                                                                <div class="col-md-7 col-sm-7 col-xs-12">
                                                                    <select name="BANK_NO" class="form-control">
                                                                        <?php
                                                                            $sql="select * from bank_account";
                                                                            $q=mysql_query($sql);
                                                                            while($r=mysql_fetch_array($q))
                                                                            {
                                                                            ?>
                                                                        <option value="<?=$r['BANK_NO']?>" <?php if($_SESSION['BANK_NO']==$r['BANK_NO']) { ?> selected <?php } ?>><?=$r['BANK_NAME']?></option>
                                                                        <?php } ?>	
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="item form-group">
                                                        <label for="name" class="control-label col-md-2 col-sm-2 col-xs-2">Reference No</label>
                                                        <div class="col-md-8 col-sm-8 col-xs-8">
                                                            <div class="row">
                                                                <div class="col-md-7 col-sm-7 col-xs-12">
                                                                    <input name="REFERENCE_NO"  value="<?=$_SESSION['ref_no']=$_GET['ref_no'];?>"class="form-control col-md-12 col-xs-12" id="name" type="text" required>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="item form-group">
                                                        <label for="name" class="control-label col-md-3 col-sm-3 col-xs-12"></label>
                                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                                            <div class="row">
                                                                <div class="col-md-6 col-sm-4 col-xs-12">
                                                                    <p class="pull-left">
                                                                        <button type="submit" class="btn btn-success">Next</button>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
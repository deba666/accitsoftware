<?php
    /*
        * @package    ACCIT
         * @author    Paperlink Softwares Team
         * @copyright    Copyright (c) 2016 , Paperlink Softwares Pvt. Ltd.  (http://www.paperlinksoftwares.com/)
         * @since    Version 1.0.0
         * @filesource
		  * Code for display all the fees amount which are not paid in debit field
         */
    ?>
<div role="main" class="right_col" style="min-height: 648px;">
    <div class="">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="page-title">
                <div class="title_left">
                    <h3>All Debit</h3>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="clearfix"></div>
                    <div class="x_content">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel recent-app">
                                    <div class="x_content">
                                        <div class="student-account-page">
                                            <!-- Nav tabs -->
                                            <div class="card">
                                                <div class="all-students-list add student">
                                                    <div class="add-student-section">
                                                        <div class="table-responsive">
                                                            <table  class="display table table-striped table-bordered" cellspacing="0" width="100%">
                                                                <thead>
                                                                    <tr class="headings">
                                                                        <th class="column-title">Invoice No.
                                                                        </th>
                                                                        <th class="column-title">Due Date
                                                                        </th>
                                                                        <th class="column-title">Fee 
                                                                        </th>
                                                                        <th class="column-title">Debit
                                                                        </th>
                                                                        <th class="column-title">Operation
                                                                        </th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <?php
                                                                        $con="where `ENROL_NO`=".$_GET['enrol_no'];
                                                                        $stud_row=getRows($con,'invoice');
                                                                         foreach($stud_row as $stud_in)
                                                                              {
                                                                        $cond="where `INVOICE_NO`=".$stud_in['INVOICE_NO'];
                                                                        $stud_rowd=getRows($cond,'invoice_details');
                                                                         foreach($stud_rowd as $stud_ind) 
                                                                        {
                                                                        
                                                                        
                                                                        ?>
                                                                    <tr class="even pointer">
                                                                        <?php
                                                                        $add1a=mysql_query("SELECT * from credit where `IND_NO`=".$stud_ind['IND_NO']);
																		while($row=mysql_fetch_array($add1a))
																		{
																		
                                                                            ?>
                                                                        <td class=" "><?=$stud_ind['INVOICE_NO']?></td>
                                                                        <td class=" "><?php echo date('d/m/Y',strtotime($stud_in['DUE_DATE']));?></td>
                                                                        <td class=" ">
                                                                         <?php
																			if($stud_ind['OFFER_INSTALMENT_NO']){																										                                                                             $cond202="where `OFFER_INSTALMENT_NO`=".$stud_ind['OFFER_INSTALMENT_NO'];
																																													 																			$std_row202=getRows($cond202,'offer_instalment'); }
																		if($std_row202[0]['OFFER_ITEM_NO']){																										 																			$cond_off_it="where `OFFER_ITEM_NO`=".$std_row202[0]['OFFER_ITEM_NO'];
																																												 																						 																				$std_row_off_it=getRows($cond_off_it,'offer_item');}
																																											  
																	if($std_row_off_it[0]['PRODUCT_NO']){																										 																			$cond_off_it_pr="where `PRODUCT_NO`=".$std_row_off_it[0]['PRODUCT_NO'];
																																											    	                                                                             $std_row_off_it_pr=getRows($cond_off_it_pr,'product');}
																			if($stud_ind['FEE_NO']=='76')
																			  {
																				$name="Agent Fee";
																			  }
																			  else
																			  {
																				$name=$std_row_off_it_pr[0]['NAME'];
																			  }
																			  ?>
                                                                            <?php echo $name;?>
                                                                                </td> 
                                                                        <td class=" "><?=$row['AMOUNT']?></td>
                                                                        <td class=" "><a class="btn btn-success" href= "" onclick="javascript:void window.open('dashboard.php?op=<?=MD5('enroll_debit_operation')?>&credit_no=<?=$row['CREDIT_NO']?>&ind_no=<?=$row['IND_NO']?>&invoice_no=<?=$row['INVOICE_NO']?>&enrol_no=<?=$stud_ind['ENROL_NO']?>&fee_no=<?=$stud_ind['FEE_NO']?>&fee_def_no=<?=$stud_ind['FEE_DEF_NO']?>&amount=<?=$row['AMOUNT']?>','1470138621367','width=700,height=450,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=0,top=0');return false;">Amount</a></td>
                                                                    </tr>
                                                                    <?php } } }  ?>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
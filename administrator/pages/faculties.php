<?php
    /*
        * @package    ACCIT
         * @author    Paperlink Softwares Team
         * @copyright    Copyright (c) 2016 , Paperlink Softwares Pvt. Ltd.  (http://www.paperlinksoftwares.com/)
         * @since    Version 1.0.0
         * @filesource
		 * Display all the faculties from database
		 * Edit or delete faculty if faculty exist
		 * Holiday list link placed
         */
    ?>
<div class="right_col" role="main">
    <?php 
    $faculty_sql=getRows('order by `CRT_NO` DESC','crse_type'); 
    
        if($_GET['del'])
        {
        	if(delete_record("DELETE FROM `crse_type` WHERE `CRT_NO` = ".$_GET['del']))
        	{
        		$_SESSION['s_msg']="Item successfully deleted";
                        ?>
                        <script> 
                            window.location.href="http://<?php echo $_SERVER['HTTP_HOST']; ?>/accitsoftware/administrator/dashboard.php?op=<?php echo MD5('faculties'); ?>"; 
                        </script>
                        <?php
        	}
        }?>
    <div class="">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="page-title">
                <div class="title_left">
                    <?php 
                        if($_SESSION['s_msg'] || $_SESSION['e_msg'])
                        {
                        ?>
                    <div class="alert <?=($_SESSION['s_msg'])?'alert-success':'alert-error'?> fade in">
                        <button data-dismiss="alert" class="close" type="button">�</button>
                        <?=($_SESSION['s_msg'])?$_SESSION['s_msg']:$_SESSION['e_msg']?>
                    </div>
                    <?php
                        unset($_SESSION['s_msg']);
                        unset($_SESSION['e_msg']);
                        }
                        ?>
                    <h3>View Faculty 
                        <!-- <a href="#" onclick="javascript:void window.open('dashboard.php?op=<?=MD5('holidays')?>&s_no=<?=$std_row6[0]['EXT_STN']?>&s_fname=<?=$std_row6[0]['FNAME']?>&s_lname=<?=$std_row6[0]['LNAME']?>&s_mname=<?=$std_row6[0]['MNAME']?>&ag_name=<?=$std_row[0]['AGENT_NAME']?>&cus_no=<?=$std_row[0]['CUSTOMER_NO']?>','147784165551367','width=700,height=500,toolbar=0,menubar=0,location=1,status=1,scrollbars=1,resizable=1,left=0,top=0');return false;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Holiday List</a> -->
                    </h3>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="clearfix"></div>
                    <div class="x_content">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel recent-app">
                                    <div class="x_content">
                                        <div class="student-account-page view-faculty-page">
                                            <!-- Nav tabs -->
                                             <?php if(count($faculty_sql) > 0) { ?>
                                            <div class="card">
                                                <div class="all-students-list add student">
                                                    <div class="add-student-section">
                                                        <div class="table-responsive">
                                                            <table class="display table table-striped table-bordered table data-tbl-tools" cellspacing="0" width="100%">
                                                                <thead>
                                                                    <tr class="headings">
                                                                        <th class="column-title">Name
                                                                        </th>
                                                                        <th class="column-title">Type
                                                                        </th>
                                                                        <th class="column-title">Study Option
                                                                        </th>
                                                                        <th class="column-title">Location
                                                                        </th>
                                                                        <th class="column-title">Active
                                                                        </th>
                                                                        <th class="column-title">Provider Code
                                                                        </th>
                                                                       
                                                                        <th class="column-title">Action
                                                                        </th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <?php 
                                                                        $sl=0;
                                                                        foreach($faculty_sql as $row)
                                                                        {?>
                                                                    <tr class="even pointer">
                                                                        <td class=" "><?=$row['CRT_NAME'];?></td>
                                                                        <td class=" ">
                                                                            <?php
                                                                                $crt = $row['CRT_TYPE'];
                                                                                
                                                                                switch ($crt) {
                                                                                	case "1":
                                                                                		echo "Language";
                                                                                		break;
                                                                                	case "2":
                                                                                		echo "Tour Group";
                                                                                		break;
                                                                                	case "3":
                                                                                		echo "Module Lession";
                                                                                		break;
                                                                                		case "4":
                                                                                		echo "All";
                                                                                		break;
                                                                                	default:
                                                                                		echo "None";
                                                                                }
                                                                                ?> 
                                                                        </td>
                                                                        <td class=" ">
                                                                            <?php
                                                                                $week = $row['COURSE_STUDY_TYPE'];
                                                                                
                                                                                switch ($week) {
                                                                                	case "0":
                                                                                		echo "Calander Week";
                                                                                		break;
                                                                                	case "1":
                                                                                		echo "Semester";
                                                                                		break;
                                                                                	default:
                                                                                		echo "None";
                                                                                }
                                                                                ?>
                                                                        </td>
                                                                        <?php $cond="where `LOCATION_NO`=".$row['LOCATION_NO']; $loc=getRows($cond,'location');?>
                                                                        <td class=" "><?=$loc[0]['LOCATION_NAME'];?> </td>
                                                                        <td class=" "><?=($row['STATUS'])?'<input type="checkbox" checked class="flat" disabled="disabled">':'<input type="checkbox" class="flat" disabled="disabled">'?></td>
                                                                        <td class=" "><?=$row['PROVIDER_CODE'];?></td>
                                                                       
                                                                        <td class="last"><a href="dashboard.php?op=<?=MD5('faculties_edit')?>&fid=<?=$row['CRT_NO'];?>" class="color-sky stting"><i class="fa fa-cog" aria-hidden="true"></i>Edit</a>| <a href="dashboard.php?op=<?=MD5('faculties')?>&del=<?=$row['CRT_NO'];?>" onclick="return confirm('Are you sure?'); return false;" class="color-sky stting"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>Delete</a></td>
                                                                    </tr>
                                                                    <?php } ?>                                                  
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                             <?php } else { ?>No records found! <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
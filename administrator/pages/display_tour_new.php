<?php
    /*
        * @package    ACCIT
         * @author    Paperlink Softwares Team
         * @copyright    Copyright (c) 2016 , Paperlink Softwares Pvt. Ltd.  (http://www.paperlinksoftwares.com/)
         * @since    Version 1.0.0
         * @filesource
		 * Code for New tour group. Link comes from sidebar
         */
    
       if($_POST['TOUR_NAME'])
       {
       	insert_record("insert into `tour_group` set TOUR_NAME='".$_POST['TOUR_NAME']."',STATUS='".$_POST['STATUS']."',STUDENTS='".$_POST['STUDENTS']."',COUNTRY_NAME='".$_POST['COUNTRY_NAME']."',START='".$_POST['START']."',AGENT_NO='".$_POST['AGENT_NO']."',FINISH='".$_POST['FINISH']."',LOCATION_NO='".$_POST['LOCATION_NO']."',COURSE_NAME='".$_POST['COURSE_NAME']."',C_START='".$_POST['C_START']."',C_FINISH='".$_POST['C_FINISH']."',ACCOMODATION='".$_POST['ACCOMODATION']."',IN_DATE='".$_POST['IN_DATE']."',OUT_DATE='".$_POST['OUT_DATE']."',NOTES='".$_POST['NOTES']."',USER_NO='".$_POST['USER_NO']."',WHO_DATE='".$_POST['WHO_DATE']."'");
    	$_SESSION['s_msg']="<strong>Fine!</strong> New Tour Added Successfully";
    	}
       	else{
       			$_SESSION['e_msg']="<strong>Oh snap!</strong>Tour is not added";
    	
    	}
    ?><head>
    <script language="javascript" type="text/javascript" src="datetimepicker.js"></script>
</head>

<div class="right_col" role="main">
<div class="">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="page-title">
            <div class="title_left">
                <?php 
                    if($_SESSION['s_msg'] || $_SESSION['e_msg'])
                    {
                    ?>
                <div class="alert <?=($_SESSION['s_msg'])?'alert-success':'alert-error'?> fade in">
                    <button data-dismiss="alert" class="close" type="button">�</button>
                    <?=($_SESSION['s_msg'])?$_SESSION['s_msg']:$_SESSION['e_msg']?>
                </div>
                <?php
                    unset($_SESSION['s_msg']);
                    unset($_SESSION['e_msg']);
                    }
                    ?>
                <h3>Add Student(New)</h3>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="clearfix"></div>
                <div class="x_content">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel recent-app">
                                <div class="x_content">
                                    <div class="student-account-page">
                                        <!-- Nav tabs -->
                                        <div class="card">
                                            <div class="all-students-list add-course">
                                                <div class="add-student-section">
                                                    <form class="form-horizontal form-label-left" action="" method="post" novalidate>
                                                        <ul id="student-acc-tab" class="nav nav-tabs" role="tablist">
                                                            <li role="presentation"  class="active"><a href="#detailst" aria-controls="detailst" role="tab" data-toggle="tab">Details</a></li>
                                                        </ul>
                                                        <div class="tab-content">
                                                            <!--Details-->
                                                            <div role="tabpanel" class="tab-pane active" id="detailst">
                                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                                    <div class="row">
                                                                        <h4>
                                                                            <fieldset>Basic Info</fieldset>
                                                                        </h4>
                                                                        <div class="col-md-4 col-sm-4 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label class="control-label col-md-12 col-sm-12 col-xs-12" for="name">Tour Name
                                                                                </label>
                                                                                <input value="<?=$std_row[0]['TOUR_NAME']?>" name="TOUR_NAME" class="form-control col-md-12 col-sm-12 col-xs-12" placeholder="Tour Name" type="text">
                                                                            </div>
                                                                        </div>
                                                                        <input id="when"  name="USER_NO" type="hidden" value="<?php echo $_SESSION['user_no']?>">
                                                                        <?php date_default_timezone_set('Australia/sydney');
                                                                            $date = date('d/m/Y h:i:s a', time());
                                                                            ?>
                                                                        <input id="who"  name="WHO_DATE"  type="hidden" value="<?php echo $date?>">
                                                                        <div class="col-md-4 col-sm-4 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label class="control-label col-md-12 col-sm-12 col-xs-12" for="name">Status
                                                                                </label>
                                                                                <input value="<?=$std_row[0]['STATUS']?>" name="STATUS" class="form-control col-md-12 col-sm-12 col-xs-12" placeholder="Status" type="text">
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-4 col-sm-4 col-xs-12">
                                                                            <div class="item form-group">
                                                                                <label class="control-label col-md-12 col-sm-12 col-xs-12">Students
                                                                                </label>
                                                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                                                    <input value="<?=$std_row[0]['STUDENTS']?>" class="form-control" type="number" step="1" min="0" max="" name="STUDENTS" id="course_hours_week" value="0" title="Qty" size="4">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                                                            <label class="pull-left control-label col-md-12 col-sm-12 col-xs-12" for="name">Country
                                                                            </label>
                                                                            <select name="COUNTRY_NAME" class="col-md-12 col-xs-12 form-control">
                                                                                <option Value='0'>Select Country</option>
                                                                                <?php
                                                                                    $dd_res1=mysql_query("Select *  from country ORDER BY `COUNTRY_NAME` ASC");
                                                                                    while($r1=mysql_fetch_array($dd_res1))
                                                                                     { 
                                                                                    ?>
                                                                                <option value="<?=$r1['COUNTRY_NO']?>"><?= $r1['COUNTRY_NAME'] ?></option>
                                                                                <?php		  
                                                                                    }
                                                                                    ?>
                                                                            </select>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                                                <div class="form-group">
                                                                                    <label class="pull-left control-label col-md-12 col-sm-12 col-xs-12" for="name">Start</label>
                                                                                    <input value="<?php echo date("Y-m-d");?>" class="form-control col-md-12 col-sm-12 col-xs-12" name="START" placeholder="MM/DD/YYYY" type="date">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                                                <div class="form-group">
                                                                                    <label for="name" class="pull-left control-label col-md-6 col-sm-6 col-xs-12">Agent</label>
                                                                                    <select name="AGENT_NO" class="col-md-12 col-xs-12 form-control">
                                                                                        <option Value="0">Select Agent</option>
                                                                                        <?php
                                                                                            $dd_res=mysql_query("Select *  from agent ORDER BY `AGENT_NAME` ASC");
                                                                                            while($r=mysql_fetch_array($dd_res))
                                                                                              { 
                                                                                            	  ?>
                                                                                        <option Value="<?=$r['AGENT_NO']?>"><?=$r['AGENT_NAME']?></option>
                                                                                        <?php
                                                                                            }
                                                                                             ?>
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                                                    <div class="form-group">
                                                                                        <label class="pull-left control-label col-md-12 col-sm-12 col-xs-12" for="name">Finish</label>
                                                                                        <input class="form-control col-md-12 col-sm-12 col-xs-12" name="FINISH" placeholder="MM/DD/YYYY" type="date" value="<?php echo date("Y-m-d");?>">
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                                                <div class="form-group">
                                                                                    <label class="control-label col-md-12 col-sm-12 col-xs-12" for="name">Location
                                                                                    </label>
                                                                                    <select name="LOCATION_NO" class="col-md-12 col-xs-12 form-control">
                                                                                        <option Value="0"></option>
                                                                                        <option Value="1">SYDNEY</option>
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                                                    <h4>
                                                                                        <fieldset>Course</fieldset>
                                                                                    </h4>
                                                                                    <div class="form-group">
                                                                                        <label class="control-label col-md-12 col-sm-12 col-xs-12" for="name">Course
                                                                                        </label>
                                                                                        <input name="COURSE_NAME" type="text" placeholder="Course" class="col-md-12 col-xs-12 form-control">
                                                                                    </div>
                                                                                    <div class="row">
                                                                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                                                                            <div class="form-group">
                                                                                                <label class="pull-left control-label col-md-12 col-sm-12 col-xs-12" for="name">Start</label>
                                                                                                <input class="form-control col-md-12 col-sm-12 col-xs-12" name="C_START" placeholder="MM/DD/YYYY" type="date" value="<?php echo date("Y-m-d");?>">
                                                                                            </div>
                                                                                        </div>
                                                                                        
                                                                                    </div>
                                                                                    <div class="row">
                                                                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                                                                            <div class="form-group">
                                                                                                <label class="pull-left control-label col-md-12 col-sm-12 col-xs-12" for="name">Finish</label>
                                                                                                <input class="form-control col-md-12 col-sm-12 col-xs-12" name="C_FINISH" placeholder="MM/DD/YYYY" type="date" value="<?php echo date("Y-m-d");?>">
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                                                    <h4>
                                                                                        <fieldset>Accomodation</fieldset>
                                                                                    </h4>
                                                                                    <div class="row">
                                                                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                                                                            <label class="control-label col-md-12 col-sm-12 col-xs-12" for="name">Include Accomodation
                                                                                            </label>
                                                                                            <input name="ACCOMODATION" type="hidden" value="0">
                                                                                            <input name="ACCOMODATION" type="checkbox" class="flat" VALUE="1">
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="row">
                                                                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                                                                            <div class="form-group">
                                                                                                <label class="pull-left control-label col-md-12 col-sm-12 col-xs-12" for="name">IN</label>
                                                                                                <input class="form-control col-md-12 col-sm-12 col-xs-12" name="IN_DATE" placeholder="MM/DD/YYYY" type="date" value="<?php echo date("Y-m-d");?>">
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="row">
                                                                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                                                                            <div class="form-group">
                                                                                                <label class="pull-left control-label col-md-12 col-sm-12 col-xs-12" for="name">Out</label>
                                                                                                <input class="form-control col-md-12 col-sm-12 col-xs-12" name="OUT_DATE" placeholder="MM/DD/YYYY" type="date" value="<?php echo date("Y-m-d");?>">
                                                                                            </div>
                                                                                        </div>
                                                                                        
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                                                <h4>
                                                                                    <fieldset>Notes</fieldset>
                                                                                </h4>
                                                                                <div class="form-group">
                                                                                    <label class="control-label col-md-12 col-sm-12 col-xs-12" for="name">
                                                                                    </label>
                                                                                    <textarea name="NOTES" rows="4" cols="60" style="margin: 0px; width: 600px; height: 98px;"></textarea>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <button type="submit" class="btn btn-success">SAVE</button>
                                                    </form>
                                                    </div>
                                                    </div>
                                                    </div>
                                                    <!--        Tour Students -->
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
    /*
        * @package    ACCIT
         * @author    Paperlink Softwares Team
         * @copyright    Copyright (c) 2016 , Paperlink Softwares Pvt. Ltd.  (http://www.paperlinksoftwares.com/)
         * @since    Version 1.0.0
         * @filesource
         */
    
    if($_GET['dtl1'])
        {
        	if(delete_record("DELETE FROM `lang_class_module` WHERE `LANG_CLASS_MODULE_NO`=".$_GET['dtl1']))
    		{
        		$_SESSION['s_msg']="Item successfully deleted";
    		}
        }
    
    ?>
<div role="main" class="right_col" style="min-height: 648px;">
    <div class="">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="page-title">
                <div class="title_left">
                    <h3>Module Classes</h3>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="clearfix"></div>
                    <div class="x_content">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel recent-app">
                                    <div class="x_content">
                                        <div class="student-account-page">
                                            <?php 
                                                if($_SESSION['s_msg'] || $_SESSION['e_msg'])
                                                {
                                                ?>
                                            <div class="alert <?=($_SESSION['s_msg'])?'alert-success':'alert-error'?> fade in">
                                                <button data-dismiss="alert" class="close" type="button">�</button>
                                                <?=($_SESSION['s_msg'])?$_SESSION['s_msg']:$_SESSION['e_msg']?>
                                            </div>
                                            <?php
                                                unset($_SESSION['s_msg']);
                                                unset($_SESSION['e_msg']);
                                                }
                                                ?>
                                            <!-- Nav tabs -->
                                            <div class="card">
                                              
                                                <div class="tab-content">
                                                    <div id="all-student" class="tab-pane active" role="tabpanel">
                                                        <div class="all-students-list">
                                                            <div class="table-responsive">
                                                                <div id="datatable-buttons_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                                                                    <div class="dataTables_wrapper form-inline dt-bootstrap no-footer" id="datatable-buttons_wrapper">
                                                                        <div id="datatable-buttons_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                                                                            <table id="datatable" class="display table table-striped table-bordered table data-tbl-tools" cellspacing="0" width="100%">
                                                                                <thead>
                                                                                    <tr class="headings" role="row">
                                                                                        <th class="column-title sorting_asc" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1" style="width: 145px;" aria-label="Date
                                                                                            : activate to sort column descending" aria-sort="ascending">Class Name
                                                                                        </th>
                                                                                        <th class="column-title sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1" style="width: 145px;" aria-label="Attention
                                                                                            : activate to sort column ascending">Date
                                                                                        </th>
                                                                                        <th class="column-title sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1" style="width: 145px;" aria-label="Student No
                                                                                            : activate to sort column ascending">Start Time
                                                                                        </th>
                                                                                        <th class="column-title sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1" style="width: 145px;" aria-label="First Name
                                                                                            : activate to sort column ascending">End Time
                                                                                        </th>
                                                                                        <th class="column-title sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1" style="width: 145px;" aria-label="Middle Name
                                                                                            : activate to sort column ascending">Teacher
                                                                                        </th>
                                                                                        <th class="column-title sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1" style="width: 145px;" aria-label="Middle Name
                                                                                            : activate to sort column ascending">Room
                                                                                        </th>
                                                                                        <th class="column-title sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1" style="width: 145px;" aria-label="Middle Name
                                                                                            : activate to sort column ascending">Level No
                                                                                        </th>
                                                                                       
                                                                                        <th class="column-title no-link last sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1" style="width: 145px;" aria-label="Action
                                                                                            : activate to sort column ascending"><span class="nobr">Action</span>
                                                                                        </th>
                                                                                    </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                    <?php	
                                                                                        $sql1="select * from lang_class_module";
                                                                                        $q1=mysql_query($sql1);
                                                                                        while($row1=mysql_fetch_array($q1))
                                                                                        { 
                                                                                        
                                                                                        	?>
                                                                                    <tr class="pointer odd" role="row">
                                                                                        <td class=""><?=$row1['CLASS_NAME'];?></td>
                                                                                        <td class=" "><?=$row1['DATE'];?></td>
                                                                                        <td class=" "><?=$row1['ST_TIME'];?></td>
                                                                                        <td class=" "><?=$row1['END_TIME'];?></td>
                                                                                        <?php $cond1="where `TEACHER_NO`=".$row1['TEACHER_NO']; $cou1=getRows($cond1,'teacher');?>
                                                                                        <td class=" "><?=$cou1[0]['TEACHER_CODE'];?></td>
                                                                                        <?php $cond="where `ROOM_NO`=".$row1['ROOM_NO']; $cou=getRows($cond,'room');?>
                                                                                        <td class=" "><?=$cou[0]['ROOM_DESC'];?></td>
                                                                                        <td class=" "><?=$row1['LEVEL_NO'];?></td>
                                                                                      
                                                                                        <td><a href="dashboard.php?op=<?=MD5('display_language_module_edit')?>&mod_no=<?=$row1['LANG_CLASS_MODULE_NO']?>">Edit</a>|<a href="dashboard.php?op=<?=MD5('classes_module')?>&dtl1=<?=$row1['LANG_CLASS_MODULE_NO']?>" class="color-red stting"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>Delete</a></td>
                                                                                        <?php }  ?>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
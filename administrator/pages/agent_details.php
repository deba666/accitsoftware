<?php 
    /*
        * @package    ACCIT
         * @author    Paperlink Softwares Team
         * @copyright    Copyright (c) 2016 , Paperlink Softwares Pvt. Ltd.  (http://www.paperlinksoftwares.com/)
         * @since    Version 1.0.0
         * @filesource
		 * Here we update an existing Agent and display all the details of that particular agent
		 * Add New Agent employee and also display all the agent employee under that Agent (Edit , Delete Links are Added with a particular Employee) 
		 * Display All the Enrolled Student Details under that particular Agent
		 * Display All the Offer made by that particular Agent 
		 * Add New Agent Diary and also display all the agent Diary under that particular Agent (Edit , Delete Links are Added with a particular Diary) 
		 * Add New Agent Commission and also display all the agent Commission under that particular Agent
         */
  ?>
  <?php  
  // * Here we update an existing Agent and display all the details of that particular agent  
    if($_POST['AGENT_CODE'])
        {
        
        if($_POST['G_LOCAL']!=1)
        {
            $abn = '';
            $taxcode = '';
        }
        else
        {
            $abn = $_POST['ABN'];
            $taxcode = $_POST['TAX_CODE'];
        }
    	   if(mysql_query("UPDATE `agent` SET `AGENT_CODE`='".$_POST['AGENT_CODE']."',`STATUS`='".$_POST['STATUS']."', `AGENT_NAME`='".$_POST['AGENT_NAME']."',`AG_TYPE`='".$_POST['AG_TYPE']."',`COUNTRY_NO`='".$_POST['COUNTRY_NO']."',`G_LOCAL`='".$_POST['G_LOCAL']."', `ABN`='".$abn."',`ADDRESS1`='".$_POST['ADDRESS1']."', `ADDRESS2`='".$_POST['ADDRESS2']."',`ADDRESS3`='".$_POST['ADDRESS3']."',`ADDRESS4`='".$_POST['ADDRESS4']."', `ADDRESS1_TWO`='".$_POST['ADDRESS1_TWO']."',`ADDRESS2_TWO`='".$_POST['ADDRESS2TWO']."',`ADDRESS3_TWO`='".$_POST['ADDRESS3_TWO']."',`ADDRESS4_TWO`='".$_POST['ADDRESS4_TWO']."',`PHONE`='".$_POST['std_loc_phone']."',`EMAIL`='".$_POST['std_loc_email']."',`FAX`='".$_POST['std_loc_fax']."',`WWW`='".$_POST['std_loc_url']."',`MOBILE`='".$_POST['std_loc_mobile']."',`COMM_FIXED`='',`COMMENTS`='',`PREVIOUS_NAME`='',`COUNTRY_NO_TWO`='',CONTRACT='',`CONTRACT_TYPE`='',`AGENT_SINCE`='',`CONTRIB_DUE_DATE`='',`BONUS_DUE_DATE`='',`VISIT_TO`='',`VISIT_FROM`='',`CONTRIB_AMOUNT`='',`BONUS_AMOUNT`='',`COMM_FEE`='',`COMM_PER`='',`EMP_NO`='0',`AGENT_PASSWORD`='',`AGENT_ACCESS`='',`SALES_USER_NO`='".$_REQUEST['SALES_USER_NO']."',`PY_FROM`='',`PY_ENR_BUDGET`='',`PY_CW_BUDGET`='',`PY_INV_BUDGET`='',`PY_COMMENTS`='',`YTD_FROM`='',`YTD_ENR_BUDGET`='',`YTD_CW_BUDGET`='',`YTD_INV_BUDGET`='',`YTD_COMMENTS`='',`OLD_AGENT_NO`='',`COUNTRY_NAME`='',`COUNTRY_NAME_TWO`='',`PHOTO`='',`WHO_DATE`='".$_POST['WHO_DATE']."',`USER_NO`='".$_POST['user']."' where `AGENT_NO`=".$_GET['aid'])){
    		   if(mysql_query("UPDATE `customer` set `TYPE_NO`='".$_POST['TYPE_NO']."',`CODE`='".$_POST['AGENT_CODE']."',`NAME`='".$_POST['AGENT_NAME']."',`STATUS_NO`='".$_POST['STATUS']."',`NOTES`='".$_POST['NOTE']."',`CUSTOM_1`='".$_POST['CUST1']."',`CUSTOM_2`='".$_POST['CUST2']."',`CUSTOM_3`='".$_POST['CUST3']."',`CUSTOM_4`='".$_POST['CUST4']."',`CUSTOM_5`='".$_POST['CUST5']."',`CUSTOM_6`='".$_POST['CUST6']."',`CUSTOM_7`='".$_POST['CUST7']."',`CUSTOM_8`='".$_POST['CUST8']."',`CUSTOM_9`='".$_POST['CUST9']."',`CUSTOM_10`='".$_POST['CUST10']."',`TAX_CODE`='".$taxcode."',`DELETE_DATE`='',`LOCK_NUM`='1',`PER_TITLE`='',`PER_FIRST_NAME`='',`PER_MIDDLE_NAME`='',`PER_LAST_NAME`='',`PER_NICK_NAME`='',`PER_DOB`='',`PER_GENDER`='',`PUBLISH_FLAG`='0',`DISPLAY_NAME`='',`ADDRESS1`='".$_POST['ADDRESS1']."',`ADDRESS2`='".$_POST['ADDRESS2']."',`ADDRESS3`='".$_POST['ADDRESS3']."',`ADDRESS4`='".$_POST['ADDRESS4']."',`P_ADDRESS1`='".$_POST['P_ADDRESS1']."',`P_ADDRESS2`='".$_POST['P_ADDRESS2']."',`P_ADDRESS3`='".$_POST['P_ADDRESS3']."',`P_ADDRESS4`='".$_POST['P_ADDRESS4']."',`PHONE`='',`MOBILE`='',`FAX`='',`WWW`='',`EMAIL`='',`LOCATION_NO`='0',`ACCOUNT_CODE`='',`CREATE_DATE`='".$_POST['WHO_DATE']."',`USER_NO`='".$_POST['user']."'where `REF_NO`=".$_GET['aid']))
    
    		$_SESSION['s_msg']="<strong>Well done!</strong> AGENT successfully updated";
    	   }
    	else
    	{
    		$_SESSION['e_msg']="<strong>Oh snap!</strong> AGENT is not updated";
    
    }
    }
    
        if($_GET['aid'])
        {
        	$cond="where `AGENT_NO`=".$_GET['aid'];
        	$std_row=getRows($cond,'agent');
    
    		$cond1="where `CUSTOMER_NO`=".$std_row[0]['CUSTOMER_NO'];
        	$std_row1=getRows($cond1,'customer');
    		
        	
        }
        
        
    	if($_GET['del1'])
        {
        	if(delete_record("DELETE FROM `agent_emps` WHERE `EMP_NO`=".$_GET['del1']))
        		$_SESSION['s_msg']="Item successfully deleted";
        }
    	if($_GET['del2'])
        {
        	if(delete_record("DELETE FROM `diary` WHERE `DIARY_NO`=".$_GET['del2']))
        		$_SESSION['s_msg']="Item successfully deleted";
        }
        if($_GET['del3'])
        {
        	if(delete_record("DELETE FROM `agent_commission` WHERE `agent_commission_no`=".$_GET['del3']))
        		$_SESSION['s_msg']="Item successfully deleted";
        }
    	?>

<script>
function checkDo()
{
   
    if(document.getElementById('G_LOCAL').checked==true)
    {
        document.getElementById('tax_1').style.display="";
        document.getElementById('tax_2').style.display="";
    }
    else
    {
        document.getElementById('tax_1').style.display="none";
        document.getElementById('tax_2').style.display="none";
    }
    return false;
}
</script>

<div class="right_col" role="main">
    <div class="">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="page-title">
                <div class="title_left">
                    <?php 
                        if($_SESSION['s_msg'] || $_SESSION['e_msg'])
                        {
                        ?>
                    <div class="alert <?=($_SESSION['s_msg'])?'alert-success':'alert-error'?> fade in">
                        <button data-dismiss="alert" class="close" type="button">�</button>
                        <?=($_SESSION['s_msg'])?$_SESSION['s_msg']:$_SESSION['e_msg']?>
                    </div>
                    <?php
                        unset($_SESSION['s_msg']);
                        unset($_SESSION['e_msg']);
                        }
                        ?>
                    <h3>Agent Details </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="clearfix"></div>
                    <div class="x_content">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel recent-app">
                                    <div class="x_content">
                                        <div class="student-account-page">
                                            <!-- Nav tabs -->
                                            <div class="card">
                                                <div class="all-students-list add student">
                                                    <div class="add-student-section">
                                                        <form class="form-horizontal form-label-left" action="" method="POST" enctype="multipart/form-data">
                                                            <button type="submit" class="btn btn-success" onclick="needToConfirm = false;">SAVE</button>
                                                                                                                    <ul id="student-acc-tab" class="nav nav-tabs" role="tablist">
    
                                                                <li role="presentation"  class="active"><a href="#details2" aria-controls="details2" role="tab" data-toggle="tab">Details</a></li>
                                                                <li role="presentation"><a href="#address2" aria-controls="address2" role="tab" data-toggle="tab">Address</a></li>
                                                                <li role="presentation"><a href="#employees2" aria-controls="employees2" role="tab" data-toggle="tab">Employees</a></li>
                                                                <li role="presentation"><a href="#students2" aria-controls="students2" role="tab" data-toggle="tab">Students</a></li>
                                                                <li role="presentation"><a href="#diary2" aria-controls="diary2" role="tab" data-toggle="tab">Diary</a></li>
                                                                <li role="presentation"><a href="#commission2" aria-controls="commission2" role="tab" data-toggle="tab">Commission</a></li>
                                                          <!--      <li role="presentation"><a href="#incentive2" aria-controls="incentive2" role="tab" data-toggle="tab">Incentive</a></li> -->
                                                            </ul>
                                                            <div class="tab-content">
                                                                <!--Details-->
                                                                <div role="tabpanel" class="tab-pane active" id="details2">
                                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                                        <div class="row">
                                                                            <h4>
                                                                                <fieldset>Basic Info</fieldset>
                                                                            </h4>
                                                                            <div class="item form-group">
                                                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Code</label>
                                                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                                                    <div class="row">
                                                                                        <div class="col-md-7 col-sm-7 col-xs-12">
                                                                                            <?php date_default_timezone_set('Australia/sydney');
                                                                                                $date = date('d/m/Y h:i:s a', time());
                                                                                                ?>
                                                                                            <input name="WHO_DATE"  type="hidden" value="<?php echo $date?>">
                                                                                            <input name="user" type="hidden" value="<?php echo $_SESSION['user_no']?>">
                                                                                            <input name="AGENT_CODE" type="text" value="<?=$std_row[0]['AGENT_CODE']?>" class="col-md-12 col-xs-12 form-control" required>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <?php
                                                                            $sql_user = mysql_query("select `USER_NO` , `USER_NAME` , `FNAME` , `LNAME` FROM `users` where 1");
                                                                            ?>
                                                                            <div class="item form-group">
                                                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Marketing Manager</label>
                                                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                                                    <div class="row">
                                                                                        <div class="col-md-6 col-sm-4 col-xs-12">
                                                                                            <select name="SALES_USER_NO" id="SALES_USER_NO"  class="form-control">
                                                                                                <option value="">Select Marketing Managers</option>
                                                                                                <?php if(mysql_num_rows($sql_user)>0) { while($row_user = mysql_fetch_array($sql_user)) { ?>
                                                                                                <option <?php if($row_user['USER_NO']==$std_row[0]['SALES_USER_NO']) { ?> selected <?php } ?> value="<?php echo $row_user['USER_NO']; ?>"><?php echo strtoupper($row_user['FNAME'])."&nbsp;".$row_user['LNAME']." ( ".$row_user['USER_NAME']." ) "; ?></option>
                                                                                                <?php } } ?>
                                                                                            </select>
                                                                                        </div>
                                                                                        
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            
                                                                            <div class="item form-group">
                                                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Status</label>
                                                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                                                    <div class="row">
                                                                                        <div class="col-md-7 col-sm-7 col-xs-12">
                                                                                          <input name="STATUS" type="hidden" value="1">
                                                                                          <input name="STATUS" type="checkbox" value="1"  <?php if($std_row[0]['STATUS']==1) { ?> checked <?php } ?>  class="flat">
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            
                                                                            <div class="item form-group">
                                                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Name</label>
                                                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                                                    <div class="row">
                                                                                        <div class="col-md-7 col-sm-7 col-xs-12">
                                                                                            <input name="AGENT_NAME" type="text" value="<?=$std_row[0]['AGENT_NAME']?>" class="col-md-12 col-xs-12 form-control" required>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <!-- <div class="col-md-3 col-sm-3 col-xs-3"><label for='formCountries[]' class="pull-left control-label col-md-12 col-sm-12 col-xs-12" for="name">Category
                                                                                </label>
                                                                                <select name="formCountries[]" multiple="multiple" class="col-md-12 col-xs-12 form-control">
                                                                                <option Value="0">Bronze</option>
                                                                                <option Value="1">Gold</option>
                                                                                 <option Value="2">Information Only</option>
                                                                                <option Value="3"> Silver</option>
                                                                                </select>
                                                                                </div> -->
                                                                            <div class="item form-group">
                                                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Category</label>
                                                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                                                    <div class="row">
                                                                                        <div class="col-md-7 col-sm-7 col-xs-12">
                                                                                            <select name="TYPE_NO" class="form-control" required>
                                                                                                <option value="0" <?php if($std_row1[0]['TYPE_NO']==0) { ?> selected <?php } ?>>(None)</option>
                                                                                                <option value="1" <?php if($std_row1[0]['TYPE_NO']==1) { ?> selected <?php } ?>>Bronze</option>
                                                                                                <option value="2" <?php if($std_row1[0]['TYPE_NO']==2) { ?> selected <?php } ?>>Gold</option>
                                                                                                <option value="3" <?php if($std_row1[0]['TYPE_NO']==3) { ?> selected <?php } ?>>Information Only</option>
                                                                                                <option value="4" <?php if($std_row1[0]['TYPE_NO']==4) { ?> selected <?php } ?>>Silver</option>
                                                                                            </select>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="item form-group">
                                                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Country</label>
                                                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                                                    <div class="row">
                                                                                        <div class="col-md-7 col-sm-7 col-xs-12">
                                                                                            <td class=""><select name="COUNTRY_NO" class="form-control" required> 
                                                                                                <?php
                                                                                                    $dd_res=mysql_query("Select *  from country");
                                                                                                    while($r=mysql_fetch_array($dd_res))
                                                                                                      { 
                                                                                                    if($std_row[0][COUNTRY_NO]==$r[COUNTRY_NO])
                                                                                                    	  {
                                                                                                         echo "<option value='$r[COUNTRY_NO]' selected> $r[COUNTRY_NAME] </option>";
                                                                                                        }
                                                                                                    	else
                                                                                                    	  {
                                                                                                    		echo "<option value='$r[COUNTRY_NO]'> $r[COUNTRY_NAME] </option>";
                                                                                                    	  }
                                                                                                      }
                                                                                                    
                                                                                                       ?>
                                                                                                </select>
                                                                                            </td>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                                                <div class="item form-group">
                                                                                    <label class="control-label col-md-12 col-sm-12 col-xs-12">
                                                                                  
                                                                                        <input type="checkbox" name="G_LOCAL" value="1" <?php if($std_row[0]['G_LOCAL']==1) { ?> checked <?php } ?>
                                                                                               onclick="javascript: checkDo();" 
                                                                                          id="G_LOCAL" />Tax Registered
                                                                                    </label>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-4 col-sm-4 col-xs-12" <?php if($std_row[0]['ABN']=='') { ?> style="display:none;" <?php } ?> id="tax_1">
                                                                            <div class="item form-group">
                                                                                <label class="control-label col-md-12 col-sm-12 col-xs-12">
                                                                                ABN<input name="ABN" type="text" value="<?=$std_row[0]['ABN']?>" class="form-control">
                                                                                </label>
                                                                            </div>
                                                                        </div>
                                                                        <?php
                                                                        
                                                                        $sql_tax_rate = mysql_query('SELECT * from `tax_rate` where DATE_FORMAT(NOW(), "%Y-%m-%d") BETWEEN VALID_FROM AND VALID_TO');
                                                                        
                                                                        ?>
                                                                        <div class="col-md-6 col-sm-6 col-xs-12" <?php if($std_row1[0]['TAX_CODE']=='') { ?> style="display:none;" <?php } ?>  id="tax_2">
                                                                            <div class="form-group">
                                                                                <label class="control-label col-md-12 col-sm-12 col-xs-12" for="name">Tax Code(s)
                                                                                </label>
                                                                                <?php 
                                                                                if(mysql_num_rows($sql_tax_rate) > 0) { ?>
                                                                                <select name="TAX_CODE" class="col-md-12 col-xs-12 form-control">
                                                                                    
                                                                                   
                                                                                    <?php while($tax_row=mysql_fetch_array($sql_tax_rate)) { 
                                                                                    if($tax_row['RATE_PERCENT']!='') { $val_tax = $tax_row['RATE_PERCENT']; $type = '%'; } else { $val_tax = $tax_row['RATE_AMOUNT']; $type = '$'; } ?>
                                                                                    <option value="<?=$tax_row['TAX_RATE_NO']?>" <?php if ($std_row1[0]['TAX_CODE'] == $tax_row['RATE_RATE_NO']) { echo 'selected="selected"';}?>><?php echo "CODE: ".$tax_row['CODE']." ( ".$val_tax." ".$type." )"; ?></option>
                                                                                    <?php } ?>
                                                                                </select>
                                                                                <?php } ?>
                                                                            </div>
                                                                        </div>
                                                                       
                                                                        <div class="row">
                                                                            <div class="col-md-12 col-sm-12">
                                                                                <div class="row">
                                                                                    <h4>
                                                                                        <fieldset>Custom Fields</fieldset>
                                                                                    </h4>
                                                                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                        <div class="form-group">
                                                                                            <input class="form-control col-md-12 col-sm-12 col-xs-12" name="CUST1" value="<?=$std_row1[0]['CUSTOM_1']?>" type="text">
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                        <input class="form-control col-md-12 col-sm-12 col-xs-12" name="CUST2" value="<?=$std_row1[0]['CUSTOM_2']?>" type="text">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="row">
                                                                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                        <div class="form-group">
                                                                                            <input class="form-control col-md-12 col-sm-12 col-xs-12" name="CUST3" value="<?=$std_row1[0]['CUSTOM_3']?>" type="text">
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                        <input class="form-control col-md-12 col-sm-12 col-xs-12" name="CUST4" value="<?=$std_row1[0]['CUSTOM_4']?>" type="text">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="row">
                                                                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                        <div class="form-group">
                                                                                            <input class="form-control col-md-12 col-sm-12 col-xs-12" name="CUST5" value="<?=$std_row1[0]['CUSTOM_5']?>" type="text">
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                        <input class="form-control col-md-12 col-sm-12 col-xs-12" name="CUST6" value="<?=$std_row1[0]['CUSTOM_6']?>" type="text">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="row">
                                                                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                        <div class="form-group">
                                                                                            <input class="form-control col-md-12 col-sm-12 col-xs-12" name="CUST7" value="<?=$std_row1[0]['CUSTOM_7']?>" type="text">
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                        <input class="form-control col-md-12 col-sm-12 col-xs-12" name="CUST8" value="<?=$std_row1[0]['CUSTOM_8']?>" type="text">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="row">
                                                                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                        <div class="form-group">
                                                                                            <input class="form-control col-md-12 col-sm-12 col-xs-12" name="CUST9" value="<?=$std_row1[0]['CUSTOM_9']?>" type="text">
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                        <input class="form-control col-md-12 col-sm-12 col-xs-12" name="CUST10" value="<?=$std_row1[0]['CUSTOM_10']?>" type="text">
                                                                                    </div>
                                                                                </div>
                                                                                <h4>
                                                                                    <fieldset>Notes</fieldset>
                                                                                </h4>
                                                                                <div class="control-label col-md-12 col-sm-12 col-xs-12">
                                                                                    <textarea rows="4" cols="20" name="NOTE" class="col-md-12 col-xs-12 form-control" ><?=$std_row1[0]['NOTES']?></textarea>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!--Address-->
                                                                <div role="tabpanel" class="tab-pane" id="address2">
                                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                                        <div class="row">
                                                                            <div class="col-md-12 col-sm-12">
                                                                                <div class="row">
                                                                                    <h4>
                                                                                        <fieldset>Local Address</fieldset>
                                                                                    </h4>
                                                                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                                                                        <div class="form-group">
                                                                                            <textarea name="ADDRESS1" class="form-control" rows="2"><?=$std_row1[0]['ADDRESS1']?></textarea>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                                                                        <div class="form-group">
                                                                                            <textarea name="ADDRESS2" class="form-control" rows="2"><?=$std_row1[0]['ADDRESS2']?></textarea>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                                                                        <div class="form-group">
                                                                                            <textarea name="ADDRESS3" class="form-control" rows="2"><?=$std_row1[0]['ADDRESS3']?></textarea>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                                                                        <div class="form-group">
                                                                                            <textarea name="ADDRESS4" class="form-control" rows="2"><?=$std_row1[0]['ADDRESS4']?></textarea>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                                        <div class="row">
                                                                            <h4>
                                                                                <fieldset>Permanent Address</fieldset>
                                                                            </h4>
                                                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                                                <div class="form-group">
                                                                                    <textarea name="P_ADDRESS1" class="form-control" rows="2"><?=$std_row1[0]['P_ADDRESS1']?></textarea>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                                                <div class="form-group">
                                                                                    <textarea name="P_ADDRESS2" class="form-control" rows="2"><?=$std_row1[0]['P_ADDRESS2']?></textarea>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                                                <div class="form-group">
                                                                                    <textarea name="P_ADDRESS3" class="form-control" rows="2"><?=$std_row1[0]['P_ADDRESS3']?></textarea>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                                                <div class="form-group">
                                                                                    <textarea  name="P_ADDRESS4" class="form-control" rows="2"><?=$std_row1[0]['P_ADDRESS4']?></textarea>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-md-12 col-sm-12">
                                                                            <div class="row">
                                                                                <h4>
                                                                                    <fieldset>Contact Details</fieldset>
                                                                                </h4>
                                                                                <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                    <div class="form-group">
                                                                                        <label class="pull-left control-label col-md-12 col-sm-12 col-xs-12" for="name">Phone
                                                                                        </label>
                                                                                        <input type="text" id="std_loc_phone" value="<?=$std_row[0]['PHONE']?>" name="std_loc_phone" placeholder="Phone" class="form-control col-md-12 col-xs-12" >
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                    <div class="form-group">
                                                                                        <label class="pull-left control-label col-md-12 col-sm-12 col-xs-12" for="name">Email
                                                                                        </label>
                                                                                        <input type="email" id="std_loc_email" value="<?=$std_row[0]['EMAIL']?>" name="std_loc_email" placeholder="Email" class="form-control col-md-12 col-xs-12"  >
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                    <div class="form-group">
                                                                                        <label class="pull-left control-label col-md-12 col-sm-12 col-xs-12" for="name">Fax
                                                                                        </label>
                                                                                        <input type="text" id="std_loc_fax" value="<?=$std_row[0]['FAX']?>" name="std_loc_fax" placeholder="Fax" class="form-control col-md-12 col-xs-12" >
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                    <div class="form-group">
                                                                                        <label class="pull-left control-label col-md-12 col-sm-12 col-xs-12" for="name">URL
                                                                                        </label>
                                                                                        <input type="url" id="std_loc_url" value="<?=$std_row[0]['WWW']?>" name="std_loc_url" placeholder="URL" class="form-control col-md-12 col-xs-12">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                    <div class="form-group">
                                                                                        <label class="pull-left control-label col-md-12 col-sm-12 col-xs-12" for="name">Mobile
                                                                                        </label>
                                                                                        <input type="text" id="std_loc_mobile" value="<?=$std_row[0]['MOBILE']?>" name="std_loc_mobile" placeholder="Mobile" class="form-control col-md-12 col-xs-12" >
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!--Employees-->
																
                                                                <div role="tabpanel" class="tab-pane" id="employees2">
                                                                    <div class="table-responsive">
                                                                        <?php  $ag= $_POST['edit']?>
                                                                        
                                                                        <input type="button" class="btn btn-success" onclick="javascript:void window.open('dashboard_popup.php?op=<?=MD5('display_agent_employee')?>&ag_no=<?=$std_row[0]['AGENT_NO']?>','1470138621367','width=700,height=500,toolbar=0,menubar=0,location=1,status=1,scrollbars=1,resizable=1,left=0,top=0');return false;" value="Add Employee">
                                                                        <br/><br/>
                                                                        <table id="example2" class="display table table-striped table-bordered" cellspacing="0" width="100%">
                                                                            <div class="hide-show-column dropdown" role="presentation">
                                                                                <a id="drop4" href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" role="button" aria-expanded="false">Hide / Show Column<span class="caret"></span></a>
                                                                                <ul id="menu6" class="dropdown-menu animated fadeInDown" role="menu">
                                                                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                                                        <span class="pull-left">
                                                                                        <input type="checkbox" class="flat">
                                                                                        </span>
                                                                                        <span class="pull-left">
                                                                                        Title
                                                                                        </span>
                                                                                        </a>
                                                                                    </li>
                                                                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                                                        <span class="pull-left">
                                                                                        <input type="checkbox" class="flat">
                                                                                        </span>
                                                                                        <span class="pull-left">
                                                                                        Last Name
                                                                                        </span>
                                                                                        </a>
                                                                                    </li>
                                                                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                                                        <span class="pull-left">
                                                                                        <input type="checkbox" class="flat">
                                                                                        </span>
                                                                                        <span class="pull-left">
                                                                                        First Name
                                                                                        </span>
                                                                                        </a>
                                                                                    </li>
                                                                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                                                        <span class="pull-left">
                                                                                        <input type="checkbox" class="flat">
                                                                                        </span>
                                                                                        <span class="pull-left">
                                                                                        Position
                                                                                        </span>
                                                                                        </a>
                                                                                    </li>
                                                                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                                                        <span class="pull-left">
                                                                                        <input type="checkbox" class="flat">
                                                                                        </span>
                                                                                        <span class="pull-left">
                                                                                        Phone
                                                                                        </span>
                                                                                        </a>
                                                                                    </li>
                                                                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                                                        <span class="pull-left">
                                                                                        <input type="checkbox" class="flat">
                                                                                        </span>
                                                                                        <span class="pull-left">
                                                                                        Fax
                                                                                        </span>
                                                                                        </a>
                                                                                    </li>
                                                                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                                                        <span class="pull-left">
                                                                                        <input type="checkbox" class="flat">
                                                                                        </span>
                                                                                        <span class="pull-left">
                                                                                        Email
                                                                                        </span>
                                                                                        </a>
                                                                                    </li>
                                                                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                                                        <span class="pull-left">
                                                                                        <input type="checkbox" class="flat">
                                                                                        </span>
                                                                                        <span class="pull-left">
                                                                                        Active
                                                                                        </span>
                                                                                        </a>
                                                                                    </li>
                                                                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                                                        <span class="pull-left">
                                                                                        <input type="checkbox" class="flat">
                                                                                        </span>
                                                                                        <span class="pull-left">
                                                                                        Primary
                                                                                        </span>
                                                                                        </a>
                                                                                    </li>
                                                                                </ul>
                                                                            </div>
                                                                            <thead>
                                                                                <tr class="headings">
                                                                                    <th class="column-title">
                                                                                        Title
                                                                                    </th>
                                                                                    <th class="column-title">
                                                                                        Last Name
                                                                                    </th>
                                                                                    <th class="column-title">
                                                                                        First Name
                                                                                    </th>
                                                                                    <th class="column-title">Position
                                                                                    </th>
                                                                                    <th class="column-title">Phone
                                                                                    </th>
                                                                                    <th class="column-title">Fax
                                                                                    </th>
                                                                                    <th class="column-title">Email
                                                                                    </th>
                                                                                    <!--<th class="column-title">Active
                                                                                    </th>
                                                                                    <th class="column-title">Primary
                                                                                    </th> -->
                                                                                    <th class="column-title">Action 
                                                                                    </th>
                                                                                </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                                <?php
																				//Add New Agent employee and also display all the agent employee under that Agent (Edit , Delete 		                                                                                  Links are Added with a particular Employee)
																				
                                                                                    $cond1="where `AGENT_NO`=".$std_row[0]['AGENT_NO'];
                                                                                    $std_row1=getRows($cond1,'agent_emps');
                                                                                    foreach($std_row1 as $std_row2)
                                                                                    {
                                                                                    ?>
                                                                                <tr class="even pointer">
                                                                                    <td class=''><?=$std_row2['TITLE']?></td>
                                                                                    <td><?=$std_row2['LAST_NAME']?></td>
                                                                                    <td><?=$std_row2['FIRST_NAME']?></td>
                                                                                    <td><?=$std_row2['AG_POSITION']?></td>
                                                                                    <td><?=$std_row2['PHONE']?></td>
                                                                                    <td><?=$std_row2['FAX']?></td>
                                                                                    <td><a href="mailto: <?=$std_row2['EMAIL']?>"><?=$std_row2['EMAIL']?></a></td>
                                                                                    <!-- <td><?=$std_row2['STATUS']?></td>
                                                                                    <td><?=$std_row2['PRIMARY_FLAG']?></td> -->
                                                                                    <td><a href="#" onclick="javascript:void window.open('dashboard_popup.php?op=<?=MD5('display_agent_employee_edit')?>&ag_no=<?=$std_row2['EMP_NO']?>','1470138621367','width=700,height=500,toolbar=0,menubar=0,location=1,status=1,scrollbars=1,resizable=1,left=0,top=0');return false;">Edit</a>|<a href="dashboard.php?op=<?=MD5('agent_details')?>&del1=<?=$std_row2['EMP_NO']?>&aid=<?=$std_row2['AGENT_NO']?>" class="color-sky stting" onclick="javascript: var p = confirm('Are you sure?'); if(p==false) { return false; } "><i class="fa fa-pencil-square-o" aria-hidden="true"></i>Delete</a></td>
                                                                                </tr>
                                                                                <?php
                                                                                    }?>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                                <!--students-->
                                                                <div role="tabpanel" class="tab-pane" id="students2">
                                                                    <div class="table-responsive">
                                                                        <h4>
                                                                            <fieldset>Enrolments</fieldset>
                                                                        </h4>
                                                                        <table id="example" class="display table table-striped table-bordered" cellspacing="0" width="100%">
                                                                            <div class="hide-show-column dropdown" role="presentation">
                                                                                <a id="drop4" href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" role="button" aria-expanded="false">Hide / Show Column<span class="caret"></span></a>
                                                                                <ul id="menu6" class="dropdown-menu animated fadeInDown" role="menu">
                                                                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                                                        <span class="pull-left">
                                                                                        <input type="checkbox" class="flat">
                                                                                        </span>
                                                                                        <span class="pull-left">
                                                                                        Enroll No
                                                                                        </span>
                                                                                        </a>
                                                                                    </li>
                                                                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                                                        <span class="pull-left">
                                                                                        <input type="checkbox" class="flat">
                                                                                        </span>
                                                                                        <span class="pull-left">
                                                                                        Student No
                                                                                        </span>
                                                                                        </a>
                                                                                    </li>
                                                                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                                                        <span class="pull-left">
                                                                                        <input type="checkbox" class="flat">
                                                                                        </span>
                                                                                        <span class="pull-left">
                                                                                        Last Name
                                                                                        </span>
                                                                                        </a>
                                                                                    </li>
                                                                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                                                        <span class="pull-left">
                                                                                        <input type="checkbox" class="flat">
                                                                                        </span>
                                                                                        <span class="pull-left">
                                                                                        First Name
                                                                                        </span>
                                                                                        </a>
                                                                                    </li>
                                                                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                                                        <span class="pull-left">
                                                                                        <input type="checkbox" class="flat">
                                                                                        </span>
                                                                                        <span class="pull-left">
                                                                                        Middle Name
                                                                                        </span>
                                                                                        </a>
                                                                                    </li>
                                                                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                                                        <span class="pull-left">
                                                                                        <input type="checkbox" class="flat">
                                                                                        </span>
                                                                                        <span class="pull-left">
                                                                                        Status
                                                                                        </span>
                                                                                        </a>
                                                                                    </li>
                                                                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                                                        <span class="pull-left">
                                                                                        <input type="checkbox" class="flat">
                                                                                        </span>
                                                                                        <span class="pull-left">
                                                                                        Start
                                                                                        </span>
                                                                                        </a>
                                                                                    </li>
                                                                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                                                        <span class="pull-left">
                                                                                        <input type="checkbox" class="flat">
                                                                                        </span>
                                                                                        <span class="pull-left">
                                                                                        End
                                                                                        </span>
                                                                                        </a>
                                                                                    </li>
                                                                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                                                        <span class="pull-left">
                                                                                        <input type="checkbox" class="flat">
                                                                                        </span>
                                                                                        <span class="pull-left">
                                                                                        Length
                                                                                        </span>
                                                                                        </a>
                                                                                    </li>
                                                                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                                                        <span class="pull-left">
                                                                                        <input type="checkbox" class="flat">
                                                                                        </span>
                                                                                        <span class="pull-left">
                                                                                        Faculty
                                                                                        </span>
                                                                                        </a>
                                                                                    </li>
                                                                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                                                        <span class="pull-left">
                                                                                        <input type="checkbox" class="flat">
                                                                                        </span>
                                                                                        <span class="pull-left">
                                                                                        Course
                                                                                        </span>
                                                                                        </a>
                                                                                    </li>
                                                                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                                                        <span class="pull-left">
                                                                                        <input type="checkbox" class="flat">
                                                                                        </span>
                                                                                        <span class="pull-left">
                                                                                        Course Code
                                                                                        </span>
                                                                                        </a>
                                                                                    </li>
                                                                                </ul>
                                                                            </div>
                                                                            <thead>
                                                                                <tr class="headings">
                                                                                    <th class="column-title">
                                                                                        Enrol No
                                                                                    </th>
                                                                                    <th class="column-title">
                                                                                        Student No
                                                                                    </th>
                                                                                    <th class="column-title">
                                                                                        Last Name
                                                                                    </th>
                                                                                    <th class="column-title">First Name
                                                                                    </th>
                                                                                    <th class="column-title">Middle Name
                                                                                    </th>
                                                                                    <th class="column-title">Status
                                                                                    </th>
                                                                                    <th class="column-title">Start
                                                                                    </th>
                                                                                    <th class="column-title">End 
                                                                                    </th>
                                                                                    <th class="column-title">Length
                                                                                    </th>
                                                                                    <th class="column-title">Faculty
                                                                                    </th>
                                                                                    <th class="column-title">Course
                                                                                    </th>
                                                                                    <th class="column-title">Course Code
                                                                                    </th>
                                                                                </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                                <?php
																				//Display All the Enrolled Student Details under that particular Agent
                                                                                    $cond3="where `AGENT_NO`=".$std_row[0]['AGENT_NO'];
                                                                                    $std_row3=getRows($cond3,'enrol');
                                                                                    foreach($std_row3 as $std_row4)
                                                                                    	{
                                                                                    	$cond5="where `STUD_NO`=".$std_row4['STUD_NO'];
                                                                                    	$std_row5=getRows($cond5,'student');
                                                                                    	foreach($std_row5 as $std_row6)
                                                                                    		{
																							
                                                                                    				?>
                                                                                <tr class="even pointer">
                                                                                    <td class=" "><?=$std_row4['ENROL_NO']?></td>
                                                                                    <td class=" "><?=$std_row6['EXT_STN']?></td>
                                                                                    <td class=" "><?=$std_row6['LNAME']?></td>
                                                                                    <td class=" "><?=$std_row6['FNAME']?></td>
                                                                                    <td class=" "><?=$std_row6['MNAME']?></td>
                                                                                    <td class=" "><?=$std_row4['STATUS']?></td>
                                                                                    <td class=" "><?=$std_row4['ST_DATE']?></td>
                                                                                    <td class=" "><?=$std_row4['END_DATE']?></td>
                                                                                    <td class=" "><?=$std_row4['CRSE_LEN']?></td>
																					<?php
																					if($std_row4['COURSE_NO'])
																							  {
                                                                                    			$cond9="where `COURSE_NO`=".$std_row4['COURSE_NO'];
                                                                                    			$std_row9=getRows($cond9,'course');
                                                                                    			} ?>
																								<?php
																								if($std_row9[0]['CRT_NO']){
																								$cond10="where `CRT_NO`=".$std_row9[0]['CRT_NO'];
                                                                                    			$std_row10=getRows($cond10,'crse_type');}
																								?>
                                                                                    <td class=" "><?=$std_row10[0]['CRT_NAME']?></td>
                                                                                    <td class=" "><?=$std_row9[0]['COURSE_NAME']?></td>
                                                                                    <td class=" "><?=$std_row9[0]['CODE']?></td>
                                                                                </tr>
                                                                                <?php
                                                                                    																		
                                                                                    }
                                                                                    }
                                                                                    ?>
                                                                            </tbody>
                                                                        </table>
                                                                        <div role="tabpanel" class="tab-pane">
                                                                            <div class="table-responsive">
                                                                                <h4>
                                                                                    <fieldset>Offers</fieldset>
                                                                                </h4>
                                                                                <table id="example" class="display table table-striped table-bordered" cellspacing="0" width="100%">
                                                                                    <thead>
                                                                                        <tr class="headings">
                                                                                            <th class="column-title">
                                                                                                Offer No
                                                                                            </th>
                                                                                            <th class="column-title">
                                                                                                Offer Date
                                                                                            </th>
                                                                                            <th class="column-title">
                                                                                                Student No
                                                                                            </th>
                                                                                            <th class="column-title">Last Name
                                                                                            </th>
                                                                                            <th class="column-title">First Name
                                                                                            </th>
                                                                                            <th class="column-title">Middle Name
                                                                                            </th>
                                                                                            <th class="column-title">Status
                                                                                            </th>
                                                                                        </tr>
                                                                                    </thead>
                                                                                    <tbody>
                                                                                        <?php
																						//Display All the Offer made by that particular Agent
                                                                                            $conda="where `REF_CUSTOMER_NO`=".$std_row[0]['CUSTOMER_NO'];
                                                                                            $std_rowa=getRows($conda,'offer');
                                                                                            foreach($std_rowa as $std_rowb)																			{
                                                                                            	$condc="where `CUSTOMER_NO`=".$std_rowb['CUSTOMER_NO'];
                                                                                            	$std_rowc=getRows($condc,'student');
                                                                                            	foreach($std_rowc as $std_rowd)																			{
                                                                                            	?>
                                                                                        <tr class="even pointer">
                                                                                            <td class=" "><?=$std_rowb['OFFER_NO']?></td>
                                                                                            <td class=" "><?=$std_rowb['OFFER_DATE']?></td>
                                                                                            <td class=" "><?=$std_rowd['EXT_STN']?></td>
                                                                                            <td class=" "><?=$std_rowd['LNAME']?></td>
                                                                                            <td class=" "><?=$std_rowd['FNAME']?></td>
                                                                                            <td class=" "><?=$std_rowd['MNAME']?></td>
                                                                                            <td class=" "><?=$std_rowb['STATUS_NO']?></td>
                                                                                         <!--   <td class="last"><a href="dashboard.php?op=<?=MD5('offer_details')?>&oid=<?=$std_rowb['OFFER_NO']?>" class="color-sky stting"><i class="fa fa-cog" aria-hidden="true"></i>Edit</a>| 
                                                                                                <a href="dashboard.php?op=<?=MD5('offer_details')?>&del=<?=$std_rowb['OFFER_NO']?>" class="color-sky stting"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>Delete</a>
                                                                                            </td> -->
                                                                                        </tr>
                                                                                        <?php
                                                                                            }}?>
                                                                                    </tbody>
                                                                                </table>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    .
                                                                </div>
                                                                <!--diary-->
                                                                <div role="tabpanel" class="tab-pane" id="diary2">
                                                                    <div class="table-responsive">
                                                                        <?php  $ag= $_POST['edit']?>
                                                                        <input type="button" class="btn btn-success" onclick="javascript:void window.open('dashboard_popup.php?op=<?=MD5('display_agent_diary')?>&ag_no=<?=$std_row[0]['AGENT_NO']?>&ag_name=<?=$std_row[0]['AGENT_NAME']?>&cus_no=<?=$std_row[0]['CUSTOMER_NO']?>','1470138621367','width=700,height=500,toolbar=0,menubar=0,location=1,status=1,scrollbars=1,resizable=1,left=0,top=0');return false;" value="Add Diary">
                                                                        <br/><br/>
                                                                        <table id="example2" class="display table table-striped table-bordered" cellspacing="0" width="100%">
                                                                            <div class="hide-show-column dropdown" role="presentation">
                                                                                <a id="drop4" href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" role="button" aria-expanded="false">Hide / Show Column<span class="caret"></span></a>
                                                                                <ul id="menu6" class="dropdown-menu animated fadeInDown" role="menu">
                                                                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                                                        <span class="pull-left">
                                                                                        <input type="checkbox" class="flat">
                                                                                        </span>
                                                                                        <span class="pull-left">
                                                                                        Attention
                                                                                        </span>
                                                                                        </a>
                                                                                    </li>
                                                                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                                                        <span class="pull-left">
                                                                                        <input type="checkbox" class="flat">
                                                                                        </span>
                                                                                        <span class="pull-left">
                                                                                        Date
                                                                                        </span>
                                                                                        </a>
                                                                                    </li>
                                                                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                                                        <span class="pull-left">
                                                                                        <input type="checkbox" class="flat">
                                                                                        </span>
                                                                                        <span class="pull-left">
                                                                                        Category
                                                                                        </span>
                                                                                        </a>
                                                                                    </li>
                                                                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                                                        <span class="pull-left">
                                                                                        <input type="checkbox" class="flat">
                                                                                        </span>
                                                                                        <span class="pull-left">
                                                                                        Subject
                                                                                        </span>
                                                                                        </a>
                                                                                    </li>
                                                                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                                                        <span class="pull-left">
                                                                                        <input type="checkbox" class="flat">
                                                                                        </span>
                                                                                        <span class="pull-left">
                                                                                        When
                                                                                        </span>
                                                                                        </a>
                                                                                    </li>
                                                                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                                                        <span class="pull-left">
                                                                                        <input type="checkbox" class="flat">
                                                                                        </span>
                                                                                        <span class="pull-left">
                                                                                        User
                                                                                        </span>
                                                                                        </a>
                                                                                    </li>
                                                                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                                                        <span class="pull-left">
                                                                                        <input type="checkbox" class="flat">
                                                                                        </span>
                                                                                        <span class="pull-left">
                                                                                        Notes
                                                                                        </span>
                                                                                        </a>
                                                                                </ul>
                                                                                <thead>
                                                                                    <tr class="headings">
                                                                                        <th class="column-title">Date
                                                                                        </th>
                                                                                        <th class="column-title">Attention
                                                                                        </th>
                                                                                        <th class="column-title">Agent Code
                                                                                        </th>
                                                                                        <th class="column-title">Subject
                                                                                        </th>
                                                                                        <th class="column-title">Agent Name
                                                                                        </th>
                                                                                        <th class="column-title">Notes
                                                                                        </th>
                                                                                        <th class="column-title">When
                                                                                        </th>
                                                                                        <th class="column-title">User
                                                                                        </th>
                                                                                        <th class="column-title">Action
                                                                                        </th>
                                                                                    </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                    <?php
																					//Add New Agent Diary and also display all the agent Diary under that particular Agent (Edit ,                                                                                      Delete Links are Added with a particular Diary)
                                                                                        $co1="where `CUSTOMER_NO`=".$std_row[0]['CUSTOMER_NO'];
                                                                                        $st1=getRows($co1,'diary');
                                                                                        foreach($st1 as $st2)																			{
                                                                                           $co2="where `USER_NO`=".$st2['USER_NO'];
                                                                                        $st3=getRows($co2,'users');
                                                                                        foreach($st3 as $st4)																			{
                                                                                        
                                                                                        	?>
                                                                                    <tr class="even pointer">
                                                                                        <td class=" "><?=$st2['DIARY_DATE']?></td>
                                                                                        <td class=" "><?php if($st2['ATTENTION']==1) { ?> YES <?php } else { ?>NO<?php } ?></td>
                                                                                        <td class=" "><?=$st2['REF_NO']?></td>
                                                                                        <td class=" "><?=$st2['SUMMARY']?></td>
                                                                                        <td class=" "><?=$std_row[0]['AGENT_NAME']?></td>
                                                                                        <td class=" "><?=$st2['NOTES']?></td>
                                                                                        <td class=" "><?=$st2['WHO_DATE']?></td>
                                                                                        <td class=" "><?=$st4['INIT']?></td>
                                                                                        <td><a href="#" onclick="javascript:void window.open('dashboard.php?op=<?=MD5('display_agent_diary_edit')?>&d_no=<?=$st2['DIARY_NO']?>&a_na=<?=$std_row[0]['AGENT_NAME']?>','1470138621367','width=700,height=500,toolbar=0,menubar=0,location=1,status=1,scrollbars=1,resizable=1,left=0,top=0');return false;">Edit</a>|<a href="dashboard.php?op=<?=MD5('agent_details')?>&del2=<?=$st2['DIARY_NO']?>&aid=<?=$std_row2['AGENT_NO']?>" class="color-sky stting"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>Delete</a></td>
                                                                                    </tr>
                                                                                    <?php }} ?>
                                                                                </tbody>
                                                                            </div>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                                <!--commission-->
                                                                <div role="tabpanel" class="tab-pane" id="commission2">
                                                                    <div class="table-responsive">
                                                                        <?php  $ag= $_POST['edit']?>
                                                                        
                                                                         <input type="button" class="btn btn-success" onclick="javascript:void window.open('dashboard_popup.php?op=<?=MD5('display_agent_commission')?>&ag_no=<?=$std_row[0]['AGENT_NO']?>','1470138621367','width=700,height=500,toolbar=0,menubar=0,location=1,status=1,scrollbars=1,resizable=1,left=0,top=0');return false;" value="Add Commission">
                                                                        <br/><br/>
                                                                        <table id="example3" class="display table table-striped table-bordered" cellspacing="0" width="100%">
                                                                            <div class="hide-show-column dropdown" role="presentation">
                                                                                <ul id="menu6" class="dropdown-menu animated fadeInDown" role="menu">
                                                                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                                                        <span class="pull-left">
                                                                                        <input type="checkbox" class="flat">
                                                                                        </span>
                                                                                        <span class="pull-left">
                                                                                        Name
                                                                                        </span>
                                                                                        </a>
                                                                                    </li>
                                                                                   
                                                                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                                                        <span class="pull-left">
                                                                                        <input type="checkbox" class="flat">
                                                                                        </span>
                                                                                        <span class="pull-left">
                                                                                        Tax Option
                                                                                        </span>
                                                                                        </a>
                                                                                    </li>
                                                                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                                                        <span class="pull-left">
                                                                                        <input type="checkbox" class="flat">
                                                                                        </span>
                                                                                        <span class="pull-left">
                                                                                        Apply
                                                                                        </span>
                                                                                        </a>
                                                                                    </li>
                                                                                </ul>
                                                                            </div>
                                                                            <thead>
                                                                                <tr class="headings">
                                                                                    <th class="column-title">Faculty
                                                                                    </th>
                                                                                    <th class="column-title">Name
                                                                                    </th>
                                                                                    <th class="column-title">Rate
                                                                                    </th>
                                                                                    
                                                                                    <th class="column-title">Default
                                                                                    </th>
                                                                                    <th class="column-title">Action
                                                                                    </th>
                                                                                </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                                <?php
                                                                                    $conda1="where `AGENT_NO`=".$std_row[0]['AGENT_NO'];
                                                                                    $std_rowa1=getRows($conda1,'agent_commission');
                                                                                    
                                                                                    
                                                                                    foreach($std_rowa1 as $std_rowb1)
										{																			
                                                                                    $conda2="where `CRT_NO`=".$std_rowb1['CRT_NO'];
                                                                                    $std_rowa2=getRows($conda2,'crse_type');
                                                                                    
                                                                                   
                                                                                   // foreach($std_rowa2 as $std_rowb2){																			
                                                                                    	?>
                                                                                <tr class="even pointer">
                                                                                    <td class=" ">
                                                                                        <?php echo $std_rowa2[0]['CRT_NAME']; ?>
                                                                                    </td>
                                                                                    <td class=" ">
                                                                                        <?=$std_rowb1['NAME']?>
                                                                                    </td>
                                                                                    <td class=" "><?php if($std_rowb1['RATE_PERCENT']>0) { echo $std_rowb1['RATE_PERCENT']."%"; } 
                                                                                    else { echo $std_rowb1['RATE_AMOUNT']."$"; } ?></td>
                                                                                   
                                                                                    
                                                                                    <td class=""><input type="checkbox" <?php if($std_rowb1['LOCK_NUM']==1) { ?> checked <?php } ?> name="LOCK_NUM" id="LOCK_NUM" readonly disabled /></td>
                                                                                    <td class="last"><a class="color-sky stting" href="#" onclick="javascript: void window.open('dashboard_popup.php?op=<?=MD5('display_agent_commission_edit')?>&AGENT_COMMISSION_NO=<?=$std_rowb1['AGENT_COMMISSION_NO']?>','147123445749','width=800,height=600,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=0,top=0');return false;"><i aria-hidden="true" class="fa fa-cog"></i>Edit</a>
                                                                                  | <a href="dashboard.php?op=<?=MD5('agent_details')?>&del3=<?=$std_rowb1['AGENT_COMMISSION_NO'];?>&aid=<?=$_GET['aid']?>" onclick="return confirm('Are you sure?'); return false;" class="color-sky stting"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>Delete</a>  
                                                                               </td>

</tr>
                                                                                <?php
                                                                                    //}
                                                                                    
                                                                                    }
                                                                                    	?>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                                <!--incentive-->
                                                                <div role="tabpanel" class="tab-pane" id="incentive2">
                                                                    <div class="table-responsive">
                                                                        <table id="example4" class="display table table-striped table-bordered" cellspacing="0" width="100%">
                                                                            <div class="hide-show-column dropdown" role="presentation">
                                                                                <a id="drop4" href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" role="button" aria-expanded="false">Hide / Show Column<span class="caret"></span></a>
                                                                                <ul id="menu6" class="dropdown-menu animated fadeInDown" role="menu">
                                                                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                                                        <span class="pull-left">
                                                                                        <input type="checkbox" class="flat">
                                                                                        </span>
                                                                                        <span class="pull-left">
                                                                                        Date
                                                                                        </span>
                                                                                        </a>
                                                                                    </li>
                                                                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                                                        <span class="pull-left">
                                                                                        <input type="checkbox" class="flat">
                                                                                        </span>
                                                                                        <span class="pull-left">
                                                                                        Description
                                                                                        </span>
                                                                                        </a>
                                                                                    </li>
                                                                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                                                        <span class="pull-left">
                                                                                        <input type="checkbox" class="flat">
                                                                                        </span>
                                                                                        <span class="pull-left">
                                                                                        Amount
                                                                                        </span>
                                                                                        </a>
                                                                                    </li>
                                                                                </ul>
                                                                            </div>
                                                                            <thead>
                                                                                <tr class="headings">
                                                                                    <th class="column-title">Date
                                                                                    </th>
                                                                                    <th class="column-title">Description
                                                                                    </th>
                                                                                    <th class="column-title">Amount
                                                                                    </th>
                                                                                </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                                <tr class="even pointer">
                                                                                    <td class=" ">26/06/2016</td>
                                                                                    <td class=" ">Kolkata</td>
                                                                                    <td class=" ">Mumbai</td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                    </div>
                                                </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
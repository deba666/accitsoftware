<?php
    /*
        * @package    ACCIT
         * @author    Paperlink Softwares Team
         * @copyright    Copyright (c) 2016 , Paperlink Softwares Pvt. Ltd.  (http://www.paperlinksoftwares.com/)
         * @since    Version 1.0.0
         * @filesource
		 * Code for Display all the agent diary fetch from student and diary table. Links come from the sidebar under diary option
         */
    ?>
<div class="right_col" role="main">
    <div class="">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="page-title">
                <div class="title_left">
                    <h3>Student Diary</h3>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="clearfix"></div>
                    <div class="x_content">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel recent-app">
                                    <div class="x_content">
                                        <div class="student-account-page">
                                            <!-- Nav tabs -->
                                            <div class="card">
                                                <div class="tab-content">
                                                    <div role="tabpanel" class="tab-pane active" id="all-student">
                                                        <div class="all-students-list">
                                                            <div class="table-responsive">
                                                                <table class="table data-tbl-tools">
                                                                    <thead>
                                                                        <tr class="headings">
                                                                            <th class="column-title">Date
                                                                            </th>
                                                                            <th class="column-title">Attention
                                                                            </th>
                                                                            <th class="column-title">Student No
                                                                            </th>
                                                                            <th class="column-title">First Name
                                                                            </th>
                                                                            <th class="column-title">Middle Name
                                                                            </th>
                                                                            <th class="column-title">Last Name
                                                                            </th>
                                                                            <th class="column-title">Subject
                                                                            </th>
                                                                            <th class="column-title">Note
                                                                            </th>
                                                                            <th class="column-title">User
                                                                            </th>
                                                                            <th class="column-title">When
                                                                            </th>
                                                                            <th class="column-title">Action
                                                                            </th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        <?php 
                                                                            $diary_sql=getRows('order by `DIARY_NO` DESC','diary'); 
                                                                            foreach($diary_sql as $row)
                                                                            {
                                                                             $cond="where `CUSTOMER_NO`=".$row['CUSTOMER_NO']; 
                                                                             $std=getRows($cond,'student');
                                                                            	 foreach($std as $row1)
                                                                            {
                                                                            
                                                                             /*$sql="select * from diary D, student S where D.CUSTOMER_NO=S.CUSTOMER_NO";
                                                                             $query=mysql_query($sql);
                                                                             while($row=mysql_fetch_array($query))
                                                                             { */
                                                                            	?>
                                                                        <tr class="even pointer">
                                                                            <?php $originalDate = $row['DIARY_DATE'];
                                                                                $newDate = date("d/m/Y", strtotime($originalDate)); ?>
                                                                            <td class=" "><?php echo $newDate;?></td>
                                                                            <td class=" "><?=($row['ATTENTION'])?'<input disabled type="checkbox" checked class="flat">':'<input disabled type="checkbox" class="flat">'?></td>
                                                                            <td class=" "><?=$row1['EXT_STN']?></td>
                                                                            <td class=" "><?=$row1['FNAME']?></td>
                                                                            <td class=" "><?=$row1['MNAME']?></td>
                                                                            <td class=" "><?=$row1['LNAME']?></td>
                                                                            <td class=" "><?=$row['SUMMARY']?></td>
                                                                            <td class=" "><?=$row['NOTES']?></td>
                                                                            <?php $cond1="where `USER_NO`=".$row['USER_NO']; $user=getRows($cond1,'users');?>
                                                                            <td class=" "><?=$user[0]['INIT']?></td>
                                                                            <?php $originalDate1 = $row['WHO_DATE'];
                                                                                $newDate1 = date("d/m/Y h:i:s", strtotime($originalDate1)); ?>
                                                                            <td class=""><?php echo $newDate1;?></td>
                                                                            <td class="last">
                                                                                <?php  $cou= $row['DIARY_NO']?>
                                                                                <a href="#" onclick="javascript:void window.open('dashboard.php?op=<?=MD5('display_student_diary_edit')?>&d_no=<?php echo $cou;?>&s_id=<?=$row1['STUD_NO']?>&s_no=<?=$row1['EXT_STN']?>&s_fname=<?=$row1['FNAME']?>&s_lname=<?=$row1['LNAME']?>&s_mname=<?=$row1['MNAME']?>','1470138621367','width=800,height=500,toolbar=0,menubar=0,location=1,status=1,scrollbars=1,resizable=1,left=0,top=0');return false;">Modify</a>| <a href="dashboard.php?op=<?=MD5('diary_student')?>&details=<?=$row['DIARY_NO'];?>"class="color-sky stting"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>Delete</a>
                                                                            </td>
                                                                        </tr>
                                                                        <?php } }?>                                                  
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
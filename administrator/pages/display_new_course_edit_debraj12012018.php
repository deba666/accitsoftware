<?php
    /*
        * @package    ACCIT
         * @author    Paperlink Softwares Team
         * @copyright    Copyright (c) 2016 , Paperlink Softwares Pvt. Ltd.  (http://www.paperlinksoftwares.com/)
         * @since    Version 1.0.0
         * @filesource
		 * Code for add new course in offer. After create new offer when you clicked on new course (For add new offer) this code will work.
         */
  
    $result = mysql_query("SELECT * FROM student WHERE STUD_NO='" . $_POST["STUD_NO"] . "'"); //Take the student details from student table 
    $row = mysql_fetch_array($result);
    $name = $row['FNAME'] ." ". $row['MNAME'] ." ". $row['LNAME'];
    if($_GET['of_no'])
    {
    $s=$_GET['of_no'];
    }
    else
    {
    $s=$_SESSION['offer_no'];
    }
    if($_GET["AGENT_NO"])
	{
    $AGENT_NO=$_GET["AGENT_NO"];
    }
	else
	{
    $AGENT_NO=$_SESSION['AGENT_NO'];
	}
    if($_POST['RATE_PERCENT'])
	{
    $commission=$_POST['RATE_PERCENT'];
	}
	else if($_POST['CUSTOM_RATE'])
	{
	$commission=$_POST['CUSTOM_RATE'];
	}
    $amount=$_GET['cost'];
    $commission_percentage=($commission*$amount)/100;
	$afte_add_tax_commission=($commission_percentage*$_POST['TAX_AMOUNT'])/100;
	$final_commission= $commission_percentage+$afte_add_tax_commission;
    $holiday_list=array();//assign variable for store all the holiday dates into $holiday_list variable which are fetch from database
    //echo "POST DATE is=".$_POST['txtfld1']."<br>";
     $input_date=date("d/m/Y", strtotime($_POST['txtfld1']));//change input date format in dd/mm/yyyy format
    $q=mysql_query("select * from holiday where CRT_NO=".$_GET['faculty']);//query for fetch all the holidays from holiday table
    while($row=mysql_fetch_array($q)){
    if(!$row['DELETE_DATE']){// check if administrator not delete the holaday from database
    //echo "DB_FROM_DATE=".date("d/m/Y", strtotime($row['FROM_DATE']))."<br>";
    
    
    $begin = new DateTime($row['FROM_DATE']);//store holiday start date in $begin
    $end = new DateTime($row['TO_DATE']);//store holiday end date in $end
    $diff = $end->diff($begin)->format("%a");//calculate total difference dates in holiday from date to end date
    //echo"TOTAL DIFFERENCE DATE IS=".$diff ."<br>";
    $daterange = new DatePeriod($begin, new DateInterval('P1D'), $end);
    
    //print_r($holiday_list);
    foreach($daterange as $date)
    	{
        $date1=$date->format("d/m/Y");//change holiday date format into dd/mm/yyyy format
        $holiday_list[]=$date1;//store all the holidays into $holiday_list variable which are fetch from database 
     
    	}
    	
         }  
     }
    
     if(in_array($input_date,$holiday_list))//check if the user input date comes under holiday dates or not
    	{
           $holiday='0';
    		echo " THIS DATE MATCHED SUCCESSFULLY!!!!!!MATHED DATE IS".$input_date."<br>";
    		$message = $input_date."  is Holiday You Can not start Course at this date Select Another date";
            echo "<script type='text/javascript'>alert('$message');</script>";
    		
    	}
    	else
    		{
                    $holiday='2';
    		$days=$_GET['len'] * 5;
           // echo "Total Course  Days=".$days."<br>";
    		//echo "INPUT DATE IS=".$input_date."<br>";
    		$date1 = str_replace('/', '-', $input_date);
    		$date1a=date("Y-m-d", strtotime($date1));
    		//echo " New Formatted Input Date is=".$date1a."<br>";
    
            $date2 = date('Y-m-d',strtotime($date1a. ' + '.$days. 'days'));
    		//echo " After Adding Course date is=".$date2."<br>";
            //$date1 = date("d/m/Y",$date1);
    		$date3 = str_replace('-', '/', $date2);
    		//echo " After Adding Course New Formatted date is=".$date3."<br>";
    		$course_end_date=date("d/m/Y", strtotime($date3));
           //echo " Course End Days=".$course_end_date."<br><br>";
    	   
    		}
     
    $da_for=date("Y-m-d", strtotime($_POST['txtfld1'])); //start date of course
    
    $da_for_en=date("Y-m-d", strtotime($_POST['txtfld2']));//end date of course
   
     $holidays=array($dates);
    
    $in = getWorkingDays($da_for,$da_for_en,$holidays);//function for take only the working days . except all the holidays,saturday and sunday
    $nin=round($in);
  
    $new_end_date=date('Y-m-d',strtotime($da_for_en. ' + '.$nin. 'days'));//added only working days with the end date of course
   
    $lastfriday=new DateTime($new_end_date);//take only last friday for end date
    $lastfriday->modify('friday');
    $lastfriday1=$lastfriday->format('d-m-Y');//change format of last friday from dd/mm/yyyy to dd-mm-yyyy
    $lastfriday2=$lastfriday->format('Y-m-d');//change format of final end date from  dd-mm-yyyy to YYYY-mm-dd
    
   
    
    
    $lastfriday3 = str_replace('/', '-', $lastfriday2);
    $lastfriday4=date('d/m/Y', strtotime($lastfriday3));//change format of final end date from  YYYY-mm-dd to dd/mm/yyyy
   
    
    if(in_array($lastfriday4,$holiday_list)) //check if the final end date is comes under holiday then the date decrese with 7 days
    	{
    $mod_friday1=date('Y-m-d', strtotime($lastfriday2.' - 7days'));
    $mod_friday2 = str_replace('/', '-', $mod_friday1);
    $mod_friday3=date('d/m/Y', strtotime($mod_friday2));
    	}
    	else
    	{
    $mod_friday1=date('Y-m-d', strtotime($lastfriday2.' - 0days'));
    $mod_friday2 = str_replace('/', '-', $mod_friday1);
    $mod_friday3=date('d/m/Y', strtotime($mod_friday2));
    	}
    	if(in_array($mod_friday3,$holiday_list))//check if the final end date is comes under holiday then the date decrese with 7 days
    	{
    $mod_friday4=date('Y-m-d', strtotime($mod_friday1.' - 7days'));
    $mod_friday5 = str_replace('/', '-', $mod_friday4);
    $mod_friday6=date('d/m/Y', strtotime($mod_friday5));
    	}
    	else
    	{
    $mod_friday4=date('Y-m-d', strtotime($mod_friday1.' - 0days'));
    $mod_friday5 = str_replace('/', '-', $mod_friday4);
    $mod_friday6=date('d/m/Y', strtotime($mod_friday5));
    	}
    
    	if(in_array($mod_friday6,$holiday_list))//check if the final end date is comes under holiday then the date decrese with 7 days
    	{
    $mod_friday7=date('Y-m-d', strtotime($mod_friday4.' - 7days'));
    $mod_friday8 = str_replace('/', '-', $mod_friday7);
    $mod_friday9=date('d/m/Y', strtotime($mod_friday8));
    	}
    	else
    	{
    $mod_friday7=date('Y-m-d', strtotime($mod_friday4.' - 0days'));
    $mod_friday8 = str_replace('/', '-', $mod_friday7);
    $mod_friday9=date('d/m/Y', strtotime($mod_friday8));
    	}
   
    $dd=str_replace('-', '/', $date1);
    
    
    $final_start_date = str_replace('/', '-', $input_date);
    $mod_final_start_date=date("Y-m-d", strtotime($final_start_date));
    
    $final_end_date = $mod_friday7;
	
    
    
    
    function getDatesFromRange($start, $end, $format = 'Y-m-d') {//Get all the dates from course start date to course final end date
        $array = array();
        $interval = new DateInterval('P1D');
    
        $realEnd = new DateTime($end);
        $realEnd->add($interval);
    
        $period = new DatePeriod(new DateTime($start), $interval, $realEnd);
    
        foreach($period as $date) { 
            $array[] = $date->format($format); 
        }
    
        return $array;
    }
    
    // Call the function
    $datesa = getDatesFromRange($mod_final_start_date, $final_end_date);//call the above function with course start date and course final end date 
    
    
    $search_date = DateTime::createFromFormat("Y-m-d", $mod_final_start_date);
    $ser=$search_date->format("Y-12-20");//search if the course start date start in december
    
    if(in_array($ser,$datesa ))//if comes in december 20 then increse the date with 28 days
    	{
    		$mod_final_end_date=date('Y-m-d', strtotime($final_end_date.' + 28days'));
    	}
    	else
    	{
    		$mod_final_end_date=date('Y-m-d', strtotime($final_end_date.' + 0days'));
    	}
	//echo "MODIFIED FINAL DATE IS=".$mod_final_end_date."</br>";
	$mod_final_end_date_check=date('m-d', strtotime($mod_final_end_date));
	//echo "MODIFIED FINAL DATE CHECK IS=".$mod_final_end_date_check."</br>";
	if($mod_final_end_date_check=='06-30')
	{
	$mod_final_end_date1=date('Y-m-d', strtotime($mod_final_end_date.' - 7days'));
	}
	else
	{
	$mod_final_end_date1=date('Y-m-d', strtotime($mod_final_end_date.' - 0days'));
	}
	$mod_final_end_date1_format = str_replace('/', '-', $mod_final_end_date1);
    $mod_final_end_date1_format1=date('d/m/Y', strtotime($mod_final_end_date1_format));
	
	
	//`TO_DATE`='$mod_final_end_date1',
	if($_POST['txtfld1']==$_POST['S_DATE'])
	{
	$mod_final_end_date1_update=$_POST['EDIT_FINISH_DATE'];
	}
	else
	{
	$mod_final_end_date1_update=$mod_final_end_date1;
	}
	//echo "END DATE UPDATE=".$mod_final_end_date1_update."</br>";
	
	if($_POST['CUSTOM_RATE'])
	{
	$commission_NO='';
	}
	else
	{
	$commission_NO=$_POST['AGENT_COMMISSION_NO'];
	}
	if($_POST['DISCOUNT']==1)
    {
    $disc_amount=$_POST['DISCOUNT_RATE'];
    }
    else if($_POST['DISCOUNT']==2)
    { 
    $disc_percent=$_POST['DISCOUNT_RATE'];
    }
	
  if($holiday=='2' && $_POST['DESCRIPTION'])
    {
    			 mysql_query("update `offer_item` set `OFFER_NO`='$s',`CUSTOMER_NO`='".$_GET['cust_no']."',`PRODUCT_NO`='".$_GET['PRODUCT_NO']."',`PARENT_NO`='',`REF_NO`='".$_GET['REFERENCE_NO']."',`REF_TYPE_NO`='',`DESCRIPTION`='".$_POST['DESCRIPTION']."',`FROM_DATE`='$da_for',`TO_DATE`='$mod_final_end_date1_update',`PERIOD_LENGTH`='".$_GET['len']."',`NOTES`='".$_POST["NOTE"]."',`AGENT_COMMISSION_NO`='$commission_NO',`COMMISSION_AMOUNT`='$final_commission',`AMOUNT`='$amount',`DISC_AMOUNT`='$disc_amount',`SUB_AMOUNT`='".$_POST['TOTAL_AMOUNT']."',`TAX_AMOUNT`='$afte_add_tax_commission',`TOTAL_AMOUNT`='".$_POST['TOTAL_AMOUNT']."',`CUSTOM_1`='".$_POST['CUSTOM_1']."',`CUSTOM_2`='".$_POST['CUSTOM_2']."',`CUSTOM_3`='".$_POST['CUSTOM_3']."',`CUSTOM_4`='".$_POST['CUSTOM_4']."',`CUSTOM_5`='".$_POST['CUSTOM_5']."',`CREATE_DATE`='".$_POST['CREATE_DATE']."',`DELETE_DATE`='',`LOCK_NUM`='".$_GET['LOCK_NUM']."',`PROVIDER_CUSTOMER_NO`='',`PRODUCT_RATE_NO`='',`PRICE_BOOK_NO`='',`CREATE_ENTRY_FLAG`='',`OPTION_NO`='',`UPDATE_DATE`='',`DISPLAY_ORDER`='',`DISC_PERCENT`='$disc_percent' where `OFFER_ITEM_NO`=".$_GET['offer_item_no']);
    				$_SESSION['s_msg']="<strong>Well done!</strong>Course Offer Updated Successfully."; 
    			
    				
             }  else {
                    $_SESSION['e_msg']="<strong>Oh snap!</strong>Course Offer Not Updated"; 
                   
                  
    } 
   
    $fin=$_GET['len'];
    $fin1=($fin*5);
 
 
$cond="where `OFFER_ITEM_NO`=".$_GET['offer_item_no'];
$of_item=getRows($cond,'offer_item'); 
?>
<script>
    function update(){
    	leng="<?php echo $fin1;?>";
        var days28 = new Date( document.forms.datepick.txtfld1.value ).getTime() + ( leng * 24 * 60 * 60 * 1000),
        fd = new Date( days28 ),
        m = fd.getMonth()+1,
        d = fd.getDate();
        document.forms.datepick.txtfld2.value = [fd.getFullYear(),(m<10?'0':'')+m,(d<10?'0':'')+d].join("-");
    
    }
</script>
<script language="javascript" type="text/javascript" src="datetimepicker.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('#sccb').click(function(){
            if (this.checked) {
                $('#cns').removeAttr("disabled");
                $('#cns2').removeAttr("disabled");
                $('#cns3').removeAttr("disabled");
    			$('#cns4').removeAttr("disabled");
    			$('#cns5').removeAttr("disabled");
            } else {
                $("#cns").attr("disabled", true);
                $("#cns2").attr("disabled", true);
                $("#cns3").attr("disabled", true);
    			$("#cns4").attr("disabled", true);
    			$("#cns5").attr("disabled", true);
            }
        });
    });
</script>
<div role="main" class="right_col" style="min-height: 738px;">
     <div class="">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="custom-alrt">
                <?php 
                    if($_SESSION['s_msg'] || $_SESSION['e_msg'])
                    {
                    ?>
                <div class="alert <?=($_SESSION['s_msg'])?'alert-success':'alert-error'?> fade in">
                    <button data-dismiss="alert" class="close" type="button"><i class="fa fa-times" aria-hidden="true"></i></button>
                    <?=($_SESSION['s_msg'])?$_SESSION['s_msg']:$_SESSION['e_msg']?>
                </div>
                <?php
                    unset($_SESSION['s_msg']);
                    unset($_SESSION['e_msg']);
                    }
                    ?>
            </div>
            <div class="page-title">
                <div class="title_left">
                    <h3>Offer Item - Course</h3>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="clearfix"></div>
                    <div class="x_content">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel recent-app">
                                    <div class="x_content">
                                        <div class="student-account-page">
                                            <!-- Nav tabs -->
                                            <div class="card">
                                                <div class="all-students-list add student">
                                                    <div class="add-student-section">
                                                        <form  method="POST" action="" name="datepick">
                                                            <ul role="tablist" class="nav nav-tabs" id="student-acc-tab">
                                                                <li role="presentation" class="active"></li>
                                                            </ul>
                                                            <div class="tab-content">
                                                                <!--Offer-->
                                                                <div id="offer" class="tab-pane active" role="tabpanel">
                                                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                                                        <div class="row">
                                                                            <h4>
                                                                                <fieldset>Course Details</fieldset>
                                                                            </h4>
                                                                            <div class="col-md-5 col-sm-5 col-xs-12">
                                                                                <?php date_default_timezone_set('Australia/Melbourne');
                                                                                    $date = date('d/m/Y h:i:s', time());
                                                                                    
                                                                                    ?>
                                                                                <input type="hidden" class="form-control col-md-12 col-xs-12" name="CREATE_DATE" value="<?php echo $date?>">
                                                                                <div class="form-group">
                                                                                    <label for="name" class="pull-left control-label col-md-6 col-sm-6 col-xs-12">Code
                                                                                    </label>
                                                                                    <input type="text" value="<?=$_GET['code']?>" class="form-control col-md-12 col-xs-12" placeholder="Course code" name="COURSE_CODE">
                                                                                </div>
                                                                            </div>
                                                                            <?php
                                                                                $p=0;
                                                                                if($s)
                                                                                {
                                                                                 $grp_field="where `OFFER_NO`=".$s;
                                                                                 $grp=getRows($grp_field,'offer_item');
                                                                                 foreach($grp as $grp1)
                                                                                 {
                                                                                 if($grp1['GROUP_NO']==null)
                                                                                 {
                                                                                  $group_no=$p;
                                                                                 }
                                                                                 else
                                                                                 {
                                                                                  $group_no=$grp1['GROUP_NO'];
                                                                                 }
                                                                                }
                                                                                }
                                                                                ?>
                                                                            <input type="hidden" value="<?php echo $group_no+1;?>" class="form-control col-md-12 col-xs-12" placeholder="Group No" name="GROUP_NO">
                                                                            <div class="col-md-5 col-sm-5 col-xs-12">
                                                                                <div class="form-group">
                                                                                    <label for="name" class="pull-left control-label col-md-4 col-sm-4 col-xs-12">Course</label>
                                                                                    <input type="text" value="<?=$_GET['name']?>" class="form-control col-md-12 col-xs-12" placeholder="Course Name" name="COURSE_NAME">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-2 col-sm-2 col-xs-12">
                                                                                <div class="form-group">
                                                                                    <label for="name" class="pull-left control-label col-md-4 col-sm-4 col-xs-12"></label><br><br>
                                                                                    <a style="margin-top: -15px;" class="btn btn-success" href="#" onclick="javascript:void window.open('dashboard.php?op=<?=MD5('display_offer_course_edit')?>&of_no=<?=$_GET['of_no1']?>&offer_item_no=<?=$_GET['offer_item_no']?>&of_no1=<?=$_GET['of_no']?>&stu_no=<?=$_GET['stu_no']?>&sname=<?=$_GET['sname']?>&cust_no=<?=$_GET['cust_no']?>&AGENT_NO=<?=$_SESSION['AGENT_NO']?>&aname=<?=$_GET['aname']?>','147123400000','width=800,height=600,toolbar=0,menubar=0,location=1,status=1,scrollbars=1,resizable=1,left=0,top=0');return false;">SELECT</a>
                                                                                </div>
                                                                            </div>
                                                                            <?php
                                                                                if($_GET['faculty'])
                                                                                {
                                                                                $cond="where `CRT_NO`=".$_GET['faculty'];
                                                                                 $row=getRows($cond,'crse_type');
                                                                                }
                                                                                ?>
                                                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                                                <div class="form-group">
                                                                                    <label for="name" class="pull-left control-label col-md-12 col-sm-12 col-xs-12">Faculty
                                                                                    </label>
                                                                                    <input type="text" value="<?=$row[0]['CRT_NAME']?>" placeholder="Faculty Name" class="date-picker form-control col-md-12 col-xs-12 active" name="FACULTY" disabled>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                                                <div class="form-group">
                                                                                    <label for="name" class="pull-left control-label col-md-12 col-sm-12 col-xs-12">Location
                                                                                    </label>
                                                                                    <input type="text" value="Sydney" class="form-control col-md-12 col-xs-12" placeholder="Location" name="LOCATION" disabled>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                                                <div class="form-group">
                                                                                    <label for="name" class="pull-left control-label col-md-12 col-sm-12 col-xs-12">Start
                                                                                    </label>
                                                                                    <?php $date2 = date( "j-M-Y") ?>
                                                                                    <input type="date" name="txtfld1" value="<?=$of_item[0]['FROM_DATE']?>" onChange="update();" class="form-control col-md-12 col-xs-12">
																					 <input type="hidden" name="S_DATE" value="<?=$of_item[0]['FROM_DATE']?>" class="form-control col-md-12 col-xs-12">
                                                                                </div>
                                                                            </div>
                                                                            <!-- <div class="col-md-2 col-sm-2 col-xs-12">
                                                                                <div class="form-group">
                                                                                <label for="name" class="pull-left control-label col-md-4 col-sm-4 col-xs-12"></label><br><br>
                                                                                <a href="javascript:NewCal('demo50','ddmmmyyyy')"><img src="images/cal.gif" width="16" height="16" border="0" alt="Pick a date"></a>
                                                                                </div>
                                                                                                                                       </div>
                                                                            <div class="col-md-2 col-sm-2 col-xs-12">
                                                                                <div class="form-group">
                                                                                    <label for="name" class="pull-left control-label col-md-12 col-sm-12 col-xs-12" >Length
                                                                                    </label>  -->
                                                                                    <input type="hidden" value="<?=$_GET['len']?>" class="form-control col-md-12 col-xs-12" placeholder="Location" name="LENGTH" disabled>
                                                                           <!--     </div>
                                                                            </div>
                                                                               <div class="col-md-3 col-sm-3 col-xs-12">
                                                                                <div class="form-group">
                                                                                    <label for="name" class="pull-left control-label col-md-12 col-sm-12 col-xs-12">Finish Before Add Holidays
                                                                                    </label> -->
                                                                                    <?php
                                                                                        // $fin=$_GET['len'];
                                                                                        // $fin1=$fin*2*5;
                                                                                        // $date1 = date( "j-M-Y",strtotime('+'.$fin1. 'day'));												  ?>
                                                                                    <?php  //$date = new DateTime($da_for_en);
                                                                                        //$date->modify('+'.$in. 'day');
                                                                                        //echo "After Holiday". date("d/m/Y", strtotime($new_end_date)); ?>
                                                                                    <input type="hidden" name="txtfld2" value="<?php echo date("Y-m-d");?>" class="form-control col-md-12 col-xs-12">
                                                                                    <!-- After HOLIDAY<input type="date" value="<?php echo date("m/d/Y", strtotime($new_end_date));?>" class="form-control col-md-12 col-xs-12"> -->
                                                                             <!--    </div>
                                                                            </div> -->
																			 
																			 <div class="col-md-6 col-sm-6 col-xs-12">
                                                                                <div class="form-group">
                                                                                    <label for="name" class="pull-left control-label col-md-12 col-sm-12 col-xs-12">Finish Date
                                                                                    </label>
                                                                                    <input type="hidden" name="EDIT_FINISH_DATE" value="<?=$of_item[0]['TO_DATE'];?>" class="form-control col-md-12 col-xs-12">
																					<input type="text"  value="<?php echo date('d/m/Y',strtotime($of_item[0]['TO_DATE']));?>" class="form-control col-md-12 col-xs-12">
                                                                                </div>
                                                                            </div>
																			
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                                                        <div class="row">
                                                                            <div class="col-md-12 col-sm-12">
                                                                                <div class="row">
                                                                                    <fieldset>
                                                                                        <h4>Item Description</h4>
                                                                                    </fieldset>
                                                                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                                                                        <div class="item form-group">
                                                                                            <label class="control-label col-md-12 col-sm-12 col-xs-12">
                                                                                            </label>
                                                                                            <div class="row">
                                                                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                                                                    <input type="text"value="<?=$_GET['code']?>-<?=$_GET['name']?>, (<?=$_GET['len']?> Weeks)"  class="date-picker form-control col-md-12 col-xs-12 active" name="DESCRIPTION" >
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                   <div class="col-md-12 col-sm-12 col-xs-12">
                                                                        <div class="row">
                                                                            <div class="col-md-12 col-sm-12">
                                                                                <div class="row">
                                                                                    <fieldset>
                                                                                        <h4>Custom Field</h4>
                                                                                    </fieldset>
                                                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                                                        <div class="item form-group">
                                                                                            <input type="text"  class="date-picker form-control active" name="CUSTOM_1" >
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                                                        <div class="form-group">
                                                                                            <input type="text" class="form-control"  name="CUSTOM_2" >
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                                                        <div class="form-group">
                                                                                            <input type="text" class="form-control " name="CUSTOM_3" >
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                                                        <div class="form-group">
                                                                                            <input type="text" class="form-control " name="CUSTOM_4" >
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                                                        <div class="form-group">
                                                                                            <input type="text" class="form-control " name="CUSTOM_5" >
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                                                        <div class="form-group">
                                                                                            <input type="text" class="form-control " name="CUSTOM_6" >
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                                                        <div class="form-group">
                                                                                            <input type="text" class="form-control " name="CUSTOM_7" >
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                                                        <div class="form-group">
                                                                                            <input type="text" class="form-control "  name="CUSTOM_8" >
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                                                        <div class="form-group">
                                                                                            <input type="text" class="form-control " name="CUSTOM_9" >
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                                                        <div class="form-group">
                                                                                            <input type="text" class="form-control " name="CUSTOM_10" >
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                   <div class="col-md-12 col-sm-12 col-xs-12">
                                                                        <div class="row">
                                                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                                                <div class="row">
                                                                                    <div class="col-md-12 col-sm-12">
                                                                                        <div class="row">
                                                                                            <fieldset id="group1">
                                                                                                <h4>Billing Rate</h4>
                                                                                            </fieldset>
                                                                                            <div class="row">
                                                                                                <div class="col-md-4 col-sm-4 col-xs-4">
                                                                                                    <div class="item form-group">
                                                                                                        <input type="radio" name="group1" style="width:20px; height:20px;" checked="checked" onclick="javascript: showCost('cost',<?=$_GET['cost']?>);">Default
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="col-md-4 col-sm-4 col-xs-4">
                                                                                                    <div class="item form-group">
                                                                                                        <input type="text" name="COST" class="form-control col-md-12 col-xs-12" value="<?=$_GET['cost']?>" readonly>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="row">
                                                                                                <div class="col-md-4 col-sm-4 col-xs-4">
                                                                                                    <div class="item form-group">
                                                                                                        <input type="radio" name="group1" style="width:20px; height:20px;">Alternate
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="col-md-4 col-sm-4 col-xs-4">
                                                                                                    <div class="item form-group">
                                                                                                        <input type="text" name="ALTERNATE" class="form-control col-md-12 col-xs-12" readonly value="">
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="row">
                                                                                                <div class="col-md-4 col-sm-4 col-xs-4">
                                                                                                    <div class="item form-group">
                                                                                                        <input type="radio" name="group1" style="width:20px; height:20px;" onclick="javascript: showCost('customcost','');">Custom
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="col-md-4 col-sm-4 col-xs-4">
                                                                                                    <div class="item form-group">
                                                                                                        <input type="text" name="CUSTOM" class="form-control col-md-12 col-xs-12" onkeyup="javascript: updateAmount(this.value);">
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                                                <div class="row">
                                                                                    <div class="col-md-12 col-sm-12">
                                                                                        <div class="row">
                                                                                            <fieldset id="group2">
                                                                                                <h4>Agent Commission</h4>
                                                                                            </fieldset>
                                                                                            <div class="row">
                                                                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                                                                    <div class="item form-group">
                                                                                                        <label for="chkPassport">
                                                                                                        <input class="sccb" type="checkbox" value="<?= $_GET['commission'] ?>" name="offer_date" style="width:20px; height:20px;" <?php echo ($of_item[0]['COMMISSION_AMOUNT'] ? 'checked' : '');?>>Apply Agent Commission </label>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="row">
                                                                                                <div class="col-md-4 col-sm-4 col-xs-4">
                                                                                                    <div class="item form-group">
                                                                                                        <input onclick="document.getElementById('custom').disabled = false; document.getElementById('charstype').disabled = true;" class="cns" type="radio" name="group2" style="width:20px; height:20px;" <?php if($of_item[0]['AGENT_COMMISSION_NO']||$of_item[0]['COMMISSION_AMOUNT']){ echo 'checked="checked"';}?>>Default Rate
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="col-md-4 col-sm-4 col-xs-4">
                                                                                                    <div class="item form-group">
                                                                                                        <select id="custom" class="form-control col-md-12 col-xs-12 cns2" name="RATE" onchange="commissionUser(this.value)">
                                                                                                            <option></option>
                                                                                                            <?php
                                                                                                                $sq="select * from agent_commission where AGENT_NO=".$AGENT_NO;
                                                                                                                $qe=mysql_query($sq);
                                                                                                                while($ro=mysql_fetch_array($qe))
                                                                                                                {
                                                                                                           if($of_item[0]['AGENT_COMMISSION_NO']==$ro['AGENT_COMMISSION_NO'])
																										 {
																										   echo "<option value='$ro[AGENT_COMMISSION_NO]' selected> $ro[NAME] </option>";
																										   }
																											else
																											{
																											 echo "<option value='$ro[AGENT_COMMISSION_NO]'> $ro[NAME] </option>";
																											  }
                                                                                                           
                                                                                                          }  ?>
                                                                                                        </select>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="col-md-4 col-sm-4 col-xs-4">
                                                                                                    <div class="item form-group">
                                                                                                        <div id="txtHint">
																										<?php if($of_item[0]['AGENT_COMMISSION_NO'] and $of_item[0]['COMMISSION_AMOUNT']){
																										?>
																										<input  type="text" name="RATE_PERCENT"  class="form-control col-md-12 col-xs-12 cns5" value="<?php echo (($of_item[0]['COMMISSION_AMOUNT']-$of_item[0]['TAX_AMOUNT'])*100)/$of_item[0]['AMOUNT'];?>">
																										<?php } ?>
																										
																										<input  type="hidden" name="AGENT_COMMISSION_NO" class="form-control col-md-12 col-xs-12 cns5" value="<?=$of_item[0]['AGENT_COMMISSION_NO'];?>">
																										</div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="row">
                                                                                                <div class="col-md-4 col-sm-4 col-xs-4">
                                                                                                    <div class="item form-group">
                                                                                                        <input onclick="document.getElementById('custom').disabled = true; document.getElementById('charstype').disabled = false;" type="radio" name="group2" style="width:20px; height:20px;" class="cns4" <?php if(!$of_item[0]['AGENT_COMMISSION_NO'] and $of_item[0]['COMMISSION_AMOUNT']){ echo 'checked="checked"';}?>>Custom Rate
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="col-md-4 col-sm-4 col-xs-4">
                                                                                                    <div class="item form-group">
                                                                                                        <input id="charstype"  type="text" name="CUSTOM_RATE" class="form-control col-md-12 col-xs-12 cns5" value="<?php if(!$of_item[0]['AGENT_COMMISSION_NO'] and $of_item[0]['COMMISSION_AMOUNT']){ echo (($of_item[0]['COMMISSION_AMOUNT']-$of_item[0]['TAX_AMOUNT'])*100)/$of_item[0]['AMOUNT']; }?>">
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                                                        <div class="row">
                                                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                                                <fieldset>
                                                                                    <h4>Notes</h4>
                                                                                </fieldset>
                                                                                <div class="form-group">
                                                                                    <textarea class="form-control" name="NOTE" cols="60" rows="4"></textarea>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                                                <fieldset>
                                                                                    <h4>Billing Amount</h4>
                                                                                </fieldset>
                                                                                <div class="row">
                                                                                    <label for="name" class="pull-left control-label col-md-4 col-sm-4 col-xs-12">Amount
                                                                                    </label>
                                                                                    <div class="col-md-8 col-sm-8 col-xs-12">
                                                                                        <div class="form-group">
                                                                                            <input id="input1" type="text" name="AMOUNT" value="<?=$_GET['cost']?>" class="form-control" readonly>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="row">
                                                                                    <label for="name" class="pull-left control-label col-md-4 col-sm-4 col-xs-12">Discount
                                                                                    </label>
                                                                                    <div class="col-md-8 col-sm-8 col-xs-12">
                                                                                        <div class="row">
                                                                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                                                                <div class="item form-group">
                                                                                                    <select class="form-control col-md-3 col-sm-3 col-xs-6" id="operation"  name="DISCOUNT" onchange="javascript: countTotal(this.value);">
                                                                                                        <option value="1" <?php if ($of_item[0]['DISC_AMOUNT']) { echo 'selected="selected"';}?>>$</option>
                                                                                                        <option value="2" <?php if ($of_item[0]['DISC_PERCENT']) { echo 'selected="selected"';}?>>%</option>
                                                                                                    </select>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                                                                <div class="item form-group">
                                                                                                    <input id="input2" type="text" value="<?php if ($of_item[0]['DISC_AMOUNT']){ echo $of_item[0]['DISC_AMOUNT'];} else if($of_item[0]['DISC_PERCENT']){echo $of_item[0]['DISC_PERCENT'];}?> " name="DISCOUNT_RATE" class="form-control" onkeyup="javascript: countTotal(this.value);">
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="row">
                                                                                    <label for="name" class="pull-left control-label col-md-4 col-sm-4 col-xs-12">GST %
                                                                                    </label>
                                                                                    <div class="col-md-8 col-sm-8 col-xs-12">
                                                                                        <div class="row">
                                                                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                                                                <?php 
																									$tax_query=mysql_query("SELECT * FROM gst_rate");
																									$tax_row=mysql_fetch_array($tax_query);
																									?>
																								<div class="item form-group">
																									<select class="form-control col-md-3 taxValueDrop" id="operation"  name="TaxValue" onchange="javascript: tax(this.value);">
																										<option value="0" <?=($of_item[0]['TAX_AMOUNT']==0)?'selected':''?>>Exclude GST</option>
																										<option value="<?=$tax_row['GST_RATE']?>" <?=($of_item[0]['TAX_AMOUNT']!=0)?'selected':''?>>Include GST</option>
																									</select>
																								</div>
                                                                                            </div>
                                                                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                                                                <div class="item form-group">
                                                                                                    <input type="text" name="TAX_AMOUNT" id="TAX" class="form-control" readonly value="<?php if($of_item[0]['TAX_AMOUNT']==0){ echo '0';}else{ echo $tax_row['GST_RATE'];}?>" />
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="row">
                                                                                    <label for="name" class="pull-left control-label col-md-4 col-sm-4 col-xs-12">Total
                                                                                    </label>
                                                                                    <div class="col-md-8 col-sm-8 col-xs-12">
                                                                                        <div class="item form-group">
                                                                                            <input readonly id="output" type="text" name="TOTAL_AMOUNT" value="<?=$_GET['cost']?>" class="form-control" readonly>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                                                        <div class="row">
                                                                            <div class="col-md-12 col-sm-12">
                                                                                <div class="row">
                                                                                    <div class="item form-group">
                                                                                        <input type="checkbox" name="RULES" style="width:20px; height:20px;">Run Fee Rules After Closing
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                 </div>
                                                                <!--Offer-->
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <center><button class="btn btn-success" type="submit">Save</button></center>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
  function recalculateSum()
    { 
        var length = parseInt(document.getElementById("length").value);
        var sub_amount = parseInt(document.getElementById("sub_amount").value);
        document.getElementById("amount").value = length * sub_amount;
        document.getElementById("total_amount").value = length * sub_amount;

    }
    // this function calculates the start date and finish date when the course type is weekly one 
    function doCalcu(val)
    { 
       var days = document.getElementById('length').value*7;
     
      var newfinishDate = new Date(new Date(val).getTime()+(days*24*60*60*1000));
      // 01, 02, 03, ... 29, 30, 31
   var dd = (newfinishDate.getDate() < 10 ? '0' : '') + newfinishDate.getDate();
   // 01, 02, 03, ... 10, 11, 12
   var MM = ((newfinishDate.getMonth() + 1) < 10 ? '0' : '') + (newfinishDate.getMonth() + 1);
   // 1970, 1971, ... 2015, 2016, ...
   var yyyy = newfinishDate.getFullYear();

   // create the format you want
 //  newfinishDateformat = (MM + "/" + dd + "/" + yyyy);
 newfinishDateformat = ( yyyy+ "-" + MM + "-" +dd );
  //alert(newfinishDateformat);
       // alert(newfinishDateformat);
      document.getElementById('finish_date').value = newfinishDateformat;

    }
     // this function calculates the start date and finish date when the course type is weekly one 
    function doCalcu2(week)
    { 
       var val = document.getElementById('start_date').value;
       if(val=='') { alert('Please select start date!'); return false; }
     var days = week*7;
      var newfinishDate = new Date(new Date(val).getTime()+(days*24*60*60*1000));
      // 01, 02, 03, ... 29, 30, 31
   var dd = (newfinishDate.getDate() < 10 ? '0' : '') + newfinishDate.getDate();
   // 01, 02, 03, ... 10, 11, 12
   var MM = ((newfinishDate.getMonth() + 1) < 10 ? '0' : '') + (newfinishDate.getMonth() + 1);
   // 1970, 1971, ... 2015, 2016, ...
   var yyyy = newfinishDate.getFullYear();

   // create the format you want
 //  newfinishDateformat = (MM + "/" + dd + "/" + yyyy);
 newfinishDateformat = ( yyyy+ "-" + MM + "-" +dd );
  //alert(newfinishDateformat);
       // alert(newfinishDateformat);
      document.getElementById('finish_date').value = newfinishDateformat;

    }

	function countTotal() {
	  var num1 = document.getElementById("amount").value;
	  var num2 = document.getElementById("input2").value;
	  //var rate = parseInt(document.getElementById("operation1").value);
	   //alert(num2);
	  //return false;
	  if(document.getElementById("operation").value==1)
	  {
	  var res=(num1-num2);
	  document.getElementById('total_amount').value = eval(res);
	  }
	  else
	  if(document.getElementById("operation").value==2)
	  {
	  var res=((num1*num2)/100);
	  
	  document.getElementById('total_amount').value = eval(num1-res);
	  }
	 
	}
	
	function tax()
    {
        countTotal();
	  var num1 = document.getElementById("total_amount").value;
	  var rate = parseInt(document.getElementById("operation1").value); 
	  if(document.getElementById("operation1").value)
	  {
	   var add_gst=((num1*rate)/100);
	     var result = parseInt(num1) + parseInt(add_gst);
       if (!isNaN(result)) {
           document.getElementById('total_amount').value = result;
       }
	  }
    }
	
	function edit_count() {
		
	  var num1 = document.getElementById("input1").value;
	  var num3 = document.getElementById("input2").value;
	  var num4 = document.getElementById("operation").value;
	  
	  //alert(num1);
	 // return false;
	 // var rate = parseInt(document.getElementById("operation1").value);
	
	  if(document.getElementById("operation").value==1)
	  {
	  var res=(num1-num3);
	  document.getElementById('operation').value = eval(res);
	  
	  }
	  else
	  if(document.getElementById("operation").value==2)
	  {
	  var res=((num1*num3)/100);
	 // alert(res);
	  // return false;
	  document.getElementById('output').value = eval(num1-res);
	  }
	 
	}
	
	function edit_tax()
    {
	  var num1 = document.getElementById("input1").value;
	  var rate = parseInt(document.getElementById("operation4").value); 
	  if(document.getElementById("operation4").value)
	  {
	   var add_gst=((num1*rate)/100);
	     var result = parseInt(num1) + parseInt(add_gst);
       if (!isNaN(result)) {
           document.getElementById('output').value = result;
       }
	  }
    }
	
	
	function showCost(id , val)
        {
            document.getElementById('customcost').value='';
            if(val!='')
            {
            document.getElementById(id).value = val;
            document.getElementById('amount').value = val;
            countTotal();
            tax();
            }
        else
        {
            document.getElementById('amount').value = '';
            document.getElementById('total_amount').value = '';
        }
        
        }
        
        function updateAmount(val)
        {
           
            document.getElementById('amount').value = val;
            countTotal();
            tax();
            
        }
	
</script>

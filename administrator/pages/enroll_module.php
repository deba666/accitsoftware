<?php
    /*
        * @package    ACCIT
         * @author    Paperlink Softwares Team
         * @copyright    Copyright (c) 2016 , Paperlink Softwares Pvt. Ltd.  (http://www.paperlinksoftwares.com/)
         * @since    Version 1.0.0
         * @filesource
         */
    ?>
<div role="main" class="right_col" style="min-height: 648px;">
    <div class="">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="page-title">
                <div class="title_left">
                    <h3>Module Enrollment</h3>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="clearfix"></div>
                    <div class="x_content">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel recent-app">
                                    <div class="x_content">
                                        <div class="student-account-page view-faculty-page">
                                            <!-- Nav tabs -->
                                            <div class="card">
                                                <div class="all-students-list add student">
                                                    <div class="add-student-section">
                                                        <div class="table-responsive">
                                                            <table class="table data-tbl-tools" cellspacing="0" width="100%">
                                                                <thead>
                                                                    <tr class="headings" role="row">
                                                                        <th class="column-title sorting_asc" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1" style="width: 88px;" aria-sort="ascending" aria-label="Student No.
                                                                            : activate to sort column descending">Student No.
                                                                        </th>
                                                                        <th class="column-title sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1" style="width: 88px;" aria-label="First Name
                                                                            : activate to sort column ascending">Mother Tongue
                                                                        </th>
                                                                        <th class="column-title sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1" style="width: 88px;" aria-label="Middle Name
                                                                            : activate to sort column ascending">Arrived
                                                                        </th>
                                                                        <th class="column-title sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1" style="width: 88px;" aria-label="Last Name
                                                                            : activate to sort column ascending">First Name
                                                                        </th>
                                                                        <th class="column-title sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1" style="width: 88px;" aria-label="Gender
                                                                            : activate to sort column ascending">Middle Name
                                                                        </th>
                                                                        <th class="column-title sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1" style="width: 88px;" aria-label="DOB
                                                                            : activate to sort column ascending">Last Name
                                                                        </th>
                                                                        <th class="column-title sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1" style="width: 114px;" aria-label="Email
                                                                            : activate to sort column ascending">Course Code
                                                                        </th>
                                                                        <th class="column-title sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1" style="width: 114px;" aria-label="Email
                                                                            : activate to sort column ascending">Course Name
                                                                        </th>
                                                                        <th class="column-title sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1" style="width: 114px;" aria-label="Email
                                                                            : activate to sort column ascending">Level
                                                                        </th>
                                                                        <th class="column-title sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1" style="width: 114px;" aria-label="Email
                                                                            : activate to sort column ascending">Faculty
                                                                        </th>
                                                                        <th class="column-title sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1" style="width: 114px;" aria-label="Email
                                                                            : activate to sort column ascending">Location
                                                                        </th>
                                                                        <th class="column-title sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1" style="width: 114px;" aria-label="Email
                                                                            : activate to sort column ascending">Start
                                                                        </th>
                                                                        <th class="column-title sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1" style="width: 114px;" aria-label="Email
                                                                            : activate to sort column ascending">End
                                                                        </th>
                                                                        <th class="column-title sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1" style="width: 114px;" aria-label="Email
                                                                            : activate to sort column ascending">Status
                                                                        </th>
                                                                        <th class="column-title sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1" style="width: 114px;" aria-label="Email
                                                                            : activate to sort column ascending">Lessons
                                                                        </th>
                                                                        <th class="column-title sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1" style="width: 114px;" aria-label="Email
                                                                            : activate to sort column ascending">DOB
                                                                        </th>
                                                                        <th class="column-title sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1" style="width: 114px;" aria-label="Email
                                                                            : activate to sort column ascending">Age
                                                                        </th>
                                                                        <th class="column-title sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1" style="width: 114px;" aria-label="Email
                                                                            : activate to sort column ascending">Birth Month
                                                                        </th>
                                                                        <th class="column-title sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1" style="width: 114px;" aria-label="Email
                                                                            : activate to sort column ascending">Gender
                                                                        </th>
                                                                        <th class="column-title sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1" style="width: 114px;" aria-label="Email
                                                                            : activate to sort column ascending">Country
                                                                        </th>
                                                                        <th class="column-title sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1" style="width: 114px;" aria-label="Email
                                                                            : activate to sort column ascending">Agent
                                                                        </th>
                                                                        <th class="column-title sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1" style="width: 114px;" aria-label="Email
                                                                            : activate to sort column ascending">Current Att
                                                                        </th>
                                                                        <th class="column-title sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1" style="width: 114px;" aria-label="Email
                                                                            : activate to sort column ascending">Overall Att
                                                                        </th>
                                                                        <th class="column-title sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1" style="width: 114px;" aria-label="Email
                                                                            : activate to sort column ascending">Outstanding Fees
                                                                        </th>
                                                                        <th class="column-title sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1" style="width: 114px;" aria-label="Email
                                                                            : activate to sort column ascending">Visa Type
                                                                        </th>
                                                                        <th class="column-title sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1" style="width: 114px;" aria-label="Email
                                                                            : activate to sort column ascending">Certificate Awarded
                                                                        </th>
                                                                        <th class="column-title sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1" style="width: 114px;" aria-label="Email
                                                                            : activate to sort column ascending">Enrolment Form Signed
                                                                        </th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <?php 
                                                                        $sql="select * from enrol";
                                                                                                                  $q=mysql_query($sql);
                                                                                                                  while($row=mysql_fetch_array($q))
                                                                                                                  {
                                                                        	if($row['STUD_NO'])
                                                                        	{
                                                                                                              ?>                                   
                                                                    <tr class="pointer odd" role="row">
                                                                        <?php
                                                                            $cond="where `STUD_NO`=".$row['STUD_NO'];
                                                                            $std_row=getRows($cond,'student');
                                                                            ?>
                                                                        <td class="sorting_1" tabindex="0"><?=$std_row[0]['EXT_STN'];?></td>
                                                                        <?php if($std_row[0]['LANGUAGE_NO'])
                                                                            {
                                                                            $cond1="where `LANGUAGE_NO`=".$std_row[0]['LANGUAGE_NO'];
                                                                                  $lan=getRows($cond1,'language');
                                                                            }?>
                                                                        <td class=" "><?=$lan[0]['LANGUAGE_DESC'];?></td>
                                                                        <td class=" "><input type="hidden" name="ARVD" id="course_publish" value="0" >
                                                                            <input type="checkbox" name="ARVD" value="1" <?php echo ($row['ARVD'] ? 'checked' : '');?>/>
                                                                        </td>
                                                                        <td class=" "><?=$std_row[0]['FNAME'];?></td>
                                                                        <td class=" "><?=$std_row[0]['MNAME'];?></td>
                                                                        <td class=" "><?=$std_row[0]['LNAME'];?></td>
                                                                        <?php
                                                                            $cond2="where `COURSE_NO`=".$row['COURSE_NO'];
                                                                            $std_row2=getRows($cond2,'course');
                                                                            ?>
                                                                        <td class=" "><?=$std_row2[0]['CODE'];?></td>
                                                                        <td class=" "><?=$std_row2[0]['COURSE_NAME'];?></td>
                                                                        <td class=" "><?=$row['agent_name'];?></td>
                                                                        <?php
                                                                            $cond3="where `CRT_NO`=".$row['CRT_NO'];
                                                                            $std_row3=getRows($cond3,'crse_type');
                                                                            ?>
                                                                        <td class=" "><?=$std_row3[0]['CRT_NAME'];?></td>
                                                                        <?php
                                                                            $cond4="where `LOCATION_NO`=".$row['LOCATION_NO'];
                                                                            $std_row4=getRows($cond4,'location');
                                                                            ?>
                                                                        <td class=" "><?=$std_row4[0]['LOCATION_NAME'];?></td>
                                                                        <td class=" "><?=$row['ST_DATE'];?></td>
                                                                        <td class=" "><?=$row['END_DATE'];?></td>
                                                                       <?php $date=date('Y-m-d'); ?>
                                                                        <td class=" "><?=check_in_range($row['ST_DATE'], $row['END_DATE'], $date);?></td>
                                                                        <td class=" "><?=$row['offer_create'];?></td>
                                                                        <td class=" "><?=$std_row[0]['DOB'];?></td>
                                                                        <td class=" "><?php $dob = $std_row[0]['DOB']; echo calcutateAge($dob);?></td>
                                                                        <?php
                                                                            $time=strtotime($std_row[0]['DOB']);
                                                                            $month=date("F",$time);
                                                                            ?>
                                                                        <td class=" "><?php echo $month;?></td>
                                                                        <td class=" "><?=$std_row[0]['GENDER'];?></td>
                                                                        <?php
                                                                            if($std_row[0]['COUNTRY_NO'])
                                                                            {
                                                                             $cond5="where `COUNTRY_NO`=".$std_row[0]['COUNTRY_NO'];
                                                                             $std_row5=getRows($cond5,'country');
                                                                            }
                                                                            ?>
                                                                        <td class=" "><?=$std_row5[0]['COUNTRY_NAME'];?></td>
                                                                        <?php if($row['AGENT_NO'])
                                                                            {
                                                                            $cond6="where `AGENT_NO`=".$row['AGENT_NO'];
                                                                                  $agn=getRows($cond6,'agent');
                                                                            }?>
                                                                        <td class=" "><?=$agn[0]['AGENT_NAME'];?></td>
                                                                        <td class=" "><?=$row['CUR_ATT'];?></td>
                                                                        <td class=" "><?=$row['OVR_ATT'];?></td>
                                                                        <?php
                                                                            $arr=array();
                                                                            $due_amount="where `ENROL_NO`=".$row['ENROL_NO'];
                                                                            $due_amount1=getRows($due_amount,'due_history');
                                                                            foreach($due_amount1 as $due_amount2)
                                                                             {
                                                                             if($due_amount2['DELETE_DATE']!='0')
                                                                              {
                                                                             $due_amount3="where `DUE_NO`=".$due_amount2['DUE_NO'];
                                                                                $due_amount4=getRows($due_amount3,'due');
                                                                             foreach($due_amount4 as $due_amount5)
                                                                                  {
                                                                            
                                                                             $arr[]=$due_amount5['AMOUNT'];
                                                                               
                                                                              $sum=array_sum($arr);
                                                                                   }
                                                                            	
                                                                              }
                                                                             }
                                                                            
                                                                           ?>
                                                                        <td class=" "><?php echo $sum;?></td>
                                                                        <td class=" "><?=$row['offer_create'];?></td>
                                                                        <td class=" "><input type="hidden" name="AWARDED" id="course_publish" value="0" >
                                                                            <input type="checkbox" name="AWARDED" value="1" <?php echo ($row['AWARDED'] ? 'checked' : '');?>/>
                                                                        </td>
                                                                        <td class=" "><input type="hidden" name="QUOTATION" id="course_publish" value="0" >
                                                                            <input type="checkbox" name="QUOTATION" value="1" <?php echo ($row['QUOTATION'] ? 'checked' : '');?>/>
                                                                        </td>
                                                                    </tr>
                                                                    <?php } } ?>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
    /*
        * @package    ACCIT
         * @author    Paperlink Softwares Team
         * @copyright    Copyright (c) 2016 , Paperlink Softwares Pvt. Ltd.  (http://www.paperlinksoftwares.com/)
         * @since    Version 1.0.0
         * @filesource
		 * Code for edit existing teacher
         */
    
    if($_POST['code']){
    	   if(mysql_query("UPDATE `teacher` SET `TEACHER_CODE`='".$_POST['name']."', `TEACHER_NAME`='".$_POST['name']."' , `LOCATION_NO`='".$_POST['location']."',`TEACHER_EMAIL`='".$_POST['email']."', `TEACHER_PHONE`='".$_POST['phone']."', `TEACHER_LEVEL`='".$_POST['level']."', `STATUS_NO`='".$_POST['status']."' WHERE `TEACHER_NO`=".$_GET['tid']))
    		$_SESSION['s_msg']="<strong>Well done!</strong> Item successfully updated";
    	else
    		$_SESSION['e_msg']="<strong>Oh snap!</strong> Item is not updated";
    
    }
    
    if($_GET['tid'])
    {
    	$cond="where `TEACHER_NO`=".$_GET['tid'];
    	$std_row=getRows($cond,'teacher');
    }?>
<div class="right_col" role="main">
    <div class="">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="page-title">
                <div class="title_left">
                    <?php 
                        if($_SESSION['s_msg'] || $_SESSION['e_msg'])
                        {
                        ?>
                    <div class="alert <?=($_SESSION['s_msg'])?'alert-success':'alert-error'?> fade in">
                        <button data-dismiss="alert" class="close" type="button">�</button>
                        <?=($_SESSION['s_msg'])?$_SESSION['s_msg']:$_SESSION['e_msg']?>
                    </div>
                    <?php
                        unset($_SESSION['s_msg']);
                        unset($_SESSION['e_msg']);
                        }
                        ?>
                    <h3>Teacher Details</h3>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="clearfix"></div>
                    <div class="x_content">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel recent-app">
                                    <div class="x_content">
                                        <div class="student-account-page">
                                            <!-- Nav tabs -->
                                         <div class="card">
                                            <div class="all-students-list add-course">
                                                <div class="add-student-section">
                                                    <form class="form-horizontal form-label-left" action="" method="post" >
                                                        <button type="submit" class="btn btn-success">Save</button>
                                                        <ul id="student-acc-tab" class="nav nav-tabs" role="tablist">
                                                            <li role="presentation"  class="active"><a href="#details" aria-controls="details" role="tab" data-toggle="tab">Details</a></li>
                                                        </ul>
                                                        <div class="tab-content">
                                                            <!--Details-->
                                                            <div role="tabpanel" class="tab-pane active" id="details">
                                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                                    <div class="row">
                                                                        <h4>
                                                                            <fieldset>Basic Info</fieldset>
                                                                        </h4>
                                                                        <div class="item form-group">
                                                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name" >Name</label>
                                                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                                                <div class="row">
                                                                                    <div class="col-md-7 col-sm-7 col-xs-12">
                                                                                        <input id="name" class="form-control col-md-12 col-xs-12" name="name" placeholder="Teacher Name" type="text" required value="<?php echo $std_row[0]['TEACHER_NAME']; ?>">
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="item form-group">
                                                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Email</label>
                                                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                                                <div class="row">
                                                                                    <div class="col-md-7 col-sm-7 col-xs-12">
                                                                                        <input id="email" class="form-control col-md-12 col-xs-12" name="email" placeholder="Teacher Name" type="email" required value="<?php echo $std_row[0]['TEACHER_EMAIL']; ?>">
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="item form-group">
                                                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Location</label>
                                                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                                                <div class="row">
                                                                                    <div class="col-md-6 col-sm-4 col-xs-12">
                                                                                        <select id="location" name="location" class="form-control" required>
                                                                                            <option value="">Select Location</option>
                                                                                            <option value="1" <?php if($std_row[0]['LOCATION_NO']=='1')  { ?> selected="selected" <?php } ?>>Sydney</option>
                                                                                            <option value="2" <?php if($std_row[0]['LOCATION_NO']=='2')  { ?> selected="selected" <?php } ?>>Kolkata</option>
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="item form-group">
                                                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Status</label>
                                                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                                                <div class="row">
                                                                                    <div class="col-md-6 col-sm-4 col-xs-12">
                                                                                        <select id="status" name="status" class="form-control" >
                                                                                            <option value="1">Active</option>
                                                                                            <option value="0">Inactive</option>
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="item form-group">
                                                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Phone</label>
                                                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                                                <div class="row">
                                                                                    <div class="col-md-7 col-sm-7 col-xs-12">
                                                                                        <input id="phone" class="form-control col-md-12 col-xs-12" name="phone" placeholder="Teacher_Phone" type="text" required value="<?php echo $std_row[0]['TEACHER_PHONE']; ?>">
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="item form-group">
                                                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Level</label>
                                                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                                                <div class="row">
                                                                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                                                                        <input type="number" step="1" min="1" max="" name="level" id="quantity" title="Qty" class="form-control input-text qty text" size="4" value="<?php echo $std_row[0]['TEACHER_LEVEL']; ?>">
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                               
                                                    </div>
                                                    <!--notes-->
                                                    <div role="tabpanel" class="tab-pane" id="notes">
                                                    </div>
                                                    </div>
                                                    </div>
                                                   
                                                    </div>
                                                         </form>
                                                </div>
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<?php
    /*
        * @package    ACCIT
         * @author    Paperlink Softwares Team
         * @copyright    Copyright (c) 2016 , Paperlink Softwares Pvt. Ltd.  (http://www.paperlinksoftwares.com/)
         * @since    Version 1.0.0
         * @filesource
		 * Code for edit holiday from holiday list
         */
    
    if($_POST['NAME'])
    	{
        $from_date=$_POST['FROM_DATE'];
    $to_date=$_POST['TO_DATE'];
    $new_format_from_date=str_replace('/', '-', $from_date);
    $new_format_from_date1=date("m/d/Y", strtotime($new_format_from_date));
    $new_format_to_date=str_replace('/', '-', $to_date);
    $new_format_to_date1=date("m/d/Y", strtotime($new_format_to_date));
    if($new_format_from_date1 < $new_format_to_date1) {
    	if(mysql_query("update `holiday` set  `LOCATION_NO`='0',`CRT_NO`='".$_POST['CRT_NAME']."',`OPTION_NO`='1',`NAME`='".$_POST['NAME']."',`FROM_DATE`='".$new_format_from_date1."',`TO_DATE`='".$new_format_to_date1."',`USER_NO`='".$_POST['user']."',`DELETE_DATE`='',`LOCK_NUM`='1' where `HOLIDAY_NO`=".$_GET['HOLIDAY_NO']))
    	   /* {
    	    	if(insert_record("insert into `course_fees` set `course_code`='".$_POST['course_code']."', `bill_option`='".$_POST['bill_option']."', `bill_cost`='".$_POST['bill_cost']."', `bill_all_cost`='".$_POST['bill_all_cost']."', `bill_tax`='".$_POST['bill_tax']."', `bill_tax_code`='".$_POST['bill_tax_code']."', `bill_max_length`='".$_POST['bill_max_length']."',`bill_max_cost`='".$_POST['bill_max_cost']."'"))
    		*/
    		$_SESSION['s_msg']="<strong>Fine!</strong> Holiday is Update Successfully";
    		//}
    		else//{
    			$_SESSION['e_msg']="<strong>Oh snap!</strong> Holiday is not Update";
    		//}
    	}
        else
        {
           $_SESSION['e_msg']="<strong>To date must be greater than From date!</strong>"; 
        }
        }
    	
    	
    if($_GET['HOLIDAY_NO'])
       {
       $co1="where `HOLIDAY_NO`=".$_GET['HOLIDAY_NO']; 
       $std_row=getRows($co1,'holiday');
       $d1 = $std_row[0]['FROM_DATE'];
       $d2 = $std_row[0]['TO_DATE'];
       
       $d1_arr = explode("/",$d1);
       $d2_arr = explode("/",$d2);
       
       $new_from_date = $d1_arr[2]."-".$d1_arr[0]."-".$d1_arr[1];
       $new_to_date = $d2_arr[2]."-".$d2_arr[0]."-".$d2_arr[1];
       }
       ?>
<div class="right_col" role="main">
<div class="">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="page-title">
            <div class="title_left">
                <?php 
                    if($_SESSION['s_msg'] || $_SESSION['e_msg'])
                    {
                    ?>
                <div class="alert <?=($_SESSION['s_msg'])?'alert-success':'alert-error'?> fade in">
                    <button data-dismiss="alert" class="close" type="button">�</button>
                    <?=($_SESSION['s_msg'])?$_SESSION['s_msg']:$_SESSION['e_msg']?>
                </div>
                <?php
                    unset($_SESSION['s_msg']);
                    unset($_SESSION['e_msg']);
                    }
                    ?>
                <h3>Holiday (Edit)</h3>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="clearfix"></div>
                <div class="x_content">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel recent-app">
                                <div class="x_content">
                                    <div class="student-account-page">
                                        <!-- Nav tabs -->
                                        <div class="card">
                                            <div class="all-students-list add-course">
                                                <div class="add-student-section">
                                                    <form class="form-horizontal form-label-left" action="" method="POST" novalidate>
                                                        <button type="submit" class="btn btn-success" onclick="needToConfirm = false;">Modify Holiday</button>
                                                        <ul id="student-acc-tab" class="nav nav-tabs" role="tablist">
                                                            <li role="presentation"  class="active"><a href="#details" aria-controls="details" role="tab" data-toggle="tab">Details</a></li>
                                                        </ul>
                                                        <div class="tab-content">
                                                            <!--Details-->
                                                            <div role="tabpanel" class="tab-pane active" id="details">
                                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                                    <div class="row">
                                                                        <h4>
                                                                            <fieldset>Basic Info</fieldset>
                                                                        </h4>
                                                                        <?php date_default_timezone_set('Australia/Sydney');
                                                                            $date = date('d/m/Y h:i:s a', time());
                                                                            ?>
                                                                        <input id="who"  name="who"  type="hidden" value="<?php echo $date?>">
                                                                        <input id="when"  name="user" type="hidden" value="<?php echo $_SESSION['user_no']?>">
                                                                        <div class="item form-group">
                                                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Faculty Name</label>
                                                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                                                <div class="row">
                                                                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                                                                        <select name="CRT_NAME" class="col-md-12 col-xs-12 form-control" required>
                                                                                            <option Value="">Select Faculty</option>
                                                                                            <?php
                                                                                                $dd_res=mysql_query("Select *  from crse_type");
                                                                                                while($r=mysql_fetch_array($dd_res))
                                                                                                  { 
                                                                                                if($std_row[0]['CRT_NO']==$r['CRT_NO'])
                                                                                                	  {
                                                                                                     echo "<option value='$r[CRT_NO]' selected> $r[CRT_NAME] </option>";
                                                                                                    }
                                                                                                	else
                                                                                                	  {
                                                                                                		echo "<option value='$r[CRT_NO]'> $r[CRT_NAME] </option>";
                                                                                                	  }
                                                                                                  }
                                                                                                
                                                                                                   ?>
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="item form-group">
                                                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Name</label>
                                                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                                                <div class="row">
                                                                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                                                                        <input value="<?=$std_row[0]['NAME']?>" class="form-control col-md-12 col-xs-12" name="NAME" placeholder="Holiday Name" type="text" required>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="item form-group">
                                                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">From Date</label>
                                                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                                                <div class="row">
                                                                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                                                                        <input value="<?php echo $new_from_date; ?>" class="form-control col-md-12 col-xs-12" name="FROM_DATE" placeholder="Holiday Name" type="date" required>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="item form-group">
                                                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">To Date</label>
                                                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                                                <div class="row">
                                                                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                                                                        <input value="<?php echo $new_to_date; ?>" class="form-control col-md-12 col-xs-12" name="TO_DATE" placeholder="Holiday Name" type="date" required>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                         <div class="item form-group">
                                                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Option</label>
                                                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                                         <div class="row">
                                                                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                                                                        <select name="OPTION_NO" id="OPTION_NO" class="col-md-12 col-xs-12 form-control" required>
                                                                                            <option Value="1" <?php if($std_row[0]['OPTION_NO']==1) { ?> selected <?php } ?>>Extend</option>
                                                                                            
                                                                                            <option Value="0" <?php if($std_row[0]['OPTION_NO']==0) { ?> selected <?php } ?>>Don't Extend</option>
                                                                                           
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                                </div>
                                                                                </div>
                                                  
                                                    </div>
                                                    <!--notes-->
                                                    <div role="tabpanel" class="tab-pane" id="notes">
                                                    </div>
                                                    </div>
                                                    </div>
                                                    
                                                    </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
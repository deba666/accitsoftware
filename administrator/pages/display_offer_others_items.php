<?php
    /*
        * @package    ACCIT
         * @author    Paperlink Softwares Team
         * @copyright    Copyright (c) 2016 , Paperlink Softwares Pvt. Ltd.  (http://www.paperlinksoftwares.com/)
         * @since    Version 1.0.0
         * @filesource
		 * code for select a particular offer (other item) 
		 * chose offer (other item) and back to the other offer page(offer details)
         */
    
    $AGENT_NO=$_GET["AGENT_NO"];
    $_SESSION['AGENT_NO']=$AGENT_NO;
    ?>
<div role="main" class="right_col" style="min-height: 648px;">
    <div class="">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="page-title">
                <div class="title_left">
                    <h3>All Other Offer</h3>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="clearfix"></div>
                    <div class="x_content">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel recent-app">
                                    <div class="x_content">
                                        <div class="student-account-page">
                                            <!-- Nav tabs -->
                                            <div class="card">
                                                <ul role="tablist" class="nav nav-tabs">
                                                    <li class="active" role="presentation"><a data-toggle="tab" role="tab" aria-controls="all-student" href="#all-student">Other Offers</a></li>
                                                </ul>
                                                <div class="tab-content">
                                                    <div id="all-student" class="tab-pane active" role="tabpanel">
                                                        <div class="all-students-list">
                                                            <div class="table-responsive">
                                                                <div id="datatable-buttons_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                                                                    <div class="dataTables_wrapper form-inline dt-bootstrap no-footer" id="datatable-buttons_wrapper">
                                                                        <div id="datatable-buttons_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                                                                            <table class="table table-striped jambo_table bulk_action dataTable no-footer dtr-inline table data-tbl-tools" id="datatable-buttons" role="grid" aria-describedby="datatable-buttons_info">
                                                                                <thead>
                                                                                    <tr class="headings">
                                                                                        <th>Code</th>
                                                                                        <th>Name</th>
                                                                                        <th>Amount</th>
                                                                                        <th>Category</th>
                                                                                        <th>Location</th>
                                                                                        <th>Faculty</th>
                                                                                        <th>Select</th>
                                                                                    </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                    <?php
                                                                                        $sql1="select * from fees where FEE_DEF_NO='100'";
                                                                                        $res1=mysql_query($sql1);
                                                                                        while($row1=mysql_fetch_array($res1))
                                                                                        {
                                                                                        ?>
                                                                                    <tr class="pointer odd" role="row">
                                                                                        <?php
                                                                                            $cond=" where `PRODUCT_NO`=".$row1['product_no'];
                                                                                            $std_row=getRows($cond,'product'); ?>
                                                                                        <td><?=$std_row[0]['CODE'];?></td>
                                                                                        <td><?=$row1['fee_name'];?></td>
                                                                                        <td><?=$row1['rate_1'];?></td>
                                                                                        <td ><?=$std_row[0]['CATEGORY_NO'];?></td>
                                                                                        <?php
                                                                                            $cond2=" where `LOCATION_NO`=".$std_row[0]['LOCATION_NO'];
                                                                                            $std_row2=getRows($cond2,'location'); ?>
                                                                                        <td ><?=$std_row2[0]['LOCATION_NAME'];?></td>
                                                                                        <?php
                                                                                            $cond1=" where `CRT_NO`=".$row1['crt_no'];
                                                                                            $std_row1=getRows($cond1,'crse_type'); ?>
                                                                                        <td ><?=$std_row1[0]['CRT_NAME'];?></td>
                                                                                        <td><a class="btn btn-success" href="#" onclick="javascript:void window.open('dashboard.php?op=<?=MD5('display_other_offer')?>&FROM_DATE=<?=$_GET['FROM_DATE']?>&TO_DATE=<?=$_GET['TO_DATE']?>&GROUP_NO=<?=$_GET['GROUP_NO']?>&of_no=<?=$_GET['of_no']?>&of_no1=<?=$_GET['of_no1']?>&AGENT_NO=<?=$_SESSION["AGENT_NO"]?>;&aname=<?=$_GET['aname']?>&stu_no=<?=$_GET['stu_no']?>&sname=<?=$_GET['sname']?>&cust_no=<?=$_GET['cust_no']?>&code=<?=$std_row[0]['CODE'];?>&name=<?=$row1['fee_name'];?>&location=<?=$std_row2[0]['LOCATION_NAME'];?>&faculty=<?=$std_row1[0]['CRT_NO'];?>&cost=<?=$row1['rate_1'];?>&PRODUCT_NO=<?=$std_row[0]['PRODUCT_NO'];?>&PRODUCT_TYPE_NO=<?=$std_row[0]['TYPE_NO'];?>&REFERENCE_NO=<?=$std_row[0]['REF_NO'];?>&LOCK_NUM=<?=$std_row[0]['LOCK_NUM'];?>','147013847888','width=800,height=600,toolbar=0,menubar=0,location=1,status=1,scrollbars=1,resizable=1,left=0,top=0');self.close();return false;">Add</a></td>
                                                                                        <?php 
                                                                                            }  ?>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
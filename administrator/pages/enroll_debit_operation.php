<?php
    /*
        * @package    ACCIT
         * @author    Paperlink Softwares Team
         * @copyright    Copyright (c) 2016 , Paperlink Softwares Pvt. Ltd.  (http://www.paperlinksoftwares.com/)
         * @since    Version 1.0.0
         * @filesource
		 * Code for insert the debit amount into debit table
         */
$amount=$_POST['show']-$_POST['amount'];
//echo "AMOUNT=".$amount;    
 if($_POST['amount'])
    	{
    	if(insert_record("insert into `debit` set `WITHDRAWAL_NO`='".$_POST['WITHDRAWAL_NO']."', `DEBIT_DATE`='".$_POST['DEBIT_DATE']."',`SCHEDULE_DATE`='".$_POST['SCHEDULE_DATE']."', `AMOUNT`='".$_POST['amount']."',`USER_NO`='".$_POST['user']."',`DEBIT_TYPE_NO`='".$_POST['DEBIT_TYPE_NO']."',`REF_NO`='".$_POST['REF_NO']."',`REF_TYPE`='".$_POST['REF_TYPE']."',`SCHEDULE_NO`='".$_POST['SCHEDULE_NO']."',`TRANS_GROUP_NO`='".$_POST['TRANS_GROUP_NO']."',`NOTES`='".$_POST['NOTES']."'"))
    
    		$_SESSION['msg']="<strong>Fine!</strong> amount debited";
    		else{
    			$_SESSION['msg']="<strong>Oh snap!</strong> Not Debited";
    		}
      		$id = mysql_insert_id();
    		if(insert_record("insert into `debit_item` set `DEBIT_NO`='$id',`AMOUNT`='".$_POST['amount']."',`ENROL_NO`='".$_GET['enrol_no']."',`INVOICE_NO`='".$_GET['invoice_no']."',`IND_NO`='".$_GET['ind_no']."',`FEE_NO`='".$_GET['FEE_NO']."',`GROSS_AMOUNT`='".$_POST['amount']."',`FEE_DEF_NO`='".$_GET['fee_def_no']."',`DEBIT_DATE`='".$_POST['DEBIT_DATE']."'"))
    		{
    		//mysql_query("update `due_history` set `DELETE_DATE`='".$_POST['DEBIT_DATE']."' where `IND_NO`='".$_GET['ind_no']."'");
    
    	   //delete_record("DELETE FROM `due` WHERE `IND_NO`='".$_GET['ind_no']."'");
		   mysql_query("update `credit` set `AMOUNT`='$amount' where `CREDIT_NO`='".$_GET['credit_no']."'");
    
    		$_SESSION['s_msg']="<strong>Fine!</strong> amount debited";
    		}
    			else{
    				$_SESSION['e_msg']="<strong>Oh snap!</strong> Not Debited";
    		}
    	}	
    		?> 		 	
<div role="main" class="right_col" style="min-height: 648px;">
    <div class="">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="page-title">
                <div class="title_left">
                    <?php 
                        if($_SESSION['s_msg'] || $_SESSION['e_msg'])
                        {
                        ?>
                    <div class="alert <?=($_SESSION['s_msg'])?'alert-success':'alert-error'?> fade in">
                        <button data-dismiss="alert" class="close" type="button">×</button>
                        <?=($_SESSION['s_msg'])?$_SESSION['s_msg']:$_SESSION['e_msg']?>
                    </div>
                    <?php
                        unset($_SESSION['s_msg']);
                        unset($_SESSION['e_msg']);
                        }
                        ?>
                    <h3>Finish Debit</h3>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="clearfix"></div>
                    <div class="x_content">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel recent-app">
                                    <div class="x_content">
                                        <div class="student-account-page edit-faculty-page">
                                            <!-- Nav tabs -->
                                            <div class="card">
                                                <div class="all-students-list add student">
                                                    <div class="add-student-section">
                                                        <form method="post" action="" class="form-horizontal form-label-left">
                                                            <div class="item form-group">
                                                                <label for="name" class="control-label col-md-3 col-sm-3 col-xs-3">Debit Under Amount:</label>
                                                                <div class="col-md-9 col-sm-9 col-xs-9">
                                                                    <div class="row">
                                                                        <input type="hidden" value="<?php echo $_SESSION['user_no']?>" name="user" >
                                                                        <input type="hidden" value="" name="WITHDRAWAL_NO" >
                                                                        <?php date_default_timezone_set('Australia/Sydney');
                                                                            $date = date("m/d/y");;?>
                                                                        <input type="hidden" value="<?php echo $date;?>" name="DEBIT_DATE" >
                                                                        <input type="hidden" value="<?php echo $date;?>" name="SCHEDULE_DATE" >
                                                                        <input type="hidden" value="" name="DEBIT_TYPE_NO" >
                                                                        <input type="hidden" value="" name="REF_NO" >
                                                                        <input type="hidden" value="" name="REF_TYPE" >
                                                                        <input type="hidden" value="" name="SCHEDULE_NO" >
                                                                        <input type="hidden" value="" name="TRANS_GROUP_NO" >
                                                                        <input type="hidden" value="" name="IND_NO" >
                                                                        <input type="hidden" value="" name="FEE_NO" >
                                                                        <input type="hidden" value="" name="FEE_DEF_NO" >
                                                                        <div class="col-md-7 col-sm-7 col-xs-9">
                                                                            <input type="hidden" value="<?php echo $_GET['amount']?>" name="show" class="form-control col-md-12 col-xs-12">
																			<input type="text" disabled="disabled"  value="<?php echo $_GET['amount']?>" class="form-control col-md-12 col-xs-12">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="item form-group">
                                                                <label for="name" class="control-label col-md-3 col-sm-3 col-xs-3">Enter Amount:</label>
                                                                <div class="col-md-9 col-sm-9 col-xs-9">
                                                                    <div class="row">
                                                                        <div class="col-md-7 col-sm-7 col-xs-9">
                                                                            <input type="text"  value="<?php echo $_GET['amount']?>" name="amount" class="form-control col-md-12 col-xs-12"  />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="item form-group">
                                                                <label for="name" class="control-label col-md-3 col-sm-3 col-xs-12"></label>
                                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                                    <div class="row">
                                                                        <div class="col-md-6 col-sm-4 col-xs-12">
                                                                            <p class="pull-left">
                                                                                <button class="btn btn-primary"  type="reset">Cancel</button>
                                                                                <button class="btn btn-primary" type="submit">Debit</button>
                                                                            </p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
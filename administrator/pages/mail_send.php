<?php
    /*
        * @package    ACCIT
         * @author    Paperlink Softwares Team
         * @copyright    Copyright (c) 2016 , Paperlink Softwares Pvt. Ltd.  (http://www.paperlinksoftwares.com/)
         * @since    Version 1.0.0
         * @filesource
		 * Code for mail template
         */
    
    $email=$_GET['email'];
    $sub= $_POST['TEMPLATE_NAME'];
    if($_POST['TEMPLATE_NAME'])
       {
    	   include('class/docxtemplate.class.php');
    
    		$docx = new DOCXTemplate('templates/Academic First Warning.tpl.docx');
    		$docx->set('STUD_FNAME', $_GET["s_fname"]);
    		$docx->set('DATE', date('d.m.Y'));
    		$docx->set('STUD_EXT', $_GET["s_no"]);
    		$docx->set('STUD_ADD', $_GET["addre"]);
    		$docx->set('STUD_ENROLL', $_GET["course"]);
    		$docx->set('ENROLL_START', $_GET["en_st"]);
    		
    		$docx->saveAs('templates/downloads/Academic First Warning.docx'); // or $docx->downloadAs('Academic First Warning.docx');
    		
    		//////////////////////End/////////////////////////////////////////
    		 
    
       		$name=$_POST['TEMPLATE_NAME'];
    		$contact='contact';
    		$message='Test Mail';
    		$response =	'
    		<center>
                <table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" style="border-collapse:collapse;margin:0;padding:0;background-color:#c3d6db;height:100%!important;width:100%!important">
                    <tbody><tr>
                        <td align="center" valign="top" style="margin:0;padding:20px;border-top:0;height:100%!important;width:100%!important">
                            
                            <table border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse:collapse;border:0">
                                <tbody><tr>
                                    <td align="center" valign="top">
                                        
                                        <table border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse:collapse;min-width:100%;background-color:#c3d6db;border-top:0;border-bottom:0">
                                            <tbody><tr>
                                            	<td valign="top" style="padding-top:9px"></td>
                                            </tr>
                                        </tbody></table>
                                        
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" valign="top">
                                        
                                        <table border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse:collapse;min-width:100%;background-color:#ffffff;border-top:5px solid #3795ac;border-bottom:0">
                                            <tbody><tr>
                                                <td valign="top"><table border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;border-collapse:collapse">
        <tbody>
                <tr>
                    <td valign="top" style="padding:0px">
                        <table align="left" width="100%" border="0" cellpadding="0" cellspacing="0" style="min-width:100%;border-collapse:collapse">
                            <tbody><tr>
                                <td valign="top" style="padding-right:0px;padding-left:0px;padding-top:0;padding-bottom:0;text-align:center">
    
    
                                           
    
                                </td>
                            </tr>
                        </tbody></table>
                    </td>
                </tr>
        </tbody>
    </table></td>
                                            </tr>
                                        </tbody></table>
                                        
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" valign="top">
                                        
                                        <table border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse:collapse;min-width:100%;background-color:#ffffff;border-top:0;border-bottom:0">
                                            <tbody><tr>
                                                <td valign="top"><table border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;border-collapse:collapse">
        <tbody>
                <tr>
                    <td valign="top" style="padding:9px">
                        <table align="left" width="100%" border="0" cellpadding="0" cellspacing="0" style="min-width:100%;border-collapse:collapse">
                            <tbody><tr>
                                <td valign="top" style="padding-right:9px;padding-left:9px;padding-top:0;padding-bottom:0;text-align:center">
    
                                        <img align="center" alt="" src="http://designingworld.com.au/test/administrator/templates/mail/header.png" width="128" style="max-width:100%;padding-bottom:0;display:inline!important;vertical-align:bottom;border:0;outline:none;text-decoration:none; width:600px;margin-top: -9px" class="CToWUd">
    
                                            
                                        </a>
    
                                </td>
                            </tr>
                        </tbody></table>
                    </td>
                </tr>
        </tbody>
    </table><table border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;border-collapse:collapse">
        <tbody>
            <tr>
                <td valign="top" style="padding-top:9px">
                  	
    			
    				
                    <table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width:100%;min-width:100%;border-collapse:collapse" width="100%">
                        <tbody><tr>
    
                            <td valign="top" style="padding:0px 18px 9px;color:#49a078;font-family:Arial,&quot;Helvetica Neue&quot;,Helvetica,sans-serif;font-size:24px;font-style:normal;font-weight:bold;line-height:125%;text-align:left">
     <img align="center" alt="" src="http://designingworld.com.au/test/administrator/templates/mail/header2.png" width="128" style="max-width:100%;margin-top: -18px;padding-bottom:0;display:inline!important;vertical-align:bottom;border:0;outline:none;text-decoration:none; width:600px" class="CToWUd">
    
                              
                            </td>
                        </tr>
                    </tbody></table>
    				
    
    				
                </td>
            </tr>
        </tbody>
    </table><table border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;border-collapse:collapse">
        <tbody>
            <tr>
                <td valign="top" style="padding-top:9px">
                  	
    			
    				
                    <table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width:100%;min-width:100%;border-collapse:collapse" width="100%">
                        <tbody><tr>
    
                            <td valign="top" style="padding:0px 18px 9px;color:#000000;font-family:Arial,&quot;Helvetica Neue&quot;,Helvetica,sans-serif;font-size:14px;font-style:normal;font-weight:normal;line-height:125%;text-align:left">
    
                               
                            </td>
                        </tr>
                    </tbody></table>
    				
    
    				
                </td>
            </tr>
        </tbody>
    </table><table border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;border-collapse:collapse">
        <tbody>
            <tr>
                <td style="padding-top:0;padding-right:18px;padding-bottom:18px;padding-left:18px" valign="top" align="center">
                    <table border="0" cellpadding="0" cellspacing="0" style="border-collapse:separate!important;border:2px solid #fc883f;border-radius:5px;background-color:#fc883f">
                        <tbody>
                            <tr>
                                <td align="center" valign="middle" style="font-family:Arial,&quot;Helvetica Neue&quot;,Helvetica,sans-serif;font-size:16px;padding:16px">
                                   '.$_POST['TEMPLATE_NAME'].'
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table><table border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;border-collapse:collapse">
        <tbody>
            <tr>
                <td valign="top" style="padding-top:9px">
                  	
    			
    				
                    <table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width:100%;min-width:100%;border-collapse:collapse" width="100%">
                        <tbody><tr>
    
                            <td valign="top" style="padding:0px 18px 9px;color:#000000;font-family:Arial,&quot;Helvetica Neue&quot;,Helvetica,sans-serif;font-size:14px;font-style:normal;font-weight:normal;line-height:125%;text-align:left">
    
                            </td>
                        </tr>
                    </tbody></table>
    				
    
    				
                </td>
            </tr>
        </tbody>
    </table></td>
                                            </tr>
                                        </tbody></table>
                                        
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" valign="top">
                                        
                                        <table border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse:collapse;min-width:100%;background-color:#3896ad;border-top:0;border-bottom:0">
                                            <tbody><tr>
                                                <td valign="top" style="padding-bottom:9px"><table border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;border-collapse:collapse">
        <tbody>
            <tr>
                <td align="center" valign="top" style="padding:9px">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;border-collapse:collapse">
        <tbody><tr>
            <td align="center" style="padding-left:9px;padding-right:9px">
                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;background-color:#3896ad;border:1px none #eeeeee;border-collapse:collapse">
                    <tbody><tr>
                        <td align="center" valign="top" style="padding-top:9px;padding-right:9px;padding-left:9px">
                            <table align="center" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse">
                                <tbody><tr>
                                    <td align="center" valign="top">
                                        
    
                                            
    
    
                                                <table align="left" border="0" cellpadding="0" cellspacing="0" style="display:inline;border-collapse:collapse">
                                                    <tbody><tr>
                                                        <td valign="top" style="padding-right:10px;padding-bottom:9px">
                                                            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse">
                                                                <tbody><tr>
                                                                    <td align="left" valign="middle" style="padding-top:5px;padding-right:10px;padding-bottom:5px;padding-left:9px">
                                                                        <table align="left" border="0" cellpadding="0" cellspacing="0" width="" style="border-collapse:collapse">
                                                                            <tbody><tr>
    
                                                                                    <td align="center" valign="middle" width="24">
                                                                                       
                                                                                    </td>
    
    
                                                                            </tr>
                                                                        </tbody></table>
                                                                    </td>
                                                                </tr>
                                                            </tbody></table>
                                                        </td>
                                                    </tr>
                                                </tbody></table>
    
                                            
    
                                            
    
    
                                                <table align="left" border="0" cellpadding="0" cellspacing="0" style="display:inline;border-collapse:collapse">
                                                    <tbody><tr>
                                                        <td valign="top" style="padding-right:10px;padding-bottom:9px">
                                                            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse">
                                                                <tbody><tr>
                                                                    <td align="left" valign="middle" style="padding-top:5px;padding-right:10px;padding-bottom:5px;padding-left:9px">
                                                                        <table align="left" border="0" cellpadding="0" cellspacing="0" width="" style="border-collapse:collapse">
                                                                            <tbody><tr>
    
                                                                                    <td align="center" valign="middle" width="24">
                                                                                       
                                                                                    </td>
    
    
                                                                            </tr>
                                                                        </tbody></table>
                                                                    </td>
                                                                </tr>
                                                            </tbody></table>
                                                        </td>
                                                    </tr>
                                                </tbody></table>
    
                                            
    
                                            
    
    
                                                <table align="left" border="0" cellpadding="0" cellspacing="0" style="display:inline;border-collapse:collapse">
                                                    <tbody><tr>
                                                        <td valign="top" style="padding-right:0;padding-bottom:9px">
                                                            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse">
                                                                <tbody><tr>
                                                                    <td align="left" valign="middle" style="padding-top:5px;padding-right:10px;padding-bottom:5px;padding-left:9px">
                                                                        <table align="left" border="0" cellpadding="0" cellspacing="0" width="" style="border-collapse:collapse">
                                                                            <tbody><tr>
    
                                                                                    <td align="center" valign="middle" width="24">
                                                                                       
                                                                                    </td>
    
    
                                                                            </tr>
                                                                        </tbody></table>
                                                                    </td>
                                                                </tr>
                                                            </tbody></table>
                                                        </td>
                                                    </tr>
                                                </tbody></table>
    
                                            
    
                                        
                                    </td>
                                </tr>
                            </tbody></table>
                        </td>
                    </tr>
                </tbody></table>
            </td>
        </tr>
    </tbody></table>
    
                </td>
            </tr>
        </tbody>
    </table><table border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;border-collapse:collapse">
        <tbody>
            <tr>
                <td valign="top" style="padding-top:9px">
                  	
    			
    				
                    <table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width:100%;min-width:100%;border-collapse:collapse" width="100%">
                        <tbody><tr>
    
                            <td valign="top" style="padding:0px 18px 9px;font-size:10px;color:#c3d6db;font-family:Tahoma,sans-serif;line-height:125%;text-align:left">
    
                                <div style="text-align:center">Copyright © 2016 ACCIT, All rights reserved.</div>
    
    <div style="text-align:center"><br>
    Our mailing address is:<br>
    <div><span>ACCIT</span><div><div>495 Kent St</div><div>Sydney</div><span>NSW</span><span>2000  </span>Austrilia <span></span></div><br><a href="#" target="_blank" data-saferedirecturl="#">Add us to your address book</a></div> <br>
    <br>
    Want to change how you receive these emails?<br>
    You can&nbsp;<a href="#" style="word-wrap:break-word;color:#c3d6db;font-weight:normal;text-decoration:underline" target="_blank" data-saferedirecturl="#">update your preferences</a>&nbsp;or&nbsp;<a href="#" style="word-wrap:break-word;color:#c3d6db;font-weight:normal;text-decoration:underline" target="_blank" data-saferedirecturl="#">unsubscribe from this list</a></div>
    
                            </td>
                        </tr>
                    </tbody></table>
    				
    
    				
                </td>
            </tr>
        </tbody>
    </table></td>
                                            </tr>
                                        </tbody></table>
                                        
                                    </td>
                                </tr>
                            </tbody></table>
                            
                        </td>
                    </tr>
                </tbody></table>
            </center>
    		';
    		foreach ($_POST['mail_to'] as $names)
    {
            $to.=",$names";
    }
    		$to = $email;
                    
    		$subject = $sub;
                    $uid = md5(uniqid(time()));
    		// Collected parameters
    		$file = 'Academic First Warning.docx';
    		$path = '/projects/P008/accit/administrator/templates/downloads/';
                    $file_size = filesize($file);
                    $handle = fopen($file, "rb");
                    $content = fread($handle, $file_size);
                    fclose($handle);
                    $content = chunk_split(base64_encode($content));
    		
    
    		//$txt = "Hello world!";
    		
            $headers  = 'MIME-Version: 1.0' . "\r\n";
    		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
    		$headers .= "From: info@kingswebsoft.com" . "\r\n" ;
    		echo $admin_mail = "tamal@paperlinksoftwares.com";
    		if(mail($to,$subject,$response,$headers))
    			{$_SESSION['s_msg'] ="You have sent Messages.";
    		//header("Location:".$_SERVER['PHP_SELF']);
    			}
    
       }
       ?> 
<div class="col-md-12 col-sm-12 col-xs-12">
    <div class="page-title">
        <div class="title_left">
            <?php 
                if($_SESSION['s_msg'] || $_SESSION['e_msg'])
                {
                ?>
            <div class="alert <?=($_SESSION['s_msg'])?'alert-success':'alert-error'?> fade in">
                <button data-dismiss="alert" class="close" type="button">×</button>
                <a href="/projects/P008/accit/administrator/templates/downloads/Academic First Warning.docx">Download</a>
                <?=($_SESSION['s_msg'])?$_SESSION['s_msg']:$_SESSION['e_msg']?>
            </div>
            <?php
                unset($_SESSION['s_msg']);
                unset($_SESSION['e_msg']);
                }
                
                ?>
            <h3>Send Message</h3>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="clearfix"></div>
            <div class="x_content">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel recent-app">
                            <div class="x_content">
                                <div class="student-account-page edit-faculty-page">
                                    <!-- Nav tabs -->
                                    <div class="card">
                                        <div class="all-students-list add student">
                                            <div class="add-student-section">
                                                <form method="post" target="_blank" action="../administrator/pages/offer_letter.php" class="form-horizontal form-label-left">
                                                    <div class="item form-group">
                                                        <?php date_default_timezone_set('Australia/sydney');
                                                            $date = date('d/m/Y h:i:s a', time());
                                                            ?>
                                                        <input id="who"  name="WHO_DATE"  type="hidden" value="<?php echo $date?>">
                                                        <input id="when"  name="user" type="hidden" value="<?php echo $_SESSION['user_no']?>">
                                                        <label for="name" class="control-label col-md-2 col-sm-2 col-xs-2">Student:</label>
                                                        <div class="col-md-8 col-sm-8 col-xs-8">
                                                            <div class="row">
                                                                <div class="col-md-7 col-sm-7 col-xs-12">
                                                                    <input required="" name="" value="<?=$_GET['s_no']?>, <?=$_GET['s_lname']?> <?=$_GET['s_mname']?> <?=$_GET['s_fname']?>" class="form-control col-md-12 col-xs-12" type="text" disabled>
																	<input name="STUD_NO" value="<?=$_GET['stud_no']?>" type="hidden">

                                                                    <input  value="<?=$_GET['ag_no']?>" class="form-control col-md-12 col-xs-12" id="name" type="hidden" name="AGENT_NO">
                                                                    <input  value="<?=$_GET['customer_no']?>" class="form-control col-md-12 col-xs-12" id="name" type="hidden" name="CUSTOMER_NO">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="item form-group">
                                                        <label for="name" class="control-label col-md-2 col-sm-2 col-xs-2">Select Type:</label>
                                                        <div class="col-md-8 col-sm-8 col-xs-8">
                                                            <div class="row">
                                                                <div class="col-md-7 col-sm-7 col-xs-12">
                                                                    <select name="TEMPLATE_NAME" class="form-control">
                                                                    <?php
                                                                        $templ=mysql_query("Select *  from templates");
                                                                                                                   while($template=mysql_fetch_array($templ))
                                                                                                                     {
                                                                                                                           echo "<option value='$template[TEMPLATE_NAME]'> $template[TEMPLATE_NAME] </option>";
                                                                        }
                                                                        ?>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
													<div class="item form-group">
                                                        <label for="name" class="control-label col-md-2 col-sm-2 col-xs-2">If Send Offer letter then select the offer:</label>
                                                        <div class="col-md-8 col-sm-8 col-xs-8">
                                                            <div class="row">
                                                                <div class="col-md-7 col-sm-7 col-xs-12">
                                                                   <select name="OFFER_NO" class="form-control">
                                                                    <?php
																	if($_GET['customer_no'])
																	{
                                                                        $of_cond="where `CUSTOMER_NO`=".$_GET['customer_no']." and `STATUS_NO`=".'3';
				                                                        $of_data= getRows($of_cond,'offer');
																		foreach($of_data as $off_it)
                                                                           {
                                                                             echo "<option value='$off_it[OFFER_NO]'> $off_it[OFFER_NO] </option>";
                                                                            }
																		}
                                                                        ?>
                                                                   </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="item form-group">
                                                        <label for="name" class="control-label col-md-2 col-sm-2 col-xs-2">Select More:</label>
                                                        <div class="col-md-8 col-sm-8 col-xs-8">
                                                            <div class="row">
                                                                <div class="col-md-7 col-sm-7 col-xs-12">
                                                                    <select name="mail_to[]" multiple>
                                                                        <?php $e_mail=getRows('order by `workflow_id`','workflow'); 
                                                                            foreach($e_mail as $mailmarge)
                                                                            {
                                                                            ?>
                                                                        <?php $m="where `STUD_NO`=".$mailmarge['student_id'];
                                                                            $mail_mg= getRows($m,'student'); ?> 
                                                                        <option value="<?=$mail_mg[0]['L_EMAIL']?>"><?=$mail_mg[0]['FNAME']?> <?=$mail_mg[0]['LNAME']?></option>
                                                                        <?php } ?>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="item form-group">
                                                        <label for="name" class="control-label col-md-3 col-sm-3 col-xs-12"></label>
                                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <div class="row">
                                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                                    <p class="pull-left">
                                                                        <button type="submit" id="Send" value="Send" name="Send" class="btn btn-success">Send Mail</button>
                                                                        <a href="#" onclick="javascript:void window.open('dashboard.php?op=<?=MD5('mail_template')?>&temp=<?php 'Template Email';?>&stud_name=<?=$mail_mg[0]['FNAME']?>','1880138621367','width=1200,height=800,toolbar=0,menubar=0,location=1,status=1,scrollbars=1,resizable=1,left=0,top=0');return false;">Mail Template</a>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
    /*
        * @package    ACCIT
         * @author    Paperlink Softwares Team
         * @copyright    Copyright (c) 2016 , Paperlink Softwares Pvt. Ltd.  (http://www.paperlinksoftwares.com/)
         * @since    Version 1.0.0
         * @filesource
		 * Code for create student absent. 
		 * In Enrolment details this comes in the absence field
         */
    
    $a=$_GET['enrol_no'];
	//echo "ENROL NO=".$a;
       if($_POST['ABSENCE_DATE'])
       {
       	if(insert_record("insert into `absence` set `ENROL_NO`='$a',`SUBJECT_NO`='',`ABSENCE_DATE`='".$_POST['ABSENCE_DATE']."',`HOURS`='".$_POST['HOURS']."',`REASON`='".$_POST['REASON']."',`COUNTED`='".$_POST['COUNTED']."',`USER_NO`='".$_POST['user']."',`WHO_DATE`='".$_POST['WHO_DATE']."',`RESULT_NO`='',`LANG_CLASS_CODE`='1'")){
       			$_SESSION['s_msg']="<strong>Fine!</strong>Student Absence Added Successfully";
				
                                ?>
<script>
          
   
       // window.opener.location.reload();
       window.opener.location.reload();
        self.close();
        
    
     </script>
        <?php }
       		else {
       			$_SESSION['e_msg']="<strong>Oh snap!</strong>Absence is not added";
				}
       
       }
       ?>
<script language="javascript" type="text/javascript" src="datetimepicker.js"></script>
<div class="col-md-12 col-sm-12 col-xs-12">
    <div class="page-title">
        <div class="title_left">
            <?php 
                if($_SESSION['s_msg'] || $_SESSION['e_msg'])
                {
                ?>
            <div class="alert <?=($_SESSION['s_msg'])?'alert-success':'alert-error'?> fade in">
                <button data-dismiss="alert" class="close" type="button">�</button>
                <?=($_SESSION['s_msg'])?$_SESSION['s_msg']:$_SESSION['e_msg']?>
            </div>
            <?php
                unset($_SESSION['s_msg']);
                unset($_SESSION['e_msg']);
                }
                ?>
            <h3>Absence</h3>
        </div>
        s
    </div>
</div>
<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="clearfix"></div>
            <div class="x_content">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel recent-app">
                            <div class="x_content">
                                <div class="student-account-page edit-faculty-page">
                                    <!-- Nav tabs -->
                                    <div class="card">
                                        <div class="all-students-list add student">
                                            <div class="add-student-section">
                                                <form method="post" action="" class="form-horizontal form-label-left">
                                                    <div class="item form-group">
                                                        <?php date_default_timezone_set('Australia/Melbourne');
                                                            $date = date('d/m/Y h:i:s a', time());
                                                            ?>
                                                        <input id="who"  name="WHO_DATE"  type="hidden" value="<?php echo $date?>">
                                                        <input id="when"  name="user" type="hidden" value="<?php echo $_SESSION['user_no']?>">
                                                        <div class="col-md-8 col-sm-8 col-xs-8">
                                                            <div class="row">
                                                                <div class="col-md-7 col-sm-7 col-xs-12">
                                                                    <input required="" name="" value="<?=$_GET['s_no']?>, <?=$_GET['s_lname']?> <?=$_GET['s_mname']?> <?=$_GET['s_fname']?>" class="form-control col-md-12 col-xs-12" type="hidden" disabled >
                                                                    <input  value="<?=$_GET['ag_no']?>" class="form-control col-md-12 col-xs-12" id="hidden" type="hidden" name="AGENT_NO">
                                                                    <input  value="<?=$_GET['cus_no']?>" class="form-control col-md-12 col-xs-12" id="name" type="hidden" name="CUSTOMER_NO">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        
                                                        <div class="col-md-3 col-sm-3 col-xs-12">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-12 col-sm-12 col-xs-12" for="name">Counted
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-1 col-sm-1 col-xs-12">
                                                            <div class="form-group">
                                                                <input name="COUNTED" type="hidden" value="0">
                                                                <input  value="1" name="COUNTED" type="checkbox" style="zoom:1.5" checked>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-1 col-sm-1 col-xs-12">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-12 col-sm-12 col-xs-12" for="name">Dates
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 col-sm-3 col-xs-12">
                                                            <div class="form-group">
                                                                <input class="form-control col-md-12 col-xs-12" name="ABSENCE_DATE" value="<?php echo date('Y-m-d');?>" type="date">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-1 col-sm-1 col-xs-12">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-12 col-sm-12 col-xs-12" for="name">Hours
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 col-sm-3 col-xs-12">
                                                            <div class="form-group">
                                                                <input class="form-control col-md-12 col-xs-12" name="HOURS" type="number">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="item form-group">
                                                        <label for="name" class="control-label col-md-2 col-sm-2 col-xs-2">Reason</label>
                                                        <div class="col-md-8 col-sm-8 col-xs-8">
                                                            <div class="row">
                                                                <div class="col-md-7 col-sm-7 col-xs-12">
                                                                    <textarea name="REASON" class="form-control col-md-12 col-xs-12" type="text"></textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="item form-group">
                                                            <label for="name" class="control-label col-md-3 col-sm-3 col-xs-12"></label>
                                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                                <div class="col-md-6 col-sm-4 col-xs-12">
                                                                    <p class="pull-left">
                                                                        <button class="btn btn-primary" type="submit">Cancel</button>
                                                                        <button class="btn btn-success" type="submit">Submit</button>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
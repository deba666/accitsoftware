<?php
    /*
        * @package    ACCIT
         * @author    Paperlink Softwares Team
         * @copyright    Copyright (c) 2016 , Paperlink Softwares Pvt. Ltd.  (http://www.paperlinksoftwares.com/)
         * @since    Version 1.0.0
         * @filesource
		 * Code for add new teacher
         */
    
    if($_POST['name'])
    	{
    	if(insert_record("insert into `teacher` set  `TEACHER_CODE`='".$_POST['name']."', `TEACHER_NAME`='".$_POST['name']."' , `TEACHER_EMAIL`='".$_POST['email']."',`LOCATION_NO`='".$_POST['location']."', `STATUS_NO`='".$_POST['status']."',`TEACHER_PHONE`='".$_POST['phone']."',`TEACHER_LEVEL`='".$_POST['level']."'"))
    	   /* {
    	    	if(insert_record("insert into `course_fees` set `course_code`='".$_POST['course_code']."', `bill_option`='".$_POST['bill_option']."', `bill_cost`='".$_POST['bill_cost']."', `bill_all_cost`='".$_POST['bill_all_cost']."', `bill_tax`='".$_POST['bill_tax']."', `bill_tax_code`='".$_POST['bill_tax_code']."', `bill_max_length`='".$_POST['bill_max_length']."',`bill_max_cost`='".$_POST['bill_max_cost']."'"))
    		*/
    		$_SESSION['s_msg']="<strong>Fine!</strong> New Teacher Added";
    		//}
    		else//{
    			$_SESSION['e_msg']="<strong>Oh snap!</strong> Teacher is not added";
    		//}
    	}
    	
    	
    if($_GET['del'])
    {
    	if(delete_record("DELETE FROM `student` WHERE `std_id` = ".$_GET['del']))
    		$_SESSION['s_msg']="Item successfully deleted";
    }
    
    ?>
<div class="right_col" role="main">
<div class="">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="page-title">
            <div class="title_left">
                <?php 
                    if($_SESSION['s_msg'] || $_SESSION['e_msg'])
                    {
                    ?>
                <div class="alert <?=($_SESSION['s_msg'])?'alert-success':'alert-error'?> fade in">
                    <button data-dismiss="alert" class="close" type="button">�</button>
                    <?=($_SESSION['s_msg'])?$_SESSION['s_msg']:$_SESSION['e_msg']?>
                </div>
                <?php
                    unset($_SESSION['s_msg']);
                    unset($_SESSION['e_msg']);
                    }
                    ?>
                <h3>Add Teacher</h3>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="clearfix"></div>
                <div class="x_content">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel recent-app">
                                <div class="x_content">
                                    <div class="student-account-page">
                                        <!-- Nav tabs -->
                                        <div class="card">
                                            <div class="all-students-list add-course">
                                                <div class="add-student-section">
                                                    <form class="form-horizontal form-label-left" action="" method="post" >
                                                        <button type="submit" class="btn btn-success">Add Teacher</button>
                                                        <ul id="student-acc-tab" class="nav nav-tabs" role="tablist">
                                                            <li role="presentation"  class="active"><a href="#details" aria-controls="details" role="tab" data-toggle="tab">Details</a></li>
                                                        </ul>
                                                        <div class="tab-content">
                                                            <!--Details-->
                                                            <div role="tabpanel" class="tab-pane active" id="details">
                                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                                    <div class="row">
                                                                        <h4>
                                                                            <fieldset>Basic Info</fieldset>
                                                                        </h4>
                                                                        <div class="item form-group">
                                                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name" >Name</label>
                                                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                                                <div class="row">
                                                                                    <div class="col-md-7 col-sm-7 col-xs-12">
                                                                                        <input id="name" class="form-control col-md-12 col-xs-12" name="name" placeholder="Teacher Name" type="text" required>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="item form-group">
                                                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Email</label>
                                                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                                                <div class="row">
                                                                                    <div class="col-md-7 col-sm-7 col-xs-12">
                                                                                        <input id="email" class="form-control col-md-12 col-xs-12" name="email" placeholder="Teacher Name" type="email" required>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="item form-group">
                                                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Location</label>
                                                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                                                <div class="row">
                                                                                    <div class="col-md-6 col-sm-4 col-xs-12">
                                                                                        <select id="location" name="location" class="form-control" required>
                                                                                            <option value="">Select Location</option>
                                                                                            <option value="1">Sydney</option>
                                                                                            <option value="2">Kolkata</option>
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="item form-group">
                                                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Status</label>
                                                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                                                <div class="row">
                                                                                    <div class="col-md-6 col-sm-4 col-xs-12">
                                                                                        <select id="status" name="status" class="form-control" >
                                                                                            <option value="1">Active</option>
                                                                                            <option value="0">Inactive</option>
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="item form-group">
                                                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Phone</label>
                                                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                                                <div class="row">
                                                                                    <div class="col-md-7 col-sm-7 col-xs-12">
                                                                                        <input id="phone" class="form-control col-md-12 col-xs-12" name="phone" placeholder="Teacher_Phone" type="text" required>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="item form-group">
                                                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Level</label>
                                                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                                                <div class="row">
                                                                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                                                                        <input type="number" step="1" min="1" max="" name="level" id="quantity" value="1" title="Qty" class="form-control input-text qty text" size="4">
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                               
                                                    </div>
                                                    <!--notes-->
                                                    <div role="tabpanel" class="tab-pane" id="notes">
                                                    </div>
                                                    </div>
                                                    </div>
                                                   
                                                    </div>
                                                         </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
    /*
        * @package    ACCIT
         * @author    Paperlink Softwares Team
         * @copyright    Copyright (c) 2016 , Paperlink Softwares Pvt. Ltd.  (http://www.paperlinksoftwares.com/)
         * @since    Version 1.0.0
         * @filesource
         */
    
    $ag_show="where `AGENT_NO`=".$_GET['a_no']; $agent=getRows($ag_show,'agent_emps'); ?>
<div class="col-md-12 col-sm-12 col-xs-12">
    <div class="page-title">
        <div class="title_left">
            <h3>Agent Details</h3>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="clearfix"></div>
            <div class="x_content">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel recent-app">
                            <div class="x_content">
                                <div class="student-account-page edit-faculty-page">
                                    <!-- Nav tabs -->
                                    <div class="card">
                                        <div class="all-students-list add student">
                                            <div class="add-student-section">
                                                <form method="post" action="" class="form-horizontal form-label-left">
                                                    <div class="item form-group">
                                                        <label for="name" class="control-label col-md-2 col-sm-2 col-xs-2">Agent Name:</label>
                                                        <div class="col-md-8 col-sm-8 col-xs-8">
                                                            <div class="row">
                                                                <div class="col-md-7 col-sm-7 col-xs-12">
                                                                    <input required="" value="<?=$agent[0]['FIRST_NAME']?> <?=$agent[0]['LAST_NAME']?>"  class="form-control col-md-12 col-xs-12" id="name" type="text" disabled>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="item form-group">
                                                        <label for="name" class="control-label col-md-2 col-sm-2 col-xs-2">Agent Postition:</label>
                                                        <div class="col-md-8 col-sm-8 col-xs-8">
                                                            <div class="row">
                                                                <div class="col-md-7 col-sm-7 col-xs-12">
                                                                    <input required="" value="<?=$agent[0]['AG_POSITION']?>"  class="form-control col-md-12 col-xs-12" id="name" type="text" disabled>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="item form-group">
                                                        <label for="name" class="control-label col-md-2 col-sm-2 col-xs-2">Agent Phone:</label>
                                                        <div class="col-md-8 col-sm-8 col-xs-8">
                                                            <div class="row">
                                                                <div class="col-md-7 col-sm-7 col-xs-12">
                                                                    <input required="" value="<?=$agent[0]['PHONE']?>"  class="form-control col-md-12 col-xs-12" id="name" type="text" disabled>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="item form-group">
                                                        <label for="name" class="control-label col-md-2 col-sm-2 col-xs-2">Agent Email:</label>
                                                        <div class="col-md-8 col-sm-8 col-xs-8">
                                                            <div class="row">
                                                                <div class="col-md-7 col-sm-7 col-xs-12">
                                                                    <input required="" value="<?=$agent[0]['EMAIL']?>"  class="form-control col-md-12 col-xs-12" id="name" type="text" disabled>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
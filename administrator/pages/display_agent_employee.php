<?php
    /*
        * @package    ACCIT
         * @author    Paperlink Softwares Team
         * @copyright    Copyright (c) 2016 , Paperlink Softwares Pvt. Ltd.  (http://www.paperlinksoftwares.com/)
         * @since    Version 1.0.0
         * @filesource
		 * Code for add new agent employee link comes from agent edit page(Add New Employee)
         */
    
       if($_POST['TITLE'])
       {
       	if(insert_record("insert into `agent_emps` set `TITLE`='".$_POST['TITLE']."',`AGENT_NO`='".$_GET['ag_no']."',`FIRST_NAME`='".$_POST['FIRST_NAME']."',`LAST_NAME`='".$_POST['LAST_NAME']."',`AG_POSITION`='".$_POST['AG_POSITION']."',`PHONE`='".$_POST['PHONE']."',`STATUS`='".$_POST['STATUS']."',`PRIMARY_FLAG`='".$_POST['PRIMARY_FLAG']."',`EMAIL`='".$_POST['EMAIL']."',`FAX`='".$_POST['FAX']."',`PHOTO`=''"))
        {
            $_SESSION['s_msg']="<strong>Fine!</strong>New Agent Employee Added Successfully";
            ?>
<script>
opener.location.reload(true);
     self.close();
     
     </script>
<?php
        }	else{
       			$_SESSION['e_msg']="<strong>Oh snap!</strong>New Agent Employee is not added";
       }
       
                }
       ?>
<div class="col-md-12 col-sm-12 col-xs-12">
    <div class="page-title">
        <div class="title_left">
            <?php 
                if($_SESSION['s_msg'] || $_SESSION['e_msg'])
                {
                ?>
            <div class="alert <?=($_SESSION['s_msg'])?'alert-success':'alert-error'?> fade in">
                <button data-dismiss="alert" class="close" type="button">�</button>
                <?=($_SESSION['s_msg'])?$_SESSION['s_msg']:$_SESSION['e_msg']?>
            </div>
            <?php
                unset($_SESSION['s_msg']);
                unset($_SESSION['e_msg']);
                }
                ?>
            <h3>Add Employee</h3>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="clearfix"></div>
            <div class="x_content">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel recent-app">
                            <div class="x_content">
                                <div class="student-account-page edit-faculty-page">
                                    <!-- Nav tabs -->
                                    <div class="card">
                                        <div class="all-students-list add student">
                                            <div class="add-student-section">
                                                <form method="post" action="" class="form-horizontal form-label-left">
                                                    <div class="item form-group">
                                                        <label for="name" class="control-label col-md-1 col-sm-1 col-xs-1">Title:</label>
                                                        <div class="col-md-4 col-sm-4 col-xs-4">
                                                            <div class="row">
                                                                <div class="col-md-7 col-sm-7 col-xs-12">
                                                                    <select name="TITLE" class="form-control">
                                                                        <option value="MR">MR.</option>
                                                                        <option value="MRS">MRS.</option>
                                                                        <option value="Miss">Miss.</option>
                                                                        <option value="DR">DR.</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div for="name" class="control-label col-md-2 col-sm-2 col-xs-2">Active:<input name="STATUS" value="0" type="hidden">
                                                            <input value="1"  name="STATUS" class="flat" type="checkbox">
                                                        </div>
                                                        <div for="name" class="control-label col-md-3 col-sm-3col-xs-3">Primary Contact:<input name="PRIMARY_FLAG" value="0" type="hidden">
                                                            <input value="1"  name="PRIMARY_FLAG" class="flat" type="checkbox">
                                                        </div>
                                                    </div>
                                                    <div class="item form-group">
                                                        <label for="name" class="control-label col-md-2 col-sm-2 col-xs-2">First Name:</label>
                                                        <div class="col-md-8 col-sm-8 col-xs-8">
                                                            <div class="row">
                                                                <div class="col-md-7 col-sm-7 col-xs-12">
                                                                    <input required="" placeholder="First Name" name="FIRST_NAME" class="form-control col-md-12 col-xs-12" id="name" type="text" required>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="item form-group">
                                                        <label for="name" class="control-label col-md-2 col-sm-2 col-xs-2">Last Name:</label>
                                                        <div class="col-md-8 col-sm-8 col-xs-8">
                                                            <div class="row">
                                                                <div class="col-md-7 col-sm-7 col-xs-12">
                                                                    <input required="" placeholder="Last Name" name="LAST_NAME" class="form-control col-md-12 col-xs-12" id="name" type="text" required>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="item form-group">
                                                        <label for="name" class="control-label col-md-2 col-sm-2 col-xs-2">Position:</label>
                                                        <div class="col-md-8 col-sm-8 col-xs-8">
                                                            <div class="row">
                                                                <div class="col-md-7 col-sm-7 col-xs-12">
                                                                    <input required="" placeholder="Position" name="AG_POSITION" class="form-control col-md-12 col-xs-12" id="name" type="text" required>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="item form-group">
                                                        <label for="name" class="control-label col-md-2 col-sm-2 col-xs-2">Phone:</label>
                                                        <div class="col-md-8 col-sm-8 col-xs-8">
                                                            <div class="row">
                                                                <div class="col-md-7 col-sm-7 col-xs-12">
                                                                    <input required="" placeholder="Phone" name="PHONE" class="form-control col-md-12 col-xs-12" id="name" type="text" required>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="item form-group">
                                                        <label for="name" class="control-label col-md-2 col-sm-2 col-xs-2">Fax:</label>
                                                        <div class="col-md-8 col-sm-8 col-xs-8">
                                                            <div class="row">
                                                                <div class="col-md-7 col-sm-7 col-xs-12">
                                                                    <input required="" placeholder="Fax" name="FAX" class="form-control col-md-12 col-xs-12" id="name" type="text" required>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="item form-group">
                                                        <label for="name" class="control-label col-md-2 col-sm-2 col-xs-2">Email:</label>
                                                        <div class="col-md-8 col-sm-8 col-xs-8">
                                                            <div class="row">
                                                                <div class="col-md-7 col-sm-7 col-xs-12">
                                                                    <input required="" placeholder="Email" name="EMAIL" class="form-control col-md-12 col-xs-12" id="name" type="email" required>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="item form-group">
                                                        <label for="name" class="control-label col-md-3 col-sm-3 col-xs-12"></label>
                                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                                            <div class="row">
                                                                <div class="col-md-6 col-sm-4 col-xs-12">
                                                                    <p class="pull-left">
                                                                        <input class="btn btn-primary" type="button" onclick="javascript: window.close();" value="Cancel">
                                                                        <button class="btn btn-success" type="submit">Submit</button>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
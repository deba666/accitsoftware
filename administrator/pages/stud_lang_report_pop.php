<?php
    /*
        * @package    ACCIT
         * @author    Paperlink Softwares Team
         * @copyright    Copyright (c) 2016 , Paperlink Softwares Pvt. Ltd.  (http://www.paperlinksoftwares.com/)
         * @since    Version 1.0.0
         * @filesource
         */
    ?>
<div role="main" class="right_col" style="min-height: 648px;">
    <div class="">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="page-title">
                <div class="title_left">
                    <h3>Language Enrollment Report </h3>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="clearfix"></div>
                    <div class="x_content">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel recent-app">
                                    <div class="x_content">
                                        <div class="student-account-page view-faculty-page enroll-popup">
                                            <form action="dashboard.php?op=<?=MD5('stud_lang_enroll_report')?>" method="POST" name="">
                                                <div class="row">
                                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                                        All Report: <input type="checkbox" value="all" name="filter" checked/>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-12 col-sm-12 col-xs-12" for="name">Enrollment Filter
                                                            </label>
                                                            <select name="status" class="form-control">
                                                                <?php $today = date("m/d/y");?>
                                                                <option value="<?=$today?>">Current</option>
                                                                <option value="<?=$today?>">Starting</option>
                                                                <option value="<?=$today?>">Finishing</option>
                                                            </select>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-12 col-sm-12 col-xs-12" for="name">From
                                                            </label>
                                                            <input type="textbox" name="from" value="11/11/1970" class="form-control">
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-12 col-sm-12 col-xs-12" for="name">Arrived
                                                            </label>
                                                            <select id="chkveg" multiple="multiple">
                                                                <option value="cheese">Cheese</option>
                                                                <option value="tomatoes">Tomatoes</option>
                                                                <option value="mozarella">Mozzarella</option>
                                                                <option value="mushrooms">Mushrooms</option>
                                                                <option value="pepperoni">Pepperoni</option>
                                                                <option value="onions">Onions</option>
                                                            </select>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-12 col-sm-12 col-xs-12" for="name">Location
                                                            </label>
                                                            <select name="COUNTRY_NAME" class="form-control">
                                                                <?php $loc=getRows('ORDER BY `LOCATION_NO` DESC','location');
                                                                    foreach($loc as $r_loc)
                                                                                {
                                                                    ?>
                                                                <option value="<?=$r_loc['LOCATION_NO']?>"><?=$r_loc['LOCATION_NAME']?></option>
                                                                <?php }?>
                                                            </select>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-12 col-sm-12 col-xs-12" for="name">Course
                                                            </label>
                                                            <select name="course" class="form-control">
                                                                <?php $course_row=getRows('ORDER BY `COURSE_NO` DESC','course');
                                                                    foreach($course_row as $row)
                                                                                {
                                                                    ?>
                                                                <option value="<?=$row['COURSE_NO']?>"><?=$row['COURSE_NAME']?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="checkbox pull-left">
                                                                <label class="pull-left">
                                                                <input type="checkbox"> Attendance bellow
                                                                </label>
                                                                <div class="quantity">
                                                                    <div class="quantity-select">
                                                                        <div class="entry value-minus">&nbsp;</div>
                                                                        <div class="entry value"><span>2</span></div>
                                                                        <div class="entry value-plus active">&nbsp;</div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-12 col-sm-12 col-xs-12" for="name">Grouping
                                                            </label>
                                                            <select name="FACULTY" id="course_faculty" class="form-control">
                                                                <option value="17">VET Courses</option>
                                                                <option value="16">Arka</option>
                                                                <option value="12">Business Programs</option>
                                                                <option value="8">Cert II Statement</option>
                                                                <option value="7">Cert II Graphics</option>
                                                                <option value="6">IT Cert 11</option>
                                                                <option value="5">IT Programs</option>
                                                                <option value="4">Cert II I Technology</option>
                                                                <option value="3">Groups</option>
                                                                <option value="2">English Language</option>
                                                            </select>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-12 col-sm-12 col-xs-12" for="name">To
                                                            </label>
                                                            <input type="textbox" name="to"  value="<?=$today?>" class="form-control">
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-12 col-sm-12 col-xs-12" for="name">Sorting
                                                            </label>
                                                            <select class="form-control">
                                                                <option value="Faculty">Student Name</option>
                                                                <option value="Country">Last Name</option>
                                                            </select>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-12 col-sm-12 col-xs-12" for="name">Faculty
                                                            </label>
                                                            <select class="form-control">
                                                                <option value="All">All</option>
                                                                <option value="All">All</option>
                                                            </select>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-12 col-sm-12 col-xs-12" for="name">Agent
                                                            </label>
                                                            <select name="agent" class="form-control">
                                                                <?php $ag_row=getRows('ORDER BY `AGENT_NO` ASC','agent');
                                                                    foreach($ag_row as $ag)
                                                                                {
                                                                    ?>
                                                                <option value="<?=$ag['AGENT_NO']?>"><?=$ag['AGENT_NAME']?></option>
                                                                <?php }?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <input type="submit" class="" name="sub" />
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
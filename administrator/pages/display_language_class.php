<?php
    /*
        * @package    ACCIT
         * @author    Paperlink Softwares Team
         * @copyright    Copyright (c) 2016 , Paperlink Softwares Pvt. Ltd.  (http://www.paperlinksoftwares.com/)
         * @since    Version 1.0.0
         * @filesource
		 * Code for add new language class . link comes from Language Classes(Side Bar->Classes->Language Classes).(Add New)
         */
       if($_POST['LANG_CLASS_NAME'])
       {
       	if(insert_record("insert into `lang_class` set `LANG_CLASS_NAME`='".$_POST['LANG_CLASS_NAME']."',`LANG_CLASS_CODE`='',`SESSION_NO`='".$_POST['SESSION_NO']."',`LEVEL_NO`='".$_POST['LEVEL_NO']."',`LOCATION_NO`='1',`LANG_CLASS_HRS`='',`TEACHER_NO`='".$_POST['TEACHER_NO']."',`ROOM_NO`='".$_POST['ROOM_NO']."',`STATUS_NO`='1',`NOTES`='',`CUSTOM_1`='".$_POST['CUSTOM_1']."',`CUSTOM_2`='".$_POST['CUSTOM_2']."',`CUSTOM_3`='".$_POST['CUSTOM_3']."',`CUSTOM_4`='".$_POST['CUSTOM_4']."',`CUSTOM_5`='".$_POST['CUSTOM_5']."',`CUSTOM_6`='".$_POST['CUSTOM_6']."',`CUSTOM_7`='".$_POST['CUSTOM_7']."',`DELETED`='',`CREATED`='',`USER_NO`='".$_POST['user']."'"))
       			$_SESSION['s_msg']="<strong>Fine!</strong>New Language Classes Added Successfully";
       		else{
       			$_SESSION['e_msg']="<strong>Oh snap!</strong>Language Classes is not added";
       }}
       ?>
      <div class="right_col" role="main">
<div class="col-md-12 col-sm-12 col-xs-12">
    <div class="page-title">
        <div class="title_left">
            <?php 
                if($_SESSION['s_msg'] || $_SESSION['e_msg'])
                {
                ?>
            <div class="alert <?=($_SESSION['s_msg'])?'alert-success':'alert-error'?> fade in">
                <button data-dismiss="alert" class="close" type="button">�</button>
                <?=($_SESSION['s_msg'])?$_SESSION['s_msg']:$_SESSION['e_msg']?>
            </div>
            <?php
                unset($_SESSION['s_msg']);
                unset($_SESSION['e_msg']);
                }
                ?>
            <h3>Add New Class</h3>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="clearfix"></div>
            <div class="x_content">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel recent-app">
                            <div class="x_content">
                                <div class="student-account-page edit-faculty-page">
                                    <!-- Nav tabs -->
                                    <div class="card">
                                        <div class="all-students-list add student">
                                            <div class="add-student-section">
                                                <form method="post" action="" class="form-horizontal form-label-left">
                                                    <div class="item form-group">
                                                        <label for="name" class="control-label col-md-2 col-sm-2 col-xs-2">Name</label>
                                                        <div class="col-md-8 col-sm-8 col-xs-8">
                                                            <div class="row">
                                                                <div class="col-md-7 col-sm-7 col-xs-12">
                                                                    <input id="when"  name="user" type="hidden" value="<?php echo $_SESSION['user_no']?>">
                                                                    <input name="LANG_CLASS_NAME" class="form-control col-md-12 col-xs-12" type="text" required>  
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="item form-group">
                                                        <label for="name" class="control-label col-md-2 col-sm-2 col-xs-2">Session</label>
                                                        <div class="col-md-8 col-sm-8 col-xs-8">
                                                            <div class="row">
                                                                <div class="col-md-7 col-sm-7 col-xs-12">
                                                                    <select name="SESSION_NO" class="form-control col-md-12 col-xs-12" required>
                                                                        <option value="">Select Session</option>
                                                                        <option value="1">Session 1</option>
                                                                        <option value="2">Session 2</option>
                                                                        <option value="3">Session 3</option>
                                                                        <option value="4">Session 4</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="item form-group">
                                                        <label for="name" class="control-label col-md-2 col-sm-2 col-xs-2">Level</label>
                                                        <div class="col-md-8 col-sm-8 col-xs-8">
                                                            <div class="row">
                                                                <div class="col-md-7 col-sm-7 col-xs-12">
                                                                    <select name="LEVEL_NO" class="form-control col-md-12 col-xs-12" required>
                                                                        <option value="">Select Level</option>
                                                                        <option value="1">1</option>
                                                                        <option value="2">2</option>
                                                                        <option value="3">3</option>
                                                                        <option value="4">4</option>
                                                                        <option value="5">5</option>
                                                                        <option value="6">6</option>
                                                                        <option value="7">7</option>
                                                                    </select>
                                                                    </select>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="item form-group">
                                                        <label for="name" class="control-label col-md-2 col-sm-2 col-xs-2">Room</label>
                                                        <div class="col-md-8 col-sm-8 col-xs-8">
                                                            <div class="row">
                                                                <div class="col-md-7 col-sm-7 col-xs-12">
                                                                    <select name="ROOM_NO" class="form-control col-md-12 col-xs-12" required>
                                                                        <option value="">Select Room</option>
                                                                        <?php
                                                                            $sql="select * from room";
                                                                             $q=mysql_query($sql);
                                                                             while($row=mysql_fetch_array($q))
                                                                            	{
                                                                            ?>
                                                                        <option value="<?=$row['ROOM_NO']?>"><?=$row['ROOM_DESC']?></option>
                                                                        <?php } ?>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <h4>
                                                        <fieldset>Teacher</fieldset>
                                                    </h4>
                                                    <div class="control-label col-md-12 col-sm-12 col-xs-12">
                                                        <div class="col-md-2 col-sm-2 col-xs-2">
                                                            <div class="form-group">
                                                                Teacher
                                                                <select name="TEACHER_NO" class="form-control col-md-12 col-xs-12" required>
                                                                    <option value="">Select Teacher</option>
                                                                    <?php
                                                                        $sql1="select * from teacher";
                                                                         $q1=mysql_query($sql1);
                                                                         while($row1=mysql_fetch_array($q1))
                                                                        	{
                                                                        ?>
                                                                    <option value="<?=$row1['TEACHER_NO']?>"><?=$row1['TEACHER_CODE']?></option>
                                                                    <?php } ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2 col-sm-2 col-xs-2">
                                                            <div class="form-group">Mon Hrs
                                                                <input type="number" class="form-control col-md-12 col-xs-12" placeholder="0" name="CUSTOM_1" required>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2 col-sm-2 col-xs-2">
                                                            <div class="form-group">Tue Hrs
                                                                <input type="number" class="form-control col-md-12 col-xs-12" placeholder="0" name="CUSTOM_2" required>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2 col-sm-2 col-xs-2">
                                                            <div class="form-group">Wed Hrs
                                                                <input type="number" class="form-control col-md-12 col-xs-12" placeholder="0" name="CUSTOM_3" required>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2 col-sm-2 col-xs-2">
                                                            <div class="form-group">Thu Hrs
                                                                <input type="number" class="form-control col-md-12 col-xs-12" placeholder="0" name="CUSTOM_4" required>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2 col-sm-2 col-xs-2">
                                                            <div class="form-group">Fri Hrs
                                                                <input type="number" class="form-control col-md-1 col-xs-1" placeholder="0" name="CUSTOM_5" required>
                                                            </div>
                                                        </div>
                                                         <div class="col-md-2 col-sm-2 col-xs-2">
                                                            <div class="form-group">Sat Hrs
                                                                <input type="number" class="form-control col-md-12 col-xs-12" placeholder="0" name="CUSTOM_6" >
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2 col-sm-2 col-xs-2">
                                                            <div class="form-group">Sun Hrs
                                                                <input type="number" class="form-control col-md-12 col-xs-12" placeholder="0" name="CUSTOM_7" >
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="item form-group">
                                                        <label for="name" class="control-label col-md-3 col-sm-3 col-xs-12"></label>
                                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                                            <div class="row">
                                                                <div class="col-md-6 col-sm-4 col-xs-12">
                                                                    <p class="pull-left">
                                                                        <button class="btn btn-primary" type="submit">Cancel</button>
                                                                        <button class="btn btn-success" type="submit">Submit</button>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
      </div>
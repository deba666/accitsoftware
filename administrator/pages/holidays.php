<?php
    /*
        * @package    ACCIT
         * @author    Paperlink Softwares Team
         * @copyright    Copyright (c) 2016 , Paperlink Softwares Pvt. Ltd.  (http://www.paperlinksoftwares.com/)
         * @since    Version 1.0.0
         * @filesource
		 * Code for display all the holidays list in Holiday
         */
    ?>
<div class="right_col" role="main">
    <?php 
        if($_GET['del'])
        {
        	if(delete_record("DELETE FROM `holiday` WHERE `HOLIDAY_NO` = ".$_GET['del']))
        	{
        		$_SESSION['s_msg']="Item successfully deleted";
        	}
        }?>
    <div class="">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="page-title">
                <div class="title_left">
                    <?php 
                        if($_SESSION['s_msg'] || $_SESSION['e_msg'])
                        {
                        ?>
                    <div class="alert <?=($_SESSION['s_msg'])?'alert-success':'alert-error'?> fade in">
                        <button data-dismiss="alert" class="close" type="button">�</button>
                        <?=($_SESSION['s_msg'])?$_SESSION['s_msg']:$_SESSION['e_msg']?>
                    </div>
                    <?php
                        unset($_SESSION['s_msg']);
                        unset($_SESSION['e_msg']);
                        }
                        ?>
                    <h3>Holiday List 
             <!-- <a href="#" onclick="javascript:void window.open('dashboard.php?op=<?=MD5('holidays_add')?>&s_no=<?=$std_row6[0]['EXT_STN']?>&s_fname=<?=$std_row6[0]['FNAME']?>&s_lname=<?=$std_row6[0]['LNAME']?>&s_mname=<?=$std_row6[0]['MNAME']?>&ag_name=<?=$std_row[0]['AGENT_NAME']?>&cus_no=<?=$std_row[0]['CUSTOMER_NO']?>','1422228621367','width=700,height=500,toolbar=0,menubar=0,location=1,status=1,scrollbars=1,resizable=1,left=0,top=0');return false;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Add Holiday</a></h3>-->
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="clearfix"></div>
                    <div class="x_content">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel recent-app">
                                    <div class="x_content">
                                        <div class="student-account-page view-faculty-page">
                                            <!-- Nav tabs -->
                                            <div class="card">
                                                <div class="all-students-list add student">
                                                    <div class="add-student-section">
                                                        <div class="table-responsive">
                                                            <table class="display table table-striped table-bordered table data-tbl-tools" cellspacing="0" width="100%">
                                                                <thead>
                                                                    <tr class="headings">
                                                                        <th class="column-title">Faculty
                                                                        </th>
                                                                        <th class="column-title">NAME
                                                                        </th>
                                                                        <th class="column-title">From Date
                                                                        </th>
                                                                        <th class="column-title">To Date
                                                                        </th>
                                                                        <th class="column-title">Option
                                                                        </th>
                                                                        <th class="column-title">Action
                                                                        </th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <?php $sql="select * from  holiday";
                                                                        $q=mysql_query($sql);
                                                                        while($row=mysql_fetch_array($q))
                                                                        {
                                                                        	if(!$row['DELETE_DATE'])
                                                                        	{
                                                                        	?>
                                                                    <?php $cond="where `CRT_NO`=".$row['CRT_NO']; $loc=getRows($cond,'crse_type');?>
                                                                    <td class=" "><?=$loc[0]['CRT_NAME'];?> </td>
                                                                    <td class=" "><?=$row['NAME'];?></td>
                                                                    <td class=" "><?=$row['FROM_DATE'];?></td>
                                                                    <td class=" "><?=$row['TO_DATE'];?></td>
                                                                    <td class=" "><?php if($row['OPTION_NO']==1) { ?>Extend<?php } else { ?>Don't Extend <?php } ?></td>
                                                                    <td class="last"><a href="dashboard.php?op=<?=MD5('holidays_edit')?>&HOLIDAY_NO=<?=$row['HOLIDAY_NO'];?>">Edit</a>| <a href="dashboard.php?op=<?=MD5('holidays')?>&del=<?=$row['HOLIDAY_NO'];?>" onclick="return confirm('Are you sure?'); return false;" class="color-sky stting"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>Delete</a></td>
                                                                    </tr>
                                                                    <?php } } ?>                                                  
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
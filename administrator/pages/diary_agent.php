<?php
    /*
        * @package    ACCIT
         * @author    Paperlink Softwares Team
         * @copyright    Copyright (c) 2016 , Paperlink Softwares Pvt. Ltd.  (http://www.paperlinksoftwares.com/)
         * @since    Version 1.0.0
         * @filesource
		 * Code for Display all the agent diary fetch from agent and diary table. Links come from the sidebar under diary option
         */
    ?>
<div style="min-height: 648px;" class="right_col" role="main">
    <div class="">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="page-title">
                <div class="title_left">
                    <h3>Agent Diary</h3>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="clearfix"></div>
                    <div class="x_content">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel recent-app">
                                    <div class="x_content">
                                        <div class="student-account-page">
                                            <!-- Nav tabs -->
                                            <div class="card">
                                                <ul class="nav nav-tabs" role="tablist">
                                                    <li role="presentation" class="active"><a href="#all-student" aria-controls="all-student" role="tab" data-toggle="tab">Agent Diary</a></li>
                                                </ul>
                                                <div class="tab-content">
                                                    <div role="tabpanel" class="tab-pane active" id="all-student">
                                                        <div class="all-students-list">
                                                            <div class="table-responsive">
                                                                <div>
                                                                    <div>
                                                                        <table class="table data-tbl-tools" cellspacing="0" width="100%">
                                                                            <thead>
                                                                                <tr class="headings">
                                                                                    <th class="column-title">Date
                                                                                    </th>
                                                                                    <th class="column-title">Attention
                                                                                    </th>
                                                                                    <th class="column-title">Agent Code
                                                                                    </th>
                                                                                    <th class="column-title">Subject
                                                                                    </th>
                                                                                    <th class="column-title">Agent Name
                                                                                    </th>
                                                                                    <th class="column-title">Notes
                                                                                    </th>
                                                                                    <th class="column-title">User
                                                                                    </th>
                                                                                    <th class="column-title">When
                                                                                    </th>
																					<th class="column-title">Action
                                                                                   </th>
                                                                                    
                                                                                </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                                <?php 
																				$sql="select * from agent";
																				$query=mysql_query($sql);
																				while($row=mysql_fetch_array($query))
																				{
																					$cond="where `CUSTOMER_NO`=".$row['CUSTOMER_NO']; 
                                                                                    $std=getRows($cond,'diary');
																					foreach($std as $diary)
																					{
                                                                                 ?>
                                                                                <tr class="even pointer">
                                                                                    <td class=" "><?= $diary['DIARY_DATE']?></td>
                                                                                    <td class=" "><?=($diary['ATTENTION'])?'<input disabled type="checkbox" checked class="flat">':'<input disabled type="checkbox" class="flat">'?></td>
                                                                                    <td class=" "><?= $row['AGENT_CODE']?></td>
                                                                                    <td class=" "><?= $diary['SUMMARY']?></td>
                                                                                    <td class=" "><?= $row['AGENT_NAME']?></td>
                                                                                    <td class=" "><?=$diary['NOTES']?></td>
                                                                                     <?php $cond1="where `USER_NO`=".$diary['USER_NO']; $user=getRows($cond1,'users');?>
                                                                                    <td class=" "><?=$user[0]['INIT']?></td>
                                                                                    <?php  $cou= $diary['DIARY_NO']?>
																					<td class=" "><?=$diary['WHO_DATE']?></td>																									<td><a href="#" onclick="javascript:void window.open('dashboard.php?op=<?=MD5('display_agent_diary_edit')?>&d_no=<?php echo $cou;?>&a_id=<?=$row['AGENT_NO']?>&s_no=<?=$row['AGENT_CODE']?>&a_na=<?=$row['AGENT_NAME']?>','1470138621367','width=800,height=500,toolbar=0,menubar=0,location=1,status=1,scrollbars=1,resizable=1,left=0,top=0');return false;">Modify</a>
                                                                            </td>
                                                                                </tr>
                                                                                <?php 
																				} } ?>                                                  
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
    /*
        * @package    ACCIT
         * @author    Paperlink Softwares Team
         * @copyright    Copyright (c) 2016 , Paperlink Softwares Pvt. Ltd.  (http://www.paperlinksoftwares.com/)
         * @since    Version 1.0.0
         * @filesource
		 * code for disply all the offer which are created for all students
         */
    ?>
<div role="main" class="right_col" style="min-height: 648px;">
    <div class="">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="page-title">
                <div class="title_left">
                    <h3>All Offer</h3>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="clearfix"></div>
                    <div class="x_content">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel recent-app">
                                    <div class="x_content">
                                        <div class="student-account-page">
                                            <!-- Nav tabs -->
                                            <div class="card">
                                                <ul role="tablist" class="nav nav-tabs">
                                                    <li class="active" role="presentation"><a data-toggle="tab" role="tab" aria-controls="all-student" href="#all-student">All Offer</a></li>
                                                </ul>
                                                <div class="tab-content">
                                                    <div id="all-student" class="tab-pane active" role="tabpanel">
                                                        <div class="all-students-list">
                                                            <div class="table-responsive">
                                                                <div>
                                                                    <table class="table data-tbl-tools">
                                                                        <thead>
                                                                            <tr class="headings" role="row">
                                                                                <th class="column-title sorting_asc">Offer No.</th>
                                                                                <th class="column-title">Status</th>
                                                                                <th class="column-title">Student No</th>
                                                                                <th class="column-title">First Name</th>
                                                                                <th class="column-title">Middle Name</th>
                                                                                <th class="column-title">Last Name</th>
                                                                               
                                                                                <th class="column-title">Offer Date</th>
                                                                           
                                                                                <th class="column-title no-link last">
                                                                                    <span class="nobr">Action</span>
                                                                                </th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                            <?php $s="select * from offer";
                                                                                $q=mysql_query($s);
                                                                                while($row=mysql_fetch_array($q))
                                                                                { 
																				if($row['CUSTOMER_NO']){
                                                                                 $s1="select * from student where CUSTOMER_NO=".$row['CUSTOMER_NO'];
                                                                                $q1=mysql_query($s1);
                                                                                while($row1=mysql_fetch_array($q1))
                                                                                { 
																				if ($row['STATUS_NO'] == '1') { 
																				$status="Preparing";}
																				else if ($row['STATUS_NO'] == '2') { 
																				$status="Offer Made";}
																				else if ($row['STATUS_NO'] == '3') { 
																				$status="Accepted";}
																				else if ($row['STATUS_NO'] == '4') { 
																				$status="Declined";}
																				else if ($row['STATUS_NO'] == '5') { 
																				$status="Withdrawn";}
                                                                                if($row['REF_CUSTOMER_NO'])
																				{
																				$agent="where `AGENT_NO`=".$row['REF_CUSTOMER_NO'];
                                                                                $agent_name=getRows($agent,'agent');
                                                                                  } ?>                                   
                                                                            <tr class="pointer odd" role="row">
                                                                                <td class="" tabindex="0"><?=$row['OFFER_NO'];?></td>
                                                                                <td class=" "><?php echo $status;?></td>
                                                                                <td class=" "><?=$row1['EXT_STN'];?></td>
                                                                                <td class=" "><?=$row1['LNAME'];?></td>
                                                                                <td class=" "><?=$row1['MNAME'];?></td>
                                                                                <td class=" "><?=$row1['FNAME'];?></td>
                                                                                
                                                                                <td class=" "><?=$row['OFFER_DATE'];?></td>
																				<?php if($row['USER_NO'])
																				{
																				$user="where `USER_NO`=".$row['USER_NO'];
                                                                                $user_name=getRows($user,'users');
                                                                                  } 
                                                                                  $name = $row1['FNAME'] ." ". $row1['MNAME'] ." ". $row1['LNAME'];

                                                                                  ?>

                                                                              
                                                                                <td class="last"><a class="color-sky stting" href="dashboard.php?op=<?=MD5('offer_details')?>&oid=<?=$row['OFFER_NO'];?>&cust_no=<?=$row['CUSTOMER_NO'];?>"><i aria-hidden="true" class="fa fa-cog"></i>Edit</a>
                                                                                    
                                                                               </td>
                                                                            </tr>
                                                                            <?php 
                                                                                } } }?>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
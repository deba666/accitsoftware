<?php
    /*
        * @package    ACCIT
         * @author    Paperlink Softwares Team
         * @copyright    Copyright (c) 2016 , Paperlink Softwares Pvt. Ltd.  (http://www.paperlinksoftwares.com/)
         * @since    Version 1.0.0
         * @filesource
		 * Code for Display dashboard.
         */
    ?>
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Dashboard</h3>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="clearfix"></div>
                    <div class="x_content">
                        <div class="row">
                            <div class="col-md-4 col-sm-4 col-xs-12 student-block-dashbord">
                                <div class="page-flow-grid">
                                    <a href="dashboard.php?op=<?=MD5('student')?>">
                                        <div class="student-grid bg-orange">
                                            <div class="content pull-left">
                                                <h4>Students</h4>
                                            </div>
                                            <div class="icon pull-right">
                                                <img src="images/student.png" class="img-responsive">
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-12 account-block-dashbord">
                                <div class="page-flow-grid">
                                    <a href="">
                                        <div class="student-grid bg-see-green">
                                            <div class="content pull-left">
                                                <h4>
                                                    Account
                                                </h4>
                                            </div>
                                            <div class="icon pull-right">
                                                <img src="images/account.png" class="img-responsive">
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-12 workflow-block-dashbord">
                                <div class="page-flow-grid">
                                    <a href="dashboard.php?op=<?=MD5('workflow')?>">
                                        <div class="student-grid workflow bg-sky-blue">
                                            <div class="content pull-left">
                                                <h4>Workflow</h4>
                                            </div>
                                            <div class="icon pull-right">
                                                <img src="images/architecher.png" class="img-responsive">
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel recent-app">
                                    <div class="x_title">
                                        <h2>Recent Applications</h2>
                                    </div>
                                    <div class="x_content">
                                        <div class="table-responsive">
                                            <table class="table data-tbl-tools ">
                                                <thead>
                                                    <tr class="headings">
                                                        <th class="column-title">NUMBER</th>
                                                        <th class="column-title">Name</th>
<th class="column-title">Father's Name</th>
<th class="column-title">Mother's Name</th>
                                                        <th class="column-title">Email</th>
                                                        <th class="column-title">Phone</th>
                                                        <th class="column-title">DOB</th>
                                                        <th class="column-title">Gender</th>
                                                        <th class="column-title">Address</th>
                                                        <th class="column-title">Country</th>
                                                        <th class="column-title no-link last"><span class="nobr">Action</span>
                                                        </th>
                                                        <th class="bulk-actions" colspan="7">
                                                            <a class="antoo" style="color:#fff; font-weight:500;">Action( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php $std_row=getRows('order by `STUD_NO` DESC ','student'); ?>
                                                    <?php
                                                        $sl=0;
                                                        foreach($std_row as $row)
                                                        {
                                                        ?>
                                                    <tr class="even pointer">
                                                       <td class=" "><?=$row['STUD_NO'];?></td>
                                                        <td class=" "><?=$row['FNAME'];?> <?=$row['LNAME'];?></td>
<td class=" "><?=$row['FATHERS_NAME'];?></td>
<td class=" "><?=$row['MOTHERS_NAME'];?></td>
                                                        <td class=" "><?=$row['L_EMAIL'];?></td>
                                                        <td class=" "><?=$row['L_MOBILE'];?></td>
                                                        <?PHP
                                                            $originalDate5 = $row['DOB'];
                                                            $newDate5 = date("d/m/y", strtotime($originalDate5)); ?>
                                                        <td class=" "><?php echo $newDate5 ?></td>
                                                        <td class=" "><?=$row['GENDER'];?></td>
                                                        <td class=""><?=$row['LADDR1'];?></td>
                                                        <td class=""><?=$row['COUNTRY_NAME'];?></td>
                                                        <td class="last"><a href="dashboard.php?op=<?=MD5('student_edit')?>&sid=<?=$row['STUD_NO'];?>&st_id=<?=$row['student_id'];?>" class="color-sky stting"><i class="fa fa-cog" aria-hidden="true"></i>Edit</a>| <a href="dashboard.php?op=<?=MD5('student_details')?>&sid=<?=$row['STUD_NO'];?>" class="color-sky stting"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>Details</a>
                                                        </td>
                                                    </tr>
                                                    <?php   }    ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
    /*
        * @package    ACCIT
         * @author    Paperlink Softwares Team
         * @copyright    Copyright (c) 2016 , Paperlink Softwares Pvt. Ltd.  (http://www.paperlinksoftwares.com/)
         * @since    Version 1.0.0
         * @filesource
		 * Code for displaying all the coirses (Edit , delete links are added bellow in this code)
         */
    ?>
<div class="right_col" role="main">
    <?php
        if($_GET['del'])
        {
        	if(delete_record("DELETE FROM `course` WHERE `COURSE_NO` = ".$_GET['del']))
        	{ 
        		$_SESSION['s_msg']="Item successfully deleted";
        	}
        }?>
    <div class="">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="page-title">
                <div class="title_left">
                    <?php 
                        if($_SESSION['s_msg'] || $_SESSION['e_msg'])
                        {
                        ?>
                    <div class="alert <?=($_SESSION['s_msg'])?'alert-success':'alert-error'?> fade in">
                        <button data-dismiss="alert" class="close" type="button">×</button>
                        <?=($_SESSION['s_msg'])?$_SESSION['s_msg']:$_SESSION['e_msg']?>
                    </div>
                    <?php
                        unset($_SESSION['s_msg']);
                        unset($_SESSION['e_msg']);
                                               }
                                              ?>
                    <h3>Course</h3>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="clearfix"></div>
                    <div class="x_content">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel recent-app">
                                    <div class="x_content">
                                        <div class="student-account-page view-faculty-page">
                                            <!-- Nav tabs -->
                                            <div class="card">
                                                <div class="all-students-list add student">
                                                    <div class="add-student-section">
                                                        <div class="table-responsive">
                                                            <table class="table data-tbl-tools" cellspacing="0" width="100%">
                                                                <thead>
                                                                    <tr class="headings">
                                                                        <th class="column-title">Code
                                                                        </th>
                                                                        <th class="column-title">Name
                                                                        </th>
                                                                        <th class="column-title">Display Name
                                                                        </th>
                                                                        <th class="column-title">Length
                                                                        </th>
                                                                        
                                                                        
                                                                        <th class="column-title">Cost
                                                                        </th>
                                                                        <th class="column-title">Alt. Cost
                                                                        </th>
                                                                        <th class="column-title">Location
                                                                        </th>
                                                                        <th class="column-title">Faculty
                                                                        </th>
                                                                      
                                                                        <th class="column-title">Action
                                                                        </th>
                                                                    </tr>
                                                                </thead>
																
                                                                <?php 
																//Displaying all the courses from course table
																$course_row=getRows('order by `COURSE_NO` DESC','course'); ?>
                                                                <tbody>
                                                                    <?php
                                                                        $sl=0;
                                                                        foreach($course_row as $row)
                                                                        {
                                                                        ?>
                                                                    <tr class="even pointer">
                                                                        <td class=" "><?=$row['CODE'];?></td>
                                                                        <td class=" "><?=$row['COURSE_NAME'];?></td>
                                                                        <td class=" "><?=$row['DISPLAY_NAME'];?></td>
                                                                        <td class=" "><?=$row['COURSE_LEN'];?> Weeks </td>
                                                                        
                                                                        
                                                                        <td class=" ">$<?=$row['COURSE_COST'];?></td>
                                                                        <td class=" ">$<?=$row['COURSE_COST_OTHER'];?></td>
                                                                        <?php $cond="where `USER_NO`=".$row['USER_NO']; $fac=getRows($cond,'users');?>
                                                                        <?php if($fac[0]['LOCATION_NO']){$cond="where `LOCATION_NO`=".$fac[0]['LOCATION_NO']; $loc=getRows($cond,'location'); }?>
                                                                        <td class=" "><?=$loc[0]['LOCATION_NAME'];?></td>
                                                                        <?php $cond="where `CRT_NO`=".$row['CRT_NO']; $crt=getRows($cond,'crse_type');?>
                                                                        <td class=" "><?=$crt[0]['CRT_NAME'];?></td>
                                                                       
                                                                        
                                                                        <td class="last"><a href="dashboard.php?op=<?=MD5('course_edit')?>&edit=<?=$row['COURSE_NO'];?>" class="color-sky stting"><i class="fa fa-cog" aria-hidden="true"></i>Edit</a>| <a href="dashboard.php?op=<?=MD5('course')?>&del=<?=$row['COURSE_NO'];?>" onclick="return confirm('Are you sure?'); return false;" class="color-sky stting"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>Delete</a></td>
                                                                    </tr>
                                                                    <?php } ?>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
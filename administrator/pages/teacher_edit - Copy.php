<?php
    /*
        * @package    ACCIT
         * @author    Paperlink Softwares Team
         * @copyright    Copyright (c) 2016 , Paperlink Softwares Pvt. Ltd.  (http://www.paperlinksoftwares.com/)
         * @since    Version 1.0.0
         * @filesource
		 * Code for edit existing teacher
         */
    
    if($_POST['code']){
    	   if(mysql_query("UPDATE `teacher` SET `TEACHER_CODE`='".$_POST['code']."', `LOCATION_NO`='".$_POST['location']."',`TEACHER_EMAIL`='".$_POST['email']."', `TEACHER_PHONE`='".$_POST['phone']."', `TEACHER_LEVEL`='".$_POST['level']."', `STATUS_NO`='".$_POST['status']."' WHERE `TEACHER_NO`=".$_GET['tid']))
    		$_SESSION['s_msg']="<strong>Well done!</strong> Item successfully updated";
    	else
    		$_SESSION['e_msg']="<strong>Oh snap!</strong> Item is not updated";
    
    }
    
    if($_GET['tid'])
    {
    	$cond="where `TEACHER_NO`=".$_GET['tid'];
    	$std_row=getRows($cond,'teacher');
    }?>
<div class="right_col" role="main">
    <div class="">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="page-title">
                <div class="title_left">
                    <?php 
                        if($_SESSION['s_msg'] || $_SESSION['e_msg'])
                        {
                        ?>
                    <div class="alert <?=($_SESSION['s_msg'])?'alert-success':'alert-error'?> fade in">
                        <button data-dismiss="alert" class="close" type="button">�</button>
                        <?=($_SESSION['s_msg'])?$_SESSION['s_msg']:$_SESSION['e_msg']?>
                    </div>
                    <?php
                        unset($_SESSION['s_msg']);
                        unset($_SESSION['e_msg']);
                        }
                        ?>
                    <h3>Teacher Details</h3>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="clearfix"></div>
                    <div class="x_content">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel recent-app">
                                    <div class="x_content">
                                        <div class="student-account-page">
                                            <!-- Nav tabs -->
                                            <div class="card">
                                                <div class="all-students-list add-course">
                                                    <div class="add-student-section">
                                                        <form class="form-horizontal form-label-left" action="" method="POST" novalidate>
                                                            <button type="submit" class="btn btn-success" onclick="needToConfirm = false;">Save</button>
                                                            <ul id="student-acc-tab" class="nav nav-tabs" role="tablist">
                                                                <li role="presentation"  class="active"><a href="#details" aria-controls="details" role="tab" data-toggle="tab">Details</a></li>
                                                            </ul>
                                                            <div class="tab-content">
                                                                <!--Details-->
                                                                <div role="tabpanel" class="tab-pane active" id="details">
                                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                                        <div class="row">
                                                                            <h4>
                                                                                <fieldset>Basic Info</fieldset>
                                                                            </h4>
                                                                            <div class="clearfix"></div>
                                                                            <div class="table-responsive">
                                                                                <table class="table table-striped jambo_table">
                                                                                    <tbody>
                                                                                        <tr class="even pointer">
                                                                                            <td class="item">Name</td>
                                                                                            <td class=""><input type="text" value="<?=$std_row[0]['TEACHER_CODE']?>" name="code" required></td>
                                                                                        </tr>
                                                                                        <tr class="even pointer">
                                                                                            <td class="item">Location</td>
                                                                                            <td class="">
                                                                                                <select id="location" name="location" class="form-control">
                                                                                                    <option value="1">SYDNEY</option>
                                                                                                    <option value="2">KOLKATA</option>
                                                                                                </select>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr class="even pointer">
                                                                                            <td class="item">Email</td>
                                                                                            <td class=""> <input type="text" value="<?=$std_row[0]['TEACHER_EMAIL']?>" name="email"></td>
                                                                                        </tr>
                                                                                        <tr class="even pointer">
                                                                                            <td class="item">Phone</td>
                                                                                            <td class=""><input type="text" value="<?=$std_row[0]['TEACHER_PHONE']?>" name="phone"></td>
                                                                                        </tr>
                                                                                        <tr class="even pointer">
                                                                                            <td class="item">Status</td>
                                                                                            <td class="">
                                                                                                <select id="location" name="status" class="form-control">
                                                                                                    <option value="1" <?php if($options=="1") echo 'selected="selected"'; ?> >Active</option>
                                                                                                    <option value="0" <?php if($options=="0") echo 'selected="selected"'; ?> >Inactive</option>
                                                                                                </select>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr class="even pointer">
                                                                                            <td class="item">Level</td>
                                                                                            <td class=""><input type="number" step="1" min="1" max="" value="<?=$std_row[0]['TEACHER_LEVEL']?>" name="level" id="quantity" title="Qty" class="form-control input-text qty text" size="4"></td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                        </form>
                                                        </div>
                                                        <!--notes-->
                                                        <div role="tabpanel" class="tab-pane" id="notes">
                                                        </div>
                                                        </div>
                                                    </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<?php
    /*
        * @package    ACCIT
         * @author    Paperlink Softwares Team
         * @copyright    Copyright (c) 2016 , Paperlink Softwares Pvt. Ltd.  (http://www.paperlinksoftwares.com/)
         * @since    Version 1.0.0
         * @filesource
         */
    
    if($_GET['del2'])
        {
        	if(delete_record("DELETE FROM `diary` WHERE `DIARY_NO` = ".$_GET['del2']))
        		$_SESSION['s_msg']="Item successfully deleted";
        }
    	?>
<div role="main" class="right_col" style="min-height: 648px;">
    <div class="">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="page-title">
                <div class="title_left">
                    <h3>All Diary(Student)</h3>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="clearfix"></div>
                    <div class="x_content">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel recent-app">
                                    <div class="x_content">
                                        <div class="student-account-page">
                                            <!-- Nav tabs -->
                                            <div class="card">
                                                <ul role="tablist" class="nav nav-tabs">
                                                    <li class="active" role="presentation"><a data-toggle="tab" role="tab" aria-controls="all-student" href="#all-student">Diary</a></li>
                                                </ul>
                                                <div class="tab-content">
                                                    <div id="all-student" class="tab-pane active" role="tabpanel">
                                                        <div class="all-students-list">
                                                            <div class="table-responsive">
                                                                <div>
                                                                    <div>
                                                                        <div>
                                                                            <table class="table data-tbl-tools">
                                                                                <div class="hide-show-column dropdown" role="presentation">
                                                                                    <a id="drop4" href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" role="button" aria-expanded="false">Hide / Show Column<span class="caret"></span></a>
                                                                                    <ul id="menu6" class="dropdown-menu animated fadeInDown" role="menu">
                                                                                        <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                                                            <span class="pull-left">
                                                                                            <input type="checkbox" class="flat">
                                                                                            </span>
                                                                                            <span class="pull-left">
                                                                                            Student No
                                                                                            </span>
                                                                                            </a>
                                                                                        </li>
                                                                                        <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                                                            <span class="pull-left">
                                                                                            <input type="checkbox" class="flat">
                                                                                            </span>
                                                                                            <span class="pull-left">
                                                                                            Attention
                                                                                            </span>
                                                                                            </a>
                                                                                        </li>
                                                                                        <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                                                            <span class="pull-left">
                                                                                            <input type="checkbox" class="flat">
                                                                                            </span>
                                                                                            <span class="pull-left">
                                                                                            First Name
                                                                                            </span>
                                                                                            </a>
                                                                                        </li>
                                                                                        <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                                                            <span class="pull-left">
                                                                                            <input type="checkbox" class="flat">
                                                                                            </span>
                                                                                            <span class="pull-left">
                                                                                            Middle Name
                                                                                            </span>
                                                                                            </a>
                                                                                        </li>
                                                                                        <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                                                            <span class="pull-left">
                                                                                            <input type="checkbox" class="flat">
                                                                                            </span>
                                                                                            <span class="pull-left">
                                                                                            Last Name
                                                                                            </span>
                                                                                            </a>
                                                                                        </li>
                                                                                        <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                                                            <span class="pull-left">
                                                                                            <input type="checkbox" class="flat">
                                                                                            </span>
                                                                                            <span class="pull-left">
                                                                                            Date
                                                                                            </span>
                                                                                            </a>
                                                                                        </li>
                                                                                        <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                                                            <span class="pull-left">
                                                                                            <input type="checkbox" class="flat">
                                                                                            </span>
                                                                                            <span class="pull-left">
                                                                                            Subjects
                                                                                            </span>
                                                                                            </a>
                                                                                        </li>
                                                                                        <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                                                            <span class="pull-left">
                                                                                            <input type="checkbox" class="flat">
                                                                                            </span>
                                                                                            <span class="pull-left">
                                                                                            Notes
                                                                                            </span>
                                                                                            </a>
                                                                                        </li>
                                                                                        <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                                                            <span class="pull-left">
                                                                                            <input type="checkbox" class="flat">
                                                                                            </span>
                                                                                            <span class="pull-left">
                                                                                            User
                                                                                            </span>
                                                                                            </a>
                                                                                        </li>
                                                                                        <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                                                            <span class="pull-left">
                                                                                            <input type="checkbox" class="flat">
                                                                                            </span>
                                                                                            <span class="pull-left">
                                                                                            When
                                                                                            </span>
                                                                                            </a>
                                                                                        </li>
                                                                                    </ul>
                                                                                </div>
                                                                                <thead>
                                                                                    <tr class="headings">
                                                                                        <th class="column-title">Attention
                                                                                        </th>
                                                                                        <th class="column-title">Date 
                                                                                        </th>
                                                                                        <th class="column-title">Category
                                                                                        </th>
                                                                                        <th class="column-title">Subjects
                                                                                        </th>
                                                                                        <th class="column-title">User
                                                                                        </th>
                                                                                        <th class="column-title">When
                                                                                        </th>
                                                                                        <th class="column-title">Detail
                                                                                        </th>
                                                                                        <th class="column-title">Action
                                                                                        </th>
                                                                                    </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                    <?php
                                                                                        $con1="where `CUSTOMER_NO`=".$_GET['cus_no'];
                                                                                        $std_r1=getRows($con1,'diary');
                                                                                        foreach($std_r1 as $std_r2)
                                                                                        {
                                                                                        $con2="where `LOOKUP_CODE_NO`=".$std_r2['CATEGORY_NO'];
                                                                                        $std_r3=getRows($con2,'lookup_code');
                                                                                        foreach($std_r3 as $std_r4)
                                                                                        {
                                                                                        $con3="where `USER_NO`=".$std_r2['USER_NO'];
                                                                                        $std_r5=getRows($con3,'users');
                                                                                        foreach($std_r5 as $std_r6)
                                                                                        {
                                                                                         ?>
                                                                                    <tr class="even pointer">
                                                                                        <td class=" "><?=($std_r2['ATTENTION'])? '<input type="checkbox" checked disabled />':'<input disabled type="checkbox"  />' ?></td>
                                                                                        <td class=" "><?=$std_r2['DIARY_DATE']?></td>
                                                                                        <td class=" "><?=$std_r4['NAME']?></td>
                                                                                        <td class=" "><?=$std_r2['SUMMARY']?></td>
                                                                                        <td class=" "><?=$std_r6['INIT']?></td>
                                                                                        <td class=" "><?=$std_r2['WHO_DATE']?></td>
                                                                                        <td class=" "><?=$std_r2['NOTES']?></td>
                                                                                        <td><a href="#" onclick="javascript:void window.open('dashboard.php?op=<?=MD5('display_student_diary_edit')?>&d_no=<?=$std_r2['DIARY_NO']?>&s_no=<?=$std_row[0]['EXT_STN']?>&s_fname=<?=$std_row[0]['FNAME']?>&s_lname=<?=$std_row[0]['LNAME']?>&s_mname=<?=$std_row6[0]['MNAME']?>&ag_name=<?=$std_row[0]['AGENT_NAME']?>','1470138621367','width=800,height=500,toolbar=0,menubar=0,location=1,status=1,scrollbars=1,resizable=1,left=0,top=0');return false;">Edit</a>|<a href="dashboard.php?op=<?=MD5('all_student_diary')?>&del2=<?=$std_r2['DIARY_NO']?>&cus_no=<?=$_GET['cus_no']?>&s_no=<?=$_GET['s_no']?>" class="color-sky stting"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>Delete</a></td>
                                                                                    </tr>
                                                                                    <?php
                                                                                        }}} 
                                                                                         ?>
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
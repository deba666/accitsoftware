<?php
    /*
        * @package    ACCIT
         * @author    Paperlink Softwares Team
         * @copyright    Copyright (c) 2016 , Paperlink Softwares Pvt. Ltd.  (http://www.paperlinksoftwares.com/)
         * @since    Version 1.0.0
         * @filesource
		 * Code for update the Student Airport
         */
    
    if($_POST['F_DATE'])
    	{
    	if(mysql_query("update `student_airport_transfer` set `STATUS_NO`='".$_POST['STATUS_NO']."',`LOCATION_NO`='".$_POST['LOCATION_NO']."',`F_DATE`='".$_POST['F_DATE']."',`F_TIME`='".$_POST['F_TIME']."',`F_FROM`='".$_POST['F_FROM']."',`F_TO`='".$_POST['F_TO']."',`F_NAME`='".$_POST['F_NAME']."',`PICKUP`='".$_POST['PICKUP']."',`NOTES`='".$_POST['NOTES']."',`USER_NO`='".$_POST['USER_NO']."',`WHO_DATE`='".$_POST['WHO_DATE']."'WHERE `AIRPORT_PICKUP_NO`=".$_GET['AIRPORT_PICKUP_NO']))
    	  
    		$_SESSION['s_msg']="<strong>Fine!</strong>Transportation is Updated";
    		
    		else
    			$_SESSION['e_msg']="<strong>Oh snap!</strong> Transportation is not Update";
    		
    	}
    ?>
<?php if($_GET['AIRPORT_PICKUP_NO'])
    {
    	$cond="where `AIRPORT_PICKUP_NO`=".$_GET['AIRPORT_PICKUP_NO'];
    	$std_row=getRows($cond,'student_airport_transfer');
    	//$name = $std_row[0]['FNAME'] ." ". $std_row[0]['MNAME'] ." ". $std_row[0]['LNAME'];
    
    	$cond3="where `STUD_NO`=".$std_row[0]['STUD_NO'];
    	$std_row3=getRows($cond3,'student');
    }
    ?>
<div role="main" class="right_col" style="min-height: 738px;">
<div class="">
<div class="col-md-12 col-sm-12 col-xs-12">
    <div class="page-title">
        <div class="title_left">
            <?php 
                if($_SESSION['s_msg'] || $_SESSION['e_msg'])
                {
                ?>
            <div class="alert <?=($_SESSION['s_msg'])?'alert-success':'alert-error'?> fade in">
                <button data-dismiss="alert" class="close" type="button">�</button>
                <?=($_SESSION['s_msg'])?$_SESSION['s_msg']:$_SESSION['e_msg']?>
            </div>
            <?php
                unset($_SESSION['s_msg']);
                unset($_SESSION['e_msg']);
                }
                ?>
            <h3>Airport Pickup</h3>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<div class="row">
<div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
        <div class="clearfix"></div>
        <div class="x_content">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel recent-app">
                        <div class="x_content">
                            <div class="student-account-page">
                                <!-- Nav tabs -->
                                <div class="card">
                                    <div class="all-students-list add student">
                                        <div class="add-student-section">
                                            <form novalidate="" method="POST" action="">
                                                <button class="btn btn-success" type="submit" onclick="needToConfirm = false;">Save</button>
                                                <ul role="tablist" class="nav nav-tabs" id="student-acc-tab">
                                                    <li role="presentation" class="active"></li>
                                                </ul>
                                                <div class="tab-content">
                                                    <!--Offer-->
                                                    <div id="offer" class="tab-pane active" role="tabpanel">
                                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                                            <?php $cond27=" where `USER_NO`=".$_SESSION['user_no'];
                                                                $std_row27=getRows($cond27,'users'); ?>
                                                            <input id="when"  name="USER_NO" type="hidden" value="<?php echo $_SESSION['user_no']?>">
                                                            <?php date_default_timezone_set('Australia/sydney');
                                                                $date = date('d/m/Y h:i:s a', time());
                                                                ?>
                                                            <input id="who"  name="WHO_DATE"  type="hidden" value="<?php echo $date?>">
                                                            <div class="row">
                                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                                    <div class="form-group">
                                                                        Student<input Value="<?=$std_row3[0]['EXT_STN']?>&nbsp;&nbsp;&nbsp;<?=$std_row3[0]['FNAME']?>" class="form-control col-md-12 col-sm-12 col-xs-12" name="F_DATE" placeholder="" type="text">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-12 col-sm-12">
                                                                    <div class="row">
                                                                        <fieldset>
                                                                        </fieldset>
                                                                        <div class="col-md-6 col-sm-6 col-xs-6">
                                                                            <div class="form-group">
                                                                                Status
                                                                                <select name="STATUS_NO" class="form-control col-md-12 col-sm-12 col-xs-12">
                                                                                    <option value="1" <?php if ($std_row[0]['STATUS_NO'] == '1') { echo 'selected="selected"';}?>>Request</option>
                                                                                    <option value="2" <?php if ($std_row[0]['STATUS_NO'] == '2') { echo 'selected="selected"';}?>>Confirmed</option>
                                                                                    <option value="3" <?php if ($std_row[0]['STATUS_NO'] == '3') { echo 'selected="selected"';}?>>Completed</option>
                                                                                    <option value="4" <?php if ($std_row[0]['STATUS_NO'] == '4') { echo 'selected="selected"';}?>>Cancelled</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-6 col-sm-6 col-xs-6">
                                                                            Location
                                                                            <select name="LOCATION_NO" class="form-control col-md-12 col-sm-12 col-xs-12">
                                                                                <option value="1" <?php if ($std_row[0]['LOCATION_NO'] == '1') { echo 'selected="selected"';}?>>SYDNEY</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-md-6 col-sm-6 col-xs-6">
                                                                            <div class="form-group">
                                                                                Tentative Arrival  Date<input Value="<?=$std_row[0]['F_DATE']?>" class="form-control col-md-12 col-sm-12 col-xs-12" name="F_DATE" placeholder="" type="date">
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-6 col-sm-6 col-xs-6">
                                                                            Arrival Time<input class="form-control col-md-12 col-sm-12 col-xs-12" name="F_TIME" Value="<?=$std_row[0]['F_TIME']?>" type="time">
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-md-6 col-sm-6 col-xs-6">
                                                                            <div class="form-group">
                                                                                Flight<input class="form-control col-md-12 col-sm-12 col-xs-12" name="F_NAME" Value="<?=$std_row[0]['F_NAME']?>" type="text">
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-6 col-sm-6 col-xs-6">
                                                                            Pickup By<input class="form-control col-md-12 col-sm-12 col-xs-12" name="PICKUP" Value="<?=$std_row[0]['PICKUP']?>" type="text">
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-md-6 col-sm-6 col-xs-6">
                                                                            <div class="form-group">
                                                                                Coming From<input class="form-control col-md-12 col-sm-12 col-xs-12" name="F_FROM" Value="<?=$std_row[0]['F_FROM']?>" type="text">
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-6 col-sm-6 col-xs-6">
                                                                            Going To<input class="form-control col-md-12 col-sm-12 col-xs-12" Value="<?=$std_row[0]['F_TO']?>" type="text" name="F_TO">
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                                                        <div class="row">
                                                                            <div class="col-md-12 col-sm-12">
                                                                                <div class="row">
                                                                                    <fieldset>
                                                                                        <h4>Notes</h4>
                                                                                    </fieldset>
                                                                                    <div class="row">
                                                                                        <div class="col-md-4 col-sm-4 col-xs-4">
                                                                                            <div class="item form-group">
                                                                                                <textarea name="NOTES" cols="60" rows="7" style="margin: 0px; width: 780px; height: 154px;"><?=$std_row[0]['NOTES']?></textarea>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!--Offer-->
                                                            </div>
                                            			</form>
                                            		</div>
                                            	</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<title>Modify Student</title>
<?php
   /*
       * @package    ACCIT
        * @author    Paperlink Softwares Team
        * @copyright    Copyright (c) 2016 , Paperlink Softwares Pvt. Ltd.  (http://www.paperlinksoftwares.com/)
        * @since    Version 1.0.0
        * @filesource
        * Code for update student details
        */
if($_FILES['file']['name']!='')
{
    $cond="where `STUD_NO`=".$_GET['sid'];//fetch all details of student from dtudent table
       $std_row=getRows($cond,'student');
       $img_name = $std_row[0]['PHOTO'];
       if($img_name!='')
       {
        @unlink($img_name);   
       }
       
    $validextensions = array("jpeg", "jpg", "png");
            $temporary = explode(".", $_FILES["file"]["name"]);
            $file_extension = end($temporary);
        
            if ((($_FILES["file"]["type"] == "image/png") || ($_FILES["file"]["type"] == "image/jpg") || ($_FILES["file"]["type"] == "image/jpeg")
                    ) && ($_FILES["file"]["size"] < 1000000)//Approx. 100kb files can be uploaded.
                    && in_array($file_extension, $validextensions)) {
        
                if ($_FILES["file"]["error"] > 0) {
                    echo "Return Code: " . $_FILES["file"]["error"] . "<br/><br/>";
                } else {
                    
                    //echo "<span>Your File Uploaded Succesfully...!!</span><br/>";
                    //echo "<br/><b>File Name:</b> " . $_FILES["file"]["name"] . "<br>";
                    //echo "<b>Type:</b> " . $_FILES["file"]["type"] . "<br>";
                    //echo "<b>Size:</b> " . ($_FILES["file"]["size"] / 1024) . " kB<br>";
                   // echo "<b>Temp file:</b> " . $_FILES["file"]["tmp_name"] . "<br>";
        
        
                    if (file_exists("upload/" . $_FILES["file"]["name"])) {
                        echo $_FILES["file"]["name"] . " <b>already exists.</b> ";
                    } else {
                        move_uploaded_file($_FILES["file"]["tmp_name"], "upload/" . time().".".$file_extension);
                        $imgFullpath = "http://".$_SERVER['SERVER_NAME'].dirname($_SERVER["REQUEST_URI"].'?').'/'. "upload/" . time().".".$file_extension;
                       // echo "<b>Stored in:</b><a href = '$imgFullpath' target='_blank'> " .$imgFullpath.'<a>';
        
                    }
                 mysql_query("update `student` set `PHOTO`='".$imgFullpath."' where `STUD_NO`=".$_GET['sid']) ;
                }
            } else {
                //echo "<span>***Invalid file Size or Type***<span>";
            }
}
   if($_GET['nid']){
   mysql_query("update `notification` set `STATUS_NO`='0' where `NOTIFICATION_NO`=".$_GET['nid']) ;
   }
      $choice=$_POST['langOpt1'];
      $choice1=implode(',',$choice);
      
       $choice2=$_POST['langOpt'];
      $choice3=implode(',',$choice2);
   if($_POST['DOB'])
      {
		//$sql= "update `student` set `EXT_STN`='".$_POST['EXT_STN']."',`GENDER`='".$_POST['GENDER']."',`DOB`='".$_POST['DOB']."',`TITLE`='".$_POST['TITLE']."',`LNAME`='".$_POST['LNAME']."',`MNAME`='".$_POST['MNAME']."',`FNAME`='".$_POST['FNAME']."',`FATHERS_NAME`='".$_POST['FATHERS_NAME']."',`MOTHERS_NAME`='".$_POST['MOTHERS_NAME']."',`CUSTOM_1`='".$_POST['CUSTOM_1']."',`CUSTOM_2`='".$_POST['CUSTOM_2']."',`CUSTOM_3`='".$_POST['CUSTOM_3']."',`CUSTOM_4`='".$_POST['CUSTOM_4']."',`CUSTOM_5`='".$_POST['CUSTOM_5']."',`CUSTOM_6`='".$_POST['CUSTOM_6']."',`CUSTOM_7`='".$_POST['CUSTOM_7']."',`CUSTOM_8`='".$_POST['CUSTOM_8']."',`CUSTOM_5`='".$_POST['CUSTOM_5']."',`CUSTOM_5`='".$_POST['CUSTOM_5']."',`CUSTOM_6`='".$_POST['CUSTOM_6']."',`CUSTOM_7`='".$_POST['CUSTOM_7']."',`CUSTOM_8`='".$_POST['CUSTOM_8']."',`PASSPORT_CODE`='".$_POST['PASSPORT_CODE']."',`ALUMNI`='',`HSTAY`='',`CITIZEN_COUNTRY_NO`='".$_POST['COUNTRY_NO']."',`CITIZEN_COUNTRY_NAME`='".$_POST['COUNTRY_NAME']."',`COUNTRY_NAME`='".$_POST['COUNTRY_NAME']."',`COUNTRY_NO`='".$_POST['COUNTRY_NO']."',`OS`='".$_POST['OS']."',`LANGUAGE_NO`='".$_POST['LANGUAGE_NO']."',`WHO_DATE`='".$_POST['WHO_DATE']."',`USER_NO`='".$_POST['USER_NO']."',`L_EMAIL`='".$_POST['L_EMAIL']."',`L_MOBILE`='".$_POST['L_MOBILE']."',`S_NOTES`='".$_POST['S_NOTES']."',`POST_TO`='".$_POST['POST_TO']."',`LADDR1`='".$_POST['LADDR1']."',`LADDR2`='".$_POST['LADDR2']."',`LADDR3`='".$_POST['LADDR3']."',`LSUB`='".$_POST['LSUB']."',`LSTATE`='".$_POST['LSTATE']."',`LPCODE`='".$_POST['LPCODE']."',`L_PH`='".$_POST['L_PH']."',`L_FAX`='".$_POST['L_FAX']."',`O_PH`='".$_POST['O_PH']."',`O_FAX`='".$_POST['O_FAX']."',`OADDR1`='".$_POST['OADDR1']."',`OADDR2`='".$_POST['OADDR2']."',`OADDR3`='".$_POST['OADDR3']."',`OSUB`='".$_POST['OSUB']."',`OSTATE`='".$_POST['OSTATE']."',`OPCODE`='".$_POST['OPCODE']."',`JOB_SEEK`='',`JOB_FND`='',`JF_DATE`='',`HS_NOTES`='',`O_MOBILE`='',`O_EMAIL`='',`APU_REQ`='',`HOLD`='',`BIRTH_COUNTRY_NAME`='".$_POST['BIRTH_COUNTRY_NAME']."',`FINISH_TYPE_NO`='',`PATHWAY_COLLEGE_NO`='',`PATHWAY_CRSE_NO`='',`PATHWAY_LEVEL_NO`='',`ATTENDANCE`='',`STUDY_TOUR_NO`='',`STUDENT_USER_NO`='',`VET_SUBURB_NO`='',`VET_COUNTRY_NO`='',`VET_INDIGENOUS_NO`='".$_POST['VET_INDIGENOUS_NO']."',`VET_LANGUAGE_NO`='',`VET_ENGLISH_LEVEL_NO`='".$_POST['VET_ENGLISH_LEVEL_NO']."',`VET_EMPLOYMENT_STATUS_NO`='".$_POST['VET_EMPLOYMENT_STATUS_NO']."',`VET_STUDY_REASON_NO`='',`VET_SCHOOL_LEVEL_NO`='".$_POST['VET_SCHOOL_LEVEL_NO']."',`VET_SCHOOL_YEAR`='".$_POST['VET_SCHOOL_YEAR']."',`VET_SCHOOL_FLAG`='".$_POST['VET_SCHOOL_FLAG']."',`PATHWAY_START_DATE`='',`PATHWAY_STUDENT_CODE`='',`PATHWAY_ENTRANCE_SCORE`='',`TEST_TYPE_NO`='',`TEST_SCORE`='',`TEST_DATE`='',`PATHWAY_TRACK_FLAG`='',`VET_VSN_CODE`='',`VET_VSN_STATUS_NO`='',`PASSPORT_TYPE_CODE`='',`PASSPORT_ISSUER`='',`PASSPORT_ISSUE_DATE`='',`PASSPORT_EXPIRY_DATE`='',`PASSPORT_COUNTRY_NO`='',`PASSPORT_PLACE_BIRTH`='',`PATHWAY_MONITOR_FLAG`='',`VET_ENGLISH_NO`='',`VET_WA_CODE`='',`VET_SCHOOL_CODE`='',`VET_TRS_CODE`='',`VET_CHESSN_CODE`='',`VET_TERTIARY_ENTRANCE_SCORE`='',`VET_EDUCATION_LEVEL_NO`='$choice3',`VET_EDUCATION_LEVEL_YEAR`='',`VET_USI`='".$_POST['VET_USI']."',`LADDR_PROPERTY`='".$_POST['LADDR_PROPERTY']."',`LADDR_UNITNO`='".$_POST['LADDR_UNITNO']."',`LADDR_STREETNO`='".$_POST['LADDR_STREETNO']."',`LADDR_STREETNAME`='".$_POST['LADDR_STREETNAME']."',`LADDR_SUBURB_NO`='".$_POST['LADDR_SUBURB_NO']."',`PADDR_PROPERTY`='',`PADDR_UNITNO`='',`PADDR_STREETNO`='',`PADDR_STREETNAME`='',`PADDR_DELIVERY_BOX`='',`PADDR_SUBURB_NO`='',`PADDR1`='',`PADDR2`='',`PADDR3`='',`PSUB`='',`PSTATE`='".$_POST['PSTATE']."',`PPCODE`='',`USI_DOCUMENT_TYPE_NO`='',`USI_STATUS_NO`='',`USI_DOC_CODE`='',`USI_DOC_DATE`='',`USI_DOC_YEAR`='',`USI_DOC_PRINTED`='',`USI_DOC_STATE_NO`='',`USI_DOC_CERTIFICATE_CODE`='',`USI_DOC_FIRST_NAME`='',`USI_DOC_LAST_NAME`='',`USI_DOC_CUSTOM_DATE_1`='',`USI_DOC_CUSTOM_STRING_1`='',`USI_DOC_CUSTOM_FLAG`='',`PREFERRED_PROGRAM_NO`='',`PREFERRED_CLASS_TYPE_NO`='',`VET_QLD_LUID`='',`VET_QLD_FULLTIME`='',`LADDR_DWELLINGTYPE_NO`='',`LADDR_FLOORNO`='',`LADDR_LOTNO`='',`LADDR_ROADTYPE_NO`='',`LADDR_ROADSUFFIX_NO`='',`PADDR_DWELLINGTYPE_NO`='',`PADDR_FLOORNO`='',`PADDR_LOTNO`='',`PADDR_ROADTYPE_NO`='',`LADDR_ROADNAME`='',`PADDR_ROADNAME`='',`LADDR_ROADNO`='',`PADDR_ROADNO`='',`VET_ETI_IDENTIFIER`='',`PADDR_ROADSUFFIX_NO`='',`TEST_CUSTOM_1`='',`TEST_CUSTOM_2`='',`TEST_CUSTOM_3`='',`TEST_CUSTOM_4`='',`TEST_CUSTOM_5`='',`TEST_CUSTOM_6`='',`PADDR_SAMEASRESIDENTIAL`='',`VETFEE_ARRIVALYEAR`='',`VETFEE_DISABILITYSUPPORT`='$choice1'";
   
	   if(mysql_query("update `student` set `EXT_STN`='".$_POST['EXT_STN']."',`GENDER`='".$_POST['GENDER']."',`DOB`='".$_POST['DOB']."',`TITLE`='".$_POST['TITLE']."',`LNAME`='".$_POST['LNAME']."',`MNAME`='".$_POST['MNAME']."',`FNAME`='".$_POST['FNAME']."',`FATHERS_NAME`='".$_POST['FATHERS_NAME']."',`MOTHERS_NAME`='".$_POST['MOTHERS_NAME']."',`CUSTOM_1`='".$_POST['CUSTOM_1']."',`CUSTOM_2`='".$_POST['CUSTOM_2']."',`CUSTOM_3`='".$_POST['CUSTOM_3']."',`CUSTOM_4`='".$_POST['CUSTOM_4']."',`CUSTOM_5`='".$_POST['CUSTOM_5']."',`CUSTOM_6`='".$_POST['CUSTOM_6']."',`CUSTOM_7`='".$_POST['CUSTOM_7']."',`CUSTOM_8`='".$_POST['CUSTOM_8']."',`CUSTOM_5`='".$_POST['CUSTOM_5']."',`CUSTOM_5`='".$_POST['CUSTOM_5']."',`CUSTOM_6`='".$_POST['CUSTOM_6']."',`CUSTOM_7`='".$_POST['CUSTOM_7']."',`CUSTOM_8`='".$_POST['CUSTOM_8']."',`PASSPORT_CODE`='".$_POST['PASSPORT_CODE']."',`ALUMNI`='',`HSTAY`='',`CITIZEN_COUNTRY_NO`='".$_POST['COUNTRY_NO']."',`CITIZEN_COUNTRY_NAME`='".$_POST['COUNTRY_NAME']."',`COUNTRY_NAME`='".$_POST['COUNTRY_NAME']."',`COUNTRY_NO`='".$_POST['COUNTRY_NO']."',`OS`='".$_POST['OS']."',`LANGUAGE_NO`='".$_POST['LANGUAGE_NO']."',`WHO_DATE`='".$_POST['WHO_DATE']."',`USER_NO`='".$_POST['USER_NO']."',`L_EMAIL`='".$_POST['L_EMAIL']."',`L_MOBILE`='".$_POST['L_MOBILE']."',`S_NOTES`='".$_POST['S_NOTES']."',`POST_TO`='".$_POST['POST_TO']."',`LADDR1`='".$_POST['LADDR1']."',`LADDR2`='".$_POST['LADDR2']."',`LADDR3`='".$_POST['LADDR3']."',`LSUB`='".$_POST['LSUB']."',`LSTATE`='".$_POST['LSTATE']."',`LPCODE`='".$_POST['LPCODE']."',`L_PH`='".$_POST['L_PH']."',`L_FAX`='".$_POST['L_FAX']."',`O_PH`='".$_POST['O_PH']."',`O_FAX`='".$_POST['O_FAX']."',`OADDR1`='".$_POST['OADDR1']."',`OADDR2`='".$_POST['OADDR2']."',`OADDR3`='".$_POST['OADDR3']."',`OSUB`='".$_POST['OSUB']."',`OSTATE`='".$_POST['OSTATE']."',`OPCODE`='".$_POST['OPCODE']."',`JOB_SEEK`='',`JOB_FND`='',`JF_DATE`='',`HS_NOTES`='',`O_MOBILE`='',`O_EMAIL`='',`APU_REQ`='',`HOLD`='',`BIRTH_COUNTRY_NAME`='".$_POST['BIRTH_COUNTRY_NAME']."',`FINISH_TYPE_NO`='',`PATHWAY_COLLEGE_NO`='',`PATHWAY_CRSE_NO`='',`PATHWAY_LEVEL_NO`='',`ATTENDANCE`='',`STUDY_TOUR_NO`='',`STUDENT_USER_NO`='',`VET_SUBURB_NO`='',`VET_COUNTRY_NO`='',`VET_INDIGENOUS_NO`='".$_POST['VET_INDIGENOUS_NO']."',`VET_LANGUAGE_NO`='',`VET_ENGLISH_LEVEL_NO`='".$_POST['VET_ENGLISH_LEVEL_NO']."',`VET_EMPLOYMENT_STATUS_NO`='".$_POST['VET_EMPLOYMENT_STATUS_NO']."',`VET_STUDY_REASON_NO`='',`VET_SCHOOL_LEVEL_NO`='".$_POST['VET_SCHOOL_LEVEL_NO']."',`VET_SCHOOL_YEAR`='".$_POST['VET_SCHOOL_YEAR']."',`VET_SCHOOL_FLAG`='".$_POST['VET_SCHOOL_FLAG']."',`PATHWAY_START_DATE`='',`PATHWAY_STUDENT_CODE`='',`PATHWAY_ENTRANCE_SCORE`='',`TEST_TYPE_NO`='',`TEST_SCORE`='',`TEST_DATE`='',`PATHWAY_TRACK_FLAG`='',`VET_VSN_CODE`='',`VET_VSN_STATUS_NO`='',`PASSPORT_TYPE_CODE`='',`PASSPORT_ISSUER`='',`PASSPORT_ISSUE_DATE`='',`PASSPORT_EXPIRY_DATE`='',`PASSPORT_COUNTRY_NO`='',`PASSPORT_PLACE_BIRTH`='',`PATHWAY_MONITOR_FLAG`='',`VET_ENGLISH_NO`='',`VET_WA_CODE`='',`VET_SCHOOL_CODE`='',`VET_TRS_CODE`='',`VET_CHESSN_CODE`='',`VET_TERTIARY_ENTRANCE_SCORE`='',`VET_EDUCATION_LEVEL_NO`='$choice3',`VET_EDUCATION_LEVEL_YEAR`='',`VET_USI`='".$_POST['VET_USI']."',`LADDR_PROPERTY`='".$_POST['LADDR_PROPERTY']."',`LADDR_UNITNO`='".$_POST['LADDR_UNITNO']."',`LADDR_STREETNO`='".$_POST['LADDR_STREETNO']."',`LADDR_STREETNAME`='".$_POST['LADDR_STREETNAME']."',`LADDR_SUBURB_NO`='".$_POST['LADDR_SUBURB_NO']."',`PADDR_PROPERTY`='',`PADDR_UNITNO`='',`PADDR_STREETNO`='',`PADDR_STREETNAME`='',`PADDR_DELIVERY_BOX`='',`PADDR_SUBURB_NO`='',`PADDR1`='',`PADDR2`='',`PADDR3`='',`PSUB`='',`PSTATE`='".$_POST['PSTATE']."',`PPCODE`='',`USI_DOCUMENT_TYPE_NO`='',`USI_STATUS_NO`='',`USI_DOC_CODE`='',`USI_DOC_DATE`='',`USI_DOC_YEAR`='',`USI_DOC_PRINTED`='',`USI_DOC_STATE_NO`='',`USI_DOC_CERTIFICATE_CODE`='',`USI_DOC_FIRST_NAME`='',`USI_DOC_LAST_NAME`='',`USI_DOC_CUSTOM_DATE_1`='',`USI_DOC_CUSTOM_STRING_1`='',`USI_DOC_CUSTOM_FLAG`='',`PREFERRED_PROGRAM_NO`='',`PREFERRED_CLASS_TYPE_NO`='',`VET_QLD_LUID`='',`VET_QLD_FULLTIME`='',`LADDR_DWELLINGTYPE_NO`='',`LADDR_FLOORNO`='',`LADDR_LOTNO`='',`LADDR_ROADTYPE_NO`='',`LADDR_ROADSUFFIX_NO`='',`PADDR_DWELLINGTYPE_NO`='',`PADDR_FLOORNO`='',`PADDR_LOTNO`='',`PADDR_ROADTYPE_NO`='',`LADDR_ROADNAME`='',`PADDR_ROADNAME`='',`LADDR_ROADNO`='',`PADDR_ROADNO`='',`VET_ETI_IDENTIFIER`='',`PADDR_ROADSUFFIX_NO`='',`TEST_CUSTOM_1`='',`TEST_CUSTOM_2`='',`TEST_CUSTOM_3`='',`TEST_CUSTOM_4`='',`TEST_CUSTOM_5`='',`TEST_CUSTOM_6`='',`PADDR_SAMEASRESIDENTIAL`='',`VETFEE_ARRIVALYEAR`='',`VETFEE_DISABILITYSUPPORT`='$choice1' where `STUD_NO`=".$_GET['sid'])){ //update student details
		
       if(mysql_query("update `customer` set `TYPE_NO`='1',`NAME`='".$_POST['LNAME']."',`CODE`='".$_POST['EXT_STN']."',`STATUS_NO`='1',`NOTES`='',`CUSTOM_1`='',`CUSTOM_2`='',`CUSTOM_3`='',`CUSTOM_4`='',`CUSTOM_5`='',`CUSTOM_6`='',`CUSTOM_7`='',`CUSTOM_8`='',`CUSTOM_9`='',`CUSTOM_10`='',`TAX_CODE`='',`DELETE_DATE`='',`LOCK_NUM`='1',`PER_TITLE`='".$_POST['TITLE']."',`PER_FIRST_NAME`='".$_POST['FNAME']."',`PER_MIDDLE_NAME`='".$_POST['MNAME']."',`PER_LAST_NAME`='".$_POST['LNAME']."',`PER_NICK_NAME`='',`PER_DOB`='".$_POST['DOB']."',`PER_GENDER`='".$_POST['GENDER']."',`PUBLISH_FLAG`='0',`DISPLAY_NAME`='',`ADDRESS1`='',`ADDRESS2`='',`ADDRESS3`='',`ADDRESS4`='',`P_ADDRESS1`='',`P_ADDRESS2`='',`P_ADDRESS3`='',`P_ADDRESS4`='',`PHONE`='',`MOBILE`='',`FAX`='',`WWW`='',`EMAIL`='',`LOCATION_NO`='0',`ACCOUNT_CODE`='',`CREATE_DATE`='".$_POST['WHO_DATE']."',`USER_NO`='".$_POST['USER_NO']."' where `REF_NO`=".$_GET['sid'])) //update customer table for student
   
               $_SESSION['s_msg']="<strong>Fine!</strong>Student Update Successfully";
       }
       else{
               $_SESSION['e_msg']="<strong>Oh snap!</strong> Student is not Modify";
       }
   }
   if($_GET['sid'])
   {
       $cond="where `STUD_NO`=".$_GET['sid'];//fetch all details of student from student  table
       $std_row=getRows($cond,'student');
       $name = $std_row[0]['FNAME'] ." ". $std_row[0]['MNAME'] ." ". $std_row[0]['LNAME'];
   
       $cond3="where `USER_NO`=".$std_row[0]['USER_NO'];
       $std_row3=getRows($cond3,'users');
   }
   if($_GET['del'])
       {
           if(delete_record("DELETE FROM `stu_contact` WHERE `STU_CON_NO` = ".$_GET['del']))
               $_SESSION['s_msg']="Item successfully deleted";
       }
       
       if($_GET['del2'])
       {
           if(delete_record("DELETE FROM `diary` WHERE `DIARY_NO` = ".$_GET['del2']))
               $_SESSION['s_msg']="Item successfully deleted";
       }
       if($_GET['del12'])
       {
           if(delete_record("DELETE FROM `student_airport_transfer` WHERE `AIRPORT_PICKUP_NO` = ".$_GET['del12']))
               $_SESSION['s_msg']="Item successfully deleted";
       }
       if($_GET['del123'])
       {
           if(delete_record("DELETE FROM `customer_visa` WHERE `CUSTOMER_VISA_NO` = ".$_GET['del123']))
               $_SESSION['s_msg']="Item successfully deleted";
       }
       if($_GET['del1234'])
       {
           if(delete_record("DELETE FROM `attend_tracking` WHERE `ATTEND_TRACKING_NO` = ".$_GET['del1234']))
               $_SESSION['s_msg']="Item successfully deleted";
       }
      
   if($_GET['offer_del'])
       {
           if(delete_record("DELETE FROM `offer` WHERE `OFFER_NO` = ".$_GET['offer_del']))
           {
               if(delete_record("DELETE FROM `offer_item` WHERE `OFFER_NO` = ".$_GET['offer_del']))
               {
                   if(delete_record("DELETE FROM `offer_instalment` WHERE `OFFER_NO` = ".$_GET['offer_del']))
                   $_SESSION['s_msg']="Item successfully deleted";
               }
           }
       }
   
   ?>
<div role="main" class="right_col" style="min-height: 686px;">
   <div class="">
      <div class="col-md-12 col-sm-12 col-xs-12">
         <div class="page-title">
            <div class="title_left">
               <?php 
                  if($_SESSION['s_msg'] || $_SESSION['e_msg'])
                  {
                  ?>
               <div class="alert <?=($_SESSION['s_msg'])?'alert-success':'alert-error'?> fade in">
                  <button data-dismiss="alert" class="close" type="button">×</button>
                  <?=($_SESSION['s_msg'])?$_SESSION['s_msg']:$_SESSION['e_msg']?>
               </div>
               <?php
                  unset($_SESSION['s_msg']);
                  unset($_SESSION['e_msg']);
                   }
                  ?>
				  <?
				//include "breadcrumb.php";
				 session_start();
				 foreach($_SESSION['pages'] as $key=>$val)
				 {
					 if($val==$_SERVER['REQUEST_URI'])
						 unset($_SESSION['pages'][$key]);
				 }	
						
				if(isset($_POST['save_data']))
					unset($_SESSION['pages']['Modify-'.strtoupper($std_row[0]['FNAME']).'']);
				else
					$_SESSION['pages']['Modify-'.strtoupper($std_row[0]['FNAME']).''] = $_SERVER['REQUEST_URI'];
				//echo "<pre>"; print_r($_SESSION['pages']);die();
				?>
				<ul class="breadcrumb">
				<?php
				foreach($_SESSION['pages'] as $key=>$val)
				{
						print "<li><a href=\"$val\">$key</a></li>";
				}
				?>
				</ul>
               <h3>Modify Student -><?= strtoupper($std_row[0]['FNAME'])."      ".strtoupper($std_row[0]['LNAME'])?></h3>
            </div>
         </div>
      </div>
      <div class="clearfix"></div>
      <div class="row">
         <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
               <div class="clearfix"></div>
               <div class="x_content">
                  <div class="row">
                     <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel recent-app">
                           <div class="x_content">
                              <div class="student-account-page">
                                 <!-- Nav tabs -->
                                 <div class="card">
                                    <div class="all-students-list add student">
                                       <div class="add-student-section">
                                          <form method="post" action="" class="form-horizontal form-label-left" 
                                             enctype="multipart/form-data">
                                             <button type="submit" class="btn btn-success">Save</button>
                                             <ul role="tablist" class="nav nav-tabs" id="student-acc-tab">
                                                                <li role="presentation" class=""><a href="#ste_details" aria-controls="details" role="tab" data-toggle="tab">Details</a></li>
                                                                <li role="presentation" class=""><a href="#ste_address" aria-controls="ste_address" role="tab" data-toggle="tab">Address</a></li>
                                                                <li role="presentation"><a href="#ste_avetmiss" aria-controls="ste_avetmiss" role="tab" data-toggle="tab">Avetmiss</a></li>
                                                                <li role="presentation" class="active"><a href="#ste_enrolments" aria-controls="ste_enrolments" role="tab" data-toggle="tab">Enrolments</a></li>
                                                                <li role="presentation" class=""><a href="#ste_offer" aria-controls="ste_offer" role="tab" data-toggle="tab">Offers</a></li>
                                                                <li role="presentation"><a href="#ste_diary" aria-controls="ste_diary" role="tab" data-toggle="tab">Diary</a></li>
                                                                
                                                                <li role="presentation"><a href="#ste_visa" aria-controls="ste_visa" role="tab" data-toggle="tab">Visa</a></li>
                                                                <input type="hidden" name="save_data">
                                                                <!-- <li role="presentation"><a href="#ste_insurance" aria-controls="ste_insurance" role="tab" data-toggle="tab">Insurance</a></li> -->
                                                               <!--<li role="presentation"><a href="#ste_surveys" aria-controls="ste_surveys" role="tab" data-toggle="tab">Surveys</a></li>-->
                                                                <li role="presentation" class=""><a href="#ste_photo" aria-controls="ste_photo" role="tab" data-toggle="tab">Photo</a></li>
                                                               
                                                            </ul>
                                             
                                             <div class="tab-content">
                                                <!--Details-->
                                                <div role="tabpanel" class="tab-pane active" id="ste_details">
                                                   <div class="col-md-6 col-sm-6 col-xs-12">
                                                      <div class="row">
                                                         <h4>
                                                            <fieldset>Basic Info
                                                            </fieldset>
                                                         </h4>
                                                         <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <div class="form-group" id="frmCheckUsername">
                                                               <label class="pull-left control-label col-md-12 col-sm-12 col-xs-12" for="name">Student No
                                                               </label>
                                                               <input class="control-label col-md-12 col-sm-12 col-xs-12" name="EXT_STN" Value="<?= $std_row[0]['EXT_STN']?>" type="text" readonly>
                                                            </div>
                                                         </div>
                                                         <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <div class="form-group">
                                                               <label class="control-label col-md-12 col-sm-12 col-xs-12" for="name">Gender
                                                               </label>
                                                               <select id="GENDER" name="GENDER" class="col-md-12 col-xs-12 form-control">
                                                                  <option value="Male" <?php if ($std_row[0]['GENDER'] == 'Male') { echo 'selected="selected"';}?>>Male</option>
                                                                  <option value="Female" <?php if ($std_row[0]['GENDER'] == 'Female') { echo 'selected="selected"';}?>>Female</option>
                                                               </select>
                                                            </div>
                                                         </div>
                                                         <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <div class="item form-group">
                                                               <label class="control-label col-md-12 col-sm-12 col-xs-12">DOB
                                                               </label>
                                                               <input Value="<?=$std_row[0]['DOB']?>" name="DOB" class="date-picker form-control col-md-12 col-xs-12" placeholder="MM/DD/YYYY" type="date">
                                                               <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                                            </div>
                                                         </div>
                                                         <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <div class="form-group">
                                                               <label class="control-label col-md-12 col-sm-12 col-xs-12" for="name">Passport No
                                                               </label>
                                                               <input Value="<?=$std_row[0]['PASSPORT_CODE']?>" name="PASSPORT_CODE" class="col-md-12 col-sm-12 col-xs-12 form-control" placeholder="Passport No" type="text">
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div class="row">
                                                         <div class="col-md-3 col-sm-3 col-xs-3">
                                                            <label class="pull-left control-label col-md-12 col-sm-12 col-xs-12" for="name">Title
                                                            </label>
                                                            <select id="TITLE" name="TITLE"  class="col-md-12 col-sm-12 col-xs-12 form-control">
                                                               <option value="1"<?php if ($std_row[0]['TITLE'] == '1') { echo 'selected="selected"';}?>>Mr</option>
                                                               <option value="2"<?php if ($std_row[0]['TITLE'] == '2') { echo 'selected="selected"';}?>>Dr</option>
                                                               <option value="3"<?php if ($std_row[0]['TITLE'] == '3') { echo 'selected="selected"';}?>>Sir</option>
                                                               <option value="4"<?php if ($std_row[0]['TITLE'] == '4') { echo 'selected="selected"';}?>>Ms</option>
                                                               <option value="5"<?php if ($std_row[0]['TITLE'] == '5') { echo 'selected="selected"';}?>>Mrs</option>
                                                               <option value="6"<?php if ($std_row[0]['TITLE'] == '6') { echo 'selected="selected"';}?>>Miss</option>
                                                            </select>
                                                         </div>
                                                         <div class="col-md-3 col-sm-3 col-xs-3">
                                                            <div class="form-group">
                                                               <label class="pull-left control-label col-md-12 col-sm-12 col-xs-12" for="name">Last Name
                                                               </label>
                                                                <input Value="<?= $std_row[0]['LNAME']?>" class="form-control col-md-12 col-sm-12 col-xs-12" name="LNAME" placeholder="Enter Name" type="text" required>
                                                            </div>
                                                         </div>
                                                         <div class="col-md-3 col-sm-3 col-xs-3">
                                                            <div class="form-group">
                                                               <label class="pull-left control-label col-md-12 col-sm-12 col-xs-12" for="name">Middle Name
                                                               </label>
                                                               <input Value="<?= $std_row[0]['MNAME']?>" class="form-control col-md-12 col-sm-12 col-xs-12" name="MNAME" placeholder="Enter Name" type="text">
                                                            </div>
                                                         </div>
                                                         <div class="col-md-3 col-sm-3 col-xs-3">
                                                            <div class="form-group">
                                                               <label class="pull-left control-label col-md-12 col-sm-12 col-xs-12" for="name">First Name
                                                               </label>
                                                                <input Value="<?= $std_row[0]['FNAME']?>" class="form-control col-md-12 col-sm-12 col-xs-12" name="FNAME" placeholder="Enter Name" type="text" required>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <!--<div class="row">
                                                         <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <div class="item form-group">
                                                               <label class="control-label col-md-12 col-sm-12 col-xs-12">Father's Name
                                                               </label>
                                                               <input Value="<?= $std_row[0]['FATHERS_NAME']?>" name="FATHERS_NAME" class="form-control col-md-12 col-xs-12" placeholder="Father's Name" type="text">
                                                            </div>
                                                         </div>
                                                         <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <div class="form-group">
                                                               <label class="control-label col-md-12 col-sm-12 col-xs-12" for="name">Mother's Name
                                                               </label>
                                                               <input Value="<?= $std_row[0]['MOTHERS_NAME']?>" name="MOTHERS_NAME" class="form-control col-md-12 col-xs-12" placeholder="Mother's Name" type="text" >
                                                            </div>
                                                         </div>
                                                      </div>-->
                                                      <div class="row">
                                                         <div class="col-md-12 col-sm-12">
                                                            <div class="row">
                                                               <h4>
                                                                  <fieldset>Custom Fields</fieldset>
                                                               </h4>
                                                               <div class="col-md-6 col-sm-6 col-xs-6">
                                                                  <div class="form-group">
                                                                     <input Value="<?= $std_row[0]['CUSTOM_1']?>" class="form-control col-md-12 col-sm-12 col-xs-12" name="CUSTOM_1" placeholder="" type="text">
                                                                  </div>
                                                               </div>
                                                               <div class="col-md-6 col-sm-6 col-xs-6">
                                                                  <input Value="<?= $std_row[0]['CUSTOM_2']?>" class="form-control col-md-12 col-sm-12 col-xs-12" name="CUSTOM_2" placeholder="" type="text">
                                                               </div>
                                                            </div>
                                                            <div class="row">
                                                               <div class="col-md-6 col-sm-6 col-xs-6">
                                                                  <div class="form-group">
                                                                     <input Value="<?= $std_row[0]['CUSTOM_3']?>" class="form-control col-md-12 col-sm-12 col-xs-12" name="CUSTOM_3" placeholder="" type="text">
                                                                  </div>
                                                               </div>
                                                               <div class="col-md-6 col-sm-6 col-xs-6">
                                                                  <input Value="<?= $std_row[0]['CUSTOM_4']?>" class="form-control col-md-12 col-sm-12 col-xs-12" name="CUSTOM_4" placeholder="" type="text">
                                                               </div>
                                                            </div>
                                                            <div class="row">
                                                               <div class="col-md-6 col-sm-6 col-xs-6">
                                                                  <div class="form-group">
                                                                     <input Value="<?= $std_row[0]['CUSTOM_5']?>" class="form-control col-md-12 col-sm-12 col-xs-12" name="CUSTOM_5" placeholder="" type="text">
                                                                  </div>
                                                               </div>
                                                               <div class="col-md-6 col-sm-6 col-xs-6">
                                                                  <input Value="<?= $std_row[0]['CUSTOM_6']?>" class="form-control col-md-12 col-sm-12 col-xs-12"placeholder="" type="text" name="CUSTOM_6">
                                                               </div>
                                                            </div>
                                                            <div class="row">
                                                               <div class="col-md-6 col-sm-6 col-xs-6">
                                                                  <div class="form-group">
                                                                     <input  class="form-control col-md-12 col-sm-12 col-xs-12" value="<?php echo $std_row[0]['CUSTOM_7']; ?>" placeholder="" type="text" name="CUSTOM_7">
                                                                  </div>
                                                               </div>
                                                               <div class="col-md-6 col-sm-6 col-xs-6">
                                                                  <input  class="form-control col-md-12 col-sm-12 col-xs-12" value = "<?php echo $std_row[0]['CUSTOM_8'];?>" placeholder="" type="text" name="CUSTOM_8">
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="col-md-6 col-sm-6 col-xs-12">
                                                      <div class="row">
                                                         <h4>
                                                            <fieldset>Others Info</fieldset>
                                                         </h4>
                                                         <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <div class="form-group">
                                                               <label class="control-label col-md-12 col-sm-12 col-xs-12" for="name">CitiZenship
                                                               </label>
                                                                <select name="COUNTRY_NAME" class="col-md-12 col-xs-12 form-control" required>
                                                                  <option Value="">Select Country</option>
                                                                  <?php
                                                                     $sql="select * from country ORDER BY `COUNTRY_NAME` ASC";
                                                                     $dd_res=mysql_query("Select *  from country ORDER BY `COUNTRY_NAME` ASC");
                                                                                                                while($r=mysql_fetch_array($dd_res))
                                                                                                                   { 
                                                                                                                    if($std_row[0]['COUNTRY_NAME']==$r['COUNTRY_NAME'])
                                                                                                                      {
                                                                                                                        echo "<option value='$r[COUNTRY_NAME]' selected> $r[COUNTRY_NAME] </option>";
                                                                                                                        }
                                                                                                                         else
                                                                                                                         {
                                                                                                                          echo "<option value='$r[COUNTRY_NAME]'> $r[COUNTRY_NAME] </option>";
                                                                                                                           }
                                                                                                                     }
                                                                     ?>
                                                               </select>
                                                            </div>
                                                         </div>
                                                         <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <div class="form-group">
                                                               <label class="control-label col-md-12 col-sm-12 col-xs-12" for="name">Student Type
                                                               </label>
                                                               <select name="OS" class="col-md-12 col-xs-12 form-control">
                                                                  <option value="1"<?php if ($std_row[0]['OS'] == '1') { echo 'selected="selected"';}?>>Local Student</option>
                                                                  <option value="2"<?php if ($std_row[0]['OS'] == '2') { echo 'selected="selected"';}?>>Overseas Student</option>
                                                               </select>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div class="row">
                                                         <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <div class="form-group">
                                                               <label class="control-label col-md-12 col-sm-12 col-xs-12" for="name">Mother Toungue
                                                               </label>
                                                                <select name="LANGUAGE_NO" class="col-md-12 col-xs-12 form-control" required>
                                                                  <option Value="">Select Language</option>
                                                                  <?php
                                                                     $sql2="select * from language ORDER BY `LANGUAGE_DESC` ASC";
                                                                     $dd_res2=mysql_query("Select *  from language ORDER BY `LANGUAGE_DESC` ASC");
                                                                                                                while($r2=mysql_fetch_array($dd_res2))
                                                                                                                   { 
                                                                                                                    if($std_row[0]['LANGUAGE_NO']==$r2['LANGUAGE_NO'])
                                                                                                                      {
                                                                                                                        echo "<option value='$r2[LANGUAGE_NO]' selected> $r2[LANGUAGE_DESC] </option>";
                                                                                                                        }
                                                                                                                         else
                                                                                                                         {
                                                                                                                          echo "<option value='$r2[LANGUAGE_NO]'> $r2[LANGUAGE_DESC] </option>";
                                                                                                                           }
                                                                                                                     }
                                                                     ?>
                                                               </select>
                                                            </div>
                                                         </div>
                                                         <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <div class="row">
                                                               <div class="col-md-6 col-sm-6 col-xs-12">
                                                                  <label class="control-label col-md-12 col-sm-12 col-xs-12" for="name">Who
                                                                  </label>
                                                                  <?php
                                                                     if($std_row[0]['USER_NO']) {$cond27=" where `USER_NO`=".$std_row[0]['USER_NO'];
                                                                         $std_row27=getRows($cond27,'users'); }?>
                                                                  <input  type="text" value="<?php echo $std_row27[0]['INIT']?>" class="form-control col-md-12 col-sm-12 col-xs-12" >
                                                                  <input name="USER_NO" type="hidden" value="<?php echo $std_row27[0]['USER_NO']?>" class="form-control col-md-12 col-sm-12 col-xs-12" >
                                                               </div>
                                                               <div class="col-md-6 col-sm-6 col-xs-12">
                                                                  <label class="control-label col-md-12 col-sm-12 col-xs-12" for="name">When
                                                                  </label>
                                                                  <?php date_default_timezone_set('Australia/sydney');
                                                                     $date = date('d/m/Y h:i:s a', time());
                                                                     ?>
                                                                  <input  name="WHO_DATE" type="text" value="<?= $std_row[0]['WHO_DATE']?>" class="form-control col-md-12 col-sm-12 col-xs-12" >
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div class="row">
                                                         <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <div class="form-group">
                                                               <label class="control-label col-md-12 col-sm-12 col-xs-12" for="name">Email
                                                               </label>
                                                                <input type="email" Value="<?= $std_row[0]['L_EMAIL']?>" name="L_EMAIL" placeholder="Email" class="form-control col-md-12 col-xs-12" required >
                                                            </div>
                                                         </div>
                                                         <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <div class="form-group">
                                                               <label class="control-label col-md-12 col-sm-12 col-xs-12" for="name">Mobile
                                                               </label>
                                                               <input type="text" name="L_MOBILE" Value="<?= $std_row[0]['L_MOBILE']?>" placeholder="Mobile" class="form-control col-md-12 col-xs-12" required>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div class="row">
                                                         <h4>
                                                            <fieldset>Notes</fieldset>
                                                         </h4>
                                                         <div class="col-md-12 col-sm-12 col-xs-12">
                                                            <textarea name="S_NOTES" class="form-control" rows="3" style="margin: 0px; height: 186px; width: 501px;"><?= $std_row[0]['S_NOTES']?></textarea>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <!--Address -->
                                                <div role="tabpanel" class="tab-pane" id="ste_address">
                                                   <h4>
                                                      <fieldset>Address
                                                        
                                                      </fieldset>
                                                   </h4>
                                                   <div class="col-md-6 col-sm-6 col-xs-12">
                                                      <div class="row">
                                                         <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <div class="form-group">
                                                               <label class="pull-left control-label col-md-12 col-sm-12 col-xs-12" for="name">Post To
                                                               </label>
                                                               <select id="POST_TO" name="POST_TO" class="col-md-12 col-xs-12 form-control">
                                                                  <option value="1"<?php if ($std_row[0]['POST_TO'] == '1') { echo 'selected="selected"';}?>>Local Address</option>
                                                                  <option value="2"<?php if ($std_row[0]['POST_TO'] == '2') { echo 'selected="selected"';}?>>Overseas Address</option>
                                                               </select>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div class="row">
                                                         <div class="col-md-12 col-sm-12">
                                                            <div class="row">
                                                               <h4>
                                                                  <fieldset>Local Address</fieldset>
                                                               </h4>
                                                               <div class="col-md-12 col-sm-12 col-xs-12">
                                                                  <div class="form-group">
                                                                     <textarea name="LADDR1" class="form-control" rows="2"><?= $std_row[0]['LADDR1']?></textarea>
                                                                  </div>
                                                               </div>
                                                               <div class="col-md-12 col-sm-12 col-xs-12">
                                                                  <div class="form-group">
                                                                     <textarea name="LADDR2" class="form-control" rows="2"><?= $std_row[0]['LADDR2']?></textarea>
                                                                  </div>
                                                               </div>
                                                               <div class="col-md-12 col-sm-12 col-xs-12">
                                                                  <div class="form-group">
                                                                     <textarea name="LADDR3" class="form-control" rows="2"><?= $std_row[0]['LADDR3']?></textarea>
                                                                  </div>
                                                               </div>
                                                               <div class="col-md-12 col-sm-12 col-xs-12">
                                                                  <div class="row">
                                                                     <div class="col-md-4 col-sm-4 col-xs-12">
                                                                        <div class="form-group">
                                                                           <input type="text" placeholder="SubUrb" value="<?= $std_row[0]['LSUB']?>" name="LSUB" class="form-control col-md-12 col-xs-12">
                                                                        </div>
                                                                     </div>
                                                                     <div class="col-md-4 col-sm-4 col-xs-12">
                                                                        <div class="form-group">
                                                                           <input type="text" placeholder="State" value="<?= $std_row[0]['LSTATE']?>" name="LSTATE" class="form-control col-md-12 col-xs-12">
                                                                        </div>
                                                                     </div>
                                                                     <div class="col-md-4 col-sm-4 col-xs-12">
                                                                        <div class="form-group">
                                                                           <input type="text" placeholder="Post Code" value="<?= $std_row[0]['LPCODE']?>" name="LPCODE" class="form-control col-md-12 col-xs-12">
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                               <div class="row">
                                                                  <div class="col-md-6 col-sm-6 col-xs-6">
                                                                     <div class="form-group">
                                                                        <label class="pull-left control-label col-md-12 col-sm-12 col-xs-12" for="name">Phone No
                                                                        </label>
                                                                        <input value="<?= $std_row[0]['L_PH']?>" type="text" name="L_PH" placeholder="Phone No" class="form-control col-md-12 col-xs-12">
                                                                     </div>
                                                                  </div>
                                                                  <div class="col-md-6 col-sm-6 col-xs-6">
                                                                     <div class="form-group">
                                                                        <label class="pull-left control-label col-md-12 col-sm-12 col-xs-12" for="name">Fax
                                                                        </label>
                                                                        <input value="<?= $std_row[0]['L_FAX']?>" type="text" name="L_FAX" placeholder="Fax" class="form-control col-md-12 col-xs-12">
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                               <div class="row">
                                                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                                                     <div class="form-group">
                                                                        <label class="pull-left control-label col-md-12 col-sm-12 col-xs-12" for="name">Avetmiss Country
                                                                        </label>
                                                                        <select name="" class="col-md-12 col-xs-12 form-control country">
                                                                           <option value='0'>Australia</option>
                                                                        </select>
                                                                     </div>
                                                                  </div>
                                                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                                                     <div class="form-group">
                                                                        <label class="pull-left control-label col-md-12 col-sm-12 col-xs-12" for="name">Avetmiss State
                                                                        </label>
                                                                        <select  class="col-md-12 col-xs-12 form-control" name="PSTATE">
                                                                           <option value="0"<?php if ($std_row[0]['PSTATE'] == '0') { echo 'selected="selected"';}?>>--Select State--</option>
                                                                           <option value="1" <?php if($std_row[0]['PSTATE'] == 1) { echo 'selected="selected"'; } ?>>New South Wales</option>
                                                                           <option value="2" <?php if($std_row[0]['PSTATE'] == 2) { echo 'selected="selected"'; } ?>>Victoria</option>
                                                                           <option value="3" <?php if($std_row[0]['PSTATE'] == 3) { echo 'selected="selected"'; } ?>>Queensland</option>
                                                                           <option value="4" <?php if($std_row[0]['PSTATE'] == 4) { echo 'selected="selected"'; } ?>>South Australia</option>
                                                                           <option value="5" <?php if($std_row[0]['PSTATE'] == 5) { echo 'selected="selected"'; } ?>>Western Australia</option>
                                                                           <option value="6" <?php if($std_row[0]['PSTATE'] == 6) { echo 'selected="selected"'; } ?>>Tasmania</option>
                                                                           <option value="7" <?php if($std_row[0]['PSTATE'] == 7) { echo 'selected="selected"'; } ?>>Northam Territory</option>
                                                                           <option value="8" <?php if($std_row[0]['PSTATE'] == 8) { echo 'selected="selected"'; } ?>>Australian Capital Territory</option>
                                                                           <option value="9" <?php if($std_row[0]['PSTATE'] == 9) { echo 'selected="selected"'; } ?>>Other Australian Territories Or Dependencies</option>
                                                                           <option value="10" <?php if($std_row[0]['PSTATE'] == 10) { echo 'selected="selected"'; } ?>>Other(Overseas But Not An Australian Territory)</option>
                                                                        </select>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <br><br><br><br><br>
                                                   <!--<div class="col-md-6 col-sm-6 col-xs-12">
                                                      <div class="row">
                                                         <h4>
                                                            <fieldset>Permanent Address / Overseas Address</fieldset>
                                                         </h4>
                                                         <div class="col-md-12 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                               <textarea name="OADDR1" class="form-control" rows="2"><?= $std_row[0]['OADDR1']?></textarea>
                                                            </div>
                                                         </div>
                                                         <div class="col-md-12 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                               <textarea name="OADDR2" class="form-control" rows="2"><?= $std_row[0]['OADDR2']?></textarea>
                                                            </div>
                                                         </div>
                                                         <div class="col-md-12 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                               <textarea name="OADDR3" class="form-control" rows="2"><?= $std_row[0]['OADDR3']?></textarea>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div class="row">
                                                         <div class="col-md-12 col-sm-12 col-xs-12">
                                                            <div class="row">
                                                               <div class="col-md-4 col-sm-4 col-xs-12">
                                                                  <div class="form-group">
                                                                     <input type="text" placeholder="SubUrb" value="<?= $std_row[0]['OSUB']?>" name="OSUB" class="form-control col-md-12 col-xs-12">
                                                                  </div>
                                                               </div>
                                                               <div class="col-md-4 col-sm-4 col-xs-12">
                                                                  <div class="form-group">
                                                                     <input type="text" placeholder="State" value="<?= $std_row[0]['OSTATE']?>" name="OSTATE" class="form-control col-md-12 col-xs-12">
                                                                  </div>
                                                               </div>
                                                               <div class="col-md-4 col-sm-4 col-xs-12">
                                                                  <div class="form-group">
                                                                     <input type="text" placeholder="Post Code" value="<?= $std_row[0]['OPCODE']?>"  name="OPCODE" class="form-control col-md-12 col-xs-12">
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="row">
                                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                               <div class="form-group">
                                                                  <label class="pull-left control-label col-md-12 col-sm-12 col-xs-12" for="name">Phone No
                                                                  </label>
                                                                  <input type="text" value="<?= $std_row[0]['O_PH']?>" name="O_PH" placeholder="Phone No" class="form-control col-md-12 col-xs-12">
                                                               </div>
                                                            </div>
                                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                               <div class="form-group">
                                                                  <label class="pull-left control-label col-md-12 col-sm-12 col-xs-12" for="name">Fax
                                                                  </label>
                                                                  <input type="text" value="<?= $std_row[0]['O_FAX']?>" name="O_FAX" placeholder="Fax" class="form-control col-md-12 col-xs-12">
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>-->
                                                     <!-- <div class="row">
                                                         <div class="col-md-12 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                               <label class="pull-left control-label col-md-12 col-sm-12 col-xs-12" for="name">Country
                                                               </label>
                                                               <select name="COUNTRY_NO" class="col-md-12 col-xs-12 form-control">
                                                                  <option Value="0">Select Country</option>
                                                                  <?php
                                                                     $sql3="select * from country ORDER BY `COUNTRY_NAME` ASC";
                                                                     $dd_res3=mysql_query("Select *  from country ORDER BY `COUNTRY_NAME` ASC");
                                                                                                                while($r3=mysql_fetch_array($dd_res3))
                                                                                                                   { 
                                                                                                                    if($std_row[0]['COUNTRY_NO']==$r3['COUNTRY_NO'])
                                                                                                                      {
                                                                                                                        echo "<option value='$r3[COUNTRY_NO]' selected> $r3[COUNTRY_NAME] </option>";
                                                                                                                        }
                                                                                                                         else
                                                                                                                         {
                                                                                                                          echo "<option value='$r3[COUNTRY_NO]'> $r3[COUNTRY_NAME] </option>";
                                                                                                                           }
                                                                                                                     }
                                                                     ?>
                                                               </select>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>-->
                                                   <div role="tabpanel" class="tab-pane">
                                                      <div class="col-md-12 col-sm-12 col-xs-12">
                                                         <div class="table-responsive">
                                                            <h4>
                                                               <fieldset>Contact <a href="#" onclick="javascript:void window.open('dashboard.php?op=<?=MD5('display_student_contact')?>&stu_no=<?=$std_row[0]['STUD_NO']?>&cust_no=<?=$std_row[0]['CUSTOMER_NO']?>&cun_no=<?=$std_row[0]['COUNTRY_NO']?>','1470138621367','width=600,height=350,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=0,top=0');return false;">&nbsp;&nbsp;Add New Contact</a></fieldset>
                                                            </h4>
                                                            <table class="display table table-striped table-bordered" cellspacing="0" width="100%">
                                                               <thead>
                                                                  <tr class="headings">
                                                                     <th class="column-title">Name
                                                                     </th>
                                                                     <th class="column-title">Contact Info
                                                                     </th>
                                                                     <th class="column-title">Address1
                                                                     </th>
                                                                     <th class="column-title">Address2
                                                                     </th>
                                                                     <th class="column-title">City
                                                                     </th>
                                                                     <th class="column-title">State
                                                                     </th>
                                                                     <th class="column-title">Post Code
                                                                     </th>
                                                                     <th class="column-title">Phone1
                                                                     </th>
                                                                     <th class="column-title">Phone2
                                                                     </th>
                                                                     <th class="column-title">Email
                                                                     </th>
                                                                     <th class="column-title">Action
                                                                     </th>
                                                                  </tr>
                                                               </thead>
                                                               <tbody>
                                                                  <?php
                                                                     $conda="where `STUD_NO`=".$std_row[0]['STUD_NO'];
                                                                     $std_rowa=getRows($conda,'stu_contact');
                                                                     foreach($std_rowa as $std_rowb)                                                                         {
                                                                         ?>
                                                                  <tr class="even pointer">
                                                                     <td class=" "><?=$std_rowb['NAME']?></td>
                                                                     <td class=" "><?=$std_rowb['CONTACT_INFO']?></td>
                                                                     <td class=" "><?=$std_rowb['ADDRESS1']?></td>
                                                                     <td class=" "><?=$std_rowb['ADDRESS2']?></td>
                                                                     <td class=" "><?=$std_rowb['SUBURB']?></td>
                                                                     <td class=" "><?=$std_rowb['STATE']?></td>
                                                                     <td class=" "><?=$std_rowb['POSTCODE']?></td>
                                                                     <td class=" "><?=$std_rowb['PHONE1']?></td>
                                                                     <td class=" "><?=$std_rowb['PHONE2']?></td>
                                                                     <td class=" "><?=$std_rowb['EMAIL']?></td>
                                                                     <td class="last"><a href="#" onclick="javascript:void window.open('dashboard.php?op=<?=MD5('display_student_contact_edit')?>&st_con_no=<?=$std_rowb['STU_CON_NO']?>','1470138621367','width=700,height=500,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=0,top=0');return false;">Edit</a>| 
                                                                        <a href="dashboard.php?op=<?=MD5('student_edit')?>&del=<?=$std_rowb['STU_CON_NO']?>&sid=<?=$std_rowb['STUD_NO']?>" class="color-sky stting"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>Delete</a>
                                                                     </td>
                                                                  </tr>
                                                                  <?php
                                                                     }?>
                                                               </tbody>
                                                            </table>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <!--Avetmiss-->
                                                <div id="ste_avetmiss" class="tab-pane" role="tabpanel">
                                                   <div class="col-md-12 col-sm-12 col-xs-12">
                                                      <div class="row">
                                                         <h4>
                                                            <fieldset>Basic Details
                                                               
                                                            </fieldset>
                                                         </h4>
                                                         <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <div class="form-group">
                                                               <label for="name" class="pull-left control-label col-md-12 col-sm-12 col-xs-12">Unique Student Identifier
                                                               </label>
                                                               <input type="text" class="form-control col-md-12 col-xs-12" value="<?=$std_row[0]['VET_USI']?>" name="VET_USI">
                                                            </div>
                                                         </div>
                                                         <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <div class="form-group">
                                                               <label for="name" class="pull-left control-label col-md-12 col-sm-12 col-xs-12">Country Of Birth
                                                               </label>
                                                               <select name="BIRTH_COUNTRY_NAME" class="col-md-12 col-xs-12 form-control">
                                                                  <option Value="0">Select Country</option>
                                                                  <?php
                                                                     $sql30="select * from country ORDER BY `COUNTRY_NAME` ASC";
                                                                     $dd_res30=mysql_query("Select *  from country ORDER BY `COUNTRY_NAME` ASC");
                                                                                                                while($r30=mysql_fetch_array($dd_res30))
                                                                                                                   { 
                                                                                                                    if($std_row[0]['BIRTH_COUNTRY_NAME']==$r30['COUNTRY_NO'])
                                                                                                                      {
                                                                                                                        echo "<option value='$r30[COUNTRY_NO]' selected> $r30[COUNTRY_NAME] </option>";
                                                                                                                        }
                                                                                                                         else
                                                                                                                         {
                                                                                                                          echo "<option value='$r30[COUNTRY_NO]'> $r30[COUNTRY_NAME] </option>";
                                                                                                                           }
                                                                                                                     }
                                                                     ?>
                                                               </select>
                                                            </div>
                                                         </div>
                                                         <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <div class="form-group">
                                                               <label for="name" class="pull-left control-label col-md-12 col-sm-12 col-xs-12">Labour Force Status
                                                               </label>
                                                               <select name="VET_EMPLOYMENT_STATUS_NO" class="col-md-12 col-xs-12 form-control">
                                                                  <option Value="0"></option>
                                                                  <?php
                                                                     $sql301="select * from system_code where `SYSTEM_TYPE_NO`=".'204';
                                                                     $dd_res301=mysql_query("select * from system_code where `SYSTEM_TYPE_NO`=".'204');
                                                                     while($r301=mysql_fetch_array($dd_res301))
                                                                     { 
                                                                     if($std_row[0]['VET_EMPLOYMENT_STATUS_NO']==$r301['SYSTEM_CODE_NO'])
                                                                      {
                                                                        echo "<option value='$r301[SYSTEM_CODE_NO]' selected> $r301[NAME] </option>";
                                                                        }
                                                                         else
                                                                         {
                                                                          echo "<option value='$r301[SYSTEM_CODE_NO]'> $r301[NAME] </option>";
                                                                           }
                                                                     }
                                                                     ?>
                                                               </select>
                                                            </div>
                                                         </div>
                                                         <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <div class="form-group">
                                                               <label for="name" class="pull-left control-label col-md-12 col-sm-12 col-xs-12">Proficiency in Spoken English
                                                               </label>
                                                               <select name="VET_ENGLISH_LEVEL_NO" class="col-md-12 col-xs-12 form-control">
                                                                  <option Value="0"></option>
                                                                  <?php
                                                                     $sql302="select * from system_code where `SYSTEM_TYPE_NO`=".'206';
                                                                     $dd_res302=mysql_query("select * from system_code where `SYSTEM_TYPE_NO`=".'206');
                                                                                                                while($r302=mysql_fetch_array($dd_res302))
                                                                                                                   { 
                                                                                                                    if($std_row[0]['VET_ENGLISH_LEVEL_NO']==$r302['SYSTEM_CODE_NO'])
                                                                                                                      {
                                                                                                                        echo "<option value='$r302[SYSTEM_CODE_NO]' selected> $r302[NAME] </option>";
                                                                                                                        }
                                                                                                                         else
                                                                                                                         {
                                                                                                                          echo "<option value='$r302[SYSTEM_CODE_NO]'> $r302[NAME] </option>";
                                                                                                                           }
                                                                                                                     }
                                                                     ?>
                                                               </select>
                                                            </div>
                                                         </div>
                                                         <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <div class="form-group">
                                                               <label for="name" class="pull-left control-label col-md-12 col-sm-12 col-xs-12">Indigenous Status
                                                               </label>
                                                               <select name="VET_INDIGENOUS_NO" class="col-md-12 col-xs-12 form-control">
                                                                  <option Value="0"></option>
                                                                  <?php
                                                                     $sql303="select * from system_code where `SYSTEM_TYPE_NO`=".'203';
                                                                     $dd_res303=mysql_query("select * from system_code where `SYSTEM_TYPE_NO`=".'203');
                                                                                                                while($r303=mysql_fetch_array($dd_res303))
                                                                                                                   { 
                                                                                                                    if($std_row[0]['VET_INDIGENOUS_NO']==$r303['SYSTEM_CODE_NO'])
                                                                                                                      {
                                                                                                                        echo "<option value='$r303[SYSTEM_CODE_NO]' selected> $r303[NAME] </option>";
                                                                                                                        }
                                                                                                                         else
                                                                                                                         {
                                                                                                                          echo "<option value='$r303[SYSTEM_CODE_NO]'> $r303[NAME] </option>";
                                                                                                                           }
                                                                                                                     }
                                                                     ?>
                                                               </select>
                                                            </div>
                                                         </div>
                                                         <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <div class="form-group">
                                                               <label for="name" class="pull-left control-label col-md-12 col-sm-12 col-xs-12">Disabilities
                                                               </label>
                                                               <select name="langOpt1[]" multiple class="langOpt">
                                                               <?php
                                                                  $sql305="select * from system_code where `SYSTEM_TYPE_NO`=".'201';
                                                                  $dd_res305=mysql_query("select * from system_code where `SYSTEM_TYPE_NO`=".'201');
                                                                  $Type_exploded = explode(",", $std_row[0]['VETFEE_DISABILITYSUPPORT']);
                                                                                                             while($r305=mysql_fetch_array($dd_res305))
                                                                                                                {
                                                                    
                                                                   
                                                                                                                    ?>
                                                                   <option value="<?php echo $r305['SYSTEM_CODE_NO']; ?>" <?php if(in_array($r305['SYSTEM_CODE_NO'],$Type_exploded)==true) { ?> selected="selected" <?php } ?>><?php echo $r305['NAME']; ?></option>
                                                                                                                <?php }
                                                                                                                    
                                                                  //}
                                                                                                                  
                                                                  ?>
                                                               </select>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div class="row">
                                                         <div class="col-md-12 col-sm-12">
                                                            <div class="row">
                                                               <h4>
                                                                  <fieldset>Address</fieldset>
                                                               </h4>
                                                               <div class="col-md-6 col-sm-6 col-xs-12">
                                                                  <div class="form-group">
                                                                     <label for="name" class="pull-left control-label col-md-12 col-sm-12 col-xs-12">Building/Property Name
                                                                     </label>
                                                                     <input type="text" value="<?= $std_row[0]['LADDR_PROPERTY']?>" class="form-control col-md-12 col-xs-12" name="LADDR_PROPERTY" id="std_property_name">
                                                                  </div>
                                                               </div>
                                                               <div class="col-md-6 col-sm-6 col-xs-12">
                                                                  <div class="form-group">
                                                                     <label for="name" class="pull-left control-label col-md-12 col-sm-12 col-xs-12">Postal Delivery Box
                                                                     </label>
                                                                     <input type="text" value="<?= $std_row[0]['LADDR_SUBURB_NO']?>" class="form-control col-md-12 col-xs-12" name="LADDR_SUBURB_NO" id="std_postal_box">
                                                                  </div>
                                                               </div>
                                                               <div class="col-md-6 col-sm-6 col-xs-12">
                                                                  <div class="form-group">
                                                                     <label for="name" class="pull-left control-label col-md-12 col-sm-12 col-xs-12">Flat/Unit Details
                                                                     </label>
                                                                     <input type="text" value="<?= $std_row[0]['LADDR_UNITNO']?>" class="form-control col-md-12 col-xs-12" name="LADDR_UNITNO" id="std_flat_dtls">
                                                                  </div>
                                                               </div>
                                                               <div class="col-md-6 col-sm-6 col-xs-12">
                                                                  <div class="form-group">
                                                                     <label for="name" class="pull-left control-label col-md-12 col-sm-12 col-xs-12">Street Name
                                                                     </label>
                                                                     <input type="text" value="<?= $std_row[0]['LADDR_STREETNAME']?>" class="form-control col-md-12 col-xs-12" name="LADDR_STREETNAME" id="std_street_name">
                                                                  </div>
                                                               </div>
                                                               <div class="col-md-6 col-sm-6 col-xs-12">
                                                                  <div class="form-group">
                                                                     <label for="name" class="pull-left control-label col-md-12 col-sm-12 col-xs-12">Street Number
                                                                     </label>
                                                                     <input type="text" value="<?= $std_row[0]['LADDR_STREETNO']?>" class="form-control col-md-12 col-xs-12" name="LADDR_STREETNO" id="std_street_number">
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="col-md-12 col-sm-12 col-xs-12">
                                                      <div class="row">
                                                         <h4>
                                                            <fieldset>Education</fieldset>
                                                         </h4>
                                                         <div class="col-md-12 col-sm-12">
                                                            <div class="row">
                                                               <div class="col-md-9 col-sm-9 col-xs-9">
                                                                  <div class="form-group">
                                                                     <label for="name" class="pull-left control-label col-md-12 col-sm-12 col-xs-12">Highest Level of School Completed
                                                                     </label>
                                                                     <select name="VET_SCHOOL_LEVEL_NO" class="col-md-12 col-xs-12 form-control">
                                                                        <option Value="0"></option>
                                                                        <?php
                                                                           $sql304="select * from system_code where `SYSTEM_TYPE_NO`=".'202';
                                                                           $dd_res304=mysql_query("select * from system_code where `SYSTEM_TYPE_NO`=".'202');
                                                                                                                      while($r304=mysql_fetch_array($dd_res304))
                                                                                                                         { 
                                                                                                                          if($std_row[0]['VET_SCHOOL_LEVEL_NO']==$r304['SYSTEM_CODE_NO'])
                                                                                                                            {
                                                                                                                              echo "<option value='$r304[SYSTEM_CODE_NO]' selected> $r304[NAME] </option>";
                                                                                                                              }
                                                                                                                               else
                                                                                                                               {
                                                                                                                                echo "<option value='$r304[SYSTEM_CODE_NO]'> $r304[NAME] </option>";
                                                                                                                                 }
                                                                                                                           }
                                                                           ?>
                                                                     </select>
                                                                  </div>
                                                               </div>
                                                               <div class="col-md-3 col-sm-3 col-xs-3">
                                                                  <div class="form-group">
                                                                     <label for="name" class="pull-left control-label col-md-12 col-sm-12 col-xs-12">Year
                                                                     </label>
                                                                     <input type="text" class="form-control col-md-12 col-xs-12" value="<?=$std_row[0]['VET_SCHOOL_YEAR']?>" name="VET_SCHOOL_YEAR" id="std_postal_box">
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="col-md-12 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                               <div class="checkbox">
                                                                  <label class="pull-left control-label">
                                                                  <input type="hidden" name="VET_SCHOOL_FLAG" id="course_publish" value="0" style="width:20px;height:20px;">
                                                                  <input type="checkbox" style="width:20px;height:20px;" name="VET_SCHOOL_FLAG" value="1" <?php echo ($std_row[0]['VET_SCHOOL_FLAG'] ? 'checked' : '');?>/>Currently attending secondary school
                                                                  </label>
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="col-md-12 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                               <label for="name" class="pull-left control-label col-md-12 col-sm-12 col-xs-12">Prior Educational Achievement
                                                               </label>
                                                               <select name="langOpt[]" multiple class="langOpt">
                                                               <?php
                                                                  $sql3050="select * from system_code where `SYSTEM_TYPE_NO`=".'205';
                                                                  $dd_res3050=mysql_query("select * from system_code where `SYSTEM_TYPE_NO`=".'205');
                                                                  $Type_exploded2 = explode(",", $std_row[0]['VET_EDUCATION_LEVEL_NO']);
                                                                                                             while($r3050=mysql_fetch_array($dd_res3050))
                                                                                                                {
                                                                                                                 ?>
                                                                   
                                                                   <option value="<?=$r3050['SYSTEM_CODE_NO']?>" <?php if(in_array($r3050['SYSTEM_CODE_NO'],$Type_exploded2)==true) { ?> selected="selected" <?php } ?>><?=$r3050['NAME']?></option>
                                                                     <?php                                               
                                                                 
                                                                                                                  }
                                                                  ?>
                                                               </select>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <!--enrolments-->
                                                <div role="tabpanel" class="tab-pane" id="ste_enrolments">
                                                   <h4>
                                                      <fieldset>Enrolments
                                                         <a class="btn btn-success" href="">New Offer</a>
                                                      </fieldset>
                                                   </h4>
                                                   <div class="table-responsive">
                                                      <table class="table data-tbl-tools dataTable">
                                                         <thead>
                                                            <tr class="headings">
                                                               <th class="column-title">Enrol No
                                                               </th>
                                                               <th class="column-title">Arrived
                                                               </th>
                                                               <th class="column-title">Course Code
                                                               </th>
                                                               <th class="column-title">Course Name
                                                               </th>
                                                               <th class="column-title">Faculty
                                                               </th>
                                                               <th class="column-title">Location
                                                               </th>
                                                               <th class="column-title">Start
                                                               </th>
                                                               <th class="column-title">End
                                                               </th>
                                                               <th class="column-title">Status
                                                               </th>
                                                               <th class="column-title">Agent Name
                                                               </th>
                                                               <th class="column-title">Course Length
                                                               </th>
                                                              
                                                               <th class="column-title">Outstanding Fees
                                                               </th>
                                                               <th class="column-title">Action
                                                               </th>
                                                            </tr>
                                                         </thead>
                                                         <tbody>
                                                            <?php
                                                               $cond10="where `STUD_NO`=".$std_row[0]['STUD_NO'];
                                                               $std_row10=getRows($cond10,'enrol');
                                                               foreach($std_row10 as $std_row11)
                                                               {
                                                               
                                                                ?>
                                                            <tr class="even pointer">
                                                               <td class=" "><?=$std_row11['ENROL_NO']?></td>
                                                               <td class=" "><input type="hidden" name="CLASS_1_YN" id="course_publish" value="0" >
                                                                   <input disabled type="checkbox" name="CLASS_2_YN" value="1" <?php echo ($std_row11['ARVD'] ? 'checked' : '');?>/>
                                                               </td>
                                                               <?php
                                                                  if($std_row11['COURSE_NO'])
                                                                  {
                                                                  $cond12="where `COURSE_NO`=".$std_row11['COURSE_NO'];
                                                                  $std_row12=getRows($cond12,'course');
                                                                  }
                                                                  ?>
                                                               <td class=" "><?= $std_row12[0]['CODE']?></td>
                                                               <td class=" "><?= $std_row12[0]['COURSE_NAME']?></td>
                                                               <?php
                                                                  $cond13="where `CRT_NO`=".$std_row12[0]['CRT_NO'];
                                                                  $std_row13=getRows($cond13,'crse_type');
                                                                   ?>
                                                               <td class=" "><?= $std_row13[0]['CRT_NAME']?></td>
                                                               <td class=" "><?php echo "SYDNEY";?></td>
                                                               <?php
                                                                  $cond13a="where `OFFER_ITEM_NO`=".$std_row11['OFFER_ITEM_NO'];
                                                                  $std_row13a=getRows($cond13a,'offer_item');
                                                                   ?>
                                                               <td class=" "><?php echo date('d/m/Y',strtotime($std_row13a[0]['FROM_DATE']));?></td>
                                                               <td class=" "><?php echo date('d/m/Y',strtotime($std_row13a[0]['TO_DATE']));?></td>
                                                               <?php $date=date('Y-m-d'); ?>
                                                               <td class=" "><?=check_in_range($std_row11['ST_DATE'], $std_row11['END_DATE'], $date);?></td>
                                                               <?php 
                                                                  $cond16="where `AGENT_NO`=".$std_row11['AGENT_NO'];
                                                                  $std_row16=getRows($cond16,'agent');?>
                                                               <td class=" "><?=$std_row16[0]['AGENT_NAME']?></td>
                                                              
                                                               <td class=" "><?= $std_row12[0]['COURSE_LEN']?>&nbsp;Weeks</td>
                                                              
                                                               <?php
                                                                  $query1=mysql_query("SELECT SUM(AMOUNT) from `invoice_details` where `ENROL_NO`=".$std_row11['ENROL_NO']);
                                                                   $row1=mysql_fetch_array($query1);
                                                                   $invoice_amount=$row1['SUM(AMOUNT)'];
                                                                  
                                                                  $query2=mysql_query("SELECT SUM(AMOUNT) from `credit` where `ENROL_NO`=".$std_row11['ENROL_NO']);
                                                                   $row2=mysql_fetch_array($query2);
                                                                   $credit_amount=$row2['SUM(AMOUNT)'];
                                                                  
                                                                  $query3=mysql_query("SELECT SUM(AMOUNT) from `debit_item` where `ENROL_NO`=".$std_row11['ENROL_NO']);
                                                                   $row3=mysql_fetch_array($query1);
                                                                   $debit_amount=$row3['SUM(AMOUNT)'];
                                                                  
                                                                     
                                                                     /*$result = mysql_query("SELECT SUM(AMOUNT) AS value_sum FROM due_history where ENROL_NO=".$std_row11['ENROL_NO']." and `DELETE_DATE`=null"); 
                                                                     $rowa = mysql_fetch_assoc($result); 
                                                                     $sum = $rowa['value_sum']; */
                                                                     
                                                                                                                             //$std_row003=getRows($cond003,'due');
                                                                             ?>
                                                               <td class=" ">$<?php echo $a = number_format(($invoice_amount-$credit_amount)+$debit_amount);?></td>
                                                               <td class="last"><a href="dashboard.php?op=<?=MD5('enrolment_details')?>&eid=<?=$std_row11['ENROL_NO']?>" class="color-sky stting"><i class="fa fa-cog" aria-hidden="true"></i>Edit</a></td>
                                                            </tr>
                                                            <?php
                                                               } 
                                                                       
                                                               ?>
                                                         </tbody>
                                                      </table>
                                                   </div>
                                                </div>
                                                <!--Offer-->
                                                <div role="tabpanel" class="tab-pane" id="ste_offer">
                                                   <h4>
                                                      <fieldset>Offer
                                                         <a class="btn btn-success" href="dashboard.php?op=<?=MD5('offer')?>&stu_no=<?=$std_row[0]['EXT_STN']?>&sname=<?php echo $name;?>&cust_no=<?=$std_row[0]['CUSTOMER_NO']?>&cun_no=<?=$std_row[0]['COUNTRY_NO']?>">New Offer</a>
                                                      </fieldset>
                                                   </h4>
                                                   <div class="table-responsive">
                                                      <table class="table data-tbl-tools dataTable">
                                                         <thead>
                                                            <tr class="headings">
                                                               <th class="column-title">Offer No
                                                               </th>
                                                               <th class="column-title">Offer Date
                                                               </th>
                                                               <th class="column-title">Status
                                                               </th>
                                                               <th class="column-title">Agent
                                                               </th>
                                                               <th class="column-title">User
                                                               </th>
                                                               <th class="column-title">When
                                                               </th>
                                                               <th class="column-title">Action
                                                               </th>
                                                            </tr>
                                                         </thead>
                                                         <tbody>
                                                            <?php
                                                               $cond20="where `CUSTOMER_NO`=".$std_row[0]['CUSTOMER_NO'];
                                                               $std_row20=getRows($cond20,'offer');
                                                               foreach($std_row20 as $std_row21)
                                                               {
                                                               
                                                                ?>
                                                            <tr class="even pointer">
                                                               <td class=" "><?=$std_row21['OFFER_NO']?></td>
                                                               <td class=" "><?=$std_row21['OFFER_DATE']?></td>
                                                               <?php
                                                                  if($std_row21['STATUS_NO']=='1')
                                                                   {
                                                                    $a="Preparing";
                                                                   }
                                                                   else if($std_row21['STATUS_NO']=='2')
                                                                   {
                                                                    $a="Offer Made";
                                                                   }
                                                                   else if($std_row21['STATUS_NO']=='3')
                                                                   {
                                                                    $a="Accepted";
                                                                   }
                                                                   else if($std_row21['STATUS_NO']=='4')
                                                                   {
                                                                    $a="Declined";
                                                                   }
                                                                   else if($std_row21['STATUS_NO']=='5')
                                                                   {
                                                                    $a="Withdrawn";
                                                                   }
                                                                   ?>
                                                               <td class=" "><?=$a?></td>
                                                               <?php
                                                                  $cond_ag="where `AGENT_NO`=".$std_row21['REF_CUSTOMER_NO'];
                                                                  $ag_data=getRows($cond_ag,'agent');
                                                                  ?>
                                                               <td class=" "><?=$ag_data[0]['AGENT_NAME']?></td>
                                                               <?php 
                                                                  if($std_row21['USER_NO'])
                                                                  {
                                                                  $cond23="where `USER_NO`=".$std_row21['USER_NO']; 
                                                                  $cou12=getRows($cond23,'users');
                                                                  }?>
                                                               <td class=" "><?=$cou12[0]['INIT']?></td>
                                                               <td class=" "><?=$std_row21['CREATE_DATE']?></td>
                                                               <td class="last"><a href="dashboard.php?op=<?=MD5('offer_details')?>&oid=<?= $std_row21['OFFER_NO'] ?>&cust_no=<?= $std_row21['CUSTOMER_NO'] ?>" class="color-sky stting"><i class="fa fa-cog" aria-hidden="true"></i>Edit</a>
                                                                  ||<a href="dashboard.php?op=<?=MD5('student_edit')?>&offer_del=<?=$std_row21['OFFER_NO']?>&sid=<?=$std_row[0]['STUD_NO']?>" class="color-sky stting"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>Delete</a> 
                                                               </td>
                                                            </tr>
                                                            <?php
                                                               } 
                                                                ?>
                                                         </tbody>
                                                      </table>
                                                   </div>
                                                </div>
                                                <!--diary-->
                                                <div role="tabpanel" class="tab-pane" id="ste_diary">
                                                   <h4>
                                                      <fieldset>Diary
                
                                                         <a class="btn btn-danger" href="#" onclick="javascript:void window.open('dashboard_popup.php?op=<?=MD5('display_student_diary')?>&s_no=<?=$std_row[0]['STUD_NO']?>&s_fname=<?=$std_row[0]['FNAME']?>&s_lname=<?=$std_row[0]['LNAME']?>&s_mname=<?=$std_row[0]['MNAME']?>&ag_name=<?=$std_row[0]['AGENT_NAME']?>&cus_no=<?=$std_row[0]['CUSTOMER_NO']?>','1470138621367','width=700,height=500,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=0,top=0');return false;">&nbsp;&nbsp;&nbsp;&nbsp;Add New Diary</a>
                                                      </fieldset>
                                                   </h4>
                                                   <div class="table-responsive">
                                                      <table class="table data-tbl-tools dataTable">
                                                         <thead>
                                                            <tr class="headings">
                                                               <th class="column-title">Attention
                                                               </th>
                                                               <th class="column-title">Date 
                                                               </th>
                                                               <th class="column-title">Category
                                                               </th>
                                                               <th class="column-title">Subjects
                                                               </th>
                                                               <th class="column-title">User
                                                               </th>
                                                               <th class="column-title">When
                                                               </th>
                                                               <th class="column-title">Detail
                                                               </th>
                                                               <th class="column-title">Action
                                                               </th>
                                                            </tr>
                                                         </thead>
                                                         <tbody>
                                                            <?php
                                                               $con1="where `CUSTOMER_NO`=".$std_row[0]['CUSTOMER_NO'];
                                                               $std_r1=getRows($con1,'diary');
                                                               foreach($std_r1 as $std_r2)
                                                               {
                                                               $con2="where `LOOKUP_CODE_NO`=".$std_r2['CATEGORY_NO'];
                                                               $std_r3=getRows($con2,'lookup_code');
                                                               foreach($std_r3 as $std_r4)
                                                               {
                                                               $con3="where `USER_NO`=".$std_r2['USER_NO'];
                                                               $std_r5=getRows($con3,'users');
                                                               foreach($std_r5 as $std_r6)
                                                               {
                                                                ?>
                                                            <tr class="even pointer">
                                                               <td class=" "><?=($std_r2['ATTENTION'])? '<input type="checkbox" checked disabled />':'<input disabled type="checkbox"  />' ?></td>
                                                               <?php
                                                                  $originalDate5 = $std_r2['DIARY_DATE'];
                                                                  $newDate5 = date("d/m/y", strtotime($originalDate5)); ?>  
                                                               <td class=" "><?php echo $newDate5 ?></td>
                                                               <td class=" "><?=$std_r4['NAME']?></td>
                                                               <td class=" "><?=$std_r2['SUMMARY']?></td>
                                                               <td class=" "><?=$std_r6['INIT']?></td>
                                                               <td class=" "><?=$std_r2['WHO_DATE']?></td>
                                                               <td class=" "><?=$std_r2['NOTES']?></td>
                                                               <td><a href="#" onclick="javascript:void window.open('dashboard_popup.php?op=<?=MD5('display_student_diary_edit')?>&d_no=<?=$std_r2['DIARY_NO']?>&s_no=<?=$std_row[0]['EXT_STN']?>&s_fname=<?=$std_row[0]['FNAME']?>&s_lname=<?=$std_row[0]['LNAME']?>&s_mname=<?=$std_row6[0]['MNAME']?>&ag_name=<?=$std_row[0]['AGENT_NAME']?>','1470138621367','width=800,height=500,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=0,top=0');return false;">Edit</a>|<a href="#" onclick="javascript: var p = confirm('Are you sure you want to delete?'); if(p==true) { window.location.href='dashboard.php?op=<?=MD5('student_edit')?>&del2=<?=$std_r2['DIARY_NO']?>&sid=<?=$std_row[0]['STUD_NO']?>'; } else { return false; }" class="color-sky stting"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>Delete</a></td>
                                                            </tr>
                                                            <?php
                                                               }}}
                                                                ?>
                                                         </tbody>
                                                      </table>
                                                   </div>
                                                </div>
                                               
                                                <!--visa-->
                                                <div role="tabpanel" class="tab-pane" id="ste_visa">
                                                   <h4>
                                                      <fieldset>Visa Details
                                                      
                                                       <a class="btn btn-danger" href="#" onclick="javascript:void window.open('dashboard_popup.php?op=<?=MD5('new_visa')?>&stu_no=<?=$std_row[0]['STUD_NO']?>&sname=<?=$std_row[0]['LNAME']?>&cust_no=<?=$std_row[0]['CUSTOMER_NO']?>&cun_no=<?=$std_row[0]['COUNTRY_NO']?>','1470138621367','width=700,height=500,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=0,top=0');return false;">&nbsp;&nbsp;&nbsp;&nbsp;New Visa</a>
                                                   </fieldset></h4>
                                                   <div class="table-responsive">
                                                      <table class="table data-tbl-tools dataTable">
                                                         <thead>
                                                            <tr class="headings">
                                                               <th class="column-title">Visa No
                                                               </th>
                                                               <th class="column-title">Visa Type
                                                               </th>
                                                               <th class="column-title">Arrive
                                                               </th>
                                                               <th class="column-title">Expiry
                                                               </th>
                                                               <th class="column-title">User
                                                               </th>
                                                               <th class="column-title">When
                                                               </th>
                                                               <th class="column-title">Action
                                                               </th>
                                                            </tr>
                                                         </thead>
                                                         <tbody>
                                                            <?php
                                                               if($std_row[0]['CUSTOMER_NO']){
                                                                $coa="where `CUSTOMER_NO`=".$std_row[0]['CUSTOMER_NO'];
                                                                 $sta=getRows($coa,'customer_visa');
                                                                 foreach($sta as $row1)
                                                                    {
                                                                               ?>
                                                            <tr class="even pointer">
                                                               <td class=" "><?=$row1['CODE']?></td>
                                                               <?php 
                                                                  $conda1a="where `LOOKUP_CODE_NO`=".$row1['TYPE_NO'];
                                                                  $std_rowa1a=getRows($conda1a,'lookup_code');
                                                                  ?>
                                                               <td class=" "><?=$std_rowa1a[0]['NAME']?></td>
                                                               <td class=" "><?=$row1['FROM_DATE']?></td>
                                                               <td class=" "><?=$row1['TO_DATE']?></td>
                                                               <?php $co11aa="where `USER_NO`=".$row1['USER_NO'];
                                                                  $std11aa=getRows($co11aa,'users');
                                                                  ?>
                                                               <td class=" "><?=$std11aa[0]['INIT']?></td>
                                                               <td class=" "><?=$row1['CREATE_DATE']?></td>
                                                               <td class=" "><a href="#" onclick="javascript:void window.open('dashboard_popup.php?op=<?=MD5('visa_edit')?>&CUSTOMER_VISA_NO=<?=$row1['CUSTOMER_VISA_NO']?>&stu_no=<?=$std_row[0]['STUD_NO']?>&sname=<?=$std_row[0]['LNAME']?>&cust_no=<?=$std_row[0]['CUSTOMER_NO']?>&cun_no=<?=$std_row[0]['COUNTRY_NO']?>','11587412200067','width=550,height=350,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=0,top=0');return false;">Modify</a>|<a href="#" onclick="javascript: var p = confirm('Are you sure you want to delete?'); if(p==true) { window.location.href='dashboard.php?op=<?=MD5('student_edit')?>&del123=<?=$row1['CUSTOMER_VISA_NO']?>&sid=<?=$std_row[0]['STUD_NO']?>'; } else { return false; }" class="color-sky stting"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>Delete</a></td>
                                                            </tr>
                                                            <?php
                                                               }    }                                                          ?>
                                                         </tbody>
                                                      </table>
                                                      <div role="tabpanel" class="tab-pane">
                                                         <div class="table-responsive">
                                                            <h4>
                                                               <fieldset>eCoE Details
                                                                  <a href="#" onclick="javascript:void window.open('dashboard.php?op=<?=MD5('ecoe')?>&stu_no=<?=$std_row[0]['STUD_NO']?>&sname=<?=$std_row[0]['LNAME']?>&cust_no=<?=$std_row[0]['CUSTOMER_NO']?>&cun_no=<?=$std_row[0]['COUNTRY_NO']?>','1470100000067','width=700,height=500,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=0,top=0');return false;">&nbsp;&nbsp;&nbsp;&nbsp;New eCoE</a>
                                                               </fieldset>
                                                            </h4>
                                                            <table class="table data-tbl-tools dataTable">
                                                               <thead>
                                                                  <tr class="headings">
                                                                     <th class="column-title">eCoE Number
                                                                     </th>
                                                                     <th class="column-title">Status
                                                                     </th>
                                                                     <th class="column-title">From
                                                                     </th>
                                                                     <th class="column-title">To
                                                                     </th>
                                                                     <th class="column-title">Current Att
                                                                     </th>
                                                                     <th class="column-title">Overall Att
                                                                     </th>
                                                                     <th class="column-title">Action
                                                                     </th>
                                                                  </tr>
                                                               </thead>
                                                               <tbody>
                                                                  <?php
                                                                     $conda1="where `STUD_NO`=".$std_row[0]['STUD_NO'];
                                                                     $std_rowa1=getRows($conda1,'attend_tracking');
                                                                     foreach($std_rowa1 as $std_rowb1)                                                                           {
                                                                         
                                                                         ?>
                                                                  <tr class="even pointer">
                                                                     <td class=" "><?=$std_rowb1['CODE']?></td>
                                                                     <td class=" "><?=($std_rowb1['STATUS_NO']=='1') ? 'Active' : 'Cancelled'?></td>
                                                                     <td class=" "><?=$std_rowb1['FROM_DATE']?></td>
                                                                     <td class=" "><?=$std_rowb1['TO_DATE']?></td>
                                                                     <td class=" "><?=$std_rowb1['CURRENT_PERCENT']?>%</td>
                                                                     <td class=" "><?=$std_rowb1['OVERALL_PERCENT']?>%</td>
                                                                     <td class="last"><a href="#" onclick="javascript:void window.open('dashboard.php?op=<?=MD5('ecoe_edit')?>&stu_no=<?=$std_row[0]['STUD_NO']?>&sname=<?=$std_row[0]['LNAME']?>&cust_no=<?=$std_row[0]['CUSTOMER_NO']?>&ecoe=<?=$std_rowb1['ATTEND_TRACKING_NO']?>','1470101234067','width=700,height=500,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=0,top=0');return false;">&nbsp;&nbsp;&nbsp;&nbsp;Edit</a>| 
                                                                         <a href="#" onclick="javascript: var p = confirm('Are you sure you want to delete?'); if(p==true) { window.location.href='dashboard.php?op=<?=MD5('student_edit')?>&del1234=<?=$std_rowb1['ATTEND_TRACKING_NO']?>&sid=<?=$std_rowb1['STUD_NO']?>'; } else { return false; }" class="color-sky stting"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>Delete</a>
                                                                     </td>
                                                                  </tr>
                                                                  <?php
                                                                     } ?>
                                                               </tbody>
                                                            </table>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   .
                                                </div>
                                                
                                                <!--insurance-->
                                                <div role="tabpanel" class="tab-pane" id="ste_insurance">
                                                   <h4>
                                                      <fieldset>Insurance
                                                      </fieldset>
                                                   </h4>
                                                   <div class="table-responsive stu-insurance">
                                                      <table id="example7" class="display table table-striped table-bordered" cellspacing="0" width="100%">
                                                         <thead>
                                                            <tr class="headings">
                                                               <th class="column-title">Product
                                                               </th>
                                                               <th class="column-title">From
                                                               </th>
                                                               <th class="column-title">To
                                                               </th>
                                                               <th class="column-title">User
                                                               </th>
                                                               <th class="column-title">When
                                                               </th>
                                                               <th class="column-title">Notes
                                                               </th>
                                                               </th>
                                                            </tr>
                                                         </thead>
                                                         <tbody>
                                                            <tr class="even pointer">
                                                               <?php 
                                                                  $ins="where `CUSTOMER_NO`=".$std_row[0]['CUSTOMER_NO'];
                                                                  $std_ins=getRows($ins,'customer_insurance');
                                                                  foreach($std_ins as $std_ins1)  
                                                                  {
                                                                      ?>
                                                               <td class=" "><?=$std_ins1['DESCRIPTION']?></td>
                                                               <td class=" "><?=$std_ins1['FROM_DATE']?></td>
                                                               <td class=" "><?=$std_ins1['TO_DATE']?></td>
                                                               <td class=" "><?=$std_ins1['CREATE_USER_NO']?></td>
                                                               <td class=" "><?=$std_ins1['CREATE_DATE']?></td>
                                                               <td class=" "><?=$std_ins1['NOTES']?></td>
                                                            </tr>
                                                            <?php } ?>
                                                         </tbody>
                                                      </table>
                                                   </div>
                                                </div>
                                                <!--surveys-->
                                                <div role="tabpanel" class="tab-pane" id="ste_surveys">
                                                   <div class="table-responsive">
                                                      <table id="example8" class="display table table-striped table-bordered" cellspacing="0" width="100%">
                                                         <thead>
                                                            <tr class="headings">
                                                               <th class="column-title">
                                                               </th>
                                                               <th class="column-title">
                                                               </th>
                                                               <th class="column-title">
                                                               </th>
                                                               <th class="column-title">
                                                               </th>
                                                               <th class="column-title">
                                                               </th>
                                                               <th class="column-title">
                                                               </th>
                                                               </th>
                                                            </tr>
                                                         </thead>
                                                         <tbody>
                                                            <tr class="even pointer">
                                                               <td class=" "></td>
                                                               <td class=" "></td>
                                                               <td class=" "></td>
                                                               <td class=" "></td>
                                                               <td class=" "></td>
                                                               <td class=" "></td>
                                                            </tr>
                                                            <tr class="odd pointer">
                                                               <td class=" "></td>
                                                               <td class=" "></td>
                                                               <td class=" "></td>
                                                               <td class=" "></td>
                                                               <td class=" "></td>
                                                               <td class=" "></td>
                                                            </tr>
                                                         </tbody>
                                                      </table>
                                                   </div>
                                                </div>
                                                <!--photo-->
                                                <div role="tabpanel" class="tab-pane" id="ste_photo">
                                                   <div class="table-responsive">
                                                      <?php 
                                                         $s=$std_row[0]['PHOTO'];
                                                         if($s!='') {
                                                         echo '<img src="'.$s.'" alt="HTML5 Icon" style="width:150px;height:150px;vertical-align: middle" >';
                                                         }
                                                         else
                                                         {
                                                            echo '<img src="images/avatar.png" alt="HTML5 Icon" style="width:150px;height:150px;vertical-align: middle" border="0" >'; 
                                                         }
?>
                                                         
                                                                                    <div id="upload">
                                                                                        <input type="file" name="file" id="file"/>
                                                                                    </div>
                                                                                    <br/>
                                                                                    <div id="detail">
                                                                                        <b>Note:</b><br/>
                                                                                        <ul>
                                                                                            <li>To Choose file Click on folder.</li>
                                                                                            
                                                                                        </ul>
                                                                                    </div>
                                                                                </div>
                                                </div>
                                                <!--Workflow-->
                                                <div role="tabpanel" class="tab-pane" id="ste_workflow">
                                                   <div class="table-responsive">
                                                      <div class="col-md-12 col-sm-12 col-xs-12">
                                                         <div class="x_panel recent-app">
                                                            <div class="x_title">
                                                               <h2>Recent Student list</h2>
                                                            </div>
                                                            <div class="x_content">
                                                               <div class="table-responsive workflow-page-section">
                                                                  <table class="table data-tbl-tools" style="width:100%;">
                                                                     <thead>
                                                                        <tr class="headings">
                                                                           <th class="column-title">Application Type:</th>
                                                                           <th class="column-title">Student No</th>
                                                                           <th class="column-title">Student Name</th>
                                                                           <th class="column-title">Student Email</th>
                                                                           <th class="column-title">Birth Date</th>
                                                                           <th class="column-title">Country Name</th>
                                                                           <th class="column-title">Phone No</th>
                                                                           <th class="column-title">Status</th>
                                                                           <th class="column-title no-link last"><span class="nobr">Action</span>
                                                                           </th>
                                                                        </tr>
                                                                     </thead>
                                                                     <tbody>
                                                                        <?php 
                                                                           //Display student details in workflow.
                                                                           if($std_row[0]['STUD_NO'])
                                                                           {
                                                                               $a="where `student_id`=".$std_row[0]['STUD_NO'];
                                                                               $a1= getRows($a,'workflow');
                                                                                                                 foreach($a1 as $row_wf)
                                                                                                                 {
                                                                                                             ?>   
                                                                        <tr class="even pointer">
                                                                           <?php $app="where `LOOKUP_CODE_NO`=".$row_wf[application_type];
                                                                              $app_type= getRows($app,'lookup_code'); 
                                                                              $app1="where `LOOKUP_TYPE_NO`=".$app_type[0][LOOKUP_TYPE_NO];
                                                                              $app_type1= getRows($app1,'lookup_type');?>
                                                                           <td class=" "><?=$app_type[0]['NAME']?>(<?=$app_type1[0]['TABLE_NAME']?>)</td>
                                                                           <?php $cond="where `STUD_NO`=".$row_wf[student_id];
                                                                              $stud_nm= getRows($cond,'student'); ?>
                                                                           <td class=" "><a href="dashboard.php?op=<?=MD5('student_edit')?>&sid=<?=$stud_nm[0]['STUD_NO']?>"><?=$stud_nm[0]['EXT_STN']?></a></td>
                                                                           <td class=" "><a href="dashboard.php?op=<?=MD5('student_edit')?>&sid=<?=$stud_nm[0]['STUD_NO']?>"><?=$stud_nm[0]['FNAME']?> <?=$stud_nm[0]['LNAME']?></a></td>
                                                                           <td><a href="mailto:<?=$stud_nm[0]['L_EMAIL']?>?Subject=<?=$app_type[0]['NAME']?>" target="_top"><?=$stud_nm[0]['L_EMAIL']?></a></td>
                                                                           <td class=" "><?=$stud_nm[0]['DOB']?></td>
                                                                           <td class=""><?=$stud_nm[0]['COUNTRY_NAME']?></td>
                                                                           <td class=""><?=$stud_nm[0]['L_MOBILE']?></td>
                                                                           <td class="">
                                                                              <?php                                               
                                                                                 $stat_date = $row_wf['create_date'];
                                                                                 $today = date('d/m/Y');
                                                                                                                        $complete = $dia_summ[0]['DIARY_DATE'];
                                                                                 $cancel=1;
                                                                                 if($today==$stat_date){
                                                                                     $favcolor = "start";
                                                                                 }elseif($today==$tomorrow){
                                                                                     $favcolor = "processing";
                                                                                 }elseif($complete==$today){
                                                                                     $favcolor = "complete";
                                                                                 }elseif($cencel==0){
                                                                                     $favcolor = "cencel";
                                                                                 }
                                                                                 
                                                                                 switch ($favcolor) {
                                                                                     case "start":
                                                                                         echo '<i class="fa fa-circle blue-blub" aria-hidden="true"></i>
                                                                                                                        <i class="fa fa-circle" aria-hidden="true"></i>
                                                                                                                        <i class="fa fa-circle " aria-hidden="true"></i>
                                                                                                                        <i class="fa fa-circle " aria-hidden="true"></i>';
                                                                                         break;
                                                                                     case "processing":
                                                                                         echo '<i class="fa fa-circle" aria-hidden="true"></i>
                                                                                                                        <i class="fa fa-circle yellow-blub" aria-hidden="true"></i>
                                                                                                                        <i class="fa fa-circle " aria-hidden="true"></i>
                                                                                                                        <i class="fa fa-circle " aria-hidden="true"></i>';
                                                                                         break;
                                                                                     case "complete":
                                                                                         echo '<i class="fa fa-circle" aria-hidden="true"></i>
                                                                                                                        <i class="fa fa-circle" aria-hidden="true"></i>
                                                                                                                        <i class="fa fa-circle green-blub" aria-hidden="true"></i>
                                                                                                                        <i class="fa fa-circle " aria-hidden="true"></i>';
                                                                                         break;
                                                                                     case "cencel":
                                                                                         echo '<i class="fa fa-circle" aria-hidden="true"></i>
                                                                                                                        <i class="fa fa-circle" aria-hidden="true"></i>
                                                                                                                        <i class="fa fa-circle " aria-hidden="true"></i>
                                                                                                                        <i class="fa fa-circle red-blub" aria-hidden="true"></i>';
                                                                                         break;
                                                                                     default:
                                                                                         echo '<i class="fa fa-circle" aria-hidden="true"></i>
                                                                                                                        <i class="fa fa-circle" aria-hidden="true"></i>
                                                                                                                        <i class="fa fa-circle " aria-hidden="true"></i>
                                                                                                                        <i class="fa fa-circle " aria-hidden="true"></i>';
                                                                                 }
                                                                                 ?>
                                                                           </td>
                                                                           <td class="last"><a class="btn btn-success assign" data-toggle="modal" data-target="#editBox" href="file.php?id=<?=$stud_nm[0]['STUD_NO']?>"><span class="glyphicon glyphicon-pencil"></span>Assign</a> | <a href="" onclick="javascript:void window.open('dashboard.php?op=<?=MD5('mail_send')?>&d_no=<?=$dia_summ[0]['DIARY_NO']?>&s_id=2&s_no=<?=$stud_nm[0]['EXT_STN']?>&s_fname=<?=$stud_nm[0]['FNAME']?>&s_lname=<?=$stud_nm[0]['MNAME']?>&email=<?=$stud_nm[0]['L_EMAIL']?>&addre=<?=$stud_nm[0]['LADDR1']?>&course=<?=$co_nm[0]['COURSE_NAME']?>&en_st=<?=$en_nm[0]['ST_DATE']?>','1470138621367','width=800,height=500,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=0,top=0');return false;">Send message</a></td>
                                                                        </tr>
                                                                        <?php } } ?>
                                                                     </tbody>
                                                                  </table>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <!--documents-->
                                               
                                                <!--Application Form-->
                                               
                                        
                                          </div>
                                             </form>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

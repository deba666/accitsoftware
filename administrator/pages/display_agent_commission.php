<?php
    /*
        * @package    ACCIT
         * @author    Paperlink Softwares Team
         * @copyright    Copyright (c) 2016 , Paperlink Softwares Pvt. Ltd.  (http://www.paperlinksoftwares.com/)
         * @since    Version 1.0.0
         * @filesource
	 * Code for add new agent commission link comes from agent edit page(Add New Commission) 
         */

       if($_POST['AGENT_NO'])
       {
           
           if($_POST['OPTION_TYPE']==1) { $val_index = "`RATE_PERCENT`='".$_POST['RATE_PERCENT']."',`RATE_AMOUNT`=''"; } else { $val_index = "`RATE_PERCENT`='',`RATE_AMOUNT`='".$_REQUEST['RATE_PERCENT']."'"; }
           $sql_update = mysql_query("UPDATE `agent_commission` SET `LOCK_NUM` = '0' WHERE `AGENT_NO` = ".$_REQUEST['AGENT_NO']);
       	if(insert_record("insert into `agent_commission` set `AGENT_NO`='".$_POST['AGENT_NO']."',`CRT_NO`='".$_POST['CRT_NO']."',`NAME`='".$_POST['NAME']."',`OPTION_NO`='".$_POST['OPTION_NO']."',".$val_index.",`CREATE_DATE`='".$_POST['CREATE_DATE']."',`DELETE_DATE`='".$_POST['DELETE_DATE']."',`LOCK_NUM`='".$_POST['LOCK_NUM']."', `APPLY_BEFORE_TAX` = '".$_POST['ABN']."'"))
        {
            $_SESSION['s_msg']="<strong>Fine!</strong>New Commission Added Successfully";
            ?>

<script>
opener.location.reload(true);
     self.close();
     
     </script>
     
<?php
        }	else{
       			$_SESSION['e_msg']="<strong>Oh snap!</strong>New Agent Employee is not added";
       }
       
                }
       ?>
<div class="col-md-12 col-sm-12 col-xs-12">
    <div class="page-title">
        <div class="title_left">
            <?php 
                if($_SESSION['s_msg'] || $_SESSION['e_msg'])
                {
                ?>
            <div class="alert <?=($_SESSION['s_msg'])?'alert-success':'alert-error'?> fade in">
                <button data-dismiss="alert" class="close" type="button">�</button>
                <?=($_SESSION['s_msg'])?$_SESSION['s_msg']:$_SESSION['e_msg']?>
            </div>
            <?php
                unset($_SESSION['s_msg']);
                unset($_SESSION['e_msg']);
                }
                ?>
            <h3>Commission Entry - Agent</h3>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<div class="row">
<div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
        <div class="clearfix"></div>
        <div class="x_content">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel recent-app">
                        <div class="x_content">
                            <div class="student-account-page edit-faculty-page">
                                <!-- Nav tabs -->
                                <div class="card">
                                    <div class="all-students-list add student">
                                        <div class="add-student-section">
                                            <form method="post" action="" class="form-horizontal form-label-left">
                                                <div class="item form-group">
                                                    <div class="item form-group">
                                                        <label for="name" class="control-label col-md-2 col-sm-2 col-xs-2">Faculty:</label>
                                                        <div class="col-md-8 col-sm-8 col-xs-8">
                                                            <div class="row">
                                                                <div class="col-md-7 col-sm-7 col-xs-12">
																 <?php date_default_timezone_set('Australia/sydney');
																	$date = date('d/m/Y h:i:s a', time());
																	?>
																<input name="CREATE_DATE"  type="hidden" value="<?php echo $date?>">
									
                                                                                                                                
                                                                                                                                
                                                                                                                                <input type="hidden" value="<?php echo $_SESSION['user_no']?>">
                                                                  
                                                                                                                                <?php
                                                                                    $conda1="where 1";
                                                                                    $std_fac=getRows($conda1,'crse_type'); ?>
                                                     <select name="CRT_NO" class="form-control col-md-12 col-xs-12" required>
                                                                        <option value="">Select Faculty</option>
                                                                        <?php foreach($std_fac as $row) { ?>
                                                                        <option value="<?php echo $row['CRT_NO']; ?>"><?php echo $row['CRT_NAME']; ?></option>
                                                                        <?php } ?>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="item form-group">
                                                        <label for="name" class="control-label col-md-2 col-sm-2 col-xs-2">Name</label>
                                                        <div class="col-md-8 col-sm-8 col-xs-8">
                                                            <div class="row">
                                                                <div class="col-md-7 col-sm-7 col-xs-12">
																		<?php
																		if($_GET['ag_no'])
																			{
																				$cond="where `AGENT_NO`=".$_GET['ag_no'];
																				$std_row=getRows($cond,'agent');
																			} ?>
                                                                        <input class="form-control col-md-4 col-sm-4 col-xs-4" name="AGENT_NO"  type="hidden" value="<?=$std_row[0]['AGENT_NO']?>">
																		<input class="form-control col-md-4 col-sm-4 col-xs-4" type="text" name="NAME" required>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                      <div class="item form-group">
                                                        <label for="name" class="control-label col-md-2 col-sm-2 col-xs-2">Rate</label>
                                                    <div class="col-md-12 col-sm-12">
                                                       
                                                        <div class="col-md-3 col-sm-3 col-xs-3">
                                                            <div class="form-group">
                                                                <input class="form-control col-md-4 col-sm-4 col-xs-4" name="RATE_PERCENT"  type="number" required size="33">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 col-sm-3 col-xs-3">
                                                           
                                                                <select name="OPTION_TYPE" >
                                                                    <option value="1" >%</option>
                                                                    <option value="2">$</option>
                                                                   
                                                                </select>
                                                            
                                                        </div>
                                                      <!--  <div class="col-md-3 col-sm-3 col-xs-3">
                                                            <div class="form-group">
                                                                <select name="OPTION_NO" class="form-control col-md-4 col-sm-4 col-xs-4">
                                                                    <option value="1">Inclusive Of Taxes</option>
                                                                    <option value="2">Exclusive of Taxes</option>
                                                                    <option value="0">Do Not Apply Taxes</option>
                                                                </select>
                                                            </div>
                                                        </div> -->
                                                    </div>
                                                      </div>
                                                    
                                                  <!--   <div class="item form-group">
                                                        <label for="name" class="control-label col-md-2 col-sm-2 col-xs-2">Apply Before Tax</label>
                                                        <div class="col-md-8 col-sm-8 col-xs-8">
                                                            <div class="row">
                                                                <div class="col-md-7 col-sm-7 col-xs-12">
																		
                                                                    <input name="ABN" type="checkbox" class="form-control col-md-4 col-sm-4 col-xs-4" id="ABN" /> 
																		
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div> -->
                                                      
                                                    <div class="item form-group">
                                                        <label for="name" class="control-label col-md-2 col-sm-2 col-xs-2">Default</label>
                                                        <div class="col-md-8 col-sm-8 col-xs-8">
                                                            <div class="row">
                                                                <div class="col-md-7 col-sm-7 col-xs-12">
																		
                                                                    <input class="form-control col-md-4 col-sm-4 col-xs-4" name="LOCK_NUM" id="LOCK_NUM" value="1"  type="checkbox" />
																		
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                        <div class="item form-group">
                                                        <label for="name" class="control-label col-md-3 col-sm-3 col-xs-12"></label>
                                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                                            <div class="row">
                                                                <div class="col-md-6 col-sm-4 col-xs-12">
                                                                    <p class="pull-left">
                                                                         <input class="btn btn-primary" type="button" onclick="javascript: window.close();" value="Cancel">
                                                                        <button class="btn btn-success" type="submit">Submit</button>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                            </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
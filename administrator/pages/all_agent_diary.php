<?php
    /*
        * @package    ACCIT
         * @author    Paperlink Softwares Team
         * @copyright    Copyright (c) 2016 , Paperlink Softwares Pvt. Ltd.  (http://www.paperlinksoftwares.com/)
         * @since    Version 1.0.0
         * @filesource
         */
    ?>
<div role="main" class="right_col" style="min-height: 648px;">
    <div class="">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="page-title">
                <div class="title_left">
                    <h3>All Diary(Agent)</h3>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="clearfix"></div>
                    <div class="x_content">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel recent-app">
                                    <div class="x_content">
                                        <div class="student-account-page">
                                            <!-- Nav tabs -->
                                            <div class="card">
                                                <ul role="tablist" class="nav nav-tabs">
                                                    <li class="active" role="presentation"><a data-toggle="tab" role="tab" aria-controls="all-student" href="#all-student">Diary</a></li>
                                                </ul>
                                                <div class="tab-content">
                                                    <div id="all-student" class="tab-pane active" role="tabpanel">
                                                        <div class="all-students-list">
                                                            <div class="table-responsive">
                                                                <div>
                                                                    <div>
                                                                        <div>
                                                                            <table class="table data-tbl-tools">
                                                                                <thead>
                                                                                    <tr class="headings">
                                                                                        <th class="column-title">Date
                                                                                        </th>
                                                                                        <th class="column-title">Attention
                                                                                        </th>
                                                                                        <th class="column-title">Agent Code
                                                                                        </th>
                                                                                        <th class="column-title">Subject
                                                                                        </th>
                                                                                        <th class="column-title">Agent Name
                                                                                        </th>
                                                                                        <th class="column-title">Notes
                                                                                        </th>
                                                                                        <th class="column-title">When
                                                                                        </th>
                                                                                        <th class="column-title">User
                                                                                        </th>
                                                                                        <th class="column-title">Action
                                                                                        </th>
                                                                                    </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                    <?php
                                                                                        $co1="where `CUSTOMER_NO`=".$_GET['cus_no'];
                                                                                        $st1=getRows($co1,'diary');
                                                                                        foreach($st1 as $st2)																			{
                                                                                           $co2="where `USER_NO`=".$st2['USER_NO'];
                                                                                        $st3=getRows($co2,'users');
                                                                                        foreach($st3 as $st4)																			{
                                                                                        
                                                                                        	?>
                                                                                    <tr class="even pointer">
                                                                                        <td class=" "><?=$st2['DIARY_DATE']?></td>
                                                                                        <td class=" "><?=$st2['ATTENTION']?></td>
                                                                                        <td class=" "><?=$st2['REF_NO']?></td>
                                                                                        <td class=" "><?=$st2['SUMMARY']?></td>
                                                                                        <td class=" "><?=$std_row[0]['AGENT_NAME']?></td>
                                                                                        <td class=" "><?=$st2['NOTES']?></td>
                                                                                        <td class=" "><?=$st2['WHO_DATE']?></td>
                                                                                        <td class=" "><?=$st4['INIT']?></td>
                                                                                        <td><a href="#" onclick="javascript:void window.open('dashboard.php?op=<?=MD5('display_agent_diary_edit')?>&d_no=<?=$st2['DIARY_NO']?>&a_na=<?=$std_row[0]['AGENT_NAME']?>','1470138621367','width=700,height=500,toolbar=0,menubar=0,location=1,status=1,scrollbars=1,resizable=1,left=0,top=0');return false;">Edit</a>|<a href="dashboard.php?op=<?=MD5('agent_details')?>&del2=<?=$st2['DIARY_NO']?>&aid=<?=$std_row2['AGENT_NO']?>" class="color-sky stting"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>Delete</a></td>
                                                                                    </tr>
                                                                                    <?php }} ?>
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
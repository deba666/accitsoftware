<?php
    /*
        * @package    ACCIT
         * @author    Paperlink Softwares Team
         * @copyright    Copyright (c) 2016 , Paperlink Softwares Pvt. Ltd.  (http://www.paperlinksoftwares.com/)
         * @since    Version 1.0.0
         * @filesource
		 * Code for display all rooms from database
		 * Link comes from sidebar(utility->rooms->all rooms)
         */
    
    
    ?>
<div class="right_col" role="main">
    <div class="">
      
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="clearfix"></div>
                    <div class="x_content">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel recent-app">
                                    <div class="x_content">
                                        <div class="student-account-page">
                                            <!-- Nav tabs -->
                                            <div class="card">
                                                <div class="tab-content">
                                                    <div role="tabpanel" class="tab-pane active" id="all-student">
                                                        <div class="all-students-list">
                                                            <div class="table-responsive">
                                                                <table  class="table data-tbl-tools">
                                                                    <thead>
                                                                        <tr class="headings">
                                                                            <th class="column-title">Receipt No</th>
                                                                             <th class="column-title">Reference Date</th>
                                                                            <th class="column-title">Reference No</th>
                                                                           
                                                                            <th class="column-title">Details</th>
                                                                            <th class="column-title">Amount</th>
                                                                            <th class="column-title">Student</th>
                                                                            <th class="column-title">User</th>
                                                                            <th class="column-title">Enrol No</th>                                                                          
                                                                        </tr>
                                                                    </thead>
                                                                    <?php $receipt=getRows('order by `RECEIPT_NO` DESC','receipt'); ?>
                                                                    <tbody>
                                                                        <?php
                                                                          
                                                                            foreach($receipt as $row)
                                                                            {
                                                                                $credit = getRows("WHERE `RECEIPT_NO` = '".$row['RECEIPT_NO']."'","credit");
                                                                                $enrol  = getRows("WHERE `ENROL_NO` = '".$credit[0]['ENROL_NO']."'","enrol");
                                                                                $stud =   getRows("WHERE `STUD_NO` = '".$enrol[0]['STUD_NO']."'","student");
                                                                                $user   = getRows("WHERE `USER_NO` = '".$row['USER_NO']."'","users");
                                                                               // $agent=getRows("WHERE `CUSTOMER_NO` = '".$row['PAYEE_CUSTOMER_NO']."'","CUSTOMER");
                                                                            ?>
                                                                        <tr class="even pointer">
                                                                            <td class=""><?=$row['RECEIPT_NO'];?></td>
                                                                            <td class=""><?=$row['REFERENCE_DATE'];?></td>
                                                                            <td class=""><?=$row['REFERENCE_ID'];?></td>
                                                                            <td class=""></td>
                                                                            <td class="last"><?=$row['AMOUNT'];?></td>
                                                                            <td class="last"><?=$stud[0]['FNAME']."&nbsp;".$stud[0]['MNAME']."&nbsp;".$stud[0]['LNAME'];?></td>
                                                                            <td class="last"><?=$user[0]['USER_NAME'];?></td>
                                                                            <td class="last"><?=$credit[0]['ENROL_NO']?></td>
                                                                        </tr>
                                                                        <?php   }    ?>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
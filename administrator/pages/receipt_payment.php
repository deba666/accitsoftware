<?php
    /*
        * @package    ACCIT
         * @author    Paperlink Softwares Team
         * @copyright    Copyright (c) 2016 , Paperlink Softwares Pvt. Ltd.  (http://www.paperlinksoftwares.com/)
         * @since    Version 1.0.0
         * @filesource
		 * Code for fill the receipt payment data
		 * The data are send to the (make_payment.php) using POST 
         */
    ?>
<script language="javascript" type="text/javascript" src="datetimepicker.js"></script>
<script type="text/javascript"></script>


<div class="col-md-12 col-sm-12 col-xs-12">
    <div class="page-title">
        <div class="title_left">
            <h3>Receipt Wizard</h3>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="clearfix"></div>
            <div class="x_content">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel recent-app">
                            <div class="x_content">
                                <div class="student-account-page edit-faculty-page">
                                    <!-- Nav tabs -->
                                    <div class="card">
                                        <div class="all-students-list add student">
                                            <div class="add-student-section">
                                                <form method="POST" action="dashboard.php?op=<?=MD5('make_payment')?>" class="form-horizontal form-label-left">
                                                    <?php date_default_timezone_set('Australia/Melbourne');
                                                        $date = date('d/m/Y h:i:s a', time());
                                                        ?>
                                                    <input id="who"  name="WHO_DATE"  type="hidden" value="<?php echo $date?>">
                                                    <input id="when"  name="user" type="hidden" value="<?php echo $_SESSION['user_no']?>">
                                                    <input id="when"  name="ENROL_NO" type="hidden" value="<?= $_SESSION['enrol_no']?>">
                                                    <input id="when"  name="OFFER_ITEM_NO" type="hidden" value="<?= $_SESSION['offer_item_no']?>">
													<input name="cust_no"  value="<?=$_SESSION['cust_no']?>" class="form-control col-md-12 col-xs-12" id="name" type="hidden">
                                                    <div class="row">
                                                        <div class="col-md-2 col-sm-2 col-xs-3">
                                                            <div class="form-group">
                                                                <label class="pull-left control-label col-md-12 col-sm-12 col-xs-12" for="name">Date</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2 col-sm-2 col-xs-3">
                                                            <div class="form-group">
                                                                <input  value="<?= $_POST['RECEIPT_DATE'] ?>" class="form-control col-md-12 col-sm-12 col-xs-12" name="RECEIPT_DATE" type="date">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="item form-group">
                                                        <label for="name" class="control-label col-md-2 col-sm-2 col-xs-2">Bank Account</label>
                                                        <div class="col-md-8 col-sm-8 col-xs-8">
                                                            <div class="row">
                                                                <div class="col-md-7 col-sm-7 col-xs-12">
                                                                    <select name="BANK_NO" class="form-control">
                                                                        <option value="1" <?php if ($std_row[0]['BANK_NO'] == '1') { echo 'selected="selected"';}?>>Working Account</option>
                                                                        <option value="2" <?php if ($std_row[0]['BANK_NO'] == '2') { echo 'selected="selected"';}?>>Power Saver 1</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="item form-group">
                                                        <label for="name" class="control-label col-md-2 col-sm-2 col-xs-2">Reference No</label>
                                                        <div class="col-md-8 col-sm-8 col-xs-8">
                                                            <div class="row">
                                                                <div class="col-md-7 col-sm-7 col-xs-12">
                                                                    <input value="<?= $_POST['REFERENCE_NO']?>" name="REFERENCE_ID" class="form-control col-md-12 col-xs-12" id="name" type="text">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-2 col-sm-2 col-xs-2">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-2 col-sm-2 col-xs-2" for="name">Payment Type
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 col-sm-3 col-xs-3">
                                                            <div class="form-group">
                                                                <select name="DEBIT_TYPE_NO"  class="control-label col-md-12 col-sm-12 col-xs-12">
                                                                    <option value="0"></option>
                                                                    <?php
                                                                        $sql="select * from debit_type";
                                                                        $q=mysql_query($sql);
                                                                        while($r=mysql_fetch_array($q))
                                                                        {
                                                                        ?>
                                                                    <option value="<?=$r['DEBIT_TYPE_NO']?>"><?=$r['DEBIT_TYPE_NAME']?></option>
                                                                    <?php } ?>	
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2 col-sm-2 col-xs-2">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-4 col-sm-4 col-xs-4" for="name">Amount
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 col-sm-3 col-xs-3">
                                                            <div class="form-group">
                                                                <input  id="n1" onkeyup="sync()" class="control-label col-md-12 col-sm-12 col-xs-12" name="n1" Value="" type="text">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2 col-sm-2 col-xs-4">
                                                            <div class="form-group">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="item form-group">
                                                        <label for="name" class="control-label col-md-2 col-sm-2 col-xs-2">Details/Drawer</label>
                                                        <div class="col-md-8 col-sm-8 col-xs-8">
                                                            <div class="row">
                                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                                    <input name="DETAIL" class="form-control col-md-12 col-xs-12" id="name" type="text">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div role="tabpanel" class="tab-pane active" id="instalment">
                                                        <div class="table-responsive">
                                                            <table class="table table-striped jambo_table bulk_action">
                                                                <thead>
                                                                    <tr class="headings">
                                                                        <th class="column-title">Invoice No</th>
                                                                        <th class="column-title">Due Date</th>
																		<th class="column-title">Fee</th>
                                                                        <th class="column-title">Amount</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <?php
                                                                        $cond20="where `ENROL_NO`=".$_POST['ENROL_NO'];
																		   $std_row20=getRows($cond20,'invoice');
																		   foreach($std_row20 as $std_row21)
																			   {
                                                                        $cond22="where `INVOICE_NO`=".$std_row21['INVOICE_NO'];
																	   $std_row22=getRows($cond22,'invoice_details');
																	   foreach($std_row22 as $std_row23)
																		   {
																	    $add1a=mysql_query("SELECT * from due where `IND_NO`=".$std_row23['IND_NO']);
																		while($row2a = mysql_fetch_array($add1a))
																		{
																		if($row2a['AMOUNT']!=0)
																		 {
																		 
                                                                        ?>
                                                                    <tr>
                                                                        
                                                                        <td class=""><?= $row2a['INVOICE_NO']?></td>
                                                                        <td class=""><?php echo date('d/m/Y',strtotime($row2a['DUE_DATE']));?></td>
																		
																		<td class=" ">
                                                                         <?php
																			if($std_row23['OFFER_INSTALMENT_NO']){																										                                                                             $cond202="where `OFFER_INSTALMENT_NO`=".$std_row23['OFFER_INSTALMENT_NO'];
																																													 																			$std_row202=getRows($cond202,'offer_instalment'); }
																		if($std_row202[0]['OFFER_ITEM_NO']){																										 																			$cond_off_it="where `OFFER_ITEM_NO`=".$std_row202[0]['OFFER_ITEM_NO'];
																																												 																						 																				$std_row_off_it=getRows($cond_off_it,'offer_item');}
																																											  
																	if($std_row_off_it[0]['PRODUCT_NO']){																										 																			$cond_off_it_pr="where `PRODUCT_NO`=".$std_row_off_it[0]['PRODUCT_NO'];
																																											    	                                                                             $std_row_off_it_pr=getRows($cond_off_it_pr,'product');}
																			if($std_row23['FEE_NO']=='76')
																			  {
																				$name="Agent Fee";
																			  }
																			  else
																			  {
																				$name=$std_row_off_it_pr[0]['NAME'];
																			  }
																			  ?>
                                                                            <?php echo $name;?>
                                                                        <td class=""><?=$row2a['AMOUNT']?></td>
                                                                        <input type="hidden" value="<?=$stud_rowd11['INVOICE_NO']?>" name="INVOICE_NO">
                                                                        <input type="hidden" value="<?=$stud_rowd11['PAYEE_CUSTOMER_NO']?>" name="PAYEE_CUSTOMER_NO">
                                                                    </tr>
                                                                    <?php } } } }?>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-2 col-sm-2 col-xs-2">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-12 col-sm-12 col-xs-12" for="name">Bank Account
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4 col-sm-4 col-xs-4">
                                                            <div class="form-group">
                                                                <?php
                                                                    $c="where `BANK_NO`=".$_POST['BANK_NO'];
                                                                                                                                $s=getRows($c,'bank_account');
                                                                    ?>
                                                                <input class="control-label col-md-12 col-sm-12 col-xs-12" name="BANK_NAME" Value="<?= $s[0]['BANK_NAME']?>" type="text">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2 col-sm-2 col-xs-2">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-12 col-sm-12 col-xs-12" for="name">Receipt Total
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4 col-sm-4 col-xs-4">
                                                            <div class="form-group">
                                                                <input id="n2" class="control-label col-md-12 col-sm-12 col-xs-12" name="n2" Value="" type="text">
                                                            </div>
                                                        </div>
                                                    </div>
                                            </div>
                                            <div class="item form-group">
                                            <label for="name" class="control-label col-md-3 col-sm-3 col-xs-12"></label>
                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                            <div class="row">
                                            <div class="col-md-6 col-sm-4 col-xs-12">
                                            <p class="pull-left">
                                            <a class="btn btn-success" href="#" onclick="javascript: window.location.href='dashboard.php?op=<?=MD5('receipt')?>&enrol_no=<?=$std_row21['ENROL_NO']?>&ref_no=<?=$_POST['REFERENCE_NO']?>&ref_date=<?=$_POST['RECEIPT_DATE']?>&BANK_NO=<?php echo $_POST['BANK_NO']; ?>';">&nbsp;&nbsp;&nbsp;&nbsp;Previous</a>
                                            <button type="submit" class="btn btn-success">Next</button>
                                            </p>
                                            </div>
                                            </div>
                                            </div>
                                            </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
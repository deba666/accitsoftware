<?php
    /*
        * @package    ACCIT
         * @author    Paperlink Softwares Team
         * @copyright    Copyright (c) 2016 , Paperlink Softwares Pvt. Ltd.  (http://www.paperlinksoftwares.com/)
         * @since    Version 1.0.0
         * @filesource
    * Code for add new course in offer. After create new offer when you clicked on new course (For add new offer) this code will work.
         */
    
    $result = mysql_query("SELECT * FROM student WHERE STUD_NO='" . $_POST["STUD_NO"] . "'"); //Take the student details from student table 
    $row = mysql_fetch_array($result);
    $name = $row['FNAME'] ." ". $row['MNAME'] ." ". $row['LNAME'];
    if($_GET['of_no'])
    {
    $s=$_GET['of_no'];
    }
    else
    {
    $s=$_SESSION['offer_no'];
    }
    
    $AGENT_NO=$_GET["AGENT_NO"];
    
    $_SESSION['AGENT_NO']=$AGENT_NO;
    if($_POST['RATE_PERCENT'])
    {
    $commission=$_POST['RATE_PERCENT'];
    }
    else if($_POST['CUSTOM_RATE'])
    {
    $commission=$_POST['CUSTOM_RATE'];
    }
    $amount=$_GET['cost'];
    $commission_percentage=($commission*$amount)/100;
    $afte_add_tax_commission=($commission_percentage*$_POST['TAX_AMOUNT'])/100;
    $final_commission= $commission_percentage+$afte_add_tax_commission;
    $q=mysql_query("select * from holiday where CRT_NO=".$_GET['faculty']);
        while($row=mysql_fetch_array($q))
        {
        if(!$row['DELETE_DATE'])
            {
                $begin = new DateTime($row['FROM_DATE']);//store holiday start date in $begin
                $end = new DateTime($row['TO_DATE']);//store holiday end date in $end
                $diff = $end->diff($begin)->format("%a");//calculate total difference dates in holiday from date to end date
                //echo"TOTAL DIFFERENCE DATE IS=".$diff ."<br>";
                $daterange = new DatePeriod($begin, new DateInterval('P1D'), $end);
                
            
            foreach($daterange as $date)
                {
                $date1=$date->format("Y-m-d");//change holiday date format into dd/mm/yyyy format
                //echo "Holiday Dates Are=".$date1."</br>";
                $holiday_list[]=$date1;//store all the holidays into $holiday_list variable which are fetch from database 
             
                }
                
             }  
        }

$start_date=$_POST['start_date'];
$length=($_POST['length']*7);
$start = new DateTime($start_date);
$start_date = date_create($start->format('Y-m-d'));
$start_date= date_format($start_date, 'Y-m-d'); 
$start_date=date('Y-m-d', strtotime($start_date));
if(in_array($start_date,$holiday_list))//check if the user input date comes under holiday dates or not
        {
           $holiday='0';
           // echo " THIS DATE MATCHED SUCCESSFULLY!!!!!!MATHED DATE IS".$start_date."<br>";
            $message = $start_date."  is Holiday You Can not start Course at this date Select Another date";
            echo "<script type='text/javascript'>alert('$message');</script>";
            
      }

$end_date1=date('Y-m-d', strtotime($start_date.' +'.$length.'days'));
$end_date=date('Y-m-d', strtotime($end_date1.'last friday'));
//$end_date=date('Y-m-d', strtotime($end_date1));
//echo "End Date=".$end_date."</br>";
$temp=date('Y-m-d', strtotime($end_date1));
//echo "End Date=".$end_date."</br>";
$datesa=array();
$i=0;   
    
    // Call the function
    $datesa = getDatesFromRange($start_date, $end_date);
    $diff = array_intersect($holiday_list, $datesa);
    $no_of_matching_holiday_date=count($diff);
    if($no_of_matching_holiday_date>0)
    {
        do{
            $i++;
            $end_date=date('Y-m-d', strtotime($end_date.' + 1days'));
            //echo "Holiday Dates= " . $end_date . "<br>";
            }
        while(in_array($end_date,$holiday_list));
    }

    $no_of_date=$no_of_matching_holiday_date+$i;
    $add_holiday_with_end_date=date('Y-m-d', strtotime($temp.' +'.$no_of_date.'days'));
    $lastfriday=new DateTime($add_holiday_with_end_date);
    $lastfriday->modify('friday');
    $lastfriday1=$lastfriday->format('d-m-Y');//change format of last friday from dd/mm/yyyy to dd-mm-yyyy
    $lastfriday2=$lastfriday->format('Y-m-d');
    $ending_date=date('Y-m-d', strtotime($lastfriday2.'last friday'));
    //echo "No Of holidays Matching=".$no_of_matching_holiday_date."</br>";
    //echo "No Of Holiday Found=".$i."</br>";
     //echo "No Of Total Holiday=".$no_of_date."</br>";
    //echo "End Date After Holiday=".date('Y-m-d', strtotime($lastfriday2.'friday'))."</br>";
function getDatesFromRange($start, $end, $format = 'Y-m-d') {//Get all the dates from course start date to course final end date
        $array = array();
        $interval = new DateInterval('P1D');
    
        $realEnd = new DateTime($end);
        $realEnd->add($interval);
    
        $period = new DatePeriod(new DateTime($start), $interval, $realEnd);
    
        foreach($period as $date) { 
            $array[] = $date->format($format); 
        }
    
        return $array;
    }
    
    if($_POST['DISCOUNT']==1)
    {
    $disc_amount=$_POST['DISCOUNT_RATE'];
    }
    else if($_POST['DISCOUNT']==2)
    { 
    $disc_percent=$_POST['DISCOUNT_RATE'];
    }
    //echo "HGb".$dd."<br>";
    //echo "HOLIDAY NO=".$holiday."<br>";
    //echo "END DATE(AFTER ADD HOLIDAY -NEXT FRIDAY)" .$lastfriday->format('d-m-Y')."<br>";
   if(isset($_POST['DESCRIPTION']))
    {
              if(insert_record("insert into `offer_item` set `OFFER_NO`='$s',`CUSTOMER_NO`='".$_GET['cust_no']."',`PRODUCT_NO`='".$_GET['PRODUCT_NO']."',`PRODUCT_TYPE_NO`='".$_GET['PRODUCT_TYPE_NO']."',`PARENT_NO`='',`REF_NO`='".$_GET['REFERENCE_NO']."',`REF_TYPE_NO`='',`DESCRIPTION`='".$_POST['DESCRIPTION']."',`FROM_DATE`='$start_date',`TO_DATE`='$ending_date',`PERIOD_LENGTH`='".$_POST['length']."',`NOTES`='".$_POST["NOTE"]."',`AGENT_COMMISSION_NO`='".$_POST['AGENT_COMMISSION_NO']."',`COMMISSION_AMOUNT`='$final_commission',`AMOUNT`='".$_POST['AMOUNT']."',`DISC_AMOUNT`='$disc_amount',`SUB_AMOUNT`='".$_POST['SUB_AMOUNT']."',`TAX_AMOUNT`='$afte_add_tax_commission',`TOTAL_AMOUNT`='".$_POST['TOTAL_AMOUNT']."',`CUSTOM_1`='".$_POST['CUSTOM_1']."',`CUSTOM_2`='".$_POST['CUSTOM_2']."',`CUSTOM_3`='".$_POST['CUSTOM_3']."',`CUSTOM_4`='".$_POST['CUSTOM_4']."',`CUSTOM_5`='".$_POST['CUSTOM_5']."',`CREATE_DATE`='".$_POST['CREATE_DATE']."',`DELETE_DATE`='',`LOCK_NUM`='".$_GET['LOCK_NUM']."',`PROVIDER_CUSTOMER_NO`='',`PRODUCT_RATE_NO`='',`PRICE_BOOK_NO`='',`CREATE_ENTRY_FLAG`='1',`OPTION_NO`='".$_POST['OPTION_NO']."',`GROUP_NO`='".$_POST['GROUP_NO']."',`UPDATE_DATE`='',`DISPLAY_ORDER`='1',`DISC_PERCENT`='$disc_percent'"))
        //echo "###End Date=".date('Y-m-d', strtotime($lastfriday2 .'last friday'));
                    $_SESSION['s_msg']="<strong>Well done!</strong>Course Offer Added."; 
                
                    
             }  else {
                    $_SESSION['e_msg']="<strong>Oh snap!</strong>Course Offer Not Added"; 
                   
                  
    } 

    ?>

<script language="javascript" type="text/javascript" src="datetimepicker.js"></script>
<div role="main" class="right_col" style="min-height: 738px;">
    <div class="">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="custom-alrt">
                <?php 
                    if($_SESSION['s_msg'] || $_SESSION['e_msg'])
                    {
                    ?>
                <div class="alert <?=($_SESSION['s_msg'])?'alert-success':'alert-error'?> fade in">
                    <button data-dismiss="alert" class="close" type="button"><i class="fa fa-times" aria-hidden="true"></i></button>
                    <?=($_SESSION['s_msg'])?$_SESSION['s_msg']:$_SESSION['e_msg']?>
                </div>
                <?php
                    unset($_SESSION['s_msg']);
                    unset($_SESSION['e_msg']);
                    }
                    ?>
            </div>
              <style>
        footer{display: none !important; }
        .nav_menu{display: none;}
       </style>
            <div class="page-title">
                <div class="title_left">
                    <h3>Offer Item - Course</h3>
                </div>
            </div>
        </div>
       
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="clearfix"></div>
                    <div class="x_content">
                        <form  method="POST" action="" name="datepick">
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="x_panel recent-app">
                                        <div class="x_content">
                                            <div class="student-account-page">
                                                <!-- Nav tabs -->
                                                <div class="card">
                                                    <div class="all-students-list add student">
                                                        <div class="add-student-section">
                                                           
                                                            <div class="tab-content">
                                                            
                                                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                                                        <div class="row">
                                                                            <h4>
                                                                                <fieldset>Course Details</fieldset>
                                                                            </h4>
                                                                            <div class="col-md-5 col-sm-5 col-xs-12">
                                                                                <?php date_default_timezone_set('Australia/Melbourne');
                                                                                    $date = date('d/m/Y h:i:s', time());
                                                                                    
                                                                                    ?>
                                                                                <input type="hidden" class="form-control col-md-12 col-xs-12" name="CREATE_DATE" value="<?php echo $date?>">
                                                                                <div class="form-group">
                                                                                    <label for="name" class="pull-left control-label">Code
                                                                                    </label>
                                                                                    <input type="text" value="<?=$_GET['code']?>" class="form-control col-md-12 col-xs-12" placeholder="Course code" name="COURSE_CODE" readonly>
                                                                                </div>
                                                                            </div>
                                                                            <?php
                                                                                $p=0;
                                                                                if($s)
                                                                                {
                                                                                 $grp_field="where `OFFER_NO`=".$s;
                                                                                 $grp=getRows($grp_field,'offer_item');
                                                                                 foreach($grp as $grp1)
                                                                                 {
                                                                                 if($grp1['GROUP_NO']==null)
                                                                                 {
                                                                                  $group_no=$p;
                                                                                 }
                                                                                 else
                                                                                 {
                                                                                  $group_no=$grp1['GROUP_NO'];
                                                                                 }
                                                                                }
                                                                                }
                                                                                ?>
                                                                            <input type="hidden" value="<?php echo $group_no+1;?>" class="form-control col-md-12 col-xs-12" placeholder="Group No" name="GROUP_NO">
                                                                            <div class="col-md-5 col-sm-5 col-xs-12">
                                                                                <div class="form-group">
                                                                                    <label for="name" class="pull-left control-label">Course</label>
                                                                                    <input type="text" value="<?=$_GET['name']?>" class="form-control col-md-12 col-xs-12" placeholder="Course Name" name="COURSE_NAME" readonly required>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-2 col-sm-2 col-xs-12">
                                                                                <div class="form-group">
                                                                                    <a style="margin-top: 29px;" class="btn btn-success" href="#" onclick="javascript:void window.open('dashboard_popup.php?op=<?=MD5('display_offer_course')?>&of_no=<?=$_GET['of_no1']?>&of_no1=<?=$_GET['of_no']?>&stu_no=<?=$_GET['stu_no']?>&sname=<?=$_GET['sname']?>&cust_no=<?=$_GET['cust_no']?>&AGENT_NO=<?=$_SESSION['AGENT_NO']?>&aname=<?=$_GET['aname']?>','147123445749','width=800,height=600,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=0,top=0');return false;">Select</a>
                                                                                </div>
                                                                            </div>
                                                                            <?php
                                                                                if($_GET['faculty'])
                                                                                {
                                                                                $cond="where `CRT_NO`=".$_GET['faculty'];
                                                                                $row=getRows($cond,'crse_type');
                                                                                }
                                                                                ?>
                                                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                                                <div class="form-group">
                                                                                    <label for="name" class="pull-left control-label">Faculty
                                                                                    </label>
                                                                                    <input type="text" value="<?=$row[0]['CRT_NAME']?>" placeholder="Faculty Name" class="date-picker form-control col-md-12 col-xs-12 active" name="FACULTY" readonly required>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                                                <div class="form-group">
                                                                                    <label for="name" class="pull-left control-label">Location
                                                                                    </label>
                                                                                    <input type="text" value="Sydney" class="form-control col-md-12 col-xs-12" placeholder="Location" name="LOCATION" readonly required>
                                                                                </div>
                                                                            </div>
                                                                            <div>&nbsp;</div>
                                                                            <div class="col-md-3 col-sm-3 col-xs-12">
                                                                                <div class="form-group">
                                                                                    <label for="name" class="pull-left control-label">Start
                                                                                    </label>
                                                                                    <?php $date2 = date( "j-M-Y") ?>
                                                                                    <input type="date" name="start_date" id="start_date" class="form-control col-md-12 col-xs-12" onchange="javascript: doCalcu(this.value);" required />
                                                                                </div>
                                                                            </div>
                                                                            <?php if($_GET['week_flag']==1){ ?>
                                                                            <div class="col-md-3 col-sm-3 col-xs-12">
                                                                                <div class="form-group">
                                                                                    <label for="name" class="pull-left control-label col-md-12 col-sm-12 col-xs-12" >Length
                                                                                    </label> 
                                                                                    <input type="number" name="length" id="length" class="form-control col-md-12 col-xs-12" readonly onchange="recalculateSum();" value="<?=$_GET['len']?>" required/>
                                                                            <input type="hidden" name="OPTION_NO" class="form-control col-md-12 col-xs-12" value="1"/>
                                                                           </div>
                                                                           </div> 
                                                                           <?php } else { ?>

                                                                           <div class="col-md-3 col-sm-3 col-xs-12">
                                                                                <div class="form-group">
                                                                                    <label for="name" class="pull-left control-label col-md-12 col-sm-12 col-xs-12" >Length
                                                                                    </label> 
                                                                                    <input type="number" name="length" id="length" class="form-control col-md-12 col-xs-12" onchange="recalculateSum();" value="<?=$_GET['len']?>" readonly required />
                                                                           </div>
                                                                           </div> 
                                                                           <?php }?>
                                                                              <?php   if(date('Y-m-d', strtotime($lastfriday2.'last friday'))!=date('Y-m-d', strtotime(' - 3days')))
                                                                              {
                                                                                ?>
                                                                            <div class="col-md-3 col-sm-3 col-xs-12">
                                                                                <div class="form-group">
                                                                                    <label for="name" class="pull-left control-label">Finish Date
                                                                                    </label>
                                                                                    <input type="text" value="<?php echo date('Y-m-d', strtotime($lastfriday2.'last friday'));?>" class="form-control col-md-12 col-xs-12" readonly id="finish_date">
                                                                                </div>
                                                                            </div>
                                                                               <?php 
                                                                              } ?>
                                                                            
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                                                        <div class="row">
                                                                            <div class="col-md-12 col-sm-12">
                                                                                <div class="row">
                                                                                    <div>&nbsp;</div>
                                                                                    <fieldset>
                                                                                        <h4>Item Description</h4>
                                                                                    </fieldset>
                                                                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                                                                        <div class="item form-group">
                                                                                            <label class="control-label col-md-12 col-sm-12 col-xs-12">
                                                                                            </label>
                                                                                            <div class="row">
                                                                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                                                                    <input type="text"value="<?=$_GET['code']?>-<?=$_GET['name']?>, (<?=$_GET['len']?> Weeks)"  class="date-picker form-control col-md-12 col-xs-12 active" name="DESCRIPTION" >
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                                                        <div class="row">
                                                                            <div class="col-md-12 col-sm-12">
                                                                                <div class="row">
                                                                                    <fieldset>
                                                                                        <h4>Custom Field</h4>
                                                                                    </fieldset>
                                                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                                                        <div class="item form-group">
                                                                                            <input type="text"  class="date-picker form-control active" name="CUSTOM_1" >
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                                                        <div class="form-group">
                                                                                            <input type="text" class="form-control"  name="CUSTOM_2" >
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                                                        <div class="form-group">
                                                                                            <input type="text" class="form-control " name="CUSTOM_3" >
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                                                        <div class="form-group">
                                                                                            <input type="text" class="form-control " name="CUSTOM_4" >
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                                                        <div class="form-group">
                                                                                            <input type="text" class="form-control " name="CUSTOM_5" >
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                                                        <div class="form-group">
                                                                                            <input type="text" class="form-control " name="CUSTOM_6" >
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                                                        <div class="form-group">
                                                                                            <input type="text" class="form-control " name="CUSTOM_7" >
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                                                        <div class="form-group">
                                                                                            <input type="text" class="form-control "  name="CUSTOM_8" >
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                                                        <div class="form-group">
                                                                                            <input type="text" class="form-control " name="CUSTOM_9" >
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                                                        <div class="form-group">
                                                                                            <input type="text" class="form-control " name="CUSTOM_10" >
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                                                        <div class="row">
                                                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                                                <div class="row">
                                                                                    <div class="col-md-12 col-sm-12">
                                                                                        <div class="row">
                                                                                            <fieldset id="group1">
                                                                                                <h4>Billing Rate</h4>
                                                                                            </fieldset>
                                                                                            <div class="row">
                                                                                                <div class="col-md-4 col-sm-4 col-xs-4">
                                                                                                    <div class="item form-group">
                                                                                                        <input type="radio" name="group1" style="width:20px; height:20px;">Default
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="col-md-4 col-sm-4 col-xs-4">
                                                                                                    <div class="item form-group">
                                                                                                        <input type="text" name="SUB_AMOUNT" id="cost" class="form-control col-md-12 col-xs-12" value="<?=$_GET['sub_amount']?>">
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="row">
                                                                                                <div class="col-md-4 col-sm-4 col-xs-4">
                                                                                                    <div class="item form-group">
                                                                                                        <input type="radio" name="group1" style="width:20px; height:20px;">Alternate
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="col-md-4 col-sm-4 col-xs-4">
                                                                                                    <div class="item form-group">
                                                                                                        <input type="text" name="ALTERNATE" class="form-control col-md-12 col-xs-12">
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="row">
                                                                                                <div class="col-md-4 col-sm-4 col-xs-4">
                                                                                                    <div class="item form-group">
                                                                                                        <input type="radio" name="group1" style="width:20px; height:20px;">Custom
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="col-md-4 col-sm-4 col-xs-4">
                                                                                                    <div class="item form-group">
                                                                                                        <input type="text" name="CUSTOM" class="form-control col-md-12 col-xs-12">
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                           
                                                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                                                <div class="row">
                                                                                    <div class="col-md-12 col-sm-12">
                                                                                        <div class="row">
                                                                                            <fieldset id="group2">
                                                                                                <h4>Agent Commission</h4>
                                                                                            </fieldset>
                                                                                            <div class="row">
                                                                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                                                                    <div class="item form-group">
                                                                                                        <label for="chkPassport">
                                                                                                        <input class="sccb" type="checkbox" value="<?= $_GET['commission'] ?>" name="offer_date" style="width:20px; height:20px;">Apply Agent Commission </label>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="row">
                                                                                                <div class="col-md-4 col-sm-4 col-xs-4">
                                                                                                    <div class="item form-group">
                                                                                                        <input onclick="document.getElementById('custom').disabled = false; document.getElementById('charstype').disabled = true;" class="cns" disabled="disabled" type="radio" name="group2" style="width:20px; height:20px;">Default Rate
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="col-md-4 col-sm-4 col-xs-4">
                                                                                                    <div class="item form-group">
                                                                                                        <select id="custom" class="form-control col-md-12 col-xs-12 cns2" disabled="disabled" name="RATE" onchange="commissionUser(this.value)">
                                                                                                            <option></option>
                                                                                                            <?php
                                                                                                                $sq="select * from agent_commission where AGENT_NO=".$_GET['AGENT_NO'];
                                                                                                                $qe=mysql_query($sq);
                                                                                                                while($ro=mysql_fetch_array($qe))
                                                                                                                {
                                                                                                                if(!$ro['DELETE_DATE']) {
                                                                                                                ?>
                                                                                                            <option value="<?=$ro["AGENT_COMMISSION_NO"]?>"><?=$ro['NAME']?></option>
                                                                                                            <?php echo "AMOUNT!@#$=".$ro['RATE_PERCENT'];?>
                                                                                                            <?php } } ?>
                                                                                                        </select>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="col-md-4 col-sm-4 col-xs-4">
                                                                                                    <div class="item form-group">
                                                                                                        <div id="txtHint"></div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="row">
                                                                                                <div class="col-md-4 col-sm-4 col-xs-4">
                                                                                                    <div class="item form-group">
                                                                                                        <input onclick="document.getElementById('custom').disabled = true; document.getElementById('charstype').disabled = false;" type="radio" name="group2" style="width:20px; height:20px;" class="cns4" disabled="disabled">Custom Rate
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="col-md-4 col-sm-4 col-xs-4">
                                                                                                    <div class="item form-group">
                                                                                                        <input id="charstype" disabled="disabled" type="text" name="CUSTOM_RATE" class="form-control col-md-12 col-xs-12 cns5">
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                                                        <div class="row">
                                                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                                                <fieldset>
                                                                                    <h4>Notes</h4>
                                                                                </fieldset>
                                                                                <div class="form-group">
                                                                                    <textarea class="form-control" name="NOTE" cols="60" rows="4"></textarea>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                                                <fieldset>
                                                                                    <h4>Billing Amount</h4>
                                                                                </fieldset>
                                                                                <div class="row">
                                                                                    <label for="name" class="pull-left control-label col-md-4 col-sm-4 col-xs-12">Amount
                                                                                    </label>
                                                                                    <div class="col-md-8 col-sm-8 col-xs-12">
                                                                                        <div class="form-group">
                                                                                            <input type="text" name="AMOUNT" id="amount" value="<?=$_GET['cost']?>"/>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="row">
                                                                                    <label for="name" class="pull-left control-label col-md-4 col-sm-4 col-xs-12">Discount
                                                                                    </label>
                                                                                    <div class="col-md-8 col-sm-8 col-xs-12">
                                                                                        <div class="row">
                                                                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                                                                <div class="item form-group">
                                                                                                    <select class="form-control col-md-3 col-sm-3 col-xs-6" id="operation"  name="DISCOUNT">
                                                                                                        <option value="1">$</option>
                                                                                                        <option value="2">%</option>
                                                                                                    </select>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                                                                <div class="item form-group">
                                                                                                    <input id="input2" type="text" name="DISCOUNT_RATE" class="form-control">
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="row">
                                                                                    <label for="name" class="pull-left control-label col-md-4 col-sm-4 col-xs-12">GST %
                                                                                    </label>
                                                                                    <div class="col-md-8 col-sm-8 col-xs-12">
                                                                                        <div class="row">
                                                                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                                                                <?php 
                                                                                                    $tax_query=mysql_query("SELECT * FROM gst_rate");
                                                                                                    $tax_row=mysql_fetch_array($tax_query);
                                                                                                    ?>
                                                                                                <div class="item form-group">
                                                                                                    <select class="form-control col-md-3 taxValueDrop" id="operation"  name="TaxValue">
                                                                                                        <option value="0">Exclude GST</option>
                                                                                                        <option value="<?=$tax_row['GST_RATE']?>">Include GST</option>
                                                                                                    </select>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                                                                <div class="item form-group">
                                                                                                    <input type="text" name="TAX_AMOUNT" onchange="recalculateSum();" id="TAX" class="form-control" readonly value="0" />
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="row">
                                                                                    <label for="name" class="pull-left control-label col-md-4 col-sm-4 col-xs-12">Total
                                                                                    </label>
                                                                                    <div class="col-md-8 col-sm-8 col-xs-12">
                                                                                        
                                                                                        <div class="item form-group">
                                                                                            <input type="text" name="TOTAL_AMOUNT" id="total_amount" value="<?=$_GET['cost']?>"/>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                   <!-- <div class="col-md-12 col-sm-12 col-xs-12">
                                                                        <div class="row">
                                                                            <div class="col-md-12 col-sm-12">
                                                                                <div class="row">
                                                                                    <div class="item form-group">
                                                                                        <input type="checkbox" name="RULES" style="width:20px; height:20px;">Run Fee Rules After Closing
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div> -->
                                                               
                                                                <!--Offer-->
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <center><input type="submit" name="sub" value="save" class="btn btn-success"></center>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
  function recalculateSum()
    { 
        var length = parseInt(document.getElementById("length").value);
        var sub_amount = parseInt(document.getElementById("sub_amount").value);
        document.getElementById("amount").value = length * sub_amount;
        document.getElementById("total_amount").value = length * sub_amount;

    }
    function doCalcu(val)
    { 
       var days = document.getElementById('length').value;
      
      var newfinishDate = new Date(new Date(val).getTime()+(days*24*60*60*1000));
      // 01, 02, 03, ... 29, 30, 31
   var dd = (newfinishDate.getDate() < 10 ? '0' : '') + newfinishDate.getDate();
   // 01, 02, 03, ... 10, 11, 12
   var MM = ((newfinishDate.getMonth() + 1) < 10 ? '0' : '') + (newfinishDate.getMonth() + 1);
   // 1970, 1971, ... 2015, 2016, ...
   var yyyy = newfinishDate.getFullYear();

   // create the format you want
   newfinishDateformat = (MM + "/" + dd + "/" + yyyy);
       // alert(newfinishDateformat);
      document.getElementById('finish_date').value = newfinishDateformat;

    }
</script>
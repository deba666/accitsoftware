<?php
    /*
        * @package    ACCIT
         * @author    Paperlink Softwares Team
         * @copyright    Copyright (c) 2016 , Paperlink Softwares Pvt. Ltd.  (http://www.paperlinksoftwares.com/)
         * @since    Version 1.0.0
         * @filesource
		 * Code for add new language module . link comes from Language Modules(Side Bar->Classes->Module classes).(Add New Module)
         */
    
       if($_POST['CLASS_NAME'])
       {
       	if(insert_record("insert into `lang_class_module` set `CLASS_NAME`='".$_POST['CLASS_NAME']."',`DATE`='".$_POST['DATE']."',`RECURRING`='".$_POST['RECURRING']."',`ST_TIME`='".$_POST['ST_TIME']."',`NO_END_DATE`='".$_POST['NO_END_DATE']."',`END_TIME`='".$_POST['END_TIME']."',`END_BY`='".$_POST['END_BY']."',`LEVEL_NO`='".$_POST['LEVEL_NO']."',`TEACHER_NO`='".$_POST['TEACHER_NO']."',`ROOM_NO`='".$_POST['ROOM_NO']."',`CAPACITY`='".$_POST['CAPACITY']."',`USER_NO`='".$_POST['user']."'"))
       			$_SESSION['s_msg']="<strong>Fine!</strong>New Language Classes Added Successfully";
       		else{
       			$_SESSION['e_msg']="<strong>Oh snap!</strong>Language Classes is not added";
       }}
       ?>
          <div class="right_col" role="main">
<div class="col-md-12 col-sm-12 col-xs-12">
    <div class="page-title">
        <div class="title_left">
            <?php 
                if($_SESSION['s_msg'] || $_SESSION['e_msg'])
                {
                ?>
            <div class="alert <?=($_SESSION['s_msg'])?'alert-success':'alert-error'?> fade in">
                <button data-dismiss="alert" class="close" type="button">�</button>
                <?=($_SESSION['s_msg'])?$_SESSION['s_msg']:$_SESSION['e_msg']?>
            </div>
            <?php
                unset($_SESSION['s_msg']);
                unset($_SESSION['e_msg']);
                }
                ?>
           <h3>Add New Class</h3>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="clearfix"></div>
            <div class="x_content">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel recent-app">
                            <div class="x_content">
                                <div class="student-account-page edit-faculty-page">
                                    <!-- Nav tabs -->
                                    <div class="card">
                                        <div class="all-students-list add student">
                                            <div class="add-student-section">
                                                <form method="post" action="" class="form-horizontal form-label-left">
                                                    <div class="item form-group">
                                                        <label for="name" class="control-label col-md-2 col-sm-2 col-xs-2">Class Name</label>
                                                        <div class="col-md-8 col-sm-8 col-xs-8">
                                                            <div class="row">
                                                                <div class="col-md-7 col-sm-7 col-xs-12">
                                                                    <input id="when"  name="user" type="hidden" value="<?php echo $_SESSION['user_no']?>">
                                                                    <input name="CLASS_NAME" class="form-control col-md-12 col-xs-12" type="text" required>  
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="item form-group">
                                                        <label for="name" class="control-label col-md-2 col-sm-2 col-xs-2">Date</label>
                                                        <div class="col-md-6 col-sm-6 col-xs-6">
                                                            <div class="row">
                                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                                    <?php date_default_timezone_set('Australia/Melbourne');
                                                                        $date = date('d/m/Y');
                                                                        ?>
                                                                    <input name="DATE" type="date" value="<?php echo $date?>" class="form-control col-md-6 col-xs-12" required>
                                                                </div>
                                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                                    <input id="date" name="RECURRING"  type="hidden" value="0">
                                                                    <input id="date" name="RECURRING"  type="checkbox" value="1">
                                                                    Recurring(Weekly)
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="item form-group">
                                                        <label for="name" class="control-label col-md-2 col-sm-2 col-xs-2">Start Time</label>
                                                        <div class="col-md-6 col-sm-6 col-xs-6">
                                                            <div class="row">
                                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                                    <?php date_default_timezone_set('Australia/Sydney');
                                                                        $date1 = date('h:i');
                                                                        ?>
                                                                    <input id="date" name="ST_TIME" class="date-picker form-control col-md-6 col-xs-6"  value="<?php echo $date1?>" type="time" required>
                                                                </div>
                                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                                    <input id="date" name="NO_END_DATE"  type="radio" value="1">No End Date
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="item form-group">
                                                        <label for="name" class="control-label col-md-2 col-sm-2 col-xs-2">End Time</label>
                                                        <div class="col-md-6 col-sm-6 col-xs-6">
                                                            <div class="row">
                                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                                    <?php date_default_timezone_set('Australia/Sydney');
                                                                        $date2 = date('h:i');
                                                                        ?>
                                                                    <input id="date" name="END_TIME" class="date-picker form-control col-md-6 col-xs-6"  value="<?php echo $date2?>" type="time">
                                                                </div>
                                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                                    <input id="date" name="NO_END_DATE"  type="radio" value="2">End By<input name="END_BY" value="<?php echo $date?>" class="form-control col-md-6 col-xs-12"  type="date" required>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="item form-group">
                                                        <label for="name" class="control-label col-md-2 col-sm-2 col-xs-2">Level</label>
                                                        <div class="col-md-6 col-sm-6 col-xs-6">
                                                            <div class="row">
                                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                                    <select name="LEVEL_NO" class="form-control col-md-6 col-xs-12" required>
                                                                        <option value="0">(Not Set)</option>
                                                                        <option value="1">1</option>
                                                                        <option value="2">2</option>
                                                                        <option value="3">3</option>
                                                                        <option value="4">4</option>
                                                                        <option value="5">5</option>
                                                                        <option value="6">6</option>
                                                                        <option value="7">7</option>
                                                                    </select>
                                                                    </select>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="item form-group">
                                                        <label for="name" class="control-label col-md-2 col-sm-2 col-xs-2">Room</label>
                                                        <div class="col-md-6 col-sm-6 col-xs-6">
                                                            <div class="row">
                                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                                    <select name="ROOM_NO" class="form-control col-md-6 col-xs-12" required>
                                                                        <option value="0">(Not Set)</option>
                                                                        <?php
                                                                            $sql="select * from room";
                                                                             $q=mysql_query($sql);
                                                                             while($row=mysql_fetch_array($q))
                                                                            	{
                                                                            ?>
                                                                        <option value="<?=$row['ROOM_NO']?>"><?=$row['ROOM_DESC']?></option>
                                                                        <?php } ?>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="item form-group">
                                                        <label for="name" class="control-label col-md-2 col-sm-2 col-xs-2">Teacher</label>
                                                        <div class="col-md-6 col-sm-6 col-xs-6">
                                                            <div class="row">
                                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                                    <select name="TEACHER_NO" class="form-control col-md-6 col-xs-12" required>
                                                                        <option value="0"></option>
                                                                        <?php
                                                                            $sql1="select * from teacher";
                                                                             $q1=mysql_query($sql1);
                                                                             while($row1=mysql_fetch_array($q1))
                                                                            	{
                                                                            ?>
                                                                        <option value="<?=$row1['TEACHER_NO']?>"><?=$row1['TEACHER_CODE']?></option>
                                                                        <?php } ?>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="item form-group">
                                                        <label for="name" class="control-label col-md-2 col-sm-2 col-xs-2">Capacity</label>
                                                        <div class="col-md-6 col-sm-6 col-xs-6">
                                                            <div class="row">
                                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                                    <input type="number" step="1" min="1" max="" placeholder="(Unlimited)" name="CAPACITY" class="form-control col-md-6 col-xs-12" required>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="item form-group">
                                                        <label for="name" class="control-label col-md-3 col-sm-3 col-xs-12"></label>
                                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                                            <div class="row">
                                                                <div class="col-md-6 col-sm-4 col-xs-12">
                                                                    <p class="pull-left">
                                                                        <button class="btn btn-primary" type="submit">Cancel</button>
                                                                        <button class="btn btn-success" type="submit">Submit</button>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
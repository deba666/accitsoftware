<?php
error_reporting(0);
session_start();
include("settings.php");
if(isset($_SESSION['user_no']) && $_SESSION['user_no']!='')
{
    
	header("location:dashboard.php");
        exit();
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Login</title>
        <!-- Bootstrap -->
        <link href="./vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <!-- Font Awesome -->
        <link href="./vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <!-- iCheck -->
        <link href="./vendors/iCheck/skins/flat/green.css" rel="stylesheet">
        <!-- bootstrap-progressbar -->
        <link href="./vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
        <!-- jVectorMap -->
        <link href="css/maps/jquery-jvectormap-2.0.3.css" rel="stylesheet"/>
        <!-- Custom Theme Style -->
        <link href="css/custom.css" rel="stylesheet">
        <!-- Hover Animation Style -->
        <link href="css/hover.css" rel="stylesheet">
        <!-- Responsive Style -->
        <link href="css/responsive.css" rel="stylesheet">

        <!--Google Font-->
        <link href='https://fonts.googleapis.com/css?family=Raleway:400,100,100italic,200,200italic,300,300italic,500,400italic,500italic,600italic,600,700,700italic,800,800italic,900,900italic' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Lato:400,100italic,100,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
        
    </head>
    <body class="nav-md">
        <div class="container body login-bg">
            <div class="main_container login-page">
              <div class="login_page_wrapper">
                <div class="login-top-section">
                  <div class="logo">
                    <img src="images/logo.png">
                  </div>
                  <div class="heading">
                    <h2>Account Login</h2>
                    <p>Please enter your username and password to login</p>
                  </div>
                  <?php if($_SESSION['msg']){ ?>
                  <div class="alert alert-success" role="alert">
                                   <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                   <strong>Oops! </strong>You are logged out.
                                 </div>
                                 <?php session_unset('msg'); } ?>
                </div>
                <div class="md-card" id="login_card">
                  <div class="md-card-content large-padding" id="login_form">
                    <div class="login_heading">
                        <div class="user_avatar"></div>
                    </div>
                    <form  id="frmLogin" name="form1" method="post" action="check_log.php" onSubmit="return check_details();">
                        <div class="uk-form-row">
                            <div class="md-input-wrapper"><input class="md-input" type="text" name="username" id="username" placeholder="Username" required><span class="md-input-bar "></span></div>
                            
                        </div>
                        <div class="uk-form-row">
                            <div class="md-input-wrapper"><input class="md-input" type="password" name="password" id="password" placeholder="Password" required><span class="md-input-bar "></span></div>
                            
                        </div>
                        
                        <div class="uk-margin-medium-top">
                            <button type="submit" name="login" class="md-btn md-btn-primary md-btn-block md-btn-large">Sign In</button>
                        </div>
                        <div class="uk-margin-top">
                          <a href="#" id="login_help_show" class="uk-float-right">Forgot Password ?</a>
                          <span class="icheck-inline">
                              <div class="icheckbox_md">
                                <input type="checkbox" name="login_page_stay_signed" id="login_page_stay_signed"></div>
                              <label for="login_page_stay_signed" class="inline-label">Stay signed in</label>
                          </span>
                        </div>

                        <div class="clearfix"></div>

                        <div class="uk-margin-top">
                          <p class="login-ft">© Copyright 2016 <a href="javascript:void(0);">ACCIT</a>. All Rights Reserved.</p>
                        </div>
                      </form>
                  </div>
                </div>
              </div>
            </div>
        </div>
        <!-- jQuery -->
        <script src="./vendors/jquery/dist/jquery.min.js"></script>
        <!-- Bootstrap -->
        <script src="./vendors/bootstrap/dist/js/bootstrap.min.js"></script>
        <!-- FastClick -->
        <script src="./vendors/fastclick/lib/fastclick.js"></script>
        <!-- bootstrap-progressbar -->
        <script src="./vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
        <!-- iCheck -->
        <script src="./vendors/iCheck/icheck.min.js"></script>
        <!-- Skycons -->
        <script src="./vendors/skycons/skycons.js"></script>
        <!--NiceScroll-->
        <script src="./vendors/nicescroll/jquery.nicescroll.js"></script>
        <!-- Custom Theme Scripts -->
        <script src="js/custom.js"></script>
        <!-- Skycons -->
        <script>
            $(document).ready(function() {
              var icons = new Skycons({
                  "color": "#73879C"
                }),
                list = [
                  "clear-day", "clear-night", "partly-cloudy-day",
                  "partly-cloudy-night", "cloudy", "rain", "sleet", "snow", "wind",
                  "fog"
                ],
                i;
            
              for (i = list.length; i--;)
                icons.set(list[i], list[i]);
            
              icons.play();
            });
        </script>
        <!-- /Skycons -->
    </body>
</html>
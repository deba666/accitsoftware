<?php
    /*
        * @package    ACCIT
         * @author    Paperlink Softwares Team
         * @copyright    Copyright (c) 2016 , Paperlink Softwares Pvt. Ltd.  (http://www.paperlinksoftwares.com/)
         * @since    Version 1.0.0
         * @filesource
		 * Code for edit the course module . Links come from course module page
         */
    
    if($_POST['SUBJ_CODE'])
    	{
    	if(insert_record("update `subject` set `SUBJ_CODE`='".$_POST['SUBJ_CODE']."',`HOURS`='".$_POST['HOURS']."',`WEEKS`='',`STATUS`='".$_POST['STATUS']."',`WHO_DATE`='".$_POST['who']."',`USER_NO`='".$_POST['user']."',`SUBJ_TYPE`='2', `SUBJ_DESC`='".$_POST['SUBJ_DESC']."',`FEE_AMOUNT`='0',`FEE_PER_MODULE`='0',`PARENT_NO`='0',`TYPE_NO`='1',`CREATE_DATE`='".$_POST['who']."',`DELETE_DATE`='',`LOCK_NUM`='1',`VET_FLAG`='".$_POST['VET_FLAG']."',`VET_CODE`='".$_POST['VET_CODE']."',`VET_NAME`='".$_POST['SUBJ_DESC']."',`VET_DELIVERY_MODE_NO`='".$_POST['VET_DELIVERY_MODE_NO']."',`VET_COMPETENCY_NO`='".$_POST['VET_COMPETENCY_NO']."',`VET_FIELD_NO`='".$_POST['VET_FIELD_NO']."',`VET_NATIONAL_CODE`='',`RESOURCE_FEE`='".$_POST['RESOURCE_FEE']."' WHERE `SUBJECT_NO`=".$_GET['edit']))  
    		$_SESSION['s_msg']="<strong>Fine!</strong> Student Subject Update";
    		else
    			$_SESSION['e_msg']="<strong>Oh snap!</strong> Student subject is not Update";
    	}
    
    if($_GET['edit'])
    {
    	$cond="where `SUBJECT_NO`=".$_GET['edit'];
    	$sub_row=getRows($cond,'subject');
    }
    
    ?>
<div class="right_col" role="main">
    <div class="">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="page-title">
                <div class="title_left">
                    <?php 
                        if($_SESSION['s_msg'] || $_SESSION['e_msg'])
                        {
                        ?>
                    <div class="alert <?=($_SESSION['s_msg'])?'alert-success':'alert-error'?> fade in">
                        <button data-dismiss="alert" class="close" type="button">×</button>
                        <?=($_SESSION['s_msg'])?$_SESSION['s_msg']:$_SESSION['e_msg']?>
                    </div>
                    <?php
                        unset($_SESSION['s_msg']);
                        unset($_SESSION['e_msg']);
                        }
                        ?>
                    <h3>Edit Course Module</h3>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="clearfix"></div>
                    <div class="x_content">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel recent-app">
                                    <div class="x_content">
                                        <div class="student-account-page">
                                            <!-- Nav tabs -->
                                            <div class="card">
                                                <div class="all-students-list add-course">
                                                    <div class="add-student-section">
                                                        <form enctype="multipart/form-data" method="post" action="" class="form-horizontal form-label-left">
                                                            <?php date_default_timezone_set('Australia/Sydney');
                                                                $date = date('d/m/Y h:i:s a', time());
                                                                ?>
                                                            <input id="who"  name="who"  type="hidden" value="<?php echo $date?>">
                                                            <input id="when"  name="user" type="hidden" value="<?php echo $_SESSION['user_no']?>">
                                                            <div class="item form-group">
                                                                <label for="name" class="control-label col-md-3 col-sm-3 col-xs-12">Code</label>
                                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                                    <div class="row">
                                                                        <div class="col-md-3 col-sm-4 col-xs-12">
                                                                            <input type="text" id="code" class="form-control col-md-12 col-xs-12" name="SUBJ_CODE" value="<?=$sub_row[0]['SUBJ_CODE']?>" required>
                                                                        </div>
                                                                        <div class="col-md-3 col-sm-4 col-xs-12">
                                                                            <label class="pull-left"> Status:<?=(!$sub_row[0]['STATUS'])?'Inactive':'Active'?></label>
                                                                        </div>
                                                                        <div class="col-md-2 col-sm-4 col-xs-12">
                                                                           <input type="hidden" name="STATUS" value="0">
                                                                            <input type="checkbox" style="width:20px;height:20px;" name="STATUS" value="1" <?php echo ($sub_row[0]['STATUS'] ? 'checked' : '');?>/>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="item form-group">
                                                                <label for="name" class="control-label col-md-3 col-sm-3 col-xs-12">Name</label>
                                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                                    <div class="row">
                                                                        <div class="col-md-7 col-sm-7 col-xs-12">
                                                                            <input type="text" value="<?=$sub_row[0]['SUBJ_DESC']?>" name="SUBJ_DESC" class="form-control col-md-12 col-xs-12" id="name" required>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="item form-group">
                                                                <label for="name" class="control-label col-md-3 col-sm-3 col-xs-12">Total Hours/lebel</label>
                                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                                    <div class="row">
                                                                        <div class="col-md-4 col-sm-4 col-xs-12">
                                                                            <input type="number" size="4" class="form-control input-text qty text" title="hours" value="<?=$sub_row[0]['HOURS']?>" id="quantity" name="HOURS" max="" min="1" step="1" required>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="item form-group">
                                                                <label for="name" class="control-label col-md-3 col-sm-3 col-xs-12">Subject ID</label>
                                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                                    <div class="row">
                                                                        <div class="col-md-7 col-sm-7 col-xs-12">
                                                                            <input type="text" id="sub_id" class="form-control col-md-12 col-xs-12" name="VET_CODE" value="<?=$sub_row[0]['VET_CODE']?>" required>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="item form-group">
                                                                <label class="control-label col-md-5 col-sm-3 col-xs-12" for="name">The Intension of this Module vocational or pre-vocational:</label>
                                                                <div class="col-md-1 col-sm-9 col-xs-12">
                                                                    <div class="row">
                                                                        <div class="col-md-7 col-sm-7 col-xs-12">
                                                                         <input type="hidden" value="0" id="std_curr_attend" name="VET_FLAG" >
                                                                        <input type="checkbox" style="width:20px;height:20px;" name="VET_FLAG" value="1" <?php echo ($sub_row[0]['VET_FLAG'] ? 'checked' : '');?>/>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="item form-group">
                                                                <label class="control-label col-md-5 col-sm-3 col-xs-12" for="name">This is the part of a VET school program:</label>
                                                                <div class="col-md-1 col-sm-9 col-xs-12">
                                                                    <div class="row">
                                                                        <div class="col-md-7 col-sm-7 col-xs-12">
                                                                           <input type="hidden" value="0" id="std_curr_attend" name="RESOURCE_FEE" >
                                                                         <input type="checkbox" style="width:20px;height:20px;" name="RESOURCE_FEE" value="1" <?php echo ($sub_row[0]['RESOURCE_FEE'] ? 'checked' : '');?>/>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="item form-group">
                                                                <label class="control-label col-md-5 col-sm-3 col-xs-12" for="name">This is a unit of competency in a national training package:</label>
                                                                <div class="col-md-1 col-sm-9 col-xs-12">
                                                                    <div class="row">
                                                                        <div class="col-md-7 col-sm-7 col-xs-12">
                                                                            <input type="hidden" value="5221" id="std_curr_attend" name="VET_COMPETENCY_NO" >
                                                                         <input type="checkbox" style="width:20px;height:20px;" name="VET_COMPETENCY_NO" value="5220" <?php echo ($sub_row[0]['VET_COMPETENCY_NO'] ? 'checked' : '');?>/>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="item form-group">
                                                                <label for="name" class="control-label col-md-3 col-sm-3 col-xs-12">Delivery Mode:</label>
                                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                                    <div class="row">
                                                                        <div class="col-md-7 col-sm-7 col-xs-12">
                                                                            <select name="VET_DELIVERY_MODE_NO" class="col-md-12 col-xs-12 form-control" required>
																				<option Value="0"></option>
																				<?php
																					$sql304="select * from system_code where `SYSTEM_TYPE_NO`=".'219';
																					$dd_res304=mysql_query("select * from system_code where `SYSTEM_TYPE_NO`=".'219 LIMIT 5');
																		   while($r304=mysql_fetch_array($dd_res304))
																			  { 
																			   if($sub_row[0]['VET_DELIVERY_MODE_NO']==$r304['SYSTEM_CODE_NO'])
																				 {
																				   echo "<option value='$r304[SYSTEM_CODE_NO]' selected> $r304[NAME] </option>";
																				   }
																					else
																					{
																					 echo "<option value='$r304[SYSTEM_CODE_NO]'> $r304[NAME] </option>";
																					  }
																				}
												                                             ?>
                                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="item form-group">
                                                                <label for="name" class="control-label col-md-3 col-sm-3 col-xs-12">Field of Education:</label>
                                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                                    <div class="row">
                                                                        <div class="col-md-7 col-sm-7 col-xs-12">
                                                                            <select name="VET_FIELD_NO" class="col-md-12 col-xs-12 form-control" required>
																				<option Value="0"></option>
																				<?php
																					$sql304="select * from system_code where `SYSTEM_TYPE_NO`=".'212';
																					$dd_res304=mysql_query("select * from system_code where `SYSTEM_TYPE_NO`=".'212');
																		   while($r304=mysql_fetch_array($dd_res304))
																			  { 
																			   if($sub_row[0]['VET_FIELD_NO']==$r304['SYSTEM_CODE_NO'])
																				 {
																				   echo "<option value='$r304[SYSTEM_CODE_NO]' selected> $r304[NAME] </option>";
																				   }
																					else
																					{
																					 echo "<option value='$r304[SYSTEM_CODE_NO]'> $r304[NAME] </option>";
																					  }
																				}
												                                             ?>
                                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="item form-group">
                                                                <label for="name" class="control-label col-md-3 col-sm-3 col-xs-12"></label>
                                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                                    <div class="row">
                                                                        <div class="col-md-6 col-sm-4 col-xs-12">
                                                                            <p class="pull-left"><br/>
                                                                                <button class="btn btn-primary" value="Reset" type="reset">Cancel</button>
                                                                                <button class="btn btn-success" type="submit" onclick="needToConfirm = false;">Submit</button>
                                                                            </p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
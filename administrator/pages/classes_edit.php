<?php
    /*
        * @package    ACCIT
         * @author    Paperlink Softwares Team
         * @copyright    Copyright (c) 2016 , Paperlink Softwares Pvt. Ltd.  (http://www.paperlinksoftwares.com/)
         * @since    Version 1.0.0
         * @filesource
         */
     
    if($_POST['code']){
    	   if(mysql_query("UPDATE `lang_class` SET `LANG_CLASS_CODE`='".$_POST['code']."', `SESSION_NO`='".$_POST['session']."',`LEVEL_NO`='".$_POST['level']."', `ROOM_NO`='".$_POST['class_room']."' WHERE `LANG_CLASS_NO`=".$_GET['cid']))
    		$_SESSION['s_msg']="<strong>Well done!</strong> Item successfully updated";
    	else
    		$_SESSION['e_msg']="<strong>Oh snap!</strong> Item is not updated";
    
    }
    
    if($_GET['cid'])
    {
    	$cond="where `LANG_CLASS_NO`=".$_GET['cid'];
    	$std_row=getRows($cond,'lang_class');
    }?>
<div class="right_col" role="main">
    <div class="">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="page-title">
                <div class="title_left">
                    <?php 
                        if($_SESSION['s_msg'] || $_SESSION['e_msg'])
                        {
                        ?>
                    <div class="alert <?=($_SESSION['s_msg'])?'alert-success':'alert-error'?> fade in">
                        <button data-dismiss="alert" class="close" type="button">�</button>
                        <?=($_SESSION['s_msg'])?$_SESSION['s_msg']:$_SESSION['e_msg']?>
                    </div>
                    <?php
                        unset($_SESSION['s_msg']);
                        unset($_SESSION['e_msg']);
                        }
                        ?>
                    <h3>Class Modify</h3>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="clearfix"></div>
                    <div class="x_content">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel recent-app">
                                    <div class="x_content">
                                        <div class="student-account-page">
                                            <!-- Nav tabs -->
                                            <div class="card">
                                                <div class="all-students-list add-course">
                                                    <div class="add-student-section">
                                                        <form class="form-horizontal form-label-left" action="" method="POST" novalidate>
                                                            <button type="submit" class="btn btn-success" onclick="needToConfirm = false;">Save</button>
                                                            <ul id="student-acc-tab" class="nav nav-tabs" role="tablist">
                                                                <li role="presentation"  class="active"><a href="#details" aria-controls="details" role="tab" data-toggle="tab">Details</a></li>
                                                            </ul>
                                                            <div class="tab-content">
                                                                <!--Details-->
                                                                <div role="tabpanel" class="tab-pane active" id="details">
                                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                                        <div class="row">
                                                                            <h4>
                                                                                <fieldset>Basic Info</fieldset>
                                                                            </h4>
                                                                            <div class="clearfix"></div>
                                                                            <div class="table-responsive">
                                                                                <table class="table table-striped jambo_table">
                                                                                    <tbody>
                                                                                        <tr class="even pointer">
                                                                                            <td class="item">Name</td>
                                                                                            <td class=""><input type="text" value="<?=$std_row[0]['LANG_CLASS_CODE']?>" name="code"></td>
                                                                                        </tr>
                                                                                        <tr class="even pointer">
                                                                                            <td class="item">Session</td>
                                                                                            <td class="">
                                                                                                <select id="session" name="session" class="form-control">
                                                                                                    <option value="1" <?php if ($std_row[0]['SESSION_NO'] == '1') { echo 'selected="selected"';}?>>Session1</option>
                                                                                                    <option value="2" <?php if ($std_row[0]['SESSION_NO'] == '2') { echo 'selected="selected"';}?>>Session2</option>
                                                                                                    <option value="3" <?php if ($std_row[0]['SESSION_NO'] == '3') { echo 'selected="selected"';}?>>Session3</option>
                                                                                                    <option value="4" <?php if ($std_row[0]['SESSION_NO'] == '4') { echo 'selected="selected"';}?>>Session4</option>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr class="even pointer">
                                                                                        <td class="item">Level</td>
                                                                                        <td class=""> <select id="level" name="level" class="form-control">
                                                                                        <option value="1" <?php if ($std_row[0]['LEVEL_NO'] == '1') { echo 'selected="selected"';}?>>1</option>
                                                                                        <option value="2" <?php if ($std_row[0]['LEVEL_NO'] == '2') { echo 'selected="selected"';}?>>2</option>
                                                                                        <option value="3" <?php if ($std_row[0]['LEVEL_NO'] == '3') { echo 'selected="selected"';}?>>3</option>
                                                                                        <option value="4" <?php if ($std_row[0]['LEVEL_NO'] == '4') { echo 'selected="selected"';}?>>4</option>
                                                                                        <option value="5" <?php if ($std_row[0]['LEVEL_NO'] == '5') { echo 'selected="selected"';}?>>5</option>
                                                                                        <option value="6" <?php if ($std_row[0]['LEVEL_NO'] == '6') { echo 'selected="selected"';}?>>6</option>
                                                                                        <option value="7" <?php if ($std_row[0]['LEVEL_NO'] == '7') { echo 'selected="selected"';}?>>7</option>
                                                                                        </td>
                                                                                        </tr>  
                                                                                        <tr class="even pointer">
                                                                                        <td class="item">Room</td>
                                                                                        <td class="">
                                                                                        <select name="class_room" class="form-control"> 
                                                                                        <option Value="0">Select Agent</option>
                                                                                        <?php
                                                                                            $dd_res=mysql_query("Select *  from room");
                                                                                            while($r=mysql_fetch_array($dd_res))
                                                                                              { 
                                                                                            if($std_row[0][ROOM_NO]==$r[ROOM_NO])
                                                                                            	  {
                                                                                                 echo "<option value='$r[ROOM_NO]' selected> $r[ROOM_DESC] </option>";
                                                                                                }
                                                                                            	else
                                                                                            	  {
                                                                                            		echo "<option value='$r[ROOM_NO]'> $r[ROOM_DESC] </option>";
                                                                                            	  }
                                                                                              }
                                                                                            
                                                                                               ?>
                                                                                        </select>
                                                                                        </td>
                                                                                        </tr> 
                                                                                    </tbody>
                                                                                </table>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                        </form>
                                                        </div>
                                                        <!--notes-->
                                                        <div role="tabpanel" class="tab-pane" id="notes">
                                                        </div>
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
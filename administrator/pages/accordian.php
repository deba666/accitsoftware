<?php
    /*
        * @package    ACCIT
         * @author    Paperlink Softwares Team
         * @copyright    Copyright (c) 2016 , Paperlink Softwares Pvt. Ltd.  (http://www.paperlinksoftwares.com/)
         * @since    Version 1.0.0
         * @filesource
         */
    
    
    if($_GET['eid'])
    {
    	$cond="where `ENROL_NO`=".$_GET['eid'];
    	$std_row=getRows($cond,'enrol');
    
    	$cond1="where `COURSE_NO`=".$std_row[0]['COURSE_NO'];
    	$std_row1=getRows($cond1,'course');
    
    
    	$cond2="where `LOCATION_NO`=".$std_row[0]['LOCATION_NO'];
    	$std_row2=getRows($cond2,'location');
    
    	$cond3="where `AGENT_NO`=".$std_row[0]['AGENT_NO'];
    	$std_row3=getRows($cond3,'agent');
    
    	$cond4="where `USER_NO`=".$std_row[0]['USER_NO'];
    	$std_row4=getRows($cond4,'users'); 
    
    	$cond6="where `STUD_NO`=".$std_row[0]['STUD_NO'];
    	$std_row6=getRows($cond6,'student'); 
    
    	if($_GET['del2'])
        {
        	if(delete_record("DELETE FROM `diary` WHERE `DIARY_NO` = ".$_GET['del2']))
        		$_SESSION['s_msg']="Item successfully deleted";
        }
    	if($_GET['del3'])
        {
        	if(delete_record("DELETE FROM `enrol_holiday` WHERE `ENROL_HOLIDAY_NO` = ".$_GET['del3']))
        		$_SESSION['s_msg']="Item successfully deleted";
        }
    	if($_GET['del4'])
        {
        	if(delete_record("DELETE FROM `absence` WHERE `ABSENCE_NO` = ".$_GET['del4']))
        		$_SESSION['s_msg']="Item successfully deleted";
        }
    
    	
    }
    ?>
<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <a href="#" onclick="javascript:void window.open('dashboard.php?op=<?=MD5('receipt_details')?>&stu_no=<?=$std_row[0]['STUD_NO']?>&sname=<?=$std_row[0]['LNAME']?>&cust_no=<?=$std_row[0]['CUSTOMER_NO']?>&cun_no=<?=$std_row[0]['COUNTRY_NO']?>','1470138621367','width=700,height=500,toolbar=0,menubar=0,location=1,status=1,scrollbars=1,resizable=1,left=0,top=0');return false;">Receipt</a>
                <a href="#" onclick="javascript:void window.open('dashboard.php?op=<?=MD5('debit')?>&stu_no=<?=$std_row[0]['STUD_NO']?>&sname=<?=$std_row[0]['LNAME']?>&cust_no=<?=$std_row[0]['CUSTOMER_NO']?>&cun_no=<?=$std_row[0]['COUNTRY_NO']?>','1470138621367','width=1200,height=800,toolbar=0,menubar=0,location=1,status=1,scrollbars=1,resizable=1,left=0,top=0');return false;">&nbsp;&nbsp;&nbsp;&nbsp;Debit</a>
                <a href="#" onclick="javascript:void window.open('dashboard.php?op=<?=MD5('credit')?>&stu_no=<?=$std_row[0]['STUD_NO']?>&sname=<?=$std_row[0]['LNAME']?>&cust_no=<?=$std_row[0]['CUSTOMER_NO']?>&cun_no=<?=$std_row[0]['COUNTRY_NO']?>','1470138621367','width=1200,height=800,toolbar=0,menubar=0,location=1,status=1,scrollbars=1,resizable=1,left=0,top=0');return false;">&nbsp;&nbsp;&nbsp;&nbsp;Credit</a>
                <a href="#" onclick="javascript:void window.open('dashboard.php?op=<?=MD5('new_invoice')?>&stu_no=<?=$std_row[0]['STUD_NO']?>&sname=<?=$std_row[0]['LNAME']?>&cust_no=<?=$std_row[0]['CUSTOMER_NO']?>&cun_no=<?=$std_row[0]['COUNTRY_NO']?>','1470138621367','width=1200,height=800,toolbar=0,menubar=0,location=1,status=1,scrollbars=1,resizable=1,left=0,top=0');return false;">&nbsp;&nbsp;&nbsp;&nbsp;New Invoice</a>
            </div>
        </div>
        <ul role="tablist" class="nav nav-tabs" id="student-acc-tab">
            <li role="presentation"  class="active"><a href="#instalment" aria-controls="instalment" role="tab" data-toggle="tab">Invoice/Instalment</a></li>
            <li role="presentation"><a href="#transaction" aria-controls="transaction" role="tab" data-toggle="tab">Transaction </a></li>
        </ul>
        <div role="tabpanel" class="tab-pane active" id="instalment">
            <div class="clearfix"></div>
            <div class="table-responsive">
                <table class="table table-striped jambo_table bulk_action">
                    <thead>
                        <tr class="headings">
                            <th class="column-title">Invoice No</th>
                            <th class="column-title">Date</th>
                            <th class="column-title">Who
                            <th class="column-title">Due Date
                            </th>
                            <th class="column-title">Reference
                            </th>
                            <th class="column-title">Amount
                            </th>
                            <th class="column-title">Tax
                            </th>
                            <th class="column-title">Received
                            </th>
                            <th class="column-title">Credit
                            </th>
                            <th class="column-title">Debit
                            </th>
                            <th class="column-title">Balance
                            </th>
                            <th class="bulk-actions" colspan="7">
                                <a class="antoo" style="color:#fff; font-weight:500;">Action( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
                <div class="row accordian">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <?php
                            $cond20="where `ENROL_NO`=".$std_row[0]['ENROL_NO'];
                            $std_row20=getRows($cond20,'invoice');
                            foreach($std_row20 as $std_row21)
                            {						  
                             ?>
                        <div class="x_panel ">
                            <div class="x_title">
                                <ul class="nav navbar-left panel_toolbox">
                                    <td><a class="collapse-link"><i class="fa fa-chevron-up"></i>Invoice<?= $std_row21['INVOICE_NO']?>&nbsp;&nbsp;Due<?= $std_row21['DUE_DATE']?></br> </a>
                                    </td>
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">
                                <div class="row">
                                    <div class="col-md-12 col-sm-12">
                                        <div class="table-responsive">
                                            <table class="table table-striped jambo_table bulk_action">
                                                <thead>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                        $cond23="where `INVOICE_NO`=".$std_row21['INVOICE_NO'];
                                                        $std_row23=getRows($cond23,'invoice_details');
                                                           foreach($std_row23 as $std_row24)
                                                         { ?>
                                                    <tr class="even pointer">
                                                        <td class=" "><?= $std_row21['INVOICE_NO']?></td>
                                                        <td class=" "><?= $std_row21['INVOICE_DATE']?></td>
                                                        <?php $cond22="where `USER_NO`=".$std_row21['USER_NO'];
                                                            $std_row22=getRows($cond22,'users');?>
                                                        <td class=" "><?= $std_row22[0]['INIT']?></td>
                                                        <td class=" "><?= $std_row21['DUE_DATE']?></td>
                                                        <?php
                                                            $cond25="where `FEE_DEF_NO`=".$std_row24['FEE_DEF_NO'];
                                                            $std_row25=getRows($cond25,'fee_defn');?> 
                                                        <td class=" "><?= $std_row25[0]['FEE_DEF_NAME']?></td>
                                                        <td class=" "><?= $std_row24['AMOUNT']?></td>
                                                        <td class=" "><?= $std_row24['TAX_AMOUNT']?></td>
                                                        <?php $cond26="where `IND_NO`=".$std_row24['IND_NO'];
                                                            $std_row26=getRows($cond26,'debit_item');?> 
                                                        <td class=" "><?= $std_row26[0]['AMOUNT']?></td>
                                                        <?php $cond27="where `IND_NO`=".$std_row24['IND_NO'];
                                                            $std_row27=getRows($cond27,'credit');?> 
                                                        <?php 
                                                            $credit=$std_row24['AMOUNT']-$std_row26[0]['AMOUNT'];
                                                            ?>
                                                        <td class=" "><?php echo $credit;?></td>
                                                        <td class=" "><?= $std_row26[0]['GROSS_AMOUNT']?></td>
                                                        <?php 
                                                            $balance=$std_row24['AMOUNT']-($std_row26[0]['AMOUNT']+$credit);
                                                            ?>
                                                        <td class=" "><?php echo $balance;?></td>
                                                        <td class=" "><?php echo $balance;?></td>
                                                    </tr>
                                                    <?php }?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php  }  ?>
                    </div>
                </div>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane active" id="transaction">
            <div class="clearfix"></div>
            <div class="table-responsive">
                <table class="table table-striped jambo_table bulk_action">
                    <thead>
                        <tr class="headings">
                            <th class="column-title">Invoice Date</th>
                            <th class="column-title">Date</th>
                            <th class="column-title">Who
                            <th class="column-title">Due Date
                            </th>
                            <th class="column-title">Reference
                            </th>
                            <th class="column-title">Amount
                            </th>
                            <th class="column-title">Tax
                            </th>
                            <th class="column-title">Received
                            </th>
                            <th class="column-title">Credit
                            </th>
                            <th class="column-title">Debit
                            </th>
                            <th class="column-title">Balance
                            </th>
                            <th class="bulk-actions" colspan="7">
                                <a class="antoo" style="color:#fff; font-weight:500;">Action( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
                <div class="row accordian">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <?php
                            $cond20="where `ENROL_NO`=".$std_row[0]['ENROL_NO'];
                            $std_row20=getRows($cond20,'invoice');
                            foreach($std_row20 as $std_row21)
                            {						  
                             ?>
                        <div class="x_panel ">
                            <div class="x_title">
                                <ul class="nav navbar-left panel_toolbox">
                                    <td><a class="collapse-link"><i class="fa fa-chevron-up"></i>Invoice<?= $std_row21['INVOICE_NO']?>&nbsp;&nbsp;Due<?= $std_row21['DUE_DATE']?></br> </a>
                                    </td>
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">
                                <div class="row">
                                    <div class="col-md-12 col-sm-12">
                                        <div class="table-responsive">
                                            <table class="table table-striped jambo_table bulk_action">
                                                <thead>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                        $cond23="where `INVOICE_NO`=".$std_row21['INVOICE_NO'];
                                                        $std_row23=getRows($cond23,'invoice_details');
                                                           foreach($std_row23 as $std_row24)
                                                         { ?>
                                                    <tr class="even pointer">
                                                        <td class=" "><?= $std_row21['INVOICE_NO']?></td>
                                                        <td class=" "><?= $std_row21['INVOICE_DATE']?></td>
                                                        <?php $cond22="where `USER_NO`=".$std_row21['USER_NO'];
                                                            $std_row22=getRows($cond22,'users');?>
                                                        <td class=" "><?= $std_row22[0]['INIT']?></td>
                                                        <td class=" "><?= $std_row21['DUE_DATE']?></td>
                                                        <?php
                                                            $cond25="where `FEE_DEF_NO`=".$std_row24['FEE_DEF_NO'];
                                                            $std_row25=getRows($cond25,'fee_defn');?> 
                                                        <td class=" "><?= $std_row25[0]['FEE_DEF_NAME']?></td>
                                                        <td class=" "><?= $std_row24['AMOUNT']?></td>
                                                        <td class=" "><?= $std_row24['TAX_AMOUNT']?></td>
                                                        <?php $cond26="where `IND_NO`=".$std_row24['IND_NO'];
                                                            $std_row26=getRows($cond26,'debit_item');?> 
                                                        <td class=" "><?= $std_row26[0]['AMOUNT']?></td>
                                                        <?php $cond27="where `IND_NO`=".$std_row24['IND_NO'];
                                                            $std_row27=getRows($cond27,'credit');?> 
                                                        <?php 
                                                            $credit=$std_row24['AMOUNT']-$std_row26[0]['AMOUNT'];
                                                            ?>
                                                        <td class=" "><?php echo $credit;?></td>
                                                        <td class=" "><?= $std_row26[0]['GROSS_AMOUNT']?></td>
                                                        <?php 
                                                            $balance=$std_row24['AMOUNT']-($std_row26[0]['AMOUNT']+$credit);
                                                            ?>
                                                        <td class=" "><?php echo $balance;?></td>
                                                        <td class=" "><?php echo $balance;?></td>
                                                    </tr>
                                                    <?php }?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php  }  ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
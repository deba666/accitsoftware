<?php
    /*
        * @package    ACCIT
         * @author    Paperlink Softwares Team
         * @copyright    Copyright (c) 2016 , Paperlink Softwares Pvt. Ltd.  (http://www.paperlinksoftwares.com/)
         * @since    Version 1.0.0
         * @filesource
		 * Code for create the receipt for fees for a particular invoice 
         */
   // insert into receipt table for generate new receipt 
     if($_POST['WHO_DATE'])
       {
    	   	if(insert_record("insert into `receipt` set `REC_STAT_NO`='2',`USER_NO`='".$_POST['user']."',`RECEIPT_DATE`='".$_GET['RECEIPT_DATE']."',`AMOUNT`='".$_GET['n1']."',`BANK_NO`='".$_GET['BANK_NO']."',`REFERENCE_ID`='".$_GET['REFERENCE_ID']."',`REFERENCE_DATE`='".$_GET['RECEIPT_DATE']."',`PAYEE_CUSTOMER_NO`='".$_GET['PAYEE_CUSTOMER_NO']."'"))
    	   {
    		mysql_query("SET @last_id_in_table1 = LAST_INSERT_ID()");
    	   insert_record("insert into `receipt_item` set `RECEIPT_NO`=@last_id_in_table1,`PAY_TYPE_NO`='".$_GET['DEBIT_TYPE_NO']."',`DETAIL`='".$_GET['DETAIL']."',`AMOUNT`='".$_GET['n1']."',`BANK_NAME`='".$_GET['BANK_NO']."',`DEPOSIT_NO`=''");
    //insert into credit table if generate receipt
    	  insert_record("insert into `credit` set `IND_NO`='".$_GET['ind_no']."',`INVOICE_NO`='".$_GET['invoice_no']."',`FEE_NO`='',`RECEIPT_NO`=@last_id_in_table1,`CREDIT_DATE`='".$_GET['RECEIPT_DATE']."',`AMOUNT`='".$_POST['AMT']."',`USER_NO`='".$_POST['user']."',`CREDIT_TYPE_NO`='".$_GET['DEBIT_TYPE_NO']."',`DUE_NO`='',`STATUS`='',`WHO_DATE`='".$_POST['WHO_DATE']."',`ENROL_NO`='".$_GET['enrol_no']."',`TRANS_GROUP_NO`='',`FEE_DEF_NO`='".$_GET['fd_no']."',`DEBIT_NO`=''");
    
           mysql_query("update `due_history` set `AMOUNT`='".$_POST['UPDATE_AMOUNT']."' where `IND_NO`='".$_GET['ind_no']."'");//update due history table after make psayment
    
    	   mysql_query("update `due` set `AMOUNT`='".$_POST['UPDATE_AMOUNT']."' where `IND_NO`='".$_GET['ind_no']."'");
    
       			$_SESSION['s_msg']="<strong>Fine!</strong>Payment Receipt Added Successfully";
    	   }
       		else{
       			$_SESSION['e_msg']="<strong>Oh snap!</strong>Payment Receipt is not added";
                 } 
       } 
       if($_GET['r_no'])
    {
    	$cond="where `RECEIPT_NO`=".$_GET['r_no'];
    	$std_row=getRows($cond,'receipt');
    }
     
      ?>
<div class="col-md-12 col-sm-12 col-xs-12">
    <div class="page-title">
        <div class="title_left">
            <?php 
                if($_SESSION['s_msg'] || $_SESSION['e_msg'])
                {
                ?>
            <div class="alert <?=($_SESSION['s_msg'])?'alert-success':'alert-error'?> fade in">
                <button data-dismiss="alert" class="close" type="button">�</button>
                <?=($_SESSION['s_msg'])?$_SESSION['s_msg']:$_SESSION['e_msg']?>
            </div>
            <?php
                unset($_SESSION['s_msg']);
                unset($_SESSION['e_msg']);
                }
                ?>
            <h3>Receipt Wizard(Payment)</h3>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="clearfix"></div>
            <div class="x_content">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel recent-app">
                            <div class="x_content">
                                <div class="student-account-page edit-faculty-page">
                                    <!-- Nav tabs -->
                                    <div class="card">
                                        <div class="all-students-list add student">
                                            <div class="add-student-section">
                                                <form oninput="x<?php echo $s?>.value=parseInt(a<?php echo $s?>.value)-parseInt(b<?php echo $s?>.value)" method="post" action="" class="form-horizontal form-label-left">
                                                    <?php date_default_timezone_set('Australia/Melbourne');
                                                        $date = date('d/m/Y h:i:s a', time());
                                                        ?>
                                                    <input id="who"  name="WHO_DATE"  type="hidden" value="<?php echo $date?>">
                                                    <input id="when"  name="user" type="hidden" value="<?php echo $_SESSION['user_no']?>">
                                                    <input id="who"  name="DEBIT_TYPE_NO" type="hidden" value="<?= $_POST['DEBIT_TYPE_NO']?>">
                                                    <div class="item form-group">
                                                        <label for="name" class="control-label col-md-2 col-sm-2 col-xs-2">Receipt Total</label>
                                                        <div class="col-md-4 col-sm-4 col-xs-4">
                                                            <div class="row">
                                                                <div class="col-md-4 col-sm-4col-xs-4">
                                                                    <input name="RECEIPT_TOTAL" class="form-control col-md-12 col-xs-12" id="name" type="text" value="<?=$_GET['n1']?>">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <input  type="hidden" name="INVOICE_NO" value="<?=$_POST['INVOICE_NO']?>">
                                                    <div role="tabpanel" class="tab-pane active" id="instalment">
                                                        <div class="table-responsive">
                                                            <table class="table table-striped jambo_table bulk_action">
                                                                <thead>
                                                                    <tr class="headings">
                                                                        <th class="column-title">Invoice No</th>
                                                                        <th class="column-title">Amount</th>
                                                                        <th class="column-title">Make Payment</th>
                                                                        <th class="column-title">Difference</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <?php
																	if($_GET['ind_no']){
                                                                        $cond22="where `IND_NO`=".$_GET['ind_no'];
                                                                        $std_row22=getRows($cond22,'due');
																		$due_amount=($std_row22[0]['AMOUNT'] - $_GET['n1']);
																		}
                                                                                                                                       
                                                                        ?>
                                                                    <tr>
                                                                        <td class=""><?= $std_row22[0]['INVOICE_NO']?></td>
                                                                        <td><input id="b<?php echo $s?>" type="text" value="<?= $std_row22[0]['AMOUNT']?>" name=""></td>
                                                                        <td><input name="AMT" id="a<?php echo $s?>" type="text"></td>
                                                                        <td><output name="x<?php echo $s?>" for="a<?php echo $s?> b<?php echo $s?>"></output></td>
                                                                        <input type="hidden" name="PAYEE_CUSTOMER_NO" value="<?= $std_row21['PAYEE_CUSTOMER_NO']?>">
																		<input type="hidden" name="UPDATE_AMOUNT" value="<?php echo $due_amount;?>">
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-2 col-sm-2 col-xs-12">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-12 col-sm-12 col-xs-12" for="name"> Totals
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4 col-sm-4 col-xs-12">
                                                            <div class="form-group">
                                                                <input class="control-label col-md-12 col-sm-12 col-xs-12" id="main" name="TOTAL_AMOUNT" Value="<?= $GET['n1']?>" type="text">
                                                            </div>
                                                        </div>
                                                    </div>
                                            </div>
                                            <div class="item form-group">
                                            <label for="name" class="control-label col-md-3 col-sm-3 col-xs-12"></label>
                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                            <div class="row">
                                            <div class="col-md-6 col-sm-4 col-xs-12">
                                            <p class="pull-left">
                                            <input type="submit" value="SAVE" name="SUB">
                                            </p>
                                            </div>
                                            </div>
                                            </div>
                                            </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
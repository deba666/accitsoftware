<?php
    /*
        * @package    ACCIT
         * @author    Paperlink Softwares Team
         * @copyright    Copyright (c) 2016 , Paperlink Softwares Pvt. Ltd.  (http://www.paperlinksoftwares.com/)
         * @since    Version 1.0.0
         * @filesource
		 * Code for add new holiday in the holiday list
         */
    
    
    
    
    if($_POST['NAME'])
    	{
        $from_date=$_POST['FROM_DATE'];
    $to_date=$_POST['TO_DATE'];
    $new_format_from_date=str_replace('/', '-', $from_date);
    $new_format_from_date1=date("m/d/Y", strtotime($new_format_from_date));
    $new_format_to_date=str_replace('/', '-', $to_date);
    $new_format_to_date1=date("m/d/Y", strtotime($new_format_to_date));
    
    if($new_format_from_date1 < $new_format_to_date1) {
    	if(insert_record("insert into `holiday` set  `LOCATION_NO`='0',`CRT_NO`='".$_POST['CRT_NAME']."',`OPTION_NO`='".$_POST['OPTION_NO']."',`NAME`='".$_POST['NAME']."',`FROM_DATE`='".$new_format_from_date1."',`TO_DATE`='".$new_format_to_date1."',`USER_NO`='".$_POST['user']."',`CREATE_DATE`='".$_POST['who']."',`DELETE_DATE`='',`LOCK_NUM`='1'"))
    	   /* {
    	    	if(insert_record("insert into `course_fees` set `course_code`='".$_POST['course_code']."', `bill_option`='".$_POST['bill_option']."', `bill_cost`='".$_POST['bill_cost']."', `bill_all_cost`='".$_POST['bill_all_cost']."', `bill_tax`='".$_POST['bill_tax']."', `bill_tax_code`='".$_POST['bill_tax_code']."', `bill_max_length`='".$_POST['bill_max_length']."',`bill_max_cost`='".$_POST['bill_max_cost']."'"))
    		*/
    		$_SESSION['s_msg']="<strong>Fine!</strong> Holiday is Added";
    		//}
    		else//{
    			$_SESSION['e_msg']="<strong>Oh snap!</strong> Holiday is not added";
    		//}
    	}
        else
        {
           $_SESSION['e_msg']="<strong>To date must be greater than From date!</strong>"; 
        }
        }
    	
    	
    if($_GET['del'])
    {
    	if(delete_record("DELETE FROM `student` WHERE `std_id` = ".$_GET['del']))
    		$_SESSION['s_msg']="Item successfully deleted";
    }
    
    ?>
<div class="right_col" role="main">
<div class="">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="page-title">
            <div class="title_left">
                <?php 
                    if($_SESSION['s_msg'] || $_SESSION['e_msg'])
                    {
                    ?>
                <div class="alert <?=($_SESSION['s_msg'])?'alert-success':'alert-error'?> fade in">
                    <button data-dismiss="alert" class="close" type="button">�</button>
                    <?=($_SESSION['s_msg'])?$_SESSION['s_msg']:$_SESSION['e_msg']?>
                </div>
                <?php
                    unset($_SESSION['s_msg']);
                    unset($_SESSION['e_msg']);
                    }
                    ?>
                <h3>Add Holiday</h3>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="clearfix"></div>
                <div class="x_content">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel recent-app">
                                <div class="x_content">
                                    <div class="student-account-page">
                                        <!-- Nav tabs -->
                                        <div class="card">
                                            <div class="all-students-list add-course">
                                                <div class="add-student-section">
                                                    <form class="form-horizontal form-label-left" action="" method="POST" novalidate>
                                                        <button type="submit" class="btn btn-success">Add Holiday</button>
                                                        <ul id="student-acc-tab" class="nav nav-tabs" role="tablist">
                                                            <li role="presentation"  class="active"><a href="#details" aria-controls="details" role="tab" data-toggle="tab">Details</a></li>
                                                        </ul>
                                                        <div class="tab-content">
                                                            <!--Details-->
                                                            <div role="tabpanel" class="tab-pane active" id="details">
                                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                                    <div class="row">
                                                                        <h4>
                                                                            <fieldset>Basic Info</fieldset>
                                                                        </h4>
                                                                        <?php date_default_timezone_set('Australia/Sydney');
                                                                            $date = date('d/m/Y h:i:s a', time());
                                                                            ?>
                                                                        <input id="who"  name="who"  type="hidden" value="<?php echo $date?>">
                                                                        <input id="when"  name="user" type="hidden" value="<?php echo $_SESSION['user_no']?>">
                                                                        <div class="item form-group">
                                                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Faculty Name</label>
                                                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                                                <div class="row">
                                                                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                                                                        <select name="CRT_NAME" class="col-md-12 col-xs-12 form-control" required>
                                                                                            <option Value="">Select Faculty</option>
                                                                                            <?php
                                                                                                $sql1="select * from crse_type ORDER BY `CRT_NAME` ASC";
                                                                                                $q1=mysql_query($sql1);
                                                                                                while($row1=mysql_fetch_array($q1))
                                                                                                {
                                                                                                
                                                                                                ?>
                                                                                            <option Value="<?=$row1['CRT_NO']?>"><?=$row1['CRT_NAME']?></option>
                                                                                            <?php } ?>
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="item form-group">
                                                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Name</label>
                                                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                                                <div class="row">
                                                                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                                                                        <input id="email" class="form-control col-md-12 col-xs-12" name="NAME" placeholder="Holiday Name" type="text" required>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="item form-group">
                                                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">From Date</label>
                                                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                                                <div class="row">
                                                                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                                                                        <input id="email" class="form-control col-md-12 col-xs-12" name="FROM_DATE" placeholder="Holiday Name" type="date" required>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="item form-group">
                                                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">To Date</label>
                                                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                                                <div class="row">
                                                                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                                                                        <input id="email" class="form-control col-md-12 col-xs-12" name="TO_DATE" placeholder="Holiday Name" type="date" required>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                         <div class="item form-group">
                                                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Option</label>
                                                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                                         <div class="row">
                                                                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                                                                        <select name="OPTION_NO" class="col-md-12 col-xs-12 form-control" required>
                                                                                            <option Value="1">Extend</option>
                                                                                            
                                                                                            <option Value="0">Don't Extend</option>
                                                                                           
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                                </div>
                                                                                </div>
                                                    </form>
                                                    </div>
                                                    <!--notes-->
                                                    <div role="tabpanel" class="tab-pane" id="notes">
                                                    </div>
                                                    </div>
                                                    </div>
                                                 
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
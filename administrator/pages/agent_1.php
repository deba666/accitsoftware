<?php
    /*
        * @package    ACCIT
         * @author    Paperlink Softwares Team
         * @copyright    Copyright (c) 2016 , Paperlink Softwares Pvt. Ltd.  (http://www.paperlinksoftwares.com/)
         * @since    Version 1.0.0
         * @filesource
		 * This page contain all the Agent details and The Agent diary link is created in this page
         */
    ?>
<div role="main" class="right_col" style="min-height: 648px;">
    <div class="">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="page-title">
                <div class="title_left">
                    <h3>All Agents</h3>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="clearfix"></div>
                    <div class="x_content">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel recent-app">
                                    <div class="x_content">
                                        <div class="student-account-page">
                                            <!-- Nav tabs -->
                                            <div class="card">
                                                <ul role="tablist" class="nav nav-tabs">
                                                    <li class="active" role="presentation"><a data-toggle="tab" role="tab" aria-controls="all-student" href="#all-student">Agent</a></li>
                                                </ul>
                                                <div class="tab-content">
                                                    <div id="all-student" class="tab-pane active" role="tabpanel">
                                                        <div class="all-students-list">
                                                            <div class="table-responsive">
                                                                <div>
                                                                    <div>
                                                                        <div>
                                                                            <table class="table data-tbl-tools">
                                                                                <thead>
                                                                                    <tr class="headings" role="row">
                                                                                        <th class="column-title sorting_asc" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1" style="width: 145px;" aria-label="Date
                                                                                            : activate to sort column descending" aria-sort="ascending">Code
                                                                                        </th>
                                                                                        <th class="column-title sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1" style="width: 145px;" aria-label="Attention
                                                                                            : activate to sort column ascending">Name
                                                                                        </th>
                                                                                        <th class="column-title sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1" style="width: 145px;" aria-label="Student No
                                                                                            : activate to sort column ascending">Address1
                                                                                        </th>
                                                                                        <th class="column-title sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1" style="width: 145px;" aria-label="Subject
                                                                                            : activate to sort column ascending">Country
                                                                                        </th>
                                                                                        <th class="column-title sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1" style="width: 145px;" aria-label="Note
                                                                                            : activate to sort column ascending">Status
                                                                                        </th>
                                                                                        <th class="column-title no-link last sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1" style="width: 145px;" aria-label="Action
                                                                                            : activate to sort column ascending"><span class="nobr">Action</span>
                                                                                        </th>
                                                                                    </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                    <?php
                                                                                        $sql="select * from  agent order by AGENT_NAME ASC";
                                                                                        $q=mysql_query($sql);
                                                                                        while($row=mysql_fetch_array($q))
                                                                                        {
                                                                                          $sql1="select COUNTRY_NAME from `country` where `COUNTRY_NO`='$row[COUNTRY_NO]'";
                                                                                          $res1=mysql_query($sql1);
                                                                                          $row1=mysql_fetch_array($res1);
                                                                                        ?>
                                                                                    <tr class="pointer odd" role="row">
                                                                                        <td class="sorting_1" tabindex="0"><?=$row['AGENT_CODE'];?></td>
                                                                                        <td class=" "><?=$row['AGENT_NAME'];?></td>
                                                                                        <td class=" "><?=$row['ADDRESS1'];?></td>
                                                                                        <td class=" "><?=$row1['COUNTRY_NAME'];?></td>
                                                                                        <td class=" "><?=($row['STATUS'])?'Inactive':'Active'?></td>
                                                                                        <td class="last"><a href="#" onclick="javascript:void window.open('dashboard.php?op=<?=MD5('display_agent_diary')?>&ag_no=<?=$row['AGENT_NO']?> &ag_name=<?=$row['AGENT_NAME']?>&cus_no=<?=$row['CUSTOMER_NO']?>','1470111111167','width=700,height=500,toolbar=0,menubar=0,location=1,status=1,scrollbars=1,resizable=1,left=0,top=0');return false;">Add New Diary</a>|<a href="#" onclick="javascript:void window.open('dashboard.php?op=<?=MD5('all_agent_diary')?>&ag_no=<?=$row['AGENT_NO']?> &ag_name=<?=$row['AGENT_NAME']?>&cus_no=<?=$row['CUSTOMER_NO']?>','1470112211167','width=700,height=500,toolbar=0,menubar=0,location=1,status=1,scrollbars=1,resizable=1,left=0,top=0');return false;">&nbsp;&nbsp;All Diary</a>
                                                                                        </td>
                                                                                        <?php 
                                                                                            }?>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
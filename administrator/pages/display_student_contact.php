<?php
    /*
        * @package    ACCIT
         * @author    Paperlink Softwares Team
         * @copyright    Copyright (c) 2016 , Paperlink Softwares Pvt. Ltd.  (http://www.paperlinksoftwares.com/)
         * @since    Version 1.0.0
         * @filesource
         * code for add new contact for a particular student.
         * Link comes under the student edit page (Student edit->address field->add new contact)
         */
    
       if($_POST['NAME'])
       {
        if(insert_record("insert into `stu_contact` set `STUD_NO`='".$_GET['stu_no']."',`NAME`='".$_POST['NAME']."',`ADDRESS1`='".$_POST['ADDRESS1']."',`ADDRESS2`='".$_POST['ADDRESS2']."',`SUBURB`='".$_POST['SUBURB']."',`POSTCODE`='".$_POST['POSTCODE']."',`STATE`='".$_POST['STATE']."',`PHONE1`='".$_POST['PHONE1']."',`PHONE2`='".$_POST['PHONE2']."',`EMAIL`='".$_POST['EMAIL']."',`CONTACT_INFO`='".$_POST['CONTACT_INFO']."',`TYPE_NO`='1',`COUNTRY_NO`='".$_GET['cun_no']."',`CUSTOMER_NO`='".$_GET['cust_no']."'"))
    
                $_SESSION['s_msg']="<strong>Fine! </strong>New Student Contact Added Successfully";
            else{
                $_SESSION['e_msg']="<strong>Oh snap! </strong>Student Contact is not added";
       }
       
       ?>
<script>
opener.location.reload(true);
     self.close();
     
     </script>
<?php
            }
       ?>

       <style>
        footer{display: none !important; }
        .nav_menu{display: none;}
       </style>
<div class="col-md-12 col-sm-12 col-xs-12">
    <div class="page-title">
        <div class="title_left">
            <?php 
                if($_SESSION['s_msg'] || $_SESSION['e_msg'])
                {
                ?>
            <div class="alert <?=($_SESSION['s_msg'])?'alert-success':'alert-error'?> fade in">
                <button data-dismiss="alert" class="close" type="button">×</button>
                <?=($_SESSION['s_msg'])?$_SESSION['s_msg']:$_SESSION['e_msg']?>
            </div>
            <?php
                unset($_SESSION['s_msg']);
                unset($_SESSION['e_msg']);
                }
                ?>
            <h3>Contact</h3>
        </div>
    </div>
</div>
<div class="card">
                                    <div class="all-students-list add student">
                                        <div class="add-student-section">
                                            <form method="post" action="" class="form-horizontal form-label-left">
                                                <div class="item form-group">
                                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                                        <div class="row">
                                                            <div class="col-md-12 col-sm-12">
                                                                <div class="row">
                                                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                                                        <div class="form-group">
                                                                            Name<input class="form-control col-md-12 col-sm-12 col-xs-12" name="NAME" placeholder="Name" type="text" required>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                                                        Address<input class="form-control col-md-12 col-sm-12 col-xs-12" name="ADDRESS1" placeholder="Address1" type="text" required>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                                                        <div class="form-group">
                                                                            Contact Info<input class="form-control col-md-12 col-sm-12 col-xs-12" name="CONTACT_INFO" placeholder="contact info" type="text" required>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-6 col-sm-6 col-xs-6"><br>
                                                                        <input class="form-control col-md-12 col-sm-12 col-xs-12" name="ADDRESS2" placeholder="Address 2" type="text" required>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                                                        <div class="form-group">
                                                                            Phone1<input class="form-control col-md-12 col-sm-12 col-xs-12" name="PHONE1" placeholder="Phone1" type="text" required>
                                                                        </div>
                                                                    </div>
                                                                    <br>
                                                                    <div class="col-md-2 col-sm-2 col-xs-2">
                                                                        <div class="form-group">
                                                                            <input class="form-control col-md-6 col-sm-6 col-xs-6" name="SUBURB" placeholder="City" type="text">
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-2 col-sm-2 col-xs-2">
                                                                        <div class="form-group">
                                                                            <input class="form-control col-md-6 col-sm-6 col-xs-6" name="STATE" placeholder="State" type="text">
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-2 col-sm-2 col-xs-2">
                                                                        <div class="form-group">
                                                                            <input class="form-control col-md-6 col-sm-6 col-xs-6" name="POSTCODE" placeholder="Postcode" type="text">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                                                        <div class="form-group">
                                                                            Phone2<input class="form-control col-md-12 col-sm-12 col-xs-12" name="PHONE2" placeholder="Phone2" type="text">
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                                                        Email<input  class="form-control col-md-12 col-sm-12 col-xs-12" placeholder="Email" name="EMAIL" type="email" required>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="item form-group">
                                                        <label for="name" class="control-label col-md-3 col-sm-3 col-xs-12"></label>
                                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                                            <div class="row">
                                                                <div class="col-md-6 col-sm-4 col-xs-12">
                                                                    <p class="pull-left">
                                                                        <input class="btn btn-primary" type="button" onclick="javascript: window.close();" value="Cancel">
                                                                        <button class="btn btn-success" type="submit">Submit</button>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                           
                                            </div>
                                                 </form>
                                        </div>
                                    </div>
                                </div>


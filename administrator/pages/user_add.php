<?php
    /*
        * @package    ACCIT
         * @author    Paperlink Softwares Team
         * @copyright    Copyright (c) 2016 , Paperlink Softwares Pvt. Ltd.  (http://www.paperlinksoftwares.com/)
         * @since    Version 1.0.0
         * @filesource
		 * Code for add new user into database
		 
         */
    ?>
<?php

if($_POST['EMAIL'])	 
$string = $_POST['FNAME'];
$string2 = $_POST['LNAME'];
$init1 = $string[0]; 	
$init2 = $string2[0]; 
$init="$init1"."$init2";
if($_POST['FNAME'])
	{
	if(insert_record("insert into `users` set  `FNAME`='".$_POST['FNAME']."',`LNAME`='".$_POST['LNAME']."',`EMAIL`='".$_POST['EMAIL']."', `PWORD`='".$_POST['PWORD']."',`ACCESS_LEVEL`='".$_POST['ACCESS_LEVEL']."',`LOCATION_NO`='1',`STATUS`='1' ,`LAST_LOGON`='".$_POST['WHO_DATE']."' ,`RESTRICT_LOCATION`=' ' ,`TYPE_NO`= '1' ,`PWORD_SALT`=' ' ,`REC_STAT_NO`=' ' ,`CHANGE_DATE`=' ',`INIT`='$init',`USER_NAME`='".$_POST['USER_NAME']."'"))
	   
		$_SESSION['s_msg']="<strong>Fine!</strong> New User Added";

		else
			$_SESSION['e_msg']="<strong>Oh snap!</strong> User is not added";
	}
	
	
if($_GET['del'])
{
	if(delete_record("DELETE FROM `users` WHERE `std_id` = ".$_GET['del']))
		$_SESSION['s_msg']="Item successfully deleted";
}

?>
<div class="right_col" role="main">

                  <div class="">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                      <div class="page-title">
                        <div class="title_left">
                        <?php 
                                    if($_SESSION['s_msg'] || $_SESSION['e_msg'])
                                    {
										?>
                                            <div class="alert <?=($_SESSION['s_msg'])?'alert-success':'alert-error'?> fade in">
                                                <button data-dismiss="alert" class="close" type="button">�</button>
                                                <?=($_SESSION['s_msg'])?$_SESSION['s_msg']:$_SESSION['e_msg']?>
                                                
                                            </div>
										<?php
                                    	unset($_SESSION['s_msg']);
                                    unset($_SESSION['e_msg']);
                                    }
                                ?>
                          <h3>Create User</h3>
                        </div>
                      </div>
                    </div>

                    <div class="clearfix"></div>

                    <div class="row">
                      <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                          <div class="clearfix"></div>

                          <div class="x_content">
                            <div class="row">
                              <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel recent-app">
                                  <div class="x_content">
                                    <div class="student-account-page">
                                      <!-- Nav tabs -->
                                      <div class="card">
                                        <div class="all-students-list add-course">
                                          <div class="add-student-section">
                                            <form class="form-horizontal form-label-left" action="" method="post">
                                             
											  
                                                            <!--Details-->
                                                            
								<div class="col-md-12 col-sm-12 col-xs-12">
                                                                     <div class="row">
                                                                        <div class="col-md-6 col-sm-6 col-xs-6">
                                                                            <div class="form-group">
                                                                                <h4><label class="control-label col-md-12 col-sm-12 col-xs-6">First Name:
                                                                                </label></h4>
                                                                                <input name="FNAME" class="form-control col-md-12 col-sm-12 col-xs-6" placeholder="Enter Frist Name" type="text" required>
                                                                            </div>
                                                                        </div>
                                                                        </div>
                                                                    <div class="row">
                                                                        <div class="col-md-6 col-sm-6 col-xs-6">
                                                                            <div class="form-group">
                                                                                <h4><label class="control-label col-md-12 col-sm-12 col-xs-6">Last Name:
                                                                                </label></h4>
                                                                                <input name="LNAME" class="form-control col-md-12 col-sm-12 col-xs-6" placeholder="Enter Last Name" type="text" required>
                                                                            </div>
                                                                        </div>
                                                                        </div>
                                                                       
                                                                    <div class="row">
                                                                        <div class="col-md-6 col-sm-6 col-xs-6">
                                                                            <div class="form-group">
                                                                                <h4><label class="control-label col-md-12 col-sm-12 col-xs-6">Email Address 
                                                                                </label></h4>
                                                                                <input name="EMAIL" class="form-control col-md-12 col-sm-12 col-xs-6" placeholder="Email Address" type="email" required>
                                                                            </div>
                                                                        </div>
                                                                        </div>
                                                                    <div class="row">
                                                                        <div class="col-md-6 col-sm-6 col-xs-6">
                                                                            <div class="form-group">
                                                                                <h4><label class="control-label col-md-12 col-sm-12 col-xs-6">USERNAME
                                                                                </label></h4>
                                                                                <input name="USER_NAME" class="form-control col-md-12 col-sm-12 col-xs-6" placeholder="Enter User Name" type="text" required>
                                                                            </div>
                                                                        </div>
                                                                        </div>
                                                                        <div class="row">
                                                                        <div class="col-md-6 col-sm-6 col-xs-6">
                                                                            <div class="form-group">
                                                                                <h4><label class="control-label col-md-12 col-sm-12 col-xs-6">Enter Password
                                                                                </label></h4>
                                                                                <input name="PWORD" class="form-control col-md-12 col-sm-12 col-xs-6" placeholder="Enter Password" type="password" required>
                                                                            </div>
                                                                        </div>
                                                                        </div>
                                                                        <div class="row">
                                                                        <div class="col-md-6 col-sm-6 col-xs-6">
                                                                            <div class="form-group">
                                                                                <h4><label class="control-label col-md-12 col-sm-12 col-xs-6" required>User Type
                                                                                </label></h4>
                                                                                <select name="ACCESS_LEVEL" class="col-md-12 col-xs-12 form-control">
											<option value="65535">Administrator</option>
                                                                                        <option value="6553">Accountant</option>
                                                                                        <option value="655">Author</option>
                                                                                        <option value="65535">Tester</option>
										</select>
                                                                            </div>
                                                                        </div>
                                                                        </div>
                                                                        
                                                                <button type="submit" class="btn btn-success">SAVE</button>
                                                              
                                                    </div>
                                                    
								  </form>					
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
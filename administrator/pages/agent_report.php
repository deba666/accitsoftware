<?php
    /*
        * @package    ACCIT
         * @author    Paperlink Softwares Team
         * @copyright    Copyright (c) 2016 , Paperlink Softwares Pvt. Ltd.  (http://www.paperlinksoftwares.com/)
         * @since    Version 1.0.0
         * @filesource
		 * Here Display All the Agent Reports
         */
    ?>
<div role="main" class="right_col" style="min-height: 648px;">
    <div class="">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="page-title">
                <div class="title_left">
                    <h3>Agents Reports</h3>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="clearfix"></div>
                    <div class="x_content">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel recent-app">
                                    <div class="x_content">
                                        <div class="student-account-page view-faculty-page">
                                            <!-- Nav tabs -->
                                            <div class="card">
                                                <div class="all-students-list add student">
                                                    <div class="add-student-section">
                                                        <div class="table-responsive">
                                                            <table class="table data-tbl-tools" cellspacing="0" width="100%">
                                                                <thead>
                                                                    <tr class="headings">
                                                                        <th class="column-title">Agent Code
                                                                        </th>
                                                                        <th class="column-title">Agent Name
                                                                        </th>
                                                                        <th class="column-title">Phone	
                                                                        </th>
                                                                        <th class="column-title">Fax
                                                                        </th>
                                                                        <th class="column-title">Email
                                                                        </th>
                                                                        <th class="column-title">URL
                                                                        </th>
                                                                        <th class="column-title">County
                                                                        </th>
                                                                        <th class="column-title">Region 
                                                                        </th>
                                                                        <th class="column-title">Street Address1
                                                                        </th>
                                                                        <th class="column-title">Street Address2
                                                                        </th>
                                                                        <th class="column-title">Street Address3
                                                                        </th>
                                                                        <th class="column-title">Street Address4
                                                                        </th>
                                                                        <th class="column-title">Postal Address1
                                                                        </th>
                                                                        <th class="column-title">Postal Address2
                                                                        </th>
                                                                        <th class="column-title">Postal Address3 
                                                                        </th>
                                                                        <th class="column-title">Postal Address4
                                                                        </th>
                                                                        <th class="column-title">Notes 
                                                                        </th>
                                                                        <th class="column-title">Employee Tilte 
                                                                        </th>
                                                                        <th class="column-title">Employee First Name 
                                                                        </th>
                                                                        <th class="column-title">Employee Last Name
                                                                        </th>
                                                                        <th class="column-title">Employee Position 
                                                                        </th>
                                                                        <th class="column-title">Employee Phone 
                                                                        </th>
                                                                        <th class="column-title">Employee Fax 
                                                                        </th>
                                                                        <th class="column-title">Employee Email 
                                                                        </th>
                                                                        <th class="column-title">Commission 
                                                                        </th>
                                                                        <th class="column-title">Rate 
                                                                        </th>
                                                                        <th class="column-title">Faculty 
                                                                        </th>
                                                                        <th class="column-title">Tax Option 
                                                                        </th>
                                                                    </tr>
                                                                </thead>
                                                                <?php 
                                                                    if($_POST['inactive']=='all')
                                                                    { 
                                                                     $aget_row =getRows(' ORDER BY `AGENT_NO` DESC','agent');
                                                                    ?>
                                                                <tbody>
                                                                    <?php  ?>
                                                                    <?php
                                                                        $sl=0;
                                                                        foreach($aget_row as $agt)
                                                                        {
                                                                        ?>
                                                                    <tr class="even pointer">
                                                                        <td class=" "><?=$agt['AGENT_CODE'];?></td>
                                                                        <td class=" "><?=$agt['AGENT_NAME'];?></td>
                                                                        <td class=" "><?=$agt['PHONE'];?></td>
                                                                        <td class=" "><?=$agt['FAX'];?></td>
                                                                        <td class=" "><?=$agt['EMAIL'];?></td>
                                                                        <td class=" "><?=$agt['WWW'];?></td>
                                                                        <td class=" "><?=$agt['COUNTRY_NO'];?></td>
                                                                        <td class=" "><?=$agt['AGENT_NO'];?></td>
                                                                        <td class=" "><?=$agt['ADDRESS1'];?></td>
                                                                        <td class=" "><?=$agt['ADDRESS2'];?></td>
                                                                        <td class=" "><?=$agt['ADDRESS3'];?></td>
                                                                        <td class=" "><?=$agt['ADDRESS4'];?></td>
                                                                        <td class=" "><?=$agt['ADDRESS1_TWO'];?></td>
                                                                        <td class=" "><?=$agt['ADDRESS2_TWO'];?></td>
                                                                        <td class=" "><?=$agt['ADDRESS3_TWO'];?></td>
                                                                        <td class=" "><?=$agt['ADDRESS4_TWO'];?></td>
                                                                        <td class=" "><?=$agt['NOTES'];?></td>
                                                                        <?php $ag_emp="where `AGENT_NO`=".$agt['AGENT_NO']; $emp=getRows($ag_emp,'agent_emps');?>
                                                                        <td class=" "><?=$emp[0]['TITLE'];?></td>
                                                                        <td class=" "><?=$emp[0]['FIRST_NAME'];?></td>
                                                                        <td class=" "><?=$emp[0]['LAST_NAME'];?></td>
                                                                        <td class=" "><?=$emp[0]['AG_POSITION'];?></td>
                                                                        <td class=" "><?=$emp[0]['PHONE'];?></td>
                                                                        <td class=" "><?=$emp[0]['FAX'];?></td>
                                                                        <td class=" "><?=$emp[0]['EMAIL'];?></td>
                                                                        <?php $ag_com="where `AGENT_NO`=".$agt['AGENT_NO']; $comm=getRows($ag_comm,'agent_commission');?>
                                                                        <td class=" "><?=$comm[0]['NAME'];?></td>
                                                                        <td class=" "><?=$comm[0]['RATE_PERCENT'];?>.00%</td>
                                                                        <?php $cr_ty="where `CRT_NO`=".$comm[0]['CRT_NO']; $crt=getRows($cr_ty,'crse_type');?>
                                                                        <td class=" "><?=$crt[0]['CRT_NAME'];?></td>
                                                                        <td class=" "><?=$crt[0]['OPTION_NO'];?></td>
                                                                    </tr>
                                                                    <?php }
                                                                        }
                                                                        elseif($_POST['active']=='act'){
                                                                         
                                                                         	 $aget_row =getRows('WHERE `STATUS`=1','agent');
                                                                        
                                                                        ?>
                                                                <tbody>
                                                                    <?php  ?>
                                                                    <?php
                                                                        foreach($aget_row as $agt)
                                                                        {
                                                                        ?>
                                                                    <tr class="even pointer">
                                                                        <td class=" "><?=$agt['AGENT_CODE'];?></td>
                                                                        <td class=" "><?=$agt['AGENT_NAME'];?></td>
                                                                        <td class=" "><?=$agt['PHONE'];?></td>
                                                                        <td class=" "><?=$agt['FAX'];?></td>
                                                                        <td class=" "><?=$agt['EMAIL'];?></td>
                                                                        <td class=" "><?=$agt['WWW'];?></td>
                                                                        <td class=" "><?=$agt['COUNTRY_NO'];?></td>
                                                                        <td class=" "><?=$agt['AGENT_NO'];?></td>
                                                                        <td class=" "><?=$agt['ADDRESS1'];?></td>
                                                                        <td class=" "><?=$agt['ADDRESS2'];?></td>
                                                                        <td class=" "><?=$agt['ADDRESS3'];?></td>
                                                                        <td class=" "><?=$agt['ADDRESS4'];?></td>
                                                                        <td class=" "><?=$agt['ADDRESS1_TWO'];?></td>
                                                                        <td class=" "><?=$agt['ADDRESS2_TWO'];?></td>
                                                                        <td class=" "><?=$agt['ADDRESS3_TWO'];?></td>
                                                                        <td class=" "><?=$agt['ADDRESS4_TWO'];?></td>
                                                                        <td class=" "><?=$agt['NOTES'];?></td>
                                                                        <?php $ag_emp="where `AGENT_NO`=".$agt['AGENT_NO']; $emp=getRows($ag_emp,'agent_emps');?>
                                                                        <td class=" "><?=$emp[0]['TITLE'];?></td>
                                                                        <td class=" "><?=$emp[0]['FIRST_NAME'];?></td>
                                                                        <td class=" "><?=$emp[0]['LAST_NAME'];?></td>
                                                                        <td class=" "><?=$emp[0]['AG_POSITION'];?></td>
                                                                        <td class=" "><?=$emp[0]['PHONE'];?></td>
                                                                        <td class=" "><?=$emp[0]['FAX'];?></td>
                                                                        <td class=" "><?=$emp[0]['EMAIL'];?></td>
                                                                        <?php $ag_com="where `AGENT_NO`=".$agt['AGENT_NO']; $comm=getRows($ag_comm,'agent_commission');?>
                                                                        <td class=" "><?=$comm[0]['NAME'];?></td>
                                                                        <td class=" "><?=$comm[0]['RATE_PERCENT'];?>.00%</td>
                                                                        <?php $cr_ty="where `CRT_NO`=".$comm[0]['CRT_NO']; $crt=getRows($cr_ty,'crse_type');?>
                                                                        <td class=" "><?=$crt[0]['CRT_NAME'];?></td>
                                                                        <td class=" "><?=$comm[0]['OPTION_NO'];?></td>
                                                                    </tr>
                                                                    <?php
                                                                        }
                                                                        }
                                                                        ?>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
    /*
        * @package    ACCIT
         * @author    Paperlink Softwares Team
         * @copyright    Copyright (c) 2016 , Paperlink Softwares Pvt. Ltd.  (http://www.paperlinksoftwares.com/)
         * @since    Version 1.0.0
         * @filesource
         */
    
    if($_GET['subject_no'])
    	{
    	if(insert_record("insert into `crse_subj` set `SUBJECT_NO`='".$_GET['subject_no']."',`COURSE_NO`='".$_GET['id']."',`VET_DELIVERYLOC_NO`='0',`VETFEE_EFTSL`=' '"))
    		$_SESSION['s_msg']="<strong>Fine!</strong> module Course Added.";		
    		else{
    			$_SESSION['e_msg']="<strong>Oh snap!</strong> module course not added";
    		}
    	}
    ?>
<div class="right_col" role="main">
    <div class="">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="page-title">
                <div class="title_left">
                    <h3>Add Course Module</h3>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="clearfix"></div>
                    <div class="x_content">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel recent-app">
                                    <div class="x_content">
                                        <div class="student-account-page">
                                            <!-- Nav tabs -->
                                            <div role="tabpanel" class="tab-pane" id="course-modules">
                                                <div id="att-Warning" class="tab-pane active" role="tabpanel">
                                                    <div class="table-responsive">
                                                        <table width="100%" cellspacing="0" class="display table table-striped table-bordered table data-tbl-tools" id="example6">
                                                            <thead>
                                                                <tr class="headings">
                                                                    <th class="column-title" style="width: 465px !important;">Code
                                                                    </th>
                                                                    <th class="column-title" style="width: 465px !important;">Name
                                                                    </th>
                                                                    <th class="column-title" style="width: 465px !important;">Hours
                                                                    </th>
                                                                    <th class="column-title" style="width: 465px !important;">Action
                                                                    </th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <?php $sub_row=getRows('order by `SUBJECT_NO` DESC','subject'); 
                                                                    foreach($sub_row as $row)
                                                                    {
                                                                    ?>
                                                                <tr class="odd pointer">
                                                                    <td class=" " style="width: 465px !important;"><?=$row['SUBJ_CODE']?> </td>
                                                                    <td class=" " style="width: 465px !important;"><?=$row['SUBJ_DESC']?></td>
                                                                    <td class=" " style="width: 465px !important;"><?=$row['HOURS']?></td>
                                                                    <td class=" " style="width: 465px !important;"><a href="dashboard.php?op=<?=MD5('display_module')?>&subject_no=<?=$row['SUBJECT_NO']?>&id=<?php echo $_GET['id']?>">ADD</a></td>
                                                                </tr>
                                                                <?php } ?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
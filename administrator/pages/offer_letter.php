<?php
error_reporting(0);
include("../settings.php");
if($_POST['STUD_NO']){
$st_cond="where `STUD_NO`=".$_POST['STUD_NO'];
$st_data= getRows($st_cond,'student');
}
?>


<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>ACCIT | Offer Letter</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
    <body>
        <div style="width: 900px; margin: 0 auto; position: relative;">
            <p class="Header">
            	<span style="font: 7.0pt &quot;Times New Roman&quot;;-sf-listtab:yes;-sf-tabstop-align:left;-sf-tabstop-pos:74.25pt;">                                          
            	</span>
            	<span style="position:Absolute; width:793px; height:546px; left:0px; margin-left:-84px; margin-top:-50px;z-index:-7168">
            		<img src="../images/TDIcaGvQ_img1.png" width="793" height="546" alt="" style="max-width: 100%;" />
            	</span>
            	<span style="width:33.75pt; text-indent:0pt;font-family:Calibri;font-size:11pt;font-weight:normal;font-style:normal; display:inline-block;">             
            	</span>
            </p>
            <p style="text-align:center;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;line-height:normal;margin-top:0pt;margin-bottom:0pt;">
                <span style="position:Absolute; width:145px; height:155px; left:0px; margin-left:-13px; margin-top:-57px;z-index:6144">
                	<img src="../images/TDIcaGvQ_img2.png" width="145" height="155" alt="" />
                </span>
                <span style="color:#FFFFFF;font-family:Arial;font-size:1pt;text-transform:none;font-weight:bold;font-style:normal;font-variant:normal;">{!404_DOCXTemplate_S_STUDENT_NAME}
                </span>
                <span style="color:#FFFFFF;font-family:Arial;font-size:1pt;text-transform:none;font-weight:bold;font-style:normal;font-variant:normal;"> Letter of Offer
                </span>
            </p>
            <p class="Title">
           		<span style="letter-spacing:-0.5pt;font-family:Century Gothic;font-size:28pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;">&#xa0;
           		</span>
            </p>
            <p class="Title">
            	<span style="letter-spacing:-0.5pt;font-family:Century Gothic;font-size:28pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;">&#xa0;
            	</span>
            </p>
            <p class="Title" style="text-align: center;">
            	<span style="letter-spacing:-0.5pt;font-family:Century Gothic;font-size:28pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;">Letter of Offer
            	</span>
            </p>
            <p style="text-align:right;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;line-height:115%;margin-top:0pt;margin-bottom:10pt;">
            	<span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;"><?php echo date('Y-m-d');?>
            	</span>
            </p>
            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;line-height:normal;margin-top:0pt;margin-bottom:0pt;">
            	<span style="font-family:Arial;font-size:10pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;">&#xa0;
            	</span>
            </p>
            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;line-height:normal;margin-top:0pt;margin-bottom:0pt;">
            	<span style="font-family:Arial;font-size:10pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;">&#xa0;
            	</span>
            </p>
            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;line-height:115%;margin-top:0pt;margin-bottom:10pt;">
            	<span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">Dear <?=$st_data[0]['FNAME']?>,
            	</span>
           	</p>
            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;line-height:115%;margin-top:0pt;margin-bottom:10pt;">
            	<span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">Congratulations! I am delighted to admit you as an international student into Australian College of Commerce and Information Technology (ACCIT). Details of your offer are set below: 
            	</span>
            </p>
            <h2>
            	<span style="color:#032348;font-family:Century Gothic;font-size:13pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">Personal Details</span>
            </h2>
            <p class="Style_3spaceSingle">
                <span style="font: 7.0pt &quot;Times New Roman&quot;;-sf-listtab:yes;"></span>
                <span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;mso-spacerun:yes;">Student Number:  
               	</span>
               	<span style="width:25.71pt; text-indent:0pt;font-family:Calibri;font-size:11pt;font-weight:normal;font-style:normal; display:inline-block;">          
               	</span>
               	<span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;"><?=$st_data[0]['EXT_STN']?>
               	</span>
            </p>
            <p class="Style_3spaceSingle">
            	<span style="font: 7.0pt &quot;Times New Roman&quot;;-sf-listtab:yes;"></span>
            	<span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;">Given Name: 
            	</span>
            	<span style="width:11.88pt; text-indent:0pt;font-family:Calibri;font-size:11pt;font-weight:normal;font-style:normal; display:inline-block;">    
            	</span>
            	<span style="width:36pt; text-indent:0pt;font-family:Calibri;font-size:11pt;font-weight:normal;font-style:normal; display:inline-block;">              
            	</span>
            	<span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;"><?=$st_data[0]['LNAME']?>,<?=$st_data[0]['FNAME']?>,</span>
            </p>
            <p class="Style_3spaceSingle">
            	<span style="font: 7.0pt &quot;Times New Roman&quot;;-sf-listtab:yes;"></span>
            	<span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;">Family Name: 
            	</span>
            	<span style="width:8.61pt; text-indent:0pt;font-family:Calibri;font-size:11pt;font-weight:normal;font-style:normal; display:inline-block;">   
            	</span>
            	<span style="width:36pt; text-indent:0pt;font-family:Calibri;font-size:11pt;font-weight:normal;font-style:normal; display:inline-block;">              
            	</span>
            	<span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;"><?=$st_data[0]['LNAME']?></span>
            </p>
            <p class="Style_3spaceSingle">
                <span style="font: 7.0pt &quot;Times New Roman&quot;;-sf-listtab:yes;"></span>
                <span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;">Date of Birth: 
                </span>
                <span style="width:9.66pt; text-indent:0pt;font-family:Calibri;font-size:11pt;font-weight:normal;font-style:normal; display:inline-block;">   
                </span>
                <span style="width:36pt; text-indent:0pt;font-family:Calibri;font-size:11pt;font-weight:normal;font-style:normal; display:inline-block;">              
                </span>
                <span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;"><?=$st_data[0]['DOB']?></span>
            </p>
            <p class="Style_3spaceSingle">
                <span style="font: 7.0pt &quot;Times New Roman&quot;;-sf-listtab:yes;"></span>
                <span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;">Passport Number: 
                </span>
                <span style="width:24.91pt; text-indent:0pt;font-family:Calibri;font-size:11pt;font-weight:normal;font-style:normal; display:inline-block;">          
                </span>
                <span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;"><?=$st_data[0]['PASSPORT_CODE']?>
                </span>
            </p>
            <p class="Style_3spaceSingle">
                <span style="font: 7.0pt &quot;Times New Roman&quot;;-sf-listtab:yes;"></span>
                <span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;">Country of Passport: 
                </span>
                <span style="width:14.34pt; text-indent:0pt;font-family:Calibri;font-size:11pt;font-weight:normal;font-style:normal; display:inline-block;">     
                </span>
                <span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;"><?=$st_data[0]['COUNTRY_NAME']?>
                </span>
            </p>
            <p class="Style_3spaceSingle">
                <span style="font: 7.0pt &quot;Times New Roman&quot;;-sf-listtab:yes;"></span>
                <span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;">Email Address: </span>
                <span style="width:3.92pt; text-indent:0pt;font-family:Calibri;font-size:11pt;font-weight:normal;font-style:normal; display:inline-block;"> 
                </span>
                <span style="width:36pt; text-indent:0pt;font-family:Calibri;font-size:11pt;font-weight:normal;font-style:normal; display:inline-block;">              
                </span>
                <span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;"><?=$st_data[0]['L_EMAIL']?>
                </span>
                <span style="width:23.37pt; text-indent:0pt;font-family:Calibri;font-size:11pt;font-weight:normal;font-style:normal; display:inline-block;">         
                </span>
                <span style="width:36pt; text-indent:0pt;font-family:Calibri;font-size:11pt;font-weight:normal;font-style:normal; display:inline-block;">              
                </span>
            </p>
            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;line-height:normal;margin-top:0pt;margin-bottom:0pt;">
            	<span style="font-family:Arial;font-size:10pt;text-transform:none;font-weight:bold;font-style:normal;font-variant:normal;">&#xa0;
            	</span>
            </p>
            <h2>
            	<span style="color:#032348;font-family:Century Gothic;font-size:13pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">Offer Details 
            	</span>
            </h2>
			<?php
			/*if($st_data[0]['CUSTOMER_NO'])
			{
				$of_cond="where `CUSTOMER_NO`=".$st_data[0]['CUSTOMER_NO']." and `STATUS_NO`=".'3';
				$of_data= getRows($of_cond,'offer');
				if($of_data[0]['OFFER_NO'])
			        { */
					if($_POST['OFFER_NO'])
			           {
						$of_conda="where `OFFER_NO`=".$_POST['OFFER_NO'];
				        $of_dataa= getRows($of_conda,'offer_item');
					    foreach($of_dataa as $of_item)
						 {
							if($of_item['PRODUCT_NO'])
								{
									$cr_cond="where `PRODUCT_NO`=".$of_item['PRODUCT_NO'];
            						$cr_data= getRows($cr_cond,'course');
									foreach($cr_data as $cr_item)
										{
			?>
            <p class="Style_3spaceSingle">
                <span style="font: 7.0pt &quot;Times New Roman&quot;;-sf-listtab:yes;"></span>
                <span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;">Offer No: 
                </span>
                <span style="width:28.45pt; text-indent:0pt;font-family:Calibri;font-size:11pt;font-weight:normal;font-style:normal; display:inline-block;">           
                </span>
                <span style="width:36pt; text-indent:0pt;font-family:Calibri;font-size:11pt;font-weight:normal;font-style:normal; display:inline-block;">              
                </span>
                <span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;"><?=$of_item['OFFER_NO']?>
                </span>
            </p>
            <p class="Style_3spaceSingle">
                <span style="font: 7.0pt &quot;Times New Roman&quot;;-sf-listtab:yes;"></span>
                <a id="CourseData"></a>
                <span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;">Course Name: </span>
                <span style="width:6.59pt; text-indent:0pt;font-family:Calibri;font-size:11pt;font-weight:normal;font-style:normal; display:inline-block;">  
                </span>
                <span style="width:36pt; text-indent:0pt;font-family:Calibri;font-size:11pt;font-weight:normal;font-style:normal; display:inline-block;">              
                </span>
                <span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;"><?=$cr_item['CODE']?>
                </span>
                <span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;"><?=$cr_item['COURSE_NAME']?>
                </span>
            </p>
            <p class="Style_3spaceSingle">
                <span style="font: 7.0pt &quot;Times New Roman&quot;;-sf-listtab:yes;"></span>
                <span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;">CRICOS Code: 
                </span>
                <span style="width:8.45pt; text-indent:0pt;font-family:Calibri;font-size:11pt;font-weight:normal;font-style:normal; display:inline-block;">   
                </span>
                <span style="width:36pt; text-indent:0pt;font-family:Calibri;font-size:11pt;font-weight:normal;font-style:normal; display:inline-block;">              
                </span>
                <span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;"><?=$cr_item['CRICOS_CODE']?>
                </span>

            </p>
            <p class="Style_3spaceSingle">
                <span style="font: 7.0pt &quot;Times New Roman&quot;;-sf-listtab:yes;"></span>
                <span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;">Proposed Start Date:
                </span>
                <span style="width:15.44pt; text-indent:0pt;font-family:Calibri;font-size:11pt;font-weight:normal;font-style:normal; display:inline-block;">      
                </span>
                <span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;"><?=$of_item['FROM_DATE']?>
                </span>
                <span style="width:20.66pt; text-indent:0pt;font-family:Calibri;font-size:11pt;font-weight:normal;font-style:normal; display:inline-block;">        
                </span>
                <span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;">Proposed End Date: 
                </span>
                <span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;"><?=$of_item['TO_DATE']?>
                </span>
            </p>
            <p class="Style_3spaceSingle">
                <span style="font: 7.0pt &quot;Times New Roman&quot;;-sf-listtab:yes;"></span>
                <span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;">Tuition Duration:
                </span>
                <span style="width:32.85pt; text-indent:0pt;font-family:Calibri;font-size:11pt;font-weight:normal;font-style:normal; display:inline-block;">             
                </span>

                <span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;"><?=$cr_item['COURSE_LEN']?>
                </span>
                <span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;"> weeks
                </span>
            </p>
            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;line-height:normal;margin-top:0pt;margin-bottom:0pt;">
            	<span style="font-family:Arial;font-size:9pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;">&#xa0;
            	</span>
            </p>
			<?php } }  } }?>
            <br style='clear:both;page-break-before:always'/>
            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;line-height:normal;margin-top:12pt;margin-bottom:0pt;margin-left:135pt;text-indent:-135pt;">
            </p>
            <div>
                <table cellspacing="0" style="width: auto; border-collapse: collapse; ">
                    <tr style="height: 2px">
                        <td style="vertical-align:top;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:169.8px;">
                            <p class="Style_3spaceSingle"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;">Conditions of Enrolment</span></p>
                        </td>
                        <td style="vertical-align:top;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:468.7333px;">
                            <p class="Style_3spaceSingle"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;">Verification of Original academic documents and IELTS result or equivalent.</span></p>
                        </td>
                    </tr>
                    <tr style="height: 2px">
                        <td style="vertical-align:top;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:169.8px;">
                            <p class="Style_3spaceSingle"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;">Offer Expiry Date</span></p>
                        </td>
                        <td style="vertical-align:top;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:468.7333px;">
                            <p class="Style_3spaceSingle"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;">14 days from this offer letter dated</span></p>
                        </td>
                    </tr>
                    <tr style="height: 2px">
                        <td style="vertical-align:top;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:169.8px;">
                            <p class="Style_3spaceSingle"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;">Orientation Date</span></p>
                        </td>
                        <td style="vertical-align:top;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:468.7333px;">
                            <p class="Style_3spaceSingle">
                                <span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;"><?php echo $dt=date('Y-m-d',strtotime( '-14 day' , strtotime ( $of_item['TO_DATE'])));?></span>
                            </p>
                        </td>
                    </tr>
                </table>
            </div>
            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;line-height:normal;margin-top:0pt;margin-bottom:0pt;">
            	<span style="font-family:Arial;font-size:9pt;text-transform:none;font-weight:bold;font-style:normal;font-variant:normal;">&#xa0;
            	</span>
            </p>
            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;line-height:115%;margin-top:0pt;margin-bottom:10pt;">
            	<span class="Default-Paragraph-Font Intense-Reference" style="font-size:11pt;line-height:115%;">Special Condition:
            	</span>
            </p>
            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;line-height:115%;margin-top:0pt;margin-bottom:10pt;">
            	<span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;mso-spacerun:yes;">Upon commencement of the course if the trainer finds that you are having difficulty understanding English, you must agree to undertake a four (4) week (or as determined by the Academic Manager) English course at an additional cost to be paid by you. This requirement must be undertaken prior to continuation of your selected course.   
            	</span>
            </p>
            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;line-height:115%;margin-top:0pt;margin-bottom:10pt;">
	            <span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">Please note that all international students must hold for Overseas Student Health Cover (OSHC) insurance. It must be maintained for the duration of your student visa. 
	            </span>
            </p>
            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;line-height:115%;margin-top:0pt;margin-bottom:10pt;">
            	<span style="color:#000000;font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">If your written acceptance is not received on or before the offer expiry date, your place in the course will be forfeited. To accept this offer of admission you must follow the instructions outlined in the document &quot;Acceptance of Offer Procedures for International Students” Studying in Australia. The student services unit at ACCIT organises an orientation and enrolment program for all new students, which you are required to attend. Please note the orientation dated above and will start at 11.00 am at ACCIT, Level 2 495 Kent St, Sydney 2000 NSW. Details of this program will be forwarded to you upon your Acceptance of Offer. 
            	</span>
            </p>
            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;line-height:115%;margin-top:0pt;margin-bottom:10pt;">
            	<span style="color:#000000;font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">Please read ACCIT polices and procedure carefully before accepting this offer. A
            	</span>
            	<span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">fter receiving payment as per invoice, you will be receiving an Electronic Confirmation of Enrolment (eCOE). This eCoE is mandatory document required to obtain a Student visa from Australian High Commission.
            	</span>
            </p>
            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;line-height:115%;margin-top:0pt;margin-bottom:10pt;">
            	<span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">Thank you for choosing ACCIT and we are looking forward to meeting you at ACCIT. 
            	</span>
            </p>
            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;line-height:115%;margin-top:0pt;margin-bottom:10pt;">
            	<span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">If you have any further information regarding your enrolment, please do not hesitate to contact me.
            	</span>
            </p>
            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;line-height:115%;margin-top:0pt;margin-bottom:10pt;">
            	<span style="color:#000000;font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">*All information, including fee information, is correct at time of writing, but it may be subject to change. 
            	</span>
            </p>
            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;line-height:115%;margin-top:0pt;margin-bottom:10pt;">
            	<span style="color:#000000;font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">&#xa0;</span>
            </p>
            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;line-height:115%;margin-top:0pt;margin-bottom:10pt;">
            	<span style="color:#000000;font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">Yours Sincerely
            	</span>
            	<span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">,</span>
            </p>
            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;line-height:115%;margin-top:0pt;margin-bottom:10pt;">
            	<span style="position:Absolute; width:91px; height:38px; left:0px; margin-left:2px; margin-top:0px;z-index:1024">
            		<img src="../images/TDIcaGvQ_img3.jpeg" width="91" height="38" alt="Description: Description: Description: Timothy signature" />
            	</span>
            </p>
            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;line-height:115%;margin-top:0pt;margin-bottom:10pt;"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">Registrar</span></p>
            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;line-height:115%;margin-top:0pt;margin-bottom:10pt;"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">Timothy Naraiyanansamy</span></p>
            <h1><span style="color:#032348;font-family:Century Gothic;font-size:16pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">Gross Tax Invoice</span></h1>
            <p style="text-align:right;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;line-height:normal;margin-top:0pt;margin-bottom:0pt;"><span style="font-family:Arial;font-size:10pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;">&#xa0;</span></p>
            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;line-height:normal;margin-top:0pt;margin-bottom:0pt;"><span style="font-family:Arial;font-size:10pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;">&#xa0;</span></p>
            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;line-height:115%;margin-top:0pt;margin-bottom:0pt;">
                <span style="font: 7.0pt &quot;Times New Roman&quot;;-sf-listtab:yes;"></span>
                <span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">Offer No: 
                </span>
                <span style="width:28.45pt; text-indent:0pt;font-family:Calibri;font-size:11pt;font-weight:normal;line-height:115%;font-style:normal; display:inline-block;">           
                </span>
                <span style="width:36pt; text-indent:0pt;font-family:Calibri;font-size:11pt;font-weight:normal;line-height:115%;font-style:normal; display:inline-block;">              
                </span>
				<?php
			/*if($st_data[0]['CUSTOMER_NO'])
			{
				$of_cond="where `CUSTOMER_NO`=".$st_data[0]['CUSTOMER_NO']." and `STATUS_NO`=".'3';
				$of_data= getRows($of_cond,'offer');
				if($of_data[0]['OFFER_NO'])
			        { */
					if($_POST['OFFER_NO'])
			           {
						$offer_cond="where `OFFER_NO`=".$_POST['OFFER_NO'];
				        $offer_data= getRows($offer_cond,'offer_item');
							if($offer_data[0]['PRODUCT_NO'])
								{
									$course_cond="where `PRODUCT_NO`=".$offer_data[0]['PRODUCT_NO'];
            						$course_data= getRows($course_cond,'course');
									}
					    }				
			?>
                <span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;"><?=$offer_data[0]['OFFER_NO']?>
                </span>
            </p>
            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;line-height:115%;margin-top:0pt;margin-bottom:10pt;">
                <span style="font: 7.0pt &quot;Times New Roman&quot;;-sf-listtab:yes;"></span>
                <span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">Course Name: 
                </span>
                <span style="width:6.59pt; text-indent:0pt;font-family:Calibri;font-size:11pt;font-weight:normal;line-height:115%;font-style:normal; display:inline-block;">  
                </span>
                <span style="width:36pt; text-indent:0pt;font-family:Calibri;font-size:11pt;font-weight:normal;line-height:115%;font-style:normal; display:inline-block;">              
                </span>
                <span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;"><?=$course_data[0]['CODE']?>
                </span>
                <span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;"><?=$course_data[0]['COURSE_NAME']?>
                </span>
                <span style="width:18.35pt; text-indent:0pt;font-family:Calibri;font-size:11pt;font-weight:normal;line-height:115%;font-style:normal; display:inline-block;">       
                </span>
                <span style="width:36pt; text-indent:0pt;font-family:Calibri;font-size:11pt;font-weight:normal;line-height:115%;font-style:normal; display:inline-block;">              
                </span>
            </p>
            <div>
                <table cellspacing="0" style="width: 100%; border-collapse: collapse; ">
                    <tr style="height: 18px">
                        <td style="vertical-align:top;border-top-style:solid;border-top-color:#167AF3;border-top-width:1pt;border-left-style:solid;border-left-color:#167AF3;border-left-width:1pt;border-right-style:solid;border-right-color:#167AF3;border-right-width:1pt;border-bottom-style:solid;border-bottom-color:#167AF3;border-bottom-width:1.5pt;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:491.8119px;">
                            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;line-height:normal;margin-top:3pt;margin-bottom:3pt;"><span class="Default-Paragraph-Font Strong" style="color:#032348;font-size:11pt;font-weight:normal;">FEE NAME</span></p>
                        </td>
                        <td style="vertical-align:top;border-top-style:solid;border-top-color:#167AF3;border-top-width:1pt;border-left-style:solid;border-left-color:#167AF3;border-left-width:1pt;border-right-style:solid;border-right-color:#167AF3;border-right-width:1pt;border-bottom-style:solid;border-bottom-color:#167AF3;border-bottom-width:1.5pt;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:161.7879px;">
                            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;line-height:normal;margin-top:3pt;margin-bottom:3pt;"><span class="Default-Paragraph-Font Strong" style="color:#032348;font-size:11pt;font-weight:normal;">AMOUNT</span></p>
                        </td>
                    </tr>
					 <?php
					/* if($st_data[0]['CUSTOMER_NO']){
                                                $cond3=" where `CUSTOMER_NO`=".$st_data[0]['CUSTOMER_NO']." and `STATUS_NO`=".'3';
                                                $std_row3=getRows($cond3,'offer');
												if($std_row3[0]['OFFER_NO'])
												{ */
												if($_POST['OFFER_NO'])
			                                    {
												$cond3a=" where `OFFER_NO`=".$_POST['OFFER_NO']." ORDER BY `FROM_DATE` ASC";
                                                $std_row3a=getRows($cond3a,'offer_item');
                                                foreach($std_row3a as $std_row4)
                                                 {
                                                ?>
                    <tr style="height: 18px">
					<?php
					if($std_row4['PRODUCT_TYPE_NO']=='1')
					  {
					?>
                        <td style="vertical-align:top;background-color:#B1D2FB;border-top-style:solid;border-top-color:#167AF3;border-top-width:1pt;border-left-style:solid;border-left-color:#167AF3;border-left-width:1pt;border-right-style:solid;border-right-color:#167AF3;border-right-width:1pt;border-bottom-style:solid;border-bottom-color:#167AF3;border-bottom-width:1pt;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:491.8119px;">
                            <p class="No-Spacing" style="margin-top:3pt;margin-bottom:3pt;">
                                <span style="color:#032348;font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;"><?=$std_row4['DESCRIPTION']?> , STARTS: <?=$std_row4['FROM_DATE']?> , END: <?=$std_row4['TO_DATE']?> Length: <?=$std_row4['PERIOD_LENGTH']?> Weeks , 20 Lessons per week</span>
                            </p>
                        </td>
						<?php }
						else
						{
						?>
						<td style="vertical-align:top;background-color:#B1D2FB;border-top-style:solid;border-top-color:#167AF3;border-top-width:1pt;border-left-style:solid;border-left-color:#167AF3;border-left-width:1pt;border-right-style:solid;border-right-color:#167AF3;border-right-width:1pt;border-bottom-style:solid;border-bottom-color:#167AF3;border-bottom-width:1pt;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:491.8119px;">
                            <p class="No-Spacing" style="margin-top:3pt;margin-bottom:3pt;">
                                <span style="color:#032348;font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;"><?=$std_row4['DESCRIPTION']?></span>
                            </p>
                        </td>
						<?php } ?>
                        <td style="vertical-align:top;background-color:#B1D2FB;border-top-style:solid;border-top-color:#167AF3;border-top-width:1pt;border-left-style:solid;border-left-color:#167AF3;border-left-width:1pt;border-right-style:solid;border-right-color:#167AF3;border-right-width:1pt;border-bottom-style:solid;border-bottom-color:#167AF3;border-bottom-width:1pt;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:161.7879px;">
                            <p class="No-Spacing" style="margin-top:3pt;margin-bottom:3pt;"><span style="color:#032348;font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;"><?=$std_row4['AMOUNT']?>
							</span></p>
                        </td>
                    </tr>
					<?php } } ?>
                    <tr style="height:0px;">
                        <td style="width:491.8133px;border:none;padding:0pt;" />
                        <td style="width:161.7867px;border:none;padding:0pt;" />
                    </tr>
                </table>
            </div>
            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;line-height:115%;margin-top:0pt;margin-bottom:0pt;"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:small-caps;line-height:115%;">&#xa0;</span></p>
            <div>
                <table align="right" cellspacing="0" style="width: 241.25pt; border-collapse: collapse; ">
                    <tr style="height: 18px">
                        <td style="vertical-align:top;border-top-style:solid;border-top-color:#167AF3;border-top-width:1pt;border-left-style:solid;border-left-color:#167AF3;border-left-width:1pt;border-right-style:solid;border-right-color:#167AF3;border-right-width:1pt;border-bottom-style:solid;border-bottom-color:#167AF3;border-bottom-width:1pt;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:161.0667px;">
                            <p class="No-Spacing" style="margin-top:3pt;margin-bottom:3pt;">
                            	<span class="Default-Paragraph-Font Strong" style="color:#032348;font-size:11pt;">TOTAL:
                            	</span>
                            </p>
                        </td>
                        <td style="vertical-align:top;border-top-style:solid;border-top-color:#167AF3;border-top-width:1pt;border-left-style:solid;border-left-color:#167AF3;border-left-width:1pt;border-right-style:solid;border-right-color:#167AF3;border-right-width:1pt;border-bottom-style:solid;border-bottom-color:#167AF3;border-bottom-width:1pt;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:160.6px;">
                            <p class="No-Spacing" style="margin-top:3pt;margin-bottom:3pt;">
							<?php $add=mysql_query("SELECT SUM(AMOUNT),SUM(COMMISSION_AMOUNT),SUM(DISC_AMOUNT),SUM(SUB_AMOUNT),SUM(TAX_AMOUNT),SUM(TOTAL_AMOUNT) from `offer_item` where `OFFER_NO`=".$std_row4['OFFER_NO']);
                                                $row1=mysql_fetch_array($add);
                                                $mark=$row1['SUM(TOTAL_AMOUNT)']; 
												?>
                                <span class="Default-Paragraph-Font Strong" style="color:#032348;font-size:11pt;"><?php echo $mark;?></span>
                            </p>
                        </td>
                    </tr>
                </table>
            </div>
            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;line-height:115%;margin-top:0pt;margin-bottom:10pt;">
            	<span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:small-caps;line-height:115%;">&#xa0;</span>
            </p>
            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;line-height:115%;margin-top:0pt;margin-bottom:10pt;">
            	<span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:small-caps;line-height:115%;">Study Period</span>
            </p>
            <div>
                <table cellspacing="0" style="width: 100%; border-collapse: collapse; ">
                    <tr style="height: 2px">
                        <td style="vertical-align:top;border-top-style:solid;border-top-color:#167AF3;border-top-width:1pt;border-left-style:solid;border-left-color:#167AF3;border-left-width:1pt;border-right-style:solid;border-right-color:#167AF3;border-right-width:1pt;border-bottom-style:solid;border-bottom-color:#167AF3;border-bottom-width:1.5pt;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:329.9333px;">
                            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;margin-top:3pt;margin-bottom:3pt;"><span style="color:#032348;font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;">PAYMENT DETAILS</span></p>
                        </td>
                        <td style="vertical-align:top;border-top-style:solid;border-top-color:#167AF3;border-top-width:1pt;border-left-style:solid;border-left-color:#167AF3;border-left-width:1pt;border-right-style:solid;border-right-color:#167AF3;border-right-width:1pt;border-bottom-style:solid;border-bottom-color:#167AF3;border-bottom-width:1.5pt;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:159.7333px;">
                            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;margin-top:3pt;margin-bottom:3pt;"><span style="color:#032348;font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;">INSTALLMENT DATE</span></p>
                        </td>
                        <td style="vertical-align:top;border-top-style:solid;border-top-color:#167AF3;border-top-width:1pt;border-left-style:solid;border-left-color:#167AF3;border-left-width:1pt;border-right-style:solid;border-right-color:#167AF3;border-right-width:1pt;border-bottom-style:solid;border-bottom-color:#167AF3;border-bottom-width:1.5pt;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:163.9333px;">
                            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;margin-top:3pt;margin-bottom:3pt;"><span style="color:#032348;font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;">AMOUNT</span></p>
                        </td>
                    </tr>
					<?php 
					if($std_row4['OFFER_NO']){
                                                $cond5="where `OFFER_NO`=".$std_row4['OFFER_NO']." ORDER BY `DUE_DATE` ASC";
                                                $std_row5=getRows($cond5,'offer_instalment');
                                                foreach($std_row5 as $std_row6)
                                                {
                                                   if(!($std_row6['DELETE_DATE']))
                                                		{
												if($std_row6['OFFER_ITEM_NO']){	
                                                $cond7="where `OFFER_ITEM_NO`=".$std_row6['OFFER_ITEM_NO'];
                                                $std_row7=getRows($cond7,'offer_item');
												}
												?>
                    <tr style="height: 2px">
                        <?php
					if($std_row7[0]['PRODUCT_TYPE_NO']=='1')
					  {
					?>
                        <td style="vertical-align:top;background-color:#B1D2FB;border-top-style:solid;border-top-color:#167AF3;border-top-width:1pt;border-left-style:solid;border-left-color:#167AF3;border-left-width:1pt;border-right-style:solid;border-right-color:#167AF3;border-right-width:1pt;border-bottom-style:solid;border-bottom-color:#167AF3;border-bottom-width:1pt;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:491.8119px;">
                            <p class="No-Spacing" style="margin-top:3pt;margin-bottom:3pt;">
                                <span style="color:#032348;font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;"><?=$std_row7[0]['DESCRIPTION']?> , STARTS: <?=$std_row7[0]['FROM_DATE']?> , END: <?=$std_row7[0]['TO_DATE']?> Length: <?=$std_row7[0]['PERIOD_LENGTH']?> Weeks , 20 Lessons per week</span>
                            </p>
                        </td>
						<?php }
						else
						{
						?>
						<td style="vertical-align:top;background-color:#B1D2FB;border-top-style:solid;border-top-color:#167AF3;border-top-width:1pt;border-left-style:solid;border-left-color:#167AF3;border-left-width:1pt;border-right-style:solid;border-right-color:#167AF3;border-right-width:1pt;border-bottom-style:solid;border-bottom-color:#167AF3;border-bottom-width:1pt;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:491.8119px;">
                            <p class="No-Spacing" style="margin-top:3pt;margin-bottom:3pt;">
                                <span style="color:#032348;font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;"><?=$std_row7[0]['DESCRIPTION']?></span>
                            </p>
                        </td>
						<?php } ?>
                        <td style="vertical-align:top;background-color:#B1D2FB;border-top-style:solid;border-top-color:#167AF3;border-top-width:1pt;border-left-style:solid;border-left-color:#167AF3;border-left-width:1pt;border-right-style:solid;border-right-color:#167AF3;border-right-width:1pt;border-bottom-style:solid;border-bottom-color:#167AF3;border-bottom-width:1pt;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:159.7333px;">
                            <p class="No-Spacing" style="margin-top:3pt;margin-bottom:3pt;"><span style="color:#032348;font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:small-caps;"><?=$std_row6['DUE_DATE']?></span></p>
                        </td>
                        <td style="vertical-align:top;background-color:#B1D2FB;border-top-style:solid;border-top-color:#167AF3;border-top-width:1pt;border-left-style:solid;border-left-color:#167AF3;border-left-width:1pt;border-right-style:solid;border-right-color:#167AF3;border-right-width:1pt;border-bottom-style:solid;border-bottom-color:#167AF3;border-bottom-width:1pt;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:163.9333px;">
                            <p class="No-Spacing" style="margin-top:3pt;margin-bottom:3pt;">
                            	<span style="color:#032348;font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:small-caps;">$<?php echo $f1 = number_format($std_row6['AMOUNT'], 2, '.', ''); ?></span>
                            </p>
                        </td>
                    </tr>
					<?php } } }?>
                </table>
            </div>
            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;line-height:115%;margin-top:0pt;margin-bottom:0pt;">
            	<span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:small-caps;line-height:115%;">&#xa0;</span>
            </p>
            <div>
                <table align="right" cellspacing="0" style="width: 241.25pt; border-collapse: collapse; ">
                    <tr style="height: 18px">
                        <td style="vertical-align:top;border-top-style:solid;border-top-color:#167AF3;border-top-width:1pt;border-left-style:solid;border-left-color:#167AF3;border-left-width:1pt;border-right-style:solid;border-right-color:#167AF3;border-right-width:1pt;border-bottom-style:solid;border-bottom-color:#167AF3;border-bottom-width:1pt;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:161.0667px;">
                            <p class="No-Spacing" style="margin-top:3pt;margin-bottom:3pt;"><span class="Default-Paragraph-Font Strong" style="color:#032348;font-size:11pt;">TOTAL:</span></p>
                        </td>
                        <td style="vertical-align:top;border-top-style:solid;border-top-color:#167AF3;border-top-width:1pt;border-left-style:solid;border-left-color:#167AF3;border-left-width:1pt;border-right-style:solid;border-right-color:#167AF3;border-right-width:1pt;border-bottom-style:solid;border-bottom-color:#167AF3;border-bottom-width:1pt;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:160.6px;">
                            <p class="No-Spacing" style="margin-top:3pt;margin-bottom:3pt;">
                                <span class="Default-Paragraph-Font Strong" style="color:#032348;font-size:11pt;"><?php echo $mark;?></span>
                            </p>
                        </td>
                    </tr>
                </table>
            </div>
            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;line-height:normal;margin-top:0pt;margin-bottom:0pt;">
            	<span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:bold;font-style:normal;font-variant:normal;">&#xa0;</span>
            </p>
            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;line-height:115%;margin-top:10pt;margin-bottom:10pt;">
            	<span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">Funds are due 1 month after the date of invoice or 2 weeks prior to starting (whichever is first). The Enrolment Fee of AUD$200.00 is non-refundable under any circumstances. Any administrative charges that are paid for package offer are non-refundable if the students do not pursue their courses. </span>
            </p>
            <h2>
            	<span style="color:#032348;font-family:Century Gothic;font-size:13pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">Bank Account Details</span>
            </h2>
            <div>
                <table cellspacing="0" style="width: 100%; border-collapse: collapse; ">
                    <tr style="height: 2px">
                        <td style="vertical-align:top;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:156px;">
                            <p class="No-Spacing"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;">Account name</span></p>
                        </td>
                        <td style="vertical-align:top;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:497.6px;">
                            <p class="No-Spacing"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;">Australian College of Commerce &amp; IT (ACCIT)</span></p>
                        </td>
                    </tr>
                    <tr style="height: 2px">
                        <td style="vertical-align:top;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:156px;">
                            <p class="No-Spacing"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;">Swift Code</span></p>
                        </td>
                        <td style="vertical-align:top;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:497.6px;">
                            <p class="No-Spacing"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;">SGBLAU2S</span></p>
                        </td>
                    </tr>
                    <tr style="height: 2px">
                        <td style="vertical-align:top;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:156px;">
                            <p class="No-Spacing"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;mso-spacerun:yes;">BSB  No</span></p>
                        </td>
                        <td style="vertical-align:top;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:497.6px;">
                            <p class="No-Spacing"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;">112 879</span></p>
                        </td>
                    </tr>
                    <tr style="height: 2px">
                        <td style="vertical-align:top;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:156px;">
                            <p class="No-Spacing"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;">Account Number</span></p>
                        </td>
                        <td style="vertical-align:top;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:497.6px;">
                            <p class="No-Spacing"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;">057 822 873</span></p>
                        </td>
                    </tr>
                    <tr style="height: 2px">
                        <td style="vertical-align:top;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:156px;">
                            <p class="No-Spacing"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;">Bank Name</span></p>
                        </td>
                        <td style="vertical-align:top;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:497.6px;">
                            <p class="No-Spacing"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;">St George Bank</span></p>
                        </td>
                    </tr>
                    <tr style="height: 2px">
                        <td style="vertical-align:top;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:156px;">
                            <p class="No-Spacing"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;">Bank Address</span></p>
                        </td>
                        <td style="vertical-align:top;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:497.6px;">
                            <p class="No-Spacing"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;">4-16 Montgomery Street, Kograh, NSW 2217, Australia</span></p>
                        </td>
                    </tr>
                </table>
            </div>
            <p class="No-Spacing">
            	<span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;">&#xa0;</span>
            </p>
            <p class="No-Spacing">
            	<span class="Default-Paragraph-Font Book-Title" style="font-size:11pt;">Student offer no must be used as a reference for payments for any initial Fees.</span>
            </p>
            <p class="No-Spacing">
            	<span class="Default-Paragraph-Font Book-Title" style="font-size:11pt;">Please refer to the refund policy attached together with the offer letter.</span>
            </p>
            <br style='clear:both;page-break-before:always'/>
            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;line-height:115%;margin-top:0pt;margin-bottom:10pt;"></p>
            <h1>
            	<span style="color:#032348;font-family:Century Gothic;font-size:16pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">Acceptance of offer procedure for international students</span>
            </h1>
            <h3>
            	<span style="color:#021730;font-family:Century Gothic;font-size:12pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">Accepting the offer of admission</span>
            </h3>
            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;line-height:normal;margin-top:0pt;margin-bottom:0pt;">
            	<span style="font-family:Arial;font-size:10pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;">&#xa0;</span>
            </p>
            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;line-height:115%;margin-top:0pt;margin-bottom:10pt;">
            	<span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">To accept the offerof a place Australian College of Commerce and Information Technology (ACCIT) as an International Student, you must:
            	</span>
            </p>
            <ol type="1" start="1" style="margin:0pt; padding-left:0pt">
                <li class="List-Paragraph" style="-sf-number-width:13.32568pt;margin-left:13.3256836pt;padding-left:4.67431641pt;text-indent:0pt;color:#146194;"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">Pay ACCIT the sum detailed on the attached “Acceptance of Offer Payment Form.” Please refer to “Payment of Fees Options” for payment options.</span></li>
            </ol>
            <p class="List-Paragraph" style="margin-left:18pt;">
            	<span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">This payment covers compulsory enrolment and tuition fees. It does not cover accommodation or living expenses.
            	</span>
            </p>
            <ol type="1" start="2" style="margin:0pt; padding-left:0pt">
                <li class="List-Paragraph" style="-sf-number-width:13.32568pt;margin-left:13.3256836pt;padding-left:4.67431641pt;text-indent:0pt;color:#146194;">
                	<span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">Pay fees by the date and in the manner specified in the letter of offer or invoice. You will be referred to the nominated Debt Collection Agency for further action if you failed to pay the College fees and charges as and when they fall due.</span>
                </li>
                <li class="List-Paragraph" style="-sf-number-width:13.32568pt;margin-left:13.3256836pt;padding-left:4.67431641pt;text-indent:0pt;color:#146194;">
                	<span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">Complete the enclosed “Application for Accommodation Assistance” form if you require assistance in getting accommodation through Homestay arrangement and return it to the Admission Office. This form </span>
                	<span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:bold;font-style:normal;font-variant:normal;line-height:115%;">MUST be received by the 
                	</span>
                	<span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:bold;font-style:normal;font-variant:normal;line-height:115%;">Admission Office</span>
                	<span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:bold;font-style:normal;font-variant:normal;line-height:115%;"> at least 5 weeks prior to the commencement date quoted on your offer letter. </span>
                	<span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">If the International Office does not receive this form by this date, we will assume that you do not require any accommodation assistance. </span>
                </li>
                <li class="List-Paragraph" style="-sf-number-width:13.32568pt;margin-left:13.3256836pt;padding-left:4.67431641pt;text-indent:0pt;color:#146194;">
                	<span style="color:#000000;font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">You can pay the fees either by cash, by cheque in Australian Dollars (made payable to ‘ACCIT), debit/credit card (additional surcharge, 2% for Master Card and Visa card and 3% for American Express card is applicable) or by electronic transfer to the ACCIT College bank account which is provided in this offer letter.</span>
                </li>
                <li class="List-Paragraph" style="-sf-number-width:13.32568pt;margin-left:13.3256836pt;padding-left:4.67431641pt;text-indent:0pt;color:#146194;"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">Change of Course, Change of course commencement date and Deferral of the course request incurs a $150 administration charge subject to approval (for student applying from overseas the first request for course commencement date change is free) and RPL incurs a $50 compulsory administration charge (in addition to $120/unit, if approved)</span>
                </li>
                <li class="List-Paragraph" style="-sf-number-width:13.32568pt;margin-left:13.3256836pt;padding-left:4.67431641pt;text-indent:0pt;color:#146194;"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">Before lodging your enrolment application you should consider whether you would have enough money to cover –</span>
                </li>
            </ol>
            <ul type="disc" style="margin:0pt; padding-left:0pt">
                <li class="List-Paragraph" style="-sf-number-width:27.03271pt;margin-left:45.0327148pt;padding-left:-9.032715pt;text-indent:0pt;color:#0D89A8;font-family:Wingdings;"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">Living expenses;</span></li>
                <li class="List-Paragraph" style="-sf-number-width:27.03271pt;margin-left:45.0327148pt;padding-left:-9.032715pt;text-indent:0pt;color:#0D89A8;font-family:Wingdings;"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">Air fares; </span></li>
                <li class="List-Paragraph" style="-sf-number-width:27.03271pt;margin-left:45.0327148pt;padding-left:-9.032715pt;text-indent:0pt;color:#0D89A8;font-family:Wingdings;"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">Course tuition fees;</span></li>
                <li class="List-Paragraph" style="-sf-number-width:27.03271pt;margin-left:45.0327148pt;padding-left:-9.032715pt;text-indent:0pt;color:#0D89A8;font-family:Wingdings;"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">Overseas student health cover (OSHC) </span></li>
                <li class="List-Paragraph" style="-sf-number-width:27.03271pt;margin-left:45.0327148pt;padding-left:-9.032715pt;text-indent:0pt;color:#0D89A8;font-family:Wingdings;"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">And all general expenses including for books and others during your stay in Australia. </span></li>
            </ul>
            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;line-height:115%;margin-top:0pt;margin-bottom:10pt;margin-left:18pt;"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">The expected living costs is approximately AUD $18,610 per annum if you are single; an additional 35% per annum if you have a spouse; and a further 20% per annum for each child (plus AUD 8000 per annum per child for the cost of schooling).</span></p>
            <ol type="1" start="7" style="margin:0pt; padding-left:0pt">
                <li class="List-Paragraph" style="-sf-number-width:13.32568pt;margin-left:13.3256836pt;padding-left:4.67431641pt;text-indent:0pt;color:#146194;"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">Complete the “Acceptance of Offer” on page 4 and sign it, and return it to the following address o</span><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:bold;font-style:normal;font-variant:normal;line-height:115%;">n or before the expiry date specified on your offer </span><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">to confirm your place in the course with the following documents:</span></li>
            </ol>
            <ol type="i" start="1" style="margin:0pt; padding-left:0pt">
                <li class="List-Paragraph" style="-sf-number-width:10.2749pt;margin-left:46.2749023pt;padding-left:7.72509766pt;text-indent:0pt;color:#146194;"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">Acceptance of Offer Payment Form,</span></li>
                <li class="List-Paragraph" style="-sf-number-width:12.79932pt;margin-left:48.7993164pt;padding-left:5.20068359pt;text-indent:0pt;color:#146194;"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">Draft for payment </span></li>
                <li class="List-Paragraph" style="-sf-number-width:15.32373pt;margin-left:51.32373pt;padding-left:2.67626953pt;text-indent:0pt;color:#146194;"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">Application for Accommodation Assistance, if needed, and </span></li>
                <li class="List-Paragraph" style="-sf-number-width:14.34082pt;margin-left:50.34082pt;padding-left:3.65917969pt;text-indent:0pt;color:#146194;"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">Application for Airport Pickup, if needed.</span></li>
            </ol>
            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;line-height:115%;margin-top:0pt;margin-bottom:10pt;">
	            <span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:bold;font-style:normal;font-variant:normal;line-height:115%;">Note: </span>
	            <span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">If you are required by the DIBP(Department of Immigration and Border Protection) to complete a pre-visa assessment (PVA), you should </span>
	            <span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:bold;font-style:normal;font-variant:normal;line-height:115%;">not </span>
	            <span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">accept the offer or make payment until you have passed your PVA. The need for a PVA depends on your citizenship and the award you are pursuing in Australia. Please refer to the closest </span>
	            <span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:bold;font-style:normal;font-variant:normal;line-height:115%;">Australian GovernmentOffice </span>
	            <span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">for advice. You can find the contact details for Australian Government Offices around the world at:</span>
            </p>
            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;line-height:115%;margin-top:0pt;margin-bottom:10pt;">
            	<span style="font: 7.0pt &quot;Times New Roman&quot;;-sf-listtab:yes;"></span>
            	<span style="width:36pt; text-indent:0pt;font-family:Calibri;font-size:11pt;font-weight:normal;line-height:115%;font-style:normal; display:inline-block;">              
            	</span>
            	<span style="width:36pt; text-indent:0pt;font-family:Calibri;font-size:11pt;font-weight:normal;line-height:115%;font-style:normal; display:inline-block;">              
            	</span>
            	<a style="color:#0000FF;font-family:Arial;font-size:10pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;text-decoration: underline;line-height:115%;" href="http://www.immi.gov.au/wwi/index.htm">
            		<span class="Default-Paragraph-Font Hyperlink" style="font-family:Arial;line-height:115%;">http://www.immi.gov.au/wwi/index.htm</span>
            	</a>
            </p>
            <h3>
            	<span style="color:#021730;font-family:Century Gothic;font-size:12pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">After acceptance</span>
            </h3>
            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;line-height:normal;margin-top:0pt;margin-bottom:0pt;">
            	<span style="font-family:Arial;font-size:10pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;">&#xa0;</span>
            </p>
            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;line-height:115%;margin-top:0pt;margin-bottom:10pt;">
            	<span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">Following receipt of the completed documents and payment as indicated above, you will be issued with a Confirmation of Enrolment Form (CoE), which will allow you to obtain/renew an Australian Student Visa.</span>
            </p>
            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;line-height:115%;margin-top:0pt;margin-bottom:10pt;">
            	<span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">On your offer acceptance and payment, we will send you a pre-departure checklist. You will be asked to arrive on the campus two to three days before the Commencement Date specified on your offer letter. Orientation and enrolment commence on the Commencement Date and </span>
            	<span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:bold;font-style:normal;font-variant:normal;line-height:115%;">you are required to attend.</span>
            </p>
            <h3>
            	<span style="color:#021730;font-family:Century Gothic;font-size:12pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">Further information</span>
            </h3>
            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;line-height:115%;margin-top:0pt;margin-bottom:10pt;">
            	<span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">If you have any difficulty in complying with the acceptance arrangements or have any concerns about any aspect of your further studies in ACCIT, Staff in our </span>
            	<span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">Admission</span>
            	<span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;"> Office are available to assist, you. They can be contacted by telephone or by e-mail.</span>
            </p>
            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;line-height:115%;margin-top:0pt;margin-bottom:10pt;">
            	<span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">Please read carefully before signing “Acceptance of Offer. This Policy does not remove the right to take further action under Australia’s consumer Protection laws:</span>
            </p>
            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;line-height:normal;margin-top:0pt;margin-bottom:0pt;"></p>
            <div>
                <table cellspacing="0" style="background-color:#FFFFFF;border-collapse: collapse; ">
                    <tr style="height: 234px">
                        <td style="vertical-align:top;background-color:#FFFFFF;border-top-style:solid;border-top-color:#E87D37;border-top-width:1.25pt;border-left-style:solid;border-left-color:#E87D37;border-left-width:1.25pt;border-right-style:solid;border-right-color:#E87D37;border-right-width:1.25pt;border-bottom-style:solid;border-bottom-color:#E87D37;border-bottom-width:1.25pt;padding-left:7.087pt;padding-right:7.087pt;padding-top:3.685pt;padding-bottom:3.685pt;width:650px;">
                            <h4><span style="color:#032348;font-family:Century Gothic;font-size:11pt;text-transform:none;font-weight:normal;font-style:italic;font-variant:normal;line-height:115%;">Privacy Statement</span></h4>
                            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;line-height:115%;margin-top:0pt;margin-bottom:10pt;"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">Information is collected on this form and during your enrolment in order to meet our obligations under the ESOS Act and the National Code 2007; to ensure student compliance with the conditions of their visas and their obligations under Australian immigration laws generally. The authority to collect this information is contained in the Education Services for Overseas Students Act 2000, the Education Services for Overseas Students Regulations 2001 and the National Code of Practice for Registration Authorities and Providers of Education and Training to Overseas Students 2007. Information collected about you on this form and during your enrolment can be provided, in certain circumstances, to the Australian Government and designated authorities and in other instances information collected on this form or during your enrolment can be disclosed without your consent where authorised or required by law. </span></p>
                            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;line-height:115%;margin-top:0pt;margin-bottom:10pt;"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">&#xa0;</span></p>
                        </td>
                    </tr>
                </table>
            </div>
            </p>
            <br style='clear:both;page-break-before:always'/>
            <p style="text-align:left;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;line-height:normal;margin-top:0pt;margin-bottom:0pt;" />
            <h1><span style="color:#032348;font-family:Century Gothic;font-size:16pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">Payment of fees options</span></h1>
            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;line-height:115%;margin-top:0pt;margin-bottom:10pt;"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">* Payment must be made in Australian dollars</span></p>
            <h4><span style="color:#032348;font-family:Century Gothic;font-size:11pt;text-transform:none;font-weight:normal;font-style:italic;font-variant:normal;line-height:115%;">Pay by Telegraphic Transfer</span></h4>
            <div>
                <table cellspacing="0" style="width: 100%; border-collapse: collapse; ">
                    <tr style="height: 2px">
                        <td style="vertical-align:top;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:156px;">
                            <p class="No-Spacing"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;">Account name</span></p>
                        </td>
                        <td style="vertical-align:top;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:497.6px;">
                            <p class="No-Spacing"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;">Australian College of Commerce &amp; IT (ACCIT)</span></p>
                        </td>
                    </tr>
                    <tr style="height: 2px">
                        <td style="vertical-align:top;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:156px;">
                            <p class="No-Spacing"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;">Swift Code</span></p>
                        </td>
                        <td style="vertical-align:top;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:497.6px;">
                            <p class="No-Spacing"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;">SGBLAU2S</span></p>
                        </td>
                    </tr>
                    <tr style="height: 2px">
                        <td style="vertical-align:top;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:156px;">
                            <p class="No-Spacing"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;mso-spacerun:yes;">BSB  No</span></p>
                        </td>
                        <td style="vertical-align:top;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:497.6px;">
                            <p class="No-Spacing"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;">112 879</span></p>
                        </td>
                    </tr>
                    <tr style="height: 2px">
                        <td style="vertical-align:top;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:156px;">
                            <p class="No-Spacing"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;">Account Number</span></p>
                        </td>
                        <td style="vertical-align:top;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:497.6px;">
                            <p class="No-Spacing"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;">057 822 873</span></p>
                        </td>
                    </tr>
                    <tr style="height: 2px">
                        <td style="vertical-align:top;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:156px;">
                            <p class="No-Spacing"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;">Bank Name</span></p>
                        </td>
                        <td style="vertical-align:top;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:497.6px;">
                            <p class="No-Spacing"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;">St George Bank</span></p>
                        </td>
                    </tr>
                    <tr style="height: 2px">
                        <td style="vertical-align:top;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:156px;">
                            <p class="No-Spacing"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;">Bank Address</span></p>
                        </td>
                        <td style="vertical-align:top;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:497.6px;">
                            <p class="No-Spacing"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;">4-16 Montgomery Street, Kograh, NSW 2217, Australia</span></p>
                        </td>
                    </tr>
                </table>
            </div>
            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;line-height:115%;margin-top:0pt;margin-bottom:10pt;"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">&#xa0;</span></p>
            <h4><span style="color:#032348;font-family:Century Gothic;font-size:11pt;text-transform:none;font-weight:normal;font-style:italic;font-variant:normal;line-height:115%;">Payment by mail</span></h4>
            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;line-height:115%;margin-top:0pt;margin-bottom:10pt;"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">Enclose a copy of your invoice, or provide your student details, including Invoice Number and Student ID Number, and return with bank draft (payable to the Australian College of Commerce and IT) to:</span></p>
            <p class="No-Spacing" style="text-indent:36pt;"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;">Att: Registrar</span></p>
            <p class="No-Spacing"><span style="font: 7.0pt &quot;Times New Roman&quot;;-sf-listtab:yes;">                    </span><span style="width:36pt; text-indent:0pt;font-family:Calibri;font-size:11pt;font-weight:normal;font-style:normal; display:inline-block;">              </span><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;">Australian College of Commerce and IT</span></p>
            <p class="No-Spacing"><span style="font: 7.0pt &quot;Times New Roman&quot;;-sf-listtab:yes;">                    </span><span style="width:36pt; text-indent:0pt;font-family:Calibri;font-size:11pt;font-weight:normal;font-style:normal; display:inline-block;">              </span><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;">Level 3, 495 Kent St,</span></p>
            <p class="No-Spacing"><span style="font: 7.0pt &quot;Times New Roman&quot;;-sf-listtab:yes;">                    </span><span style="width:36pt; text-indent:0pt;font-family:Calibri;font-size:11pt;font-weight:normal;font-style:normal; display:inline-block;">              </span><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;">Sydney, NSW 2000. Australia.</span></p>
            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;line-height:normal;margin-top:0pt;margin-bottom:0pt;margin-left:18pt;"><span style="font-family:Arial;font-size:10pt;text-transform:none;font-weight:bold;font-style:normal;font-variant:normal;">&#xa0;</span></p>
            <h4><span style="color:#032348;font-family:Century Gothic;font-size:11pt;text-transform:none;font-weight:normal;font-style:italic;font-variant:normal;line-height:115%;">Payment in person</span></h4>
            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;line-height:115%;margin-top:0pt;margin-bottom:10pt;"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">This option is only available to students who have applied for admission to ACCIT from within Australia. </span><span style="font-family:Arial;font-size:10pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">Please present your invoice with your payment to the Accounts section at the campus.</span></p>
            <h1><span style="color:#032348;font-family:Century Gothic;font-size:16pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">Acceptance of offer</span></h1>
            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;line-height:normal;margin-top:0pt;margin-bottom:0pt;"><span style="font-family:Arial;font-size:10pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;">&#xa0;</span></p>
            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;line-height:115%;margin-top:0pt;margin-bottom:10pt;">
                <span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">I </span> <span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">Kohli</span><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;"> acknowledge receipt of an Offer of Admission for International Students and I accept your offer of Admission to the course indicated below:</span>
            </p>
            <h2><span style="color:#032348;font-family:Century Gothic;font-size:13pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">Please complete all details very clearly below:</span></h2>
            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;line-height:normal;margin-top:0pt;margin-bottom:0pt;"><span style="font-family:Arial;font-size:12pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;">&#xa0;</span></p>
            <div>
                <table cellspacing="0" style="width: 100%; border-collapse: collapse; ">
                    <tr style="height: 5.33333349px">
                        <td style="vertical-align:top;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:183.1333px;">
                            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;margin-top:3pt;margin-bottom:3pt;"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;">Student Number</span></p>
                        </td>
                        <td style="vertical-align:top;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:470.4667px;" colspan="4">
                            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;margin-top:3pt;margin-bottom:3pt;">
                                <span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;"><?=$st_data[0]['EXT_STN']?></span>
                            </p>
                        </td>
                    </tr>
                    <tr style="height: 2px">
                        <td style="vertical-align:top;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:183.1333px;">
                            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;margin-top:3pt;margin-bottom:3pt;"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;">Family Name</span></p>
                        </td>
                        <td style="vertical-align:top;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:470.4667px;" colspan="4">
                            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;margin-top:3pt;margin-bottom:3pt;">
                                <span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;"><?=$st_data[0]['FNAME']?></span>
                            </p>
                        </td>
                    </tr>
                    <tr style="height: 2px">
                        <td style="vertical-align:top;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:183.1333px;">
                            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;margin-top:3pt;margin-bottom:3pt;"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;">Given Name</span></p>
                        </td>
                        <td style="vertical-align:top;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:213px;">
                            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;margin-top:3pt;margin-bottom:3pt;"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;"><?=$st_data[0]['LNAME']?></span><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;mso-spacerun:yes;">  </span>
                            </p>
                        </td>
                        <td style="vertical-align:top;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:94.06667px;">
                            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;margin-top:3pt;margin-bottom:3pt;"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;">Title</span></p>
                        </td>
						<?php 
							if($st_data[0]['TITLE'] == '1')
							 { 
							 $t="Mr";
							 }
							 else if($st_data[0]['TITLE'] == '2')
							 { 
							 $t="Dr";
							 }
							 else if($st_data[0]['TITLE'] == '3')
							 { 
							 $t="Sir";
							 }
							 else if($st_data[0]['TITLE'] == '4')
							 { 
							 $t="Ms";
							 }
							 else if($st_data[0]['TITLE'] == '5')
							 { 
							 $t="Mrs";
							 }
							 else if($st_data[0]['TITLE'] == '6')
							 { 
							 $t="Miss";
							 }
							 ?>
                        <td style="vertical-align:top;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:163.4px;" colspan="2">
                            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;margin-top:3pt;margin-bottom:3pt;">
                                <span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;"><?php echo $t;?></span>
                            </p>
                        </td>
                    </tr>
                    <tr style="height: 2px">
                        <td style="vertical-align:top;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:183.1333px;">
                            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;margin-top:3pt;margin-bottom:3pt;"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;">Date of Birth</span></p>
                        </td>
                        <td style="vertical-align:top;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:470.4667px;" colspan="4">
                            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;margin-top:3pt;margin-bottom:3pt;">
                                <span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;"><?=$st_data[0]['DOB']?></span>
                            </p>
                        </td>
                    </tr>
                    <tr style="height: 2px">
                        <td style="vertical-align:top;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:183.1333px;">
                            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;margin-top:3pt;margin-bottom:3pt;"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;">Passport Number</span></p>
                        </td>
                        <td style="vertical-align:top;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:213px;">
                            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;margin-top:3pt;margin-bottom:3pt;">
                                <span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;"><?=$st_data[0]['PASSPORT_NO']?></span>
                            </p>
                        </td>
                        <td style="vertical-align:top;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:94.06667px;">
                            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;margin-top:3pt;margin-bottom:3pt;"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;">Citizenship</span></p>
                        </td>
                        <td style="vertical-align:top;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:163.4px;" colspan="2">
                            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;margin-top:3pt;margin-bottom:3pt;">
                                <span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;"><?=$st_data[0]['COUNTRY_NAME']?></span>
                            </p>
                        </td>
                    </tr>
                    <tr style="height: 2px">
                        <td style="vertical-align:middle;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:183.1333px;">
                            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;margin-top:3pt;margin-bottom:3pt;"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;">Address</span></p>
                        </td>
                        <td style="vertical-align:top;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:470.4667px;" colspan="4">
                            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;margin-top:3pt;margin-bottom:3pt;">
                                <span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;"><?=$st_data[0]['LADDR1']?></span>
                            </p>
                        </td>
                    </tr>
                    <tr style="height: 2px">
                        <td style="vertical-align:middle;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:183.1333px;">
                            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;margin-top:3pt;margin-bottom:3pt;"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;">State</span></p>
                        </td>
                        <td style="vertical-align:top;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:213px;">
                            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;margin-top:3pt;margin-bottom:3pt;">
                                <span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;"><?=$st_data[0]['LSTATE']?></span>
                            </p>
                        </td>
                        <td style="vertical-align:top;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:96.73334px;" colspan="2">
                            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;margin-top:3pt;margin-bottom:3pt;"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;">Postcode</span></p>
                        </td>
                        <td style="vertical-align:top;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:160.7333px;">
                            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;margin-top:3pt;margin-bottom:3pt;">
                                <span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;"><?=$st_data[0]['LPCODE']?></span>
                            </p>
                        </td>
                    </tr>
                    <tr style="height: 2px">
                        <td style="vertical-align:top;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:183.1333px;">
                            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;margin-top:3pt;margin-bottom:3pt;"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;">Telephone number</span></p>
                        </td>
                        <td style="vertical-align:top;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:470.4667px;" colspan="4">
                            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;margin-top:3pt;margin-bottom:3pt;">
                                <span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;"><?=$st_data[0]['L_MOBILE']?></span>
                            </p>
                        </td>
                    </tr>
                    <tr style="height: 2px">
                        <td style="vertical-align:top;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:183.1333px;">
                            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;margin-top:3pt;margin-bottom:3pt;"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;">Email address</span></p>
                        </td>
                        <td style="vertical-align:top;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:470.4667px;" colspan="4">
                            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;margin-top:3pt;margin-bottom:3pt;">
                                <span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;"><?=$st_data[0]['L_EMAIL']?></span>
                            </p>
                        </td>
                    </tr>
                    <tr style="height:0px;">
                        <td style="width:183.1333px;border:none;padding:0pt;" />
                        <td style="width:213px;border:none;padding:0pt;" />
                        <td style="width:94.06665px;border:none;padding:0pt;" />
                        <td style="width:2.666667px;border:none;padding:0pt;" />
                        <td style="width:160.7334px;border:none;padding:0pt;" />
                    </tr>
                </table>
            </div>
            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;line-height:normal;margin-top:0pt;margin-bottom:0pt;"><span style="font-family:Arial;font-size:12pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;">&#xa0;</span></p>
            <h4><span style="color:#032348;font-family:Century Gothic;font-size:11pt;text-transform:none;font-weight:normal;font-style:italic;font-variant:normal;line-height:115%;">Payment details</span></h4>
            <div>
                <table cellspacing="0" style="width: 100%; border-collapse: collapse; ">
                    <tr style="height: 11.8666658px">
                        <td style="vertical-align:top;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:35.53333px;">
                            <p style="text-align:center;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;margin-top:6pt;margin-bottom:0pt;"><font face="Wingdings">&#113;</font></p>
                        </td>
                        <td style="vertical-align:top;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:148.1333px;">
                            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;margin-top:6pt;margin-bottom:0pt;"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;">Telegraphic transfer</span></p>
                        </td>
                        <td style="vertical-align:top;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:160.4667px;">
                            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;margin-top:6pt;margin-bottom:0pt;"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;">Amount transferred</span></p>
                        </td>
                        <td style="vertical-align:top;border-bottom-style:solid;border-bottom-width:1pt;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:139.0667px;">
                            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;margin-top:6pt;margin-bottom:0pt;"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;">&#xa0;</span></p>
                        </td>
                        <td style="vertical-align:top;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:19.26667px;">
                            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;margin-top:6pt;margin-bottom:0pt;"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;">&#xa0;</span></p>
                        </td>
                        <td style="vertical-align:top;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:48.33333px;">
                            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;margin-top:6pt;margin-bottom:0pt;"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;">Date</span></p>
                        </td>
                        <td style="vertical-align:top;border-bottom-style:solid;border-bottom-width:1pt;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:102.8px;">
                            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;margin-top:6pt;margin-bottom:0pt;"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;">&#xa0;</span></p>
                        </td>
                    </tr>
                    <tr style="height: 2px">
                        <td style="vertical-align:top;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:35.53333px;">
                            <p style="text-align:center;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;margin-top:6pt;margin-bottom:0pt;"><font face="Wingdings">&#113;</font></p>
                        </td>
                        <td style="vertical-align:top;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:148.1333px;">
                            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;margin-top:6pt;margin-bottom:0pt;"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;">Payment by mail</span></p>
                        </td>
                        <td style="vertical-align:top;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:160.4667px;">
                            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;margin-top:6pt;margin-bottom:0pt;"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;">Amount sent</span></p>
                        </td>
                        <td style="vertical-align:top;border-top-style:solid;border-top-width:1pt;border-bottom-style:solid;border-bottom-width:1pt;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:139.0667px;">
                            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;margin-top:6pt;margin-bottom:0pt;"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;">&#xa0;</span></p>
                        </td>
                        <td style="vertical-align:top;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:19.26667px;">
                            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;margin-top:6pt;margin-bottom:0pt;"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;">&#xa0;</span></p>
                        </td>
                        <td style="vertical-align:top;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:48.33333px;">
                            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;margin-top:6pt;margin-bottom:0pt;"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;">Date</span></p>
                        </td>
                        <td style="vertical-align:top;border-top-style:solid;border-top-width:1pt;border-bottom-style:solid;border-bottom-width:1pt;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:102.8px;">
                            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;margin-top:6pt;margin-bottom:0pt;"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;">&#xa0;</span></p>
                        </td>
                    </tr>
                    <tr style="height: 2px">
                        <td style="vertical-align:top;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:35.53333px;">
                            <p style="text-align:center;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;margin-top:6pt;margin-bottom:0pt;"><font face="Wingdings">&#113;</font></p>
                        </td>
                        <td style="vertical-align:top;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:148.1333px;">
                            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;margin-top:6pt;margin-bottom:0pt;"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;">Payment in person</span></p>
                        </td>
                        <td style="vertical-align:top;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:160.4667px;">
                            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;margin-top:6pt;margin-bottom:0pt;"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;">Amount paid</span></p>
                        </td>
                        <td style="vertical-align:top;border-top-style:solid;border-top-width:1pt;border-bottom-style:solid;border-bottom-width:1pt;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:139.0667px;">
                            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;margin-top:6pt;margin-bottom:0pt;"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;">&#xa0;</span></p>
                        </td>
                        <td style="vertical-align:top;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:19.26667px;">
                            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;margin-top:6pt;margin-bottom:0pt;"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;">&#xa0;</span></p>
                        </td>
                        <td style="vertical-align:top;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:48.33333px;">
                            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;margin-top:6pt;margin-bottom:0pt;"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;">Date</span></p>
                        </td>
                        <td style="vertical-align:top;border-top-style:solid;border-top-width:1pt;border-bottom-style:solid;border-bottom-width:1pt;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:102.8px;">
                            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;margin-top:6pt;margin-bottom:0pt;"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;">&#xa0;</span></p>
                        </td>
                    </tr>
                </table>
            </div>
            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;line-height:115%;margin-top:6pt;margin-bottom:6pt;"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">&#xa0;</span></p>
            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;line-height:115%;margin-top:0pt;margin-bottom:10pt;"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">Please ensure that all details are completed before signing. Return the completed form to the Registrar as soon as possible after payment has been made.</span></p>
            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;line-height:115%;margin-top:0pt;margin-bottom:10pt;"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">&#xa0;</span></p>
            <div>
                <table cellspacing="0" style="width: auto; border-collapse: collapse; ">
                    <tr style="height: 2px">
                        <td style="vertical-align:top;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:75.6px;">
                            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;margin-top:0pt;margin-bottom:0pt;"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">Signature</span></p>
                        </td>
                        <td style="vertical-align:top;border-bottom-style:solid;border-bottom-width:1pt;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:122.8667px;">
                            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;margin-top:0pt;margin-bottom:0pt;"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">&#xa0;</span></p>
                        </td>
                        <td style="vertical-align:top;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:292.9333px;">
                            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;margin-top:0pt;margin-bottom:0pt;"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">&#xa0;</span></p>
                        </td>
                        <td style="vertical-align:top;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:47.26667px;">
                            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;margin-top:0pt;margin-bottom:0pt;"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">Date</span></p>
                        </td>
                        <td style="vertical-align:top;border-bottom-style:solid;border-bottom-width:1pt;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:100.5333px;">
                            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;margin-top:0pt;margin-bottom:0pt;"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">&#xa0;</span></p>
                        </td>
                    </tr>
                </table>
            </div>
            <h1><span style="color:#032348;font-family:Century Gothic;font-size:16pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">Refund policy for overseas students</span></h1>
            <p class="Policy"><span style="font-family:Calibri;font-size:10pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">If an applicant accepts a place offered by ACCIT and pays the fees, it means a binding contract is created between the student and ACCIT. </span></p>
            <p class="Policy"><span style="font-family:Calibri;font-size:10pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">Notification of cancellation/withdrawal from unit/s of competency, withdrawal or deferral from a course of study must be made in writing to ACCIT. </span></p>
            <p class="Policy"><span style="font-family:Calibri;font-size:10pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">In the case of cancellation/withdrawal, the cancellation fee will be calculated as shown below. </span></p>
            <p class="Policy"><span style="font-family:Calibri;font-size:10pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">Students whose enrolment is terminated by ACCIT will not be entitled to any refund of fees. Any fee refund is wholly at the discretion of ACCIT. </span></p>
            <p class="Policy"><span style="font-family:Calibri;font-size:10pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;mso-spacerun:yes;">Where a student is unable to enroll in a similar course at ACCIT and the enrolment is cancelled then all fees paid will be refunded with the exception of $70 as and administration fee.  </span></p>
            <p class="Policy"><span style="font-family:Calibri;font-size:10pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">In the event of default by the provider, the ESOS Act 2000 and the ESOS Regulations 2001 will apply. </span></p>
            <p class="Policy"><span style="font-family:Calibri;font-size:10pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">All refunds under this policy will be paid within four (4) weeks after receiving a written claim from the student. </span></p>
            <p class="Policy"><span style="font-family:Calibri;font-size:10pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">This agreement does not remove the right to take further action under Australia's consumer protection laws. </span></p>
            <p class="Policy"><span style="font-family:Calibri;font-size:10pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">All applications for refund of fees and Overseas Student Health Cover (OSHC) must be made in writing and sent to Registrar, ACCIT</span></p>
            <p class="Policy"><span style="font-family:Calibri;font-size:10pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">Applications for refunds should include all relevant information to enable payment, such as bank name, bank account details, and address of bank and name of account holder.</span></p>
            <p class="Policy"><span style="font-family:Calibri;font-size:10pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">Refund Payment. Payment of refunds will be made in Australian dollars.</span></p>
            <p class="Policy"><span style="font-family:Calibri;font-size:10pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">Important Note: In the event of cancelation of a course by ACCIT, student will be eligible for a full refund.</span></p>
            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;line-height:normal;margin-top:0pt;margin-bottom:0pt;"><span style="font-family:Arial;font-size:10pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;">&#xa0;</span></p>
            <h3><span style="color:#021730;font-family:Century Gothic;font-size:12pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">Refund Circumstances</span></h3>
            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;line-height:normal;margin-top:0pt;margin-bottom:0pt;"><span style="font-family:Arial;font-size:10pt;text-transform:none;font-weight:bold;font-style:normal;font-variant:normal;">&#xa0;</span></p>
            <div>
                <table cellspacing="0" style="width: auto; border-collapse: collapse; ">
                    <tr style="height: 2px">
                        <td style="vertical-align:top;border-top-style:solid;border-top-color:#167AF3;border-top-width:1pt;border-left-style:solid;border-left-color:#167AF3;border-left-width:1pt;border-right-style:solid;border-right-color:#167AF3;border-right-width:1pt;border-bottom-style:solid;border-bottom-color:#167AF3;border-bottom-width:1.5pt;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:295.2px;">
                            <p class="Policy" style="text-align:center;"><span style="color:#032348;font-family:Calibri;font-size:10pt;text-transform:none;font-weight:bold;font-style:normal;font-variant:normal;line-height:115%;">Refund Circumstances</span></p>
                        </td>
                        <td style="vertical-align:top;border-top-style:solid;border-top-color:#167AF3;border-top-width:1pt;border-left-style:solid;border-left-color:#167AF3;border-left-width:1pt;border-right-style:solid;border-right-color:#167AF3;border-right-width:1pt;border-bottom-style:solid;border-bottom-color:#167AF3;border-bottom-width:1.5pt;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:295.2px;">
                            <p class="Policy" style="text-align:center;"><span style="color:#032348;font-family:Calibri;font-size:10pt;text-transform:none;font-weight:bold;font-style:normal;font-variant:normal;line-height:115%;">Refund Amount</span></p>
                        </td>
                    </tr>
                    <tr style="height: 2px">
                        <td style="vertical-align:top;background-color:#B1D2FB;border-top-style:solid;border-top-color:#167AF3;border-top-width:1pt;border-left-style:solid;border-left-color:#167AF3;border-left-width:1pt;border-right-style:solid;border-right-color:#167AF3;border-right-width:1pt;border-bottom-style:solid;border-bottom-color:#167AF3;border-bottom-width:1pt;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:295.2px;">
                            <p class="Policy"><span style="color:#032348;font-family:Calibri;font-size:10pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">If a student's visa application is rejected before commencement, and the DIBP official rejection advice is provided to ACCIT.</span></p>
                        </td>
                        <td style="vertical-align:top;background-color:#B1D2FB;border-top-style:solid;border-top-color:#167AF3;border-top-width:1pt;border-left-style:solid;border-left-color:#167AF3;border-left-width:1pt;border-right-style:solid;border-right-color:#167AF3;border-right-width:1pt;border-bottom-style:solid;border-bottom-color:#167AF3;border-bottom-width:1pt;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:295.2px;">
                            <p class="Policy"><span style="color:#032348;font-family:Calibri;font-size:10pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">refund equal to 100% of the tuition fees less enrolment fee and administration fee of $450.00</span></p>
                        </td>
                    </tr>
                    <tr style="height: 2px">
                        <td style="vertical-align:top;border-top-style:solid;border-top-color:#167AF3;border-top-width:1pt;border-left-style:solid;border-left-color:#167AF3;border-left-width:1pt;border-right-style:solid;border-right-color:#167AF3;border-right-width:1pt;border-bottom-style:solid;border-bottom-color:#167AF3;border-bottom-width:1pt;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:295.2px;">
                            <p class="Policy"><span style="color:#032348;font-family:Calibri;font-size:10pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">If written notice of cancellation of enrolment is received by ACCIT at least 4 weeks prior to Course commencement date</span></p>
                        </td>
                        <td style="vertical-align:top;border-top-style:solid;border-top-color:#167AF3;border-top-width:1pt;border-left-style:solid;border-left-color:#167AF3;border-left-width:1pt;border-right-style:solid;border-right-color:#167AF3;border-right-width:1pt;border-bottom-style:solid;border-bottom-color:#167AF3;border-bottom-width:1pt;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:295.2px;">
                            <p class="Policy"><span style="color:#032348;font-family:Calibri;font-size:10pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">refund equal to 80% of the tuition fees less enrolment fee of $200</span></p>
                        </td>
                    </tr>
                    <tr style="height: 2px">
                        <td style="vertical-align:top;background-color:#B1D2FB;border-top-style:solid;border-top-color:#167AF3;border-top-width:1pt;border-left-style:solid;border-left-color:#167AF3;border-left-width:1pt;border-right-style:solid;border-right-color:#167AF3;border-right-width:1pt;border-bottom-style:solid;border-bottom-color:#167AF3;border-bottom-width:1pt;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:295.2px;">
                            <p class="Policy"><span style="color:#032348;font-family:Calibri;font-size:10pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;mso-spacerun:yes;">If it is received prior to, but less than 28                                                                                                                                                                                                   days before the Course commencement date</span></p>
                        </td>
                        <td style="vertical-align:top;background-color:#B1D2FB;border-top-style:solid;border-top-color:#167AF3;border-top-width:1pt;border-left-style:solid;border-left-color:#167AF3;border-left-width:1pt;border-right-style:solid;border-right-color:#167AF3;border-right-width:1pt;border-bottom-style:solid;border-bottom-color:#167AF3;border-bottom-width:1pt;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:295.2px;">
                            <p class="Policy"><span style="color:#032348;font-family:Calibri;font-size:10pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">refund equal to 50% of the tuition fees less enrolment fee of $200</span></p>
                        </td>
                    </tr>
                    <tr style="height: 2px">
                        <td style="vertical-align:top;border-top-style:solid;border-top-color:#167AF3;border-top-width:1pt;border-left-style:solid;border-left-color:#167AF3;border-left-width:1pt;border-right-style:solid;border-right-color:#167AF3;border-right-width:1pt;border-bottom-style:solid;border-bottom-color:#167AF3;border-bottom-width:1pt;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:295.2px;">
                            <p class="Policy"><span style="color:#032348;font-family:Calibri;font-size:10pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">If a student cancels enrolment on or after the Course commencement date </span></p>
                        </td>
                        <td style="vertical-align:top;border-top-style:solid;border-top-color:#167AF3;border-top-width:1pt;border-left-style:solid;border-left-color:#167AF3;border-left-width:1pt;border-right-style:solid;border-right-color:#167AF3;border-right-width:1pt;border-bottom-style:solid;border-bottom-color:#167AF3;border-bottom-width:1pt;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:295.2px;">
                            <p class="Policy"><span style="color:#032348;font-family:Calibri;font-size:10pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">no refund of the tuition fees and enrolment fee of $ 200</span></p>
                        </td>
                    </tr>
                </table>
            </div>
            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;line-height:normal;margin-top:0pt;margin-bottom:0pt;"><span style="font-family:Arial;font-size:10pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;">&#xa0;</span></p>
            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;line-height:normal;margin-top:0pt;margin-bottom:0pt;"><span style="font-family:Arial;font-size:10pt;text-transform:none;font-weight:bold;font-style:normal;font-variant:normal;">&#xa0;</span></p>
            <div>
                <table cellspacing="0" style="width: auto; border-collapse: collapse; ">
                    <tr style="height: 2px">
                        <td style="vertical-align:top;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:75.6px;">
                            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;margin-top:0pt;margin-bottom:0pt;"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">Signature</span></p>
                        </td>
                        <td style="vertical-align:top;border-bottom-style:solid;border-bottom-width:1pt;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:122.8667px;">
                            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;margin-top:0pt;margin-bottom:0pt;"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">&#xa0;</span></p>
                        </td>
                        <td style="vertical-align:top;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:292.9333px;">
                            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;margin-top:0pt;margin-bottom:0pt;"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">&#xa0;</span></p>
                        </td>
                        <td style="vertical-align:top;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:47.26667px;">
                            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;margin-top:0pt;margin-bottom:0pt;"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">Date</span></p>
                        </td>
                        <td style="vertical-align:top;border-bottom-style:solid;border-bottom-width:1pt;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:100.5333px;">
                            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;margin-top:0pt;margin-bottom:0pt;"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">&#xa0;</span></p>
                        </td>
                    </tr>
                </table>
            </div>
            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;line-height:normal;margin-top:0pt;margin-bottom:0pt;"><span style="font-family:Arial;font-size:10pt;text-transform:none;font-weight:bold;font-style:normal;font-variant:normal;">&#xa0;</span></p>
            <h1><span style="color:#032348;font-family:Century Gothic;font-size:16pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">Attendance Policy</span></h1>
            <p class="Policy"><span style="font-family:Calibri;font-size:10pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">ACCIT will not adopt Department of Education-DIBP course progress policy for all its courses from 12th October 2009. </span></p>
            <div>
                <table cellspacing="0" style="width: 100%; border-collapse: collapse; ">
                    <tr style="height: 2px">
                        <td style="vertical-align:top;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:653.6px;" colspan="3">
                            <h5><span style="color:#167AF3;font-family:Century Gothic;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;">Introduction</span></h5>
                        </td>
                    </tr>
                    <tr style="height: 2px">
                        <td style="vertical-align:middle;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:36.93333px;">
                            <p class="Policy"><span style="color:#032348;font-family:Calibri;font-size:10pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">1.1</span></p>
                        </td>
                        <td style="vertical-align:top;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:616.6667px;" colspan="2">
                            <p class="Policy"><span style="font-family:Calibri;font-size:10pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">The attendance of international students enrolled at ACCIT( the College) is monitored closely to meet the requirements of the ESOS Act 2000, and National Code 2007. Students are informed of their requirement to attend a minimum of 80% of classes at all times but are encouraged to attend 100% of classes to ensure successful academic outcomes in their studies. Any class session missed regardless of cause reduces the opportunity for learning and can adversely affect a student’s achievement in their enrolled course.</span></p>
                        </td>
                    </tr>
                    <tr style="height: 2px">
                        <td style="vertical-align:middle;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:36.93333px;">
                            <p class="Policy"><span style="color:#032348;font-family:Calibri;font-size:10pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">1.2</span></p>
                        </td>
                        <td style="vertical-align:top;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:616.6667px;" colspan="2">
                            <p class="Policy"><span style="font-family:Calibri;font-size:10pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">ACCIT is required under the ESOS Act 2000 and National Code 2007 to report to the Department of Immigration and Border Protection (DIBP) any student who has failed to meet the required 80% attendance. </span></p>
                        </td>
                    </tr>
                    <tr style="height: 2px">
                        <td style="vertical-align:middle;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:36.93333px;">
                            <p class="Policy"><span style="color:#032348;font-family:Calibri;font-size:10pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">1.3</span></p>
                        </td>
                        <td style="vertical-align:top;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:616.6667px;" colspan="2">
                            <p class="Policy"><span style="font-family:Calibri;font-size:10pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">VET students are required to maintain fulltime enrolment in their chosen course with a minimum face-to-face contact hours of 20 hours/week. Students who undertake additional subjects may attend classes for more than 20 hours/week, however for attendance monitoring purposes only 20 hours/week will be recorded in any given weeks of the session. The students should maintain an overall minimum attendance of 80% at any time.</span></p>
                        </td>
                    </tr>
                    <tr style="height: 84.53333px">
                        <td style="vertical-align:middle;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:36.93333px;">
                            <p class="Policy"><span style="color:#032348;font-family:Calibri;font-size:10pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">1.4</span></p>
                        </td>
                        <td style="vertical-align:top;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:616.6667px;" colspan="2">
                            <p class="Policy"><span style="font-family:Calibri;font-size:10pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">The students may apply for deferral from studies under exceptional circumstances, e.g. for health/medical reasons or based on compassionate grounds (bereavement). The application for deferral must be accompanied by documentary evidence (e.g. medical certificates from a registered medical practitioner) in order to use this deferral. The “Student Deferral, Suspension and Cancellation provides for further clarification regarding student deferral. </span></p>
                        </td>
                    </tr>
                    <tr style="height: 2px">
                        <td style="vertical-align:top;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:652.9333px;" colspan="2">
                            <h5><span style="color:#167AF3;font-family:Century Gothic;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;">Attendance Management for Vocational Education and Training (VET) students.</span></h5>
                        </td>
                        <td style="border:none;" width="0.5pt">&nbsp;</td>
                    </tr>
                    <tr style="height: 2px">
                        <td style="vertical-align:middle;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:36.93333px;">
                            <p class="Policy"><span style="color:#032348;font-family:Calibri;font-size:10pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">2.1</span></p>
                        </td>
                        <td style="vertical-align:top;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:616px;">
                            <p class="Policy"><span style="font-family:Calibri;font-size:10pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">VET students must maintain minimum attendance of 80%, </span></p>
                        </td>
                        <td style="border:none;" width="0.5pt">&nbsp;</td>
                    </tr>
                    <tr style="height: 2px">
                        <td style="vertical-align:middle;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:36.93333px;">
                            <p class="Policy"><span style="color:#032348;font-family:Calibri;font-size:10pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">2.2</span></p>
                        </td>
                        <td style="vertical-align:top;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:616px;">
                            <p class="Policy"><span style="font-family:Calibri;font-size:10pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">VET students will be reported to DIBP if their attendance falls below 80%, and cannot be made up to 80% by the end of the course. If there are mitigating circumstances, such as documentary evidence of compassionate or compelling circumstances applying, the minimum attendance allowed will be 70%.</span></p>
                        </td>
                        <td style="border:none;" width="0.5pt">&nbsp;</td>
                    </tr>
                    <tr style="height: 2px">
                        <td style="vertical-align:middle;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:36.93333px;">
                            <p class="Policy"><span style="color:#032348;font-family:Calibri;font-size:10pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">2.3</span></p>
                        </td>
                        <td style="vertical-align:top;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:616px;">
                            <p class="Policy"><span style="font-family:Calibri;font-size:10pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">Reporting is the last resort after the student has been counselled and has had 20 working days to appeal to ACCIT </span></p>
                        </td>
                        <td style="border:none;" width="0.5pt">&nbsp;</td>
                    </tr>
                    <tr style="height: 2px">
                        <td style="vertical-align:middle;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:36.93333px;">
                            <p class="Policy"><span style="color:#032348;font-family:Calibri;font-size:10pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">2.4</span></p>
                        </td>
                        <td style="vertical-align:top;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:616px;">
                            <p class="Policy"><span style="font-family:Calibri;font-size:10pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">If the appeal is unsuccessful the student will be reported to DIBP via PRISMS for Non-Attendance of Classes and will be sent a letter (Non-Compliance Notification Letter). </span></p>
                        </td>
                        <td style="border:none;" width="0.5pt">&nbsp;</td>
                    </tr>
                    <tr style="height: 2px">
                        <td style="vertical-align:middle;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:36.93333px;">
                            <p class="Policy"><span style="color:#032348;font-family:Calibri;font-size:10pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">2.5</span></p>
                        </td>
                        <td style="vertical-align:top;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:616px;">
                            <p class="Policy"><span style="font-family:Calibri;font-size:10pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">In order to alert the student about their attendance, a series of warning letters will be sent. Students with attendance approaching 80% (85% - 80%) will be sent a “Caution- Low Attendance Letter” and then when it goes below 80 %, a “Warning – Low Attendance Letter.” These disciplinary notification letters outline the student’s unsatisfactory attendance and give instructions on when and where to meet with the Director of Studies or Principal to discuss their situation. </span></p>
                        </td>
                        <td style="border:none;" width="0.5pt">&nbsp;</td>
                    </tr>
                    <tr style="height: 2px">
                        <td style="vertical-align:top;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:652.9333px;" colspan="2">
                            <h5><span style="color:#167AF3;font-family:Century Gothic;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;">Monitoring of Attendance</span></h5>
                        </td>
                        <td style="border:none;" width="0.5pt">&nbsp;</td>
                    </tr>
                    <tr style="height: 2px">
                        <td style="vertical-align:middle;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:36.93333px;">
                            <p class="Policy"><span style="color:#032348;font-family:Calibri;font-size:10pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">3.1</span></p>
                        </td>
                        <td style="vertical-align:top;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:616px;">
                            <p class="Policy"><span style="font-family:Calibri;font-size:10pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">Monitoring of attendance is covered by Marking roll call sheet every day by trainers</span></p>
                            <p class="Policy"><span style="font-family:Calibri;font-size:10pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">Trainers/Assessor must use the student daily attendance record to record student attendance at each scheduled class and note early departures and late arrivals </span></p>
                            <p class="Policy"><span style="font-family:Calibri;font-size:10pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">Reviewing of attendance by Director of studies every fortnight.</span></p>
                            <p class="Policy"><span style="font-family:Calibri;font-size:10pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">The sending of caution and warning letters as well as reporting letters each fortnightly basis.</span></p>
                        </td>
                        <td style="border:none;" width="0.5pt">&nbsp;</td>
                    </tr>
                    <tr style="height: 20.9333324px">
                        <td style="vertical-align:top;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:652.9333px;" colspan="2">
                            <h5><span style="color:#167AF3;font-family:Century Gothic;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;">Approved Leave</span></h5>
                        </td>
                        <td style="border:none;" width="0.5pt">&nbsp;</td>
                    </tr>
                    <tr style="height: 2px">
                        <td style="vertical-align:middle;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:36.93333px;">
                            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;margin-top:0pt;margin-bottom:0pt;"><span style="color:#032348;font-family:Arial;font-size:10pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;">4.1</span></p>
                        </td>
                        <td style="vertical-align:top;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:616px;">
                            <p class="Policy"><span style="font-family:Calibri;font-size:10pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">Students will not be granted approved leave during the academic term (session).</span></p>
                        </td>
                        <td style="border:none;" width="0.5pt">&nbsp;</td>
                    </tr>
                    <tr style="height: 2px">
                        <td style="vertical-align:top;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:652.9333px;" colspan="2">
                            <h5><span style="color:#167AF3;font-family:Century Gothic;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;">Counselling</span></h5>
                        </td>
                        <td style="border:none;" width="0.5pt">&nbsp;</td>
                    </tr>
                    <tr style="height: 2px">
                        <td style="vertical-align:middle;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:36.93333px;">
                            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;margin-top:0pt;margin-bottom:0pt;"><span style="color:#032348;font-family:Arial;font-size:10pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;">5.1</span></p>
                        </td>
                        <td style="vertical-align:top;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:616px;">
                            <p class="Policy"><span style="font-family:Calibri;font-size:10pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">The College endeavours to intervene with the student to improve attendance by counselling students when their attendance begins to fall.</span></p>
                        </td>
                        <td style="border:none;" width="0.5pt">&nbsp;</td>
                    </tr>
                    <tr style="height: 2px">
                        <td style="vertical-align:middle;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:36.93333px;">
                            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;margin-top:0pt;margin-bottom:0pt;"><span style="color:#032348;font-family:Arial;font-size:10pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;">5.2</span></p>
                        </td>
                        <td style="vertical-align:top;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:616px;">
                            <p class="Policy"><span style="font-family:Calibri;font-size:10pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">Caution and Warning letters will offer counselling to ascertain reasons for low attendance and suggest ways of improving it.</span></p>
                        </td>
                        <td style="border:none;" width="0.5pt">&nbsp;</td>
                    </tr>
                    <tr style="height: 2px">
                        <td style="vertical-align:middle;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:36.93333px;">
                            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;margin-top:0pt;margin-bottom:0pt;"><span style="color:#032348;font-family:Arial;font-size:10pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;">5.3</span></p>
                        </td>
                        <td style="vertical-align:top;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:616px;">
                            <p class="Policy"><span style="font-family:Calibri;font-size:10pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">Records of counselling must be kept in database log.</span></p>
                        </td>
                        <td style="border:none;" width="0.5pt">&nbsp;</td>
                    </tr>
                    <tr style="height: 2px">
                        <td style="vertical-align:top;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:652.9333px;" colspan="2">
                            <h5><span style="color:#167AF3;font-family:Century Gothic;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;">Appeals</span></h5>
                        </td>
                        <td style="border:none;" width="0.5pt">&nbsp;</td>
                    </tr>
                    <tr style="height: 2px">
                        <td style="vertical-align:middle;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:36.93333px;">
                            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;margin-top:0pt;margin-bottom:0pt;"><span style="color:#032348;font-family:Arial;font-size:10pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;">6.1</span></p>
                        </td>
                        <td style="vertical-align:top;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:616px;">
                            <p class="Policy"><span style="font-family:Calibri;font-size:10pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">Students will have a right to appeal with regard to attendance matters or reporting Complaints, Appeals and Grievance Policy.</span></p>
                        </td>
                        <td style="border:none;" width="0.5pt">&nbsp;</td>
                    </tr>
                    <tr style="height: 2px">
                        <td style="vertical-align:middle;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:36.93333px;">
                            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;margin-top:0pt;margin-bottom:0pt;"><span style="color:#032348;font-family:Arial;font-size:10pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;">6.2</span></p>
                        </td>
                        <td style="vertical-align:top;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:616px;">
                            <p class="Policy"><span style="font-family:Calibri;font-size:10pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">Reporting is the last resort after the student has been counselled and has had 20 working days to appeal to ACCIT.</span></p>
                        </td>
                        <td style="border:none;" width="0.5pt">&nbsp;</td>
                    </tr>
                    <tr style="height:0px;">
                        <td style="width:36.93333px;border:none;padding:0pt;" />
                        <td style="width:616px;border:none;padding:0pt;" />
                        <td style="width:0.6666667px;border:none;padding:0pt;" />
                    </tr>
                </table>
            </div>
            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;line-height:normal;margin-top:0pt;margin-bottom:0pt;"><span style="font-family:Arial;font-size:10pt;text-transform:none;font-weight:bold;font-style:normal;font-variant:normal;">&#xa0;</span></p>
            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;line-height:normal;margin-top:0pt;margin-bottom:0pt;"><span style="font-family:Arial;font-size:10pt;text-transform:none;font-weight:bold;font-style:normal;font-variant:normal;">I have read and understand ACCIT attendance procedure</span></p>
            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;line-height:normal;margin-top:0pt;margin-bottom:0pt;"><span style="font-family:Arial;font-size:10pt;text-transform:none;font-weight:bold;font-style:normal;font-variant:normal;">&#xa0;</span></p>
            <div>
                <table cellspacing="0" style="width: auto; border-collapse: collapse; ">
                    <tr style="height: 2px">
                        <td style="vertical-align:top;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:75.6px;">
                            <p style="text-align:left;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;margin-top:0pt;margin-bottom:0pt;"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">Signature</span></p>
                        </td>
                        <td style="vertical-align:top;border-bottom-style:solid;border-bottom-width:1pt;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:122.8667px;">
                            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;margin-top:0pt;margin-bottom:0pt;"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">&#xa0;</span></p>
                        </td>
                        <td style="vertical-align:top;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:292.9333px;">
                            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;margin-top:0pt;margin-bottom:0pt;"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">&#xa0;</span></p>
                        </td>
                        <td style="vertical-align:top;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:47.26667px;">
                            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;margin-top:0pt;margin-bottom:0pt;"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">Date</span></p>
                        </td>
                        <td style="vertical-align:top;border-bottom-style:solid;border-bottom-width:1pt;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:100.5333px;">
                            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;margin-top:0pt;margin-bottom:0pt;"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">&#xa0;</span></p>
                        </td>
                    </tr>
                </table>
            </div>
            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;line-height:normal;margin-top:0pt;margin-bottom:0pt;"><span style="font-family:Arial;font-size:10pt;text-transform:none;font-weight:bold;font-style:normal;font-variant:normal;">&#xa0;</span></p>
            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;line-height:115%;margin-top:0pt;margin-bottom:10pt;"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">&#xa0;</span></p>
            <br style='clear:both;page-break-before:always'/>
            <p style="text-align:left;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;line-height:normal;margin-top:0pt;margin-bottom:0pt;" />
            <h2><span style="color:#032348;font-family:Century Gothic;font-size:13pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">Important information for persons applying for a student visa</span></h2>
            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;line-height:normal;margin-top:0pt;margin-bottom:0pt;"><span style="font-family:Arial;font-size:10pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;">&#xa0;</span></p>
            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;line-height:115%;margin-top:0pt;margin-bottom:10pt;">
                <span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">Dear</span><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">Kohli</span><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">,</span>
            </p>
            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;line-height:115%;margin-top:0pt;margin-bottom:10pt;"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">The following website provides important instructions on applying for a student visa and requirements</span></p>
            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;line-height:115%;margin-top:0pt;margin-bottom:10pt;"><a style="color:#0000FF;font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;text-decoration: underline;line-height:115%;" href="http://www.border.gov.au/Trav/Stud"><span class="Default-Paragraph-Font Hyperlink" style="font-size:11pt;line-height:115%;">http://www.border.gov.au/Trav/Stud</span></a></p>
            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;line-height:115%;margin-top:0pt;margin-bottom:10pt;"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">on the Department of Immigration and Border Protection (DIBP). </span></p>
            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;line-height:115%;margin-top:0pt;margin-bottom:10pt;"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">You can find the contact details for </span><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:bold;font-style:normal;font-variant:normal;line-height:115%;">Australian High Commission </span><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">around the world where you can make enquiries about the visa application process and lodge your application.</span></p>
            <h3><span style="color:#021730;font-family:Century Gothic;font-size:12pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">Opening a Bank account</span></h3>
            <ul type="disc" style="margin:0pt; padding-left:0pt">
                <li class="List-Paragraph" style="-sf-number-width:27.03271pt;margin-left:45.0327148pt;padding-left:-9.032715pt;text-indent:0pt;color:#0D89A8;font-family:Wingdings;"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">Bring your passport and student card/confirmation of enrollment to the bank.</span></li>
                <li class="List-Paragraph" style="-sf-number-width:27.03271pt;margin-left:45.0327148pt;padding-left:-9.032715pt;text-indent:0pt;color:#0D89A8;font-family:Wingdings;"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">You must open your bank account within 6 month of first arrival in Australia.</span></li>
                <li class="List-Paragraph" style="-sf-number-width:27.03271pt;margin-left:45.0327148pt;padding-left:-9.032715pt;text-indent:0pt;color:#0D89A8;font-family:Wingdings;"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">Inform the officer that you want to open </span><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:bold;font-style:normal;font-variant:normal;line-height:115%;">student account</span><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;"> (no account keeping fee for student account).</span></li>
                <li class="List-Paragraph" style="-sf-number-width:27.03271pt;margin-left:45.0327148pt;padding-left:-9.032715pt;text-indent:0pt;color:#0D89A8;font-family:Wingdings;"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">Get your key-card and keep it securely</span></li>
            </ul>
            <div>
                <table cellspacing="0" style="width: auto; border-collapse: collapse; ">
                    <tr style="height: 2px">
                        <td style="vertical-align:top;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:127.6667px;">
                            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;margin-top:0pt;margin-bottom:0pt;"><a style="font-family:Calibri;font-size:10pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:150%;" href="http://www.commbank.com.au/"><img src="../images/TDIcaGvQ_img4.png" width="110" height="25" alt="Description: Description: Description: Commonwealth Bank of Australia" /></a></p>
                        </td>
                        <td style="vertical-align:top;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:127.7333px;">
                            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;margin-top:0pt;margin-bottom:0pt;"><a style="font-family:Calibri;font-size:10pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:150%;" href="http://www.stgeorge.com.au/"><img src="../images/TDIcaGvQ_img5.png" width="94" height="29" alt="Description: Description: Description: St.George" /></a></p>
                        </td>
                        <td style="vertical-align:top;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:127.6667px;">
                            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;margin-top:0pt;margin-bottom:0pt;"><img src="../images/TDIcaGvQ_img6.png" width="216" height="35" alt="Description: Description: Description: Westpac" /></p>
                        </td>
                        <td style="vertical-align:top;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:127.7333px;">
                            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;margin-top:0pt;margin-bottom:0pt;"><a style="font-family:Calibri;font-size:10pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:150%;" href="http://www.anz.com.au/default.asp"><img src="../images/TDIcaGvQ_img7.png" width="65" height="30" alt="Description: Description: Description: ANZ home" /></a></p>
                        </td>
                        <td style="vertical-align:top;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:127.7333px;">
                            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;margin-top:0pt;margin-bottom:0pt;"><a style="font-family:Calibri;font-size:10pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:150%;" href="http://www.national.com.au/?ncID=ZBA"><img src="../images/TDIcaGvQ_img8.png" width="108" height="33" alt="Description: Description: Description: National Australia Bank Logo (Home)" /></a></p>
                        </td>
                    </tr>
                </table>
            </div>
            <h3><span style="color:#021730;font-family:Century Gothic;font-size:12pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">&#xa0;</span></h3>
            <h3><span style="color:#021730;font-family:Century Gothic;font-size:12pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">Applying for Tax files number ( After you get your work permit)</span></h3>
            <ul type="disc" style="margin:0pt; padding-left:0pt">
                <li class="List-Paragraph" style="-sf-number-width:27.03271pt;margin-left:45.0327148pt;padding-left:-9.032715pt;text-indent:0pt;color:#0D89A8;font-family:Wingdings;"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">Go to www. ato.gov.au</span></li>
            </ul>
            <ul type="disc" style="margin:0pt; padding-left:0pt">
                <li class="List-Paragraph" style="-sf-number-width:27.03271pt;margin-left:45.0327148pt;padding-left:-9.032715pt;text-indent:0pt;color:#0D89A8;font-family:Wingdings;"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">Click For Individuals </span></li>
                <li class="List-Paragraph" style="-sf-number-width:27.03271pt;margin-left:45.0327148pt;padding-left:-9.032715pt;text-indent:0pt;color:#0D89A8;font-family:Wingdings;"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">Click Apply for a tax file number (TFN)</span></li>
                <li class="List-Paragraph" style="-sf-number-width:27.03271pt;margin-left:45.0327148pt;padding-left:-9.032715pt;text-indent:0pt;color:#0D89A8;font-family:Wingdings;"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">Follow the steps and tax file number will be sent out to your given address</span></li>
                <br style='clear:both;page-break-before:always'/>
            </ul>
            <p style="text-align:left;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;line-height:normal;margin-top:0pt;margin-bottom:0pt;" />
            <h1><span style="color:#032348;font-family:Century Gothic;font-size:16pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">Pre-departure checklist</span></h1>
            <div>
                <table cellspacing="0" style="width: 100%; border-collapse: collapse; ">
                    <tr style="height: 2px">
                        <td style="vertical-align:top;background-color:#B1D2FB;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:599.4px;">
                            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;margin-top:6pt;margin-bottom:6pt;"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;mso-spacerun:yes;">Have you:  Got your passport and student visa (valid for at least the length of your course)?</span></p>
                        </td>
                        <td style="vertical-align:top;background-color:#B1D2FB;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:54.2px;">
                            <p style="text-align:center;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;margin-top:6pt;margin-bottom:6pt;"><font face="Wingdings">&#113;</font></p>
                        </td>
                    </tr>
                    <tr style="height: 2px">
                        <td style="vertical-align:top;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:599.4px;">
                            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;margin-top:6pt;margin-bottom:6pt;"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;mso-spacerun:yes;">Got all the other documents certified by the appropriate authority you may need?              </span></p>
                        </td>
                        <td style="vertical-align:top;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:54.2px;">
                            <p style="text-align:center;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;margin-top:6pt;margin-bottom:6pt;"><font face="Wingdings">&#113;</font></p>
                        </td>
                    </tr>
                    <tr style="height: 2px">
                        <td style="vertical-align:top;background-color:#B1D2FB;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:599.4px;">
                            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;margin-top:6pt;margin-bottom:6pt;"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;mso-spacerun:yes;">Paid your Overseas Student Health cover for the full stay?     </span></p>
                        </td>
                        <td style="vertical-align:top;background-color:#B1D2FB;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:54.2px;">
                            <p style="text-align:center;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;margin-top:6pt;margin-bottom:6pt;"><font face="Wingdings">&#113;</font></p>
                        </td>
                    </tr>
                    <tr style="height: 2px">
                        <td style="vertical-align:top;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:599.4px;">
                            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;margin-top:6pt;margin-bottom:6pt;"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;mso-spacerun:yes;">Made your accommodation arrangements?                </span></p>
                        </td>
                        <td style="vertical-align:top;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:54.2px;">
                            <p style="text-align:center;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;margin-top:6pt;margin-bottom:6pt;"><font face="Wingdings">&#113;</font></p>
                        </td>
                    </tr>
                    <tr style="height: 2px">
                        <td style="vertical-align:top;background-color:#B1D2FB;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:599.4px;">
                            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;margin-top:6pt;margin-bottom:6pt;"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;">Made arrangements for pick-up or transport from the airport to your accommodation?</span></p>
                        </td>
                        <td style="vertical-align:top;background-color:#B1D2FB;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:54.2px;">
                            <p style="text-align:center;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;margin-top:6pt;margin-bottom:6pt;"><font face="Wingdings">&#113;</font></p>
                        </td>
                    </tr>
                    <tr style="height: 2px">
                        <td style="vertical-align:top;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:599.4px;">
                            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;margin-top:6pt;margin-bottom:6pt;"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;">Packed suitable clothing for all four seasons in Sydney? It can get cold in the winter. </span></p>
                        </td>
                        <td style="vertical-align:top;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:54.2px;">
                            <p style="text-align:center;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;margin-top:6pt;margin-bottom:6pt;"><font face="Wingdings">&#113;</font></p>
                        </td>
                    </tr>
                    <tr style="height: 2px">
                        <td style="vertical-align:top;background-color:#B1D2FB;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:599.4px;">
                            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;margin-top:6pt;margin-bottom:6pt;"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;mso-spacerun:yes;">Checked that any electronic equipment you are bringing is compatible in Australia?      </span></p>
                        </td>
                        <td style="vertical-align:top;background-color:#B1D2FB;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:54.2px;">
                            <p style="text-align:center;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;margin-top:6pt;margin-bottom:6pt;"><font face="Wingdings">&#113;</font></p>
                        </td>
                    </tr>
                    <tr style="height: 2px">
                        <td style="vertical-align:top;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:599.4px;">
                            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;margin-top:6pt;margin-bottom:6pt;"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;mso-spacerun:yes;">Packed spare glasses or contact lenses?                                                                             </span></p>
                        </td>
                        <td style="vertical-align:top;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:54.2px;">
                            <p style="text-align:center;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;margin-top:6pt;margin-bottom:6pt;"><font face="Wingdings">&#113;</font></p>
                        </td>
                    </tr>
                    <tr style="height: 2px">
                        <td style="vertical-align:top;background-color:#B1D2FB;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:599.4px;">
                            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;margin-top:6pt;margin-bottom:6pt;"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;">Organised your finances so that you have access to enough money to live on while you are in Australia (through your bank account, credit card etc)?</span></p>
                        </td>
                        <td style="vertical-align:top;background-color:#B1D2FB;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:54.2px;">
                            <p style="text-align:center;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;margin-top:6pt;margin-bottom:6pt;"><font face="Wingdings">&#113;</font></p>
                        </td>
                    </tr>
                    <tr style="height: 2px">
                        <td style="vertical-align:top;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:599.4px;">
                            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;margin-top:6pt;margin-bottom:6pt;"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;">Checked that you are not bringing goods which are prohibited by Australian Customs or Quarantine regulations ( www.daffa.gov.au/aqis | www.customs.gov.au )</span></p>
                        </td>
                        <td style="vertical-align:top;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:54.2px;">
                            <p style="text-align:center;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;margin-top:6pt;margin-bottom:6pt;"><font face="Wingdings">&#113;</font></p>
                        </td>
                    </tr>
                    <tr style="height: 2px">
                        <td style="vertical-align:top;background-color:#B1D2FB;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:599.4px;">
                            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;margin-top:6pt;margin-bottom:6pt;"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;mso-spacerun:yes;">Written contact details of your family, friends and consulate?                 </span></p>
                        </td>
                        <td style="vertical-align:top;background-color:#B1D2FB;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:54.2px;">
                            <p style="text-align:center;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;margin-top:6pt;margin-bottom:6pt;"><font face="Wingdings">&#113;</font></p>
                        </td>
                    </tr>
                    <tr style="height: 2px">
                        <td style="vertical-align:top;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:599.4px;">
                            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;margin-top:6pt;margin-bottom:6pt;"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;mso-spacerun:yes;">Got padlocks for your checked luggage?                                              </span></p>
                        </td>
                        <td style="vertical-align:top;padding-left:5.4pt;padding-right:5.4pt;padding-top:0pt;padding-bottom:0pt;width:54.2px;">
                            <p style="text-align:center;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;margin-top:6pt;margin-bottom:6pt;"><font face="Wingdings">&#113;</font></p>
                        </td>
                    </tr>
                </table>
            </div>
            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;line-height:115%;margin-top:0pt;margin-bottom:10pt;"><span style="font-family:Arial;font-size:10pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">&#xa0;</span></p>
            <p style="text-align:justify;page-break-inside:auto;page-break-after:auto;page-break-before:avoid;line-height:115%;margin-top:0pt;margin-bottom:10pt;"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;line-height:115%;">&#xa0;</span></p>
            <p class="Footer"><span style="font-family:Calibri;font-size:11pt;text-transform:none;font-weight:normal;font-style:normal;font-variant:normal;">&#xa0;</span></p>
        </div>
		
		
		
    </body>
</html>

<center>
<button onClick="myFunction()">Download/Print</button></center>

<script>
function myFunction() {
    window.print();
}
</script>
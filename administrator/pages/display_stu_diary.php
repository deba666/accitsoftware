<?php
    /*
        * @package    ACCIT
         * @author    Paperlink Softwares Team
         * @copyright    Copyright (c) 2016 , Paperlink Softwares Pvt. Ltd.  (http://www.paperlinksoftwares.com/)
         * @since    Version 1.0.0
         * @filesource
		 * Code for update a particualr student diary
         */
    
    if($_POST['DIARY_NO'])
    	{
    	if(mysql_query("UPDATE `diary` SET `DIARY_DATE`='".$_POST['DIARY_DATE']."', `ATTENTION`='".$_POST['ATTENTION']."', `CATEGORY_NO`='".$_POST['CATEGORY_NO']."', `SUMMARY`='".$_POST['SUMMARY']."', `NOTES`='".$_POST['NOTES']."' WHERE `DIARY_NO`=".$_GET['id']))
    		$_SESSION['s_msg']="<strong>Well done!</strong> Diary successfully updated";
    	else
    		$_SESSION['e_msg']="<strong>Oh snap!</strong> Diary is not updated";
    	}	
    ?>
<?php $cond="where `DIARY_NO`=".$_GET['id'];
    $std_d=getRows($cond,'diary');?>
<div class="right_col" role="main">
    <div class="">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="page-title">
                <div class="title_left">
                    <h3>Modify Student diary</h3>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="clearfix"></div>
                    <div class="x_content">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel recent-app">
                                    <div class="x_content">
                                        <div class="student-account-page edit-faculty-page">
                                            <!-- Nav tabs -->
                                            <div class="card">
                                                <div class="all-students-list add student">
                                                    <div class="add-student-section">
                                                        <form class="form-horizontal form-label-left" action="" method="post">
                                                            <div class="item form-group">
                                                                <input type="hidden" name="DIARY_NO" value="<?=$std_d[0]['DIARY_NO']?>" />
                                                                <label class="control-label col-md-3 col-sm-3 col-xs-2" for="name">Student Name:</label>
                                                                <div class="col-md-9 col-sm-9 col-xs-8">
                                                                    <div class="row">
                                                                        <div class="col-md-7 col-sm-7 col-xs-12">
                                                                            <?php $cond="where `CUSTOMER_NO`=".$std_d[0]['CUSTOMER_NO']; $std=getRows($cond,'student');?>
                                                                            <input type="text" id="name" class="form-control col-md-12 col-xs-12" disabled="disabled" name="" Value="<?=$std[0]['FNAME']?> <?=$std[0]['MNAME']?> <?=$std[0]['LNAME']?>" required="">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="item form-group">
                                                                <label for="name" class="control-label col-md-3 col-sm-3 col-xs-2">Date:</label>
                                                                <div class="col-md-4 col-sm-4 col-xs-9">
                                                                    <div class="row">
                                                                        <div class="col-md-4 col-sm-4 col-xs-4">
                                                                            <input type="text" required="" value="<?=$std_d[0]['DIARY_DATE']?>" name="DIARY_DATE" class="form-control col-md-12 col-xs-12" id="name">
                                                                        </div>
                                                                        <div class="col-md-4 col-sm-4 col-xs-4">
                                                                            <label class="pull-left">
                                                                            Attention: 
                                                                            <input type="hidden" name="ATTENTION" id="ATTENTION" value="0" >
                                                                            <input type="checkbox" name="ATTENTION" value="1" <?php echo ($std_d[0]['ATTENTION'] ? 'checked' : '');?>/>
                                                                            </label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="item form-group">
                                                                <label class="control-label col-md-3 col-sm-3 col-xs-2" for="name">CATEGORY</label>
                                                                <div class="col-md-7 col-sm-7 col-xs-8">
                                                                    <div class="row">
                                                                        <div class="col-md-6 col-sm-4 col-xs-8">
                                                                            <select name="CATEGORY_NO" class="form-control">
                                                                                <!--<?php
                                                                                    $diary_sql=getRows('order by `lookup_type_no` DESC','lookup_type'); 
                                                                                    foreach($diary_sql as $row)
                                                                                    {
                                                                                    if($row['FIELD_NAME']=='CATEGORY_NAME')
                                                                                    ?>
                                                                                    <option value="<?=$row['LOOKUP_TYPE_NO']?>"><?=$row['NAME']?></option>
                                                                                    <?php } ?> -->
                                                                                <?php
                                                                                    $sql="select * from lookup_type ORDER BY lookup_type_no ASC";
                                                                                    
                                                                                    $dd_res=mysql_query("Select *  from lookup_type");
                                                                                                                               while($r=mysql_fetch_array($dd_res))
                                                                                                                                  { 
                                                                                                                                   if($std_d[0]['LOOKUP_TYPE_NO']==$r['LOOKUP_TYPE_NO'])
                                                                                                                                     {
                                                                                                                                       echo "<option value='$r[LOOKUP_TYPE_NO]' selected> $r[NAME] </option>";
                                                                                                                                       }
                                                                                                                                        else
                                                                                                                                        {
                                                                                                                                         echo "<option value='$r[LOOKUP_TYPE_NO]'> $r[NAME] </option>";
                                                                                                                                          }
                                                                                                                                    }
                                                                                    ?>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="item form-group">
                                                                <input type="hidden" name="status" value="unread">
                                                                <label class="control-label col-md-2 col-sm-2 col-xs-2" for="name">Subject:</label>
                                                                <div class="col-md-9 col-sm-9 col-xs-8">
                                                                    <div class="row">
                                                                        <div class="col-md-8 col-sm-8 col-xs-8">
                                                                            <input type="text" id="name" class="form-control col-md-12 col-xs-12" name="SUMMARY" value="<?=$std_d[0]['SUMMARY']?>" required="">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="item form-group">
                                                                <input type="hidden" name="status" value="unread">
                                                                <label class="control-label col-md-2 col-sm-2 col-xs-2" for="name">Note:</label>
                                                                <div class="col-md-8 col-sm-8 col-xs-8">
                                                                    <div class="row">
                                                                        <div class="col-md-7 col-sm-7 col-xs-12">
                                                                            <textarea rows="4" cols="10" name="NOTES" ><?=$std_d[0]['NOTES']?></textarea>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="item form-group">
                                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name"></label>
                                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                                    <div class="row">
                                                                        <div class="col-md-6 col-sm-4 col-xs-12">
                                                                            <p class="pull-left">
                                                                                <button type="reset" class="btn btn-primary">Cancel</button>
                                                                                <button type="submit" class="btn btn-success">Modify</button>
                                                                            </p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
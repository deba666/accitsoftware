<?php
    /*
        * @package    ACCIT
         * @author    Paperlink Softwares Team
         * @copyright    Copyright (c) 2016 , Paperlink Softwares Pvt. Ltd.  (http://www.paperlinksoftwares.com/)
         * @since    Version 1.0.0
         * @filesource
		 * Code for details Workflow of enrolment
         */
    ?>
<div role="main" class="right_col" style="min-height: 648px;">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Enrolment Work Flow</h3>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="clearfix"></div>
                    <div class="x_content">
                        <div class="row">
                            <div class="">
                                <!--   <a class="btn btn-primary" data-toggle="modal" href="" onclick="javascript:void window.open('dashboard.php?op=<?=MD5('student_1')?>&a_no=<?=$en_nm[0]['AGENT_NO']?>','1470219021367','width=800,height=500,toolbar=0,menubar=0,location=1,status=1,scrollbars=1,resizable=1,left=0,top=0');return false;"><?=$ag_nm[0]['AGENT_NAME']?>Student Diary</a>
                                    <a class="btn btn-primary" data-toggle="modal" href="" onclick="javascript:void window.open('dashboard.php?op=<?=MD5('agent_1')?>&a_no=<?=$en_nm[0]['AGENT_NO']?>','1470134321367','width=800,height=500,toolbar=0,menubar=0,location=1,status=1,scrollbars=1,resizable=1,left=0,top=0');return false;"><?=$ag_nm[0]['AGENT_NAME']?>Agent Diary</a> -->
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel recent-app">
                                    <div class="x_title">
                                        <h2>Recent Enrol list</h2>
                                    </div>
                                    <div class="x_content">
                                        <div class="table-responsive workflow-page-section">
                                            <table class="table data-tbl-tools" style="width:100%;">
                                                <thead>
                                                    <tr class="headings">
                                                        <th class="column-title">Application Type:</th>
                                                        <th class="column-title">Enrol No</th>
                                                        <th class="column-title">Student No</th>
                                                        <th class="column-title">Student Name</th>
                                                        <th class="column-title">Student Email</th>
                                                        <th class="column-title">Agent Name</th>
                                                        <th class="column-title">Course Name</th>
                                                        <th class="column-title">Course Length</th>
                                                        <th class="column-title">Status</th>
                                                        <th class="column-title no-link last"><span class="nobr">Action</span>
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php 
                                                        $a="where `application_type`=".'96';
                                                        $a1= getRows($a,'workflow');
                                                                                          foreach($a1 as $row_wf)
                                                                                          {
                                                                                      ?>   
                                                    <tr class="even pointer">
                                                        <?php $app="where `LOOKUP_CODE_NO`=".$row_wf[application_type];
                                                            $app_type= getRows($app,'lookup_code'); 
                                                            $app1="where `LOOKUP_TYPE_NO`=".$app_type[0][LOOKUP_TYPE_NO];
                                                            $app_type1= getRows($app1,'lookup_type');?>
                                                        <td class=" "><?=$app_type[0]['NAME']?>(<?=$app_type1[0]['TABLE_NAME']?>)</td>
                                                        <?php $cond="where `STUD_NO`=".$row_wf[student_id];
                                                            $stud_nm= getRows($cond,'enrol');?>
                                                        <td class=" "><a href="dashboard.php?op=<?=MD5('enrolment_details')?>&eid=<?=$stud_nm[0]['ENROL_NO']?>"><?=$stud_nm[0]['ENROL_NO']?></a></td>
                                                        <?php $cond1="where `STUD_NO`=".$row_wf[student_id];
                                                            $stud_nm1= getRows($cond1,'student');?>
                                                        <td class=" "><a href="dashboard.php?op=<?=MD5('student_edit')?>&sid=<?=$stud_nm1[0]['STUD_NO']?>"><?=$stud_nm1[0]['EXT_STN']?></a></td>
                                                        <td class=" "><a href="dashboard.php?op=<?=MD5('student_edit')?>&sid=<?=$stud_nm1[0]['STUD_NO']?>"><?=$stud_nm1[0]['FNAME']?><?=$stud_nm1[0]['LNAME']?></a></td>
                                                        <td><a href="mailto:<?=$stud_nm1[0]['L_EMAIL']?>?Subject=<?=$app_type[0]['NAME']?>" target="_top"><?=$stud_nm1[0]['L_EMAIL']?></a></td>
                                                        <?php 
                                                            if($stud_nm[0][AGENT_NO])
                                                             {
                                                            	$cond2="where `AGENT_NO`=".$stud_nm[0][AGENT_NO];
                                                            	 $stud_nm2= getRows($cond2,'agent'); } ?>
                                                        <td class=" "><?=$stud_nm2[0]['AGENT_NAME']?></td>
                                                        <?php
                                                            if($stud_nm[0][COURSE_NO])
                                                             {
                                                            	$cond3="where `COURSE_NO`=".$stud_nm[0][COURSE_NO];
                                                            	 $stud_nm3= getRows($cond3,'course'); } ?>
                                                        <td class=""><?=$stud_nm3[0]['COURSE_NAME']?></td>
                                                        <td class=""><?=$stud_nm[0]['CRSE_LEN']?></td>
                                                        <td class="">
                                                            <?php												
                                                                $stat_date = $row_wf['create_date'];
                                                                $today = date('d/m/Y');
                                                                                                       $complete = $dia_summ[0]['DIARY_DATE'];
                                                                $cancel=1;
                                                                if($today==$stat_date){
                                                                	$favcolor = "start";
                                                                }elseif($today==$tomorrow){
                                                                	$favcolor = "processing";
                                                                }elseif($complete==$today){
                                                                	$favcolor = "complete";
                                                                }elseif($cencel==0){
                                                                	$favcolor = "cencel";
                                                                }
                                                                
                                                                switch ($favcolor) {
                                                                	case "start":
                                                                		echo '<i class="fa fa-circle blue-blub" aria-hidden="true"></i>
                                                                                                       <i class="fa fa-circle" aria-hidden="true"></i>
                                                                                                       <i class="fa fa-circle " aria-hidden="true"></i>
                                                                                                       <i class="fa fa-circle " aria-hidden="true"></i>';
                                                                		break;
                                                                	case "processing":
                                                                		echo '<i class="fa fa-circle" aria-hidden="true"></i>
                                                                                                       <i class="fa fa-circle yellow-blub" aria-hidden="true"></i>
                                                                                                       <i class="fa fa-circle " aria-hidden="true"></i>
                                                                                                       <i class="fa fa-circle " aria-hidden="true"></i>';
                                                                		break;
                                                                	case "complete":
                                                                		echo '<i class="fa fa-circle" aria-hidden="true"></i>
                                                                                                       <i class="fa fa-circle" aria-hidden="true"></i>
                                                                                                       <i class="fa fa-circle green-blub" aria-hidden="true"></i>
                                                                                                       <i class="fa fa-circle " aria-hidden="true"></i>';
                                                                		break;
                                                                	case "cencel":
                                                                		echo '<i class="fa fa-circle" aria-hidden="true"></i>
                                                                                                       <i class="fa fa-circle" aria-hidden="true"></i>
                                                                                                       <i class="fa fa-circle " aria-hidden="true"></i>
                                                                                                       <i class="fa fa-circle red-blub" aria-hidden="true"></i>';
                                                                		break;
                                                                	default:
                                                                		echo '<i class="fa fa-circle" aria-hidden="true"></i>
                                                                                                       <i class="fa fa-circle" aria-hidden="true"></i>
                                                                                                       <i class="fa fa-circle " aria-hidden="true"></i>
                                                                                                       <i class="fa fa-circle " aria-hidden="true"></i>';
                                                                }
                                                                ?>
                                                        </td>
                                                        <td class="last"><a class="btn btn-success assign" data-toggle="modal" data-target="#editBox" href="file.php?id=<?=$stud_nm[0]['STUD_NO']?>"><span class="glyphicon glyphicon-pencil"></span>Assign</a> | <a href="" onclick="javascript:void window.open('dashboard.php?op=<?=MD5('mail_send')?>&d_no=<?=$dia_summ[0]['DIARY_NO']?>&s_id=2&s_no=<?=$stud_nm[0]['EXT_STN']?>&s_fname=<?=$stud_nm[0]['FNAME']?>&s_lname=<?=$stud_nm[0]['MNAME']?>&email=<?=$stud_nm[0]['L_EMAIL']?>&addre=<?=$stud_nm[0]['LADDR1']?>&course=<?=$co_nm[0]['COURSE_NAME']?>&en_st=<?=$en_nm[0]['ST_DATE']?>','1470138621367','width=800,height=500,toolbar=0,menubar=0,location=1,status=1,scrollbars=1,resizable=1,left=0,top=0');return false;">Send message</a></td>
                                                    </tr>
                                                    <?php }?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--    <div class="modal fade" id="editBox" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
    <?php    if($_POST['submit']=='sub')
        {
        		if(insert_record("insert into `assign` set `std_no`='".$_POST['std_no']."', `std_fname`='".$_POST['std_fname']."', `f_stuff`='".$_POST['f_stuff']."', `a_stuff`='".$_POST['a_stuff']."', `to_date`='".$_POST['to_date']."', `from_date`='".$_POST['from_date']."'"))
        		$name= $_POST['std_fname'];
        		$stuff= $_POST['f_stuff'];
        		$account= $_POST['a_stuff'];
        		$to_date= $_POST['to_date'];
        		$from_date= $_POST['from_date'];
        		$response="<table>	
        			<tr>
        			<td >Hello Jeeva
        			
        			</td></tr>
        
        			</table>
        			<table width=\"100%\" cellpadding=\"5\" cellspacing=\"5\" align=\"center\">
        			
        			<tr>
        			<td colspan=\"3\">Assignment Details</td>
        			</tr>
        			
        				
        			<tr>
        			<td><strong>Name</strong></td>
        			<td><strong>:</strong></td>
        			<td><strong>$name</strong></td>
        			</tr>
        			
        			<tr>
        			<td><strong>Stuff</strong></td>
        			<td><strong>:</strong></td>
        			<td><strong>$name</strong></td>
        			</tr>
        		
        			<tr>
        			<td><strong>Account Stuff</strong></td>
        			<td><strong>:</strong></td>
        			<td><strong>$account</strong></td>
        			</tr>
        		
        			<tr>
        			<td><strong>To assign</strong></td>
        			<td><strong>:</strong></td>
        			<td><strong>$to_date</strong></td>
        			</tr>
        			
        			<tr>
        			<td><strong>From assign</strong></td>
        			<td><strong>:</strong></td>
        			<td><strong>$from_date</strong></td>
        			</tr>
        			
        			</table>";
        	
        			$response1 = '
        			
        						<style type="text/css">
        							.clearfix:after {
        							content: "";
        							display: table;
        							clear: both;
        							}
        							a {
        							color: #5D6975;
        							text-decoration: underline;
        							}
        							body {
        							position: relative;
        							width: 21cm;  
        							height: 29.7cm; 
        							margin: 0 auto; 
        							color: #001028;
        							background: #FFFFFF; 
        							font-family: Arial, sans-serif; 
        							font-size: 12px; 
        							font-family: Arial;
        							}
        							header {
        							padding: 10px 0;
        							margin-bottom: 30px;
        							}
        							#logo {
        							text-align: center;
        							margin-bottom: 10px;
        							}
        							#logo img {
        							width: 90px;
        							}
        							h1 {
        							border-top: 1px solid  #5D6975;
        							border-bottom: 1px solid  #5D6975;
        							color: #5D6975;
        							font-size: 2.4em;
        							line-height: 1.4em;
        							font-weight: normal;
        							text-align: center;
        							margin: 0 0 20px 0;
        							background: url(dimension.png);
        							}
        							#project {
        							float: left;
        							}
        							#project span {
        							color: #5D6975;
        							text-align: right;
        							width: 76px;
        							margin-right: 10px;
        							display: inline-block;
        							font-size: 0.8em;
        							}
        							#company {
        							float: right;
        							text-align: right;
        							}
        							#project div,
        							#company div {
        							white-space: nowrap;        
        							}
        							table {
        							width: 100%;
        							border-collapse: collapse;
        							border-spacing: 0;
        							margin-bottom: 20px;
        							}
        							table tr:nth-child(2n-1) td {
        							background: #F5F5F5;
        							}
        							table th,
        							table td {
        							text-align: center;
        							}
        							table th {
        							padding: 5px 20px;
        							color: #5D6975;
        							border-bottom: 1px solid #C1CED9;
        							white-space: nowrap;        
        							font-weight: normal;
        							}
        							table .service,
        							table .desc {
        							text-align: left;
        							}
        							table td {
        							padding: 20px;
        							text-align: center;
        							}
        							table td.service,
        							table td.desc {
        							vertical-align: top;
        							}
        							table td.unit,
        							table td.qty,
        							table td.total {
        							font-size: 1.2em;
        							}
        							table td.grand {
        							border-top: 1px solid #5D6975;;
        							}
        							#notices .notice {
        							color: #5D6975;
        							font-size: 1.2em;
        							}
        							footer {
        							color: #5D6975;
        							width: 100%;
        							height: 30px;
        							position: absolute;
        							bottom: 0;
        							border-top: 1px solid #C1CED9;
        							padding: 8px 0;
        							text-align: center;
        							}
        							#project span{ text-transform: uppercase; }
        						</style>
        						<body>
        							<header class="clearfix">
        								<div id="logo">
        									<img src="logo.png">
        								</div>
        								<h1>ACCIT College</h1>
        								<div id="company" class="clearfix">
        									<div>College Name</div>
        									<div>455 Foggy Heights,<br /> AZ 85004, US</div>
        									<div>(602) 519-0450</div>
        									<div><a href="mailto:College@example.com">College@example.com</a></div>
        								</div>
        								<div id="project">
        									<div><span>Student No</span> E002</div>
        									<div><span>Student Name</span> Tamal</div>
        									<div><span>Gender</span> Male</div>
        									<div><span>DOB</span> 20 july 1991</div>
        									<div><span>ADDRESS</span> 796 Silver Harbour, TX 79273, US</div>
        									<div><span>EMAIL</span> <a href="mailto:john@example.com">john@example.com</a></div>
        									<div><span>Phone</span> 002102151522</div>
        									<div><span>DATE</span> August 17, 2015</div>
        								</div>
        							</header>
        							<main>
        								<table>
        									<thead>
        										<tr>
        											<th class="desc">Course Id</th>
        											<th class="desc">Course Name</th>
        											<th>PRICE</th>
        											<th>TOTAL</th>
        										</tr>
        									</thead>
        									<tbody>
        										<tr>
        											<td class="service">c00145</td>
        											<td class="service">Design</td>
        											<td class="unit">$40.00</td>
        											<td class="total">$1,040.00</td>
        										</tr>
        										<tr>
        											<td class="service">c00145</td>
        											<td class="service">Development</td>
        											<td class="unit">$40.00</td>
        											<td class="total">$3,200.00</td>
        										</tr>
        									</tbody>
        								</table>
        							</main>
        
        			';
        		$to = $_POST['std_email'];
        		$subject = "Contact Details";
        		//$txt = "Hello world!";
        		$headers  = 'MIME-Version: 1.0' . "\r\n";
        		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
        		$headers .= "From: info@kingswebsoft.com" . "\r\n";
        		$admin_mail = "tamal.paperlinksoftwars@gmail.com";
        		
        		@mail($to,$subject,$response1,$headers);
        		if(@mail($admin_mail,$subject,$response,$headers))
        			$_SESSION['msg'] ="We have received your Messages.We will get back to you soon.";
        		//header("Location:".$_SERVER['PHP_SELF']);
        		
        else
        $_SESSION['msg']="<strong>Oh snap!</strong> Not Assing";
        }
        ?>
         // Content Will show Here
        </div>
    </div>
    </div>
    <div class="modal fade in" id="myModal-custom-short" style="display: none; padding-left: 19px;">
    <div class="modal-dialog">
    <div class="modal-content">
    <div class="modal-header">
        <a style="margin-top: 5px;border: none;" data-dismiss="modal" class="btn btn-default">
        <span class="glyphicon glyphicon-remove"></span></a>
    </div>
    <div class="modal-body short-section">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">Short</h3>
            </div>
            <div class="panel-body">
                <form name="basicform">
                    <ul>
                        <li>
                            <a class="btn btn-primary" href="#" id="add-lbl">Add Level</a>
                        </li>
                        <li>
                            <a class="btn btn-primary" href="#" id="dlete-lbl">Delete Level</a>
                        </li>
                    </ul>
                    <div class="responsive-table">
                        <table class="table">
                            <thead>
                              <tr>
                                <th>Column (Short By)</th>
                                <th>Short On</th>
                                <th>Order</th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr>
                                <td> 
                                    <select class="form-control">
                                      <option>Column A</option>
                                      <option>Column B</option>
                                      <option>Column C</option>
                                      <option>Column D</option>
                                      <option>Column E</option>
                                    </select>
                                </td>
                                <td>
                                    <select class="form-control">
                                      <option>Column A</option>
                                      <option>Column B</option>
                                      <option>Column C</option>
                                      <option>Column D</option>
                                      <option>Column E</option>
                                    </select>
                                </td>
                                <td>
                                    <select class="form-control">
                                      <option>Column A</option>
                                      <option>Column B</option>
                                      <option>Column C</option>
                                      <option>Column D</option>
                                      <option>Column E</option>
                                    </select>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                    </div>
                    <div class="show-objct">
                        Your Data Show Here...
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <div class="btn-group">
            <button data-dismiss="modal" class="btn btn-danger"><span class="glyphicon glyphicon-remove"></span> Cancel</button>
            <button class="btn btn-primary"><span class="glyphicon glyphicon-check"></span> Save</button>
        </div>
    </div>
    </div>
    <!-- /.modal-content -->
</div>
<!-- /.modal-dalog -->
</div>
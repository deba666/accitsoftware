<?php
    /*
        * @package    ACCIT
         * @author    Paperlink Softwares Team
         * @copyright    Copyright (c) 2016 , Paperlink Softwares Pvt. Ltd.  (http://www.paperlinksoftwares.com/)
         * @since    Version 1.0.0
         * @filesource
		 * Code for add ecoe for a particular student
		 * Links comes under student edit page(Visa field)
         */
    
    if($_POST['STATUS_NO'])
    	{
    	if(insert_record("insert into `attend_tracking` set `STATUS_NO`='".$_POST['STATUS_NO']."',`CUSTOMER_NO`='".$_GET['cust_no']."',`STUD_NO`='".$_GET['stu_no']."',`CODE`='".$_POST['CODE']."',`DESCRIPTION`='".$_POST['DESCRIPTION']."',`FROM_DATE`='".$_POST['FROM_DATE']."',`TO_DATE`='".$_POST['TO_DATE']."',`ABSENT_TOTAL`='".$_POST['ABSENT_TOTAL']."',`ABSENT_INC_TOTAL`='".$_POST['ABSENT_INC_TOTAL']."',`CURRENT_TOTAL`='".$_POST['CURRENT_TOTAL']."',`OVERALL_TOTAL`='".$_POST['OVERALL_TOTAL']."',`OVERALL_PERCENT`='0.00',`CURRENT_PERCENT`='0.00',`CURRENT_INC_PERCENT`='".$_POST['CURRENT_INC_PERCENT']."',`OVERALL_INC_PERCENT`='".$_POST['OVERALL_INC_PERCENT']."',`CREATE_USER_NO`='".$_POST['USER_NO']."',`CREATE_DATE`='".$_POST['CREATE_DATE']."',`DELETE_DATE`='".$_POST['DELETE_DATE']."',`DELETE_USER_NO`='".$_POST['DELETE_USER_NO']."',`UPDATE_DATE`='".$_POST['UPDATE_DATE']."',`UPDATE_USER_NO`='".$_POST['UPDATE_USER_NO']."',`LOCK_NUM`='1'"))
    	  
    		$_SESSION['s_msg']="<strong>Fine!</strong> New eCoE Added";
    		//}
    		else//{
    			$_SESSION['e_msg']="<strong>Oh snap!</strong> eCoE is not added";
    		//}
    	}
    	
    	
    if($_GET['del'])
    {
    	if(delete_record("DELETE FROM `student` WHERE `STUD_NO` = ".$_GET['del']))
    		$_SESSION['s_msg']="Item successfully deleted";
    }
    
    ?>
<div class="right_col" role="main">
<div class="">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="page-title">
            <div class="title_left">
                <?php 
                    if($_SESSION['s_msg'] || $_SESSION['e_msg'])
                    {
                    ?>
                <div class="alert <?=($_SESSION['s_msg'])?'alert-success':'alert-error'?> fade in">
                    <button data-dismiss="alert" class="close" type="button">�</button>
                    <?=($_SESSION['s_msg'])?$_SESSION['s_msg']:$_SESSION['e_msg']?>
                </div>
                <?php
                    unset($_SESSION['s_msg']);
                    unset($_SESSION['e_msg']);
                    }
                    ?>
                <h3>Visa</h3>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="clearfix"></div>
                <div class="x_content">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel recent-app">
                                <div class="x_content">
                                    <div class="student-account-page">
                                        <!-- Nav tabs -->
                                        <div class="card">
                                            <div class="all-students-list add-course">
                                                <div class="add-student-section">
                                                    <form class="form-horizontal form-label-left" action="" method="post" novalidate>
                                                        <button type="submit" class="btn btn-success">save</button>
                                                        <ul id="student-acc-tab" class="nav nav-tabs" role="tablist">
                                                            <li role="presentation"  class="active"><a href="#details" aria-controls="details" role="tab" data-toggle="tab">Details</a></li>
                                                        </ul>
                                                        <div class="tab-content">
                                                            <!--Details-->
                                                            <div role="tabpanel" class="tab-pane active" id="details">
                                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                                    <div class="row">
                                                                        <h4>
                                                                            <fieldset>Basic Info</fieldset>
                                                                        </h4>
                                                                        <div class="item form-group">
                                                                            <?php date_default_timezone_set('Australia/Melbourne');
                                                                                $date = date('d/m/Y h:i:s a', time());
                                                                                ?>
                                                                            <input id="who"  name="CREATE_DATE"  type="hidden" value="<?php echo $date?>">
                                                                            <input id="when"  name="USER_NO" type="hidden" value="<?php echo $_SESSION['user_no']?>">
                                                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Status</label>
                                                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                                                <div class="row">
                                                                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                                                                        <select name="STATUS_NO" class="form-control" >
                                                                                            <option value="1">Active</option>
                                                                                            <option value="2">Cancelled</option>
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="item form-group">
                                                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">eCoE No</label>
                                                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                                                <div class="row">
                                                                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                                                                        <input id="email" class="form-control col-md-12 col-xs-12" name="CODE" placeholder="eCoE No" type="text">
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="item form-group">
                                                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">From</label>
                                                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                                                <div class="row">
                                                                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                                                                        <input value="<?php echo date("Y-m-d");?>" class="form-control col-md-12 col-xs-12" name="FROM_DATE" type="date">
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="item form-group">
                                                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">To</label>
                                                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                                                <div class="row">
                                                                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                                                                        <input value="<?php echo date("Y-m-d");?>" type="date" name="TO_DATE" class="form-control col-md-12 col-xs-12">
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                    </form>
                                                    </div>
                                                    <!--notes-->
                                                    <div role="tabpanel" class="tab-pane" id="notes">
                                                    </div>
                                                    </div>
                                                    </div>
                                                   
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
    /*
        * @package    ACCIT
         * @author    Paperlink Softwares Team
         * @copyright    Copyright (c) 2016 , Paperlink Softwares Pvt. Ltd.  (http://www.paperlinksoftwares.com/)
         * @since    Version 1.0.0
         * @filesource
		 * Code for sending mail template to student
         */
    	
    if($_GET['stud_name'])
       {
    	   include('class/docxtemplate.class.php');
    
    		$docx = new DOCXTemplate('templates/ACCIT-OfferLetter.tpl.docx');
    		$docx->set('STUD_FNAME', $_GET["s_fname"]);
    		$docx->set('DATE', date('d.m.Y'));
    		$docx->set('S_STUD_NO', $_GET["s_no"]);
		$docx->set('STUD_LNAME', $_GET["s_lname"]);
    		$docx->set('S_DOB', $_GET["s_dob"]);
    		$docx->set('S_CUSTOM_4', $_GET["s_passport"]);
    		$docx->set('S_COUNTRY_NAME', $_GET["s_country_name"]);
    		$docx->set('S_L_EMAIL', $_GET["email"]);
    		$docx->set('O_OFFER_NO', $_GET["of_no"]);
    		$docx->set('E_COURSE_CODE', $_GET["pr_code"]);
    		$docx->set('E_COURSE_NAME', $_GET["of_desc"]);
		$docx->set('E_ST_DATE', $_GET["st_date"]);
    		$docx->set('E_END_DATE', $_GET["end_date"]);
		$docx->set('E_CRSE_LEN', $_GET["len"]);
    		
    		
    		$docx->saveAs('templates/downloads/ACCIT-OfferLetter.docx'); // or $docx->downloadAs('Academic First Warning.docx');
    		
    		//////////////////////End/////////////////////////////////////////
    		 
    
       		
    		$response =	'
    		<center>
                <table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" style="border-collapse:collapse;margin:0;padding:0;background-color:#c3d6db;height:100%!important;width:100%!important">
                    <tbody><tr>
                        <td align="center" valign="top" style="margin:0;padding:20px;border-top:0;height:100%!important;width:100%!important">
                            
                            <table border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse:collapse;border:0">
                                <tbody><tr>
                                    <td align="center" valign="top">
                                        
                                        <table border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse:collapse;min-width:100%;background-color:#c3d6db;border-top:0;border-bottom:0">
                                            <tbody><tr>
                                            	<td valign="top" style="padding-top:9px"></td>
                                            </tr>
                                        </tbody></table>
                                        
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" valign="top">
                                        
                                        <table border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse:collapse;min-width:100%;background-color:#ffffff;border-top:5px solid #3795ac;border-bottom:0">
                                            <tbody><tr>
                                                <td valign="top"><table border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;border-collapse:collapse">
        <tbody>
                <tr>
                    <td valign="top" style="padding:0px">
                        <table align="left" width="100%" border="0" cellpadding="0" cellspacing="0" style="min-width:100%;border-collapse:collapse">
                            <tbody><tr>
                                <td valign="top" style="padding-right:0px;padding-left:0px;padding-top:0;padding-bottom:0;text-align:center">
    
    
                                           
    
                                </td>
                            </tr>
                        </tbody></table>
                    </td>
                </tr>
        </tbody>
    </table></td>
                                            </tr>
                                        </tbody></table>
                                        
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" valign="top">
                                        
                                        <table border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse:collapse;min-width:100%;background-color:#ffffff;border-top:0;border-bottom:0">
                                            <tbody><tr>
                                                <td valign="top"><table border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;border-collapse:collapse">
        <tbody>
                <tr>
                    <td valign="top" style="padding:9px">
                        <table align="left" width="100%" border="0" cellpadding="0" cellspacing="0" style="min-width:100%;border-collapse:collapse">
                            <tbody><tr>
                                <td valign="top" style="padding-right:9px;padding-left:9px;padding-top:0;padding-bottom:0;text-align:center">
    
                                        <img align="center" alt="" src="http://designingworld.com.au/test/administrator/templates/mail/header.png" width="128" style="max-width:100%;padding-bottom:0;display:inline!important;vertical-align:bottom;border:0;outline:none;text-decoration:none; width:600px;margin-top: -9px" class="CToWUd">
    
                                            
                                        </a>
    
                                </td>
                            </tr>
                        </tbody></table>
                    </td>
                </tr>
        </tbody>
    </table><table border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;border-collapse:collapse">
        <tbody>
            <tr>
                <td valign="top" style="padding-top:9px">
                  	
    			
    				
                    <table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width:100%;min-width:100%;border-collapse:collapse" width="100%">
                        <tbody><tr>
    
                            <td valign="top" style="padding:0px 18px 9px;color:#49a078;font-family:Arial,&quot;Helvetica Neue&quot;,Helvetica,sans-serif;font-size:24px;font-style:normal;font-weight:bold;line-height:125%;text-align:left">
     <img align="center" alt="" src="http://designingworld.com.au/test/administrator/templates/mail/header2.png" width="128" style="max-width:100%;margin-top: -18px;padding-bottom:0;display:inline!important;vertical-align:bottom;border:0;outline:none;text-decoration:none; width:600px" class="CToWUd">
    
                              
                            </td>
                        </tr>
                    </tbody></table>
    				
    
    				
                </td>
            </tr>
        </tbody>
    </table><table border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;border-collapse:collapse">
        <tbody>
            <tr>
                <td valign="top" style="padding-top:9px">
                  	
    			
    				
                    <table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width:100%;min-width:100%;border-collapse:collapse" width="100%">
                        <tbody><tr>
    
                            <td valign="top" style="padding:0px 18px 9px;color:#000000;font-family:Arial,&quot;Helvetica Neue&quot;,Helvetica,sans-serif;font-size:14px;font-style:normal;font-weight:normal;line-height:125%;text-align:left">
    
                               
                            </td>
                        </tr>
                    </tbody></table>
    				
    
    				
                </td>
            </tr>
        </tbody>
    </table><table border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;border-collapse:collapse">
        <tbody>
            <tr>
                <td style="padding-top:0;padding-right:18px;padding-bottom:18px;padding-left:18px" valign="top" align="center">
                    <table border="0" cellpadding="0" cellspacing="0" style="border-collapse:separate!important;border:2px solid #fc883f;border-radius:5px;background-color:#fc883f">
                        <tbody>
                            <tr>
                                <td align="center" valign="middle" style="font-family:Arial,&quot;Helvetica Neue&quot;,Helvetica,sans-serif;font-size:16px;padding:16px">
                                  <p>Templete Name</p>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table><table border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;border-collapse:collapse">
        <tbody>
            <tr>
                <td valign="top" style="padding-top:9px">
                  	
    			
    				
                    <table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width:100%;min-width:100%;border-collapse:collapse" width="100%">
                        <tbody><tr>
    
                            <td valign="top" style="padding:0px 18px 9px;color:#000000;font-family:Arial,&quot;Helvetica Neue&quot;,Helvetica,sans-serif;font-size:14px;font-style:normal;font-weight:normal;line-height:125%;text-align:left">
    
                            </td>
                        </tr>
                    </tbody></table>
    				
    
    				
                </td>
            </tr>
        </tbody>
    </table></td>
                                            </tr>
                                        </tbody></table>
                                        
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" valign="top">
                                        
                                        <table border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse:collapse;min-width:100%;background-color:#3896ad;border-top:0;border-bottom:0">
                                            <tbody><tr>
                                                <td valign="top" style="padding-bottom:9px"><table border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;border-collapse:collapse">
        <tbody>
            <tr>
                <td align="center" valign="top" style="padding:9px">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;border-collapse:collapse">
        <tbody><tr>
            <td align="center" style="padding-left:9px;padding-right:9px">
                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;background-color:#3896ad;border:1px none #eeeeee;border-collapse:collapse">
                    <tbody><tr>
                        <td align="center" valign="top" style="padding-top:9px;padding-right:9px;padding-left:9px">
                            <table align="center" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse">
                                <tbody><tr>
                                    <td align="center" valign="top">
                                        
    
                                            
    
    
                                                <table align="left" border="0" cellpadding="0" cellspacing="0" style="display:inline;border-collapse:collapse">
                                                    <tbody><tr>
                                                        <td valign="top" style="padding-right:10px;padding-bottom:9px">
                                                            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse">
                                                                <tbody><tr>
                                                                    <td align="left" valign="middle" style="padding-top:5px;padding-right:10px;padding-bottom:5px;padding-left:9px">
                                                                        <table align="left" border="0" cellpadding="0" cellspacing="0" width="" style="border-collapse:collapse">
                                                                            <tbody><tr>
    
                                                                                    <td align="center" valign="middle" width="24">
                                                                                       
                                                                                    </td>
    
    
                                                                            </tr>
                                                                        </tbody></table>
                                                                    </td>
                                                                </tr>
                                                            </tbody></table>
                                                        </td>
                                                    </tr>
                                                </tbody></table>
    
                                            
    
                                            
    
    
                                                <table align="left" border="0" cellpadding="0" cellspacing="0" style="display:inline;border-collapse:collapse">
                                                    <tbody><tr>
                                                        <td valign="top" style="padding-right:10px;padding-bottom:9px">
                                                            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse">
                                                                <tbody><tr>
                                                                    <td align="left" valign="middle" style="padding-top:5px;padding-right:10px;padding-bottom:5px;padding-left:9px">
                                                                        <table align="left" border="0" cellpadding="0" cellspacing="0" width="" style="border-collapse:collapse">
                                                                            <tbody><tr>
    
                                                                                    <td align="center" valign="middle" width="24">
                                                                                       
                                                                                    </td>
    
    
                                                                            </tr>
                                                                        </tbody></table>
                                                                    </td>
                                                                </tr>
                                                            </tbody></table>
                                                        </td>
                                                    </tr>
                                                </tbody></table>
    
                                            
    
                                            
    
    
                                                <table align="left" border="0" cellpadding="0" cellspacing="0" style="display:inline;border-collapse:collapse">
                                                    <tbody><tr>
                                                        <td valign="top" style="padding-right:0;padding-bottom:9px">
                                                            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse">
                                                                <tbody><tr>
                                                                    <td align="left" valign="middle" style="padding-top:5px;padding-right:10px;padding-bottom:5px;padding-left:9px">
                                                                        <table align="left" border="0" cellpadding="0" cellspacing="0" width="" style="border-collapse:collapse">
                                                                            <tbody><tr>
    
                                                                                    <td align="center" valign="middle" width="24">
                                                                                       
                                                                                    </td>
    
    
                                                                            </tr>
                                                                        </tbody></table>
                                                                    </td>
                                                                </tr>
                                                            </tbody></table>
                                                        </td>
                                                    </tr>
                                                </tbody></table>
    
                                            
    
                                        
                                    </td>
                                </tr>
                            </tbody></table>
                        </td>
                    </tr>
                </tbody></table>
            </td>
        </tr>
    </tbody></table>
    
                </td>
            </tr>
        </tbody>
    </table><table border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;border-collapse:collapse">
        <tbody>
            <tr>
                <td valign="top" style="padding-top:9px">
                  	
    			
    				
                    <table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width:100%;min-width:100%;border-collapse:collapse" width="100%">
                        <tbody><tr>
    
                            <td valign="top" style="padding:0px 18px 9px;font-size:10px;color:#c3d6db;font-family:Tahoma,sans-serif;line-height:125%;text-align:left">
    
                                <div style="text-align:center">Copyright © 2016 ACCIT, All rights reserved.</div>
    
    <div style="text-align:center"><br>
    Our mailing address is:<br>
    <div><span>ACCIT</span><div><div>495 Kent St</div><div>Sydney</div><span>NSW</span><span>2000  </span>Austrilia <span></span></div><br><a href="#" target="_blank" data-saferedirecturl="#">Add us to your address book</a></div> <br>
    <br>
    Want to change how you receive these emails?<br>
    You can&nbsp;<a href="#" style="word-wrap:break-word;color:#c3d6db;font-weight:normal;text-decoration:underline" target="_blank" data-saferedirecturl="#">update your preferences</a>&nbsp;or&nbsp;<a href="#" style="word-wrap:break-word;color:#c3d6db;font-weight:normal;text-decoration:underline" target="_blank" data-saferedirecturl="#">unsubscribe from this list</a></div>
    
                            </td>
                        </tr>
                    </tbody></table>
    				
    
    				
                </td>
            </tr>
        </tbody>
    </table></td>
                                            </tr>
                                        </tbody></table>
                                        
                                    </td>
                                </tr>
                            </tbody></table>
                            
                        </td>
                    </tr>
                </tbody></table>
            </center>
    		';
    		
    		
       }
       ?> 
<div class="col-md-12 col-sm-12 col-xs-12">
    <div class="page-title">
        <div class="title_left">
            <h3>See Mail</h3>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="clearfix"></div>
            <?php echo $response;?>
        </div>
    </div>
</div>
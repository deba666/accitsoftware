<?php
    /*
        * @package    ACCIT
         * @author    Paperlink Softwares Team
         * @copyright    Copyright (c) 2016 , Paperlink Softwares Pvt. Ltd.  (http://www.paperlinksoftwares.com/)
         * @since    Version 1.0.0
         * @filesource
         */
    ?>
<div class="right_col" role="main">
    <div class="">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="page-title">
                <div class="title_left">
                    <?php 
                        if($_SESSION['s_msg'] || $_SESSION['e_msg'])
                        {
                        ?>
                    <div class="alert <?=($_SESSION['s_msg'])?'alert-success':'alert-error'?> fade in">
                        <button data-dismiss="alert" class="close" type="button">�</button>
                        <?=($_SESSION['s_msg'])?$_SESSION['s_msg']:$_SESSION['e_msg']?>
                    </div>
                    <?php
                        unset($_SESSION['s_msg']);
                        unset($_SESSION['e_msg']);
                        }
                        ?>
                    <h3>Add Student</h3>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="clearfix"></div>
                    <div class="x_content">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel recent-app">
                                    <div class="x_content">
                                        <div class="student-account-page">
                                            <!-- Nav tabs -->
                                            <div class="card">
                                                <div class="all-students-list add-course">
                                                    <div class="add-student-section">
                                                        <form class="form-horizontal form-label-left" action="" method="post" novalidate>
                                                            <button type="submit" class="btn btn-success">Add</button>
                                                            <div class="tab-content">
                                                                <!--Details-->
                                                                <div role="tabpanel" class="tab-pane active" id="details1">
                                                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                                                        <div class="row">
                                                                            <h4>
                                                                                <fieldset>Step 1: Chose a Student</fieldset>
                                                                            </h4>
                                                                            <div class="item form-group">
                                                                                <label class="control-label col-md-12 col-sm-12 col-xs-12" for="name">
                                                                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                                                                        <div class="row">
                                                                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                                                                <?php date_default_timezone_set('Australia/sydney');
                                                                                                    $date = date('d/m/Y h:i:s a', time());
                                                                                                    ?>
                                                                                                <input id="who"  name="WHO_DATE"  type="hidden" value="<?php echo $date?>">
                                                                                                <input id="when"  name="user" type="hidden" value="<?php echo $_SESSION['user_no']?>">
                                                                                                <input name="AGENT_CODE" type="text" class="col-md-12 col-xs-12 form-control">
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </label>
                                                                            </div>
                                                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                                                <div class="item form-group">
                                                                                    <label class="control-label col-md-6 col-sm-6 col-xs-12">
                                                                                    <input type="hidden" name="G_LOCAL" id="course_publish" value="0" >
                                                                                    <input type="checkbox" name="G_LOCAL" value="1"/>Shoe Automatically Filled And Unlimited
                                                                                    </label>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <h4>
                                                                            <fieldset>Step2: Select an Option</fieldset>
                                                                        </h4>
                                                                        <div class="col-md-12 col-sm-12col-xs-12">
                                                                            <input id="date" name="END_BY"  type="radio" disabled>Automatically fill schedule      What is this ?
                                                                        </div>
                                                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                                                            <input id="date" name="END_BY"  type="radio" disabled>Add to this instance only
                                                                        </div>
                                                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                                                            <input id="date" name="END_BY"  type="radio" disabled>Add to classes until<input id="date" name="END_BY1" class="form-control col-md-6 col-xs-12  type="text" placeholder="MM/DD/YYYY" disabled>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                   		 </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
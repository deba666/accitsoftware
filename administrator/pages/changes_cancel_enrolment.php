<?php
    /*
        * @package    ACCIT
         * @author    Paperlink Softwares Team
         * @copyright    Copyright (c) 2016 , Paperlink Softwares Pvt. Ltd.  (http://www.paperlinksoftwares.com/)
         * @since    Version 1.0.0
         * @filesource
		 * code for cancel student enrolment.
         */
    
   // here the enrolment is delete from all the linked table 
    if($_GET['del'])
    {
    	if(delete_record("DELETE FROM `enrol` WHERE `ENROL_NO` = ".$_GET['del']))
    	{
    	if(delete_record("DELETE FROM `invoice` WHERE `ENROL_NO` = ".$_GET['del']))
    	{
    	if(delete_record("DELETE FROM `invoice_details` WHERE `ENROL_NO` = ".$_GET['del']))
    		$_SESSION['s_msg']="Enrolment Cancelled Successfully";
    	}
    	}	
    }
    ?>
<div class="right_col" role="main">
    <div class="">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="page-title">
                <div class="title_left">
                    <h3>Cancel Enrolment</h3>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="clearfix"></div>
                    <div class="x_content">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel recent-app">
                                    <div class="x_content">
                                        <div class="student-account-page">
                                            <!-- Nav tabs -->
                                            <div class="card">
                                                <ul class="nav nav-tabs" role="tablist">
                                                    <li role="presentation"  class="active">
                                                        <a href="#all-student" aria-controls="all-student" role="tab" data-toggle="tab">Details</a>
                                                        <h4><a href="#" onclick="javascript:void window.open('dashboard.php?op=<?=MD5('display_new_tour')?>','147013847777','width=800,height=600,toolbar=0,menubar=0,location=1,status=1,scrollbars=1,resizable=1,left=0,top=0');return false;">New</a></h4>
                                                    </li>
                                                </ul>
                                                <!-- Tab panes -->
                                                <div class="tab-content">
                                                    <div role="tabpanel" class="tab-pane active" id="all-student">
                                                        <div class="all-students-list">
                                                            <div class="table-responsive">
                                                                <table  class="table data-tbl-tools">
                                                                    <thead>
                                                                        <tr class="headings">
                                                                            <th class="column-title">Enrol No</th>
                                                                            <th class="column-title">Stud No</th>
                                                                            <th class="column-title">First Name</th>
                                                                            <th class="column-title">Last Name</th>
                                                                            <th class="column-title">Course</th>
                                                                            <th class="column-title">Length</th>
                                                                            <th class="column-title">Course Start Date</th>
                                                                            <th class="column-title">Course End Date</th>
                                                                            <th class="column-title">Action</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <?php
																	//Display some information of the enroled student . Cancel link is bellow in the code
                                                                        $sql="select * from enrol where `ENROL_NO`=".$_GET['ENROL_NO'];
                                                                        $q=mysql_query($sql);
                                                                        while($r=mysql_fetch_array($q))
                                                                        {
                                                                        ?>
                                                                    <tr class="even pointer">
                                                                        <td class=" "><?=$r['ENROL_NO']?></td>
                                                                        <?php $cond3="where STUD_NO=".$r['STUD_NO'];
                                                                            $r3=getRows($cond3,'student');?>
                                                                        <td class=" "><?=$r3[0]['EXT_STN']?></td>
                                                                        <td class=" "><?=$r3[0]['FNAME']?></td>
                                                                        <td class=" "><?=$r3[0]['LNAME']?></td>
                                                                        <?php $cond2="where COURSE_NO=".$r['COURSE_NO'];
                                                                            $r2=getRows($cond2,'course');?>
                                                                        <td class=" "><?=$r2[0]['COURSE_NAME']?></td>
                                                                        <td class=" "><?=$r2[0]['COURSE_LEN']?></td>
                                                                        <td class=" "><?=$r['ST_DATE']?></td>
                                                                        <td class=" "><?=$r['END_DATE']?></td>
                                                                        <td class="last"><a href="dashboard.php?op=<?=MD5('changes_cancel_enrolment')?>&del=<?=$r['ENROL_NO'];?>" class="color-sky stting"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>Cancel Enrolment</a></td>
                                                                    </tr>
                                                                    <?php } ?>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
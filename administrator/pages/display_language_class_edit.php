<?php
    /*
        * @package    ACCIT
         * @author    Paperlink Softwares Team
         * @copyright    Copyright (c) 2016 , Paperlink Softwares Pvt. Ltd.  (http://www.paperlinksoftwares.com/)
         * @since    Version 1.0.0
         * @filesource
		 * Code for edit language classes . link comes from Language Classes(Side Bar->Classes->Language Classes).(Edit link)
         */
    
       if($_POST['LANG_CLASS_NAME'])
       {
       	if(mysql_query("update`lang_class` set `LANG_CLASS_NAME`='".$_POST['LANG_CLASS_NAME']."',`LANG_CLASS_CODE`='',`SESSION_NO`='".$_POST['SESSION_NO']."',`LEVEL_NO`='".$_POST['LEVEL_NO']."',`LOCATION_NO`='1',`LANG_CLASS_HRS`='',`TEACHER_NO`='".$_POST['TEACHER_NO']."',`ROOM_NO`='".$_POST['ROOM_NO']."',`STATUS_NO`='1',`NOTES`='',`CUSTOM_1`='".$_POST['CUSTOM_1']."',`CUSTOM_2`='".$_POST['CUSTOM_2']."',`CUSTOM_3`='".$_POST['CUSTOM_3']."',`CUSTOM_4`='".$_POST['CUSTOM_4']."',`CUSTOM_5`='".$_POST['CUSTOM_5']."',`CUSTOM_6`='".$_POST['CUSTOM_6']."',`CUSTOM_7`='".$_POST['CUSTOM_7']."',`DELETED`='',`CREATED`='',`USER_NO`='".$_POST['user']."' where `LANG_CLASS_NO`=".$_GET['lang_no']))
       			$_SESSION['s_msg']="<strong>Fine!</strong>Language Classes Updated Successfully";
       		else{
       			$_SESSION['e_msg']="<strong>Oh snap!</strong>Language Classes is not Update";
       }}
       if($_GET['lang_no'])
       {
       $co1="where `LANG_CLASS_NO`=".$_GET['lang_no']; 
       $std_row=getRows($co1,'lang_class');
       }
       ?>
       
       <div class="right_col" role="main">
<div class="col-md-12 col-sm-12 col-xs-12">
    <div class="page-title">
        <div class="title_left">
            <?php 
                if($_SESSION['s_msg'] || $_SESSION['e_msg'])
                {
                ?>
            <div class="alert <?=($_SESSION['s_msg'])?'alert-success':'alert-error'?> fade in">
                <button data-dismiss="alert" class="close" type="button">�</button>
                <?=($_SESSION['s_msg'])?$_SESSION['s_msg']:$_SESSION['e_msg']?>
            </div>
            <?php
                unset($_SESSION['s_msg']);
                unset($_SESSION['e_msg']);
                }
                ?>
            <h3>Edit Class</h3>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="clearfix"></div>
            <div class="x_content">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel recent-app">
                            <div class="x_content">
                                <div class="student-account-page edit-faculty-page">
                                    <!-- Nav tabs -->
                                    <div class="card">
                                        <div class="all-students-list add student">
                                            <div class="add-student-section">
                                                <form method="post" action="" class="form-horizontal form-label-left">
                                                    <div class="item form-group">
                                                        <label for="name" class="control-label col-md-2 col-sm-2 col-xs-2">Name</label>
                                                        <div class="col-md-8 col-sm-8 col-xs-8">
                                                            <div class="row">
                                                                <div class="col-md-7 col-sm-7 col-xs-12">
                                                                    <input id="when"  name="user" type="hidden" value="<?php echo $_SESSION['user_no']?>">
                                                                    <input name="LANG_CLASS_NAME" class="form-control col-md-12 col-xs-12" type="text" value="<?=$std_row[0]['LANG_CLASS_NAME']?>" required>  
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="item form-group">
                                                        <label for="name" class="control-label col-md-2 col-sm-2 col-xs-2">Session</label>
                                                        <div class="col-md-8 col-sm-8 col-xs-8">
                                                            <div class="row">
                                                                <div class="col-md-7 col-sm-7 col-xs-12">
                                                                    <select name="SESSION_NO" class="form-control col-md-12 col-xs-12" required>
                                                                        <option value="1" <?php if ($std_row[0]['SESSION_NO'] == '1') { echo 'selected="selected"';}?>>Session1</option>
                                                                        <option value="2" <?php if ($std_row[0]['SESSION_NO'] == '2') { echo 'selected="selected"';}?>>Session2</option>
                                                                        <option value="3" <?php if ($std_row[0]['SESSION_NO'] == '3') { echo 'selected="selected"';}?>>Session3</option>
                                                                        <option value="4" <?php if ($std_row[0]['SESSION_NO'] == '4') { echo 'selected="selected"';}?>>Session4</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="item form-group">
                                                        <label for="name" class="control-label col-md-2 col-sm-2 col-xs-2">Level</label>
                                                        <div class="col-md-8 col-sm-8 col-xs-8">
                                                            <div class="row">
                                                                <div class="col-md-7 col-sm-7 col-xs-12">
                                                                    <select name="LEVEL_NO" class="form-control col-md-12 col-xs-12" required>
                                                                        <option value="0" <?php if ($std_row[0]['LEVEL_NO'] == '0') { echo 'selected="selected"';}?>>(Not Set)</option>
                                                                        <option value="1" <?php if ($std_row[0]['LEVEL_NO'] == '1') { echo 'selected="selected"';}?>>1</option>
                                                                        <option value="2" <?php if ($std_row[0]['LEVEL_NO'] == '2') { echo 'selected="selected"';}?>>2</option>
                                                                        <option value="3" <?php if ($std_row[0]['LEVEL_NO'] == '3') { echo 'selected="selected"';}?>>3</option>
                                                                        <option value="4" <?php if ($std_row[0]['LEVEL_NO'] == '4') { echo 'selected="selected"';}?>>4</option>
                                                                        <option value="5" <?php if ($std_row[0]['LEVEL_NO'] == '5') { echo 'selected="selected"';}?>>5</option>
                                                                        <option value="6" <?php if ($std_row[0]['LEVEL_NO'] == '6') { echo 'selected="selected"';}?>>6</option>
                                                                        <option value="7" <?php if ($std_row[0]['LEVEL_NO'] == '7') { echo 'selected="selected"';}?>>7</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="item form-group">
                                                        <label for="name" class="control-label col-md-2 col-sm-2 col-xs-2">Room</label>
                                                        <div class="col-md-8 col-sm-8 col-xs-8">
                                                            <div class="row">
                                                                <div class="col-md-7 col-sm-7 col-xs-12">
                                                                    <select name="ROOM_NO" class="form-control col-md-12 col-xs-12" required>
                                                                    <?php
                                                                        $dd_res=mysql_query("Select *  from room");
                                                                        while($r=mysql_fetch_array($dd_res))
                                                                          { 
                                                                        if($std_row[0]['ROOM_NO']==$r['ROOM_NO'])
                                                                        	  {
                                                                             echo "<option value='$r[ROOM_NO]' selected> $r[ROOM_DESC] </option>";
                                                                            }
                                                                        	else
                                                                        	  {
                                                                        		echo "<option value='$r[ROOM_NO]'> $r[ROOM_DESC] </option>";
                                                                        	  }
                                                                          }
                                                                        
                                                                           ?>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <h4>
                                                        <fieldset>Teacher</fieldset>
                                                    </h4>
                                                    <div class="control-label col-md-12 col-sm-12 col-xs-12">
                                                        <div class="col-md-2 col-sm-2 col-xs-2">
                                                            <div class="form-group">
                                                                Teacher
                                                                <select name="TEACHER_NO" class="form-control col-md-12 col-xs-12" required>
                                                                    <option value="0"></option>
                                                                    <?php
                                                                        $dd_res1=mysql_query("Select *  from teacher");
                                                                        while($r1=mysql_fetch_array($dd_res1))
                                                                          { 
                                                                        if($std_row[0]['TEACHER_NO']==$r1['TEACHER_NO'])
                                                                        	  {
                                                                             echo "<option value='$r1[TEACHER_NO]' selected> $r1[TEACHER_CODE] </option>";
                                                                            }
                                                                        	else
                                                                        	  {
                                                                        		echo "<option value='$r1[TEACHER_NO]'> $r1[TEACHER_CODE] </option>";
                                                                        	  }
                                                                          }
                                                                        
                                                                           ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2 col-sm-2 col-xs-2">
                                                            <div class="form-group">Mon Hrs
                                                                <input type="number" class="form-control col-md-12 col-xs-12" placeholder="0" name="CUSTOM_1" value="<?=$std_row[0]['CUSTOM_1']?>" required>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2 col-sm-2 col-xs-2">
                                                            <div class="form-group">Tue Hrs
                                                                <input type="number" class="form-control col-md-12 col-xs-12" placeholder="0" name="CUSTOM_2" value="<?=$std_row[0]['CUSTOM_2']?>" required>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2 col-sm-2 col-xs-2">
                                                            <div class="form-group">Wed Hrs
                                                                <input type="number" class="form-control col-md-12 col-xs-12" placeholder="0" name="CUSTOM_3" value="<?=$std_row[0]['CUSTOM_3']?>" required>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2 col-sm-2 col-xs-2">
                                                            <div class="form-group">Thu Hrs
                                                                <input type="number" class="form-control col-md-12 col-xs-12" placeholder="0" name="CUSTOM_4" value="<?=$std_row[0]['CUSTOM_4']?>" required>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2 col-sm-2 col-xs-2">
                                                            <div class="form-group">Fri Hrs
                                                                <input type="number" class="form-control col-md-12 col-xs-12" placeholder="0" name="CUSTOM_5" value="<?=$std_row[0]['CUSTOM_5']?>" required>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2 col-sm-2 col-xs-2">
                                                            <div class="form-group">Sat Hrs
                                                                <input type="number" class="form-control col-md-12 col-xs-12" placeholder="0" name="CUSTOM_6" value="<?=$std_row[0]['CUSTOM_6']?>">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2 col-sm-2 col-xs-2">
                                                            <div class="form-group">Sun Hrs
                                                                <input type="number" class="form-control col-md-12 col-xs-12" placeholder="0" name="CUSTOM_7" value="<?=$std_row[0]['CUSTOM_7']?>">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="item form-group">
                                                        <label for="name" class="control-label col-md-3 col-sm-3 col-xs-12"></label>
                                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                                            <div class="row">
                                                                <div class="col-md-6 col-sm-4 col-xs-12">
                                                                    <p class="pull-left"><br/>
                                                                        <button class="btn btn-primary" type="submit">Cancel</button>
                                                                        <button class="btn btn-success" type="submit" onclick="needToConfirm = false;">Submit</button>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
       </div>
<?php
function calculateEndDateOffer($start_date,$length,$array_terms)
{
    if(count($array_terms)>0)
    {
        for($k=0;$k<$length;$k++)
        {           
            $date_obj = new DateTime($start_date);
            $date_add = $date_obj->modify('+'.$k.' day');
            $date_to_check = $date_add->format('Y-m-d');           
            for($i=0;$i<count($array_terms);$i++)
            {
                if($array_terms[$i]['extend']==1)
                {   
                    $date_to_check_timestamp = strtotime($date_to_check); 
                    $date_from_timestamp = strtotime($array_terms[$i]['start_date']);
                    $date_to_timestamp = strtotime($array_terms[$i]['end_date']);                  
                    if($date_to_check_timestamp >= $date_from_timestamp && $date_to_check_timestamp <= $date_to_timestamp)
                    {                   
                        $length++;                                  
                    }   
                }              
            }          
        }
        return $date_to_check;
    }
    else
    {
        $date_obj = new DateTime($start_date);
        $date_add = $date_obj->modify('+'.$length.' day');
        $date_to_check = $date_add->format('Y-m-d');     
        return $date_to_check;
    }
    
}

function calculateAttendance($type,$student_id,$eid)
{
    $curr_date_to_check_timestamp = strtotime(date("Y-m-d")); 
    $enrol_details = mysql_fetch_array(mysql_query("SELECT `ST_DATE`, `END_DATE` , `CRSE_LEN` , `CRT_NO` FROM `enrol` WHERE `ENROL_NO` = '".$eid."'"));
    if($curr_date_to_check_timestamp >= strtotime($enrol_details['ST_DATE']) && $curr_date_to_check_timestamp <= strtotime($enrol_details['END_DATE']))
    {
        $check_holiday = mysql_fetch_array(mysql_query("SELECT COUNT(`HOLIDAY_NO`) AS C FROM `HOLIDAY` WHERE (CURRENT_DATE() BETWEEN  (STR_TO_DATE(FROM_DATE,'%m/%d/%Y')) AND (STR_TO_DATE(TO_DATE,'%m/%d/%Y')))"));
        if($check_holiday[0]==0)
        {       
            $holiday_to = mysql_fetch_array(mysql_query("SELECT `FROM_DATE` , CURRENT_DATE() as CURRENTDATE , DATEDIFF(STR_TO_DATE(FROM_DATE,'%m/%d/%Y'),CURRENT_DATE()) as DIFF from `holiday` WHERE `CRT_NO` = '".$enrol_details['CRT_NO']."' AND DATEDIFF(STR_TO_DATE(FROM_DATE,'%m/%d/%Y'),CURRENT_DATE())>0 order by DATEDIFF(STR_TO_DATE(FROM_DATE,'%m/%d/%Y'),CURRENT_DATE()) ASC LIMIT 0 , 1"));              
            $holiday_from = mysql_fetch_array(mysql_query("SELECT `TO_DATE` , CURRENT_DATE() as CURRENTDATE , DATEDIFF(CURRENT_DATE(),STR_TO_DATE(TO_DATE,'%m/%d/%Y')) as DIFF from `holiday` WHERE `CRT_NO` = '".$enrol_details['CRT_NO']."' AND DATEDIFF(CURRENT_DATE(),STR_TO_DATE(TO_DATE,'%m/%d/%Y'))>0 order by DATEDIFF(CURRENT_DATE(),STR_TO_DATE(TO_DATE,'%m/%d/%Y')) ASC LIMIT 0 , 1"));              
            
           
            if($holiday_from['TO_DATE']>0)
            {
                $from_date = $holiday_from['TO_DATE'];
            }
            else
            {
                $from_date = $enrol_details['ST_DATE'];
            }
            if($holiday_to['FROM_DATE']>0)
            {
                $to_date = $holiday_to['FROM_DATE'];
            }
            else
            {
                $to_date = $enrol_details['END_DATE'];
            }            
            $date_obj1 = new DateTime($from_date);
            $date_from = $date_obj1->modify('+1'.' day');
            $date_from = $date_from->format('Y-m-d'); 
            
            $date_obj2 = new DateTime($to_date);
            $date_to = $date_obj2->modify('-1'.' day');
            $date_to = $date_to->format('Y-m-d'); 
            
            
            $date_start = new DateTime($date_from);
            $date_end = new DateTime($date_to);
            $diff_days = $date_end->diff($date_start)->format("%a"); 
            $total_hours = $diff_days*5; 
            //$total_hours = $diff_days*7;
            $absent_hours = mysql_fetch_array(mysql_query("SELECT SUM(`HOURS`) as ABSENTHOURS FROM `absence` WHERE `ENROL_NO` = '".$eid."' AND `COUNTED` = '1'"));
            $present_hours = $total_hours - $absent_hours['ABSENTHOURS']; 
            $current_attendance_percentage = number_format((($present_hours/$total_hours)*100),2); 
            return $current_attendance_percentage;
           // $overall_attendance_percentage = number_format((($present_hours/$total_hours)*100),2); 
            if($type==1)
            {

            }
            else if($type==2)
            {

            }
            else
            {

            }
        }
    }
}
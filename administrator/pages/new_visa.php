<?php
    /*
        * @package    ACCIT
         * @author    Paperlink Softwares Team
         * @copyright    Copyright (c) 2016 , Paperlink Softwares Pvt. Ltd.  (http://www.paperlinksoftwares.com/)
         * @since    Version 1.0.0
         * @filesource
		 * Code for applying new visa for a particular student
		 
         */
    
    if($_POST['TYPE_NO'])
    	{
    	if(insert_record("insert into `customer_visa` set `TYPE_NO`='".$_POST['TYPE_NO']."',`CUSTOMER_NO`='".$_GET['cust_no']."',`CODE`='".$_POST['CODE']."',`FROM_DATE`='".$_POST['FROM_DATE']."',`TO_DATE`='".$_POST['TO_DATE']."',`USER_NO`='".$_POST['USER_NO']."',`CREATE_DATE`='".$_POST['CREATE_DATE']."',`DELETE_DATE`='".$_POST['DELETE_DATE']."',`LOCK_NUM`='1'"))
    	{  
    		$_SESSION['s_msg']="<strong>Fine!</strong> New Visa Added";
        ?>
<script>
           
    
        window.opener.location.reload();
        self.close();
   
     </script>
<?php
        }	
    		else {
    			$_SESSION['e_msg']="<strong>Oh snap!</strong> Visa is not added";
    		
    	}
        }	
    	
    if($_GET['del'])
    {
    	if(delete_record("DELETE FROM `student` WHERE `std_id` = ".$_GET['del']))
    		$_SESSION['s_msg']="Item successfully deleted";
    }
    
    ?>
   <script>
function checkDo()
{
    if(document.getElementById('visa_dates').checked==true)
    {
        document.getElementById('arr_date1').style.display="none";
        document.getElementById('arr_date2').style.display="none";
    }
    else
    {
        document.getElementById('arr_date1').style.display="";
        document.getElementById('arr_date2').style.display="";
    }
    return false;
}
</script>
<div class="right_col" role="main">
<div class="">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="page-title">
            <div class="title_left">
                <?php 
                    if($_SESSION['s_msg'] || $_SESSION['e_msg'])
                    {
                    ?>
                <div class="alert <?=($_SESSION['s_msg'])?'alert-success':'alert-error'?> fade in">
                    <button data-dismiss="alert" class="close" type="button">�</button>
                    <?=($_SESSION['s_msg'])?$_SESSION['s_msg']:$_SESSION['e_msg']?>
                </div>
                <?php
                    unset($_SESSION['s_msg']);
                    unset($_SESSION['e_msg']);
                    }
                    ?>
                <h3>Visa</h3>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="clearfix"></div>
                <div class="x_content">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel recent-app">
                                <div class="x_content">
                                    <div class="student-account-page">
                                        <!-- Nav tabs -->
                                        <div class="card">
                                            <div class="all-students-list add-course">
                                                <div class="add-student-section">
                                                    <form class="form-horizontal form-label-left" action="" method="post" novalidate>
                                                        
                                                        <ul id="student-acc-tab" class="nav nav-tabs" role="tablist">
                                                            <li role="presentation"  class="active"><a href="#details" aria-controls="details" role="tab" data-toggle="tab">Details</a></li>
                                                        </ul>
                                                        <div class="tab-content">
                                                            <!--Details-->
                                                            <div role="tabpanel" class="tab-pane active" id="details">
                                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                                    <div class="row">
                                                                        <h4>
                                                                            <fieldset>Basic Info</fieldset>
                                                                        </h4>
                                                                        <div class="item form-group">
                                                                            <?php date_default_timezone_set('Australia/Sydeny');
                                                                                $date = date('d/m/Y h:i:s a', time());
                                                                                ?>
                                                                            <input id="who"  name="CREATE_DATE"  type="hidden" value="<?php echo $date?>">
                                                                            <input id="when"  name="USER_NO" type="hidden" value="<?php echo $_SESSION['user_no']?>">
                                                                            <label class="control-label col-md-3 col-sm-3 col-xs-4" for="name">Type</label>
                                                                            <div class="col-md-8 col-sm-8 col-xs-8">
                                                                                <div class="row">
                                                                                    <div class="col-md-8 col-sm-8 col-xs-8">
                                                                                        <select id="location" name="TYPE_NO" class="form-control" required>
                                                                                            <?php
                                                                                                $sql=mysql_query("select * from lookup_code where LOOKUP_TYPE_NO=".'30');
                                                                                                while($row=mysql_fetch_array($sql))
                                                                                                {
                                                                                                ?>
                                                                                            <option value="<?=$row['LOOKUP_CODE_NO']?>"><?=$row['NAME']?></option>
                                                                                            <?php } ?>
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="item form-group">
                                                                            <label class="control-label col-md-3 col-sm-3 col-xs-4" for="name">Visa No</label>
                                                                            <div class="col-md-9 col-sm-9 col-xs-8">
                                                                                <div class="row">
                                                                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                                                                        <input id="email" class="form-control col-md-12 col-xs-12" name="CODE" placeholder="Visa No" type="text" required>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        
                                                                            <div class="col-md-12 col-sm-12 col-xs-4">
                                                                                <div class="form-group">
                                                                                    <div class="checkbox">
                                                                                        <label class="pull-left control-label">
                                                                                            <input name="SESSION" value="0" type="radio" checked="checked"  class="flat" onclick="javascript: checkDo();" id="visa_dates"> I don't have visa date yet
                                                                                        </label>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                       
                                                                        
                                                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                                                <div class="form-group">
                                                                                    <div class="checkbox">
                                                                                        <label class="pull-left control-label">
                                                                                            <input name="SESSION" value="1" type="radio" class="flat" onclick="javascript: checkDo();" id="visa_dates"> Visa dates are
                                                                                        </label>

                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                       
                                                                        <div class="item form-group" id="arr_date1" style="display: none;">
                                                                            <label class="control-label col-md-3 col-sm-3 col-xs-3" for="name">Arrive</label>
                                                                            <div class="col-md-9 col-sm-9 col-xs-8">
                                                                                <div class="row">
                                                                                    <div class="col-md-12 col-sm-12 col-xs-8">
                                                                                        <input class="form-control col-md-12 col-xs-12" name="FROM_DATE" type="date">
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="item form-group" id="arr_date2" style="display: none;">
                                                                            <label class="control-label col-md-3 col-sm-3 col-xs-3" for="name">Expiry</label>
                                                                            <div class="col-md-9 col-sm-9 col-xs-8">
                                                                                <div class="row">
                                                                                    <div class="col-md-12 col-sm-12 col-xs-8">
                                                                                        <input type="date" name="TO_DATE" class="form-control col-md-12 col-xs-12">
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <br/>
                                                                            
                                                                        </div>
<p class="pull-left"><br/>
                                                                        <input class="btn btn-primary" type="button" onclick="javascript: window.close();" value="Cancel">
                                                                        <button class="btn btn-success" type="submit">Submit</button>
                                                                    </p>
                                                    				</form>
                                                    			</div>
                                                    <!--notes-->
                                                    <div role="tabpanel" class="tab-pane" id="notes">
                                                    </div>
                                                   		 </div>
                                                    	</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
    /*
        * @package    ACCIT
         * @author    Paperlink Softwares Team
         * @copyright    Copyright (c) 2016 , Paperlink Softwares Pvt. Ltd.  (http://www.paperlinksoftwares.com/)
         * @since    Version 1.0.0
         * @filesource
         */
    
    if($_POST['stud_no'])
    {
      /*  if(insert_record("insert into `absence` set `ENROL_NO`='$a',`SUBJECT_NO`='',`ABSENCE_DATE`='".$_POST['ABSENCE_DATE']."',`HOURS`='".$_POST['HOURS']."',`REASON`='".$_POST['REASON']."',`COUNTED`='".$_POST['COUNTED']."',`USER_NO`='".$_POST['user']."',`WHO_DATE`='".$_POST['WHO_DATE']."',`RESULT_NO`='',`LANG_CLASS_CODE`='1'")){
                $_SESSION['s_msg']="<strong>Fine!</strong>Student Absence Added Successfully";
                }
            else {
                $_SESSION['e_msg']="<strong>Oh snap!</strong>Absence is not added";
                }
       */
       }

    if(isset($_POST['stuExt']) && count($_POST['stuExt']) >0){
        foreach ($_POST['stuExt'] as $key => $stuExt) {
            if(isset($_POST[$stuExt.'DAY']) && $_POST[$stuExt.'DAY'] != ''){
                $ENROL_NO   = $_POST['ENROL_NO'];
                $DAY        = $_POST[$stuExt.'DAY'];

                foreach ($DAY as $key1 => $dayName) {
                    $ABSENCE_DATE = date( 'Y-m-d', strtotime( 'monday this week' ) );
                    if($key1 > 0){
                        $ABSENCE_DATE = date( 'Y-m-d', strtotime( $ABSENCE_DATE.' + '.$key1.' Days' ) );
                    }

                    if(!empty($dayName)){
                        $sql = "INSERT INTO `absence` SET   
                            `ENROL_NO`          = '".$ENROL_NO[$key]."',
                            `SUBJECT_NO`        = '',
                            `ABSENCE_DATE`      = '".$ABSENCE_DATE."',
                            `HOURS`             = '".$dayName."',
                            `REASON`            = 'Auto Generated',
                            `COUNTED`           = '0',
                            `USER_NO`           = '".$_POST['user']."',
                            `WHO_DATE`          = '".$_POST['WHO_DATE']."',
                            `RESULT_NO`         = '',
                            `LANG_CLASS_CODE`   = ''
                        ";
                        insert_record($sql);
                    }
                }
                
            }
        }
    }
    ?>
<script language="javascript" type="text/javascript" src="datetimepicker.js"></script>
<div class="right_col" role="main">
    <div class="">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="page-title">
                <div class="title_left">
                    <?php 
                        if($_SESSION['s_msg'] || $_SESSION['e_msg'])
                        {
                        ?>
                    <div class="alert <?=($_SESSION['s_msg'])?'alert-success':'alert-error'?> fade in">
                        <button data-dismiss="alert" class="close" type="button">�</button>
                        <?=($_SESSION['s_msg'])?$_SESSION['s_msg']:$_SESSION['e_msg']?>
                    </div>
                    <?php
                        unset($_SESSION['s_msg']);
                        unset($_SESSION['e_msg']);
                                               }
                                              ?>
                    <h3>Absences</h3>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="clearfix"></div>
                    <div class="x_content">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel recent-app">
                                    <div class="x_content">
                                        <div class="student-account-page view-faculty-page">
                                            <!-- Nav tabs -->
                                            <div class="card">
                                                <div class="all-students-list add student">
                                                    <div class="add-student-section">
                                                        <div class="table-responsive">
                                                            <form method="POST" action="" class="form-horizontal form-label-left">
                                                                <table id="" class="display table table-striped table-bordered table data-tbl-tools" cellspacing="0" width="100%">
                                                                    <thead>
                                                                        <div class="row">
                                                                            <div class="col-md-3 col-sm-3 col-xs-4">
                                                                                <input class="control-label col-md-12 col-sm-12 col-xs-12" type="date" name="ABSENCE_DATE" value="<?=$monday = date( 'd/m/Y', strtotime( 'monday this week' ) );?>">
                                                                            </div>
                                                                            <p class="Style_3spaceSingle">
                                                                                <span style="margin-right: 50px;"> Class:  <?=$_GET['class']?></span>
                                                                               
                                                                                <span style="margin-right: 50px;">Level:  <?=$_GET['level']?></span>
                                                                                
                                                                                <span style="margin-right: 50px;">Room:   <?=$_GET['room']?></span>
                                                                                
                                                                                <span style="margin-right: 50px;">Teacher:    <?=$_GET['teacher']?></span>
                                                                                
                                                                            </p>
                                                                        </div>
                                                                        <tr class="headings">
                                                                            <th class="column-title">Student
                                                                            </th>
                                                                            <th class="column-title">Student No / Tour
                                                                            </th>
                                                                            <th class="column-title">Mon-<?=$monday = date( 'd-F', strtotime( 'monday this week' ) );?>
                                                                            </th>
                                                                            <th class="column-title">Tue-<?=$monday = date( 'd-F', strtotime( 'tuesday this week' ) );?>
                                                                            </th>
                                                                            <th class="column-title">Wed-<?=$monday = date( 'd-F', strtotime( 'wednesday this week' ) );?>
                                                                            </th>
                                                                            <th class="column-title">Thu-<?=$monday = date( 'd-F', strtotime( 'thursday this week' ) );?>
                                                                            </th>
                                                                            <th class="column-title">Fri-<?=$monday = date( 'd-F', strtotime( 'friday this week' ) );?>
                                                                            </th>
                                                                        </tr>
                                                                    </thead>
                                                                    <?php
                                                                        $co="where `LANG_CLASS_NO`=".$_GET['abs'];
                                                                        $st=getRows($co,'course');
                                                                        ?>
                                                                    <tbody>
                                                                        <?php
                                                                            $sl=0;
                                                                            foreach($st as $st1)
                                                                            {
                                                                            $co1="where `COURSE_NO`=".$st1['COURSE_NO'];
                                                                            $st2=getRows($co1,'enrol');
                                                                            foreach($st2 as $st3)
                                                                            {
                                                                            ?>
                                                                        <tr class="even pointer">
                                                                            <input type="hidden" value="<?=$_SESSION['user_no']?>" name="user"/>
                                                                            <input type="hidden" value="<?php date_default_timezone_set('Australia/sydney');
                                                                                echo $date = date('d/m/Y h:i:s A', time());?>" name="WHO_DATE"/>
                                                                            <?php $cond="where `STUD_NO`=".$st3['STUD_NO']; $std=getRows($cond,'student');?>
                                                                            <td><?=$std[0]['LNAME'];?>,<?=$std[0]['MNAME'];?><?=$std[0]['FNAME'];?></td>
                                                                            <td><?=$std[0]['EXT_STN'];?>
                                                                                <input type="hidden" value="<?=$std[0]['EXT_STN'];?>" name="stuExt[]"/>
                                                                                <input type="hidden" value="<?=$st3['ENROL_NO'];?>" name="ENROL_NO[]"/>
                                                                            </td>
                                                                            <input type="hidden" name="stud_no" value="<?=$std[0]['STUD_NO'];?>">
                                                                            <input type="hidden" name="abs" value="<?=$st[0]['LANG_CLASS_NO']?>">
                                                                            <td><input type="text" name="<?=$std[0]['EXT_STN'];?>DAY[]"></td>
                                                                            <td><input type="text" name="<?=$std[0]['EXT_STN'];?>DAY[]"></td>
                                                                            <td><input type="text" name="<?=$std[0]['EXT_STN'];?>DAY[]"></td>
                                                                            <td><input type="text" name="<?=$std[0]['EXT_STN'];?>DAY[]"></td>
                                                                            <td><input type="text" name="<?=$std[0]['EXT_STN'];?>DAY[]"></td>                   
                                                                        </tr>  
                                                                         <?php } }?>
                                                                    </tbody>
                                                                 </table>
                                                                 <div class="item form-group">
                                                                    <label for="name" class="control-label col-md-3 col-sm-3 col-xs-12"></label>
                                                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                                                      <div class="row">
                                                                        <div class="col-md-6 col-sm-4 col-xs-12">
                                                                          <p class="pull-left">
                                                                            <button class="btn btn-primary" type="submit">Cancel</button>
                                                                            <button class="btn btn-success" type="submit" name="submitAbs">Submit</button>
                                                                          </p>
                                                                        </div>
                                                                      </div>
                                                                    </div>
                                                                  </div>
                                                             </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
    /*
        * @package    ACCIT
         * @author    Paperlink Softwares Team
         * @copyright    Copyright (c) 2016 , Paperlink Softwares Pvt. Ltd.  (http://www.paperlinksoftwares.com/)
         * @since    Version 1.0.0
         * @filesource
         */
    
    if($_POST['class_name'])
    	{
    	if(insert_record("insert into `lang_class` set `LANG_CLASS_CODE`='".$_POST['class_name']."',`SESSION_NO`='".$_POST['class_session']."',`LEVEL_NO`='".$_POST['class_level']."',`ROOM_NO`='".$_POST['class_room']."',`USER_NO`='".$_POST['user']."'"))
    		$_SESSION['s_msg']="<strong>Fine!</strong> Student class Added";
    		else{
    			$_SESSION['e_msg']="<strong>Oh snap!</strong> Student classes is not added";
    		}
    	}
    ?>
<div class="right_col" role="main">
    <div class="">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="page-title">
                <div class="title_left">
                    <?php 
                        if($_SESSION['s_msg'] || $_SESSION['e_msg'])
                        {
                        ?>
                    <div class="alert <?=($_SESSION['s_msg'])?'alert-success':'alert-error'?> fade in">
                        <button data-dismiss="alert" class="close" type="button">×</button>
                        <?=($_SESSION['s_msg'])?$_SESSION['s_msg']:$_SESSION['e_msg']?>
                    </div>
                    <?php
                        unset($_SESSION['s_msg']);
                        unset($_SESSION['e_msg']);
                        }
                        ?>
                    <h3>Add Class</h3>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="clearfix"></div>
                    <div class="x_content">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel recent-app">
                                    <div class="x_content">
                                        <div class="student-account-page edit-faculty-page">
                                            <!-- Nav tabs -->
                                            <div class="card">
                                                <div class="all-students-list add student">
                                                    <div class="add-student-section">
                                                        <form class="form-horizontal form-label-left" action="" method="post">
                                                            <div class="item form-group">
                                                                <input type="hidden" name="status" value="unread" />
                                                                <input id="when"  name="user" type="hidden" value="<?php echo $_SESSION['user_no']?>">
                                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Name</label>
                                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                                    <div class="row">
                                                                        <div class="col-md-7 col-sm-7 col-xs-12">
                                                                            <input id="name" class="form-control col-md-12 col-xs-12" name="class_name" placeholder="Enter Class Name" type="text" required>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="item form-group">
                                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Session</label>
                                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                                    <div class="row">
                                                                        <div class="col-md-7 col-sm-7 col-xs-12">
                                                                            <select name="class_session" class="form-control" >
                                                                                <option value="1">Session1</option>
                                                                                <option value="2">Session2</option>
                                                                                <option value="3">Session3</option>
                                                                                <option value="4">Session4</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="item form-group">
                                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Level</label>
                                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                                    <div class="row">
                                                                        <div class="col-md-7 col-sm-7 col-xs-12">
                                                                            <select name="class_level" class="form-control" >
                                                                                <option value="0">(Not Set)</option>
                                                                                <option value="1">1</option>
                                                                                <option value="2">2</option>
                                                                                <option value="3">3</option>
                                                                                <option value="4">4</option>
                                                                                <option value="5">5</option>
                                                                                <option value="6">6</option>
                                                                                <option value="7">7</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="item form-group">
                                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Room</label>
                                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                                    <div class="row">
                                                                        <div class="col-md-6 col-sm-4 col-xs-12">
                                                                            <select name="class_room" class="form-control">
                                                                                <option value="0">(Not Set)</option>
                                                                                <?php
                                                                                    $dd_res=mysql_query("Select *  from room");
                                                                                    while($r=mysql_fetch_array($dd_res))
                                                                                      { 
                                                                                         echo "<option value='$r[ROOM_NO]'> $r[ROOM_DESC] </option>";
                                                                                        }
                                                                                       ?>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="item form-group">
                                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name"></label>
                                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                                    <div class="row">
                                                                        <div class="col-md-6 col-sm-4 col-xs-12">
                                                                            <p class="pull-left">
                                                                                <button type="submit" class="btn btn-primary">Cancel</button>
                                                                                <button type="submit" class="btn btn-success">Submit</button>
                                                                            </p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
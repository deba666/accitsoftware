<?php
    /*
        * @package    ACCIT
         * @author    Paperlink Softwares Team
         * @copyright    Copyright (c) 2016 , Paperlink Softwares Pvt. Ltd.  (http://www.paperlinksoftwares.com/)
         * @since    Version 1.0.0
         * @filesource
         */
    ?>
<div class="right_col" role="main">
    <div class="">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="page-title">
                <div class="title_left">
                    <h3>All Students</h3>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="clearfix"></div>
                    <div class="x_content">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel recent-app">
                                    <div class="x_content">
                                        <div class="student-account-page">
                                            <!-- Nav tabs -->
                                            <div class="card">
                                                <ul class="nav nav-tabs" role="tablist">
                                                    <li role="presentation"  class="active"><a href="#all-student" aria-controls="all-student" role="tab" data-toggle="tab">All Students</a></li>
                                                    <!--<li role="presentation"><a href="#add-student" aria-controls="add-student" role="tab" data-toggle="tab">Add Students</a></li>
                                                        <li role="presentation"><a href="#student-account" aria-controls="student-account" role="tab" data-toggle="tab">Students Account</a></li>-->
                                                </ul>
                                                <!-- Tab panes -->
                                                <div class="tab-content">
                                                    <div role="tabpanel" class="tab-pane active" id="all-student">
                                                        <div class="all-students-list">
                                                            <div class="table-responsive">
                                                                <table  class="table data-tbl-tools">
                                                                    <thead>
                                                                        <tr class="headings">
                                                                            <th class="column-title">Student No.</th>
                                                                            <th class="column-title">Name </th>
                                                                            <th class="column-title">Gender</th>
                                                                            <th class="column-title">DOB</th>
                                                                            <th class="column-title">Phone</th>
                                                                            <th class="column-title">Email</th>
                                                                            <th class="column-title no-link last"><span class="nobr">Action</span> </th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        <?php $std_row="select * from student ORDER BY STUD_NO DESC";
                                                                            $q=mysql_query($std_row);
                                                                            while($row=mysql_fetch_array($q))
                                                                            {
                                                                            $name = $row['FNAME'] ." ". $row['MNAME'] ." ". $row['LNAME'];
                                                                            ?>
                                                                        <tr class="even pointer">
                                                                            <td class=" "><?=$row['EXT_STN'];?> </td>
                                                                            <td class=" "><?=$row['FNAME'];?>&nbsp;&nbsp;<?=$row['MNAME'];?>&nbsp;&nbsp;<?=$row['LNAME'];?></td>
                                                                            <td class=" "><?=$row['GENDER'];?></td>
                                                                            <?php
                                                                                $originalDate5 = $row['DOB'];
                                                                                $newDate5 = date("d/m/y", strtotime($originalDate5)); ?>	
                                                                            <td class=" "><?php echo $newDate5 ?></td>
                                                                            <td class=" "><?=$row['L_MOBILE'];?></td>
                                                                            <td class=" "><a href="mailto: <?=$row['L_EMAIL'];?>" ><?=$row['L_EMAIL'];?></a></td>
                                                                            <td class="last"><a href="dashboard.php?op=<?=MD5('offer')?>&stu_no=<?=$row['EXT_STN']?>&sname=<?php echo $name;?>&cust_no=<?=$row['CUSTOMER_NO']?>&cun_no=<?=$row['COUNTRY_NO']?>">Add Offer</a>
                                                                            </td>
                                                                        </tr>
                                                                        <?php   }    ?>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
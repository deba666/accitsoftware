<?php
    /*
        * @package    ACCIT
         * @author    Paperlink Softwares Team
         * @copyright    Copyright (c) 2016 , Paperlink Softwares Pvt. Ltd.  (http://www.paperlinksoftwares.com/)
         * @since    Version 1.0.0
         * @filesource
		 * code for tour student. Links come from sidebar(Tour groups->all tour->edit->students->Add new->students)
         */
    
    if($_POST['LNAME'])
    	{
    	if(insert_record("insert into `tour_student` set  `TOUR_NO`='".$_GET['tid']."',`LNAME`='".$_POST['LNAME']."',`FNAME`='".$_POST['FNAME']."',`GENDER`='".$_POST['GENDER']."',`DOB`='".$_POST['DOB']."',`NOTES`='".$_POST['NOTES']."',`USER_NO`='".$_POST['USER_NO']."',`WHO_DATE`='".$_POST['WHO_DATE']."'"))
    		$_SESSION['s_msg']="<strong>Fine!</strong> New Student Added For Tour";		
    		else
    			$_SESSION['e_msg']="<strong>Oh snap!</strong> Student is not added";	
    	}
    ?>
<script language="javascript" type="text/javascript" src="datetimepicker.js"></script>
<div class="right_col" role="main">
<div class="">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="page-title">
            <div class="title_left">
                <?php 
                    if($_SESSION['s_msg'] || $_SESSION['e_msg'])
                    {
                    ?>
                <div class="alert <?=($_SESSION['s_msg'])?'alert-success':'alert-error'?> fade in">
                    <button data-dismiss="alert" class="close" type="button">�</button>
                    <?=($_SESSION['s_msg'])?$_SESSION['s_msg']:$_SESSION['e_msg']?>
                </div>
                <?php
                    unset($_SESSION['s_msg']);
                    unset($_SESSION['e_msg']);
                    }
                    ?>
                <h3>Tour Group Details(Modify)</h3>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="clearfix"></div>
                <div class="x_content">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel recent-app">
                                <div class="x_content">
                                    <div class="student-account-page">
                                        <!-- Nav tabs -->
                                        <div class="card">
                                            <div class="all-students-list add-course">
                                                <div class="add-student-section">
                                                    <form class="form-horizontal form-label-left" action="" method="post" novalidate>
                                                        <ul id="student-acc-tab" class="nav nav-tabs" role="tablist">
                                                            <li role="presentation"  class="active"><a href="#details" aria-controls="details" role="tab" data-toggle="tab">Details</a></li>
                                                        </ul>
                                                        <div class="tab-content">
                                                            <!--Details-->
                                                            <div role="tabpanel" class="tab-pane active" id="details">
                                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                                    <div class="row">
                                                                        <h4>
                                                                            <fieldset>Basic Info</fieldset>
                                                                        </h4>
                                                                        <input id="when"  name="USER_NO" type="hidden" value="<?php echo $_SESSION['user_no']?>">
                                                                        <?php date_default_timezone_set('Australia/sydney');
                                                                            $date = date('d/m/Y h:i:s a', time());
                                                                            ?>
                                                                        <input id="who"  name="WHO_DATE"  type="hidden" value="<?php echo $date?>">  
                                                                        <div class="item form-group">
                                                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Last Name</label>
                                                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                                                <div class="row">
                                                                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                                                                        <input id="LNAME" class="form-control col-md-12 col-xs-12" name="LNAME" placeholder="Last Name" type="text">
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="item form-group">
                                                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">First Name</label>
                                                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                                                <div class="row">
                                                                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                                                                        <input id="FNAME" class="form-control col-md-12 col-xs-12" name="FNAME" placeholder="First Name" type="text">
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="item form-group">
                                                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Gender</label>
                                                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                                                <div class="row">
                                                                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                                                                        <select id="GENDER" name="GENDER" class="form-control" >
                                                                                            <option value="Male">Male</option>
                                                                                            <option value="Female">Female</option>
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="item form-group">
                                                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">DOB</label>
                                                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                                                    <input class="form-control col-md-12 col-xs-12" name="DOB" placeholder="mm/dd/yyyy" type="date">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="item form-group">
                                                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name.
                                                                                ">Notes</label>
                                                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                                                <div class="row">
                                                                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                                                                        <textarea name="NOTES" rows="4" cols="60" style="margin: 0px; width: 600px; height: 98px;"></textarea>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <button type="submit" class="btn btn-success">Save</button>
                                                   					 </form>
                                                   				 </div>
                                                   			 </div>
                                                   		 </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
    /*
        * @package    ACCIT
         * @author    Paperlink Softwares Team
         * @copyright    Copyright (c) 2016 , Paperlink Softwares Pvt. Ltd.  (http://www.paperlinksoftwares.com/)
         * @since    Version 1.0.0
         * @filesource
		 * Code for add new agent diary link comes from agent edit page(Add New Diary)
         */
    
       if($_POST['DIARY_DATE'])
       {
       	if(insert_record("insert into `diary` set `DIARY_DATE`='".$_POST['DIARY_DATE']."',`REF_NO`='".$_POST['AGENT_NO']."',`REF_TYPE`='3',`CONTACT_TYPE_NO`='',`ATTENTION`='".$_POST['ATTENTION']."',`PRIORITY`='',`SUMMARY`='".$_POST['SUMMARY']."',`NOTES`='".$_POST['NOTES']."',`CATEGORY_NO`='".$_POST['CATEGORY_NO']."',`CUSTOMER_NO`='".$_POST['CUSTOMER_NO']."',`PUBLISH`='0',`DOCUMENT_NO`='',`WHO_DATE`='".$_POST['WHO_DATE']."',`USER_NO`='".$_POST['user']."'"))
        { 
            $_SESSION['s_msg']="<strong>Fine!</strong>New Agent Employee Added Successfully";
            ?>
<script>
opener.location.reload(true);
     self.close();
     
     </script>
<?php
        }	else{
       			$_SESSION['e_msg']="<strong>Oh snap!</strong>New Agent Employee is not added";
       }
       }
       ?>
<script language="javascript" type="text/javascript" src="datetimepicker.js"></script>
<div class="col-md-12 col-sm-12 col-xs-12">
    <div class="page-title">
        <div class="title_left">
            <?php 
                if($_SESSION['s_msg'] || $_SESSION['e_msg'])
                {
                ?>
            <div class="alert <?=($_SESSION['s_msg'])?'alert-success':'alert-error'?> fade in">
                <button data-dismiss="alert" class="close" type="button">�</button>
                <?=($_SESSION['s_msg'])?$_SESSION['s_msg']:$_SESSION['e_msg']?>
            </div>
            <?php
                unset($_SESSION['s_msg']);
                unset($_SESSION['e_msg']);
                }
                ?>
            <h3>Diary Entry - Agent</h3>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="clearfix"></div>
            <div class="x_content">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel recent-app">
                            <div class="x_content">
                                <div class="student-account-page edit-faculty-page">
                                    <!-- Nav tabs -->
                                    <div class="card">
                                        <div class="all-students-list add student">
                                            <div class="add-student-section">
                                                <form method="post" action="" class="form-horizontal form-label-left">
                                                    <div class="item form-group">
                                                        <?php date_default_timezone_set('Australia/Melbourne');
                                                            $date = date('d/m/Y h:i:s a', time());
                                                            ?>
                                                        <input id="who"  name="WHO_DATE"  type="hidden" value="<?php echo $date?>">
                                                        <input id="when"  name="user" type="hidden" value="<?php echo $_SESSION['user_no']?>">
                                                        <label for="name" class="control-label col-md-2 col-sm-2 col-xs-2">Agent Name:</label>
                                                        <div class="col-md-8 col-sm-8 col-xs-8">
                                                            <div class="row">
                                                                <div class="col-md-7 col-sm-7 col-xs-12">
                                                                    <input required="" value="<?=$_GET['ag_name']?>" class="form-control col-md-12 col-xs-12" id="name" type="text" disabled>
                                                                    <input  value="<?=$_GET['ag_no']?>" class="form-control col-md-12 col-xs-12" id="name" type="hidden" name="AGENT_NO">
                                                                    <input  value="<?=$_GET['cus_no']?>" class="form-control col-md-12 col-xs-12" id="name" type="hidden" name="CUSTOMER_NO">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                
                                                    <div class="item form-group">
                                                        <label for="name" class="control-label col-md-2 col-sm-2 col-xs-2">Date</label>
                                                        <div class="col-md-8 col-sm-8 col-xs-8">
                                                            <div class="row">
                                                                <div class="col-md-7 col-sm-7 col-xs-12">
                                                                    <input class="form-control col-md-12 col-sm-12 col-xs-12" name="DIARY_DATE" value="" type="date" required>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                      <div class="item form-group">
                                                        <label for="name" class="control-label col-md-2 col-sm-2 col-xs-2">Attention</label>
                                                        <div class="col-md-8 col-sm-8 col-xs-8">
                                                            <div class="row">
                                                                <div class="col-md-7 col-sm-7 col-xs-12">
                                                                    <input type="hidden" name="ATTENTION"  value="0">
                                                                <input type="checkbox" name="ATTENTION" value="1" class="flat">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                    
                                                    
                                                    
                                                    
                                                    
                                                    
                                                    <div class="item form-group">
                                                        <label for="name" class="control-label col-md-2 col-sm-2 col-xs-2">Category</label>
                                                        <div class="col-md-8 col-sm-8 col-xs-8">
                                                            <div class="row">
                                                                <div class="col-md-7 col-sm-7 col-xs-12">
                                                                    <select name="CATEGORY_NO" class="form-control" required>
                                                                        <option value="0">NONE</option>
                                                                        <?php 
                                                                            $sql="select * from lookup_code where LOOKUP_TYPE_NO=1";
                                                                            $q=mysql_query($sql);
                                                                            while($r=mysql_fetch_array($q))
                                                                            {
                                                                            ?>
                                                                        <option value="<?=$r['LOOKUP_CODE_NO']?>"><?=$r['NAME']?></option>
                                                                        <?php } ?>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="item form-group">
                                                        <label for="name" class="control-label col-md-2 col-sm-2 col-xs-2">Subject</label>
                                                        <div class="col-md-8 col-sm-8 col-xs-8">
                                                            <div class="row">
                                                                <div class="col-md-7 col-sm-7 col-xs-12">
                                                                    <input required="" placeholder="Subject" name="SUMMARY" class="form-control col-md-12 col-xs-12" id="name" type="text" required>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="item form-group">
                                                        <label for="name" class="control-label col-md-2 col-sm-2 col-xs-2">Notes</label>
                                                        <div class="col-md-8 col-sm-8 col-xs-8">
                                                            <div class="row">
                                                                <div class="col-md-7 col-sm-7 col-xs-12">
                                                                    <textarea placeholder="Notes" name="NOTES" class="form-control col-md-12 col-xs-12" id="name" type="text"></textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="item form-group">
                                                        <label for="name" class="control-label col-md-3 col-sm-3 col-xs-12"></label>
                                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                                            <div class="row">
                                                                <div class="col-md-6 col-sm-4 col-xs-12">
                                                                    <p class="pull-left">
                                                                        <input class="btn btn-primary" type="button" onclick="javascript: window.close();" value="Cancel">
                                                                        <button class="btn btn-success" type="submit">Submit</button>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
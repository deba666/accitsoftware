<?php 

    /*
        * @package    ACCIT
         * @author    Paperlink Softwares Team
         * @copyright    Copyright (c) 2016 , Paperlink Softwares Pvt. Ltd.  (http://www.paperlinksoftwares.com/)
         * @since    Version 1.0.0
         * @filesource
		 * Code for display the dashboard
         */
session_start();
error_reporting(E_ALL ^ (E_NOTICE | E_WARNING | E_DEPRECATED));
include("settings.php");

 
if (!isset($_SESSION['frnd_admin']) && !isset($_SESSION['frnd_admin_id'])=='65535')
{
  echo "<script>window.location.assign('index.php')</script>";
}
$row_user = mysql_fetch_array(mysql_query("SELECT `PROFILE_IMAGE` FROM `users` WHERE `USER_NO` = '".$_SESSION['user_no']."'"));
?>
 <?php include("header.php"); ?>
    <body class="nav-md">
        <div class="container body">
            <div class="main_container">
                <div class="col-md-3 col-sm-3 col-xs-12 left_col">
                    <div class="left_col scroll-view">
                        <div class="navbar nav_title" style="border: 0;">
                            <a href="dashboard.php" class="site_title">
                                <span><img src="images/logo.png" alt="..." class="img-responsive"></span>
                            </a>
                        </div>
                        <div class="clearfix"></div>
                        <!-- menu profile quick info -->
                        <div class="profile">
                            <div class="profile_pic">
                                 <?php if($row_user['PROFILE_IMAGE']!='') { ?>
                                <img height="63" width="63" class="img-circle profile_img" src="upload/<?=$row_user['PROFILE_IMAGE']?>" border="0" />
                                                                            <?php } else { ?>
                                                                            <img height="63" width="63" src="images/avatar.png" border="0" class="img-circle profile_img" />
                                                                            <?php } ?>
                                
                            </div>
                            <div class="profile_info">
                                <span>Welcome,</span>
                                <h2><?=$_SESSION['frnd_admin']?></h2>
                            </div>
                        </div>
                        <!-- /menu profile quick info -->
                        <div class="clearfix"></div>
                        <!-- sidebar menu -->
                        <?php include("sidebar.php"); ?>
                        <!-- /sidebar menu -->
                    </div>
                </div>
                
                <!-- top navigation -->
                
                <?php include("top.php"); ?>
                <!-- /top navigation -->
                
                <!-- page content -->
                
                <?php include("body.php"); ?>
                <!-- /page content -->
                
                <!-- footer content -->
                <?php include("footer.php"); ?>
<?php

/*
        * @package    ACCIT
         * @author    Paperlink Softwares Team
         * @copyright    Copyright (c) 2016 , Paperlink Softwares Pvt. Ltd.  (http://www.paperlinksoftwares.com/)
         * @since    Version 1.0.0
         * @filesource
		 * Code for Logout
         */
session_start();
error_reporting(E_ALL ^ (E_NOTICE | E_WARNING | E_DEPRECATED));
date_default_timezone_set('Australia/sydney');
$date = date('d/m/Y h:i:s A', time());
$offline="0";
mysql_query("UPDATE `users` SET `LAST_LOGON`='".$date."',`STATUS`='".$offline."' WHERE `USER_NO`=".$_SESSION['user_no']);

echo "<script>window.location.assign('index.php')</script>";
$_SESSION['msg']="logout";
session_destroy();
?>


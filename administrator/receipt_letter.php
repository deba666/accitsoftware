<?php
include ("settings.php");
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Receipt</title>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    </head>
    <body style="margin: 96px 96px 96px 96px;background-image: url(./bg.png);background-repeat: no-repeat;
    background-position: 70% 0%;background-size: cover;    margin-bottom: 20px;">
        <table style="width:70%;border-collapse:collapse;margin-left:auto;margin-right:auto;">
            <colgroup>
                <col style="width:236px" />
                <col style="width:132px" />
                <col style="width:132px" />
                <col style="width:160px" />
            </colgroup>
            <tr>
                <td style="border-right-color:#E87D37;border-right-style:solid;border-right-width:0.75pt;padding-left:5.4pt;padding-right:5.4pt;width:177.2pt;vertical-align:top;">
                    <p style="text-align:center;margin-left:0pt;margin-right:0pt;margin-top:0pt;margin-bottom:0pt;"><span>&#xa0;</span></p>
                </td>
                <td style="border-left-color:#E87D37;border-left-style:solid;border-left-width:0.75pt;border-right-color:#E87D37;border-right-style:solid;border-right-width:0.75pt;padding-left:5.4pt;padding-right:5.4pt;width:99.2pt;vertical-align:top;">
                    <p style="text-align:center;margin-left:0pt;margin-right:0pt;margin-top:0pt;margin-bottom:0pt;"><span>&#xa0;</span></p>
                </td>
                <td style="border-left-color:#E87D37;border-left-style:solid;border-left-width:0.75pt;border-right-color:#E87D37;border-right-style:solid;border-right-width:0.75pt;padding-left:5.4pt;padding-right:5.4pt;width:99.25pt;vertical-align:top;">
                    <p style="margin-left:0pt;margin-right:0pt;margin-top:0pt;margin-bottom:0pt;"><span>&#xa0;</span></p>
                </td>
                <td style="border-left-color:#E87D37;border-left-style:solid;border-left-width:0.75pt;padding-left:5.4pt;padding-right:5.4pt;width:120.5pt;vertical-align:top;">
                    <p style="margin-left:0pt;margin-right:0pt;margin-top:0pt;margin-bottom:0pt;"><span>&#xa0;</span></p>
                </td>
            </tr>
            <tr>
                <td rowspan="2" style="border-right-color:#E87D37;border-right-style:solid;border-right-width:0.75pt;padding-left:5.4pt;padding-right:5.4pt;width:177.2pt;vertical-align:top;">
                <p style="margin-left:0pt;margin-right:0pt;margin-top:0pt;margin-bottom:0pt;padding: 1pt 4pt;"><span style="margin-left: 0;position: absolute;margin-top: 0px;z-index: 9999;
                   left: 90px;top: 90px;"><img src="logo.png" width="100" /></span></p>
                    <p style="text-align:center;margin-left:0pt;margin-right:0pt;margin-top:0pt;margin-bottom:0pt;"><span style="font-family:Arial;font-size:8pt;font-weight: bold;">AUSTRALIAN COLLEGE OF COMMERCE AND INFORMATION TECHNOLOGY</span></p>
                </td>
                <td style="border-left-color:#E87D37;border-left-style:solid;border-left-width:0.75pt;border-right-color:#E87D37;border-right-style:solid;border-right-width:0.75pt;padding-left:5.4pt;padding-right:5.4pt;width:99.2pt;vertical-align:top;">
                    <p style="text-align:center;margin-left:0pt;margin-right:0pt;margin-top:0pt;margin-bottom:0pt;"><span style="font-family:Arial;font-size:8pt;">495 Kent St, Sydney</span></p>
                </td>
                <td style="border-left-color:#E87D37;border-left-style:solid;border-left-width:0.75pt;border-right-color:#E87D37;border-right-style:solid;border-right-width:0.75pt;padding-left:5.4pt;padding-right:5.4pt;width:99.25pt;vertical-align:top;">
                    <p style="margin-left:0pt;margin-right:0pt;margin-top:0pt;margin-bottom:0pt;"><span style="font-family:Arial;font-size:8pt;">Phone: +61 2 92613009 </span></p>
                </td>
                <td style="border-left-color:#E87D37;border-left-style:solid;border-left-width:0.75pt;padding-left:5.4pt;padding-right:5.4pt;width:120.5pt;vertical-align:top;">
                    <p style="margin-left:0pt;margin-right:0pt;margin-top:0pt;margin-bottom:0pt;"><span style="font-family:Arial;font-size:8pt;">E-mail: info@accit.nsw.edu.au</span></p>
                </td>
            </tr>
            <tr>
                <td style="border-right-color:#E87D37;border-right-style:solid;border-right-width:0.75pt;padding-left:5.4pt;padding-right:5.4pt;width:99.2pt;vertical-align:top;">
                    <p style="text-align:center;margin-left:0pt;margin-right:0pt;margin-top:0pt;margin-bottom:0pt;"><span style="font-family:Arial;font-size:8pt;">NSW, 2000</span></p>
                </td>
                <td style="border-left-color:#E87D37;border-left-style:solid;border-left-width:0.75pt;border-right-color:#E87D37;border-right-style:solid;border-right-width:0.75pt;padding-left:5.4pt;padding-right:5.4pt;width:99.25pt;vertical-align:top;">
                    <p style="margin-left:0pt;margin-right:0pt;margin-top:0pt;margin-bottom:0pt;"><span style="font-family:Arial;font-size:8pt;">Fax &nbsp; &nbsp;: +61 2 89716938</span></p>
                </td>
                <td style="border-left-color:#E87D37;border-left-style:solid;border-left-width:0.75pt;padding-left:5.4pt;padding-right:5.4pt;width:120.5pt;vertical-align:top;">
                    <p style="margin-left:0pt;margin-right:0pt;margin-top:0pt;margin-bottom:0pt;"><span style="font-family:Arial;font-size:8pt;">www.accit.nsw.edu.au</span></p>
                </td>
            </tr>
            <tr>
                <td style="border-right-color:#E87D37;border-right-style:solid;border-right-width:0.75pt;padding-left:5.4pt;padding-right:5.4pt;width:177.2pt;">
                    <p style="margin-left:0pt;margin-right:0pt;line-height:115%;font-size:8pt;margin-top:0pt;margin-bottom:0pt;"><span>&#xa0;</span></p>
                </td>
                <td style="border-left-color:#E87D37;border-left-style:solid;border-left-width:0.75pt;border-right-color:#E87D37;border-right-style:solid;border-right-width:0.75pt;padding-left:5.4pt;padding-right:5.4pt;width:99.2pt;vertical-align:top;">
                    <p style="text-align:center;margin-left:0pt;margin-right:0pt;margin-top:0pt;margin-bottom:0pt;"><span>&#xa0;</span></p>
                </td>
                <td style="border-left-color:#E87D37;border-left-style:solid;border-left-width:0.75pt;border-right-color:#E87D37;border-right-style:solid;border-right-width:0.75pt;padding-left:5.4pt;padding-right:5.4pt;width:99.25pt;vertical-align:top;">
                    <p style="margin-left:0pt;margin-right:0pt;margin-top:0pt;margin-bottom:0pt;"><span>&#xa0;</span></p>
                </td>
                <td style="border-left-color:#E87D37;border-left-style:solid;border-left-width:0.75pt;padding-left:5.4pt;padding-right:5.4pt;width:120.5pt;vertical-align:top;">
                    <p style="margin-left:0pt;margin-right:0pt;margin-top:0pt;margin-bottom:0pt;"><span>&#xa0;</span></p>
                </td>
            </tr>
        </table>
        <div>
            <p style="text-align:center;margin-left:0pt;margin-right:0pt;margin-top:0pt;margin-bottom:0pt;padding: 1pt 4pt;"><span style="font-family:Cambria;font-size:1pt;font-weight: bold;color:#FFFFFF;">a000716 Franck BOSSI Reciept No 4720</span></p>
            <p style="margin-left:0pt;margin-right:0pt;line-height:115%;font-size:11pt;margin-top:0pt;margin-bottom:0pt;padding: 1pt 4pt;"><span>&#xa0;</span></p>
            <p style="text-align:center;margin-left:0pt;margin-right:0pt;margin-top:0pt;margin-bottom:0pt;padding: 1pt 4pt;"><span style="font-family:Century Gothic;font-size:28pt;letter-spacing:-0.5pt;">Receipt</span></p>
            <p style="margin-left:0pt;margin-right:0pt;margin-top:0pt;margin-bottom:0pt;padding: 1pt 4pt;"><span>&#xa0;</span></p>
            <p style="text-align:right;margin-left:0pt;margin-right:0pt;margin-top:0pt;margin-bottom:0pt;padding: 1pt 4pt;"><span style="font-family:Calibri;font-size:11pt;">Date: <?=date('d/m/Y',strtotime($_GET['receipt_date']))?></span></p>
            <p style="text-align:right;margin-left:0pt;margin-right:0pt;margin-top:0pt;margin-bottom:0pt;padding: 1pt 4pt;"><span style="font-family:Calibri;font-size:11pt;">Receipt No: <?=$_GET['receipt_no']?></span></p>
            <p style="margin-left:0pt;margin-right:0pt;margin-top:12pt;margin-bottom:0pt;padding: 1pt 4pt;"><span style="font-family:Calibri;font-size:11pt;color: red;"><?=$_GET['lname']?> <?=$_GET['fname']?></span></p>
            <p style="margin-left:0pt;margin-right:0pt;margin-top:0pt;margin-bottom:0pt;padding: 1pt 4pt;"><span style="font-family:Calibri;font-size:11pt;"><?=$_GET['laddr1']?>, &nbsp;</span></p>
            <p style="margin-left:0pt;margin-right:0pt;margin-top:0pt;margin-bottom:0pt;padding: 1pt 4pt;"><span style="font-family:Calibri;font-size:11pt;"><?=$_GET['lsub']?>, <?=$_GET['lstate']?> <?=$_GET['lpcode']?></span></p>
            <p style="margin-left:0pt;margin-right:0pt;line-height:115%;font-size:11pt;margin-top:0pt;margin-bottom:0pt;padding: 1pt 4pt;"><span>&#xa0;</span></p>
            <p style="margin-left:0pt;margin-right:0pt;margin-top:0pt;margin-bottom:0pt;padding: 1pt 4pt;"><span style="font-family:Calibri;font-size:11pt;font-weight: bold;">Student No:</span><span>&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0; </span><span style="font-family:Calibri;font-size:11pt;"><?=$_GET['stud_no']?></span><span style="font-family:Calibri;font-size:11pt;font-weight: bold;"> </span></p>
            <p style="margin-left:0pt;margin-right:0pt;margin-top:0pt;margin-bottom:0pt;padding: 1pt 4pt;"><span style="font-family:Calibri;font-size:11pt;font-weight: bold;">Course:</span><span>&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0; </span>
            <span>&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;</span><span style="font-family:Calibri;font-size:11pt;"><?=$_GET['course_name']?></span></p>
            <p style="margin-left:0pt;margin-right:0pt;margin-top:0pt;margin-bottom:0pt;padding: 1pt 4pt;" />
            <table style="width:100%;border-collapse:collapse;table-layout:fixed;">
                <colgroup>
                    <col style="width:188px" />
                    <col style="width:264px" />
                    <col style="width:147px" />
                </colgroup>
                <tr>
                    <td style="border-top-color:#167AF3;border-top-style:solid;border-top-width:0.75pt;border-bottom-color:#167AF3;border-bottom-style:solid;border-bottom-width:1.5pt;border-left-color:#167AF3;border-left-style:solid;border-left-width:0.75pt;border-right-color:#167AF3;border-right-style:solid;border-right-width:0.75pt;padding-left:5.4pt;padding-right:5.4pt;width:141.5pt;vertical-align:top;">
                        <p style="margin-left:0pt;margin-right:0pt;margin-top:3pt;margin-bottom:0pt;"><span style="font-family:Calibri;font-size:11pt;color:#032348;">INVOICE NO</span></p>
                    </td>
                    <td style="border-top-color:#167AF3;border-top-style:solid;border-top-width:0.75pt;border-bottom-color:#167AF3;border-bottom-style:solid;border-bottom-width:1.5pt;border-left-color:#167AF3;border-left-style:solid;border-left-width:0.75pt;border-right-color:#167AF3;border-right-style:solid;border-right-width:0.75pt;padding-left:5.4pt;padding-right:5.4pt;width:198.45pt;vertical-align:top;">
                        <p style="margin-left:0pt;margin-right:0pt;margin-top:3pt;margin-bottom:0pt;"><span style="font-family:Calibri;font-size:11pt;color:#032348;">FEE NAME</span></p>
                    </td>
                    <td style="border-top-color:#167AF3;border-top-style:solid;border-top-width:0.75pt;border-bottom-color:#167AF3;border-bottom-style:solid;border-bottom-width:1.5pt;border-left-color:#167AF3;border-left-style:solid;border-left-width:0.75pt;border-right-color:#167AF3;border-right-style:solid;border-right-width:0.75pt;padding-left:5.4pt;padding-right:5.4pt;width:110.9pt;vertical-align:top;">
                        <p style="margin-left:0pt;margin-right:0pt;margin-top:3pt;margin-bottom:0pt;"><span style="font-family:Calibri;font-size:11pt;color:#032348;">AMOUNT</span></p>
                    </td>
                </tr>
				<?php
				$cond_cr="where `RECEIPT_NO`=".$_GET['receipt_no'];
				$cr_data=getRows($cond_cr,'credit');
				foreach($cr_data as $cr_item)
				{
					 ?>
                <tr>
                    <td style="border-top-color:#167AF3;border-top-style:solid;border-top-width:0.75pt;border-bottom-color:#167AF3;border-bottom-style:solid;border-bottom-width:0.75pt;border-left-color:#167AF3;border-left-style:solid;border-left-width:0.75pt;border-right-color:#167AF3;border-right-style:solid;border-right-width:0.75pt;padding-left:5.4pt;padding-right:5.4pt;width:141.5pt;background-color:#B1D2FB;vertical-align:top;">
                        <p style="margin-left:0pt;margin-right:0pt;margin-top:3pt;margin-bottom:0pt;"><span style="font-family:Calibri;font-size:11pt;color:#032348;"><?=$cr_item['INVOICE_NO']?></span></p>
                    </td>
					<?php
						if($cr_item['IND_NO'])
						{
						$cond_inv_det="where `IND_NO`=".$cr_item['IND_NO'];
						$inv_data=getRows($cond_inv_det,'invoice_details');
						}
						if($inv_data[0]['OFFER_INSTALMENT_NO'])
						{
						$cond_off_ins="where `OFFER_INSTALMENT_NO`=".$inv_data[0]['OFFER_INSTALMENT_NO'];
						$ins_data=getRows($cond_off_ins,'offer_instalment');
						}
						if($ins_data[0]['OFFER_ITEM_NO'])
						{
						$cond_off_itm="where `OFFER_ITEM_NO`=".$ins_data[0]['OFFER_ITEM_NO'];
						$itm_data=getRows($cond_off_itm,'offer_item');
						}
						if($itm_data[0]['PRODUCT_NO'])
						{
						$cond_pro_itm="where `PRODUCT_NO`=".$itm_data[0]['PRODUCT_NO'];
						$pro_data=getRows($cond_pro_itm,'product');
						}
						if($inv_data[0]['FEE_NO']==76)
						{
						$name="Agent Fee";
						}
						if($inv_data[0]['FEE_NO']==76)
						{
						$name="Agent Fee";
						}
						else
						{
						$name=$pro_data[0]['NAME'];
						}
						?>
                    <td style="border-top-color:#167AF3;border-top-style:solid;border-top-width:0.75pt;border-bottom-color:#167AF3;border-bottom-style:solid;border-bottom-width:0.75pt;border-left-color:#167AF3;border-left-style:solid;border-left-width:0.75pt;border-right-color:#167AF3;border-right-style:solid;border-right-width:0.75pt;padding-left:5.4pt;padding-right:5.4pt;width:198.45pt;background-color:#B1D2FB;vertical-align:top;">
                        <p style="margin-left:0pt;margin-right:0pt;margin-top:3pt;margin-bottom:0pt;"><span style="font-family:Calibri;font-size:11pt;color:#032348;"><?= $name?></span></p>
                    </td>
                    <td style="border-top-color:#167AF3;border-top-style:solid;border-top-width:0.75pt;border-bottom-color:#167AF3;border-bottom-style:solid;border-bottom-width:0.75pt;border-left-color:#167AF3;border-left-style:solid;border-left-width:0.75pt;border-right-color:#167AF3;border-right-style:solid;border-right-width:0.75pt;padding-left:5.4pt;padding-right:5.4pt;width:110.9pt;background-color:#B1D2FB;vertical-align:top;">
                        <p style="margin-left:0pt;margin-right:0pt;margin-top:3pt;margin-bottom:0pt;"><span style="font-family:Calibri;font-size:11pt;color:#032348;">$<?=$cr_item['AMOUNT']?></span></p>
                    </td>
                </tr>
				<?php } ?>
            </table>
            <p style="margin-left:0pt;margin-right:0pt;line-height:115%;font-size:11pt;margin-top:0pt;margin-bottom:10pt;padding: 1pt 4pt;"><span>&#xa0;</span></p>
            <p style="margin-left:0pt;margin-right:0pt;line-height:115%;font-size:13pt;margin-top:2pt;margin-bottom:0pt;padding: 1pt 4pt;page-break-after:avoid;mso-pagination:lines-together;"><span style="font-family:Century Gothic;font-size:13pt;color:#032348;">Payment breakdown</span></p>
            <table style="width:100%;border-collapse:collapse;table-layout:fixed;">
                <colgroup>
                    <col style="width:188px" />
                    <col style="width:264px" />
                    <col style="width:147px" />
                </colgroup>
                <tr>
                    <td style="border-top-color:#167AF3;border-top-style:solid;border-top-width:0.75pt;border-bottom-color:#167AF3;border-bottom-style:solid;border-bottom-width:1.5pt;border-left-color:#167AF3;border-left-style:solid;border-left-width:0.75pt;border-right-color:#167AF3;border-right-style:solid;border-right-width:0.75pt;padding-left:5.4pt;padding-right:5.4pt;width:141.5pt;vertical-align:top;">
                        <p style="margin-left:0pt;margin-right:0pt;margin-top:3pt;margin-bottom:0pt;"><span style="font-family:Calibri;font-size:11pt;color:#032348;">PAYMENT TYPE</span></p>
                    </td>
                    <td style="border-top-color:#167AF3;border-top-style:solid;border-top-width:0.75pt;border-bottom-color:#167AF3;border-bottom-style:solid;border-bottom-width:1.5pt;border-left-color:#167AF3;border-left-style:solid;border-left-width:0.75pt;border-right-color:#167AF3;border-right-style:solid;border-right-width:0.75pt;padding-left:5.4pt;padding-right:5.4pt;width:198.45pt;vertical-align:top;">
                        <p style="margin-left:0pt;margin-right:0pt;margin-top:3pt;margin-bottom:0pt;"><span style="font-family:Calibri;font-size:11pt;color:#032348;">DETAILS</span></p>
                    </td>
                    <td style="border-top-color:#167AF3;border-top-style:solid;border-top-width:0.75pt;border-bottom-color:#167AF3;border-bottom-style:solid;border-bottom-width:1.5pt;border-left-color:#167AF3;border-left-style:solid;border-left-width:0.75pt;border-right-color:#167AF3;border-right-style:solid;border-right-width:0.75pt;padding-left:5.4pt;padding-right:5.4pt;width:110.9pt;vertical-align:top;">
                        <p style="margin-left:0pt;margin-right:0pt;margin-top:3pt;margin-bottom:0pt;"><span style="font-family:Calibri;font-size:11pt;color:#032348;">AMOUNT</span></p>
                    </td>
                </tr>
                <tr>
				<?php
				$cond_rec="where `RECEIPT_NO`=".$_GET['receipt_no'];
				$rec_data=getRows($cond_rec,'receipt_item');
				if($rec_data[0]['PAY_TYPE_NO'])
				{
				$cond_pay_type="where `DEBIT_TYPE_NO`=".$rec_data[0]['PAY_TYPE_NO'];
				$pay_type_data=getRows($cond_pay_type,'debit_type');
				}
					 ?>
                    <td style="border-top-color:#167AF3;border-top-style:solid;border-top-width:0.75pt;border-bottom-color:#167AF3;border-bottom-style:solid;border-bottom-width:0.75pt;border-left-color:#167AF3;border-left-style:solid;border-left-width:0.75pt;border-right-color:#167AF3;border-right-style:solid;border-right-width:0.75pt;padding-left:5.4pt;padding-right:5.4pt;width:141.5pt;background-color:#B1D2FB;vertical-align:top;">
                        <p style="margin-left:0pt;margin-right:0pt;margin-top:3pt;margin-bottom:0pt;"><span style="font-family:Calibri;font-size:11pt;color:#032348;"><?=$pay_type_data[0]['DEBIT_TYPE_NAME']?></span></p>
                    </td>
                    <td style="border-top-color:#167AF3;border-top-style:solid;border-top-width:0.75pt;border-bottom-color:#167AF3;border-bottom-style:solid;border-bottom-width:0.75pt;border-left-color:#167AF3;border-left-style:solid;border-left-width:0.75pt;border-right-color:#167AF3;border-right-style:solid;border-right-width:0.75pt;padding-left:5.4pt;padding-right:5.4pt;width:198.45pt;background-color:#B1D2FB;vertical-align:top;">
                        <p style="margin-left:0pt;margin-right:0pt;margin-top:3pt;margin-bottom:0pt;"><span><?=$rec_data[0]['DETAIL']?></span></p>
                    </td>
                    <td style="border-top-color:#167AF3;border-top-style:solid;border-top-width:0.75pt;border-bottom-color:#167AF3;border-bottom-style:solid;border-bottom-width:0.75pt;border-left-color:#167AF3;border-left-style:solid;border-left-width:0.75pt;border-right-color:#167AF3;border-right-style:solid;border-right-width:0.75pt;padding-left:5.4pt;padding-right:5.4pt;width:110.9pt;background-color:#B1D2FB;vertical-align:top;">
                        <p style="margin-left:0pt;margin-right:0pt;margin-top:3pt;margin-bottom:0pt;"><span style="font-family:Calibri;font-size:11pt;color:#032348;">$<?=$_GET['receipt_amount']?></span></p>
                    </td>
                </tr>
            </table>
            <p style="margin-left:0pt;margin-right:0pt;margin-top:0pt;margin-bottom:0pt;padding: 1pt 4pt;"><span>&#xa0;</span></p>
            <table style="border-collapse:collapse;margin-left:auto;table-layout:fixed;">
                <colgroup>
                    <col style="width:147px" span="2" />
                </colgroup>
                <tr style="height:21.6pt;">
                    <td style="border-top-color:#167AF3;border-top-style:double;border-top-width:0.75pt;border-bottom-color:#167AF3;border-bottom-style:solid;border-bottom-width:0.75pt;border-left-color:#167AF3;border-left-style:solid;border-left-width:0.75pt;border-right-color:#167AF3;border-right-style:solid;border-right-width:0.75pt;padding-left:5.4pt;padding-right:5.4pt;width:110.85pt;vertical-align:top;">
                        <p style="margin-left:0pt;margin-right:0pt;margin-top:3pt;margin-bottom:0pt;"><span style="font-family:Cambria;font-size:10pt;font-weight: bold;color:#032348;">Total Amount</span><span style="font-family:Cambria;font-size:10pt;font-weight: bold;color:#032348;text-transform: uppercase;">:</span></p>
                    </td>
                    <td style="border-top-color:#167AF3;border-top-style:double;border-top-width:0.75pt;border-bottom-color:#167AF3;border-bottom-style:solid;border-bottom-width:0.75pt;border-left-color:#167AF3;border-left-style:solid;border-left-width:0.75pt;border-right-color:#167AF3;border-right-style:solid;border-right-width:0.75pt;padding-left:5.4pt;padding-right:5.4pt;width:110.85pt;vertical-align:top;">
                        <p style="margin-left:0pt;margin-right:0pt;margin-top:3pt;margin-bottom:0pt;"><span style="font-family:Cambria;font-size:10pt;font-weight: bold;color:#032348;">$<?=$_GET['receipt_amount']?></span></p>
                    </td>
                </tr>
            </table>
            <p style="margin-left:0pt;margin-right:0pt;margin-top:0pt;margin-bottom:0pt;padding: 1pt 4pt;"><span>&#xa0;</span></p>
            <p style="margin-left:0pt;margin-right:0pt;margin-top:0pt;margin-bottom:0pt;padding: 1pt 4pt;"><span>&#xa0;</span></p>
            <p style="margin-left:0pt;margin-right:0pt;margin-top:0pt;margin-bottom:0pt;padding: 1pt 4pt;"><span>&#xa0;</span></p>
        </div>
        <p style="margin-left:0pt;margin-right:0pt;margin-top:0pt;margin-bottom:0pt;padding: 1pt 4pt;"><span>&#xa0;</span></p>
        <p style="margin-left:0pt;margin-right:0pt;margin-top:0pt;margin-bottom:0pt;padding: 1pt 4pt;"><span>&#xa0;</span></p>
        <p style="margin-left:0pt;margin-right:0pt;margin-top:0pt;margin-bottom:0pt;padding: 1pt 4pt;"><span>&#xa0;</span></p>
        <p style="margin-left:0pt;margin-right:0pt;margin-top:0pt;margin-bottom:0pt;padding: 1pt 4pt;"><span>&#xa0;</span></p>
        <p style="margin-left:0pt;margin-right:0pt;margin-top:0pt;margin-bottom:0pt;padding: 1pt 4pt;"><span>&#xa0;</span></p>
        <p style="margin-left:0pt;margin-right:0pt;margin-top:0pt;margin-bottom:0pt;padding: 1pt 4pt;"><span>&#xa0;</span></p>

        <div>
            <p style="margin-left:0pt;margin-right:0pt;line-height:115%;font-size:13pt;margin-top:2pt;margin-bottom:0pt;page-break-after:avoid;mso-pagination:lines-together;"><span style="font-family:Century Gothic;font-size:13pt;color:#032348;">Official Use</span></p>
            <p style="float: left;">
                <span style="font-family:Calibri;font-size:11pt;display: inline-block;">Signature</span>
                <span style="font-family:Calibri;font-size:11pt;display: inline-block; width: 200px; border-bottom: 1px solid #000;padding-left: 20px;">scbanerjee</span>
            </p>
            <p style="float: right;">
                <span style="font-family:Calibri;font-size:11pt;display: inline-block;">Staff Name</span>
                <span style="font-family:Calibri;font-size:11pt;display: inline-block; width: 200px; border-bottom: 1px solid #000;padding-left: 20px;">jeeva</span>
            </p>
        </div>
        <p style="margin-left:0pt;margin-right:0pt;margin-top:0pt;margin-bottom:0pt;padding: 1pt 4pt;"><span>&#xa0;</span></p>
        <p style="margin-left:0pt;margin-right:0pt;margin-top:0pt;margin-bottom:0pt;padding: 1pt 4pt;"><span>&#xa0;</span></p>
        <p style="margin-left:0pt;margin-right:0pt;margin-top:0pt;margin-bottom:0pt;padding: 1pt 4pt;"><span>&#xa0;</span></p>

        <table style="border-collapse:collapse;margin-left:auto;margin-right:auto;width: 100%;">
            <colgroup>
                <col style="width:197px" />
                <col style="width:201px" />
                <col style="width:135px" />
                <col style="width:67px" />
            </colgroup>
            <tr>
                <td style="border-top-color:#000000;border-top-style:solid;border-top-width:0.75pt;padding-left:5.4pt;padding-right:5.4pt;width:161.3pt;">
                    <p style="margin-left:0pt;margin-right:0pt;margin-top:0pt;margin-bottom:0pt;"><span style="font-family:Arial;font-size:7pt;font-weight: bold;">ABN:</span><span style="font-family:Arial;font-size:7pt;"> 80 126 576 896</span></p>
                </td>
                <td style="border-top-color:#000000;border-top-style:solid;border-top-width:0.75pt;padding-left:5.4pt;padding-right:5.4pt;width:160.15pt;">
                    <p style="margin-left:0pt;margin-right:0pt;margin-top:0pt;margin-bottom:0pt;text-align: center;"><span style="font-family:Arial;font-size:7pt;font-weight: bold;">National Provider &nbsp;Code :</span><span style="font-family:Arial;font-size:7pt;"> 91412</span></p>
                </td>
                <td colspan="2" style="border-top-color:#000000;border-top-style:solid;border-top-width:0.75pt;padding-left:5.4pt;padding-right:5.4pt;width:160.55pt;">
                    <p style="margin-left:0pt;margin-right:0pt;margin-top:0pt;margin-bottom:0pt;text-align: right;"><span style="font-family:Arial;font-size:7pt;font-weight: bold;">CRICOS Code:</span><span style="font-family:Arial;font-size:7pt;"> 02978C</span></p>
                </td>
            </tr>
            <tr style="height:8.25pt;">
                <td style="padding-left:5.4pt;padding-right:5.4pt;width:161.3pt;">
                    <p style="margin-left:0pt;margin-right:0pt;margin-top:0pt;margin-bottom:0pt;"><span style="font-family:Arial;font-size:7pt;">Version: 1.2</span></p>
                </td>
                <td style="padding-left:5.4pt;padding-right:5.4pt;width:160.15pt;">
                    <p style="text-align:center;margin-left:0pt;margin-right:0pt;margin-top:0pt;margin-bottom:0pt;"><span style="font-family:Arial;font-size:7pt;font-weight: bold;">Doc ID:</span><span style="font-family:Arial;font-size:7pt;font-weight: bold;">ACCIT01-1-95</span></p>
                </td>
                <td colspan="2" style="padding-left:5.4pt;padding-right:5.4pt;width:160.55pt;">
                    <p style="margin-left:0pt;margin-right:0pt;margin-top:0pt;margin-bottom:0pt;" />
                </td>
            </tr>
            <tr>
                <td colspan="3" style="padding-left:5.4pt;padding-right:5.4pt;width:430.65pt;">
                    <p style="margin-left:0pt;margin-right:0pt;margin-top:0pt;margin-bottom:0pt;"><span>&#xa0;</span></p>
                </td>
                <td style="padding-left:5.4pt;padding-right:5.4pt;width:51.35pt;">
                    <p style="margin-left:0pt;margin-right:0pt;margin-top:0pt;margin-bottom:0pt;text-align: right;"><span style="font-family:Arial;font-size:7pt;">Page </span><span style="font-family:Arial;font-size:7pt;"> of </span></p>
                </td>
            </tr>
        </table>
    </body>
</html>

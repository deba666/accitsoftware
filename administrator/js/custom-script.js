/*==========================
TOUCHY SCROLL FOR SIDEBAR
==========================*/
$("#sidebar").niceScroll({
    cursorcolor: "#2f2e2e",
    cursoropacitymax: 0.7,
    boxzoom: false,
    touchbehavior: true
});


$(function () {
	
	
	

	
	// button state demo
    $('#fat-btn')
      .click(function () {
        var btn = $(this)
        btn.button('loading')
        setTimeout(function () {
          btn.button('reset')
        }, 3000)
     })
	  

	
	/*===================
	LIST-ACCORDION
	===================*/	  

	$('#list-accordion').accordion({
		header: ".title",
		autoheight: false
	});
	




/*==============================
	  NOTY TOP
	================================*/
	
	$('.alert_t').click(function() {
		
		var noty_id = noty({
			layout : 'top',
			text: 'noty - a jquery notification library!',
			modal : true,
			type:'alert',
			
			 });
		  });

	$('.error_t').click(function() {
		
		var noty_id = noty({
			layout : 'top',
			text: 'noty - a jquery notification library!',
			modal : true,
			type : 'error', 
			 });
		  });
		  
	$('.success_t').click(function() {
		
		var noty_id = noty({
			layout : 'top',
			text: 'noty - a jquery notification library!',
			modal : true,
			type : 'success', 
			 });
		  });
		  
	$('.info_t').click(function() {
		
		var noty_id = noty({
			layout : 'top',
			text: 'noty - a jquery notification library!',
			modal : true,
			type : 'information', 
			 });
		  });
	
	$('.confirm_t').click(function() {
		
		var noty_id = noty({
			layout : 'top',
			text: 'noty - a jquery notification library!',
			modal : true,
			buttons: [
				{type: 'btn btn-success', text: 'Ok', click: function($noty) {
		  
					// this = button element
					// $noty = $noty element
		  
					$noty.close();
					noty({force: true, text: 'You clicked "Ok" button', type: 'success'});
				  }
				},
				{type: 'button btn btn-warning', text: 'Cancel', click: function($noty) {
					$noty.close();
					noty({force: true, text: 'You clicked "Cancel" button', type: 'error'});
				  }
				}
				],
			 type : 'success', 
			 });
		 
});


$(function () {

    // Smart Wizard 	
    $('#horizontal-wizard').smartWizard({
        enableFinishButton: false,
        onFinish: onFinishCallback
    });

    function onFinishCallback() {
        $('#horizontal-wizard').smartWizard('showMessage', 'Finish Clicked');
        //alert('Finish Clicked');
    }
    $('#vertical-wizard').smartWizard();



});


/*======================
	DATA TABLE
========================*/
$(function () {
    $('.data-tbl-simple').dataTable({
        "sPaginationType": "full_numbers",
        "iDisplayLength": 10,
        "oLanguage": {
            "sLengthMenu": "<span class='lenghtMenu'> _MENU_</span><span class='lengthLabel'>Entries per page:</span>",
        },
        "sDom": '<"table_top clearfix"fl<"clear">>,<"table_content"t>,<"table_bottom"p<"clear">>'

    });
    $("div.table_top select").addClass('tbl_length');
/*$(".tbl_length").chosen({
		disable_search_threshold: 4	
	});
		*/

});

$(function () {
    $('.data-tbl-inbox').dataTable({
			 "aoColumnDefs": [
						{ "bSortable": false, "aTargets": [ 0,1 ] }
					],
					"aaSorting": [[1, 'asc']],
        "sPaginationType": "full_numbers",
        "iDisplayLength": 10,
        "oLanguage": {
            "sLengthMenu": "<span class='lenghtMenu'> _MENU_</span><span class='lengthLabel'>Message per page:</span>",
        },
        "sDom": '<"table_top clearfix"fl<"clear">>,<"table_content"t>,<"table_bottom"p<"clear">>'

    });
    $("div.table_top select").addClass('tbl_length');
$(".tbl_length").chosen({
		disable_search_threshold: 4	
	});

});

$(function () {
    $('.data-tbl-striped').dataTable({
			 "aoColumnDefs": [
						{ "bSortable": false, "aTargets": [ 0,1,7 ] }
					],
					"aaSorting": [[1, 'asc']],
        "sPaginationType": "full_numbers",
        "iDisplayLength": 10,
        "oLanguage": {
            "sLengthMenu": "<span class='lenghtMenu'> _MENU_</span><span class='lengthLabel'>Message per page:</span>",
        },
        "sDom": '<"table_top clearfix"fl<"clear">>,<"table_content"t>,<"table_bottom"p<"clear">>'

    });
    $("div.table_top select").addClass('tbl_length');
$(".tbl_length").chosen({
		disable_search_threshold: 4	
	});

});

$(function () {
    $('.data-tbl-boxy').dataTable({
        "sPaginationType": "full_numbers",
        "iDisplayLength": 10,
        "oLanguage": {
            "sLengthMenu": "<span class='lenghtMenu'> _MENU_</span><span class='lengthLabel'>Entries per page:</span>",
        },
        "sDom": '<"tbl-searchbox clearfix"fl<"clear">>,<"table_content"t>,<"widget-bottom"p<"clear">>'

    });
    $("div.tbl-searchbox select").addClass('tbl_length');
/*$(".tbl_length").chosen({
		disable_search_threshold: 4	
	});
		*/

});



$(function () {

    $('.data-tbl-tools').dataTable({
        "sPaginationType": "full_numbers",
        "iDisplayLength": 10,
        "oLanguage": {
            "sLengthMenu": "<span class='lenghtMenu'> _MENU_</span><span class='lengthLabel'>Entries per page:</span>",
        },
        "sDom": '<"tbl-tools-searchbox"fl<"clear">>,<"tbl_tools"CT<"clear">>,<"table_content"t>,<"widget-bottom"p<"clear">>',

        "oTableTools": {
            "sSwfPath": "swf/copy_cvs_xls_pdf.swf"
        }
    });
    $("div.tbl-tools-searchbox select").addClass('tbl_length');
/* $(".tbl_length").chosen({
        disable_search_threshold: 4
    });*/
});






 $(function() {
                /* tells us if we dragged the box */
                var dragged = false;
				
                /* timeout for moving the mox when scrolling the window */
                var moveBoxTimeout;
				
                /* make the actionsBox draggable */
                $('#actionsBox').draggable({
                    start: function(event, ui) {
                        dragged = true;
                    },
                    stop: function(event, ui) {
                        var $actionsBox = $('#actionsBox');
                        /*
                        calculate the current distance from the window's top until the element
                        this value is going to be used further, to move the box after we scroll
                         */
                        $actionsBox.data('distanceTop',parseFloat($actionsBox.css('top'),10) - $(document).scrollTop());
                    }
                });
				
                /*
                when clicking on an input (checkbox),
                change the class of the table row,
                and show the actions box (if any checked)
                 */
                $('.data-tbl-inbox input[type="checkbox"]').bind('click',function(e) {
                    var $this = $(this);
                    if($this.is(':checked'))
                        $this.parents('tr:first').addClass('selected');
                    else
                        $this.parents('tr:first').removeClass('selected');
                    showActionsBox();
                });
				
                function showActionsBox(){
                    /* number of checked inputs */
                    var BoxesChecked = $('.data-tbl-inbox input:checked').length;
                    /* update the number of checked inputs */
                    $('#cntBoxMenu').html(BoxesChecked);
                    /*
                    if there is at least one selected, show the BoxActions Menu
                    otherwise hide it
                     */
                    var $actionsBox = $('#actionsBox');
                    if(BoxesChecked > 0){
                        /*
                        if we didn't drag, then the box stays where it is
                        we know that that position is the document current top
                        plus the previous distance that the box had relative to the window top (distanceTop)
                         */
                        if(!dragged)
                            $actionsBox.stop(true).animate({'top': parseInt(15 + $(document).scrollTop()) + 'px','opacity':'1'},500);
                        else
                            $actionsBox.stop(true).animate({'top': parseInt($(document).scrollTop() + $actionsBox.data('distanceTop')) + 'px','opacity':'1'},500);
                    }
                    else{
                        $actionsBox.stop(true).animate({'top': parseInt($(document).scrollTop() - 50) + 'px','opacity':'0'},500,function(){
                            $(this).css('left','50%');
                            dragged = false;
                            /* if the submenu was open we hide it again */
                            var $toggleBoxMenu = $('#toggleBoxMenu');
                            if($toggleBoxMenu.hasClass('closed')){
                                $toggleBoxMenu.click();
                            }
                        });
                    }
                }
				
                /*
                when scrolling, move the box to the right place
                 */
                $(window).scroll(function(){
                    clearTimeout(moveBoxTimeout);
                    moveBoxTimeout = setTimeout(showActionsBox,500);
                });
				
                /* open sub box menu for other actions */
                $('#toggleBoxMenu').toggle(
                function(e){
                    $(this).addClass('closed').removeClass('open');
                    $('#actionsBox .submenu').stop(true,true).slideDown();
                },
                function(e){
                    $(this).addClass('open').removeClass('closed');
                    $('#actionsBox .submenu').stop(true,true).slideUp();
                }
            );
				
                /*
                close the actions box menu:
                hides it, and then removes the element from the DOM,
                meaning that it will no longer appear
                 */
                $('#closeBoxMenu').bind('click',function(e){
                    $('#actionsBox').animate({'top':'-50px','opacity':'0'},1000,function(){
                        $(this).remove();
                    });
                });
				
                /*
                as an example, for all the actions (className:box_action)
                alert the values of the checked inputs
                 */
                $('#actionsBox .box_action').bind('click',function(e){
                    var ids = '';
                    $('.data-tbl-inbox input:checked').each(function(e,i){
                        var $this = $(this);
                        ids += 'id : ' + $this.attr('id') + ' , value : ' + $this.val() + '\n';
						
                    });
                    alert('checked inputs:\n'+ids);
                });
            });



			
/*===================================
THEME SWITCHER
=====================================*/

	$(function()
	{
		
		$('#sidebar-off').click(function()
		{
			$(this).attr('disabled','disabled');
			$(this).siblings('button').removeAttr('disabled');
			$('#sidebar').addClass('side-hide');
			$('.top-nav').addClass('full-fluid');
			$('#main-content').addClass('full-fluid');
			});
			
			$('#sidebar-on').click(function()
		{
			$(this).attr('disabled','disabled');
			$(this).siblings('button').removeAttr('disabled');
			$('#sidebar').removeClass('side-hide');
			$('.top-nav').removeClass('full-fluid');
			$('#main-content').removeClass('full-fluid');
			});
			
			$('#right-sidebar').click(function()
		{
			$(this).attr('disabled','disabled');
			$(this).siblings('button').removeAttr('disabled');
			$('#sidebar').addClass('right-sidebar');
			$('.top-nav').addClass('merge-left');
			$('#main-content').addClass('merge-left');
			
			});
			
			$('#left-sidebar').click(function()
		{
			$(this).attr('disabled','disabled');
			$(this).siblings('button').removeAttr('disabled');
			$('#sidebar').removeClass('right-sidebar');
			$('.top-nav').removeClass('merge-left');
			$('#main-content').removeClass('merge-left');
			
			});
		});
		
		$(function()
		{
			$('.theme-color').click(function()
			{
				var stylesheet = $(this).attr('title').toLowerCase();
				$('#themes').attr('href','css'+'/'+stylesheet+'.css');
				});
			});
	});
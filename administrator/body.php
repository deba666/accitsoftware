<?php
    /*
        * @package    ACCIT
         * @author    Paperlink Softwares Team
         * @copyright    Copyright (c) 2016 , Paperlink Softwares Pvt. Ltd.  (http://www.paperlinksoftwares.com/)
         * @since    Version 1.0.0
         * @filesource
		 * Code for assign all the pages in this section
         */
		
	$op=$_REQUEST['op'];
	
	switch ($op)
	{
	   case MD5('settings'): 		      
	   			include "pages/settings.php";
	   break;
	   case MD5('avetmiss_report'): 		      
	   			include "pages/avetmiss_report.php";
	   break;
	   case MD5('avetmiss_esoc_report'): 		      
	   			include "pages/avetmiss_esoc_report.php";
	   break;
	   case MD5('mail_send'):
				include "pages/mail_send.php";
	   break;
	   
	   case MD5('profile'): 		      
	   			include "pages/profile.php";
	   break;
	   case MD5('logout'): 		      
	   			include "logout.php";
	   break;
	   case MD5('change'): 		      
	   			include "pages/change.php";
	   break;
       case MD5('holidays'): 		      
	   			include "pages/holidays.php";
	   break;
	   case MD5('holidays_add'): 		      
	   			include "pages/holidays_add.php";
	   break;
	    case MD5('holidays_edit'): 		      
	   			include "pages/holidays_edit.php";
	   break;
	    case MD5('holi'): 		      
	   			include "pages/holi.php";
	   break;
	    /////////////////////////Configarations/////////////////////////////
	   
	   case MD5('config'): 		      
	   			include "config/config.php";
	   break;
	   case MD5('receipt_new'): 		      
	   			include "pages/receipt_new.php";
	   break;

	   case MD5('make_payment'): 		      
	   			include "pages/make_payment.php";
	   break;
	    case MD5('pay_amount'): 		      
	   			include "pages/pay_amount.php";
	   break;

/////////////////////////Extend Enrolment/////////////////////////////
	   
	   case MD5('changes_extend_enrolment'): 		      
	   			include "pages/changes_extend_enrolment.php";
	   break;
	   case MD5('changes_shorten'): 		      
	   			include "pages/changes_shorten.php";
	   break;

	   case MD5('changes_start_date'): 		      
	   			include "pages/changes_start_date.php";
	   break;
	    case MD5('changes_cancel_enrolment'): 		      
	   			include "pages/changes_cancel_enrolment.php";
	   break;
	    /////////////////////////Users/////////////////////////////
	   
	    case MD5('users'): 		      
	   			include "pages/users.php";
	   break;
	   case MD5('user_add'): 		      
	   			include "pages/user_add.php";
	   break;
	   case MD5('accordian'): 		      
	   			include "pages/accordian.php";
	   break;
	   	   
	   /////////////////////////student Page/////////////////////////////
	   
	    case MD5('student'):
				include "pages/student.php";
	   break;
       
            case MD5('display_student_offer'):
				include "pages/display_student_offer.php";
	   break;
	   
	   case MD5('student_add'):
				include "pages/student_add.php";
				
	   break;
	   
	    case MD5('student_details'):
				include "pages/student_details.php";
	   break;
	   
	   case MD5('student_edit'):
				include "pages/student_edit.php";
				
	   break;
	   case MD5('display_student_contact'):
				include "pages/display_student_contact.php";
	   break;
	   case MD5('display_student_contact_edit'):
				include "pages/display_student_contact_edit.php";
	   break;
	   case MD5('display_offer_agent'):
				include "pages/display_offer_agent.php";
	   break;
	   case MD5('display_offer_course'):
				include "pages/display_offer_course.php";
	   break;
	   case MD5('display_offer_course_edit'):
				include "pages/display_offer_course_edit.php";
	   break;
	   case MD5('display_new_course'):
				include "pages/display_new_course.php";
	   break;
	    case MD5('display_new_course_edit'):
				include "pages/display_new_course_edit.php";
	   break;
       
       case MD5('display_offer_agent_edit'):
				include "pages/display_offer_agent_edit.php";
        break;
	   case MD5('display_other_offer'):
				include "pages/display_other_offer.php";
	   break;
	   case MD5('display_other_offer_edit'):
				include "pages/display_other_offer_edit.php";
	   break;
	   break;
	   case MD5('display_other_offer_after_enrolment'):
				include "pages/display_other_offer_after_enrolment.php";
	   break;
	   case MD5('display_other_offer_after_enrolment_edit'):
				include "pages/display_other_offer_after_enrolment_edit.php";
	   break;
	   
	     /////////////////////////Offer Page/////////////////////////////
	    case MD5('offer'):
				include "pages/offer.php";
	   break;
	   case MD5('student_2'):
				include "pages/student_2.php";
	   break;
	   
	   case MD5('offer_all'):
				include "pages/offer_all.php";
	    break;

	   case MD5('offer_edit'):
				include "pages/offer_edit.php";
	    break;
		case MD5('offer_add'):
				include "pages/offer_add.php";
	    break;

	   case MD5('offer_details'):
				include "pages/offer_details.php";
	   break;
	   case MD5('split_offer'):
				include "pages/split_offer.php";
	   break;
	   case MD5('display_offer_others_items'):
				include "pages/display_offer_others_items.php";
	   break;
	   case MD5('display_offer_others_items_after_enrolment'):
				include "pages/display_offer_others_items_after_enrolment.php";
	   break;
	   case MD5('display_offer_others_items_edit'):
				include "pages/display_offer_others_items_edit.php";
	   break;
	   case MD5('display_new_insurance'):
				include "pages/display_new_insurance.php";
	   break;
	   case MD5('display_offer_insurance'):
				include "pages/display_offer_insurance.php";
	   break;
	   /////////////////////////Enrollment/////////////////////////////
	    case MD5('enroll_language'):
				include "pages/enroll_language.php";
	   break;
	   
	   case MD5('enroll_module'):
				include "pages/enroll_module.php";
	   break;
	   
	   case MD5('enroll_language_details'):
				include "pages/enroll_language_details.php";
	   break;
	   case MD5('enrolment_details'):
				include "pages/enrolment_details.php";
	   break;
	   case MD5('display_student_diary'):
				include "pages/display_student_diary.php";
	   break;
	   case MD5('display_leave'):
				include "pages/display_leave.php";
	   break;
	   case MD5('display_leave_edit'):
				include "pages/display_leave_edit.php";
	   break;
	   case MD5('display_student_absence'):
				include "pages/display_student_absence.php";
	   break;
	   case MD5('display_student_absence_edit'):
				include "pages/display_student_absence_edit.php";
	   break;
	   case MD5('enroll_debit'):
				include "pages/enroll_debit.php";
	   break;
	    case MD5('enroll_debit_transfer'):
				include "pages/enroll_debit_transfer.php";
	   break;
                case MD5('payment_offer_settings'):
				include "pages/payment_offer_settings.php";
                    break;
	   
	    case MD5('enroll_debit_transfer_other_page'):
				include "pages/enroll_debit_transfer_other_page.php";
	   break;
	   
	   case MD5('enroll_debit_transfer_other_enrolment'):
				include "pages/enroll_debit_transfer_other_enrolment.php";
	   break;
	    case MD5('enroll_debit_refund_student'):
				include "pages/enroll_debit_refund_student.php";
	   break;
	    case MD5('enroll_debit_refund_agent'):
				include "pages/enroll_debit_refund_agent.php";
	   break;
	   case MD5('enroll_debit2'):
				include "pages/enroll_debit2.php";
	   break;
	   case MD5('enroll_debit_operation'):
				include "pages/enroll_debit_operation.php";
	   break;
	   
	   case MD5('enroll_credit'):
				include "pages/enroll_credit.php";
	   break;
	   case MD5('enroll_credit_discount'):
				include "pages/enroll_credit_discount.php";
	   break;
	   case MD5('enroll_credit_discount_operation'):
				include "pages/enroll_credit_discount_operation.php";
				
	   case MD5('enroll_credit_writeoff'):
				include "pages/enroll_credit_writeoff.php";
	   break;
	   case MD5('enroll_credit_writeoff_operation'):
				include "pages/enroll_credit_writeoff_operation.php";
	   break;
	   
	   case MD5('enroll_credit_currency_fluctuation'):
				include "pages/enroll_credit_currency_fluctuation.php";
	   break;
	   case MD5('enroll_credit_currency_fluctuation_operation'):
				include "pages/enroll_credit_currency_fluctuation_operation.php";
	   break;
	   
	    case MD5('enroll_credit_bank_charge'):
				include "pages/enroll_credit_bank_charge.php";
	   break;
	   case MD5('enroll_credit_bank_charge_operation'):
				include "pages/enroll_credit_bank_charge_operation.php";
	   break;
	   
	    case MD5('enroll_credit_agent_fee'):
				include "pages/enroll_credit_agent_fee.php";
	   break;
	   case MD5('enroll_credit_agent_fee_operation'):
				include "pages/enroll_credit_agent_fee_operation.php";
	   break;
	   
	    case MD5('enroll_credit_cancel_outstanding_fees'):
				include "pages/enroll_credit_cancel_outstanding_fees.php";
	   break;
	   case MD5('enroll_credit_cancel_outstanding_fees_operation'):
				include "pages/enroll_credit_cancel_outstanding_fees_operation.php";
	   break;
	   
	   case MD5('enroll_credit_operation'):
				include "pages/enroll_credit_operation.php";
	   break;
           case MD5('enroll_debit'):
				include "pages/enroll_debit.php";
	   break;
	   case MD5('enroll_debit2'):
				include "pages/enroll_debit2.php";
	   break;
	   case MD5('enroll_debit_notice'):
				include "pages/enroll_debit_notice.php";
	   break;
	   case MD5('enroll_debit_notice_1'):
				include "pages/enroll_debit_notice_1.php";
	   break;
	   case MD5('enroll_debit_operation'):
				include "pages/enroll_debit_operation.php";
	   break;
	   
	   case MD5('enroll_credit'):
				include "pages/enroll_credit.php";
	   break;
	   case MD5('enroll_credit2'):
				include "pages/enroll_credit2.php";
	   break;
	   case MD5('enroll_credit_operation'):
				include "pages/enroll_credit_operation.php";
	   break;
	   
	   

	   /////////////////////////Tour Groups/////////////////////////////
	   
	   case MD5('display_tour_all'): 		      
	   			include "pages/display_tour_all.php";
	   break;
	   case MD5('display_tour_all_edit'): 		      
	   			include "pages/display_tour_all_edit.php";
	   break;
	   
	    case MD5('display_tour_new'): 		      
	   			include "pages/display_tour_new.php";
	   break;
	    case MD5('display_tour_students'): 		      
	   			include "pages/display_tour_students.php";
	   break;
	   case MD5('display_tour_students_edit'): 		      
	   			include "pages/display_tour_students_edit.php";
	   break;
	   break;
	    case MD5('display_tour_fees'): 		      
	   			include "pages/display_tour_fees.php";
	   break;
	   break;
	    case MD5('display_tour_airport'): 		      
	   			include "pages/display_tour_airport.php";
	   break;
	   case MD5('display_tour_airport_edit'): 		      
	   			include "pages/display_tour_airport_edit.php";
	   break;
	   break;
	    case MD5('display_tour_classes'): 		      
	   			include "pages/display_tour_classes.php";
	   break;
	   case MD5('display_tour_agent'): 		      
	   			include "pages/display_tour_agent.php";
	   break;
	   
	   /////////////////////////Diary/////////////////////////////
	    case MD5('diary_student'):
				include "pages/diary_student.php";
	   break;
	   
	   case MD5('diary_agent'):
				include "pages/diary_agent.php";
	   break;
	   
	   case MD5('diary_accomodation'):
				include "pages/diary_accomodation.php";
	   break;

	    case MD5('display_stu_diary'):
				include "pages/display_stu_diary.php";
	   break;
	   case MD5('display_student_diary_edit'):
				include "pages/display_student_diary_edit.php";
	   break;

	    
	   /////////////////////////classes/////////////////////////////
	   case MD5('classes'):
				include "pages/classes.php";
	   break;
            case MD5('display_assign_class'):
				include "pages/display_assign_class.php";
	   break;

	   case MD5('classes_add'):
				include "pages/classes_add.php";
	   break;
	   case MD5('classes_module'):
				include "pages/classes_module.php";
	   break;
	   case MD5('display_language_module'):
				include "pages/display_language_module.php";
	   break;
	   case MD5('display_language_module_student_add'):
				include "pages/display_language_module_student_add.php";
	   break;
	   case MD5('display_language_module_edit'):
				include "pages/display_language_module_edit.php";
	   break;

	   case MD5('classes_edit'):
				include "pages/classes_edit.php";
	   break;
	   
	    case MD5('classes_language'):
				include "pages/classes_language.php";
	   break;

	   case MD5('classes_language'):
				include "pages/classes_language.php";
	   break;

	    case MD5('display_language_class'):
				include "pages/display_language_class.php";
	   break; 

	   case MD5('display_language_class_edit'):
				include "pages/display_language_class_edit.php";
	   break; 

	   case MD5('classes_language_enroll'):
				include "pages/'classes_language_enroll.php";
	   break; 

	    case MD5('display_absence_class'):
				include "pages/display_absence_class.php";
	   break;


	   case MD5('display_absence_details'):
				include "pages/display_absence_details.php";
	   break;
	   case MD5('classes_enrolment'):
				include "pages/classes_enrolment.php";
	   break;
	   
	   //////////////////////Language/////////////////////////////////
	   
	    case MD5('classes_language'):
				include "pages/classes_language.php";
	   break;

	    case MD5('classes_module'):
				include "pages/classes_module.php";
	   break; 
	    case MD5('classes_language_details'):
				include "pages/classes_language_details.php";
	   break; 
       
       
            //////////////////////Finances/////////////////////////////////
	   
	    case MD5('invoices'):
				include "pages/invoices.php";
	   break;
       
            case MD5('receipts'):
				include "pages/receipts.php";
	   break;
             case MD5('outstanding'):
				include "pages/outstanding.php";
	   break;
	   
	   
	   
	   ////////////////////////Room/////////////////////////////////////
	    case MD5('room'):
				include "pages/room.php";
	   break;
	   case MD5('room_add'):
				include "pages/room_add.php";
	   break;   
	   
	   case MD5('room_edit'):
				include "pages/room_edit.php";
	   break;   
	   
	   /////////////////////////Prospectus/////////////////////////////
	    case MD5('prospectus'):
				include "pages/prospectus.php";
	   break;
	   
	   case MD5('prospectus_edit'):
				include "pages/prospectus_edit.php";
	   break;
	   
	   /////////////////////////Advertise/////////////////////////////
	    case MD5('advertise'):
				include "pages/advertise.php";
	   break;
	   
	   case MD5('advertise_edit'):
				include "pages/advertise_edit.php";
	   break;
	   
	  
	   /////////////////////////testimonial/////////////////////////////
	   
	   case MD5('testimonial'):
				include "pages/testimonial.php";
	   break;
	   
	   case MD5('testimonial_edit'):
				include "pages/testimonial_edit.php";
	   break;
	   
	   /////////////////////////Student service/////////////////////////////
	    case MD5('services'):
				include "pages/services.php";
	   break;

	    case MD5('services_edit'):
				include "pages/services_edit.php";
	   break; 
	   
	   /////////////////////////Register applicant/////////////////////////////
	    case MD5('applicant'):
				include "pages/applicant.php";
	   break;
	   
	   
	   /////////////////////////Faculties/////////////////////////////
	    case MD5('faculties'):
				include "pages/faculties.php";
	   break;

	    case MD5('faculties_add'):
				include "pages/faculties_add.php";
	   break; 
	   
	   case MD5('faculties_edit'):
				include "pages/faculties_edit.php";
	   break; 
	   
	    /////////////////////////Teacher/////////////////////////////
	   case MD5('teacher'):
				include "pages/teacher.php";
	   break;
	    case MD5('teacher_add'):
				include "pages/teacher_add.php";
	   break;
	    case MD5('teacher_edit'):
				include "pages/teacher_edit.php";
	   break;
	    case MD5('teacher_details'):
				include "pages/teacher_details.php";
	   break;
	   
        case MD5('display_module_course'):
				include "pages/display_module_course.php";
	  
	   break;
	    /////////////////////////Course/////////////////////////////
	    case MD5('course'):
				include "pages/course.php";
	   break;

	    case MD5('course_add'):
				include "pages/course_add.php";
	   break; 
	   
	   case MD5('course_edit'):
				include "pages/course_edit.php";
	   break; 
	   
	   case MD5('courses_module'):
				include "pages/courses_module.php";
	   break; 
	   
	   case MD5('courses_module_edit'):
				include "pages/courses_module_edit.php";
	   break;	   
       
	   case MD5('courses_module_add'):
				include "pages/courses_module_add.php";
	   break;

	   case MD5('display_module'):
				include "pages/display_module.php";
	   break;
	    case MD5('course_module_modify'):
				include "pages/course_module_modify.php";
	   break;
	   case MD5('course_module_modify_add'):
				include "pages/course_module_modify_add.php";
	   break;
           case MD5('display_auto_include_fees'):
				include "pages/display_auto_include_fees.php";
	   break;
           case MD5('display_auto_include_fees_edit'):
				include "pages/display_auto_include_fees_edit.php";
	   break;

		
	  /////////////////////////Fees/////////////////////////////
	    case MD5('fees'):
				include "pages/fees.php";
	   break;
	   case MD5('receipt'):
				include "pages/receipt.php";
	   break;
	   case MD5('receipt_details'):
				include "pages/receipt_details.php";
	   break;
	   case MD5('receipt_payment'):
				include "pages/receipt_payment.php";
	   break;
	   case MD5('receipt_payment_edit'):
				include "pages/receipt_payment_edit.php";
	   break;
	   case MD5('debit'):
				include "pages/debit.php";
	   break;
	   case MD5('credit'):
				include "pages/credit.php";
	   break;
	   case MD5('new_invoice'):
				include "pages/new_invoice.php";
	   break;
	    case MD5('receipt_invoice_select'):
				include "pages/receipt_invoice_select.php";
	   break;

		/////////////////////////Agents/////////////////////////////
	    case MD5('agent'):
				include "pages/agent.php";
	   break;
	    case MD5('agent_add'):
				include "pages/agent_add.php";
	   break;

	    case MD5('agent_edit'):
				include "pages/agent_edit.php";
	   break; 
	   case MD5('agent_details'):
				include "pages/agent_details.php";
	   break; 
	   case MD5('display_agent_employee'):
				include "pages/display_agent_employee.php";
	   break;
	    case MD5('display_agent_employee_edit'):
				include "pages/display_agent_employee_edit.php";
                
                 break;
	    case MD5('display_agent_commission_edit'):
				include "pages/display_agent_commission_edit.php";
	   break;
	   
	   break; 
	   case MD5('display_agent_diary'):
				include "pages/display_agent_diary.php";
	   break;
	    case MD5('display_agent_diary_edit'):
				include "pages/display_agent_diary_edit.php";
	   break;

	    break; 
	   case MD5('display_agent_commission'):
				include "pages/display_agent_commission.php";
	   break;
	    case MD5('display_agent_commission_edit'):
				include "pages/display_agent_commission_edit.php";
	   break;
	   //////////////////////////workflow/////////////////////////////
	   case MD5('display_workflow_agent_detail'):
				include "pages/display_workflow_agent_detail.php";
	   break;
           case MD5('mail_template'):
				include "pages/mail_template.php";
	   break;
           case MD5('workflow_history'):
				include "pages/workflow_history.php";
	   break;
           /////////////////////////Reports////////////////////////////////

           case MD5('stud_lang_enroll_report'):
				include "pages/stud_lang_enroll_report.php";
	   break;
           
           case MD5('stud_lang_report_pop'):
				include "pages/stud_lang_report_pop.php";
	   break;
           
           case MD5('agent_report_pop'):
				include "pages/agent_report_pop.php";
	   break;
           case MD5('agent_report'):
				include "pages/agent_report.php";
	   break;

 /////////////////////////Accomodation////////////////////////////////

           case MD5('new_accom'):
				include "pages/new_accom.php";
	   break;
	   case MD5('accom_provider'):
				include "pages/accom_provider.php";
	   break;
	   case MD5('accom_bed'):
				include "pages/accom_bed.php";
	   break;
	   
	    /////////////////////////Airport////////////////////////////////

           case MD5('airport_new'):
				include "pages/airport_new.php";
	   break;
	   case MD5('airport_new_edit'):
				include "pages/airport_new_edit.php";
	   break;

	   /////////////////////////Visa////////////////////////////////

           case MD5('new_visa'):
				include "pages/new_visa.php";
	   break;
	   case MD5('visa_edit'):
				include "pages/visa_edit.php";
	   break;
	   case MD5('ecoe'):
				include "pages/ecoe.php";
	   break;
	   case MD5('ecoe_edit'):
				include "pages/ecoe_edit.php";
	   break;
	    /////////////////////////Workflow/////////////////////////////
	    case MD5('workflow'):
				include "pages/workflow.php";
	   break;
	   
	   case MD5('workflow_edit'):
				include "pages/workflow_edit.php";
	   break;
	   case MD5('diary_workflow_details'):
				include "pages/diary_workflow_details.php";
	   break;
	   case MD5('student_1'):
				include "pages/student_1.php";
	   break;
	   case MD5('agent_1'):
				include "pages/agent_1.php";
	   break;
	   case MD5('all_agent_diary'):
				include "pages/all_agent_diary.php";
	   break;
	    case MD5('all_student_diary'):
				include "pages/all_student_diary.php";
	   break;
	   case MD5('student_workflow_details'):
				include "pages/student_workflow_details.php";
	   break;
	   case MD5('enrolment_workflow_details'):
				include "pages/enrolment_workflow_details.php";
	   break;
	   
//// profile ////
       
        case MD5('edit_profile'):
				include "pages/edit_profile.php";
	   break;
	   
	   
	   /////////////////////////Dashboard/////////////////////////////   
	    default :
				include "pages/home.php";
	   break;
	}
?>	
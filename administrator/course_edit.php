<?php
    /*
        * @package    ACCIT
         * @author    Paperlink Softwares Team
         * @copyright    Copyright (c) 2016 , Paperlink Softwares Pvt. Ltd.  (http://www.paperlinksoftwares.com/)
         * @since    Version 1.0.0
         * @filesource
		 * Code for edit an existing course 
		 * Add new module under the course module
		 * Delete a module from the listed course module
         */

//Update code for the course    
    if($_POST['COURSE_NAME'])
    	{
    	if(mysql_query("update `course` set  `COURSE_NAME`='".$_POST['COURSE_NAME']."',`CODE`='".$_POST['CODE']."',`CRICOS_CODE`='".$_POST['CRICOS_CODE']."', `CRICOS_NAME`='".$_POST['CRICOS_NAME']."',`COURSE_LEN`='".$_POST['COURSE_LEN']."',`STATUS`='".$_POST['STATUS']."', `DISPLAY_NAME`='".$_POST['DISPLAY_NAME']."',`CRT_TYPE`='".$_POST['FACULTY']."', `COURSE_COST`='".$_POST['COURSE_COST']."',`GST`='".$_POST['TAX_CODE']."',`HRS_WEEK`='".$_POST['HRS_WEEK']."', `COURSE_COST_OTHER`='".$_POST['COURSE_COST_OTHER']."',`WEEK_OPTION_NO`='".$_POST['WEEK_OPTION_NO']."',`TYPE_NO`='".$_POST['SESSION']."',`RATE_OPTION_NO`='".$_POST['COURSE_FEE_CATEGORY']."',`CLASS_1_YN`='".$_POST['CLASS_1_YN']."',`CLASS_2_YN`='".$_POST['CLASS_2_YN']."',`CLASS_3_YN`='".$_POST['CLASS_3_YN']."',`CLASS_4_YN`='".$_POST['CLASS_4_YN']."',`CLASS_5_YN`='".$_POST['CLASS_5_YN']."',`CLASS_6_YN`='".$_POST['CLASS_6_YN']."',`WHO_DATE`='".$_POST['WHO_DATE']."',`USER_NO`='".$_POST['user']."' WHERE `COURSE_NO`=".$_GET['edit']))
    	   {
    	    	mysql_query("update `product` set `TYPE_NO`='1', `LOCATION_NO`='1', `CRT_NO`='".$_POST['FACULTY']."', `CUSTOMER_NO`='0', `CODE`='".$_POST['CODE']."', `NAME`='".$_POST['COURSE_NAME']."',`DISPLAY_NAME`='".$_POST['DISPLAY_NAME']."', `CATEGORY_NO`='0',`STATUS_NO`='1', `NOTES`='', `RATE_1`='".$_POST['COURSE_COST']."',`RATE_2`='', `RATE_3`='',`RATE_4`='', `TAX_OPTION_NO`='1',`USER_NO`='".$_POST['user']."', `CREATE_DATE`='".$_POST['WHO_DATE']."',`DELETE_DATE`='', `LOCK_NUM`='1',`ITEM_CODE`='', `ITEM_NAME`='',`PURCHASE_ITEM_NO`='',`REVENUE_GL_ACCOUNT_NO`='',`EXPENSE_GL_ACCOUNT_NO`='',`COMPANY_NO`='1',`TRUST_OPTION_NO`='0',`TRUST_CATEGORY_NO`='0',`ITEM_LENGTH`='',`ITEM_OPTION_NO`='0',`ACCOUNT_CODE`='',`ITEM_RATE_NO`='',`PUBLISH_FLAG`='',`PUBLISH_FROM_DATE`='',`PUBLISH_TO_DATE`='',`EXP_ACCOUNT_NO`='0' WHERE `REF_NO`=".$_GET['edit']);
    		
    		$_SESSION['s_msg']="<strong>Fine!</strong> Student Courses Updated";
    		}
    		else{
    			$_SESSION['e_msg']="<strong>Oh snap!</strong> Student Courses is not Updated";
    		}
    	}
    	
    	
    if($_GET['del'])
    {
    	if(delete_record("DELETE FROM `course`  WHERE `COURSE_NO`=".$_GET['del']))
    		$_SESSION['s_msg']="Item successfully deleted";
    }
    if($_GET['edit'])
    {
    	$cond="where `COURSE_NO`=".$_GET['edit'];
    	$std_row=getRows($cond,'course');
    }
    
    if($_GET['del_sub'])
    {
    	if(delete_record("DELETE FROM `crse_subj` WHERE `CSJ_NO` = ".$_GET['del_sub']))
    	$_SESSION['s_msg']="Item successfully deleted";
    
    }?>
<div class="right_col" role="main">
    <div class="">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="page-title">
                <div class="title_left">
                    <?php 
                        if($_SESSION['s_msg'] || $_SESSION['e_msg'])
                        {
                        ?>
                    <div class="alert <?=($_SESSION['s_msg'])?'alert-success':'alert-error'?> fade in">
                        <button data-dismiss="alert" class="close" type="button">×</button>
                        <?=($_SESSION['s_msg'])?$_SESSION['s_msg']:$_SESSION['e_msg']?>
                    </div>
                    <?php
                        unset($_SESSION['s_msg']);
                        unset($_SESSION['e_msg']);
                        }
                        ?>
                    <h3>Edit Course</h3>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="clearfix"></div>
                    <div class="x_content">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel recent-app">
                                    <div class="x_content">
                                        <div class="student-account-page">
                                            <!-- Nav tabs -->
                                            <div class="card">
                                                <div class="all-students-list add-course">
                                                    <div class="add-student-section">
                                                        <form class="form-horizontal form-label-left" action="" method="post" novalidate>
                                                            <button type="submit" class="btn btn-success" onclick="needToConfirm = false;">Save</button>
                                                            <ul id="student-acc-tab" class="nav nav-tabs" role="tablist">
                                                                <li role="presentation"  class="active"><a href="#details" aria-controls="details" role="tab" data-toggle="tab">Details</a></li>
                                                                <li role="presentation"><a href="#auto-include-fees" aria-controls="auto-include-fees" role="tab" data-toggle="tab">Auto Include Fees</a></li>
                                                                <li role="presentation"><a href="#fee-rules" aria-controls="fee-rules" role="tab" data-toggle="tab">Fee Rules</a></li>
                                                                <li role="presentation"><a href="#course-modules" aria-controls="course-modules" role="tab" data-toggle="tab">Course Modules</a></li>
                                                                <li role="presentation"><a href="#avetmiss" aria-controls="avetmiss" role="tab" data-toggle="tab">AVETMISS</a></li>
                                                                <li role="presentation"><a href="#notes" aria-controls="notes" role="tab" data-toggle="tab">Notes</a></li>
                                                            </ul>
                                                            <div class="tab-content">
                                                                <!--Details-->
                                                                <div role="tabpanel" class="tab-pane active" id="details">
                                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                                        <div class="row">
                                                                            <h4>
                                                                                <fieldset>Basic Info</fieldset>
                                                                            </h4>
                                                                            <?php date_default_timezone_set('Australia/Melbourne');
                                                                                $date = date('d/m/Y h:i:s a', time());
                                                                                ?>
                                                                            <input id="who"  name="WHO_DATE"  type="hidden" value="<?php echo $date?>">
                                                                            <input id="when"  name="user" type="hidden" value="<?php echo $_SESSION['user_no']?>">
                                                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                                                <div class="form-group">
                                                                                    <label class="pull-left control-label col-md-12 col-sm-12 col-xs-12" for="name">Code
                                                                                    </label>
                                                                                    <input id="course_code" value="<?=$std_row[0]['CODE']?>" class="form-control col-md-12 col-xs-12" name="CODE" placeholder="Type Code" type="text">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                                                <div class="form-group">
                                                                                    <label class="control-label col-md-12 col-sm-12 col-xs-12" for="Faculty">Faculty
                                                                                    </label>
                                                                                    <select name="FACULTY" id="course_faculty" class="col-md-12 col-xs-12 form-control">
                                                                                        <option value='1' <?php if ($std_row[0]['CRT_TYPE'] == '1') { echo 'selected="selected"';}?>>IT Programs</option>
                                                                                        <option value='2' <?php if ($std_row[0]['CRT_TYPE'] == '2') { echo 'selected="selected"';}?>>Business Programs</option>
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                                                <div class="item form-group">
                                                                                    <label class="control-label col-md-12 col-sm-12 col-xs-12">Status
                                                                                    </label>
                                                                                    <select name="STATUS"  class="col-md-12 col-xs-12 form-control">
                                                                                        <option value="0" <?php if ($std_row[0]['STATUS'] == '0') { echo 'selected="selected"';}?>>Active</option>
                                                                                        <option value="1" <?php if ($std_row[0]['STATUS'] == '1') { echo 'selected="selected"';}?>>Deactive</option>
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                                                <div class="form-group">
                                                                                    <label class="pull-left control-label col-md-12 col-sm-12 col-xs-12" for="name">Name
                                                                                    </label>
                                                                                    <input id="course_name" value="<?=$std_row[0]['COURSE_NAME']?>" class="form-control col-md-12 col-sm-12 col-xs-12" name="COURSE_NAME" placeholder="Certificate II in Information, Digital Media and Technology" type="text">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                                                <div class="form-group">
                                                                                    <label class="pull-left control-label col-md-12 col-sm-12 col-xs-12" for="name">Display Name
                                                                                    </label>
                                                                                    <input id="course_d_name" value="<?=$std_row[0]['DISPLAY_NAME']?>" class="form-control col-md-12 col-sm-12 col-xs-12" name="DISPLAY_NAME" placeholder="Certificate II in Information, Digital Media and Technology" type="text">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-md-7 col-sm-7 col-xs-12">
                                                                                <div class="form-group">
                                                                                    <label class="pull-left control-label col-md-12 col-sm-12 col-xs-12" for="name">Category
                                                                                    </label>
                                                                                    <select name="COURSE_FEE_CATEGORY" id="course_fee_category" class="col-md-12 col-xs-12 form-control">
                                                                                        <option Value="0" <?php if ($std_row[0]['RATE_OPTION_NO'] == '0') { echo 'selected="selected"';}?>>(None)</option>
                                                                                        <option Value="1" <?php if ($std_row[0]['RATE_OPTION_NO'] == '1') { echo 'selected="selected"';}?>>Material fees</option>
                                                                                        <option Value="2" <?php if ($std_row[0]['RATE_OPTION_NO'] == '2') { echo 'selected="selected"';}?>>COE charges</option>
                                                                                        <option Value="3" <?php if ($std_row[0]['RATE_OPTION_NO'] == '3') { echo 'selected="selected"';}?>>Late Fees penalty</option>
                                                                                        <option Value="4" <?php if ($std_row[0]['RATE_OPTION_NO'] == '4') { echo 'selected="selected"';}?>>Credit card</option>
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-5 col-sm-5 col-xs-12">
                                                                                <div class="form-group">
                                                                                    <div class="checkbox" style="    margin-top: 24px;">
                                                                                        <label class="pull-left control-label">
                                                                                        <input type="hidden" name="CLASS_1_YN" id="course_publish" value="0" >
                                                                                        <input type="checkbox" name="CLASS_2_YN" value="1" <?php echo ($std_row[0]['CLASS_2_YN'] ? 'checked' : '');?>/>Publish to Web
                                                                                        </label>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-md-12 col-sm-12">
                                                                                <div class="row">
                                                                                    <h4>
                                                                                        <fieldset>Course Study</fieldset>
                                                                                    </h4>
                                                                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                                                                        <div class="form-group">
                                                                                            <label class="pull-left control-label col-md-12 col-sm-12 col-xs-12" for="name">Option
                                                                                            </label>
                                                                                            <select name="CLASS_3_YN" id="course_length" class="col-md-12 col-xs-12 form-control">
                                                                                                <option value="0" <?php if ($std_row[0]['CLASS_3_YN'] == '0') { echo 'selected="selected"';}?>>Fixed Length</option>
                                                                                                <option value="1" <?php if ($std_row[0]['CLASS_3_YN'] == '1') { echo 'selected="selected"';}?>>Weekly Rate</option>
                                                                                            </select>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                                                                        <div class="form-group">
                                                                                            <label class="pull-left control-label col-md-12 col-sm-12 col-xs-12" for="name">Hours Per Week
                                                                                            </label>
                                                                                            <input class="form-control" type="number" step="1" min="1" max="" name="HRS_WEEK" id="course_hours_week" value="<?=$std_row[0]['HRS_WEEK']?>" title="Qty" size="4">
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                                                                        <div class="form-group">
                                                                                            <label class="pull-left control-label col-md-12 col-sm-12 col-xs-12" for="name">Length (Week)
                                                                                            </label>
                                                                                            <input class="form-control" type="number" step="1" min="1" max="" name="COURSE_LEN" id="length_week" value="<?=$std_row[0]['COURSE_LEN']?>" title="Qty" size="4">
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                                        <div class="row">
                                                                            <h4>
                                                                                <fieldset>Billing Details</fieldset>
                                                                            </h4>
                                                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                                                <div class="form-group">
                                                                                    <label class="control-label col-md-12 col-sm-12 col-xs-12" for="name">Option
                                                                                    </label>
                                                                                    <select name="WEEK_OPTION_NO" class="col-md-12 col-xs-12 form-control">
                                                                                        <option value="0" <?php if ($std_row[0]['WEEK_OPTION_NO'] == '0') { echo 'selected="selected"';}?>>Course Cost</option>
                                                                                        <option value="1" <?php if ($std_row[0]['WEEK_OPTION_NO'] == '1') { echo 'selected="selected"';}?>>Weekly Rate</option>
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                                                <div class="form-group">
                                                                                    <label class="control-label col-md-12 col-sm-12 col-xs-12" for="name">Cost
                                                                                    </label>
                                                                                    <input class="form-control" placeholder="$0.00" type="number" value="<?=$std_row[0]['COURSE_COST']?>" step="1" min="1" max="" name="COURSE_COST" id="length_week" title="Qty" size="4">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                                                <div class="form-group">
                                                                                    <label class="control-label col-md-12 col-sm-12 col-xs-12" for="name">Alt. Cost
                                                                                    </label>
                                                                                    <input name="COURSE_COST_OTHER" id="bill_all_cost" value="<?=$std_row[0]['COURSE_COST_OTHER']?>" class=" form-control col-md-12 col-xs-12" placeholder="$0.00" type="text">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-md-5 col-sm-5 col-xs-12">
                                                                                <div class="form-group">
                                                                                    <div class="checkbox">
                                                                                        <label class="pull-left control-label">
                                                                                        <input name="CLASS_4_YN" type="hidden" value="0">
                                                                                        <input type="checkbox" name="CLASS_4_YN" value="1" <?php echo ($std_row[0]['CLASS_4_YN'] ? 'checked' : '');?> />Max Length
                                                                                        </label>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-5 col-sm-5 col-xs-12">
                                                                                <div class="form-group">
                                                                                    <div class="checkbox">
                                                                                        <label class="pull-left control-label">
                                                                                        <input name="CLASS_5_YN" type="hidden" value="0">
                                                                                        <input name="CLASS_5_YN" type="checkbox" value="1"  <?php echo ($std_row[0]['CLASS_5_YN'] ? 'checked' : '');?>  class="flat">Max Cost
                                                                                        </label>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-2 offset">
                                                                            </div>
                                                                        </div>
                                                                        <div class="row">
                                                                            <h4>
                                                                                <fieldset>Tax</fieldset>
                                                                            </h4>
                                                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                                                <div class="form-group">
                                                                                    <label class="control-label col-md-12 col-sm-12 col-xs-12" for="name">Option
                                                                                    </label>
                                                                                    <select name="CLASS_6_YN" class="col-md-12 col-xs-12 form-control">
                                                                                        <option value="0" <?php if ($std_row[0]['CLASS_6_YN'] == '0') { echo 'selected="selected"';}?>>Exclusive of Texes</option>
                                                                                        <option value="1" <?php if ($std_row[0]['CLASS_6_YN'] == '1') { echo 'selected="selected"';}?>>Inclusive of Texes</option>
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                                                <div class="form-group">
                                                                                    <label class="control-label col-md-12 col-sm-12 col-xs-12" for="name">Tax Code(s)
                                                                                    </label>
                                                                                    <select name="TAX_CODE" class="col-md-12 col-xs-12 form-control">
                                                                                        <option value="0" <?php if ($std_row[0]['GST'] == '0') { echo 'selected="selected"';}?>>(None)</option>
                                                                                        <option value="1" <?php if ($std_row[0]['GST'] == '1') { echo 'selected="selected"';}?>>GST</option>
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row">
                                                                            <h4>
                                                                                <fieldset>Class Sessions</fieldset>
                                                                            </h4>
                                                                            <div class="col-md-3 col-sm-3 col-xs-12">
                                                                                <div class="form-group">
                                                                                    <div class="checkbox">
                                                                                        <label class="pull-left control-label">
                                                                                        <input type="radio" name="SESSION" value="1" checked <?php if($std_row[0]['TYPE_NO']=="1"){ echo 'checked="checked"';}?>/>Session 1
                                                                                        </label>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-3 col-sm-3 col-xs-12">
                                                                                <div class="form-group">
                                                                                    <div class="checkbox">
                                                                                        <label class="pull-left control-label">
                                                                                        <input type="radio" name="SESSION" value="2" <?php if($std_row[0]['TYPE_NO']=="2"){ echo 'checked="checked"';}?>/>Session 2
                                                                                        </label>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-3 col-sm-3 col-xs-12">
                                                                                <div class="form-group">
                                                                                    <div class="checkbox">
                                                                                        <label class="pull-left control-label">
                                                                                        <input type="radio" name="SESSION" value="3" <?php if($std_row[0]['TYPE_NO']=="3"){ echo 'checked="checked"';}?>/>Session 3
                                                                                        </label>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-3 col-sm-3 col-xs-12">
                                                                                <div class="form-group">
                                                                                    <div class="checkbox">
                                                                                        <label class="pull-left control-label">
                                                                                        <input type="radio" name="SESSION" value="4" <?php if($std_row[0]['TYPE_NO']=="4"){ echo 'checked="checked"';}?>/>Session 4
                                                                                        </label>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row">
                                                                            <h4>
                                                                                <fieldset>CRICOS</fieldset>
                                                                            </h4>
                                                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                                                <div class="form-group">
                                                                                    <label class="control-label col-md-12 col-sm-12 col-xs-12" for="name">Code
                                                                                    </label>
                                                                                    <input id="cricos_id" value="<?=$std_row[0]['CRICOS_CODE']?>" name="CRICOS_CODE" class="form-control col-md-12 col-xs-12" placeholder="0845869" type="text">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8 col-sm-8 col-xs-12">
                                                                                <div class="form-group">
                                                                                    <label class="control-label col-md-12 col-sm-12 col-xs-12" for="name">Name
                                                                                    </label>
                                                                                    <input id="cricos_name" value="<?=$std_row[0]['CRICOS_NAME']?>" class="form-control col-md-12 col-sm-12 col-xs-12" name="CRICOS_NAME" placeholder="Certificate II in Information, Digital Media and Technology" type="text">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!--Auto Include Fees-->
                                                                <div role="tabpanel" class="tab-pane" id="auto-include-fees">
                                                                </div>
                                                                <!--Fee Rules-->
                                                                <div role="tabpanel" class="tab-pane" id="fee-rules">
                                                                </div>
                                                                <!--course-modules-->
                                                                <div role="tabpanel" class="tab-pane" id="course-modules">
                                                                    <div id="att-Warning" class="tab-pane active" role="tabpanel">
                                                                        <div class="table-responsive">
                                                                            <table width="100%" cellspacing="0" class="display table table-striped table-bordered" id="example6">
                                                                                <thead>
                                                                                    <tr class="headings">
                                                                                        <th class="column-title" style="width: 465px !important;">Code
                                                                                        </th>
                                                                                        <th class="column-title" style="width: 465px !important;">Name
                                                                                        </th>
                                                                                        <th class="column-title" style="width: 465px !important;">Hours
                                                                                        </th>
                                                                                        <th class="column-title" style="width: 465px !important;">DELETE
                                                                                        </th>
                                                                                    </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                    <?php  
																					//Listed All the course module under the courses   
                                                                                        $cours= $_GET['edit'];
                                                                                        $cmod=getRows('where `COURSE_NO`='.$_GET['edit'],'crse_subj');
                                                                                                                                  foreach($cmod as $mod)
                                                                                                                                  {?>
                                                                                    <tr class="odd pointer">
                                                                                        <td class=" " style="width: 465px !important;">
                                                                                            <?php   $cond="where `SUBJECT_NO`=".$mod['SUBJECT_NO']; $code=getRows($cond,'subject');?>
                                                                                            <?=$code[0]['SUBJ_CODE']?>
                                                                                        </td>
                                                                                        <td class=" " style="width: 465px !important;"><?=$code[0]['SUBJ_DESC']?></td>
                                                                                        <td class=" " style="width: 465px !important;"><?=$code[0]['HOURS']?></td>
                                                                                        <td class=" " style="width: 465px !important;"><a href="dashboard.php?op=<?=MD5('course_edit')?>&edit=<?=$std_row[0]['COURSE_NO'];?>&del_sub=<?=$mod['CSJ_NO'];?>" onclick="return confirm('Are you sure?'); return false;" class="color-sky stting"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>Delete</a></td>
                                                                                    </tr>
                                                                                    <?php } ?>
                                                                                </tbody>
                                                                            </table>
																			
                                                                            </div>
                                                                    </div>
                                                                </div>
                                                                <!--avetmiss-->
                                                                <div role="tabpanel" class="tab-pane" id="avetmiss">
                                                        <form class="form-horizontal form-label-left">
                                                        <div class="item form-group">
                                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Program ID</label>
                                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                                        <div class="row">
                                                        <div class="col-md-7 col-sm-7 col-xs-12">
                                                        <input id="pid" class="form-control" name="pid" value="<?=$std_row[0]['VET_CODE']?>" type="text">
                                                        </div>
                                                        </div>
                                                        </div>
                                                        <div class="col-md-3 col-sm-3 col-xs-12"></div>
                                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                                        <div class="form-group">
                                                        <div class="checkbox">
                                                        <label class="pull-left control-label">
                                                        <?=($std_row['VET_FLAG'])?'<input type="checkbox" checked class="flat">':'<input type="checkbox" class="flat">'?> The intention of this course is vocational or pre-vocational
                                                        </label>
                                                        </div>
                                                        </div>
                                                        </div>
                                                        </div>
                                                        <div class="item form-group">
                                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Level Of Recognition</label>
                                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                                        <div class="row">
                                                        <div class="col-md-7 col-sm-7 col-xs-12">
                                                        <select disabled class="form-control">
                                                        <option>Level Of Recognition</option>
                                                        <option>Level Of Recognition</option>
                                                        </select>
                                                        </div>
                                                        </div>
                                                        </div>
                                                        </div>
                                                        <div class="item form-group">
                                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Level Of Education</label>
                                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                                        <div class="row">
                                                        <div class="col-md-7 col-sm-7 col-xs-12">
                                                        <select disabled class="form-control">
                                                        <option>Level Of Education</option>
                                                        <option>Level Of Education</option>
                                                        </select>
                                                        </div>
                                                        </div>
                                                        </div>
                                                        </div>
                                                        <div class="item form-group">
                                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Fields Of Education</label>
                                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                                        <div class="row">
                                                        <div class="col-md-7 col-sm-7 col-xs-12">
                                                        <select disabled class="form-control">
                                                        <option>Fields Of Education</option>
                                                        <option>Fields Of Education</option>
                                                        </select>
                                                        </div>
                                                        </div>
                                                        </div>
                                                        </div>
                                                        <div class="item form-group">
                                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Occupational Outcome</label>
                                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                                        <div class="row">
                                                        <div class="col-md-7 col-sm-7 col-xs-12">
                                                        <select disabled class="form-control">
                                                        <option>Occupational Outcome</option>
                                                        <option>Occupational Outcome</option>
                                                        </select>
                                                        </div>
                                                        </div>
                                                        </div>
                                                        </div>
                                                        </form>
                                                        </div>
                                                        <!--notes-->
                                                        <div role="tabpanel" class="tab-pane" id="notes">
                                                        </div>
                                                        </div>
                                                    </div>
                                                   
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
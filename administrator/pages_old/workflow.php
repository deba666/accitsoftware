<div role="main" class="right_col" style="min-height: 648px;">

                  <div class="">
                    <div class="page-title">
                      <div class="title_left">
                        <h3>Work Flow</h3>
                      </div>
                    </div>

                    <div class="clearfix"></div>

                    <div class="row">
                      <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                          <div class="clearfix"></div>

                          <div class="x_content">
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="x_panel recent-app">
                                      <div class="x_title">
                                        <h2>Recent Applications</h2>
                                      </div>

                                      <div class="x_content">
                                        <div class="table-responsive">
                                          <table class="table table-striped jambo_table bulk_action">
                                            <thead>
                                              <tr class="headings">
                                                <th class="column-title">Name</th>
                                                <th class="column-title">DOB</th>
                                                <th class="column-title">Gender</th>
                                                <th class="column-title">Address</th>
                                                <th class="column-title">Country</th>
                                                <th class="column-title no-link last"><span class="nobr">Action</span>
                                                </th>
                                              </tr>
                                            </thead>

                                            <tbody>
                                            <?php $std_row=getRows('order by `STUD_NO`','student'); 
                                                        $sl=0;
                                                        foreach($std_row as $row)
                                                        {
                                                    ?>  
                                              <tr class="even pointer">
                                                <td class=" "><a href="#"><?=$row['LNAME'];?> <?=$row['MNAME'];?> <?=$row['FNAME'];?></a></td>
                                                <td class=" "><?=$row['DOB'];?></td>
                                                <td class=" "><?=$row['GENDER'];?></td>
                                                <td class=""><?=$row['LADDR1'];?> , <?=$row['LSUB'];?> , <?=$row['LSTATE'];?></td>
                                                <td class=""><?=$row['COUNTRY_NAME'];?></td>
                                                <td class="last"><a class="btn btn-success assign" data-toggle="modal" data-target="#editBox" href="file.php?id=<?=$row['STUD_NO'];?>"><span class="glyphicon glyphicon-pencil"></span>Assign</a> | <a href="dashboard.php?op=<?=MD5('student_details')?>&sid=<?=$row['STUD_NO'];?>" class="color-sky stting"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>Details</a></td>
                                              </tr>
                                              <?php }?>
                                            </tbody>
                                          </table>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                            </div>

                                 
                            <div class="clearfix"></div>

                            <div class="row">
                              <div class="student-assign-alert">
								<span id="notification"><h2>Display Workflow Here</h2>  </span>

                                <div class="col-md-6 col-sm-6 col-xs-12">
                                  <div role="alert" class="alert alert-danger">
                                    <button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>
                                    <strong>Warning!</strong> Student Assign Not Done...
                                  </div>
                                </div>
                                

                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                
                <div class="modal fade" id="editBox" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                    <?php    if($_POST['submit']=='sub')
								{
										if(insert_record("insert into `assign` set `std_no`='".$_POST['std_no']."', `std_fname`='".$_POST['std_fname']."', `f_stuff`='".$_POST['f_stuff']."', `a_stuff`='".$_POST['a_stuff']."', `to_date`='".$_POST['to_date']."', `from_date`='".$_POST['from_date']."'"))
										$name= $_POST['std_fname'];
										$stuff= $_POST['f_stuff'];
										$account= $_POST['a_stuff'];
										$to_date= $_POST['to_date'];
										$from_date= $_POST['from_date'];
										$response="<table>	
											<tr>
											<td >Hello Jeeva
											
											</td></tr>
											</table>
											<table width=\"100%\" cellpadding=\"5\" cellspacing=\"5\" align=\"center\">
											
											<tr>
											<td colspan=\"3\">Assignment Details</td>
											</tr>
											
												
											<tr>
											<td><strong>Name</strong></td>
											<td><strong>:</strong></td>
											<td><strong>$name</strong></td>
											</tr>
											
											<tr>
											<td><strong>Stuff</strong></td>
											<td><strong>:</strong></td>
											<td><strong>$name</strong></td>
											</tr>
										
											<tr>
											<td><strong>Account Stuff</strong></td>
											<td><strong>:</strong></td>
											<td><strong>$account</strong></td>
											</tr>
										
											<tr>
											<td><strong>To assign</strong></td>
											<td><strong>:</strong></td>
											<td><strong>$to_date</strong></td>
											</tr>
											
											<tr>
											<td><strong>From assign</strong></td>
											<td><strong>:</strong></td>
											<td><strong>$from_date</strong></td>
											</tr>
											
											</table>";
									
											$response1 = '
											
														<style type="text/css">
															.clearfix:after {
															content: "";
															display: table;
															clear: both;
															}
															a {
															color: #5D6975;
															text-decoration: underline;
															}
															body {
															position: relative;
															width: 21cm;  
															height: 29.7cm; 
															margin: 0 auto; 
															color: #001028;
															background: #FFFFFF; 
															font-family: Arial, sans-serif; 
															font-size: 12px; 
															font-family: Arial;
															}
															header {
															padding: 10px 0;
															margin-bottom: 30px;
															}
															#logo {
															text-align: center;
															margin-bottom: 10px;
															}
															#logo img {
															width: 90px;
															}
															h1 {
															border-top: 1px solid  #5D6975;
															border-bottom: 1px solid  #5D6975;
															color: #5D6975;
															font-size: 2.4em;
															line-height: 1.4em;
															font-weight: normal;
															text-align: center;
															margin: 0 0 20px 0;
															background: url(dimension.png);
															}
															#project {
															float: left;
															}
															#project span {
															color: #5D6975;
															text-align: right;
															width: 76px;
															margin-right: 10px;
															display: inline-block;
															font-size: 0.8em;
															}
															#company {
															float: right;
															text-align: right;
															}
															#project div,
															#company div {
															white-space: nowrap;        
															}
															table {
															width: 100%;
															border-collapse: collapse;
															border-spacing: 0;
															margin-bottom: 20px;
															}
															table tr:nth-child(2n-1) td {
															background: #F5F5F5;
															}
															table th,
															table td {
															text-align: center;
															}
															table th {
															padding: 5px 20px;
															color: #5D6975;
															border-bottom: 1px solid #C1CED9;
															white-space: nowrap;        
															font-weight: normal;
															}
															table .service,
															table .desc {
															text-align: left;
															}
															table td {
															padding: 20px;
															text-align: center;
															}
															table td.service,
															table td.desc {
															vertical-align: top;
															}
															table td.unit,
															table td.qty,
															table td.total {
															font-size: 1.2em;
															}
															table td.grand {
															border-top: 1px solid #5D6975;;
															}
															#notices .notice {
															color: #5D6975;
															font-size: 1.2em;
															}
															footer {
															color: #5D6975;
															width: 100%;
															height: 30px;
															position: absolute;
															bottom: 0;
															border-top: 1px solid #C1CED9;
															padding: 8px 0;
															text-align: center;
															}
															#project span{ text-transform: uppercase; }
														</style>
														<body>
															<header class="clearfix">
																<div id="logo">
																	<img src="logo.png">
																</div>
																<h1>ACCIT College</h1>
																<div id="company" class="clearfix">
																	<div>College Name</div>
																	<div>455 Foggy Heights,<br /> AZ 85004, US</div>
																	<div>(602) 519-0450</div>
																	<div><a href="mailto:College@example.com">College@example.com</a></div>
																</div>
																<div id="project">
																	<div><span>Student No</span> E002</div>
																	<div><span>Student Name</span> Tamal</div>
																	<div><span>Gender</span> Male</div>
																	<div><span>DOB</span> 20 july 1991</div>
																	<div><span>ADDRESS</span> 796 Silver Harbour, TX 79273, US</div>
																	<div><span>EMAIL</span> <a href="mailto:john@example.com">john@example.com</a></div>
																	<div><span>Phone</span> 002102151522</div>
																	<div><span>DATE</span> August 17, 2015</div>
																</div>
															</header>
															<main>
																<table>
																	<thead>
																		<tr>
																			<th class="desc">Course Id</th>
																			<th class="desc">Course Name</th>
																			<th>PRICE</th>
																			<th>TOTAL</th>
																		</tr>
																	</thead>
																	<tbody>
																		<tr>
																			<td class="service">c00145</td>
																			<td class="service">Design</td>
																			<td class="unit">$40.00</td>
																			<td class="total">$1,040.00</td>
																		</tr>
																		<tr>
																			<td class="service">c00145</td>
																			<td class="service">Development</td>
																			<td class="unit">$40.00</td>
																			<td class="total">$3,200.00</td>
																		</tr>
																	</tbody>
																</table>
															</main>
       
											';
										$to = $_POST['std_email'];
										$subject = "Contact Details";
										//$txt = "Hello world!";
										$headers  = 'MIME-Version: 1.0' . "\r\n";
										$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
										$headers .= "From: info@kingswebsoft.com" . "\r\n";
										$admin_mail = "tamal.paperlinksoftwars@gmail.com";
										
										@mail($to,$subject,$response1,$headers);
										if(@mail($admin_mail,$subject,$response,$headers))
											$_SESSION['msg'] ="We have received your Messages.We will get back to you soon.";
										//header("Location:".$_SERVER['PHP_SELF']);
										
								else
								$_SESSION['msg']="<strong>Oh snap!</strong> Not Assing";
								}
						?>
                         // Content Will show Here
                        </div>
                    </div>
                </div>
                
                
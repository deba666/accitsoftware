<?php
if($_GET['del'])
{
	if(delete_record("DELETE FROM `teacher` WHERE `TEACHER_NO` = ".$_GET['del']))
	{ 
		$_SESSION['s_msg']="Item successfully deleted";
	}
}?>
<div class="right_col" role="main">

                  <div class="">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                      <div class="page-title">
					 
                        <div class="title_left">
                          <h3>All Teacher</h3>
                        </div>
                      </div>
                    </div>

                    <div class="clearfix"></div>

                    <div class="row">
                      <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                          <div class="clearfix"></div>

                          <div class="x_content">
                            <div class="row">
                              <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel recent-app">
                                  <div class="x_content">
                                    <div class="student-account-page">
                                      <!-- Nav tabs -->
                                      
                                      <div class="card">
                                                                            
                                        <div class="tab-content">
                                          <div role="tabpanel" class="tab-pane active" id="all-student">
                                            <div class="all-students-list">
                                              <div class="table-responsive">
                                                <table  class="table data-tbl-tools">
                                                  <thead>
                                                    <tr class="headings">
                                                      <th class="column-title">Name</th>
                                                      <th class="column-title">Location</th>
                                                      <th class="column-title">Email</th>
                                                      <th class="column-title">Phone</th>
                                                      <th class="column-title">Status</th>
                                                      <th class="column-title">Level</th>
													  <th class="column-title no-link last"><span class="nobr">Action</span> </th>
                                                    </tr>
                                                  </thead>
                                                  <?php $teacher=getRows('order by `TEACHER_NO` DESC','teacher'); ?>
                                                  <tbody>
													 <?php
                                                        $sl=0;
                                                        foreach($teacher as $row)
                                                        {
                                                    ?>
                                                       <tr class="even pointer">
                                                       <td class=" "><?=$row['TEACHER_CODE'];?> </td>
                                                       <?php $cond="where `LOCATION_NO`=".$row['LOCATION_NO'];
													   $loc= getRows($cond,'location'); ?>
                                                      <td class=" "><?=$loc[0]['LOCATION_NAME'];?> </td>
                                                      <td class=" "><?=$row['TEACHER_EMAIL'];?></td>
                                                      <td class=" "><?=$row['TEACHER_PHONE'];?></td>
                                                      <td class=" "><?=($row['STATUS_NO'])?'Active':'Inactive'?></td>
                                                      <td class=" "><?=$row['TEACHER_LEVEL'];?></td>
													  <td class="last"><a href="dashboard.php?op=<?=MD5('teacher_edit')?>&tid=<?=$row['TEACHER_NO'];?>" class="color-sky stting"><i class="fa fa-cog" aria-hidden="true"></i>Edit</a>| <a href="dashboard.php?op=<?=MD5('teacher')?>&del=<?=$row['TEACHER_NO'];?>" class="color-sky stting"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>Delete</a></td>
                                                    
                                                    </tr>
                                                     <?php   }    ?>
                                                    
                                                  </tbody>
                                                </table>
                                              </div>
                                            </div>
                                          </div>
                                          
                                            
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                 </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                
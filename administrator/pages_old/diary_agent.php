<div style="min-height: 648px;" class="right_col" role="main">

                  <div class="">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                      <div class="page-title">
					 
                        <div class="title_left">
                          <h3>Agent Diary</h3>
                        </div>
                      </div>
                    </div>

                    <div class="clearfix"></div>

                    <div class="row">
                      <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                          <div class="clearfix"></div>

                          <div class="x_content">
                            <div class="row">
                              <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel recent-app">
                                  <div class="x_content">
                                    <div class="student-account-page">
                                      <!-- Nav tabs -->
                                      <div class="card">
                                        <ul class="nav nav-tabs" role="tablist">
                                          <li role="presentation" class="active"><a href="#all-student" aria-controls="all-student" role="tab" data-toggle="tab">Agent Diary</a></li> 
                                        </ul> 
                                        <div class="tab-content">
                                          <div role="tabpanel" class="tab-pane active" id="all-student">
                                            <div class="all-students-list">
                                              <div class="table-responsive">
                                                <div class="dataTables_wrapper form-inline dt-bootstrap no-footer" id="datatable-buttons_wrapper">
                                                <div id="datatable-buttons_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                                                <table style="width: 2012px;" aria-describedby="datatable-buttons_info" role="grid" id="datatable-buttons" class="table table-striped jambo_table bulk_action dataTable no-footer dtr-inline collapsed">
                                                
                                                  
                                                  <thead>
                                                    <tr role="row" class="headings"><th aria-label="Date
                                                        
                                                      : activate to sort column descending" style="width: 100px;" colspan="1" rowspan="1" aria-controls="datatable-buttons" tabindex="0" class="column-title sorting_asc" aria-sort="ascending">Date
                                                        
                                                      </th><th aria-label="Attention
                                                   
                                                      : activate to sort column ascending" style="width: 78px;" colspan="1" rowspan="1" aria-controls="datatable-buttons" tabindex="0" class="column-title sorting">Attention
                                                   
                                                      </th><th aria-label="Agent Code
                                                       
                                                      : activate to sort column ascending" style="width: 78px;" colspan="1" rowspan="1" aria-controls="datatable-buttons" tabindex="0" class="column-title sorting">Agent Code
                                                       
                                                      </th><th aria-label="Subject
                                                    
                                                      : activate to sort column ascending" style="width: 78px;" colspan="1" rowspan="1" aria-controls="datatable-buttons" tabindex="0" class="column-title sorting">Subject
                                                    
                                                      </th><th aria-label="Agent Name
                                                        
                                                      : activate to sort column ascending" style="width: 78px;" colspan="1" rowspan="1" aria-controls="datatable-buttons" tabindex="0" class="column-title sorting">Agent Name
                                                        
                                                      </th><th aria-label="Notes
                                                      
                                                      : activate to sort column ascending" style="width: 78px;" colspan="1" rowspan="1" aria-controls="datatable-buttons" tabindex="0" class="column-title sorting">Notes
                                                      
                                                      </th><th aria-label="User
                                               
                                                      : activate to sort column ascending" style="width: 78px;" colspan="1" rowspan="1" aria-controls="datatable-buttons" tabindex="0" class="column-title sorting">User
                                               
                                                      </th><th aria-label="When
                                               
                                                      : activate to sort column ascending" style="width: 78px;" colspan="1" rowspan="1" aria-controls="datatable-buttons" tabindex="0" class="column-title sorting">When
                                               
                                                      </th><th aria-label="Action
                                                      : activate to sort column ascending" style="width: 0px; display: none;" colspan="1" rowspan="1" aria-controls="datatable-buttons" tabindex="0" class="column-title no-link last sorting"><span class="nobr">Action</span>
                                                      </th></tr>
                                                  </thead>
                                                  <tbody>  
                                                            
                                                   <tr role="row" class="pointer odd">
												   <?php
                                                    /* $sql="select * from `diary`";
													$q=mysql_query($sql);
													while($row=mysql_fetch_array($q))
													{
													$sql1="select * from `agent` where `AGENT_NO`=".$row['REF_NO'];
													$q1=mysql_query($sql1);
													while($row1=mysql_fetch_array($q1))
													{
													$sql2="select * from `users` where `USER_NO`=".$row['USER_NO'];
													$q2=mysql_query($sql2);
													while($row2=mysql_fetch_array($q2))
													{ */
													 $sql="select * from diary D, agent S where D.CUSTOMER_NO=S.CUSTOMER_NO";
												 $query=mysql_query($sql);
												 while($row=mysql_fetch_array($query))
												 {
													?>
															?>
                                                     <td class=" "><?=$row['DIARY_DATE']?></td>
													<td class=" "><?=($row['ATTENTION'])?'<input type="checkbox" checked class="flat">':'<input type="checkbox" class="flat">'?></td>
                                                          <td class=" "><?=$row['AGENT_CODE']?></td>
														  <td class=" "><?=$row['SUMMARY']?></td>
                                                          <td class=" "><?=$row1['AGENT_NAME']?></td>
														  <td class=" "><?=$row['NOTES']?></td>
															<?php $cond="where `USER_NO`=".$row['USER_NO']; $user=getRows($cond,'users');?>
                                                           <td class=" "><?=$user[0]['INIT']?></td>
														 <td class="last"><?=$row['WHO_DATE']?></td>
                                                        </tr>
                                                   <?php } ?>
                                                  </tbody>
                                                </table>
                                                </div>
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                 </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
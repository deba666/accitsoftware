<?php
    if($_GET['eid'])
    {
    	$cond="where `ENROL_NO`=".$_GET['eid'];
    	$std_row=getRows($cond,'enrol');
    
    	$cond1="where `COURSE_NO`=".$std_row[0]['COURSE_NO'];
    	$std_row1=getRows($cond1,'course');
    
    
    	$cond2="where `LOCATION_NO`=".$std_row[0]['LOCATION_NO'];
    	$std_row2=getRows($cond2,'location');
    
    	$cond3="where `AGENT_NO`=".$std_row[0]['AGENT_NO'];
    	$std_row3=getRows($cond3,'agent');
    
    	$cond4="where `USER_NO`=".$std_row[0]['USER_NO'];
    	$std_row4=getRows($cond4,'users'); 
    
    	$cond6="where `STUD_NO`=".$std_row[0]['STUD_NO'];
    	$std_row6=getRows($cond6,'student');
    
    	if($_GET['del2'])
        {
        	if(delete_record("DELETE FROM `diary` WHERE `DIARY_NO` = ".$_GET['del2']))
        		$_SESSION['s_msg']="Item successfully deleted";
        }
    	if($_GET['del3'])
        {
        	if(delete_record("DELETE FROM `enrol_holiday` WHERE `ENROL_HOLIDAY_NO` = ".$_GET['del3']))
        		$_SESSION['s_msg']="Item successfully deleted";
        }
    	if($_GET['del4'])
        {
        	if(delete_record("DELETE FROM `absence` WHERE `ABSENCE_NO` = ".$_GET['del4']))
        		$_SESSION['s_msg']="Item successfully deleted";
        }
    
    	
    }
    ?>
<div role="main" class="right_col" style="min-height: 686px;">
    <div class="">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="page-title">
                <div class="title_left">
                    <?php 
                        if($_SESSION['s_msg'] || $_SESSION['e_msg'])
                        {
                        ?>
                    <div class="alert <?=($_SESSION['s_msg'])?'alert-success':'alert-error'?> fade in">
                        <button data-dismiss="alert" class="close" type="button">�</button>
                        <?=($_SESSION['s_msg'])?$_SESSION['s_msg']:$_SESSION['e_msg']?>
                    </div>
                    <?php
                        unset($_SESSION['s_msg']);
                        unset($_SESSION['e_msg']);
                        }
                        ?>
                    <h3>Language Enrol Details</h3>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="clearfix"></div>
                    <div class="x_content">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel recent-app">
                                    <div class="x_content">
                                        <div class="student-account-page">
                                            <!-- Nav tabs -->
                                            <div class="card">
                                                <div class="all-students-list add student">
                                                    <div class="add-student-section">
                                                        <form novalidate="" method="post" action="" class="form-horizontal form-label-left">
                                                            <button type="submit" class="btn btn-success">Save Now</button>
                                                            <ul role="tablist" class="nav nav-tabs" id="student-acc-tab">
                                                                <li role="presentation"  class="active"><a href="#en_details" aria-controls="en_details" role="tab" data-toggle="tab">Details</a></li>
                                                                <li role="presentation"><a href="#en_diary" aria-controls="en_diary" role="tab" data-toggle="tab">Student Diary</a></li>
                                                                <li role="presentation"><a href="#en_avetmiss" aria-controls="en_avetmiss" role="tab" data-toggle="tab">Avetmiss</a></li>
                                                                <li role="presentation"><a href="#en_changes" aria-controls="en_changes" role="tab" data-toggle="tab">Changes</a></li>
                                                                <li role="presentation"><a href="#en_fees" aria-controls="en_fees" role="tab" data-toggle="tab">Fees</a></li>
                                                                <li role="presentation"><a href="#en_leave" aria-controls="en_leave" role="tab" data-toggle="tab">Leave</a></li>
                                                                <li role="presentation"><a href="#en_absence" aria-controls="en_absence" role="tab" data-toggle="tab">Absence</a></li>
                                                                <li role="presentation"><a href="#en_results" aria-controls="en_results" role="tab" data-toggle="tab">Results</a></li>
                                                                <li role="presentation"><a href="#en_classes" aria-controls="en_classes" role="tab" data-toggle="tab">Classes</a></li>
                                                                <li role="presentation"><a href="#en_course" aria-controls="en_course" role="tab" data-toggle="tab">Course Module</a></li>
                                                            </ul>
                                                            <div class="tab-content">
                                                                <!--Details-->
                                                                <div role="tabpanel" class="tab-pane active" id="en_details">
                                                                    <div class="col-md-8 col-sm-8 col-xs-12">
                                                                        <div class="row">
                                                                            <h4>
                                                                                <fieldset>Basic Info</fieldset>
                                                                            </h4>
                                                                            <div class="col-md-1 col-sm-1 col-xs-12">
                                                                                <div class="form-group">
                                                                                    <label class="control-label col-md-12 col-sm-12 col-xs-12" for="name">Enrol No
                                                                                    </label>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-5 col-sm-5 col-xs-12">
                                                                                <div class="form-group">
                                                                                    <input class="control-label col-md-12 col-sm-12 col-xs-12" name="EXT_STN" Value="<?= $std_row[0]['ENROL_NO']?>" type="text">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-1 col-sm-1 col-xs-12">
                                                                                <div class="form-group">
                                                                                    <label class="control-label col-md-12 col-sm-12 col-xs-12" for="name">Status
                                                                                    </label>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-5 col-sm-5 col-xs-12">
                                                                                <div class="form-group">
                                                                                    <input class="control-label col-md-12 col-sm-12 col-xs-12" name="EXT_STN" Value="<?= $std_row[0]['STATUS_NO']?>" type="text">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-md-1 col-sm-1 col-xs-12">
                                                                                <div class="form-group">
                                                                                    <label class="control-label col-md-12 col-sm-12 col-xs-12" for="name">Length
                                                                                    </label>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-2 col-sm-2 col-xs-12">
                                                                                <div class="form-group">
                                                                                    <input class="control-label col-md-12 col-sm-12 col-xs-12" name="EXT_STN" Value="<?= $std_row[0]['CRSE_LEN']?>" type="text">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-2 col-sm-2 col-xs-12">
                                                                                <div class="form-group">
                                                                                    <label class="control-label col-md-12 col-sm-12 col-xs-12" for="name">Week
                                                                                    </label>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-1 col-sm-1 col-xs-12">
                                                                                <div class="form-group">
                                                                                    <input type="hidden" name="ARVD" id="course_publish" value="0" >
                                                                                    <input class="control-label col-md-12 col-sm-12 col-xs-12" style="zoom:1.5" type="checkbox" name="ARVD" value="1" <?php echo ($std_row[0]['ARVD'] ? 'checked' : '');?>/>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-2 col-sm-2 col-xs-12">
                                                                                <div class="form-group">
                                                                                    <label class="control-label col-md-12 col-sm-12 col-xs-12" for="name">Arrived
                                                                                    </label>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-1 col-sm-1 col-xs-12">
                                                                                <div class="form-group">
                                                                                    <input type="hidden" name="AWARDED" id="course_publish" value="0" >
                                                                                    <input class="control-label col-md-12 col-sm-12 col-xs-12" style="zoom:1.5" type="checkbox" name="AWARDED" value="1" <?php echo ($std_row[0]['AWARDED'] ? 'checked' : '');?>/>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-3 col-sm-3 col-xs-12">
                                                                                <div class="form-group">
                                                                                    <label class="control-label col-md-12 col-sm-12 col-xs-12" for="name">Certificate Awarded
                                                                                    </label>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-md-1 col-sm-1 col-xs-12">
                                                                                <div class="form-group">
                                                                                    <label class="control-label col-md-12 col-sm-12 col-xs-12" for="name">Start
                                                                                    </label>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-3 col-sm-3 col-xs-12">
                                                                                <div class="form-group">
                                                                                    <input class="control-label col-md-12 col-sm-12 col-xs-12" name="ST_DATE" Value="<?= $std_row[0]['ST_DATE']?>" type="text">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-1 col-sm-1 col-xs-12">
                                                                                <div class="form-group">
                                                                                    <label class="control-label col-md-12 col-sm-12 col-xs-12" for="name">Finish
                                                                                    </label>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-3 col-sm-3 col-xs-12">
                                                                                <div class="form-group">
                                                                                    <input class="control-label col-md-12 col-sm-12 col-xs-12" name="END_DATE" Value="<?= $std_row[0]['END_DATE']?>" type="text">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-1 col-sm-1 col-xs-12">
                                                                                <div class="form-group">
                                                                                    <input class="control-label col-md-12 col-sm-12 col-xs-12" style="zoom:1.5" type="checkbox" name="AWARDED" value="1">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-3 col-sm-3 col-xs-12">
                                                                                <div class="form-group">
                                                                                    <label class="control-label col-md-12 col-sm-12 col-xs-12" for="name">Enrolment Form Signed
                                                                                    </label>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                                                        <h4>
                                                                            <fieldset>Attendance</fieldset>
                                                                        </h4>
                                                                        <div class="row">
                                                                            <div class="col-md-2 col-sm-2 col-xs-4">
                                                                                <div class="form-group">
                                                                                    <label class="control-label col-md-12 col-sm-12 col-xs-12" for="name">Current
                                                                                    </label>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-2 col-sm-2 col-xs-4">
                                                                                <div class="form-group">
                                                                                    <input class="control-label col-md-12 col-sm-12 col-xs-12" id="when"  name="USER_NO" type="text" value="<?= $std_row[0]['CUR_ATT']?>">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-1 col-sm-1 col-xs-4">
                                                                                <div class="form-group">
                                                                                    <label class="control-label col-md-12 col-sm-12 col-xs-12" for="name">%
                                                                                    </label>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-md-2 col-sm-2 col-xs-4">
                                                                                <div class="form-group">
                                                                                    <label class="control-label col-md-12 col-sm-12 col-xs-12" for="name">Overall
                                                                                    </label>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-2 col-sm-2 col-xs-4">
                                                                                <div class="form-group">
                                                                                    <input class="control-label col-md-12 col-sm-12 col-xs-12" id="when"  name="USER_NO" type="text" value="<?= $std_row[0]['OVR_ATT']?>">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-1 col-sm-1 col-xs-4">
                                                                                <div class="form-group">
                                                                                    <label class="control-label col-md-12 col-sm-12 col-xs-12" for="name">%
                                                                                    </label>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-md-2 col-sm-2 col-xs-4">
                                                                                <div class="form-group">
                                                                                    <label class="control-label col-md-12 col-sm-12 col-xs-12" for="name">Hrs Absent 
                                                                                    </label>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-3 col-sm-3 col-xs-4">
                                                                                <div class="form-group">
                                                                                    <input class="control-label col-md-12 col-sm-12 col-xs-12" id="when"  name="USER_NO" type="text" value="<?= $std_row[0]['ABSENT_TOTAL']?>">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <h4>
                                                                        <fieldset>Other</fieldset>
                                                                    </h4>
                                                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                                                        <div class="row">
                                                                            <div class="col-md-1 col-sm-1 col-xs-12">
                                                                                <div class="form-group">
                                                                                    <label class="control-label col-md-12 col-sm-12 col-xs-12" for="name">Course
                                                                                    </label>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                                                <div class="form-group">
                                                                                    <input class="control-label col-md-12 col-sm-12 col-xs-12" id="when"  name="USER_NO" type="text" value="<?= $std_row1[0]['COURSE_NAME']?>">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-1 col-sm-1 col-xs-12">
                                                                                <div class="form-group">
                                                                                    <label class="control-label col-md-12 col-sm-12 col-xs-12" for="name">Location
                                                                                    </label>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                                                <div class="form-group">
                                                                                    <input class="control-label col-md-12 col-sm-12 col-xs-12" id="when"  name="USER_NO" type="text" value="<?= $std_row2[0]['LOCATION_NAME']?>">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-md-1 col-sm-1 col-xs-12">
                                                                                <div class="form-group">
                                                                                    <label class="control-label col-md-12 col-sm-12 col-xs-12" for="name">Agent
                                                                                    </label>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                                                <div class="form-group">
                                                                                    <input class="control-label col-md-12 col-sm-12 col-xs-12" id="when"  name="USER_NO" type="text" value="<?= $std_row3[0]['AGENT_NAME']?>">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-md-2 col-sm-2 col-xs-9">
                                                                                <div class="form-group">
                                                                                    <label class="control-label col-md-12 col-sm-12 col-xs-12" for="name">Booking Date
                                                                                    </label>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-2 col-sm-2 col-xs-9">
                                                                                <div class="form-group">
                                                                                    <input class="control-label col-md-12 col-sm-12 col-xs-12" name="EXT_STN" Value="<?= $std_row[0]['ADD_DATE']?>" type="text">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-1 col-sm-1 col-xs-9">
                                                                                <div class="form-group">
                                                                                    <label class="control-label col-md-12 col-sm-12 col-xs-12" for="name">Who
                                                                                    </label>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-1 col-sm-1 col-xs-9">
                                                                                <div class="form-group">
                                                                                    <input class="control-label col-md-12 col-sm-12 col-xs-12" name="EXT_STN" Value="<?= $std_row4[0]['INIT']?>" type="text">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-1 col-sm-1 col-xs-9">
                                                                                <div class="form-group">
                                                                                    <label class="control-label col-md-12 col-sm-12 col-xs-12" for="name">When
                                                                                    </label>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-2 col-sm-2 col-xs-9">
                                                                                <div class="form-group">
                                                                                    <input class="control-label col-md-12 col-sm-12 col-xs-12" name="EXT_STN" type="text" Value="<?= $std_row[0]['WHO_DATE']?>">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <h4>
                                                                        <fieldset>Custom Fields</fieldset>
                                                                    </h4>
                                                                    <div class="control-label col-md-12 col-sm-12 col-xs-12">
                                                                        <label class="control-label col-md-6 col-sm-6 col-xs-12" for="name">
                                                                        <input name="CUST1" type="text" class="col-md-12 col-xs-12 form-control">
                                                                        </label>
                                                                        <label class="control-label col-md-6 col-sm-6 col-xs-12" for="name">
                                                                        <input name="CUST2" type="text" class="col-md-12 col-xs-12 form-control">
                                                                        </label>
                                                                        <label class="control-label col-md-6 col-sm-6 col-xs-12" for="name">
                                                                        <input name="CUST4" type="text" class="col-md-12 col-xs-12 form-control">
                                                                        </label>
                                                                        <label class="control-label col-md-6 col-sm-6 col-xs-12" for="name">
                                                                        <input name="CUST4" type="text" class="col-md-12 col-xs-12 form-control">
                                                                        </label>
                                                                        <label class="control-label col-md-6 col-sm-6 col-xs-12" for="name">
                                                                        <input name="CUST5" type="text" class="col-md-12 col-xs-12 form-control">
                                                                        </label>
                                                                        <label class="control-label col-md-6 col-sm-6 col-xs-12" for="name">
                                                                        <input name="CUST6" type="text" class="col-md-12 col-xs-12 form-control">
                                                                        </label>
                                                                        <label class="control-label col-md-6 col-sm-6 col-xs-12" for="name">
                                                                        <input name="CUST7" type="text" class="col-md-12 col-xs-12 form-control">
                                                                        </label>
                                                                        <label class="control-label col-md-6 col-sm-6 col-xs-12" for="name">
                                                                        <input name="CUST8" type="text" class="col-md-12 col-xs-12 form-control">
                                                                        </label>
                                                                        <label class="control-label col-md-6 col-sm-6 col-xs-12" for="name">
                                                                        <input name="CUST9" type="text" class="col-md-12 col-xs-12 form-control">
                                                                        </label>
                                                                        <label class="control-label col-md-6 col-sm-6 col-xs-12" for="name">
                                                                        <input name="CUST10" type="text" class="col-md-12 col-xs-12 form-control">
                                                                        </label>
                                                                    </div>
                                                                    <h4>
                                                                        <fieldset>Notes</fieldset>
                                                                    </h4>
                                                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                                                        <div class="row">
                                                                            <textarea rows="3" cols="5" style="margin: 0px; width: 1157px; height: 79px;"></textarea>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!-- Student Diary -->
                                                                <div role="tabpanel" class="tab-pane" id="en_diary">
                                                                    <div class="table-responsive">
                                                                        <?php  $ag= $_POST['edit']?>
                                                                        <h3><a href="#" onclick="javascript:void window.open('dashboard.php?op=<?=MD5('display_student_diary')?>&s_no=<?=$std_row6[0]['EXT_STN']?>&s_fname=<?=$std_row6[0]['FNAME']?>&s_lname=<?=$std_row6[0]['LNAME']?>&s_mname=<?=$std_row6[0]['MNAME']?>&ag_name=<?=$std_row[0]['AGENT_NAME']?>&cus_no=<?=$std_row[0]['CUSTOMER_NO']?>','1470138621367','width=700,height=500,toolbar=0,menubar=0,location=1,status=1,scrollbars=1,resizable=1,left=0,top=0');return false;">Add New Diary</a></h3>
                                                                        <table id="example2" class="display table table-striped table-bordered" cellspacing="0" width="100%">
                                                                            <div class="hide-show-column dropdown" role="presentation">
                                                                                <a id="drop4" href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" role="button" aria-expanded="false">Hide / Show Column<span class="caret"></span></a>
                                                                                <ul id="menu6" class="dropdown-menu animated fadeInDown" role="menu">
                                                                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                                                        <span class="pull-left">
                                                                                        <input type="checkbox" class="flat">
                                                                                        </span>
                                                                                        <span class="pull-left">
                                                                                        Attention
                                                                                        </span>
                                                                                        </a>
                                                                                    </li>
                                                                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                                                        <span class="pull-left">
                                                                                        <input type="checkbox" class="flat">
                                                                                        </span>
                                                                                        <span class="pull-left">
                                                                                        Date
                                                                                        </span>
                                                                                        </a>
                                                                                    </li>
                                                                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                                                        <span class="pull-left">
                                                                                        <input type="checkbox" class="flat">
                                                                                        </span>
                                                                                        <span class="pull-left">
                                                                                        Category
                                                                                        </span>
                                                                                        </a>
                                                                                    </li>
                                                                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                                                        <span class="pull-left">
                                                                                        <input type="checkbox" class="flat">
                                                                                        </span>
                                                                                        <span class="pull-left">
                                                                                        Subject
                                                                                        </span>
                                                                                        </a>
                                                                                    </li>
                                                                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                                                        <span class="pull-left">
                                                                                        <input type="checkbox" class="flat">
                                                                                        </span>
                                                                                        <span class="pull-left">
                                                                                        When
                                                                                        </span>
                                                                                        </a>
                                                                                    </li>
                                                                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                                                        <span class="pull-left">
                                                                                        <input type="checkbox" class="flat">
                                                                                        </span>
                                                                                        <span class="pull-left">
                                                                                        User
                                                                                        </span>
                                                                                        </a>
                                                                                    </li>
                                                                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                                                        <span class="pull-left">
                                                                                        <input type="checkbox" class="flat">
                                                                                        </span>
                                                                                        <span class="pull-left">
                                                                                        Notes
                                                                                        </span>
                                                                                        </a>
                                                                                </ul>
                                                                                <thead>
                                                                                    <tr class="headings">
                                                                                        <th class="column-title">Attention
                                                                                        </th>
                                                                                        <th class="column-title">Date
                                                                                        </th>
                                                                                        <th class="column-title">Category
                                                                                        </th>
                                                                                        <th class="column-title">Subject
                                                                                        </th>
                                                                                        <th class="column-title">User
                                                                                        </th>
                                                                                        <th class="column-title">Details
                                                                                        </th>
                                                                                        <th class="column-title">Action
                                                                                        </th>
                                                                                    </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                    <?php
                                                                                        $co1="where `CUSTOMER_NO`=".$std_row[0]['CUSTOMER_NO'];
                                                                                        $st1=getRows($co1,'diary');
                                                                                        foreach($st1 as $st2)																			{
                                                                                        	?>
                                                                                    <tr class="even pointer">
                                                                                        <td class=" "><?=($st2['ATTENTION'])?'<input type="checkbox" checked class="flat">':'<input type="checkbox" class="flat">'?></td>
                                                                                        <td class=" "><?=$st2['DIARY_DATE']?></td>
                                                                                        <?php
                                                                                            $coa="where `LOOKUP_CODE_NO`=".$st2['CATEGORY_NO'];
                                                                                                   $sta=getRows($coa,'lookup_code');
                                                                                            ?>
                                                                                        <td class=" "><?=$sta[0]['NAME']?></td>
                                                                                        <td class=" "><?=$st2['SUMMARY']?></td>
                                                                                        <?php
                                                                                            $cob="where `USER_NO`=".$st2['USER_NO'];
                                                                                                   $stab=getRows($cob,'users');
                                                                                            ?>
                                                                                        <td class=" "><?=$stab[0]['INIT']?></td>
                                                                                        <td class=" "><?=$st2['NOTES']?></td>
                                                                                        <td><a href="#" onclick="javascript:void window.open('dashboard.php?op=<?=MD5('display_student_diary_edit')?>&d_no=<?=$st2['DIARY_NO']?>&s_no=<?=$std_row6[0]['EXT_STN']?>&s_fname=<?=$std_row6[0]['FNAME']?>&s_lname=<?=$std_row6[0]['LNAME']?>&s_mname=<?=$std_row6[0]['MNAME']?>&ag_name=<?=$std_row[0]['AGENT_NAME']?>','1470138621367','width=700,height=500,toolbar=0,menubar=0,location=1,status=1,scrollbars=1,resizable=1,left=0,top=0');return false;">Edit</a>|<a href="dashboard.php?op=<?=MD5('enrolment_details')?>&del2=<?=$st2['DIARY_NO']?>&eid=<?=$std_row[0]['ENROL_NO']?>" class="color-sky stting"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>Delete</a></td>
                                                                                    </tr>
                                                                                    <?php }  ?>
                                                                                </tbody>
                                                                            </div>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                                <!--Avetmiss-->
                                                                <div id="en_avetmiss" class="tab-pane" role="tabpanel">
                                                                    <div class="row">
                                                                        <div class="col-md-9 col-sm-9 col-xs-9">
                                                                            <div class="col-md-3 col-sm-3 col-xs-9">
                                                                                <div class="form-group">
                                                                                    <label for="name" class="pull-left control-label col-md-12 col-sm-12 col-xs-12">Funding Source
                                                                                    </label>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-6 col-sm-6 col-xs-9">
                                                                                <div class="form-group">
                                                                                    <select class="form-control col-md-12 col-xs-12">
                                                                                        <option>1</option>
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-md-9 col-sm-9 col-xs-9">
                                                                            <div class="col-md-3 col-sm-3 col-xs-9">
                                                                                <div class="form-group">
                                                                                    <label for="name" class="pull-left control-label col-md-12 col-sm-12 col-xs-12">Specific Funding Identifier
                                                                                    </label>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-6 col-sm-6 col-xs-9">
                                                                                <div class="form-group">
                                                                                    <input class="form-control col-md-12 col-xs-12" type="text">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-md-9 col-sm-9 col-xs-9">
                                                                            <div class="col-md-3 col-sm-3 col-xs-9">
                                                                                <div class="form-group">
                                                                                    <label for="name" class="pull-left control-label col-md-12 col-sm-12 col-xs-12">Study Reason
                                                                                    </label>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-6 col-sm-6 col-xs-9">
                                                                                <div class="form-group">
                                                                                    <select class="form-control col-md-12 col-xs-12">
                                                                                        <option>1</option>
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-md-6 col-sm-6 col-xs-6">
                                                                            <div class="col-md-3 col-sm-3 col-xs-6">
                                                                                <div class="form-group">
                                                                                    <label for="name" class="pull-left control-label col-md-12 col-sm-12 col-xs-12">Year Completed
                                                                                    </label>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-3 col-sm-3 col-xs-6">
                                                                                <div class="form-group">
                                                                                    <input class="form-control col-md-12 col-xs-12" type="text">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!--Changes-->
                                                                <div role="tabpanel" class="tab-pane" id="en_changes">
                                                                    <div class="table-responsive">
                                                                        <table id="example" class="display table table-striped table-bordered" cellspacing="0" width="100%">
                                                                            <div class="hide-show-column dropdown" role="presentation">
                                                                                <a id="drop4" href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" role="button" aria-expanded="false">Hide / Show Column<span class="caret"></span></a>
                                                                                <ul id="menu6" class="dropdown-menu animated fadeInDown" role="menu">
                                                                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                                                        <span class="pull-left">
                                                                                        <input type="checkbox" class="flat">
                                                                                        </span>
                                                                                        <span class="pull-left">
                                                                                        Type
                                                                                        </span>
                                                                                        </a>
                                                                                    </li>
                                                                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                                                        <span class="pull-left">
                                                                                        <input type="checkbox" class="flat">
                                                                                        </span>
                                                                                        <span class="pull-left">
                                                                                        Start Date
                                                                                        </span>
                                                                                        </a>
                                                                                    </li>
                                                                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                                                        <span class="pull-left">
                                                                                        <input type="checkbox" class="flat">
                                                                                        </span>
                                                                                        <span class="pull-left">
                                                                                        Finish Date
                                                                                        </span>
                                                                                        </a>
                                                                                    </li>
                                                                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                                                        <span class="pull-left">
                                                                                        <input type="checkbox" class="flat">
                                                                                        </span>
                                                                                        <span class="pull-left">
                                                                                        Weeks
                                                                                        </span>
                                                                                        </a>
                                                                                    </li>
                                                                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                                                        <span class="pull-left">
                                                                                        <input type="checkbox" class="flat">
                                                                                        </span>
                                                                                        <span class="pull-left">
                                                                                        Who
                                                                                        </span>
                                                                                        </a>
                                                                                    </li>
                                                                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                                                        <span class="pull-left">
                                                                                        <input type="checkbox" class="flat">
                                                                                        </span>
                                                                                        <span class="pull-left">
                                                                                        When
                                                                                        </span>
                                                                                        </a>
                                                                                    </li>
                                                                                </ul>
                                                                            </div>
                                                                            <thead>
                                                                                <tr class="headings">
                                                                                    <th class="column-title">Type
                                                                                    </th>
                                                                                    <th class="column-title">Start Date
                                                                                    </th>
                                                                                    <th class="column-title">End Date
                                                                                    </th>
                                                                                    <th class="column-title">Weeks
                                                                                    </th>
                                                                                    <th class="column-title">Who
                                                                                    </th>
                                                                                    <th class="column-title">When
                                                                                    </th>
                                                                                    <th class="column-title">Action
                                                                                    </th>
                                                                                </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                                <?php
                                                                                    /*  $cond10="where `STUD_NO`=".$std_row[0]['STUD_NO'];
                                                                                      $std_row10=getRows($cond10,'enrol');
                                                                                      foreach($std_row10 as $std_row11)
                                                                                      {
                                                                                     $cond12="where `REF_NO`=".$std_row11['COURSE_NO'];
                                                                                      $std_row12=getRows($cond12,'product');
                                                                                      foreach($std_row12 as $std_row13)
                                                                                      {
                                                                                     /*$cond14="where `CRT_NO`=".$std_row11['CRT_NO'];
                                                                                      $std_row14=getRows($cond14,'crse_type');
                                                                                      foreach($std_row14 as $std_row15)
                                                                                      {
                                                                                      $cond16="where `LOCATION_NO`=".$std_row15['LOCATION_NO'];
                                                                                      $std_row16=getRows($cond16,'location');
                                                                                      foreach($std_row16 as $std_row17)
                                                                                      { */
                                                                                     /* $cond18="where `AGENT_NO`=".$std_row11['AGENT_NO'];
                                                                                      $std_row18=getRows($cond18,'agent');
                                                                                      foreach($std_row18 as $std_row19)
                                                                                      { */
                                                                                    	  ?>
                                                                                <tr class="even pointer">
                                                                                    <td class=" "><?=$std_row11['CUR_ATT']?></td>
                                                                                    <td class=" "><?=$std_row11['OVR_ATT']?></td>
                                                                                    <td class=" "></td>
                                                                                    <td class=" "></td>
                                                                                    <td class=" "></td>
                                                                                    <td class=" "></td>
                                                                                    <td class=" "><a href="dashboard.php?op=<?=MD5('offer_details')?>&oid=<?=$std_row21['OFFER_NO']?>" class="color-sky stting"><i class="fa fa-cog" aria-hidden="true"></i>Edit</a></td>
                                                                                </tr>
                                                                                <?php
                                                                                    ?>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                                <!--Fees-->
                                                                <div role="tabpanel" class="tab-pane" id="en_fees">
                                                                <div class="table-responsive">
                                                                    <div class="page-title">
                                                                        <div class="title_left">
                                                                            <a href="#" onclick="javascript:void window.open('dashboard.php?op=<?=MD5('receipt')?>&enrol_no=<?=$std_row[0]['ENROL_NO']?>&stu_no=<?=$std_row[0]['STUD_NO']?>&sname=<?=$std_row[0]['LNAME']?>&cust_no=<?=$std_row[0]['CUSTOMER_NO']?>&cun_no=<?=$std_row[0]['COUNTRY_NO']?>','1470138621367','width=700,height=450,toolbar=0,menubar=0,location=1,status=1,scrollbars=1,resizable=1,left=0,top=0');return false;">&nbsp;&nbsp;&nbsp;&nbsp;Receipt</a>
                                                                            <a href="#" onclick="javascript:void window.open('dashboard.php?op=<?=MD5('debit')?>&stu_no=<?=$std_row[0]['STUD_NO']?>&sname=<?=$std_row[0]['LNAME']?>&cust_no=<?=$std_row[0]['CUSTOMER_NO']?>&cun_no=<?=$std_row[0]['COUNTRY_NO']?>','1470138621367','width=1200,height=800,toolbar=0,menubar=0,location=1,status=1,scrollbars=1,resizable=1,left=0,top=0');return false;">&nbsp;&nbsp;&nbsp;&nbsp;Debit</a>
                                                                            <a href="#" onclick="javascript:void window.open('dashboard.php?op=<?=MD5('credit')?>&stu_no=<?=$std_row[0]['STUD_NO']?>&sname=<?=$std_row[0]['LNAME']?>&cust_no=<?=$std_row[0]['CUSTOMER_NO']?>&cun_no=<?=$std_row[0]['COUNTRY_NO']?>','1470138621367','width=1200,height=800,toolbar=0,menubar=0,location=1,status=1,scrollbars=1,resizable=1,left=0,top=0');return false;">&nbsp;&nbsp;&nbsp;&nbsp;Credit</a>
                                                                            <a href="#" onclick="javascript:void window.open('dashboard.php?op=<?=MD5('new_invoice')?>&stu_no=<?=$std_row[0]['STUD_NO']?>&sname=<?=$std_row[0]['LNAME']?>&cust_no=<?=$std_row[0]['CUSTOMER_NO']?>&cun_no=<?=$std_row[0]['COUNTRY_NO']?>','1470138621367','width=1200,height=800,toolbar=0,menubar=0,location=1,status=1,scrollbars=1,resizable=1,left=0,top=0');return false;">&nbsp;&nbsp;&nbsp;&nbsp;New Invoice</a>
                                                                        </div>
                                                                    </div>
                                                                    <ul role="tablist" class="nav nav-tabs" id="student-acc-tab">
                                                                        <li role="presentation"  class="active"><a href="#instalment" aria-controls="instalment" role="tab" data-toggle="tab">Invoice/Instalment</a></li>
                                                                        <li role="presentation"><a href="#transaction" aria-controls="transaction" role="tab" data-toggle="tab">Transaction </a></li>
                                                                    </ul>
                                                                    <div role="tabpanel" class="tab-pane active" id="instalment">
                                                                        <div class="table-responsive">
                                                                            <table class="table table-striped jambo_table bulk_action">
                                                                                <thead>
                                                                                    <tr class="headings">
                                                                                        <th class="column-title">Date</th>
                                                                                        <th class="column-title">Who
                                                                                        <th class="column-title">Due Date
                                                                                        </th>
                                                                                        <th class="column-title">Reference
                                                                                        </th>
                                                                                        <th class="column-title">Amount
                                                                                        </th>
                                                                                        <th class="column-title">Tax
                                                                                        </th>
                                                                                        <th class="column-title">Received
                                                                                        </th>
                                                                                        <th class="column-title">Credit
                                                                                        </th>
                                                                                        <th class="column-title">Debit
                                                                                        </th>
                                                                                        <th class="column-title">Balance
                                                                                        </th>
                                                                                        <th class="bulk-actions" colspan="7">
                                                                                            <a class="antoo" style="color:#fff; font-weight:500;">Action( <span class="action-cnt"></span> ) <i class="fa fa-chevron-down"></i></a>
                                                                                        </th>
                                                                                    </tr>
                                                                                </thead>
                                                                            </table>
                                                                            <div class="row accordian">
                                                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                                                    <?php
                                                                                        $cond20="where `ENROL_NO`=".$std_row[0]['ENROL_NO'];
                                                                                        $std_row20=getRows($cond20,'invoice');
                                                                                        foreach($std_row20 as $std_row21)
                                                                                        {
                                                                                         ?>
                                                                                    <div class="x_panel ">
                                                                                        <div class="x_title">
                                                                                            <ul class="nav navbar-left panel_toolbox equal-with">
                                                                                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i>Invoice<?= $std_row21['INVOICE_NO']?>&nbsp;&nbsp;Due<?= $std_row21['DUE_DATE']?></a>
                                                                                                </li>
																								<li></li>
																								<li></li>
																								<li></li>
																								<li>$<?= $std_row21['INVOICE_AMOUNT']?>.00</li>
																								<?php
																						$con="where `INVOICE_NO`=".$std_row21['INVOICE_NO'];
                                                                                        $std_r=getRows($con,'invoice_details'); ?>
																						<li>$<?= $std_r[0]['TAX_AMOUNT']?>.00</li>
																						<?php
																						$con1="where `IND_NO`=".$std_r[0]['IND_NO'];
                                                                                        $std_r1=getRows($con1,'credit');
																						foreach($std_r1 as $std_r2)
																							{
																						if($std_r2['RECEIPT_NO']!='0')
																							{
																							
																							//$credit=$std_r2['AMOUNT'];
																							$credit=0;
																							}
																							else
																								{
																								$credit=$std_r2['AMOUNT'];
																								//$credit=0;
																								}
																							}
																							$received=$std_row21['INVOICE_AMOUNT']-$credit;
																							$debit=$std_row21['INVOICE_AMOUNT']-($received+$credit);
																							$balance=$std_row21['INVOICE_AMOUNT']-($received+$credit)-$debit;
																							
																						 ?>
																								<li>$<?php echo $received;?>.00</li>
																								<li>$<?php echo $credit;?>.00</li>
																								<?php 
																						$sql11="select SUM(AMOUNT) from debit_item where ENROL_NO=".$std_row[0]['ENROL_NO'];
																						$que=mysql_query($sql11);
																						while($row11=mysql_fetch_array($que))
																							{
																							$rr=$row11['SUM(AMOUNT)'];
																							}
																						 ?>
																								<li>$<?php echo $rr;?>.00</li>
																								<li>$<?php echo $balance;?>.00</li>
                                                                                            </ul>
                                                                                            <div class="clearfix"></div>
                                                                                        </div>
                                                                                        <div class="x_content">
                                                                                            <div class="row">
                                                                                                <div class="col-md-12 col-sm-12">
                                                                                                    <div class="table-responsive">
                                                                                                        <table class="table table-striped jambo_table bulk_action">
                                                                                                            <tbody>
                                                                                             <?php
                                                                                                 $cond23="where `INVOICE_NO`=".$std_row21['INVOICE_NO'];
                                                                                                 $std_row23=getRows($cond23,'invoice_details');
																								 $cond25="where `INVOICE_NO`=".$std_row23[0]['INVOICE_NO'];
																								 $std_row25=getRows($cond25,'credit');
																								 foreach($std_row25 as $std_row26)
																								  { 
																									$c="where `fee_no`=".$std_row26['FEE_NO'];
                                                                                                    $s=getRows($c,'fees');
                                                                                                  ?>
                                                                                                  <tr class="even pointer">
                                                                                                    <td class=" " align="center"><?= $std_row21['INVOICE_DATE']?></td>
                                                                                                     <?php $cond22="where `USER_NO`=".$std_row21['USER_NO'];
                                                                                                       $std_row22=getRows($cond22,'users');?>
                                                                                                       <td class=" " align="center"><?= $std_row22[0]['INIT']?></td>
                                                                                                        <td class=" " align="center"><?= $std_row21['DUE_DATE']?></td>
																										<?php ?>
                                                                                                         <td class=" " align="center"><?= $s[0]['fee_name']?></td>
                                                                                                         <td class=" " align="center">$<?= $std_row26['AMOUNT']?>.00</td>
                                                                                                         <td class=" " align="center">$<?= $std_row23[0]['TAX_AMOUNT']?>.00</td>
																										<?php
																										
																											/*if($std_row26['RECEIPT_NO']!='0')
																											{
																							
																							//$credit=$std_r2['AMOUNT'];
																											$credit1=0;
																												}
																											else
																												{
																											$credit1=$std_row26['AMOUNT'];
																											//$credit=0;
																											}
																										 }
																										$received1=$std_row21['INVOICE_AMOUNT']-$credit1;
																										$debit1=$std_row21['INVOICE_AMOUNT']-($received1+$credit1);*/
																										$balance1=$std_row21['INVOICE_AMOUNT']-($received1+$credit1)-$debit1; ?>

                                                                                                         <td class=" " align="center">$<?php echo $received1;?>.00</td>
                                                                                                         <td class=" " align="center">$<?php echo $credit1;?>.00</td>
                                                                                                         <td class=" " align="center">$<?php echo $debit1;?>.00</td>
																										 <td class=" " align="center">$<?php echo $balance1;?>.00</td>
                                                                                                          </tr>
                                                                                                     <?php } ?>
                                                                                                       </tbody>
                                                                                                      </table>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <?php  } ?>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div></div>
                                                                   <!--   <div role="tabpanel" class="tab-pane" id="transaction">
                                                                        <div class="table-responsive">
                                                                            <table class="table table-striped jambo_table bulk_action">
                                                                                <thead>
                                                                                    <tr class="headings">
                                                                                        <th class="column-title">Date</th>
																						<th class="column-title">Activity</th>
                                                                                        <th class="column-title">Who
                                                                                        <th class="column-title">Reference
                                                                                        </th>
                                                                                        <th class="column-title">Amount
                                                                                        </th>
                                                                                        <th class="column-title">Tax
                                                                                        </th>
                                                                                        <th class="column-title">Received
                                                                                        </th>
                                                                                        <th class="column-title">Credit
                                                                                        </th>
                                                                                        
                                                                                        <th class="column-title">Balance
                                                                                        </th>
                                                                                        <th class="bulk-actions" colspan="7">
                                                                                            <a class="antoo" style="color:#fff; font-weight:500;">Action( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                                                                                        </th>
                                                                                    </tr>
                                                                                </thead>
                                                                            </table>
                                                                            <div class="row accordian">
                                                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                                                  <?php
                                                                                        $cond20="where `ENROL_NO`=".$std_row[0]['ENROL_NO'];
                                                                                        $std_row20=getRows($cond20,'invoice');
                                                                                        foreach($std_row20 as $std_row21)
                                                                                        {                         
                                                                                         ?>
                                                                                    <div class="x_panel ">
                                                                                        <div class="x_title">
                                                                                            <ul class="nav navbar-left panel_toolbox">
                                                                                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i>Invoice<?= $std_row21['INVOICE_NO']?>&nbsp;&nbsp;Due<?= $std_row21['DUE_DATE']?></a>
                                                                                                </li>
                                                                                            </ul>
                                                                                            <div class="clearfix"></div>
                                                                                        </div>
                                                                                        <div class="x_content">
                                                                                            <div class="row">
                                                                                                <div class="col-md-12 col-sm-12">
                                                                                                    <div class="table-responsive">
                                                                                                        <table class="table table-striped jambo_table bulk_action">
                                                                                                            <thead>
                                                                                                            </thead>
                                                                                                            <tbody>
                                                                                                                <?php
                                                                                                                    $cond23="where `INVOICE_NO`=".$std_row21['INVOICE_NO'];
                                                                                                                    $std_row23=getRows($cond23,'invoice_details');
                                                                                                                    foreach($std_row23 as $std_row24)
                                                                                                                     { ?>
                                                                                                                <tr class="even pointer">
                                                                                                                    <td class=" "><?= $std_row21['INVOICE_DATE']?></td>
																													<td class=" ">Invoice s<?= $std_row21['INVOICE_NO']?></td>
                                                                                                                    <?php $cond22="where `USER_NO`=".$std_row21['USER_NO'];
                                                                                                                        $std_row22=getRows($cond22,'users');?>
                                                                                                                    <td class=" "><?= $std_row22[0]['INIT']?></td>
                                                                                                                    <?php
                                                                                                                        $cond25="where `FEE_DEF_NO`=".$std_row24['FEE_DEF_NO'];
                                                                                                                        $std_row25=getRows($cond25,'fee_defn');?> 
                                                                                                                    <td class=" "><?= $std_row25[0]['FEE_DEF_NAME']?></td>
                                                                                                                    <td class=" "><?= $std_row24['AMOUNT']?></td>
                                                                                                                    <td class=" "><?= $std_row24['TAX_AMOUNT']?></td>
                                                                                                                    <?php $cond26="where `IND_NO`=".$std_row24['IND_NO'];
                                                                                                                        $std_row26=getRows($cond26,'debit_item');?> 
                                                                                                                    
                                                                                                                    <?php $cond27="where `IND_NO`=".$std_row24['IND_NO'];
                                                                                                                        $std_row27=getRows($cond27,'credit');?> 
                                                                                                                    <?php 
                                                                                                                        $credit=$std_row24['AMOUNT']-$std_row26[0]['AMOUNT'];
                                                                                                                        ?>
                                                                                                                    <td class=" "><?php echo $credit;?></td>
                                                                                                                    <td class=" "><?= $std_row26[0]['GROSS_AMOUNT']?></td>
                                                                                                                    <?php 
                                                                                                                        $balance=$std_row24['AMOUNT']-($std_row26[0]['AMOUNT']+$credit);
                                                                                                                        ?>
                                                                                                                    <td class=" "><?php echo $balance;?></td>
                                                                                                                    
                                                                                                                </tr>
                                                                                                                <?php }?>
                                                                                                            </tbody>
                                                                                                        </table>
																										<?php  }  ?>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    
                                                                                </div> 
                                                                            </div>
                                                                        </div>
                                                                    </div>-->
                                                                </div>
                                                                <!--Leave-->
                                                                <div role="tabpanel" class="tab-pane" id="en_leave">
                                                                    <div class="table-responsive">
                                                                        <a href="#" onclick="javascript:void window.open('dashboard.php?op=<?=MD5('display_leave')?>&stu_no=<?=$std_row[0]['STUD_NO']?>&sname=<?=$std_row[0]['LNAME']?>&cust_no=<?=$std_row[0]['CUSTOMER_NO']?>&enrol_no=<?=$std_row[0]['ENROL_NO']?>','1470138621367','width=700,height=500,toolbar=0,menubar=0,location=1,status=1,scrollbars=1,resizable=1,left=0,top=0');return false;">Leave</a>
                                                                        <table id="example3" class="display table table-striped table-bordered" cellspacing="0" width="100%">
                                                                            <div class="hide-show-column dropdown" role="presentation">
                                                                                <a id="drop4" href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" role="button" aria-expanded="false">Hide / Show Column<span class="caret"></span></a>
                                                                                <ul id="menu6" class="dropdown-menu animated fadeInDown" role="menu">
                                                                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                                                        <span class="pull-left">
                                                                                        <input type="checkbox" class="flat">
                                                                                        </span>
                                                                                        <span class="pull-left">
                                                                                        Student No
                                                                                        </span>
                                                                                        </a>
                                                                                    </li>
                                                                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                                                        <span class="pull-left">
                                                                                        <input type="checkbox" class="flat">
                                                                                        </span>
                                                                                        <span class="pull-left">
                                                                                        Attention
                                                                                        </span>
                                                                                        </a>
                                                                                    </li>
                                                                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                                                        <span class="pull-left">
                                                                                        <input type="checkbox" class="flat">
                                                                                        </span>
                                                                                        <span class="pull-left">
                                                                                        First Name
                                                                                        </span>
                                                                                        </a>
                                                                                    </li>
                                                                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                                                        <span class="pull-left">
                                                                                        <input type="checkbox" class="flat">
                                                                                        </span>
                                                                                        <span class="pull-left">
                                                                                        Middle Name
                                                                                        </span>
                                                                                        </a>
                                                                                    </li>
                                                                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                                                        <span class="pull-left">
                                                                                        <input type="checkbox" class="flat">
                                                                                        </span>
                                                                                        <span class="pull-left">
                                                                                        Last Name
                                                                                        </span>
                                                                                        </a>
                                                                                    </li>
                                                                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                                                        <span class="pull-left">
                                                                                        <input type="checkbox" class="flat">
                                                                                        </span>
                                                                                        <span class="pull-left">
                                                                                        Date
                                                                                        </span>
                                                                                        </a>
                                                                                    </li>
                                                                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                                                        <span class="pull-left">
                                                                                        <input type="checkbox" class="flat">
                                                                                        </span>
                                                                                        <span class="pull-left">
                                                                                        Subjects
                                                                                        </span>
                                                                                        </a>
                                                                                    </li>
                                                                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                                                        <span class="pull-left">
                                                                                        <input type="checkbox" class="flat">
                                                                                        </span>
                                                                                        <span class="pull-left">
                                                                                        Notes
                                                                                        </span>
                                                                                        </a>
                                                                                    </li>
                                                                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                                                        <span class="pull-left">
                                                                                        <input type="checkbox" class="flat">
                                                                                        </span>
                                                                                        <span class="pull-left">
                                                                                        User
                                                                                        </span>
                                                                                        </a>
                                                                                    </li>
                                                                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                                                        <span class="pull-left">
                                                                                        <input type="checkbox" class="flat">
                                                                                        </span>
                                                                                        <span class="pull-left">
                                                                                        When
                                                                                        </span>
                                                                                        </a>
                                                                                    </li>
                                                                                </ul>
                                                                            </div>
                                                                            <thead>
                                                                                <tr class="headings">
                                                                                    <th class="column-title">Option
                                                                                    </th>
                                                                                    <th class="column-title">From 
                                                                                    </th>
                                                                                    <th class="column-title">To
                                                                                    </th>
                                                                                    <th class="column-title">Comment
                                                                                    </th>
                                                                                    <th class="column-title">Who
                                                                                    </th>
                                                                                    <th class="column-title">Created
                                                                                    </th>
                                                                                    <th class="column-title">Action
                                                                                    </th>
                                                                                </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                                <?php
                                                                                    $cond30="where `ENROL_NO`=".$std_row[0]['ENROL_NO'];
                                                                                     $std_row30=getRows($cond30,'enrol_holiday');
                                                                                     foreach($std_row30 as $std_row31)
                                                                                      {
                                                                                      
                                                                                    	  ?>
                                                                                <tr class="even pointer">
                                                                                    <td class=" "><?= $std_row31['OPTION_NO']?></td>
                                                                                    <td class=" "><?= $std_row31['FROM_DATE']?></td>
                                                                                    <td class=" "><?= $std_row31['TO_DATE']?></td>
                                                                                    <td class=" "><?= $std_row31['NAME']?></td>
                                                                                    <?php $cond32="where `USER_NO`=".$std_row31['USER_NO'];
                                                                                        $std_row32=getRows($cond32,'users'); ?>
                                                                                    <td class=" "><?= $std_row32[0]['INIT']?></td>
                                                                                    <td class=" "><?= $std_row31['CREATE_DATE']?></td>
                                                                                    <td class=" ">
                                                                                    <a href="#" onclick="javascript:void window.open('dashboard.php?op=<?=MD5('display_leave_edit')?>&e_holiday=<?=$std_row31['ENROL_HOLIDAY_NO']?>&s_no=<?=$std_row6[0]['EXT_STN']?>&s_fname=<?=$std_row6[0]['FNAME']?>&s_lname=<?=$std_row6[0]['LNAME']?>&s_mname=<?=$std_row6[0]['MNAME']?>&ag_name=<?=$std_row[0]['AGENT_NAME']?>','1470138621490','width=700,height=500,toolbar=0,menubar=0,location=1,status=1,scrollbars=1,resizable=1,left=0,top=0');return false;">Edit</a>|<a href="dashboard.php?op=<?=MD5('enrolment_details')?>&del3=<?=$std_row31['ENROL_HOLIDAY_NO']?>&eid=<?=$std_row[0]['ENROL_NO']?>" class="color-sky stting"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>Delete</a>
                                                                                    </td>
                                                                                </tr>
                                                                                <?php
                                                                                    }
                                                                                     ?>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                                <!-- Absence-->
                                                                <div role="tabpanel" class="tab-pane" id="en_absence">
                                                                    <div class="table-responsive">
                                                                        <a href="#" onclick="javascript:void window.open('dashboard.php?op=<?=MD5('display_student_absence')?>&enrol_no=<?=$std_row[0]['ENROL_NO']?>&stu_no=<?=$std_row[0]['STUD_NO']?>&sname=<?=$std_row[0]['LNAME']?>&cust_no=<?=$std_row[0]['CUSTOMER_NO']?>&cun_no=<?=$std_row[0]['COUNTRY_NO']?>','2470177621123','width=700,height=500,toolbar=0,menubar=0,location=1,status=1,scrollbars=1,resizable=1,left=0,top=0');return false;">New Absence</a></h4>
                                                                        <table id="example3" class="display table table-striped table-bordered" cellspacing="0" width="100%">
                                                                            <div class="hide-show-column dropdown" role="presentation">
                                                                                <a id="drop4" href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" role="button" aria-expanded="false">Hide / Show Column<span class="caret"></span></a>
                                                                                <ul id="menu6" class="dropdown-menu animated fadeInDown" role="menu">
                                                                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                                                        <span class="pull-left">
                                                                                        <input type="checkbox" class="flat">
                                                                                        </span>
                                                                                        <span class="pull-left">
                                                                                        Student No
                                                                                        </span>
                                                                                        </a>
                                                                                    </li>
                                                                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                                                        <span class="pull-left">
                                                                                        <input type="checkbox" class="flat">
                                                                                        </span>
                                                                                        <span class="pull-left">
                                                                                        Attention
                                                                                        </span>
                                                                                        </a>
                                                                                    </li>
                                                                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                                                        <span class="pull-left">
                                                                                        <input type="checkbox" class="flat">
                                                                                        </span>
                                                                                        <span class="pull-left">
                                                                                        First Name
                                                                                        </span>
                                                                                        </a>
                                                                                    </li>
                                                                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                                                        <span class="pull-left">
                                                                                        <input type="checkbox" class="flat">
                                                                                        </span>
                                                                                        <span class="pull-left">
                                                                                        Middle Name
                                                                                        </span>
                                                                                        </a>
                                                                                    </li>
                                                                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                                                        <span class="pull-left">
                                                                                        <input type="checkbox" class="flat">
                                                                                        </span>
                                                                                        <span class="pull-left">
                                                                                        Last Name
                                                                                        </span>
                                                                                        </a>
                                                                                    </li>
                                                                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                                                        <span class="pull-left">
                                                                                        <input type="checkbox" class="flat">
                                                                                        </span>
                                                                                        <span class="pull-left">
                                                                                        Date
                                                                                        </span>
                                                                                        </a>
                                                                                    </li>
                                                                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                                                        <span class="pull-left">
                                                                                        <input type="checkbox" class="flat">
                                                                                        </span>
                                                                                        <span class="pull-left">
                                                                                        Subjects
                                                                                        </span>
                                                                                        </a>
                                                                                    </li>
                                                                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                                                        <span class="pull-left">
                                                                                        <input type="checkbox" class="flat">
                                                                                        </span>
                                                                                        <span class="pull-left">
                                                                                        Notes
                                                                                        </span>
                                                                                        </a>
                                                                                    </li>
                                                                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                                                        <span class="pull-left">
                                                                                        <input type="checkbox" class="flat">
                                                                                        </span>
                                                                                        <span class="pull-left">
                                                                                        User
                                                                                        </span>
                                                                                        </a>
                                                                                    </li>
                                                                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                                                        <span class="pull-left">
                                                                                        <input type="checkbox" class="flat">
                                                                                        </span>
                                                                                        <span class="pull-left">
                                                                                        When
                                                                                        </span>
                                                                                        </a>
                                                                                    </li>
                                                                                </ul>
                                                                            </div>
                                                                            <thead>
                                                                                <tr class="headings">
                                                                                    <th class="column-title">Date
                                                                                    </th>
                                                                                    <th class="column-title">Hours
                                                                                    </th>
                                                                                    <th class="column-title">Reason
                                                                                    </th>
                                                                                    <th class="column-title">Counted
                                                                                    </th>
                                                                                    <th class="column-title">Who
                                                                                    </th>
                                                                                    <th class="column-title">When
                                                                                    </th>
                                                                                    <th class="column-title">Action
                                                                                    </th>
                                                                                </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                                <?php
                                                                                    $cond40="where `ENROL_NO`=".$std_row[0]['ENROL_NO'];
                                                                                    $std_row40=getRows($cond40,'absence');
                                                                                    foreach($std_row40 as $std_row41)
                                                                                    {
                                                                                     ?>
                                                                                <tr class="even pointer">
                                                                                    <td class=" "><?= $std_row41['ABSENCE_DATE']?></td>
                                                                                    <td class=" "><?= $std_row41['HOURS']?></td>
                                                                                    <td class=" "><?= $std_row41['REASON']?></td>
                                                                                    <td class=" "><?=($std_row41['COUNTED'])?'<input type="checkbox" checked class="flat">':'<input type="checkbox" class="flat">'?></td>
                                                                                    <?php $cond42="where `USER_NO`=".$std_row41['USER_NO'];
                                                                                        $std_row42=getRows($cond42,'users'); ?>
                                                                                    <td class=" "><?= $std_row42[0]['INIT']?></td>
                                                                                    <td class=" "><?= $std_row41['WHO_DATE']?></td>
                                                                                    <td class=" "><a href="#" onclick="javascript:void window.open('dashboard.php?op=<?=MD5('display_student_absence_edit')?>&absence_no=<?=$std_row41['ABSENCE_NO']?>&s_no=<?=$std_row6[0]['EXT_STN']?>&s_fname=<?=$std_row6[0]['FNAME']?>&s_lname=<?=$std_row6[0]['LNAME']?>&s_mname=<?=$std_row6[0]['MNAME']?>&ag_name=<?=$std_row[0]['AGENT_NAME']?>','1470138621490','width=700,height=500,toolbar=0,menubar=0,location=1,status=1,scrollbars=1,resizable=1,left=0,top=0');return false;">Edit</a>|<a href="dashboard.php?op=<?=MD5('enrolment_details')?>&del4=<?=$std_row41['ABSENCE_NO']?>&eid=<?=$std_row[0]['ENROL_NO']?>" class="color-sky stting"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>Delete</a></td>
                                                                                </tr>
                                                                                <?php
                                                                                    }
                                                                                     ?>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                                <!--Results-->
                                                                <div role="tabpanel" class="tab-pane" id="en_results">
                                                                    <div class="table-responsive">
                                                                        <a href="#" onclick="javascript:void window.open('dashboard.php?op=<?=MD5('new_result')?>&stu_no=<?=$std_row[0]['STUD_NO']?>&sname=<?=$std_row[0]['LNAME']?>&cust_no=<?=$std_row[0]['CUSTOMER_NO']?>&cun_no=<?=$std_row[0]['COUNTRY_NO']?>','1470138621367','width=1200,height=800,toolbar=0,menubar=0,location=1,status=1,scrollbars=1,resizable=1,left=0,top=0');return false;">New Result</a></fieldset></h4>
                                                                        <table id="example4" class="display table table-striped table-bordered" cellspacing="0" width="100%">
                                                                            <div class="hide-show-column dropdown" role="presentation">
                                                                                <a id="drop4" href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" role="button" aria-expanded="false">Hide / Show Column<span class="caret"></span></a>
                                                                                <ul id="menu6" class="dropdown-menu animated fadeInDown" role="menu">
                                                                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                                                        <span class="pull-left">
                                                                                        <input type="checkbox" class="flat">
                                                                                        </span>
                                                                                        <span class="pull-left">
                                                                                        Date
                                                                                        </span>
                                                                                        </a>
                                                                                    </li>
                                                                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                                                        <span class="pull-left">
                                                                                        <input type="checkbox" class="flat">
                                                                                        </span>
                                                                                        <span class="pull-left">
                                                                                        Form
                                                                                        </span>
                                                                                        </a>
                                                                                    </li>
                                                                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                                                        <span class="pull-left">
                                                                                        <input type="checkbox" class="flat">
                                                                                        </span>
                                                                                        <span class="pull-left">
                                                                                        To
                                                                                        </span>
                                                                                        </a>
                                                                                    </li>
                                                                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                                                        <span class="pull-left">
                                                                                        <input type="checkbox" class="flat">
                                                                                        </span>
                                                                                        <span class="pull-left">
                                                                                        Flight
                                                                                        </span>
                                                                                        </a>
                                                                                    </li>
                                                                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                                                        <span class="pull-left">
                                                                                        <input type="checkbox" class="flat">
                                                                                        </span>
                                                                                        <span class="pull-left">
                                                                                        By
                                                                                        </span>
                                                                                        </a>
                                                                                    </li>
                                                                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                                                        <span class="pull-left">
                                                                                        <input type="checkbox" class="flat">
                                                                                        </span>
                                                                                        <span class="pull-left">
                                                                                        Status
                                                                                        </span>
                                                                                        </a>
                                                                                    </li>
                                                                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                                                        <span class="pull-left">
                                                                                        <input type="checkbox" class="flat">
                                                                                        </span>
                                                                                        <span class="pull-left">
                                                                                        Notes
                                                                                        </span>
                                                                                        </a>
                                                                                    </li>
                                                                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                                                        <span class="pull-left">
                                                                                        <input type="checkbox" class="flat">
                                                                                        </span>
                                                                                        <span class="pull-left">
                                                                                        User
                                                                                        </span>
                                                                                        </a>
                                                                                    </li>
                                                                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                                                        <span class="pull-left">
                                                                                        <input type="checkbox" class="flat">
                                                                                        </span>
                                                                                        <span class="pull-left">
                                                                                        When
                                                                                        </span>
                                                                                        </a>
                                                                                    </li>
                                                                                </ul>
                                                                            </div>
                                                                            <thead>
                                                                                <tr class="headings">
                                                                                    <th class="column-title">Date
                                                                                    </th>
                                                                                    <th class="column-title">Teacher
                                                                                    </th>
                                                                                    <th class="column-title">Level
                                                                                    </th>
                                                                                    <th class="column-title">Class
                                                                                    </th>
                                                                                    <th class="column-title">Competency-Ali Test
                                                                                    </th>
                                                                                    <th class="column-title">Comments
                                                                                    </th>
                                                                                </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                                <tr class="even pointer">
                                                                                    <td class=" "></td>
                                                                                    <td class=" "></td>
                                                                                    <td class=" "></td>
                                                                                    <td class=" "></td>
                                                                                    <td class=" "></td>
                                                                                    <td class=" "></td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                                <!--Classes-->
                                                                <div role="tabpanel" class="tab-pane" id="en_classes">
                                                                    <div class="table-responsive">
                                                                        <div class="row">
                                                                            <div class="col-md-1 col-sm-1 col-xs-12">
                                                                                <div class="form-group">
                                                                                    <label class="control-label col-md-12 col-sm-12 col-xs-12" for="name">Level
                                                                                    </label>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-5 col-sm-5 col-xs-12">
                                                                                <div class="form-group">
                                                                                    <select class="control-label col-md-12 col-sm-12 col-xs-12">
                                                                                        <option>1</option>
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div role="tabpanel" class="tab-pane">
                                                                            <div class="table-responsive">
                                                                                <table id="example" class="display table table-striped table-bordered" cellspacing="0" width="100%">
                                                                                    <thead>
                                                                                        <tr class="headings">
                                                                                            <th class="column-title">Session
                                                                                            </th>
                                                                                            <th class="column-title">From
                                                                                            </th>
                                                                                            <th class="column-title">To
                                                                                            </th>
                                                                                            <th class="column-title">Class
                                                                                            </th>
                                                                                            <th class="column-title">Level
                                                                                            </th>
                                                                                            <th class="column-title">Room
                                                                                            </th>
                                                                                            <th class="column-title">Teacher
                                                                                            </th>
                                                                                        </tr>
                                                                                    </thead>
                                                                                    <tbody>
                                                                                        <tr class="even pointer">
                                                                                            <td class=" "></td>
                                                                                            <td class=" "></td>
                                                                                            <td class=" "></td>
                                                                                            <td class=" "></td>
                                                                                            <td class=" "></td>
                                                                                            <td class=" "></td>
                                                                                            <td class="last"></td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    .
                                                                </div>
                                                                <!--Course Module-->
                                                                <div role="tabpanel" class="tab-pane" id="en_course">
                                                                    <div class="table-responsive">
                                                                        <table id="example6" class="display table table-striped table-bordered" cellspacing="0" width="100%">
                                                                            <thead>
                                                                                <tr class="headings">
                                                                                    <th class="column-title">Code
                                                                                    </th>
                                                                                    <th class="column-title">Name
                                                                                    </th>
                                                                                    <th class="column-title">Finished
                                                                                    </th>
                                                                                    <th class="column-title">Outcome
                                                                                    </th>
                                                                                </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                                <tr class="even pointer">
                                                                                    <td class=" "></td>
                                                                                    <td class=" "></td>
                                                                                    <td class=" "></td>
                                                                                    <td class=" "></td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="right_col" role="main">
<?php 
$path="../".DOC_FILE_PATH;
if($_GET['del'])

{
	$file_name=getName($_GET['del'], "faculty", "id", "file");
	if(delete_record("DELETE FROM `faculty` WHERE `id` = ".$_GET['del']))
	{
		@unlink($path.$file_name);
		$_SESSION['s_msg']="Item successfully deleted";
	}
}?>
                  <div class="">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                      <div class="page-title">
                        <div class="title_left">
                          <h3>View Faculty</h3>
                        </div>
                      </div>
                    </div>

                    <div class="clearfix"></div>

                    <div class="row">
                      <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                          <div class="clearfix"></div>

                          <div class="x_content">
                            <div class="row">
                              <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel recent-app">
                                  <div class="x_content">
                                    <div class="student-account-page view-faculty-page">
                                      <!-- Nav tabs -->
                                      <div class="card">
                                        <div class="all-students-list add student">
                                          <div class="add-student-section">
                                            <div class="table-responsive">
                                              <table id="datatable" class="display table table-striped table-bordered" cellspacing="0" width="100%">
                                           
                                                <thead>
                                                  <tr class="headings">
                                                    <th class="column-title">Name
                                                    </th>
                                                    <th class="column-title">Type
                                                    </th>
                                                    <th class="column-title">Study Option
                                                    </th>
                                                    <th class="column-title">Location
                                                    </th>
                                                    <th class="column-title">Provider Code
                                                    </th>
                                                    <th class="column-title">Provider Name
                                                    </th>
                                                    <th class="column-title">Active
                                                    </th>
                                                    <th class="column-title">Action
                                                    </th>
                                                  </tr>
                                                </thead>

                                                <tbody>
                                                <?php $faculty_sql=getRows('order by `id` DESC','faculty'); 
												$sl=0;
												foreach($faculty_sql as $row)
												{?>
                                                  <tr class="even pointer">
                                                    <td class=" "><?=$row['f_name'];?></td>
                                                    <td class=" "><?=$row['type'];?></td>
                                                    <td class=" "><?=$row['study'];?></td>
                                                    <td class=" "><?=$row['location'];?></td>
                                                    <td class=" "><?=$row['pcode'];?></td>
                                                    <td class=" "><?=$row['p_name'];?></td>
                                                    <td class=" "> 
                                                    <?=($row['is_active'])?'<input type="checkbox" checked class="flat">':'<input type="checkbox" class="flat">'?>
                                                    </td>
                                                    <td class="last"><a href="dashboard.php?op=<?=MD5('faculties_edit')?>&fid=<?=$row['id'];?>" class="color-sky stting"><i class="fa fa-cog" aria-hidden="true"></i>Edit</a>| <a href="dashboard.php?op=<?=MD5('faculties')?>&del=<?=$row['id'];?>" onclick="return confirm('Are you sure?'); return false;" class="color-sky stting"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>Delete</a></td>
                                                  </tr>
                                                  <?php } ?>
                                                  
                                                  
                                                </tbody>
                                              </table>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                 </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
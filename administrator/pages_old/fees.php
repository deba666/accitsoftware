<div class="right_col" role="main">

                  <div class="">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                      <div class="page-title">
					 
                        <div class="title_left">
                          <h3>All Fees</h3>
                        </div>
                      </div>
                    </div>

                    <div class="clearfix"></div>

                    <div class="row">
                      <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                          <div class="clearfix"></div>

                          <div class="x_content">
                            <div class="row">
                              <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel recent-app">
                                  <div class="x_content">
                                    <div class="student-account-page">
                                      <!-- Nav tabs -->
                                      
                                      <div class="card">
                                                                            
                                        <div class="tab-content">
                                          <div role="tabpanel" class="tab-pane active" id="all-student">
                                            <div class="all-students-list">
                                              <div class="table-responsive">
                                                <table  class="table data-tbl-tools">
                                                  <thead>
                                                    <tr class="headings">
                                                      <th class="column-title">Code</th>
                                                      <th class="column-title">Display Name</th>
                                                      <th class="column-title">Description</th>
                                                      <th class="column-title">Type</th>
                                                      <th class="column-title">Invoice rate</th>
                                                      <th class="column-title">Purchase rate</th>
                                                      <th class="column-title">Category</th>
                                                      <th class="column-title">Faculty</th>
                                                      <th class="column-title">Location</th>
                                                      <th class="column-title">Status</th>
                                                    </tr>
                                                  </thead>
                                                  <?php $teacher=getRows('order by `fee_no` DESC','fees'); ?>
                                                  <tbody>
													 <?php
                                                        $sl=0;
                                                        foreach($teacher as $row)
                                                        {
                                                    ?>
                                                       <tr class="even pointer">
                                                          <td class=" "><?=$row['fee_name'];?> </td>
                                                          <td class=" "><?=$row['display_name'];?> </td>
                                                          <td class=" "><?=$row['fee_name'];?></td>
                                                          <?php $cond="where `FEE_DEF_NO`=".$row['fee_def_no']; $feed=getRows($cond,'fee_defn');?>
                                                          <td class=" "><?=$feed[0]['FEE_DEF_NAME'];?></td> 
                                                          <td class=" "><?=$row['rate_1'];?></td>
                                                          <td class=" "><?=$row['rate_2'];?> </td>
                                                          <td class=" "><?=$row['crt_no'];?> </td>
                                                          <?php $cond="where `CRT_NO`=".$row['crt_no']; $crt=getRows($cond,'crse_type');?>
                                                          <td class=" "><?=$crt[0]['CRT_NAME'];?></td>
                                                          <?php $cond="where `USER_NO`=".$row['user_no']; $fac=getRows($cond,'users');?>
                                                          <?php $cond="where `LOCATION_NO`=".$fac[0]['LOCATION_NO']; $loc=getRows($cond,'location');?>
                                                          <td class=" "><?=$loc[0]['LOCATION_NAME'];?></td>
                                                          <td class=" "><?=($row['status'])?'Active':'Inactive'?></td>
                                                     
                                                    </tr>
                                                     <?php   }    ?>
                                                    
                                                  </tbody>
                                                </table>
                                              </div>
                                            </div>
                                          </div>
                                          <div role="tabpanel" class="tab-pane" id="add-student">
                                            <div class="add-student-section">
                                              <form class="form-horizontal form-label-left" action="" method="post">
                                              <div class="item form-group">
                                                  <label class="control-label col-md-2 col-sm-2 col-xs-12" for="number">Student No <span class="required">*</span>
                                                  </label>
                                                  <div class="col-md-7 col-sm-7 col-xs-12">
                                                    <input id="std_no" class="form-control col-md-12 col-xs-12" data-validate-length-range="6" data-validate-words="2" name="std_no" placeholder="Student No"  type="text">
                                                    <span class="fa fa-bookmark-o form-control-feedback left" aria-hidden="true"></span>
                                                  </div>
                                                </div>
                                                <div class="item form-group">
                                                  <label class="control-label col-md-2 col-sm-2 col-xs-12" for="name">Name <span class="required">*</span>
                                                  </label>
                                                  <div class="col-md-7 col-sm-7 col-xs-12">
                                                    <div class="row">
                                                      <div class="col-md-4">
                                                       
                                                        <input id="std_fname" class="form-control col-md-12 col-xs-12" data-validate-length-range="6" data-validate-words="2" name="std_fname" placeholder="Enter First Name"  type="text">
                                                        <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                                                      </div>
                                                      <div class="col-md-4">
                                                        <input id="std_mname" class="form-control col-md-12 col-xs-12" data-validate-length-range="6" data-validate-words="2" name="std_mname" placeholder="Enter Middle Name"  type="text">
                                                        <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                                                      </div>
                                                      <div class="col-md-4">
                                                        <input id="std_lname" class="form-control col-md-12 col-xs-12" data-validate-length-range="6" data-validate-words="2" name="std_lname" placeholder="Enter Last Name"  type="text">
                                                        <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                                                      </div>
                                                    </div>
                                                  </div>
                                                </div>
                                                <div class="item form-group">
                                                  <label class="control-label col-md-2 col-sm-2 col-xs-12" for="email">Email <span class="required">*</span>
                                                  </label>
                                                  <div class="col-md-7 col-sm-7 col-xs-12">
                                                    <input type="email" id="std_email" name="std_email" required="required" placeholder="Email" class="form-control col-md-7 col-xs-12">
                                                    <span class="fa fa-envelope-o form-control-feedback left" aria-hidden="true"></span>
                                                  </div>
                                                </div>
                                                <div class="item form-group">
                                                  <label class="control-label col-md-2 col-sm-2 col-xs-12" for="textarea">Address<span class="required">*</span>
                                                  </label>
                                                  <div class="col-md-7 col-sm-7 col-xs-12">
                                                    <textarea id="std_add" required="required" name="std_add" placeholder="Address" class="form-control col-md-7 col-xs-12"></textarea>
                                                    <span class="fa fa-map-marker form-control-feedback left" aria-hidden="true"></span>
                                                  </div>
                                                </div>
                                                <div class="form-group">
                                                  <label class="control-label col-md-2 col-sm-2 col-xs-12">Pin Code<span class="required">*</span>
                                                  </label>
                                                  <div class="col-md-7 col-sm-7 col-xs-12">
                                                    <input type="text" id="std_pin" name="std_pin" required="required" placeholder="Example, 700102" class="form-control col-md-7 col-xs-12">
                                                    <span class="fa fa-thumb-tack form-control-feedback left" aria-hidden="true"></span>
                                                  </div>
                                                </div>
                                                <div class="form-group">
                                                  <label class="control-label col-md-2 col-sm-2 col-xs-12">Phone Number<span class="required">*</span>
                                                  </label>
                                                  <div class="col-md-7 col-sm-7 col-xs-12">
                                                    <input type="text" id="std_ph" name="std_ph" required="required" placeholder="Enter 10 Digits Number" class="form-control col-md-7 col-xs-12">
                                                    <span class="fa fa-phone form-control-feedback left" aria-hidden="true"></span>
                                                  </div>
                                                </div>
                                                <!--<div class="form-group">
                                                  <label class="control-label col-md-2 col-sm-2 col-xs-12">Fax<span class="required">*</span>
                                                  </label>
                                                  <div class="col-md-7 col-sm-7 col-xs-12">
                                                    <input type="text" id="text" name="text" required="required" placeholder="00012420" class="form-control col-md-7 col-xs-12">
                                                    <span class="fa fa-fax form-control-feedback left" aria-hidden="true"></span>
                                                  </div>
                                                </div>-->
                                                <div class="form-group">
                                                  <label class="control-label col-md-2 col-sm-2 col-xs-12">DOB<span class="required">*</span>
                                                  </label>
                                                  <div class="col-md-7 col-sm-7 col-xs-12">
                                                    <input id="birthday" name="birthday" class="date-picker form-control col-md-7 col-xs-12" required="required" placeholder="MM/DD/YYYY" type="text">
                                                    <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                                  </div>
                                                </div>
                                                <div class="form-group">
                                                  <label class="control-label col-md-2 col-sm-2 col-xs-12">Gender<span class="required">*</span>
                                                  </label>
                                                  <div class="col-md-7 col-sm-7 col-xs-12">
                                                    <p class="gender">Male
                                                      <input type="radio" class="flat" name="std_gen" id="std_gen" value="Male" checked="" required />Female
                                                      <input type="radio" class="flat" name="std_gen" id="std_gen" value="Female" />
                                                    </p>
                                                  </div>
                                                </div>
                                                <div class="form-group">
                                                  <label class="control-label col-md-2 col-sm-2 col-xs-12">Street Name<span class="required">*</span>
                                                  </label>
                                                  <div class="col-md-7 col-sm-7 col-xs-12">
                                                    <input type="text" id="std_str_add" name="std_str_add" required="required" placeholder="Street Name" class="form-control col-md-7 col-xs-12">
                                                    <span class="fa fa-phone form-control-feedback left" aria-hidden="true"></span>
                                                  </div>
                                                </div>
                                                <div class="form-group">
                                                  <label class="control-label col-md-2 col-sm-2 col-xs-12">Country<span class="required">*</span>
                                                  </label>
                                                  <div class="col-md-7 col-sm-7 col-xs-12">
                                                    <select name="std_country" id="std_country" class="form-control">
                                                      <option>Australia</option>
                                                     <!-- <option>United Kingdom</option>
                                                      <option>India</option>
                                                      <option>South Africa</option>
                                                      <option>Malaysia</option>-->
                                                    </select>
                                                    <span class="fa fa-map form-control-feedback left" aria-hidden="true"></span>
                                                  </div>
                                                </div>
                                                <div class="form-group">
                                                  <label class="control-label col-md-2 col-sm-2 col-xs-12">State<span class="required">*</span>
                                                  </label>
                                                  <div class="col-md-7 col-sm-7 col-xs-12">
                                                    <select id="std_state" name="std_state" class="form-control">
                                                      <option>Sydeny</option>
                                                      <!--<option>2</option>
                                                      <option>3</option>
                                                      <option>4</option>
                                                      <option>5</option>-->
                                                    </select>
                                                    <span class="fa fa-map form-control-feedback left" aria-hidden="true"></span>
                                                  </div>
                                                </div>
                                                <div class="form-group">
                                                  <label class="control-label col-md-2 col-sm-2 col-xs-12">Citizen Country Name<span class="required">*</span>
                                                  </label>
                                                  <div class="col-md-7 col-sm-7 col-xs-12">
                                                    <select id="std_citizen" name="std_citizen" class="form-control">
                                                      <option>Sydeny</option>
                                                     <!-- <option>2</option>
                                                      <option>3</option>
                                                      <option>4</option>
                                                      <option>5</option>-->
                                                    </select>
                                                    <span class="fa fa-map form-control-feedback left" aria-hidden="true"></span>
                                                  </div>
                                                </div>
                                                <div class="form-group">
                                                  <label class="control-label col-md-2 col-sm-2 col-xs-12">Birth Country Name<span class="required">*</span>
                                                  </label>
                                                  <div class="col-md-7 col-sm-7 col-xs-12">
                                                    <select id="std_birth_country" name="std_birth_country" class="form-control">
                                                      <option>Sydeny</option>
                                                     <!-- <option>2</option>
                                                      <option>3</option>
                                                      <option>4</option>
                                                      <option>5</option>-->
                                                    </select>
                                                    <span class="fa fa-map form-control-feedback left" aria-hidden="true"></span>
                                                  </div>
                                                </div>
                                                <div class="form-group">
                                                  <label class="control-label col-md-2 col-sm-2 col-xs-12">Subject<span class="required">*</span>
                                                  </label>
                                                  <div class="col-md-7 col-sm-7 col-xs-12">
                                                    <select id="std_subject" name="std_subject" class="form-control">
                                                      <option>Select Subject</option>
                                                      <option>2</option>
                                                      <option>3</option>
                                                      <option>4</option>
                                                      <option>5</option>
                                                    </select>
                                                    <span class="fa fa-book form-control-feedback left" aria-hidden="true"></span>
                                                  </div>
                                                </div>
                                                <div class="col-md-7 col-sm-7 col-xs-12 col-md-offset-2">
                                                  <button type="submit" class="btn btn-primary">Cancel</button>
                                                  <button type="submit" class="btn btn-success">Submit</button>
                                                </div>
                                              </form>
                                            </div>
                                          </div>
                                          <div role="tabpanel" class="tab-pane" id="student-account">
                                            <div class="students-acc">
                                              <div class="table-responsive">
                                                <table id="datatable" class="table table-striped jambo_table bulk_action">
                                                  <div class="hide-show-column dropdown" role="presentation">
                                                  <a id="drop4" href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" role="button" aria-expanded="false">Hide / Show Column<span class="caret"></span></a>
                                                  <ul id="menu6" class="dropdown-menu animated fadeInDown" role="menu">
                                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                      <span class="pull-left">
                                                        <input type="checkbox" class="flat">
                                                      </span>
                                                      <span class="pull-left">
                                                        Receipt No
                                                      </span>
                                                    </a>
                                                    </li>
                                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                      <span class="pull-left">
                                                        <input type="checkbox" class="flat">
                                                      </span>
                                                      <span class="pull-left">
                                                        Receipt Date
                                                      </span>
                                                    </a>
                                                    </li>
                                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                      <span class="pull-left">
                                                        <input type="checkbox" class="flat">
                                                      </span>
                                                      <span class="pull-left">
                                                        Reference No
                                                      </span>
                                                    </a>
                                                    </li>
                                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                      <span class="pull-left">
                                                        <input type="checkbox" class="flat">
                                                      </span>
                                                      <span class="pull-left">
                                                        Details
                                                      </span>
                                                    </a>
                                                    </li>
                                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                      <span class="pull-left">
                                                        <input type="checkbox" class="flat">
                                                      </span>
                                                      <span class="pull-left">
                                                        Amount
                                                      </span>
                                                    </a>
                                                    </li>
                                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                      <span class="pull-left">
                                                        <input type="checkbox" class="flat">
                                                      </span>
                                                      <span class="pull-left">
                                                        Student No
                                                      </span>
                                                    </a>
                                                    </li>
                                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                      <span class="pull-left">
                                                        <input type="checkbox" class="flat">
                                                      </span>
                                                      <span class="pull-left">
                                                        First Name
                                                      </span>
                                                    </a>
                                                    </li>
                                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                      <span class="pull-left">
                                                        <input type="checkbox" class="flat">
                                                      </span>
                                                      <span class="pull-left">
                                                        Middle Name
                                                      </span>
                                                    </a>
                                                    </li>
                                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                      <span class="pull-left">
                                                        <input type="checkbox" class="flat">
                                                      </span>
                                                      <span class="pull-left">
                                                        Middle Name
                                                      </span>
                                                    </a>
                                                    </li>
                                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                      <span class="pull-left">
                                                        <input type="checkbox" class="flat">
                                                      </span>
                                                      <span class="pull-left">
                                                        Last Name
                                                      </span>
                                                    </a>
                                                    </li>
                                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                      <span class="pull-left">
                                                        <input type="checkbox" class="flat">
                                                      </span>
                                                      <span class="pull-left">
                                                        User
                                                      </span>
                                                    </a>
                                                    </li>
                                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                      <span class="pull-left">
                                                        <input type="checkbox" class="flat">
                                                      </span>
                                                      <span class="pull-left">
                                                        Enrol No
                                                      </span>
                                                    </a>
                                                    </li>
                                                  </ul>
                                                </div>                                                
                                                  <thead>
                                                  <tr class="headings">
                                                    <th class="column-title">Name
                                                      <input id="name" class="form-control col-md-12 col-xs-12" name="search" placeholder="Search" type="text">
                                                    </th>
                                                    <th class="column-title">Email
                                                      <input id="name" class="form-control col-md-12 col-xs-12" name="search" placeholder="Search" type="text">
                                                    </th>
                                                    <th class="column-title">Payment Ammount
                                                      <input id="name" class="form-control col-md-12 col-xs-12" name="search" placeholder="Search" type="text">
                                                    </th>
                                                    <th class="column-title">Transaction ID
                                                      <input id="name" class="form-control col-md-12 col-xs-12" name="search" placeholder="Search" type="text">
                                                    </th>
                                                    <th class="column-title">Bank Name
                                                      <input id="name" class="form-control col-md-12 col-xs-12" name="search" placeholder="Search" type="text">
                                                    </th>
                                                    <th class="column-title no-link last"><span class="nobr">Action</span>
                                                    </th>
                                                  </tr>
                                                  </thead>

                                                  <tbody>
                                                   <?php
                                                        $sl=0;
                                                        foreach($std_row as $row)
                                                        {
                                                    ?>
                                                  <tr class="even pointer">
                                                    <td class=" "><?=$row['std_fname'];?> <?=$row['std_lname'];?></td>
                                                    <td class=" "><?=$row['std_email'];?></td>
                                                    <td class=" ">7000,000/-</td>
                                                    <td class=" ">XXXXXXXX20156</td>
                                                    <td class="">SBI</td>
                                                    <td class="last"><a href="#" class="color-sky stting"><i class="fa fa-eye" aria-hidden="true"></i>View Details</a>
                                                    </td>
                                                  </tr>
                                                  <?php   }    ?>
                                                  </tbody>
                                                </table>
                                              </div>
                                            </div>

                                            <div class="clearfix"></div>

                                              <!--<div class="row">
                                                <div class="col-sm-5">
                                                    <div class="dataTables_info" id="datatable_info" role="status" aria-live="polite">Showing 1 to 10 of 57 entries</div>
                                                </div>
                                                <div class="col-sm-7">
                                                    <div class="dataTables_paginate paging_simple_numbers" id="datatable_paginate">
                                                        <ul class="pagination">
                                                            <li class="paginate_button previous disabled" id="datatable_previous"><a href="#" aria-controls="datatable" data-dt-idx="0" tabindex="0">Previous</a></li>
                                                            <li class="paginate_button active"><a href="#" aria-controls="datatable" data-dt-idx="1" tabindex="0">1</a></li>
                                                            <li class="paginate_button "><a href="#" aria-controls="datatable" data-dt-idx="2" tabindex="0">2</a></li>
                                                            <li class="paginate_button "><a href="#" aria-controls="datatable" data-dt-idx="3" tabindex="0">3</a></li>
                                                            <li class="paginate_button "><a href="#" aria-controls="datatable" data-dt-idx="4" tabindex="0">4</a></li>
                                                            <li class="paginate_button "><a href="#" aria-controls="datatable" data-dt-idx="5" tabindex="0">5</a></li>
                                                            <li class="paginate_button "><a href="#" aria-controls="datatable" data-dt-idx="6" tabindex="0">6</a></li>
                                                            <li class="paginate_button next" id="datatable_next"><a href="#" aria-controls="datatable" data-dt-idx="7" tabindex="0">Next</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                              </div>-->
                                            
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                 </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                
<div class="right_col" role="main">
<?php 
if($_GET['del'])
{
	if(delete_record("DELETE FROM `crse_type` WHERE `CRT_NO` = ".$_GET['del']))
	{
		$_SESSION['s_msg']="Item successfully deleted";
	}
}?>
                  <div class="">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                      <div class="page-title">
                        <div class="title_left">
                          <?php 
                                    if($_SESSION['s_msg'] || $_SESSION['e_msg'])
                                    {
										?>
                                            <div class="alert <?=($_SESSION['s_msg'])?'alert-success':'alert-error'?> fade in">
                                                <button data-dismiss="alert" class="close" type="button">×</button>
                                                <?=($_SESSION['s_msg'])?$_SESSION['s_msg']:$_SESSION['e_msg']?>
                                            </div>
										<?php
                                    	unset($_SESSION['s_msg']);
                                    unset($_SESSION['e_msg']);
                                    }
                                ?>
                          <h3>View Faculty</h3>
                        </div>
                      </div>
                    </div>

                    <div class="clearfix"></div>

                    <div class="row">
                      <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                          <div class="clearfix"></div>

                          <div class="x_content">
                            <div class="row">
                              <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel recent-app">
                                  <div class="x_content">
                                    <div class="student-account-page view-faculty-page">
                                      <!-- Nav tabs -->
                                      <div class="card">
                                        <div class="all-students-list add student">
                                          <div class="add-student-section">
                                            <div class="table-responsive">
                                              <table id="datatable" class="display table table-striped table-bordered" cellspacing="0" width="100%">
                                           
                                                <thead>
                                                  <tr class="headings">
                                                    <th class="column-title">Name
                                                    </th>
                                                    <th class="column-title">Type
                                                    </th>
                                                    <th class="column-title">Study Option
                                                    </th>
                                                    <th class="column-title">Location
                                                    </th>
                                                    <th class="column-title">Active
                                                    </th>
                                                    <th class="column-title">Provider Code
                                                    </th>
                                                    <th class="column-title">Provider Name
                                                    </th>
                                                    <th class="column-title">Action
                                                    </th>
                                                  </tr>
                                                </thead>
                                                <tbody>
                                                <?php $faculty_sql=getRows('order by `CRT_NO` DESC','crse_type'); 
												$sl=0;
												foreach($faculty_sql as $row)
												{?>
                                                  <tr class="even pointer">
                                                    <td class=" "><?=$row['CRT_NAME'];?></td>
                                                    <td class=" ">
													 <?php
															$crt = $row['CRT_TYPE'];
															
															switch ($crt) {
																case "1":
																	echo "Language";
																	break;
																case "2":
																	echo "Tour Group";
																	break;
																case "3":
																	echo "Module Lession";
																	break;
																	case "4":
																	echo "All";
																	break;
																default:
																	echo "None";
															}
															?> 
													
													</td>
                                                    <td class=" ">
													 <?php
															$week = $row['COURSE_STUDY_TYPE'];
															
															switch ($week) {
																case "0":
																	echo "Calander Week";
																	break;
																case "1":
																	echo "Semester";
																	break;
																default:
																	echo "None";
															}
															?>
													</td>
                                                     <?php $cond="where `LOCATION_NO`=".$row['LOCATION_NO']; $loc=getRows($cond,'location');?>
                                                      <td class=" "><?=$loc[0]['LOCATION_NAME'];?> </td>
                                                    <td class=" "><?=($row['STATUS'])?'<input type="checkbox" checked class="flat">':'<input type="checkbox" class="flat">'?></td>
                                                    <td class=" "><?=$row['PROVIDER_CODE'];?></td>
                                                    <td class=" "><?=$row['PROVIDER_NAME'];?></td>
                                                    <td class="last"><a href="dashboard.php?op=<?=MD5('faculties_edit')?>&fid=<?=$row['CRT_NO'];?>" class="color-sky stting"><i class="fa fa-cog" aria-hidden="true"></i>Edit</a>| <a href="dashboard.php?op=<?=MD5('faculties')?>&del=<?=$row['CRT_NO'];?>" onclick="return confirm('Are you sure?'); return false;" class="color-sky stting"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>Delete</a></td>
                                                  </tr>
                                                  <?php } ?>                                                  
                                                </tbody>
                                              </table>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                 </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
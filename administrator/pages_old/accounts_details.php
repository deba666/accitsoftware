<?php 
if($_GET['sid'])
{
	$cond="where `std_no`=".$_GET['sid'];
	$std_row=getRows($cond,'student');
	$cond2="where `std_no`=".$_GET['sid'];
	$std_row2=getRows($cond2,'student_address');
	$cond3="where `std_no`=".$_GET['sid'];
	$std_row3=getRows($cond3,'accounts');
	
}
?>

<div role="main" class="right_col" style="min-height: 782px;">

                  <div class="">
                    <div class="page-title">
                      <div class="title_left">
                        <h3>Student Account Details</h3>
                      </div>
                    </div>

                    <div class="clearfix"></div>

                    <div class="row">
                      <div class="col-md-12">
                        <div class="x_panel">
                          <div class="clearfix"></div>

                          <div class="x_content">
                            <div class="row">
                              <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel recent-app">
                                  <div class="x_content">
                                    <div class="student-details-page">
                                      <div class="col-md-3 col-sm-3 col-xs-12">
                                        <div class="profile-left">
                                          <div class="upload-pic">
                                            <form>
                                              <div class="student-profile_pic">
                                                  <img class="img-circle profile_img" alt="..." src="images/student2.png">
                                                  <div class="fileUpload">
                                                    <span><i aria-hidden="true" class="fa fa-pencil"></i></span>
                                                    <input type="file" class="upload">
                                                </div>
                                              </div>
                                            </form>
                                          </div>
                                          <div class="table-responsive">
                                            <table class="table table-striped jambo_table">
                                              <tbody>
                                                <tr class="even pointer">
                                                  <td class="item">Student Id</td>
                                                  <td class=""><?=$std_row[0]['std_no']?></td>
                                                </tr>
                                                <tr class="even pointer">
                                                  <td class="item">Name</td>
                                                  <td class=""><?=$std_row[0]['std_fname']?> <?=$std_row[0]['std_mname']?> <?=$std_row[0]['std_lname']?></td>
                                                </tr>
                                                <tr class="even pointer">
                                                  <td class="item">Course</td>
                                                  <td class="">GeoGraphy</td>
                                                </tr>
                                                <tr class="even pointer">
                                                  <td class="item">Batch</td>
                                                  <td class="">2013</td>
                                                </tr>
                                                <tr class="even pointer">
                                                  <td class="item">Email Id</td>
                                                  <td class=""><?=$std_row[0]['std_email']?></td>
                                                </tr>
                                                <tr class="even pointer">
                                                  <td class="item">Mobile No</td>
                                                  <td class=""> <?=$std_row[0]['std_ph']?></td>
                                                </tr>
                                              </tbody>
                                            </table>
                                          </div>
                                        </div>
                                      </div>

                                      <div class="col-md-9 col-sm-9 col-xs-12">
                                        <div class="profile-right">
                                          <div class="card">
                                            <ul role="tablist" class="nav nav-tabs">
                                             <!-- <li class="active" role="presentation"><a aria-expanded="true" data-toggle="tab" role="tab" aria-controls="personal" href="#personal"><i aria-hidden="true" class="fa fa-street-view"></i>Personal</a></li>
                                              <li class="" role="presentation"><a aria-expanded="false" data-toggle="tab" role="tab" aria-controls="academic" href="#academic"><i aria-hidden="true" class="fa fa-graduation-cap"></i>Academic</a></li>
                                              <li class="" role="presentation"><a aria-expanded="false" data-toggle="tab" role="tab" aria-controls="address" href="#address"><i aria-hidden="true" class="fa fa-home"></i>Address</a></li>
                                              <li class="" role="presentation"><a aria-expanded="false" data-toggle="tab" role="tab" aria-controls="documents" href="#documents"><i aria-hidden="true" class="fa fa-file-text"></i>Documents</a></li>
												-->

                                              <li class="" role="presentation"><a aria-expanded="false" data-toggle="tab" role="tab" aria-controls="fees" href="#fees"><i aria-hidden="true" class="fa fa-inr"></i>Fees</a></li>
                                            </ul>

											<!-- Other Informations -->
                                          <!--

											<div class="tab-content">
                                              <div id="personal" class="tab-pane active" role="tabpanel">

                                                <div class="personal-info-tab">
                                                  <div class="edit-section">
                                                    <h4><i aria-hidden="true" class="fa fa-info-circle"></i>Personal Details</h4>
                                                    <span class="pull-right">
                                                      <button class="btn btn-success edit" type="submit">
													  <i aria-hidden="true" class="fa fa-pencil"></i>
													  <td><a href="dashboard.php?op=<?=MD5('student_edit')?>&sid=<?=$std_row[0]['std_no'];?>">edit</a></td></button>
                                                    </span>
                                                  </div>

                                                  <div class="clearfix"></div>

                                                  <div class="table-responsive">
                                                    <table class="table table-striped jambo_table">
                                                      <tbody>
                                                        <tr class="even pointer">
                                                          <td class="item">Title</td>
                                                          <td class=""><?=$std_row[0]['std_title']?></td>
                                                        </tr>
                                                        <tr class="even pointer">
                                                          <td class="item">First Name</td>
                                                          <td class=""><?=$std_row[0]['std_fname']?></td>
                                                        </tr>
                                                        <tr class="even pointer">
                                                          <td class="item">Middle Name</td>
                                                          <td class=""> <?=$std_row[0]['std_mname']?></td>
                                                        </tr>
                                                        <tr class="even pointer">
                                                          <td class="item">Last Name</td>
                                                          <td class=""><?=$std_row[0]['std_lname']?></td>
                                                        </tr>
                                                        <tr class="even pointer">
                                                          <td class="item">Date of Birth</td>
                                                          <td class=""><?=$std_row[0]['birthday']?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Age: <?php $dob = $std_row[0]['birthday']; 
														  echo ageCalculator($dob);?></td>
                                                        </tr>
                                                        <tr class="even pointer">
                                                          <td class="item">Birth Country Name</td>
                                                          <td class="">India</td>
                                                        </tr>
                                                        <tr class="even pointer">
                                                          <td class="item">Gender</td>
                                                          <td class="">Male</td>
                                                        </tr>
                                                        <tr class="even pointer">
                                                          <td class="item">Nationality</td>
                                                          <td class="">Indian</td>
                                                        </tr>
                                                        <tr class="even pointer">
                                                          <td class="item">Citizenship</td>
                                                          <td class=""><?=$std_row[0]['std_citizen']?></td>
                                                        </tr>
                                                        <tr class="even pointer">
                                                          <td class="item">Bloodgroup</td>
                                                          <td class="">B+</td>
                                                        </tr>
                                                        <tr class="even pointer">
                                                          <td class="item">Language</td>
                                                          <td class=""><?=$std_row[0]['std_languge']?></td>
                                                        </tr>
                                                      </tbody>
                                                    </table>
                                                  </div>
                                                </div>
                                              </div>
                                              <div id="academic" class="tab-pane" role="tabpanel">
                                                <div class="academic-tab">
                                                  <div class="edit-section">
                                                    <h4><i aria-hidden="true" class="fa fa-info-circle"></i>Academic Details</h4>
                                                    <span class="pull-right">
                                                      <button class="btn btn-success edit" type="submit"><i aria-hidden="true" class="fa fa-pencil"></i>Edit</button>
                                                    </span>
                                                  </div>

                                                  <div class="clearfix"></div>

                                                  <div class="table-responsive">
                                                    <table class="table table-striped jambo_table">
                                                      <tbody>
                                                        <tr class="even pointer">
                                                          <td class="item">Examination</td>
                                                          <td class="">HS</td>
                                                        </tr>
                                                        <tr class="even pointer">
                                                          <td class="item">Board</td>
                                                          <td class="">WBBHSE</td>
                                                        </tr>
                                                        <tr class="even pointer">
                                                          <td class="item">Percentage</td>
                                                          <td class=""> 79%</td>
                                                        </tr>
                                                        <tr class="even pointer">
                                                          <td class="item">Year of Passing</td>
                                                          <td class="">2017</td>
                                                        </tr>
                                                        <tr class="even pointer">
                                                          <td class="item">Course</td>
                                                          <td class="">Diploma</td>
                                                        </tr>
                                                      </tbody>
                                                    </table>
                                                  </div>
                                                </div>
                                              </div>

                                              <div id="address" class="tab-pane" role="tabpanel">
                                                <div class="academic-tab">
                                                  <div class="edit-section">
                                                    <h4><i aria-hidden="true" class="fa fa-info-circle"></i>Address</h4>
                                                    <span class="pull-right">
                                                      <button class="btn btn-success edit" type="submit"><i aria-hidden="true" class="fa fa-pencil"></i>Edit</button>
                                                    </span>
                                                  </div>

                                                  <div class="clearfix"></div>

                                                  <div class="table-responsive">
                                                    <table class="table table-striped jambo_table">
                                                      <tbody>
                                                        <tr class="even pointer">
                                                          <td class="item">Address1</td>
                                                          <td class=""><?=$std_row2[0]['std_loc_addr1']?></td>
                                                        </tr>
                                                        <tr class="even pointer">
                                                          <td class="item">Address2</td>
                                                          <td class=""><?=$std_row2[0]['std_loc_addr2']?></td>
                                                        </tr>
                                                        <tr class="even pointer">
                                                          <td class="item">Address3</td>
                                                          <td class=""><?=$std_row2[0]['std_loc_addr3']?></td>
                                                        </tr>
                                                      </tbody>
                                                    </table>
                                                  </div>
                                                </div>
                                              </div>
                                              <div id="documents" class="tab-pane" role="tabpanel">
                                                <div class="academic-tab">
                                                  <div class="edit-section">
                                                    <h4><i aria-hidden="true" class="fa fa-info-circle"></i>Documents</h4>
                                                    <span class="pull-right">
                                                      <button class="btn btn-success edit" type="submit"><i aria-hidden="true" class="fa fa-pencil"></i>Edit</button>
                                                    </span>
                                                  </div>

                                                  <div class="clearfix"></div>

                                                  <div class="table-responsive">
                                                    <table class="table table-striped jambo_table">
                                                      <tbody>
                                                        <tr class="even pointer">
                                                          <td class="item">Document No</td>
                                                          <td class="">8002</td>
                                                        </tr>
                                                        <tr class="even pointer">
                                                          <td class="item">Name</td>
                                                          <td class="">Sk Rafael</td>
                                                        </tr>
                                                        <tr class="even pointer">
                                                          <td class="item">Description</td>
                                                          <td class=""></td>
                                                        </tr>
                                                        <tr class="even pointer">
                                                          <td class="item">Category No</td>
                                                          <td class="">00215</td>
                                                        </tr>
                                                        <tr class="even pointer">
                                                          <td class="item">Customer No</td>
                                                          <td class="">00215263</td>
                                                        </tr>
                                                        <tr class="even pointer">
                                                          <td class="item">Submission Date</td>
                                                          <td class="">30/02/2011</td>
                                                        </tr>
                                                      </tbody>
                                                    </table>
                                                  </div>
                                                </div>
                                              </div>
                                              <div id="fees" class="tab-pane" role="tabpanel">
                                                <div class="academic-tab">
                                                  <div class="edit-section">
                                                    <h4><i aria-hidden="true" class="fa fa-info-circle"></i>Fees</h4>
                                                    <span class="pull-right">
                                                      <button class="btn btn-success edit" type="submit"><i aria-hidden="true" class="fa fa-pencil"></i>Edit</button>
                                                    </span>
                                                  </div> -->

												  <?php
												  $sql="select * from accounts";
												  $q=mysql_query($sql);
												  $row=mysql_fetch_array($q);

												  ?>

                                                  <div class="clearfix"></div>

                                                  <div class="table-responsive">
                                                    <table class="table table-striped jambo_table">
                                                      <tbody>
                                                        <tr class="even pointer">
                                                          <td class="item">Student No</td>
                                                            <td class=""><?=$std_row3[0]['std_no']?></td>
                                                        </tr>
														<tr class="even pointer">
                                                          <td class="item">Receipt No</td>
                                                            <td class=""><?=$std_row3[0]['acc_id']?></td>
                                                        </tr>
                                                        <tr class="even pointer">
                                                          <td class="item">Student Name</td>
															 <td class=""><?=$std_row3[0]['name']?></td>                                                     
															</tr>
                                                        <tr class="even pointer">
                                                          <td class="item">Status</td>
                                                           <td class=""><?=$std_row3[0]['status']?></td>
                                                        </tr>
                                                        <tr class="even pointer">
                                                          <td class="item">Payment Amount</td>
                                                          <td class=""><?=$std_row3[0]['payment_amount']?></td>
                                                        </tr>
                                                        <tr class="even pointer">
                                                          <td class="item">Due Amount</td>
                                                          <td class=""><?=$std_row3[0]['due_amount']?></td>
                                                        </tr>
                                                      </tbody>
                                                    </table>
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                 </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
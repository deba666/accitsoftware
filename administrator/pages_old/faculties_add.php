<?php

if($_POST['company'])
{
			 if(insert_record("insert into `crse_type` set `COMPANY_NO`='".$_POST['company']."', `LOCATION_NO`='".$_POST['location']."', `CRT_TYPE`='".$_POST['type']."', `CRT_NAME`='".$_POST['f_name']."', `COURSE_STUDY_TYPE`='".$_POST['study']."', `HRS_WEEK`='".$_POST['quantity']."', `PROVIDER_CODE`='".$_POST['pcode']."', `VET_FLAG`='".$_POST['e_avet']."', `PROVIDER_NAME`='".$_POST['p_name']."', `STATUS`='".$_POST['is_active']."',`USER_NO`='".$_POST['user']."', `WHO_DATE`='".$_POST['time']."'"))
$_SESSION['s_msg']="<strong>Well done!</strong> Faculty Added";
else
$_SESSION['e_msg']="<strong>Oh snap!</strong> Faculty not added";
}
?>

<div class="right_col" role="main">
                  <div class="">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                      <div class="page-title">
                        <div class="title_left">
                        <?php 
                                    if($_SESSION['s_msg'] || $_SESSION['e_msg'])
                                    {
										?>
                                            <div class="alert <?=($_SESSION['s_msg'])?'alert-success':'alert-error'?> fade in">
                                                <button data-dismiss="alert" class="close" type="button">×</button>
                                                <?=($_SESSION['s_msg'])?$_SESSION['s_msg']:$_SESSION['e_msg']?>
                                            </div>
										<?php
                                    	unset($_SESSION['s_msg']);
                                    unset($_SESSION['e_msg']);
                                    }
                                ?>
                          <h3>Add Faculty</h3>
                        </div>
                      </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="row">
                      <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                          <div class="clearfix"></div>
                          <div class="x_content">
                            <div class="row">
                              <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel recent-app">
                                  <div class="x_content">
                                    <div class="student-account-page edit-faculty-page">
                                      <!-- Nav tabs -->
                                      <div class="card">
                                        <div class="all-students-list add student">
                                          <div class="add-student-section">
                                            <form class="form-horizontal form-label-left" action="" method="post" enctype="multipart/form-data">
                                            <input type="hidden" value="<?=$_SESSION['user_no']?>" name="user"/>
                                            <input type="hidden" value="<?php date_default_timezone_set('Australia/sydney');
echo $date = date('d/m/Y h:i:s A', time());?>" name="time"/>
                                              <div class="item form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Company</label>
                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                  <div class="row">
                                                    <div class="col-md-7 col-sm-7 col-xs-12">
                                                      <select name="company" id="company" class="form-control">
                                                        <option value="1">Austrilan College of Commerce and Information Technology</option>
                                                        <option value="2">ACCIT</option>
                                                      </select>
                                                    </div>
                                                  </div>
                                                </div>
                                              </div>
                                              <div class="item form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Location</label>
                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                  <div class="row">
                                                    <div class="col-md-6 col-sm-4 col-xs-12">
                                                      <select id="location" name="location" class="form-control" >
                                                        <option value="1">Sydney</option>
                                                        <option value="2">Kolkata</option>
                                                      </select>
                                                    </div>
                                                    <div class="col-md-6 col-sm-4 col-xs-12">
                                                      <label class="pull-left">
                                                      
                                                        <input type="hidden" name="is_active" value="0" />
                                                        <input value="1" id="is_active" name="is_active" type="checkbox" class="flat">
                                                        Active
                                                      </label>

                                                    </div>
                                                  </div>
                                                </div>
                                              </div>
                                              <div class="item form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Type</label>
                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                  <div class="row">
                                                    <div class="col-md-7 col-sm-7 col-xs-12">
                                                      <select id="type" name="type" class="form-control">
                                                        <option value="1">Language</option>
                                                        <option value="2">Tour Group</option>
                                                        <option value="3">Module Lession</option>
                                                        <option value="4">All</option>
                                                      </select>
                                                    </div>
                                                  </div>
                                                </div>
                                              </div>
                                              <div class="item form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Name</label>
                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                  <div class="row">
                                                    <div class="col-md-7 col-sm-7 col-xs-12">
                                                       <input id="f_name" class="form-control col-md-12 col-xs-12" name="f_name" placeholder="Faculties Name" type="text">
                                                    </div>
                                                  </div>
                                                </div>
                                              </div>
                                              <div class="item form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Study Option</label>
                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                  <div class="row">
                                                    <div class="col-md-7 col-sm-7 col-xs-12">
                                                      <select id="study" name="study" class="form-control">
                                                        <option value="0">Calander Week</option>
                                                        <option value="1">Semester</option>
                                                      </select>
                                                    </div>
                                                  </div>
                                                </div>
                                              </div>
                                              <div class="item form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Hours Per Week</label>
                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                  <div class="row">
                                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                                      <input type="number" step="1" min="1" max="" name="quantity" id="quantity" value="1" title="Qty" class="form-control input-text qty text" size="4">
                                                    </div>
                                                  </div>
                                                </div>
                                              </div>
                                              <div class="item form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Provider Code</label>
                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                  <div class="row">
                                                    <div class="col-md-6 col-sm-4 col-xs-12">
                                                      <input id="pcode" class="form-control col-md-12 col-xs-12" name="pcode" placeholder="02442B" type="text">
                                                    </div>
                                                    <div class="col-md-6 col-sm-4 col-xs-12">
                                                      <label class="pull-left">
                                                        <input type="hidden" name="e_avet" value="0" />
                                                        <input name="e_avet" id="e_avet" value="1" type="checkbox" class="flat">
                                                        
                                                        Enable AVETMISS
                                                      </label>

                                                    </div>
                                                  </div>
                                                </div>
                                              </div>
                                              <div class="item form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Provider Name</label>
                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                  <div class="row">
                                                    <div class="col-md-6 col-sm-4 col-xs-12">
                                                      <input id="p_name" class="form-control col-md-12 col-xs-12" name="p_name" placeholder="" type="text">
                                                    </div>
                                                  </div>
                                                </div>
                                              </div>
                                              <!--<div class="item form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Web Offer Template</label>
                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                  <div class="row">
                                                    <div class="col-md-6 col-sm-4 col-xs-12">
                                                      <input type="file" id="file" name="file" title="Upload File">
                                                    </div>
                                                  </div>
                                                </div>
                                              </div>-->
                                              
                                              <div class="item form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name"></label>
                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                  <div class="row">
                                                    <div class="col-md-6 col-sm-4 col-xs-12">
                                                      <p class="pull-left">
                                                        <button type="reset" value="Reset"  class="btn btn-primary">Cancel</button>
                                                        <button type="submit" class="btn btn-success">Submit</button>
                                                      </p>
                                                    </div>
                                                  </div>
                                                </div>
                                              </div>
                                            </form>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                 </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
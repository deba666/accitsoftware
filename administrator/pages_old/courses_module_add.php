<?php
if($_POST['code'])
	{
	if(insert_record("insert into `subject` set `SUBJ_CODE`='".$_POST['code']."',`STATUS`='".$_POST['status']."',`SUBJ_DESC`='".$_POST['name']."',`HOURS`='".$_POST['hours']."',`VET_CODE`='".$_POST['sub_id']."',`VET_COMPETENCY_NO`='".$_POST['package']."',`VET_FIELD_NO`='".$_POST['program']."',`VET_NATIONAL_CODE`='".$_POST['intension']."',`VET_DELIVERY_MODE_NO`='".$_POST['mode']."',`VET_NAME`='".$_POST['foe']."',`WHO_DATE`='".$_POST['who']."',`USER_NO`='".$_POST['user']."'"))
		$_SESSION['s_msg']="<strong>Fine!</strong> module Course Added.";
		else{
			$_SESSION['e_msg']="<strong>Oh snap!</strong> module course not added";
		}
	}
?>

<div class="right_col" role="main">

                  <div class="">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                      <div class="page-title">
                        <div class="title_left">
                        <?php 
                                    if($_SESSION['s_msg'] || $_SESSION['e_msg'])
                                    {
										?>
                                            <div class="alert <?=($_SESSION['s_msg'])?'alert-success':'alert-error'?> fade in">
                                                <button data-dismiss="alert" class="close" type="button">×</button>
                                                <?=($_SESSION['s_msg'])?$_SESSION['s_msg']:$_SESSION['e_msg']?>
                                            </div>
										<?php
                                    	unset($_SESSION['s_msg']);
                                    unset($_SESSION['e_msg']);
                                    }
                                ?>
                          <h3>Add Course Module</h3>
                        </div>
                      </div>
                    </div>

                    <div class="clearfix"></div>

                    <div class="row">
                      <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                          <div class="clearfix"></div>

                          <div class="x_content">
                            <div class="row">
                              <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel recent-app">
                                  <div class="x_content">
                                    <div class="student-account-page">
                                      <!-- Nav tabs -->
                                      <div class="card">
                                        <div class="all-students-list add-course">
                                          <div class="add-student-section">
                                            <form class="form-horizontal form-label-left" action="" method="post" enctype="multipart/form-data">
                                              <?php date_default_timezone_set('Australia/Sydney');
                                                        $date = date('d/m/Y h:i:s a', time());
														?>
													  <input id="who"  name="who"  type="hidden" value="<?php echo $date?>">
													  <input id="when"  name="user" type="hidden" value="<?php echo $_SESSION['user_no']?>">
                                              <div class="item form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Code</label>
                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                  <div class="row">
                                                    <div class="col-md-6 col-sm-4 col-xs-12">
                                                      <input type="text" id="code" class="form-control col-md-12 col-xs-12" name="code" placeholder="Subject code">
                                                    </div>
                                                    <div class="col-md-6 col-sm-4 col-xs-12">
                                                      <label class="pull-left">
                                                      
                                                        <input type="hidden" name="status" value="0">
                                                        <input type="checkbox" value="1" name="status">
                                                        Active
                                                      </label>

                                                    </div>
                                                  </div>
                                                </div>
                                              </div>
                                              
                                              <div class="item form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Name</label>
                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                  <div class="row">
                                                    <div class="col-md-7 col-sm-7 col-xs-12">
                                                       <input type="text" id="name" class="form-control col-md-12 col-xs-12" name="name" placeholder="Subject Name">
                                                    </div>
                                                  </div>
                                                </div>
                                              </div>
                                              
                                              
                                              <div class="item form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Total Hours/lebel</label>
                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                  <div class="row">
                                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                                      <input type="number" step="1" min="1" max="" name="hours" id="quantity" value="1" title="Qty" class="form-control input-text qty text" size="4">
                                                    </div>
                                                  </div>
                                                </div>
                                              </div>
                                            
                                              
                                              <div class="item form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Subject ID</label>
                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                  <div class="row">
                                                    <div class="col-md-7 col-sm-7 col-xs-12">
                                                       <input type="text" id="sub_id" class="form-control col-md-12 col-xs-12" name="sub_id" placeholder="Subject ID">
                                                    </div>
                                                  </div>
                                                </div>
                                              </div>
                                              
                                              <div class="item form-group">
                                                <label class="control-label col-md-5 col-sm-3 col-xs-12" for="name">The Intension of this Module vocational or pre-vocational:</label>
                                                <div class="col-md-1 col-sm-9 col-xs-12">
                                                  <div class="row">
                                                    <div class="col-md-7 col-sm-7 col-xs-12">
                                                       <input type="checkbox" id="name" class="form-control col-md-12 col-xs-12" name="package">
                                                    </div>
                                                  </div>
                                                </div>
                                              </div>
                                                <div class="item form-group">
                                                <label class="control-label col-md-5 col-sm-3 col-xs-12" for="name">This is the part of a VET school program:</label>
                                                <div class="col-md-1 col-sm-9 col-xs-12">
                                                  <div class="row">
                                                    <div class="col-md-7 col-sm-7 col-xs-12">
                                                       <input type="checkbox" id="name" class="form-control col-md-12 col-xs-12" name="intension">
                                                    </div>
                                                  </div>
                                                </div>
                                              </div>
                                              
                                              <div class="item form-group">
                                                <label class="control-label col-md-5 col-sm-3 col-xs-12" for="name">This is a unit of competency in a national training package:</label>
                                                <div class="col-md-1 col-sm-9 col-xs-12">
                                                  <div class="row">
                                                    <div class="col-md-7 col-sm-7 col-xs-12">
                                                       <input type="checkbox" id="name" class="form-control col-md-12 col-xs-12" name="package">
                                                    </div>
                                                  </div>
                                                </div>
                                              </div>
                                              
                                              
                                              
                                              <div class="item form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Delivery Mode:</label>
                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                  <div class="row">
                                                    <div class="col-md-7 col-sm-7 col-xs-12">
                                                       <input type="text" id="mode" class="form-control col-md-12 col-xs-12" name="mode" placeholder="Delivery Mode">
                                                    </div>
                                                  </div>
                                                </div>
                                              </div>
                                              <div class="item form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Field of Education:</label>
                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                  <div class="row">
                                                    <div class="col-md-7 col-sm-7 col-xs-12">
                                                       <input type="text" id="foe" class="form-control col-md-12 col-xs-12" name="foe" placeholder="Field of Education">
                                                    </div>
                                                  </div>
                                                </div>
                                              </div>
                                              <div class="item form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name"></label>
                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                  <div class="row">
                                                    <div class="col-md-6 col-sm-4 col-xs-12">
                                                      <p class="pull-left">
                                                        <button type="reset" value="Reset" class="btn btn-primary">Cancel</button>
                                                        <button type="submit" class="btn btn-success">Submit</button>
                                                      </p>
                                                    </div>
                                                  </div>
                                                </div>
                                              </div>
                                            </form>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                 </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
<div role="main" class="right_col" style="min-height: 648px;">

                  <div class="">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                      <div class="page-title">
					 
                        <div class="title_left">
                          <h3>Language Classes Details</h3>
                        </div>
                      </div>
                    </div>

                    <div class="clearfix"></div>

                    <div class="row">
                      <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                          <div class="clearfix"></div>

                          <div class="x_content">
                            <div class="row">
                              <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel recent-app">
                                  <div class="x_content">
                                    <div class="student-account-page">
                                      <!-- Nav tabs -->
                                      <div class="card">
                                        <ul role="tablist" class="nav nav-tabs">
                                          <li class="active" role="presentation"><a data-toggle="tab" role="tab" aria-controls="all-student" href="#all-student">Language Classes Details</a></li> 
                                        </ul> 
                                        <div class="tab-content">
                                          <div id="all-student" class="tab-pane active" role="tabpanel">
                                            <div class="all-students-list">
                                              <div class="table-responsive">
                                                <div id="datatable-buttons_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                                                <div class="dataTables_wrapper form-inline dt-bootstrap no-footer" id="datatable-buttons_wrapper">
                                                <div id="datatable-buttons_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                                                
                                                <table id="datatable" class="display table table-striped table-bordered" cellspacing="0" width="100%">
                                                
                                                  
                                                  <thead>
                                                    <tr class="headings" role="row"><th class="column-title sorting_asc" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1" style="width: 145px;" aria-label="Date
                                                        
                                                      : activate to sort column descending" aria-sort="ascending">Class
                                                        
                                                      </th><th class="column-title sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1" style="width: 145px;" aria-label="Attention
                                                   
                                                      : activate to sort column ascending">Session
                                                   
                                                      </th><th class="column-title sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1" style="width: 145px;" aria-label="Student No
                                                       
                                                      : activate to sort column ascending">Level
                                                       
                                                      </th><th class="column-title sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1" style="width: 145px;" aria-label="First Name
                                                    
                                                      : activate to sort column ascending">Room
                                                    
                                                      </th><th class="column-title sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1" style="width: 145px;" aria-label="Middle Name
                                                        
                                                      : activate to sort column ascending">Teacher
                                                        
                                                      </th><th class="column-title sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1" style="width: 145px;" aria-label="Last Name
                                                      
                                                      : activate to sort column ascending">Students
                                                      
                                                      </th><th class="column-title sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1" style="width: 145px;" aria-label="Subject
                                               
                                                      : activate to sort column ascending">Capacity
                                               
                                                      </th><th class="column-title sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1" style="width: 145px;" aria-label="Note
                                               
                                                      : activate to sort column ascending">On Holiday
                                               
                                                      </th><th class="column-title no-link last sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1" style="width: 145px;" aria-label="Action
                                                      : activate to sort column ascending"><span class="nobr">Action</span>
                                                      </th></tr>
                                                  </thead>
                                                  <tbody>  
                                                            
                                                             
                                                   <?php
														 $sql="select * from lang_class L,room R,class C,class_schedule C1,teacher T,course C2 where C1.ROOM_NO=R.ROOM_NO and C.CLASS_NO=C1.CLASS_NO LIMIT 10";
														 $q=mysql_query($sql);
														 while($row=mysql_fetch_array($q))
															{
																?>
                                                  <tr class="pointer odd" role="row">
                                                  
                                                        <td class="sorting_1" tabindex="0"><?=$row['COURSE_NAME'];?></td>
                                                          <td class=" "><?=$row['SESSION_NO'];?></td>
                                                          <td class=" "><?=$row['LEVEL_NO'];?></td>
                                                          <td class=" "><?=$row['ROOM_DESC'];?></td>
                                                          <td class=" "><?=$row['TEACHER_CODE'];?></td>
                                                          <td class=" "><?=$row['STUD_TOTAL'];?></td>
                                                          <td class=" "><?=$row['ROOM_CAPACITY'];?></td>
                                                          <td class=" "></td>	
                                                          <td class="last"><a class="color-sky stting" href="#"><i aria-hidden="true" class="fa fa-cog"></i>Edit</a>| <a class="color-sky stting" href="#"><i aria-hidden="true" class="fa fa-pencil-square-o"></i>Details</a></td>
														  <?php } ?>
                                                        </tr></tbody>
                                                </table>
                                                </div>
                                                </div>
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                 </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
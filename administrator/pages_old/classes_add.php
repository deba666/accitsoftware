<?php
if($_POST['class_name'])
	{
	if(insert_record("insert into `classes` set `class_name`='".$_POST['class_name']."',`class_session`='".$_POST['class_session']."',`class_room`='".$_POST['class_room']."',`class_level`='".$_POST['class_level']."',`class_type`='".$_POST['class_type']."',`class_per_w`='".$_POST['class_per_w']."', `status`='".$_POST['status']."'"))
		$_SESSION['s_msg']="<strong>Fine!</strong> Student class Added";
		else{
			$_SESSION['e_msg']="<strong>Oh snap!</strong> Student classes is not added";
		}
	}
?>

<div class="right_col" role="main">
                  <div class="">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                      <div class="page-title">
                        <div class="title_left">
                        <?php 
                                    if($_SESSION['s_msg'] || $_SESSION['e_msg'])
                                    {
										?>
                                            <div class="alert <?=($_SESSION['s_msg'])?'alert-success':'alert-error'?> fade in">
                                                <button data-dismiss="alert" class="close" type="button">×</button>
                                                <?=($_SESSION['s_msg'])?$_SESSION['s_msg']:$_SESSION['e_msg']?>
                                            </div>
										<?php
                                    	unset($_SESSION['s_msg']);
                                    unset($_SESSION['e_msg']);
                                    }
                                ?>
                          <h3>Add Class</h3>
                        </div>
                      </div>
                    </div>

                    <div class="clearfix"></div>

                    <div class="row">
                      <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                          <div class="clearfix"></div>

                          <div class="x_content">
                            <div class="row">
                              <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel recent-app">
                                  <div class="x_content">
                                    <div class="student-account-page edit-faculty-page">
                                      <!-- Nav tabs -->
                                      <div class="card">
                                        <div class="all-students-list add student">
                                          <div class="add-student-section">
                                            <form class="form-horizontal form-label-left" action="" method="post">
                                              <div class="item form-group">
                                              <input type="hidden" name="status" value="unread" />
                                                 <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Class Name</label>
                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                  <div class="row">
                                                    <div class="col-md-7 col-sm-7 col-xs-12">
                                                       <input id="name" class="form-control col-md-12 col-xs-12" name="class_name" placeholder="Enter Class Name" type="text" required>
                                                    </div>
                                                  </div>
                                                </div>
                                              </div>
                                              <div class="item form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Session</label>
                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                  <div class="row">
                                                    <div class="col-md-7 col-sm-7 col-xs-12">
                                                      <select name="class_session" class="form-control" >
                                                        <option value="session1">Session1</option>
                                                        <option value="session1">Session2</option>
                                                        <option value="session1">Session3</option>
                                                        <option value="session1">Session4</option>
                                                      </select>
                                                    </div>
                                                  </div>
                                                </div>
                                              </div>
                                              <div class="item form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Level</label>
                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                  <div class="row">
                                                    <div class="col-md-7 col-sm-7 col-xs-12">
                                                      <select name="class_level" class="form-control" >
                                                        <option value="1">1</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4">4</option>
                                                        <option value="5">5</option>
                                                        <option value="6">6</option>
                                                        <option value="7">7</option>
                                                      </select>
                                                    </div>
                                                  </div>
                                                </div>
                                              </div>
                                              <div class="item form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Room</label>
                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                  <div class="row">
                                                    <div class="col-md-6 col-sm-4 col-xs-12">
                                                      <select name="class_room" class="form-control">
                                                        <option value="room 2.1">(1)Room 2.1 (Lab)</option>
                                                        <option value="room 2.2">(2)Room 2.2</option>
                                                        <option value="room 3.1">(3)Room 3.1</option>
                                                        <option value="room 3.2">(4)Room 3.2</option>
                                                        <option value="room 3.3">(5)Room 3.3</option>
                                                        <option value="room 3.4">(6)Room 3.4</option>
                                                        <option value="room 3.5">(7)Room 3.5</option>
                                                        <option value="room 3.6">(8)Room 3.6</option>
                                                        <option value="room 3.7">(9)Room 3.7</option>
                                                      </select>
                                                    </div>
                                                    
                                                  </div>
                                                </div>
                                              </div>
                                              <div class="item form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Type</label>
                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                  <div class="row">
                                                    <div class="col-md-7 col-sm-7 col-xs-12">
                                                      <select name="class_type" class="form-control">
                                                        <option>Language Class</option>
                                                        <option>Language Module</option>
                                                      </select>
                                                    </div>
                                                  </div>
                                                </div>
                                              </div>
                                              
                                              
                                              <div class="item form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Hours Per Week</label>
                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                  <div class="row">
                                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                                      <input class="form-control col-md-12 col-xs-12" name="class_per_w" type="text"/>
                                                    </div>
                                                  </div>
                                                </div>
                                              </div>
                                              
                                              
                                              <div class="item form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name"></label>
                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                  <div class="row">
                                                    <div class="col-md-6 col-sm-4 col-xs-12">
                                                      <p class="pull-left">
                                                        <button type="submit" class="btn btn-primary">Cancel</button>
                                                        <button type="submit" class="btn btn-success">Submit</button>
                                                      </p>
                                                    </div>
                                                  </div>
                                                </div>
                                              </div>
                                            </form>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                 </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

              <div role="main" class="right_col" style="min-height: 648px;">

                  <div class="">
                    <div class="page-title">
                      <div class="title_left">
                        <h3>Admin Settings</h3>
                      </div>
                    </div>

                    <div class="clearfix"></div>

                    <div class="row">
                      <div class="col-md-12">
                        <div class="x_panel">
                          <div class="clearfix"></div>

                          <div class="x_content">
                            <div class="row">
                              <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel recent-app">
                                  <div class="x_content">
                                    <div class="admin-details-page text-center">
                                      
                                      <div class="clearfix"></div>
                                      <?php $user=getRows('order by `USER_NO` ASC','users'); ?>
                                                 
													 <?php
                                                        $sl=0;
                                                        foreach($user as $row)
                                                        {
                                                    ?>

                                               <div class="col-md-4 col-sm-4 col-xs-12 profile_details">
                                                <div class="well profile_view">
                                                  <div class="col-sm-12">
                                                    <div class="left col-xs-7">
                                                      <h2 class="admin-name"><?=$row['FNAME'];?> <?=$row['LNAME'];?> </h2><?=($row['STATUS'])?'<p style="color:green">Online</p>':'<p style="color:blue">Offline</p>'?>
                                                      <p><strong>Username: </strong><?=$row['USER_NAME'];?>(<?=$row['INIT'];?>)</p>
                                                      <ul class="list-unstyled">
                                                        <li><i class="fa fa-building"></i> Address: <?php $cond="where `LOCATION_NO`=".$row['LOCATION_NO']; $loc=getRows($cond,'location');?> <?=$loc[0]['LOCATION_NAME'];?></li>
                                                        <li><i class="fa fa-envelope-o"></i> Email: <?=$row['EMAIL'];?></li>
                                                      
                                                      </ul>
                                                    </div>
                                                    <div class="right col-xs-5 text-center">
                                                      <img class="img-circle img-responsive" alt="" src="images/admin2.png">
                                                    </div>
                                                  </div>
                                                  <div class="col-xs-12 bottom text-center">
                                                    <div class="col-xs-12 col-sm-12 emphasis">
                                                     <a href="dashboard.php?op=<?=MD5('change')?>&change=<?=$row['USER_NO'];?>"><button class="btn btn-primary btn-xs" onclick="" type="button"><i class="fa fa-user"> </i> Change</button></a>
                                                    </div>
                                                  </div>
                                                </div>
                                             </div>
                                           <?php   }    ?>
                                    </div>
                                  </div>
                                 </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
<?php 
if($_GET['details'])
{
	$cond="where `ENROL_NO`=".$_GET['details'];
	$enrol=getRows($cond,'enrol');
}

?>
<div class="right_col" role="main">
   <div class="">
      <div class="col-md-12 col-sm-12 col-xs-12">
         <div class="page-title">
            <div class="title_left">
               <h3>Enrollment Language details</h3>
            </div>
         </div>
      </div>
      <div class="clearfix"></div>
      <div class="row">
         <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
               <div class="clearfix"></div>
               <div class="x_content">
                  <div class="row">
                     <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel recent-app">
                           <div class="x_content">
                              <div class="student-account-page">
                                 <!-- Nav tabs -->
                                 <div class="card">
                                    <div class="all-students-list add student">
                                       <div class="add-student-section">
                                          <form class="form-horizontal form-label-left" action="" method="post" enctype="multipart/form-data" novalidate>

                                             <ul id="student-acc-tab" class="nav nav-tabs" role="tablist">
                                                <li role="presentation"  class="active"><a href="#details" aria-controls="details" role="tab" data-toggle="tab">Details</a></li>
                                                <li role="presentation"><a href="#diary" aria-controls="diary" role="tab" data-toggle="tab">Student Diary</a></li>
                                                <li role="presentation"><a href="#avetmiss" aria-controls="avetmiss" role="tab" data-toggle="tab">Avetmiss</a></li>
                                                <li role="presentation"><a href="#enrolments" aria-controls="enrolments" role="tab" data-toggle="tab">Enrolments</a></li>
                                                <li role="presentation"><a href="#changes" aria-controls="changes" role="tab" data-toggle="tab">Changes</a></li>
                                                <li role="presentation"><a href="#fees" aria-controls="fees" role="tab" data-toggle="tab">Fees</a></li>
                                                <li role="presentation"><a href="#leave" aria-controls="leave" role="tab" data-toggle="tab">Leave</a></li>
                                                <li role="presentation"><a href="#absence" aria-controls="absence" role="tab" data-toggle="tab">Absence</a></li>
                                                <li role="presentation"><a href="#result" aria-controls="result" role="tab" data-toggle="tab">Result</a></li>
                                                <li role="presentation"><a href="#classes" aria-controls="classes" role="tab" data-toggle="tab">Classes</a></li>
                                                <li role="presentation"><a href="#modules" aria-controls="modules" role="tab" data-toggle="tab">Course Modules</a></li>
                                             </ul>
                                             <div class="tab-content">
                                                <!--Details-->
                                                <div role="tabpanel" class="tab-pane active" id="details">
                                                   <div class="col-md-6 col-sm-6 col-xs-12">
                                                      <div class="row">
                                                         <h4>
                                                            <fieldset>Basic Info</fieldset>
                                                         </h4>
                                                         <div class="col-md-4 col-sm-4 col-xs-12">
                                                            <div class="form-group">
                                                               <label class="pull-left control-label col-md-12 col-sm-12 col-xs-12" for="name">Enroll No:</label>
                                                               <input class="form-control col-md-12 col-xs-12" name="enrol_no" disabled="disabled" Value="<?=$enrol[0]['ENROL_NO'];?>" type="text">
                                                            </div>
                                                         </div>
                                                         <div class="col-md-4 col-sm-4 col-xs-12">
                                                            <div class="form-group">
                                                              <label class="pull-left control-label col-md-12 col-sm-12 col-xs-12" for="name">Status:</label>
                                                               <input class="form-control col-md-12 col-xs-12 demoInputBox" name="std_no" disabled="disabled" Value="<?=$enrol[0]['CUR_WK'];?> Week" type="text">
                                                            </div>
                                                         </div>
                                                         <div class="col-md-2 col-sm-2 col-xs-2">
                                                            <legend>Attendance</legend>
                                                            Curr:<input class="form-control col-md-2 col-xs-2 " name="curr" disabled="disabled" Value="<?=$enrol[0]['CUR_ATT'];?>" type="text"><br/>
                                                            Overall:<input class="form-control col-md-2 col-xs-2 " name="ove"disabled="disabled" Value="<?=$enrol[0]['OVR_ATT'];?>" type="text"></br>
                                                            Hours Absence:<input class="form-control col-md-2 col-xs-2" name="absence" disabled="disabled" Value="<?=$enrol[0]['CURRENT_TOTAL'];?>" type="text">
                                                            
                                                            
                                                         </div>
                                                      </div>
                                                      <div class="row">
                                                         <div class="col-md-3 col-sm-3 col-xs-3">
                                                            <div class="form-group">
                                                               <label class="pull-left control-label col-md-12 col-sm-12 col-xs-12" for="name">Length
                                                               </label>
                                                               <input disabled="disabled" Value="<?=$enrol[0]['CRSE_LEN'];?>" class="form-control col-md-12 col-sm-12 col-xs-12" name="course_length" 
                                                                type="text">
                                                            </div>
                                                         </div>
                                                         <div class="col-md-3 col-sm-3 col-xs-3">
                                                            <div class="form-group">
                                                               <label class="pull-left control-label col-md-12 col-sm-12 col-xs-12" for="name">Start
                                                               </label>
                                                               <input disabled="disabled" Value="<?=$enrol[0]['ST_DATE'];?>" class="form-control col-md-12 col-sm-12 col-xs-12" name="start" 
                                                               type="text">
                                                            </div>
                                                         </div>
                                                         <div class="col-md-3 col-sm-3 col-xs-3">
                                                            <div class="form-group">
                                                               <label class="pull-left control-label col-md-12 col-sm-12 col-xs-12" for="name">Finish
                                                               </label>
                                                               <input disabled="disabled" Value="<?=$enrol[0]['END_DATE'];?>" class="form-control col-md-12 col-sm-12 col-xs-12" name="end" 
                                                                type="text">
                                                            </div>
                                                         </div>
                                                         <div class="col-md-3 col-sm-1 col-xs-1">
                                                            <label class="pull-center control-label col-md-12 col-sm-12 col-xs-12" for="name">Arrived</label><br />
                                                           <?=($enrol[0]['ARVD'])?'<input name="ar" type="checkbox" disabled value="" checked />':'<input name="ar" type="checkbox" disabled value="" />'?>
                                                           
                                                         </div>
                                                         <div class="col-md-3 col-sm-1 col-xs-1">
                                                          
                                                           <label class="pull-center control-label col-md-12 col-sm-12 col-xs-12" for="name">Certiftcate Awared</label><br />
                                                          <?=($enrol[0]['AWARDED'])?'<input name="cer" type="checkbox" disabled value="" checked />':'<input name="cer" type="checkbox" disabled value="" />'?>
                                                         </div>
                                                         <div class="col-md-3 col-sm-1 col-xs-1">
                                                          
                                                           <label class="pull-center control-label col-md-12 col-sm-12 col-xs-12" for="name">Re-enrolment for signed</label><br />
                                                           <?=($enrol[0]['RE_ENROL'])?'<input name="sign" type="checkbox" disabled value="" checked />':'<input name="sign" type="checkbox" disabled value="" />'?>
                                                         </div>
                                                      </div>
                                                      <div class="row">
                                                         <div class="col-md-12 col-sm-12">
                                                            <div class="row">
                                                               <h4>
                                                                  <fieldset>Custom Fields</fieldset>
                                                               </h4>
                                                               <div class="col-md-6 col-sm-6 col-xs-6">
                                                                  <div class="form-group">
                                                                     <input id="std_custom1" class="form-control col-md-12 col-sm-12 col-xs-12" name="std_custom1" placeholder="" type="text">
                                                                  </div>
                                                               </div>
                                                               <div class="col-md-6 col-sm-6 col-xs-6">
                                                                  <input id="std_custom2" class="form-control col-md-12 col-sm-12 col-xs-12" name="std_custom2" placeholder="" type="text">
                                                               </div>
                                                            </div>
                                                            <div class="row">
                                                               <div class="col-md-6 col-sm-6 col-xs-6">
                                                                  <div class="form-group">
                                                                     <input id="std_custom3" class="form-control col-md-12 col-sm-12 col-xs-12" name="std_custom3" placeholder="" type="text">
                                                                  </div>
                                                               </div>
                                                               <div class="col-md-6 col-sm-6 col-xs-6">
                                                                  <input id="std_custom4" class="form-control col-md-12 col-sm-12 col-xs-12" name="std_custom4" placeholder="" type="text">
                                                               </div>
                                                            </div>
                                                            <div class="row">
                                                               <div class="col-md-6 col-sm-6 col-xs-6">
                                                                  <div class="form-group">
                                                                     <input id="std_custom5" class="form-control col-md-12 col-sm-12 col-xs-12" name="std_custom5" placeholder="Passport" type="text">
                                                                  </div>
                                                               </div>
                                                               <div class="col-md-6 col-sm-6 col-xs-6">
                                                                  <input id="std_custom6" class="form-control col-md-12 col-sm-12 col-xs-12" name="std_custom6" placeholder="" type="text">
                                                               </div>
                                                            </div>
                                                            <div class="row">
                                                               <div class="col-md-6 col-sm-6 col-xs-6">
                                                                  <div class="form-group">
                                                                     <input id="std_custom7" class="form-control col-md-12 col-sm-12 col-xs-12" name="std_custom7" placeholder="" type="text">
                                                                  </div>
                                                               </div>
                                                               <div class="col-md-6 col-sm-6 col-xs-6">
                                                                  <input id="std_custom8" class="form-control col-md-12 col-sm-12 col-xs-12" name="std_custom8" placeholder="" type="text">
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="col-md-6 col-sm-6 col-xs-12">
                                                      <div class="row">
                                                         <h4>
                                                            <fieldset>Others Info</fieldset>
                                                         </h4>
                                                         <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <div class="form-group">
                                                               <label class="control-label col-md-12 col-sm-12 col-xs-12" for="name">Course </label>
                                                               <?php $cond="where `COURSE_NO`=".$enrol[0]['COURSE_NO']; $course=getRows($cond,'course');?>
                                                               <input  class="form-control col-md-12 col-sm-12 col-xs-12" name="course" disabled="disabled" Value="<?=$course[0]['COURSE_NAME'];?>" type="text">
                                                            </div>
                                                         </div>
                                                         <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <div class="form-group">
                                                               <label class="control-label col-md-12 col-sm-12 col-xs-12" for="name">Agent </label>
                                                               <?php $cond="where `AGENT_NO`=".$enrol[0]['AGENT_NO']; $agent=getRows($cond,'agent');?>
                                                              <input  class="form-control col-md-12 col-sm-12 col-xs-12" name="agent" disabled="disabled" Value="<?=$agent[0]['AGENT_NAME'];?>" type="text">
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div class="row">
                                                         <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <div class="form-group">
                                                               <label class="control-label col-md-12 col-sm-12 col-xs-12" for="name">Location
                                                               </label>
                                                               <?php $cond="where `LOCATION_NO`=".$enrol[0]['LOCATION_NO']; $location=getRows($cond,'location');?>
                                                                <input class="form-control col-md-12 col-sm-12 col-xs-12" name="location" disabled="disabled" Value="<?=$location[0]['LOCATION_NAME'];?>" type="text">
                                                            </div>
                                                         </div>
                                                         <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <div class="row">
                                                               <div class="col-md-6 col-sm-6 col-xs-12">
                                                                  <label class="control-label col-md-12 col-sm-12 col-xs-12" for="name">Who
                                                                  </label>
                                                                  <?php $cond="where `USER_NO`=".$enrol[0]['USER_NO']; $user=getRows($cond,'users');?>
                                                                  <input  name="user" class="date-picker form-control col-md-12 col-xs-12" disabled="disabled" Value="<?=$user[0]['INIT'];?>" type="text">
                                                               </div>
                                                               <div class="col-md-6 col-sm-6 col-xs-12">
                                                                  <label class="control-label col-md-12 col-sm-12 col-xs-12" for="name">When
                                                                  </label>
                                                                  <input  name="std_when" class="date-picker form-control col-md-12 col-xs-12" disabled="disabled" Value="<?=$enrol[0]['WHO_DATE'];?>" type="text">
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div class="row">
                                                         <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <div class="form-group">
                                                               <label class="control-label col-md-12 col-sm-12 col-xs-12" for="name">Booking Date
                                                               </label>
                                                               <input type="text" name="booking"  class="form-control col-md-12 col-xs-12" disabled="disabled" Value="<?=$enrol[0]['ADD_DATE'];?>">
                                                            </div>
                                                         </div>
                                                         <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <div class="form-group">
                                                               <label class="control-label col-md-12 col-sm-12 col-xs-12" for="name">Agent Incentive Due
                                                               </label>
                                                               <input type="text" name="due"  Value="none" disabled="disabled"class="form-control col-md-12 col-xs-12">
                                                            </div>
                                                         </div>
                                                      </div>
                                                      
                                                   </div>
                                                </div>
                                                <!--diary-->
                                                <div role="tabpanel" class="tab-pane" id="diary">
                                                   <div class="table-responsive">
                                                      <table id="datatable" class="display table table-striped table-bordered" cellspacing="0" width="100%">
                                                         <thead>
                                                            <tr class="headings">
                                                               <th class="column-title">Attention</th>
                                                               <th class="column-title">Date</th>
                                                               <th class="column-title">Cateory </th>
                                                               <th class="column-title">Subject </th>
                                                               <th class="column-title">User</th>
                                                               <th class="column-title">Details</th>
                                                            </tr>
                                                         </thead>
                                                         
                                                         <tbody>
                                                       <?php  
													           $cond="where `CUSTOMER_NO`=".$_GET['cid']; $customer=getRows($cond,'diary');

																foreach($customer as $row)
																{
																	?>
                                                            <tr class="even pointer">
                                                               <td class=" "><?=($customer[0]['ATTENTION'])?'<input name="sign" type="checkbox" disabled value="" checked />':'<input name="sign" type="checkbox" disabled value="" />'?></td>
                                                               <td class=" "><?=$customer[0]['DIARY_DATE']?></td>
                                                               <td class=" "><?=$customer[0]['CATEGORY_NO']?></td>
                                                               <td class=" "><?=$customer[0]['SUMMARY']?></td>
                                                               <?php $cond="where `USER_NO`=".$customer[0]['USER_NO']; $u=getRows($cond,'users');?>
                                                               <td class=" "><?=$u[0]['INIT']?></td>
                                                               <td class=" "><?=$customer[0]['NOTES']?></td>

                                                            </tr>
															<?php  }?>
                                                         </tbody>
                                                      </table>
                                                   </div>
                                                </div>
                                                <!--Avetmiss-->
                                                <div role="tabpanel" class="tab-pane" id="avetmiss">
                                                   <div class="col-md-6 col-sm-6 col-xs-12">
                                                      <div class="row">
                                                         <h4>
                                                            <fieldset>Basic Details</fieldset>
                                                         </h4>
                                                         <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <div class="form-group">
                                                               <label class="pull-left control-label col-md-12 col-sm-12 col-xs-12" for="name">Unique Student Identifier
                                                               </label>
                                                               <input type="text" id="avet_usi_no" name="avet_usi_no" placeholder="Unique Student Identifier" class="form-control col-md-12 col-xs-12">
                                                            </div>
                                                         </div>
                                                         <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <div class="form-group">
                                                               <label class="pull-left control-label col-md-12 col-sm-12 col-xs-12" for="name">Country Of Birth
                                                               </label>
                                                               <select id="std_birth_country" name="std_birth_country" class="col-md-12 col-xs-12 form-control">
                                                                  <option>Sydeny</option>
                                                                  <option>Kolkata</option>
                                                               </select>
                                                            </div>
                                                         </div>
                                                         <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <div class="form-group">
                                                               <label class="pull-left control-label col-md-12 col-sm-12 col-xs-12" for="name">Labour Force Status
                                                               </label>
                                                               <select id="std_labour_status" name="std_labour_status" class="col-md-12 col-xs-12 form-control">
                                                                  <option>Labour Force Status</option>
                                                                  <option>Labour Force Status</option>
                                                               </select>
                                                            </div>
                                                         </div>
                                                         <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <div class="form-group">
                                                               <label class="pull-left control-label col-md-12 col-sm-12 col-xs-12" for="name">Proficiency in Spoken English
                                                               </label>
                                                               <select id="std_spn_eng" name="std_spn_eng" class="col-md-12 col-xs-12 form-control">
                                                                  <option>Good in Spoken English</option>
                                                                  <option>Average in Spoken English</option>
                                                               </select>
                                                            </div>
                                                         </div>
                                                         <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <div class="form-group">
                                                               <label class="pull-left control-label col-md-12 col-sm-12 col-xs-12" for="name">Indigenous Status
                                                               </label>
                                                               <select id="std_indigen" name="std_indigen" class="col-md-12 col-xs-12 form-control">
                                                                  <option>Indigenous Status</option>
                                                                  <option>Indigenous Status</option>
                                                               </select>
                                                            </div>
                                                         </div>
                                                         <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <div class="form-group">
                                                               <label class="pull-left control-label col-md-12 col-sm-12 col-xs-12" for="name">Disabilities
                                                               </label>
                                                               <select id="std_disabl" name="std_disabl" class="col-md-12 col-xs-12 form-control">
                                                                  <option>None</option>
                                                                  <option>Disabilities</option>
                                                               </select>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div class="row">
                                                         <div class="col-md-12 col-sm-12">
                                                            <div class="row">
                                                               <h4>
                                                                  <fieldset>Address</fieldset>
                                                               </h4>
                                                               <div class="col-md-6 col-sm-6 col-xs-12">
                                                                  <div class="form-group">
                                                                     <label class="pull-left control-label col-md-12 col-sm-12 col-xs-12" for="name">Building/Property Name
                                                                     </label>
                                                                     <input id="std_property_name" name="std_property_name" type="text" placeholder="" class="form-control col-md-12 col-xs-12">
                                                                  </div>
                                                               </div>
                                                               <div class="col-md-6 col-sm-6 col-xs-12">
                                                                  <div class="form-group">
                                                                     <label class="pull-left control-label col-md-12 col-sm-12 col-xs-12" for="name">Postal Delivery Box
                                                                     </label>
                                                                     <input type="text" id="std_postal_box" name="std_postal_box" placeholder="" class="form-control col-md-12 col-xs-12">
                                                                  </div>
                                                               </div>
                                                               <div class="col-md-6 col-sm-6 col-xs-12">
                                                                  <div class="form-group">
                                                                     <label class="pull-left control-label col-md-12 col-sm-12 col-xs-12" for="name">Flat/Unit Details
                                                                     </label>
                                                                     <input type="text" id="std_flat_dtls" name="std_flat_dtls" placeholder="" class="form-control col-md-12 col-xs-12">
                                                                  </div>
                                                               </div>
                                                               <div class="col-md-6 col-sm-6 col-xs-12">
                                                                  <div class="form-group">
                                                                     <label class="pull-left control-label col-md-12 col-sm-12 col-xs-12" for="name">Street Name
                                                                     </label>
                                                                     <input type="text" id="std_street_name" name="std_street_name" placeholder="" class="form-control col-md-12 col-xs-12">
                                                                  </div>
                                                               </div>
                                                               <div class="col-md-6 col-sm-6 col-xs-12">
                                                                  <div class="form-group">
                                                                     <label class="pull-left control-label col-md-12 col-sm-12 col-xs-12" for="name">Street Number
                                                                     </label>
                                                                     <input type="text" id="std_street_number" name="std_street_number" placeholder="" class="form-control col-md-12 col-xs-12">
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="col-md-6 col-sm-6 col-xs-12">
                                                      <div class="row">
                                                         <h4>
                                                            <fieldset>Education</fieldset>
                                                         </h4>
                                                         <div class="col-md-12 col-sm-12">
                                                            <div class="row">
                                                               <div class="col-md-9 col-sm-9 col-xs-9">
                                                                  <div class="form-group">
                                                                     <label class="pull-left control-label col-md-12 col-sm-12 col-xs-12" for="name">Highest Level of Shool Completed
                                                                     </label>
                                                                     <select id="std_sch_lebel" name="std_sch_lebel" class="col-md-12 col-xs-12 form-control">
                                                                        <option>Diploma</option>
                                                                        <option>B.Sc</option>
                                                                     </select>
                                                                  </div>
                                                               </div>
                                                               <div class="col-md-3 col-sm-3 col-xs-3">
                                                                  <div class="form-group">
                                                                     <label class="pull-left control-label col-md-12 col-sm-12 col-xs-12" for="name">Year
                                                                     </label>
                                                                     <select id="std_year_sch" name="std_year_sch" class="col-md-12 col-xs-12 form-control">
                                                                        <option>2012</option>
                                                                        <option>2011</option>
                                                                        <option>2010</option>
                                                                        <option>2009</option>
                                                                        <option>2008</option>
                                                                        <option>2007</option>
                                                                        <option>2006</option>
                                                                        <option>2006</option>
                                                                     </select>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="col-md-12 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                               <div class="checkbox">
                                                                  <label class="pull-left control-label">
                                                                  <input type="checkbox" id="std_curr_attend" name="std_curr_attend" class="flat"> Currently attending secondary school
                                                                  </label>
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="col-md-12 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                               <label class="pull-left control-label col-md-12 col-sm-12 col-xs-12" for="name">Prior Educational Achivement
                                                               </label>
                                                               <select id="std_edu_achiv" name="std_edu_achiv" class="col-md-12 col-xs-12 form-control">
                                                                  <option>None</option>
                                                                  <option>Prior Educational Achivement</option>
                                                               </select>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <!--enrolments-->
                                                <div role="tabpanel" class="tab-pane" id="enrolments">
                                                   <div class="table-responsive">
                                                      <table id="datatable" class="display table table-striped table-bordered" cellspacing="0" width="100%">
                                                <thead>
                                                   <tr class="headings">
                                                      <th class="column-title">Student No.
                                                      </th>
                                                      <th class="column-title">First Name 
                                                      </th>
                                                      <th class="column-title">Middle Name
                                                      </th>
                                                      <th class="column-title">Last Name
                                                      </th>
                                                      <th class="column-title">Course Name 
                                                      </th>
                                                      <th class="column-title">Faculty
                                                      </th>
                                                      <th class="column-title">Start 
                                                      </th>
                                                      <th class="column-title">End
                                                      </th>
                                                      <th class="column-title">Status
                                                      </th>
                                                      <th class="column-title">DOB
                                                      </th>
                                                      <th class="column-title">Gender
                                                      </th>
                                                      <th class="column-title">Country
                                                      </th>
                                                      <th class="column-title">Agent
                                                      </th>
                                                      <th class="column-title">Current Attn. 
                                                      </th>
                                                      <th class="column-title">Overall Attn.
                                                      </th>
                                                      <th class="column-title">OutStanding Fees 
                                                      </th>
                                                      <th class="column-title">Visa Type 
                                                      </th>
                                                      
                                                   </tr>
                                                </thead>
                                                <?php
												 $cond="where `ENROL_NO`=".$_GET['eid']; $en=getRows($cond,'enrol');
												 ?>
                                                <tbody>
                                                   <?php
                                                      
                                                      foreach($en as $row)
                                                      {
                                                      ?>
                                                   <tr class="even pointer">
                                                      <td class=" "><?=$row['STUD_NO'];?></td>
                                                      <?php $cond="where `STUD_NO`=".$row['STUD_NO']; $std=getRows($cond,'student');?>
                                                      <td class=" "><?=$std[0]['FNAME'];?></td>
                                                      <td class=" "><?=$std[0]['MNAME'];?></td>
                                                      <td class=" "><?=$std[0]['LNAME'];?></td>
                                                      <?php $cond="where `COURSE_NO`=".$row['COURSE_NO']; $course=getRows($cond,'course');?>
                                                      <td class=" "><?=$course[0]['COURSE_NAME'];?></td>
                                                      <?php $cond="where `CRT_NO`=".$row['CRT_NO']; $crt=getRows($cond,'crse_type');?>
                                                      <td class=" "><?=$crt[0]['CRT_NAME'];?></td>
                                                      <td class=" "><?=$row['ST_DATE'];?></td>
                                                      <td class=" "><?=$row['END_DATE'];?></td>
                                                      <td class=" "><?=($row['STATUS'])?'Active':'Inactive'?></td>
                                                      <td class=" "><?=$std[0]['DOB'];?></td>
                                                      <td class=" "><?=$std[0]['GENDER'];?></td>
                                                      <?php $cond="where `COUNTRY_NO`=".$std[0]['COUNTRY_NO']; $cou=getRows($cond,'country');?>
                                                      <td class=" "><?=$cou[0]['COUNTRY_NAME'];?></td>
                                                      <?php $cond="where `AGENT_NO`=".$row['AGENT_NO']; $ag=getRows($cond,'agent');?>
                                                      <td class=" "><?=$ag[0]['AGENT_NAME'];?></td>
                                                      <td class=" "><?=$row['CUR_ATT'];?></td>
                                                      <td class=" "><?=$row['OVR_ATT'];?></td>
                                                      <td class=" "><?=$row['ENROL_NO'];?></td>  
                                                      <td class=" "><?=$row['VS_TYPE']?></td>
                                                      
                                                   </tr>
                                                   <?php } ?>
                                                </tbody>
                                             </table>
                                                   </div>
                                                </div>
                                                <!--Offer-->
                                                <div role="tabpanel" class="tab-pane" id="changes">
                                                   <div class="table-responsive">
                                                      <table id="example2" class="display table table-striped table-bordered" cellspacing="0" width="100%">
                                                         
                                                         <thead>
                                                            <tr class="headings">
                                                               <th class="column-title">Student No</th>
                                                               <th class="column-title"> Arrived </th>
                                                               <th class="column-title"> First Name</th>
                                                               <th class="column-title">Middle Name
                                                               </th>
                                                               <th class="column-title">Last Name
                                                               </th>
                                                               <th class="column-title">Course Name
                                                               </th>
                                                               <th class="column-title">Faculty
                                                               </th>
                                                               <th class="column-title">Start Date
                                                               </th>
                                                               <th class="column-title">End Date
                                                               </th>
                                                               <th class="column-title">DOB
                                                               </th>
                                                               <th class="column-title">Status
                                                               </th>
                                                               <th class="column-title">Gender
                                                               </th>
                                                               <th class="column-title">Country
                                                               </th>
                                                               <th class="column-title">Agent
                                                               </th>
                                                               <th class="column-title">Mother Tongue
                                                               </th>
                                                               <th class="column-title">Current Att.
                                                               </th>
                                                            </tr>
                                                         </thead>
                                                         <tbody>
                                                            <tr class="even pointer">
                                                               <td class=" ">a000843</td>
                                                               <td class=" "><input type="checkbox" class="flat"> </td>
                                                               <td class=" ">Sk</td>
                                                               <td class=" ">Rafael</td>
                                                               <td class=" ">Hand</td>
                                                               <td class=" ">Diploma</td>
                                                               <td class=" ">Business Progress</td>
                                                               <td class=" ">25/05/2016</td>
                                                               <td class=" ">26/05/2016</td>
                                                               <td class=" ">Week 6 of 36</td>
                                                               <td class=" ">01 june 1991</td>
                                                               <td class=" ">Male</td>
                                                               <td class=" ">India</td>
                                                               <td class=" ">Direct Enroll</td>
                                                               <td class=" ">Malay</td>
                                                               <td class=" ">100</td>
                                                            </tr>
                                                            <tr class="odd pointer">
                                                               <td class=" ">a000843</td>
                                                               <td class=" "><input type="checkbox" class="flat"> </td>
                                                               <td class=" ">Sk</td>
                                                               <td class=" ">Rafael</td>
                                                               <td class=" ">Hand</td>
                                                               <td class=" ">Diploma</td>
                                                               <td class=" ">Business Progress</td>
                                                               <td class=" ">25/05/2016</td>
                                                               <td class=" ">26/05/2016</td>
                                                               <td class=" ">Week 6 of 36</td>
                                                               <td class=" ">01 june 1991</td>
                                                               <td class=" ">Male</td>
                                                               <td class=" ">India</td>
                                                               <td class=" ">Direct Enroll</td>
                                                               <td class=" ">Malay</td>
                                                               <td class=" ">100</td>
                                                            </tr>
                                                         </tbody>
                                                      </table>
                                                   </div>
                                                </div>
                                                <!--airport-->
                                                <div role="tabpanel" class="tab-pane" id="airport">
                                                   <div class="table-responsive">
                                                      <table id="example4" class="display table table-striped table-bordered" cellspacing="0" width="100%">
                                                         <div class="hide-show-column dropdown" role="presentation">
                                                            <a id="drop4" href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" role="button" aria-expanded="false">Hide / Show Column<span class="caret"></span></a>
                                                            <ul id="menu6" class="dropdown-menu animated fadeInDown" role="menu">
                                                               <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                                  <span class="pull-left">
                                                                  <input type="checkbox" class="flat">
                                                                  </span>
                                                                  <span class="pull-left">
                                                                  Date
                                                                  </span>
                                                                  </a>
                                                               </li>
                                                               <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                                  <span class="pull-left">
                                                                  <input type="checkbox" class="flat">
                                                                  </span>
                                                                  <span class="pull-left">
                                                                  Form
                                                                  </span>
                                                                  </a>
                                                               </li>
                                                               <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                                  <span class="pull-left">
                                                                  <input type="checkbox" class="flat">
                                                                  </span>
                                                                  <span class="pull-left">
                                                                  To
                                                                  </span>
                                                                  </a>
                                                               </li>
                                                               <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                                  <span class="pull-left">
                                                                  <input type="checkbox" class="flat">
                                                                  </span>

                                                                  <span class="pull-left">
                                                                  Flight
                                                                  </span>
                                                                  </a>
                                                               </li>
                                                               <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                                  <span class="pull-left">
                                                                  <input type="checkbox" class="flat">
                                                                  </span>
                                                                  <span class="pull-left">
                                                                  By
                                                                  </span>
                                                                  </a>
                                                               </li>
                                                               <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                                  <span class="pull-left">
                                                                  <input type="checkbox" class="flat">
                                                                  </span>
                                                                  <span class="pull-left">
                                                                  Status
                                                                  </span>
                                                                  </a>
                                                               </li>
                                                               <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                                  <span class="pull-left">
                                                                  <input type="checkbox" class="flat">
                                                                  </span>
                                                                  <span class="pull-left">
                                                                  Notes
                                                                  </span>
                                                                  </a>
                                                               </li>
                                                               <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                                  <span class="pull-left">
                                                                  <input type="checkbox" class="flat">
                                                                  </span>
                                                                  <span class="pull-left">
                                                                  User
                                                                  </span>
                                                                  </a>
                                                               </li>
                                                               <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                                  <span class="pull-left">
                                                                  <input type="checkbox" class="flat">
                                                                  </span>
                                                                  <span class="pull-left">
                                                                  When
                                                                  </span>
                                                                  </a>
                                                               </li>
                                                            </ul>
                                                         </div>
                                                         <thead>
                                                            <tr class="headings">
                                                               <th class="column-title">Date
                                                               </th>
                                                               <th class="column-title">From
                                                               </th>
                                                               <th class="column-title">To
                                                               </th>
                                                               <th class="column-title">Flight
                                                               </th>
                                                               <th class="column-title">By
                                                               </th>
                                                               <th class="column-title">Status
                                                               </th>
                                                               <th class="column-title">Notes
                                                               </th>
                                                               <th class="column-title">User
                                                               </th>
                                                               <th class="column-title">When
                                                               </th>
                                                            </tr>
                                                         </thead>
                                                         <tbody>
                                                            <tr class="even pointer">
                                                               <td class=" ">26/06/2016</td>
                                                               <td class=" ">Kolkata</td>
                                                               <td class=" ">Mumbai</td>
                                                               <td class=" ">Airindia</td>
                                                               <td class=" ">Pending</td>
                                                               <td class=" "></td>
                                                               <td class=" ">JK</td>
                                                               <td class=" "></td>
                                                               <td class=" "></td>
                                                            </tr>
                                                         </tbody>
                                                      </table>
                                                   </div>
                                                </div>
                                                <!--visa-->
                                                <div role="tabpanel" class="tab-pane" id="visa">
                                                   <div class="table-responsive stu-visa">
                                                      <table id="example5" class="display table table-striped table-bordered" cellspacing="0" width="100%">
                                                         <div class="hide-show-column dropdown" role="presentation">
                                                            <a id="drop4" href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" role="button" aria-expanded="false">Hide / Show Column<span class="caret"></span></a>
                                                            <ul id="menu6" class="dropdown-menu animated fadeInDown" role="menu">
                                                               <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                                  <span class="pull-left">
                                                                  <input type="checkbox" class="flat">
                                                                  </span>
                                                                  <span class="pull-left">
                                                                  Visa No.
                                                                  </span>
                                                                  </a>
                                                               </li>
                                                               <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                                  <span class="pull-left">
                                                                  <input type="checkbox" class="flat">
                                                                  </span>
                                                                  <span class="pull-left">
                                                                  Visa Type
                                                                  </span>
                                                                  </a>
                                                               </li>
                                                               <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                                  <span class="pull-left">
                                                                  <input type="checkbox" class="flat">
                                                                  </span>
                                                                  <span class="pull-left">
                                                                  Arrive
                                                                  </span>
                                                                  </a>
                                                               </li>
                                                               <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                                  <span class="pull-left">
                                                                  <input type="checkbox" class="flat">
                                                                  </span>
                                                                  <span class="pull-left">
                                                                  Expiry
                                                                  </span>
                                                                  </a>
                                                               </li>
                                                               <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                                  <span class="pull-left">
                                                                  <input type="checkbox" class="flat">
                                                                  </span>
                                                                  <span class="pull-left">
                                                                  User
                                                                  </span>
                                                                  </a>
                                                               </li>
                                                               <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                                  <span class="pull-left">
                                                                  <input type="checkbox" class="flat">
                                                                  </span>
                                                                  <span class="pull-left">
                                                                  When
                                                                  </span>
                                                                  </a>
                                                               </li>
                                                            </ul>
                                                         </div>
                                                         <thead>
                                                            <tr class="headings">
                                                               <th class="column-title">Visa No.
                                                               </th>
                                                               <th class="column-title">Visa Type
                                                               </th>
                                                               <th class="column-title">Arrive
                                                               </th>
                                                               <th class="column-title">Expiry
                                                               </th>
                                                               <th class="column-title">User
                                                               </th>
                                                               <th class="column-title">When
                                                               </th>
                                                            </tr>
                                                         </thead>
                                                         <tbody>
                                                            <tr class="even pointer">
                                                               <td class=" ">23568569856</td>
                                                               <td class=" "></td>
                                                               <td class=" ">Mumbai</td>
                                                               <td class=" ">27/09/2022</td>
                                                               <td class=" ">Jk</td>
                                                               <td class=" ">Monday</td>
                                                            </tr>
                                                            <tr class="odd pointer">
                                                               <td class=" ">23568569856</td>
                                                               <td class=" "></td>
                                                               <td class=" ">Mumbai</td>
                                                               <td class=" ">27/09/2022</td>
                                                               <td class=" ">Jk</td>
                                                               <td class=" ">Monday</td>
                                                            </tr>
                                                         </tbody>
                                                      </table>
                                                   </div>
                                                </div>
                                                <!--att-Warning-->
                                                <div role="tabpanel" class="tab-pane" id="att-Warning">
                                                   <div class="table-responsive">
                                                      <table id="example6" class="display table table-striped table-bordered" cellspacing="0" width="100%">
                                                         <div class="hide-show-column dropdown" role="presentation">
                                                            <a id="drop4" href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" role="button" aria-expanded="false">Hide / Show Column<span class="caret"></span></a>
                                                            <ul id="menu6" class="dropdown-menu animated fadeInDown" role="menu">
                                                               <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                                  <span class="pull-left">
                                                                  <input type="checkbox" class="flat">
                                                                  </span>
                                                                  <span class="pull-left">
                                                                  Letter Level
                                                                  </span>
                                                                  </a>
                                                               </li>
                                                               <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                                  <span class="pull-left">
                                                                  <input type="checkbox" class="flat">
                                                                  </span>
                                                                  <span class="pull-left">
                                                                  Data Sent
                                                                  </span>
                                                                  </a>
                                                               </li>
                                                            </ul>
                                                         </div>
                                                         <thead>
                                                            <tr class="headings">
                                                               <th style="width: 465px !important;" class="column-title">Letter Level
                                                               </th>
                                                               <th style="width: 465px !important;" class="column-title">Data Sent
                                                               </th>
                                                               </th>
                                                            </tr>
                                                         </thead>
                                                         <tbody>
                                                            <tr class="even pointer">
                                                               <td style="width: 465px !important;" class=" ">Demo</td>
                                                               <td style="width: 465px !important;" class=" ">Test</td>
                                                            </tr>
                                                            <tr class="odd pointer">
                                                               <td style="width: 465px !important;" class=" ">Demo</td>
                                                               <td style="width: 465px !important;" class=" ">Test</td>
                                                            </tr>
                                                         </tbody>
                                                      </table>
                                                   </div>
                                                </div>
                                                <!--insurance-->
                                                <div role="tabpanel" class="tab-pane" id="insurance">
                                                   <div class="table-responsive stu-insurance">
                                                      <table id="example7" class="display table table-striped table-bordered" cellspacing="0" width="100%">
                                                         <div class="hide-show-column dropdown" role="presentation">
                                                            <a id="drop4" href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" role="button" aria-expanded="false">Hide / Show Column<span class="caret"></span></a>
                                                            <ul id="menu6" class="dropdown-menu animated fadeInDown" role="menu">
                                                               <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                                  <span class="pull-left">
                                                                  <input type="checkbox" class="flat">
                                                                  </span>
                                                                  <span class="pull-left">
                                                                  Product
                                                                  </span>
                                                                  </a>
                                                               </li>
                                                               <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                                  <span class="pull-left">
                                                                  <input type="checkbox" class="flat">
                                                                  </span>
                                                                  <span class="pull-left">
                                                                  From
                                                                  </span>
                                                                  </a>
                                                               </li>
                                                               <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                                  <span class="pull-left">
                                                                  <input type="checkbox" class="flat">
                                                                  </span>
                                                                  <span class="pull-left">
                                                                  To
                                                                  </span>
                                                                  </a>
                                                               </li>
                                                               <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                                  <span class="pull-left">
                                                                  <input type="checkbox" class="flat">
                                                                  </span>
                                                                  <span class="pull-left">
                                                                  User
                                                                  </span>
                                                                  </a>
                                                               </li>
                                                               <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                                  <span class="pull-left">
                                                                  <input type="checkbox" class="flat">
                                                                  </span>
                                                                  <span class="pull-left">
                                                                  When
                                                                  </span>
                                                                  </a>
                                                               </li>
                                                               <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                                  <span class="pull-left">
                                                                  <input type="checkbox" class="flat">
                                                                  </span>
                                                                  <span class="pull-left">
                                                                  Notes
                                                                  </span>
                                                                  </a>
                                                               </li>
                                                            </ul>
                                                         </div>
                                                         <thead>
                                                            <tr class="headings">
                                                               <th class="column-title">Product
                                                               </th>
                                                               <th class="column-title">From
                                                               </th>
                                                               <th class="column-title">To
                                                               </th>
                                                               <th class="column-title">User
                                                               </th>
                                                               <th class="column-title">When
                                                               </th>
                                                               <th class="column-title">Notes
                                                               </th>
                                                               </th>
                                                            </tr>
                                                         </thead>
                                                         <tbody>
                                                            <tr class="even pointer">
                                                               <td class=" ">Product Name</td>
                                                               <td class=" ">Kolkata</td>
                                                               <td class=" ">Mumbai</td>
                                                               <td class=" ">Jk</td>
                                                               <td class=" ">Monday</td>
                                                               <td class=" "></td>
                                                            </tr>
                                                            <tr class="odd pointer">
                                                               <td class=" ">Product Name</td>
                                                               <td class=" ">Kolkata</td>
                                                               <td class=" ">Mumbai</td>
                                                               <td class=" ">Jk</td>
                                                               <td class=" ">Monday</td>
                                                               <td class=" "></td>
                                                            </tr>
                                                         </tbody>
                                                      </table>
                                                   </div>
                                                </div>
                                                <!--surveys-->
                                                <div role="tabpanel" class="tab-pane" id="surveys">
                                                   <div class="table-responsive">
                                                      <table id="example8" class="display table table-striped table-bordered" cellspacing="0" width="100%">
                                                         <div class="hide-show-column dropdown" role="presentation">
                                                            <a id="drop4" href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" role="button" aria-expanded="false">Hide / Show Column<span class="caret"></span></a>
                                                            <ul id="menu6" class="dropdown-menu animated fadeInDown" role="menu">
                                                               <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                                  <span class="pull-left">
                                                                  <input type="checkbox" class="flat">
                                                                  </span>
                                                                  <span class="pull-left">
                                                                  </span>
                                                                  </a>
                                                               </li>
                                                               <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                                  <span class="pull-left">
                                                                  <input type="checkbox" class="flat">
                                                                  </span>
                                                                  <span class="pull-left">
                                                                  </span>
                                                                  </a>
                                                               </li>
                                                               <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                                  <span class="pull-left">
                                                                  <input type="checkbox" class="flat">
                                                                  </span>
                                                                  <span class="pull-left">
                                                                  </span>
                                                                  </a>
                                                               </li>
                                                               <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                                  <span class="pull-left">
                                                                  <input type="checkbox" class="flat">

                                                                  </span>
                                                                  <span class="pull-left">
                                                                  </span>
                                                                  </a>
                                                               </li>
                                                               <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                                  <span class="pull-left">
                                                                  <input type="checkbox" class="flat">
                                                                  </span>
                                                                  <span class="pull-left">
                                                                  </span>
                                                                  </a>
                                                               </li>
                                                               <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                                  <span class="pull-left">
                                                                  <input type="checkbox" class="flat">
                                                                  </span>
                                                                  <span class="pull-left">
                                                                  </span>
                                                                  </a>
                                                               </li>
                                                            </ul>
                                                         </div>
                                                         <thead>
                                                            <tr class="headings">
                                                               <th class="column-title">
                                                               </th>
                                                               <th class="column-title">
                                                               </th>
                                                               <th class="column-title">
                                                               </th>
                                                               <th class="column-title">
                                                               </th>
                                                               <th class="column-title">
                                                               </th>
                                                               <th class="column-title">
                                                               </th>
                                                               </th>
                                                            </tr>
                                                         </thead>
                                                         <tbody>
                                                            <tr class="even pointer">
                                                               <td class=" "></td>
                                                               <td class=" "></td>
                                                               <td class=" "></td>
                                                               <td class=" "></td>
                                                               <td class=" "></td>
                                                               <td class=" "></td>
                                                            </tr>
                                                            <tr class="odd pointer">
                                                               <td class=" "></td>
                                                               <td class=" "></td>
                                                               <td class=" "></td>
                                                               <td class=" "></td>
                                                               <td class=" "></td>
                                                               <td class=" "></td>
                                                            </tr>
                                                         </tbody>
                                                      </table>
                                                   </div>
                                                </div>
                                                <!--photo-->
                                                <div role="tabpanel" class="tab-pane" id="photo">
                                                   <div class="table-responsive">
                                                   </div>
                                                </div>
                                                <!--documents-->
                                                <div role="tabpanel" class="tab-pane" id="documents">
                                                   <div class="table-responsive stu-document">
                                                      <table id="example10" class="display table table-striped table-bordered" cellspacing="0" width="100%">
                                                         <div class="hide-show-column dropdown" role="presentation">
                                                            <a id="drop4" href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" role="button" aria-expanded="false">Hide / Show Column<span class="caret"></span></a>
                                                            <ul id="menu6" class="dropdown-menu animated fadeInDown" role="menu">
                                                               <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                                  <span class="pull-left">
                                                                  <input type="checkbox" class="flat">
                                                                  </span>
                                                                  <span class="pull-left">
                                                                  Name
                                                                  </span>
                                                                  </a>
                                                               </li>
                                                               <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                                  <span class="pull-left">
                                                                  <input type="checkbox" class="flat">
                                                                  </span>
                                                                  <span class="pull-left">
                                                                  Size
                                                                  </span>
                                                                  </a>
                                                               </li>
                                                               <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                                  <span class="pull-left">
                                                                  <input type="checkbox" class="flat">
                                                                  </span>
                                                                  <span class="pull-left">
                                                                  Type
                                                                  </span>
                                                                  </a>
                                                               </li>
                                                               <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                                  <span class="pull-left">
                                                                  <input type="checkbox" class="flat">
                                                                  </span>
                                                                  <span class="pull-left">
                                                                  Date Modified
                                                                  </span>
                                                                  </a>
                                                               </li>
                                                            </ul>
                                                         </div>
                                                         <thead>
                                                            <tr class="headings">
                                                               <th class="column-title">Name
                                                               </th>
                                                               <th class="column-title">Size
                                                               </th>
                                                               <th class="column-title">Type
                                                               </th>
                                                               <th class="column-title">Date modified
                                                               </th>
                                                               </th>
                                                            </tr>
                                                         </thead>
                                                         <tbody>
                                                            <tr class="even pointer">
                                                               <td class=" ">Jeeva</td>
                                                               <td class=" ">6''</td>
                                                               <td class=" ">JPG</td>
                                                               <td class=" ">22/04/2012</td>
                                                            </tr>
                                                            <tr class="odd pointer">
                                                               <td class=" ">Jeeva</td>
                                                               <td class=" ">6''</td>
                                                               <td class=" ">JPG</td>
                                                               <td class=" ">22/04/2012</td>
                                                            </tr>
                                                         </tbody>
                                                      </table>
                                                   </div>
                                                </div>
                                             </div>
                                          </form>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>


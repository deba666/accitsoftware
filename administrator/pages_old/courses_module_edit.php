<?php
if($_POST['code'])
	{
	if(insert_record("update `subject` set  `SUBJ_CODE`='".$_POST['code']."',`STATUS`='".$_POST['status']."',`SUBJ_DESC`='".$_POST['name']."',`HOURS`='".$_POST['hours']."',`VET_CODE`='".$_POST['sub_id']."',`VET_COMPETENCY_NO`='".$_POST['package']."',`VET_FIELD_NO`='".$_POST['program']."',`VET_NATIONAL_CODE`='".$_POST['intension']."',`VET_DELIVERY_MODE_NO`='".$_POST['mode']."',`VET_NAME`='".$_POST['foe']."',`WHO_DATE`='".$_POST['who']."',`USER_NO`='".$_POST['user']."' WHERE `SUBJECT_NO`=".$_GET['edit']))  
		$_SESSION['s_msg']="<strong>Fine!</strong> Student Subject Update";
		else
			$_SESSION['e_msg']="<strong>Oh snap!</strong> Student subject is not Update";
	}

if($_GET['edit'])
{
	$cond="where `SUBJECT_NO`=".$_GET['edit'];
	$sub_row=getRows($cond,'subject');
}

?>

<div class="right_col" role="main">

                  <div class="">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                      <div class="page-title">
                        <div class="title_left">
                        <?php 
						
                                    if($_SESSION['s_msg'] || $_SESSION['e_msg'])
                                    {
										?>
                                            <div class="alert <?=($_SESSION['s_msg'])?'alert-success':'alert-error'?> fade in">
                                                <button data-dismiss="alert" class="close" type="button">×</button>
                                                <?=($_SESSION['s_msg'])?$_SESSION['s_msg']:$_SESSION['e_msg']?>
                                            </div>
										<?php
                                    	unset($_SESSION['s_msg']);
                                    unset($_SESSION['e_msg']);
                                    }
                                ?>
                          <h3>Edit Course Module</h3>
                        </div>
                      </div>
                    </div>

                    <div class="clearfix"></div>

                    <div class="row">
                      <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                          <div class="clearfix"></div>

                          <div class="x_content">
                            <div class="row">
                              <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel recent-app">
                                  <div class="x_content">
                                    <div class="student-account-page">
                                      <!-- Nav tabs -->
                                      <div class="card">
                                        <div class="all-students-list add-course">
                                          <div class="add-student-section">
                                            <form enctype="multipart/form-data" method="post" action="" class="form-horizontal form-label-left">
                                                      <?php date_default_timezone_set('Australia/Sydney');
                                                        $date = date('d/m/Y h:i:s a', time());
														?>
													  <input id="who"  name="who"  type="hidden" value="<?php echo $date?>">
													  <input id="when"  name="user" type="hidden" value="<?php echo $_SESSION['user_no']?>">
                                              <div class="item form-group">
                                                <label for="name" class="control-label col-md-3 col-sm-3 col-xs-12">Code</label>
                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                  <div class="row">
                                                    <div class="col-md-3 col-sm-4 col-xs-12">
                                                      <input type="text" id="code" class="form-control col-md-12 col-xs-12" name="code" value="<?=$sub_row[0]['SUBJ_CODE']?>">
                                                    </div>
                                                    <div class="col-md-3 col-sm-4 col-xs-12">
                                                      <label class="pull-left"> Now status:<?=(!$sub_row[0]['STATUS'])?'Inactive':'Active'?></label>
                                                    </div>
                                                    <div class="col-md-2 col-sm-4 col-xs-12">
                                                      <select class="form-control" name="status" id="status">
                                                        <option value="1">Active</option>
                                                        <option value="0">Inactive</option>
                                                      </select>
                                                    </div>
                                                    
                                                  </div>
                                                </div>
                                              </div>
                                              
                                              <div class="item form-group">
                                                <label for="name" class="control-label col-md-3 col-sm-3 col-xs-12">Name</label>
                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                  <div class="row">
                                                    <div class="col-md-7 col-sm-7 col-xs-12">
                                                       <input type="text" value="<?=$sub_row[0]['SUBJ_DESC']?>" name="name" class="form-control col-md-12 col-xs-12" id="name">
                                                    </div>
                                                  </div>
                                                </div>
                                              </div>
                                              
                                              
                                              <div class="item form-group">
                                                <label for="name" class="control-label col-md-3 col-sm-3 col-xs-12">Total Hours/lebel</label>
                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                  <div class="row">
                                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                                      <input type="number" size="4" class="form-control input-text qty text" title="hours" value="<?=$sub_row[0]['HOURS']?>" id="quantity" name="hours" max="" min="1" step="1">
                                                    </div>
                                                  </div>
                                                </div>
                                              </div>
                                            
                                              
                                              <div class="item form-group">
                                                <label for="name" class="control-label col-md-3 col-sm-3 col-xs-12">Subject ID</label>
                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                  <div class="row">
                                                    <div class="col-md-7 col-sm-7 col-xs-12">
                                                       <input type="text" id="sub_id" class="form-control col-md-12 col-xs-12" name="sub_id" value="<?=$sub_row[0]['VET_CODE']?>">
                                                    </div>
                                                  </div>
                                                </div>
                                              </div>
                                              
                                              <div class="item form-group">
                                                <label class="control-label col-md-5 col-sm-3 col-xs-12" for="name">The Intension of this Module vocational or pre-vocational:</label>
                                                <div class="col-md-1 col-sm-9 col-xs-12">
                                                  <div class="row">
                                                    <div class="col-md-7 col-sm-7 col-xs-12">
                                                       <input type="checkbox" id="name" class="form-control col-md-12 col-xs-12" name="intension">
                                                    </div>
                                                  </div>
                                                </div>
                                              </div>
                                                <div class="item form-group">
                                                <label class="control-label col-md-5 col-sm-3 col-xs-12" for="name">This is the part of a VET school program:</label>
                                                <div class="col-md-1 col-sm-9 col-xs-12">
                                                  <div class="row">
                                                    <div class="col-md-7 col-sm-7 col-xs-12">
                                                       <input type="checkbox" id="name" class="form-control col-md-12 col-xs-12" name="program">
                                                    </div>
                                                  </div>
                                                </div>
                                              </div>
                                              
                                              <div class="item form-group">
                                                <label class="control-label col-md-5 col-sm-3 col-xs-12" for="name">This is a unit of competency in a national training package:</label>
                                                <div class="col-md-1 col-sm-9 col-xs-12">
                                                  <div class="row">
                                                    <div class="col-md-7 col-sm-7 col-xs-12">
                                                       <input type="checkbox" id="name" class="form-control col-md-12 col-xs-12" name="package">
                                                    </div>
                                                  </div>
                                                </div>
                                              </div>
                                              
                                              
                                              
                                              <div class="item form-group">
                                                <label for="name" class="control-label col-md-3 col-sm-3 col-xs-12">Delivery Mode:</label>
                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                  <div class="row">
                                                    <div class="col-md-7 col-sm-7 col-xs-12">
                                                       <input type="text" id="mode" class="form-control col-md-12 col-xs-12" name="mode" value="<?=$sub_row[0]['VET_DELIVERY_MODE_NO']?>">
                                                    </div>
                                                  </div>
                                                </div>
                                              </div>
                                              <div class="item form-group">
                                                <label for="name" class="control-label col-md-3 col-sm-3 col-xs-12">Field of Education:</label>
                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                  <div class="row">
                                                    <div class="col-md-7 col-sm-7 col-xs-12">
                                                       <input type="text" id="foe" class="form-control col-md-12 col-xs-12" name="foe" value="<?=$sub_row[0]['VET_NAME']?>">
                                                    </div>
                                                  </div>
                                                </div>
                                              </div>
                                              <div class="item form-group">
                                                <label for="name" class="control-label col-md-3 col-sm-3 col-xs-12"></label>
                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                  <div class="row">
                                                    <div class="col-md-6 col-sm-4 col-xs-12">
                                                      <p class="pull-left">
                                                        <button class="btn btn-primary" value="Reset" type="reset">Cancel</button>
                                                        <button class="btn btn-success" type="submit">Submit</button>
                                                      </p>
                                                    </div>
                                                  </div>
                                                </div>
                                              </div>
                                            </form>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                 </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
<?php
    /*
        * @package    ACCIT
         * @author    Paperlink Softwares Team
         * @copyright    Copyright (c) 2016 , Paperlink Softwares Pvt. Ltd.  (http://www.paperlinksoftwares.com/)
         * @since    Version 1.0.0
         * @filesource
		 * Code for displaying all the course module/Subject.(Edit and delete links are also given bellow in the code) Links Feth from the left sidebar under utility
         */
    ?>
<div class="right_col" role="main">
    <?php
        if($_GET['del'])
        {
        	if(delete_record("DELETE FROM `subject` WHERE `SUBJECT_NO` = ".$_GET['del']))
        	{ 
        		$_SESSION['s_msg']="Item successfully deleted";
        	}
        }?>
    <div class="">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="page-title">
                <div class="title_left">
                    <?php 
                        if($_SESSION['s_msg'] || $_SESSION['e_msg'])
                        {
                        ?>
                    <div class="alert <?=($_SESSION['s_msg'])?'alert-success':'alert-error'?> fade in">
                        <button data-dismiss="alert" class="close" type="button">×</button>
                        <?=($_SESSION['s_msg'])?$_SESSION['s_msg']:$_SESSION['e_msg']?>
                    </div>
                    <?php
                        unset($_SESSION['s_msg']);
                        unset($_SESSION['e_msg']);
                                               }
                                              ?>
                    <h3>Course Module </h3>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="clearfix"></div>
                    <div class="x_content">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel recent-app">
                                    <div class="x_content">
                                        <div class="student-account-page view-faculty-page">
                                            <!-- Nav tabs -->
                                            <div class="card">
                                                <div class="all-students-list add student">
                                                    <div class="add-student-section">
                                                        <div class="table-responsive">
                                                            <table class="table data-tbl-tools" cellspacing="0" width="100%">
                                                                <thead>
                                                                    <tr class="headings">
                                                                        <th class="column-title">Code
                                                                        </th>
                                                                        <th class="column-title">Name
                                                                        </th>
                                                                        <th class="column-title">Hours
                                                                        </th>
                                                                        <th class="column-title">Action
                                                                        </th>
                                                                    </tr>
                                                                </thead>
                                                                <?php $subject_row=getRows('order by `SUBJECT_NO` DESC','subject'); ?>
                                                                <tbody>
                                                                    <?php 
																	//Display all rhe subjects edit and delete links bellow in the code
                                                                        foreach($subject_row as $row)
                                                                        {
                                                                        ?>
                                                                    <tr class="even pointer">
                                                                        <td class=" "><?=$row['SUBJ_CODE'];?></td>
                                                                        <td class=" "><?=$row['SUBJ_DESC']?></td>
                                                                        <td class="last"><?=$row['HOURS']?></td>
                                                                        <td class="last"><a href="dashboard.php?op=<?=MD5('courses_module_edit')?>&edit=<?=$row['SUBJECT_NO'];?>" class="color-sky stting"><i class="fa fa-cog" aria-hidden="true"></i>Edit</a>| <a href="dashboard.php?op=<?=MD5('courses_module')?>&del=<?=$row['SUBJECT_NO'];?>" onclick="return confirm('Are you sure?'); return false;" class="color-sky stting"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>Delete</a></td>
                                                                    </tr>
                                                                    <?php } ?>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
error_reporting(E_ALL ^ (E_NOTICE | E_WARNING | E_DEPRECATED));
/*
        * @package    ACCIT
         * @author    Paperlink Softwares Team
         * @copyright    Copyright (c) 2016 , Paperlink Softwares Pvt. Ltd.  (http://www.paperlinksoftwares.com/)
         * @since    Version 1.0.0
         * @filesource
		 * Code for Database connection setting
         */
	
	include ("connection.php");
		
	$dbh=@mysql_connect ($servername, $username, $password) or die ('I cannot connect to the database because: ' . mysql_error());
	mysql_select_db($dbname);
	
	include("function.php");
	
	$admin_row=getRows('where `USER_NO`=1','users');
	
	define("SITE_URL", "http://localhost/accitsoftware/");
	define("ADMIN_MAIL", $admin_row[0]['EMAIL']);
    define("AVETMISS_URL", $_SERVER['DOCUMENT_ROOT']."/accitsoftware/");
	define("DISCLAIMER", 15);
	define("PRIVACY", 16);
	define("DATA_PROTECTION", 17);
	
	define("PROSPECTUS_FILE_PATH", "upload/images/");
	define("DOC_FILE_PATH", "upload/document/");
	

?>
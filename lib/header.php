<?php
session_start();
error_reporting(E_ALL ^ (E_NOTICE | E_WARNING | E_DEPRECATED));
include("administrator/settings.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<title>ACCIT - Australian college of commerce and information technology</title>

<!-- Bootstrap -->
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/custom.css" rel="stylesheet">
<link rel="stylesheet" href="css/build.css"/>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">


<!--Date Picker-->
<link rel="stylesheet" href="css/calender.css" type="text/css">


<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
</head>
<script type="text/javascript">
onbeforeunload = function(e){
	if(confirm('Are you nuts? Do you really want to leave me?')){
		return 'OK, Good Bye then';
	}
	else {
		e = e || event;
		if(e.preventDefault){e.preventDefault();}
		e.returnValue = false;
		return 'I said, "Are you nuts? Do you really want to leave me?"';
	}
}
 
</script>
<body>
<header>
  <div class="container">
    <div class="row">
      <div class="col-md-2 col-sm-2 col-xs-12">
        <h2 class="logo"><a href="index.html"><img src="images/logo.png" class="img-responsive"></a></h2>
      </div>
      <div class="col-md-7 col-sm-7 col-xs-12">
        <marquee><h4>Welcome To <span>ACCIT</span>Enrolment Section</h4></marquee>
      </div>
      <div class="col-md-2 col-sm-2 col-xs-12">
        <a href="javascript:void(0);" >
          <div class="close pull-right" data-toggle="tooltip" data-placement="left" title="Back to Home">
            <img src="images/close-icon.png" class="img-responsive" >
          </div>
        </a>
      </div>
    </div>
  </div>
</header>
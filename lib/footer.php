<footer>
  
</footer>




<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>

<!--Date Picker-->
<script type="text/javascript" src="js/zebra_datepicker.js"></script>
<script type="text/javascript" src="js/core.js"></script>

<!-- jQuery easing plugin -->
<script src="js/jquery.easing.min.js" type="text/javascript"></script>
<script src="js/classie.js"></script>
<script src="js/custom.js"></script>



<script>
    function init() {
        window.addEventListener('scroll', function(e){
            var distanceY = window.pageYOffset || document.documentElement.scrollTop,
                shrinkOn = 300,
                header = document.querySelector("header");
            if (distanceY > shrinkOn) {
                classie.add(header,"smaller");
            } else {
                if (classie.has(header,"smaller")) {
                    classie.remove(header,"smaller");
                }
            }
        });
    }
    window.onload = init();
</script>
<script type="text/javascript">
    function changeState(el) {
        if (el.readOnly) el.checked=el.readOnly=false;
        else if (!el.checked) el.readOnly=el.indeterminate=true;
    }
</script>
<script>
$(document).ready(function () {
    $('#id_radio1').click(function () {
       $('.slidingDiv').show('fast');
});
$('#id_radio2').click(function () {
      $('.slidingDiv').hide('fast');
 });
});
</script>
<script>
$(document).ready(function () {
    $('#id_radio6').click(function () {
       $('.slidingDiv2').show('fast');
       $('.slidingDiv7').hide('fast');
       $('.slidingDiv8').hide('fast');
});
$('#id_radio3').click(function () {
      $('.slidingDiv2').hide('fast');
      $('.slidingDiv7').hide('fast');
      $('.slidingDiv8').hide('fast');
 });
$('#id_radio4').click(function () {
      $('.slidingDiv2').hide('fast');
      $('.slidingDiv7').hide('fast');
      $('.slidingDiv8').hide('fast');
 });
$('#id_radio5').click(function () {
      $('.slidingDiv2').hide('fast');
      $('.slidingDiv7').hide('fast');
      $('.slidingDiv8').hide('fast');
 });
$('#id_radio7').click(function () {
      $('.slidingDiv2').hide('fast');
      $('.slidingDiv7').show('fast');
      $('.slidingDiv8').hide('fast');
 });
$('#id_radio8').click(function () {
      $('.slidingDiv2').hide('fast');
      $('.slidingDiv7').hide('fast');
      $('.slidingDiv8').show('fast');

 });
});
</script>
<script>
$(document).ready(function () {
    $('#id_radiovisa').click(function () {
       $('.slidingvisa').show('fast');
       $('.slidingvisa2').hide('fast');
       $('.slidingvisa3').hide('fast');
    });
    $('#id_radiovisa2').click(function () {
       $('.slidingvisa').hide('fast');
       $('.slidingvisa2').show('fast');
       $('.slidingvisa3').hide('fast');
    });
    $('#id_radiovisa3').click(function () {
       $('.slidingvisa').hide('fast');
       $('.slidingvisa2').hide('fast');
       $('.slidingvisa3').show('fast');
    });
});
</script>
<script>
$(document).ready(function () {
    $('#id_radiocourse').click(function () {
       $('.slidingcourse1').show('fast');
       $('.slidingcourse2').hide('fast');
       $('.slidingcourse3').hide('fast');
       $('.slidingcourse4').hide('fast');
       $('.slidingcourse5').hide('fast');
    });
    $('#id_radiocourse2').click(function () {
       $('.slidingcourse1').hide('fast');
       $('.slidingcourse2').show('fast');
       $('.slidingcourse3').hide('fast');
       $('.slidingcourse4').hide('fast');
       $('.slidingcourse5').hide('fast');
    });
    $('#id_radiocourse3').click(function () {
       $('.slidingcourse1').hide('fast');
       $('.slidingcourse2').hide('fast');
       $('.slidingcourse3').show('fast');
       $('.slidingcourse4').hide('fast');
       $('.slidingcourse5').hide('fast');
    });
    $('#id_radiocourse4').click(function () {
       $('.slidingcourse1').hide('fast');
       $('.slidingcourse2').hide('fast');
       $('.slidingcourse3').hide('fast');
       $('.slidingcourse4').show('fast');
       $('.slidingcourse5').hide('fast');
    });
    $('#id_radiocourse5').click(function () {
       $('.slidingcourse1').hide('fast');
       $('.slidingcourse2').hide('fast');
       $('.slidingcourse3').hide('fast');
       $('.slidingcourse4').hide('fast');
       $('.slidingcourse5').show('fast');
    });
});
</script>
<script>
$(document).ready(function () {
    $('#id_radiocourse6').click(function () {
       $('.slidingcourse6').show('fast');
});
$('#id_radiocourse7').click(function () {
      $('.slidingcourse6').hide('fast');
 });
});
</script>
<script>
$(document).ready(function () {
    $('#checkboxvisa3').click(function () {
       $('#passporthide').slideToggle('fast');
    });
});
</script>
<script type="text/javascript">
  $(function () {
    $('[data-toggle="tooltip"]').tooltip()
  })
</script>

<script type="text/javascript">
 $('#datepicker-example10').Zebra_DatePicker({
      view: 'years'
  });
</script>
<script type="text/javascript">
  $('#datepicker-example8').Zebra_DatePicker({
      format: 'M d, Y'
  });
</script>
<script type="text/javascript">
  $('#datepicker-example9').Zebra_DatePicker({
      format: 'M d, Y'
  });
</script>
<script type="text/javascript">
  $('#datepicker-exampleeducation').Zebra_DatePicker({
      format: 'M d, Y'
  });
</script>
<script type="text/javascript">
  $('#datepicker-exampleflight').Zebra_DatePicker({
      format: 'M d, Y'
  });
</script>

<!--<script type="text/javascript">
 $('#datepicker-example11').Zebra_DatePicker({
      view: 'years'
  });
</script>
<script type="text/javascript">
 $('#datepicker-example12').Zebra_DatePicker({
      view: 'years'
  });
</script>
<script type="text/javascript">
 $('#datepicker-example13').Zebra_DatePicker({
      view: 'years'
  });
</script>-->

<!--Another Course-->
<script>
$(document).ready(function () {
    $('#id_radioanothercourse').click(function () {
       $('.slidinganothercourse1').show('fast');
       $('.slidinganothercourse2').hide('fast');
       $('.slidinganothercourse3').hide('fast');
       $('.slidinganothercourse4').hide('fast');
       $('.slidinganothercourse5').hide('fast');
    });
    $('#id_radioanothercourse2').click(function () {
       $('.slidinganothercourse1').hide('fast');
       $('.slidinganothercourse2').show('fast');
       $('.slidinganothercourse3').hide('fast');
       $('.slidinganothercourse4').hide('fast');
       $('.slidinganothercourse5').hide('fast');
    });
    $('#id_radioanothercourse3').click(function () {
       $('.slidinganothercourse1').hide('fast');
       $('.slidinganothercourse2').hide('fast');
       $('.slidinganothercourse3').show('fast');
       $('.slidinganothercourse4').hide('fast');
       $('.slidinganothercourse5').hide('fast');
    });
    $('#id_radioanothercourse4').click(function () {
       $('.slidinganothercourse1').hide('fast');
       $('.slidinganothercourse2').hide('fast');
       $('.slidinganothercourse3').hide('fast');
       $('.slidinganothercourse4').show('fast');
       $('.slidinganothercourse5').hide('fast');
    });
    $('#id_radioanothercourse5').click(function () {
       $('.slidinganothercourse1').hide('fast');
       $('.slidinganothercourse2').hide('fast');
       $('.slidinganothercourse3').hide('fast');
       $('.slidinganothercourse4').hide('fast');
       $('.slidinganothercourse5').show('fast');
    });
});
</script>

<script>
$(document).ready(function () {
    $('#id_radioanothercourse6').click(function () {
       $('.slidinganothercourse6').show('fast');
});
$('#id_radioanothercourse7').click(function () {
      $('.slidinganothercourse6').hide('fast');
 });
});
</script>
<script>
$(document).ready(function () {
    $('#id_radiocourse8').click(function () {
       $('.slidingcourse8').show('fast');
});
$('#id_radiocourse9').click(function () {
      $('.slidingcourse8').hide('fast');
 });
});
</script>

<script>
$(document).ready(function () {
    $('#id_radiocourse16').click(function () {
       $('.slidingcourse16').show('fast');
});
$('#id_radiocourse17').click(function () {
      $('.slidingcourse16').hide('fast');
 });
});
</script>

<script>
$(document).ready(function () {
    $('.add-course').click(function () {
       $('.add-another-course-div').show('fast');
       $('.add-course').hide('fast');
       $('.remove-another-course').show('fast');
    });
    $('.remove-another-course').click(function () {
       $('.add-another-course-div').hide('fast');
       $('.add-course').show('fast');
       $('.remove-another-course').hide('fast');
    });
});
</script>
<script>
$(document).ready(function () {
    $('#id_radioeducation').click(function () {
    $('.slidingeducation1').show('fast');
});
$('#id_radioeducation2').click(function () {
      $('.slidingeducation1').hide('fast');
 });
$('#id_radioeducation3').click(function () {
      $('.slidingeducation1').hide('fast');
 });
});
</script>
<script>
$(document).ready(function () {
    $('#id_radiowork-program').click(function () {
    $('.slidingwork-program1').show('fast');
});
$('#id_radiowork-program2').click(function () {
      $('.slidingwork-program1').hide('fast');
 });
$('#id_radiowork-program3').click(function () {
      $('.slidingwork-program1').hide('fast');
 });
});
</script>

<script>
$(document).ready(function () {
    $('#id_radioflight').click(function () {
    $('.slidingflight1').show('fast');
});
$('#id_radioflight2').click(function () {
      $('.slidingflight1').hide('fast');
 });
});
</script>

</body>
</html>